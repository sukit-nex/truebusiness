ALTER TABLE `dn_section` ADD `thumb` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `sub_title`;

ALTER TABLE `dn_section` ADD `section_type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `thumb`;

ALTER TABLE `dn_section`  ADD `content_layout` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `section_type`,  ADD `m_content_layout` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `content_layout`;

ALTER TABLE `dn_section`  ADD `design_type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `m_content_layout`,  ADD `design_template` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `design_type`,  ADD `column_per_view` INT NOT NULL DEFAULT '0'  AFTER `design_template`;

ALTER TABLE `dn_section` ADD `page_code` INT NULL AFTER `page`;

ALTER TABLE `dn_section` ADD `section_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `page`;

ALTER TABLE `dn_section` ADD `video_thumb` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `content_editor`;

ALTER TABLE `dn_banner`  ADD `bg_type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `logo_banner`,  ADD `content_position_hoz` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `bg_type`,  ADD `content_position_ver` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `content_position_hoz`;

ALTER TABLE `dn_banner` ADD `video_type` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `video`;

ALTER TABLE `dn_banner` ADD `video_file` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `video_type`;

ALTER TABLE `dn_section`  ADD `custom_class` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `video_id`,  ADD `ele_img1` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `custom_class`,  ADD `ele_img2` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `ele_img1`,  ADD `ele_img3` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `ele_img2`,  ADD `ele_img4` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `ele_img3`;

ALTER TABLE `dn_section_cate` ADD `sec_code` INT NULL AFTER `code`;

ALTER TABLE `dn_banner` CHANGE `cate_code` `sec_code` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `dn_banner` ADD `video_thumb` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `video_file`;

ALTER TABLE `dn_section` ADD `video_type` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `video_id`;

ALTER TABLE `dn_section` ADD `video_file` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `video_type`;

ALTER TABLE `dn_section` ADD `section_layout` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `section_name`;

ALTER TABLE `dn_section` ADD `bg_size` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `bg_fixed`;

ALTER TABLE `dn_section` ADD `gen_height` INT NOT NULL DEFAULT '0' AFTER `bg_fixed`;

ALTER TABLE `dn_section` ADD `display_per_column` INT NOT NULL DEFAULT '1' AFTER `column_per_view`;

ALTER TABLE `dn_section_lists`  ADD `content` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `post_time`,  ADD `content_editor` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `content`,  ADD `price` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `content_editor`,  ADD `btn_show` INT NOT NULL DEFAULT '0'  AFTER `price`,  ADD `btn_text` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `btn_show`,  ADD `hero_banner` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `btn_text`,  ADD `hero_banner_m` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `hero_banner`,  ADD `link_type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `hero_banner_m`;

ALTER TABLE `dn_section` ADD `column_id` INT NULL AFTER `page_code`;

ALTER TABLE `dn_row` ADD `is_container` INT NOT NULL DEFAULT '0' AFTER `row_bg_m`;

ALTER TABLE `dn_section` ADD `widget` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `mg_mobile`;

ALTER TABLE `dn_section` ADD `listing_alignment` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'center' AFTER `display_per_column`;

ALTER TABLE `dn_section` ADD `tab_show` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `design_template`;

ALTER TABLE `dn_section` ADD `tab_setting` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `tab_show`;

ALTER TABLE `dn_section` ADD `hl_set` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `tab_setting`;

ALTER TABLE `dn_section` ADD `btn_more` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `hl_set`;

ALTER TABLE `dn_seo` ADD `seo_type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'page' AFTER `share_img`;

ALTER TABLE `db_section_lists`  ROW_FORMAT=DYNAMIC;

ALTER TABLE `dn_section_lists` ADD `slug` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `link_out`;

ALTER TABLE `dn_section_cate` ADD `slug` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `onoff`;

ALTER TABLE `dn_section` ADD `video_thumb_m` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `video_thumb`;

ALTER TABLE `dn_menu` ADD `slug` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `status`;

ALTER TABLE `dn_section_lists` ADD `is_highlight` INT NOT NULL DEFAULT '0' AFTER `slug`;

ALTER TABLE `dn_section` ADD `list_limit` INT NOT NULL DEFAULT '0' AFTER `section_type`;

ALTER TABLE `dn_section_lists` ADD `tags` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `is_highlight`;

ALTER TABLE `dn_section_lists` ADD `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `sub_title`;

ALTER TABLE `dn_section`  ADD `link_type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `widget`,  ADD `page_internal_code` INT NULL  AFTER `link_type`,  ADD `link_out` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL  AFTER `page_internal_code`;

ALTER TABLE `dn_section` ADD `btn_text` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `link_out`;

ALTER TABLE `dn_section` ADD `is_tags` INT NOT NULL DEFAULT '0' AFTER `sticky`;

CREATE TABLE IF NOT EXISTS dn_section_lists_preview LIKE dn_section_lists;

CREATE TABLE IF NOT EXISTS dn_section_preview LIKE dn_section;

ALTER TABLE `dn_section_lists_preview` ADD `preview` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `is_tags`;

ALTER TABLE `dn_section_preview` ADD `preview` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `is_tags`;

ALTER TABLE `dn_section_lists` ADD `approve_date` DATETIME NULL AFTER `is_tags`, ADD `approve_status` INT NOT NULL DEFAULT '0' AFTER `approve_date`;

ALTER TABLE `dn_section_lists` ADD `submit_date` DATETIME NULL AFTER `is_tags`;

ALTER TABLE `dn_section_lists` ADD `approve_note` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `approve_status`;

UPDATE `dn_section_lists` SET `approve_status` = '2';

ALTER TABLE `dn_section_lists` ADD `approve_by` INT NULL AFTER `approve_date`;

ALTER TABLE `dn_section` ADD `acc_type` INT NOT NULL DEFAULT '0' AFTER `btn_text`;

ALTER TABLE `dn_section` ADD `acc_switch` INT NOT NULL DEFAULT '0' AFTER `acc_type`;

ALTER TABLE `dn_section_lists` ADD `published_date` DATETIME NULL AFTER `approve_note`;

ALTER TABLE `dn_section` ADD `sec_title_alignment` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'center' AFTER `acc_switch`;

ALTER TABLE `dn_section` ADD `cate_title_alignment` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'left' AFTER `sec_title_alignment`;

ALTER TABLE `dn_section_preview` ADD `sec_title_alignment` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'center' AFTER `acc_switch`;

ALTER TABLE `dn_section_preview` ADD `cate_title_alignment` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'left' AFTER `sec_title_alignment`;

/* After Live 09 - Jun - 2021 */
ALTER TABLE `dn_section` ADD `custom_css` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `cate_title_alignment`;

ALTER TABLE `dn_section_preview` ADD `custom_css` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `cate_title_alignment`;

ALTER TABLE `dn_section` ADD `sort_option` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'asc' AFTER `custom_css`;

ALTER TABLE `dn_section_preview` ADD `sort_option` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'asc' AFTER `custom_css`;

ALTER TABLE `dn_section_cate` ADD `cate_permission` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `image_hover`;

ALTER TABLE `dn_section_lists_preview` ADD `published_date` DATETIME NULL AFTER `preview`;

ALTER TABLE `dn_section_cate` ADD `must_be_approve` INT NOT NULL DEFAULT '0' AFTER `cate_permission`;

ALTER TABLE `dn_section_cate` CHANGE `sec_code` `sec_code` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

UPDATE `dn_user` SET `user_type` = 'WC' WHERE `dn_user`.`user_type` = 'W';

ALTER TABLE `dn_coverage_history` ADD `geojson_status` INT NOT NULL DEFAULT '0' AFTER `geojson_file`;

ALTER TABLE `dn_section` ADD `section_template` VARCHAR(255) NULL AFTER `section_type`;

ALTER TABLE `dn_section_cate` ADD `thumb_image` VARCHAR(255) NULL AFTER `must_be_approve`, ADD `bg_d` VARCHAR(255) NULL AFTER `thumb_image`, ADD `bg_m` VARCHAR(255) NULL AFTER `bg_d`;

ALTER TABLE `dn_section_cate` ADD `prefix_title` VARCHAR(255) NULL AFTER `sec_key`;

ALTER TABLE `dn_section_cate` ADD `banner_top_d` VARCHAR(255) NULL AFTER `u_by`, ADD `banner_top_m` VARCHAR(255) NULL AFTER `banner_top_d`;