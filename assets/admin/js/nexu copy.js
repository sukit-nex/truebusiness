var sortMenu;
$(document).ready( function(){
  //sortableMenuColumn();

  sortableMenuItem();
  console.log( 'Nex column '+$('.nexu-column').length );
  $('.nexu-column').each( function(){
    console.log( 'Nex Menu '+$(this).find('.nexu-menu').length );
  });

  $('.update-menu').click( function(){
    updateMenu();
  });
  $('.nexu-btn-addmenu').click( function(){
    console.log('click');
    var cate_name = $(this).closest('.nexu-column').find('.nexu-cate-title h2').html();
    var cate_code = $(this).closest('.nexu-column').attr('data-cate-code');
    $('#nexuModal').find('.nexu-modal-title').html( 'เพิ่มเมนูในกลุ่ม '+cate_name );
  });

  $('.nav-type').click( function(){
    var type = $(this).attr('data-value');
    $('input[name="type"]').val(type);
  });

});

function editMemu( $this ){
  $('.loader').fadeOut();
  var menu_code = $this.attr('data-element');
  var token = $('input[name="token"]').val();
  console.log( 'Menu code = ' + menu_code );
  $.ajax( base_url+'admin/menu/loadMenuByCode', {
    method: "POST",
    data:  {'menu_code' : menu_code, 'token' : token } ,
    dataType: 'json',
    success: function ( arresult ) {
      if(arresult.rs){
        $('.loader').fadeOut();
        $('input[name="menu_code"]').val( arresult.menus[0].code );
        // Add value to name with lang
        $.each( arresult.menus , function(index , value ){
          var lang_text = value.lang;
          $('.lang_'+lang_text+' input[name="name[]"]').val( value.name );
          console.log( value.lang + ' : '+value.name );
        });

        //Active menu type
        $('.nav-type').removeClass('active');
        $('#type-'+arresult.menus[0].type+'-tab').addClass('active');
        $('.tab-type').removeClass('active');
        $('#type-'+arresult.menus[0].type).addClass('active');

        // Set Is new tab
        $("input[name=is_newtab][value='"+arresult.menus[0].is_newtab+"']").prop("checked",true);

        // Set Status
        $("input[name=onoff][value='"+arresult.menus[0].onoff+"']").prop("checked",true);

        $('#nexuModal').modal('show');
        console.log('load menu to edit complete');
      }
    }
  });

}

function updateMenu(){
  $('.loader').fadeIn();
  var menu = [];
  /*$('.nexu-column').each( function(){
    console.log( 'Nex Menu '+$(this).find('.nexu-menu').length );
  });*/

  $('.nexu-menu').each( function(index,$value){
    var cate_code = $(this).closest('.nexu-column').attr('data-cate-code');
    var title = $(this).find('.nexu-nexu-title').html();
    var code = $(this).attr('data-code');
    var is_parent = $(this).closest('.nexu-menu-box').find('.nexu-sub-box').find('.nexu-menu').length > 0 ? 1 : 0 ;
    var parent_code = '';
    if( $(this).parent().hasClass('nexu-sub-box') ){
      parent_code = $(this).closest('.nexu-menu-box').attr('data-parent-code');
    }
    var sticky = index;
    menu.push( {  'cate_code' : cate_code,
                  'title' : title,
                  'code' : code,
                  'is_parent' : is_parent,
                  'parent_code' : parent_code,
                  'sticky' : sticky
                }
             );
  });
  var token = $('input[name="token"]').val();

  $.ajax( base_url+'admin/menu/updatemenu', {
    method: "POST",
    data:  {'token' : token ,
            'menus' : menu
          } ,
    dataType: 'json',
    success: function ( arresult ) {
      if(arresult.rs){
        $('.loader').fadeOut();
        window.location.href = window.location.href;
        console.log('complete');
      }
    }
  });
  //console.log( menu );
  //menu.push({'id':'1','name':'Ohm'},{'id':'2','name':'Ohm 2'},{'id':'3','name':'Ohm 3'});
  //console.log( menu );
  /*$.each( menu,function(index,value){
    console.log( value['name'] );
  });*/
}

function sortableMenuColumn(){
  $( ".nexu-row" ).sortable({
    connectWith: ".nexu-row",
  });
  $( ".nexu-row" ).disableSelection();
}
function sortableMenuItem(){
  sortMenu = $( ".nexu-move" ).sortable({
    connectWith: ".nexu-move",
    placeholder: "ui-sortable-placeholder",
    over: function( event, ui ) {
      console.log('over');
      //console.log(ui.item[0].innerText);
    },
    stop: function( event, ui ) {
      //console.log(  ui.item.parent().parent().attr( 'data-code') );
      //console.log( ui.item.index() )
      //console.log( ui.item.parent().parent().find('.nexu-menu').length );
      //console.log(  ui.item.closest('.nexu-menu').attr( 'data-code') );
        //console.log(ui.position);
      console.log(  'Drag el data code = ' + ui.item.closest('.nexu-menu').attr( 'data-code') );
      console.log(  'Place el data code = ' + ui.item.closest('.nexu-menu').parent().parent().attr( 'data-code') );
      console.log( 'Parent level  = ' + ui.item.parent().parent().closest('.nexu-move').attr('data-level'));
      if( typeof( ui.item.parent().parent().closest('.nexu-move').attr('data-level') ) !== "undefined" ){
        parent_level = ui.item.parent().parent().closest('.nexu-move').attr('data-level');
        current_level = parseInt( parent_level ) + 1;
      }else{
        current_level = 0;
      }
      ui.item.parent().closest('.nexu-move').attr('data-level', current_level);
      console.log( current_level );



    },
    tolerance: "pointer"
  });
  $( ".nexu-move" ).disableSelection();

}

function setMenu(){
  console.log('Set menu start');
}
