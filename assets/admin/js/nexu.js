$(document).ready( function(){
  sortableMenuItem();

  $('.update-menu').click( function(){
    update_menu('update-menu-form');
  });

  $('.nav-type').click( function(){
    var type = $(this).attr('data-value');
    $('input[name="type"]').val(type);
  });
});
function editMenu( $this ){
  var code = $this.closest('.drag-list').attr('data-code');
  //console.log( 'Edit code is '+code );
  var token = $('input[name="token"]').val();
  $.ajax( base_url+'admin/menu/edit', {
    method: "POST",
    data:  {'token' : token ,
            'code' : code
          } ,
    dataType: 'json',
    success: function ( arresult ) {
        if(arresult.rs){
        //console.log( arresult.details );
            $.each( arresult.details, function( key, value ) {
                //console.log( key + ": " + value );
                $('input[name="menu_code"]').val( value.code );
                $('input[name="parent_code"]').val( value.parent_code );

                $('.lang_'+key).find('.nameInput').val( value.name );

                $('#type-'+value.type+'-tab').trigger('click');
                if( value.type == "page" ){
                    //$('select[name="page_code"]').val( value.page_code );
                    $('select[name="page_code"] option').each( function( index ){
                    //console.log( $(this).val() );
                    if( $(this).val() == value.page_code ){
                        $(this).attr( 'selected', 'selected' );
                    }
                });
                } else if( value.type == "link" ){
                    $('input[name="link"]').val( value.link );
                } else if( value.type == "slug" ){
                    $('input[name="slug"]').val( value.slug );
                }

                //$('select[name="position"]').val( value.position );
                $('select[name="position"] option').each( function( index ){
                    if( $(this).val() == value.position ){
                        $(this).attr( 'selected', 'selected' );
                    }
                });

                $('input[name="is_newtab"]').removeAttr('checked');
                $('input[name="is_newtab"]').each( function( index ){
                    if( $(this).val() == value.is_newtab ){
                        $(this).prop( 'checked', true );
                    }
                });

                $('input[name="onoff"]').removeAttr('checked');
                $('input[name="onoff"]').each( function( index ){
                    if( $(this).val() == value.onoff ){
                        $(this).prop( 'checked', true );
                    }
                });

            });
            $('html, body').animate({
                scrollTop: ($(".main-panel").offset().top - 80)
            }, 2000);
        }
    }
  });
}
function activeMenu( $this ){
  var code = $this.closest('.drag-list').attr('data-code');
  console.log( 'Active code is '+code );
  var token = $('input[name="token"]').val();
  var onoff = 0;
  if( $this.closest('.drag-list').hasClass('non-active') ){
    onoff = 1;
  }
  $.ajax( base_url+'admin/menu/activeMenu', {
    method: "POST",
    data:  {'token' : token ,
            'code' : code,
            'onoff' : onoff
          } ,
    dataType: 'json',
    success: function ( arresult ) {
      if(arresult.rs){
        if( onoff == 1 ){
          $this.closest('.drag-list').removeClass('non-active');
          $this.find('i').removeClass('mdi-eye-off');
          $this.find('i').addClass('mdi-eye');
        }else{
          $this.closest('.drag-list').addClass('non-active');
          $this.find('i').removeClass('mdi-eye');
          $this.find('i').addClass('mdi-eye-off');
        }
      }
    }
  });
}
function sortableMenuItem(){
  $('.nexu-move').nestedSortable({
		forcePlaceholderSize: true,
		items: 'li',
		handle: 'div',
		placeholder: 'drag-highlight',
		listType: 'ul',
		maxLevels: 2,
		opacity: .6,
    stop: function( event, ui ) {
      var place_position = ui.item.closest('.drag-column').attr( 'data-position');
      console.log( 'Place position : '+ place_position );
      var position = ui.item.closest('.drag-list').attr( 'data-position');
      console.log( 'Position : '+ position );
      if( place_position != position ){
        $(this).sortable( "cancel" );
      }else{
        var drag_code = ui.item.closest('.drag-list').attr( 'data-code');
        var parent_code = ui.item.closest('.drag-list').parent().parent().attr( 'data-code') ;

        var parent_level = 0;
        var current_level = 0;
        console.log( 'drag code : '+drag_code);
        console.log( 'parent code : '+parent_code);
        if( typeof( ui.item.parent().parent().closest('.nexu-move').attr('data-level') ) !== "undefined" ){
          parent_level = ui.item.parent().parent().closest('.nexu-move').attr('data-level');
          current_level = parseInt( parent_level ) + 1;
        }else{
          current_level = 0;
        }
        if( current_level == 0 ){
          ui.item.parent().closest('.nexu-move').attr('data-level', current_level);
          ui.item.closest('.drag-list').removeClass('level-0');
          ui.item.closest('.drag-list').removeClass('level-1');
          ui.item.closest('.drag-list').removeClass('level-2');
          ui.item.closest('.drag-list').addClass('level-0');
          updateMenuSort( drag_code, parent_code );
        }else if( current_level == 1 ){
          ui.item.parent().closest('.nexu-move').attr('data-level', current_level);
          ui.item.closest('.drag-list').removeClass('level-0');
          ui.item.closest('.drag-list').removeClass('level-1');
          ui.item.closest('.drag-list').removeClass('level-2');
          ui.item.closest('.drag-list').addClass('level-1');
          updateMenuSort( drag_code, parent_code );
        }else if( current_level == 2 ){
          ui.item.parent().closest('.nexu-move').attr('data-level', current_level);
          ui.item.closest('.drag-list').removeClass('level-0');
          ui.item.closest('.drag-list').removeClass('level-1');
          ui.item.closest('.drag-list').removeClass('level-2');
          ui.item.closest('.drag-list').addClass('level-2');
          updateMenuSort( drag_code, parent_code );
        }


        //updateMenuSort( drag_code, parent_code );
      }
    }
	});
}

function updateMenuSort( drag_code, parent_code ){
  //$('.loader').show();
  var token = $('input[name="token"]').val();
  $.ajax( base_url+'admin/menu/updateSort', {
    method: "POST",
    data:  {'token' : token ,
            'code' : drag_code ,
            'parent_code' : parent_code
          } ,
    dataType: 'json',
    success: function ( arresult ) {
      if(arresult.rs){
        console.log('complete');
      }
    }
  });

  //Mixer New Sort
  var sortArray = [];
  
  //console.log($('.drag-box  > ul > li').length);
  var box_level_0 = $('.drag-box  > ul > li');
  var max_level_0 = box_level_0.length;
  //console.log( max_level_0 );
  if( max_level_0 > 0 ){
    var i = max_level_0;
    box_level_0.each( function( index_lv0, value_lv0 ){
  
        var ar_value = {
          'code' : $(this).attr('data-code'),
          'sticky' : i
        }
        sortArray.push( ar_value );

        // Level 1 zone
        var box_level_1 = $(this).find('ul.submenu-list > li');
        var max_level_1 = box_level_1.length;
        if( box_level_1.length > 0 ){
          var i_1 = max_level_1;
          box_level_1.each( function( index_lv1 ){

            var ar_value = {
              'code' : $(this).attr('data-code'),
              'sticky' : i_1
            }
            sortArray.push( ar_value );

            // Level 2 zone
            var box_level_2 = $(this).find('ul.submenu-list > li');
            var max_level_2 = box_level_2.length;
            if( box_level_2.length > 0 ){
              var i_2 = max_level_2;
              box_level_2.each( function( index_lv2){

                var ar_value = {
                  'code' : $(this).attr('data-code'),
                  'sticky' : i_2
                }
                sortArray.push( ar_value );

                i_2--;
              }); // End each level 2
            } // End if level 2

            i_1--;
          }); // End each level 1
        } // End if level 1

        //console.log('LV-'+index_lv0+' Child >'+max_level_1);  

        i--;
      
    }) // End each level 0


    // ajax call after push sorted array
    setTimeout( function(){
      $.ajax( base_url+'admin/menu/updateSortList', {
        method: "POST",
        data:  {'token' : token ,
                'menulists' : sortArray
              } ,
        dataType: 'json',
        success: function ( arresult ) {
          if(arresult.rs){
            console.log(' updatesortlist complete');
            $('.loader').hide();
          }
        }
      });
    },1500);

  } // End if level 0

}


function updateMenuSort_backup20210618( drag_code, parent_code ){
  $('.loader').show();
  var token = $('input[name="token"]').val();
  $.ajax( base_url+'admin/menu/updateSort', {
    method: "POST",
    data:  {'token' : token ,
            'code' : drag_code ,
            'parent_code' : parent_code
          } ,
    dataType: 'json',
    success: function ( arresult ) {
      if(arresult.rs){
        console.log('complete');
      }
    }
  });
  // Auto sorting There
  var sortArray = [];

  var max_level_0 = $('.level-0').length;
  if( max_level_0 > 0 ){
      var i = max_level_0;
      $('.level-0').each( function( index_lv0 ){
        if( i > 0 ){
          var ar_value = {
            'code' : $(this).attr('data-code'),
            'sticky' : i
          }
          sortArray.push( ar_value );
          // Level 1 zone
          var max_level_1 = $(this).find('.level-1').length
          if( max_level_1 > 0 ){
            var x = max_level_1;
            $(this).find('.level-1').each( function( index_lv1 ){
              if( x > 0 ){
              var ar_value = {
                'code' : $(this).attr('data-code'),
                'sticky' : x
              }
              sortArray.push( ar_value );
              // Level 2 zone
              var max_level_2 = $(this).find('.level-2').length
              console.log('Max level 2 is '+max_level_2);
              if( max_level_2 > 0 ){
                var y = max_level_2;
                $(this).find('.level-2').each( function( index_lv2 ){
                if( y > 0 ){
                    var ar_value = {
                      'code' : $(this).attr('data-code'),
                      'sticky' : y
                    }
                    sortArray.push( ar_value );
                    y--;
                  }
                });
              }
              // End level 2
              x--;
            }
            });
            console.log(' has level 1 ');
          }else{
            console.log(' no level 1 ');
          }
          // End level 1
          i--;
        }
      });
      // ajax call after push sorted array
      setTimeout( function(){
        $.ajax( base_url+'admin/menu/updateSortList', {
          method: "POST",
          data:  {'token' : token ,
                  'menulists' : sortArray
                } ,
          dataType: 'json',
          success: function ( arresult ) {
            if(arresult.rs){
              console.log(' updatesortlist complete');
              $('.loader').hide();
            }
          }
        });
      },1500);
    }
}

function update_menu( form_class ){
  $('.loader').show();
  var form = $('.'+form_class )[0];
  var formData = new FormData(form);
  event.preventDefault();
  $.ajax( base_url+'admin/menu/updateMenu', {
    method: "POST",
    processData: false,
    contentType: false,
    data:  formData ,
    dataType: 'json',
    success: function ( arresult ) {
      console.log( arresult.rs );
      if(arresult.rs){
        $('.loader').hide();
        window.location.href = window.location.href;
      }
    }
  });
}
