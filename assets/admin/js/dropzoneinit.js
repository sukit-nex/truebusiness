(function($) {
  'use strict';
  Dropzone.autoDiscover = false;
  $("#my-gallery-dropzone").dropzone({
    url: base_url+"admin/upload/do_upload_dropzone_gall",
    success: function(file,response){
      var obj = JSON.parse( response );
      var html = "";
      html += '<div class="col-md-2 col-sm-4 col-xs-6 img-wrap">';
      html += '  <div class="img-preview">';
      html += '    <img src="'+obj.filename+'">';
      html += '    <div class="img-icon-group" onclick="deleteImage( $(this) )" data-id="'+obj.id+'">';
      html += '      <div class="icon-box"><i class="mdi mdi-delete-forever"></i></div>';
      html += '    </div>';
      html += '  </div>';
      html += '</div>';
      $('.img-list-append').append( html );
      //console.log( response );
    }
  });
//  Dropzone.autoDiscover = false;

/*$(function() {
    Dropzone.options.uiDZResume = {
        success: function(file, response){
            alert(response);
        }
    };
}); */

})(jQuery);
