(function($) {
  'use strict';
  $(function() {
    $('.repeater-features').repeater({
      // (Optional)
      // "defaultValues" sets the values of added items.  The keys of
      // defaultValues refer to the value of the input's name attribute.
      // If a default value is not specified for an input, then it will
      // have its value cleared.
      defaultValues: {
        'text-input': 'foo',
        'title': '',
        'description': ''
      },
      // (Optional)
      // "show" is called just after an item is added.  The item is hidden
      // at this point.  If a show callback is not given the item will
      // have $(this).show() called on it.
      show: function() {

        var list_group_item_count = $('.repeater .list-group .list-group-item').length;

        $(this).find('#features_thailand-0-tab').attr('href','#features_thailand-'+list_group_item_count);
        $(this).find('#features_english-0-tab').attr('href','#features_english-'+list_group_item_count);
        $(this).find('#features_thailand-0').attr('id','features_thailand-'+list_group_item_count);
        $(this).find('#features_english-0').attr('id','features_english-'+list_group_item_count);
               
        //remove image dropify
        $(this).find('.dropify-message, .dropify-loader, .dropify-errors-container, .dropify-preview, .dropify-clear').remove();
        $(this).find('.dropify-wrapper').removeClass();
        $(this).find('.dropify').attr('data-default-file', '');

        // start image dropify
        var drop = $('.dropify').dropify();
        drop = drop.data('dropify');
        $(this).find('.dropify-clear').trigger('click');

        $(this).slideDown();
      },
      // (Optional)
      // "hide" is called when a user clicks on a data-repeater-delete
      // element.  The item is still visible.  "hide" is passed a function
      // as its first argument which will properly remove the item.
      // "hide" allows for a confirmation step, to send a delete request
      // to the server, etc.  If a hide callback is not given the item
      // will be deleted.
      hide: function(deleteElement) {
        if (confirm('Are you sure you want to delete this element?')) {
          $(this).slideUp(deleteElement);
        }
      },
      // (Optional)
      // Removes the delete button from the first list item,
      // defaults to false.
      //isFirstItemUndeletable: true
    })
  });
})(jQuery);