
function approve_confirm( $this, onoff = 1 ){
	var code = $this.attr('data-code');
	swal({
		title: 'คุณแน่ใจใช่ไหม ?',
		icon: 'warning',
		showCancelButton: true,
		buttons: {
			cancel: {
				text: "ยกเลิก",
				value: null,
				visible: true,
				className: "btn btn-danger",
				closeModal: true,
			},
			confirm: {
				text: "ยืนยัน",
				value: true,
				visible: true,
				className: "btn btn-primary",
				closeModal: true
			}
		}
	}).then((willDelete) => {
		if (willDelete) {
			var formData = new FormData();
			formData.append('code', code);
			formData.append('onoff', onoff);
			formData.append('token', $("input[name='token']").val());
			$.ajax(base_url + 'admin/section/confirmApprove', {
				method: "POST",
				processData: false,
				contentType: false,
				data: formData,
				dataType: 'json',
				success: function (arresult) {
					if (arresult.rs) {
						location.reload();
					} else {
						//
					}
				}
			});
		}
	})
}
function approve_reject( $this ) {
	var code = $this.attr('data-code');
	swal({
		title: 'คุณแน่ใจใช่ไหม ?',
		text: "กรุณาใส่เหตุผลในการไม่อนุมัติ",
		icon: 'warning',
		showCancelButton: true,
		 content: {
		   element: "input",
		   attributes: {
		     placeholder: "Type the reason",
		     type: "text",
		     class: 'form-control',
		     name: 'approve_note'
		   },
		 },
		buttons: {
			cancel: {
				text: "ยกเลิก",
				value: null,
				visible: true,
				className: "btn btn-danger",
				closeModal: true,
			},
			confirm: {
				text: "ยืนยัน",
				value: true,
				visible: true,
				className: "btn btn-primary",
				closeModal: true
			}
		}
	}).then((willDelete) => {
		if (willDelete) {
			var formData = new FormData();
			formData.append('code', code);
			formData.append('approve_note', $('input[name=approve_note]').val());
			formData.append('token', $("input[name='token']").val());
			$.ajax(base_url + 'admin/section/rejectApprove', {
				method: "POST",
				processData: false,
				contentType: false,
				data: formData,
				dataType: 'json',
				success: function (arresult) {
					if (arresult.rs) {
						location.reload();
					} else {
						//
					}
				}
			});
		}
	})
}