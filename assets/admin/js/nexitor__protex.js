// JavaScript Document

var activeElementPopup = '';
$( document ).ready( function(){
  $('.btn-addelement').hide();
  installNexitor();
  $('.saveForm').click( function(){
    $('.loader').fadeIn();
    generateHtmlData('save');
  });
  /*$('form').on('submit', function(){
    submitFrom();
  });*/

  $('.btn-nexitor-preview').click( function(){
	  $('.nexitor-preview-here').html('');
	  $('.nexitor-preview-here').removeClass('before_disable');
	  setTimeout(function(){
		  generateHtmlData('preview');
	  },1000)
  });
});
function generateHtmlData(action){

  var action_fn = action;
  $install = $('.install-nexitor');
  $nexitor_row = $('.nexitor-row');
  $nexitor_col = $('.nexitor-col');
  var html = "";
  $install.each( function(index_in,value_in){ // Each nexitor
    // Add text area to install section
    var name = $(this).attr('data-area');
    var textarea = '<textarea name="'+name+'[]" style="position:absolute;left:0;top:0;z-index:-20;"></textarea>';
    var textarea_editor = '<textarea name="'+name+'_editor[]" style="position:absolute;left:0;top:0;z-index:-20;"></textarea>';
    if( $('textarea[name="'+name+'[]"]').length == 0 ){
      $(this).append( textarea );
    }
    if( $('textarea[name="'+name+'_editor[]"]').length == 0 ){
      $(this).append( textarea_editor );
    }
    // Each nexitor row for create html of row
    $nexitor_row.each( function(index,value){
      html += '<div class="row">';
      $(this).find('.nexitor-col').each( function(index,value){
          html += '<div class="'+$(this).attr('class')+'">';
          $(this).find('.box-nexitor').each( function( index,value ){
            var type = $(this).attr('data-type');
            if( type == "title"){
              var text = $(this).find('input').val();
              var style = $(this).find('input').attr('style');
              var htag = $(this).find('.nexitor-body').attr('data-htag');
              var align = $(this).find('.nexitor-body').attr('data-align');
              var link = $(this).find('.nexitor-body').attr('data-link');
              var bold = $(this).find('.nexitor-body').attr('data-bold');
              var italic = $(this).find('.nexitor-body').attr('data-italic');
              var underline = $(this).find('.nexitor-body').attr('data-underline');

              if( link == "" ){
                html += '<div class="box-content '+align+'">';
                html += ' <'+htag+' class="'+bold+' '+italic+' '+underline+'" style="'+style+'">'+text+'</'+htag+'>';
                html += '</div>';
              }else{

                html += ' <div class="box-content '+align+'">';
				        html += '	<a href="'+link+'" target="_blank">';
                html += '   	<'+htag+' class="'+bold+' '+italic+' '+underline+'" style="'+style+'">'+text+'</'+htag+'>';
				        html += '	</a>';
                html += ' </div>';

              }
            }else if( type == "content"){
              var text = $(this).find('.nexitor-textarea').html();
              html += '<div class="box-content '+align+'">';
              html += text;
              html += '</div>';
            }else if( type == "image" ){
              if( $(this).find('.dropify-wrapper').hasClass('has-preview') ){

                var src_img = $(this).find('.dropify-wrapper').find('.dropify-render').find('img').attr('src');
                var new_image = false;
                if( src_img.indexOf("data:image") >= 0 ){
                  new_image = true;
                }

                var filename = $(this).find('.dropify-filename-inner').html();
                if( new_image ){
                  var time = $('input[name="time"]').val();
                  file_basename = filename.split(".")[0];
                  file_ext = filename.split(".")[1];
                  filename = file_basename+'_'+time+'.'+file_ext;
                }

                //====
                var width = $(this).find('.nexitor-body').attr('data-width');
                var align = $(this).find('.nexitor-body').attr('data-align');
                var link = $(this).find('.nexitor-body').attr('data-link');
        				if( action_fn == "save" ){
        					if( link == "" ){
        					  html += '<div class="box-content '+align+'">';
        					  html += ' <img src="'+upload_path+''+filename+'" class="'+width+'" />';
        					  html += '</div>';
        					}else{

        					  html += '<div class="box-content '+align+'">';
        					  html += '	<a href="'+link+'" target="_blank">';
        					  html += '   <img src="'+upload_path+''+filename+'" class="'+width+'" />';
        					  html += '	</a>';
        					  html += '</div>';

        					}
        				}else{
        					$(this).find('.dropify-render').find('img').addClass(width);
        					var img_html = $(this).find('.dropify-render').html();


        					//var src = $(this).find('.dropify-render').find('img').attr('src');

        					if( link == "" ){
        					  html += '<div class="box-content '+align+'">';
        					  html += img_html;
        					  //html += ' <img src="'+src+'" class="'+width+'" />';
        					  html += '</div>';
        					}else{
        					  html += '<div class="box-content '+align+'">';
        					  html += '	<a href="'+link+'" target="_blank">';
        					  html += img_html;
        					  //html += ' <img src="'+src+'" class="'+width+'" />';
        					  html += '	</a>';
        					  html += '</div>';
        					}
        				}


              }
            }else if(  type == "video" ){
              var text = $(this).find('input').val();
              var width = $(this).find('.nexitor-body').attr('data-width');
              var align = $(this).find('.nexitor-body').attr('data-align');
              var ratio = $(this).find('.nexitor-body').attr('data-ratio');

              html += '<div class="box-content '+align+'">';
              html += ' <div class="embed-responsive embed-responsive-'+ratio+' d-inline-block '+width+'">';
  						html += '		<iframe src="'+text+'" allowfullscreen=""></iframe>';
							html += '  </div>';
              html += '</div>';
            }
          });
          html += '</div>';
      });
      html += '</div>';
    });
    $(this).find('textarea[name="'+name+'[]"]').val( html );
    $(this).find('input').each( function(){
      $(this).attr('value', $(this).val() );
    });
	if(action_fn == 'save'){
	  $('.dropify_editor').each( function(){
		if( $(this).closest('.dropify-wrapper').hasClass('has-preview') ){
      var src_img = $(this).closest('.dropify-wrapper').find('.dropify-render').find('img').attr('src');
      var new_image = false;
      if( src_img.indexOf("data:image") >= 0 ){
        new_image = true;
      }
		  $(this).closest('.dropify-wrapper').find('.dropify-render').find('img').attr('src','');
		  var nameoffile = $(this).closest('.dropify-wrapper').find('.dropify-filename-inner').html();
		  if( nameoffile != ""){
        var time = $('input[name="time"]').val();
        if( new_image ){
          var file_basename = nameoffile.split(".")[0];
          var file_ext = nameoffile.split(".")[1];
          $(this).attr('data-default-file',upload_path+''+file_basename+'_'+time+'.'+file_ext );
        }
		  }
		}else{
		  $(this).attr('data-default-file','');
		}
	  });
	}
    /*$('svg').each( function(){
      $(this).replaceWith('<img src="'+$(this).attr('data-url')+'" class="svg">');
    });*/
    $(this).find('textarea[name="'+name+'_editor[]"]').val( $(this).html() );

  });


  if(action_fn == 'save'){

	submitForm();

  }else{
	$('.tab-pane').each(function(index, element) {
        if( $(this).hasClass('active') ){
			var name = $('.tab-pane').find( '.install-nexitor' ).attr('data-area');
			var html = $('.tab-pane').find( 'textarea[name="'+name+'[]"]' ).val();
			$('.nexitor-preview-here').html(html);
			$('.nexitor-preview-here').addClass('before_disable');
			$('.loader').fadeOut();
		}
    });

  }

}
function submitForm(){
  $('form.mainForm').submit();
}
function installNexitor(){
  $('.install-nexitor').each( function(){
    if( $(this).find('.box-layer').length == 0){
      $(this).append( loadInstallHTML() );
    }
  });
  $('body').prepend( loadPopupHTML() );
  $('.nexitor-choice').click( function(){
    nexitorClick( $(this) );
  });
  $(".summernote-onlycode").summernote({
    toolbar: [
      ['view', ['codeview']],
    ],
    height: 500,
    codemirror: { // codemirror options
      theme: 'default',
      lineNumbers: true
    },
  });
  //$(".summernote-onlycode").summernote('codeview.toggle');
  
  $(".summernote").summernote({
  	toolbar: [
  	  ['style', ['bold', 'italic', 'underline', 'clear']],
  	  ['fontsize', ['height','color']],
  	  ['para', [ 'ul', 'ol', 'paragraph']],
  	  ['insert', ['link','table', 'hr']],
      ['misc', ['']],
      ['view', ['codeview']],
  	],
    height: 500,
    callbacks:{
      onImageUpload : function( files, editor, welEditable ){
        for( var i = files.length - 1; i >= 0; i-- ){
          sendFile(files[i], this);
        }
      },
      onPaste: function (e) {
          if (document.queryCommandSupported("insertText")) {
              var text = $(e.currentTarget).html();
              var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

              setTimeout(function () {
                  document.execCommand('insertText', false, bufferText);
              }, 10);
              e.preventDefault();
          } else { //IE
              var text = window.clipboardData.getData("text")
              if (trap) {
                  trap = false;
              } else {
                  trap = true;
                  setTimeout(function () {
                      document.execCommand('paste', false, text);
                  }, 10);
                  e.preventDefault();
              }
          }

      }
    }
  });
  $('.dropify_editor').each( function(){
    var drop = $(this).dropify();
    drop = drop.data('dropify');
    drop.destroy();
    drop.destroy();
    drop.init();
  });
  sortableRow();
  sortableElement();
}
function addColumn( $this ){
  var column = $this.attr('data-columns');
  var html = loadLayoutHTML( column );
  $this.closest('.install-nexitor').find('.box-layer').append( html );
  generateSVG();
  sortableRow();
}
function elementClick( $this ){
  var element = $this.attr('data-element');
  var html = "";
  if( element == "title" ){
    html = loadTitleHTML();
  }else if( element == "content" ){
    html = loadContentHTML();
  }else if( element == "image" ){
    html = loadImageHTML();
  }else if( element == "video" ){
    html = loadVideoHTML();
  }
  $this.closest('.nexitor-col').find('.add-element').append( html );
  //$(this).closest('.nexitor-col').find('.box-element').hide();
  $this.closest('.nexitor-col').find('.btn-addelement').show();
  generateSVG();
  sortableElement();
  if( element == "image" ){
    $this.closest('.nexitor-col').find('.dropify_editor').dropify();
  }
}
function loadInstallHTML(){
  var html = "";
  html += '<div class="box-layer sortable-row"></div>';
  html += '<div class="box-addcolumn ">';
  html += '    <div class="addcolumn">';
  html += '        <div class="addcolumn-list"><a href="javascript:void(0)" class="addcolumn-style addcolumn-1 addcolumn-choice" data-columns="1" onclick="addColumn( $(this) )"><span></span></a></div>';
  html += '          <div class="addcolumn-list">';
  html += '            <a href="javascript:void(0)" class="addcolumn-style addcolumn-2-11 "><span></span><span></span></a>';
  html += '              <div class="addcolumn-hover">';
  html += '                  <a href="javascript:void(0)" class="addcolumn-style addcolumn-2-11 addcolumn-choice" data-columns="211" onclick="addColumn( $(this) )"><span></span><span></span></a>';
  html += '                  <a href="javascript:void(0)" class="addcolumn-style addcolumn-2-13 addcolumn-choice" data-columns="213" onclick="addColumn( $(this) )"><span></span><span></span></a>';
  html += '                  <a href="javascript:void(0)" class="addcolumn-style addcolumn-2-31 addcolumn-choice" data-columns="231" onclick="addColumn( $(this) )"><span></span><span></span></a>';
  html += '              </div>';
  html += '          </div>';
  html += '          <div class="addcolumn-list"><a href="javascript:void(0)" class="addcolumn-style addcolumn-3 addcolumn-choice" data-columns="3" onclick="addColumn( $(this) )"><span></span><span></span><span></span></a></div>';
  html += '          <div class="addcolumn-list"><a href="javascript:void(0)" class="addcolumn-style addcolumn-4 addcolumn-choice" data-columns="4" onclick="addColumn( $(this) )"><span></span><span></span><span></span><span></span></a></div>';
  html += '      </div>';
  html += '      <div class="addcolumn-text">Choose columns</div>';
  html += '  </div>';
  return html;
}
function loadLayoutHTML( column ){
  var html = "";
   html += '<div class="content-row grid-margin">';
   html += '  <div class="content-row-control">';
   html += '    <ul>';
   //html += '         <li><a href="javascript:void(0)" onClick="nexitorpopup( $(this) )"><img src="'+assets_path+'images/nexitor/icon-settings.svg" class="svg"></a></li>';
   html += '         <li><a href="javascript:void(0)"><img src="'+assets_path+'images/nexitor/icon-move.svg" class="svg"></a></li>';
   html += '         <li><a href="javascript:void(0)" class="close-content-row" onclick="closeContentRow( $(this) );"><img src="'+assets_path+'images/nexitor/icon-cancel.svg" class="svg"></a></li>';
   html += '    </ul>';
   html += '  </div>';
   html += '  <div class="row nexitor-row">';
        if( column == '1' ){
          html += '<div class="col col-12 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
        }
        else if( column == '211' ){
          html += '<div class="col col-12 col-sm-6 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-6 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
        }
        else if( column == '213' ){
          html += '<div class="col col-12 col-sm-3 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-9 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
        }
        else if( column == '231' ){
          html += '<div class="col col-12 col-sm-9 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-3 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
        }
        else if( column == '3' ){
          html += '<div class="col col-12 col-sm-4 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-4 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-4 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
        }
        else if( column == '4' ){
          html += '<div class="col col-12 col-sm-3 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-3 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-3 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-3 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
        }
   html += '  </div>';
   html += '</div>';
   return html;
}

function loadActionButtonHTML(){
  var html = "";
  //html += '<a href="javascript:void(0)" class="btn-addelement" onclick="displayBoxElement($(this))"><span><img src="'+assets_path+'images/nexitor/icon-plus.svg" class="svg"></span></a>';
  html += '<div class="box-element show">';
  html += '    <div class="group-element">';
  html += '        <div class="element-list" data-element="title" onclick="elementClick($(this))">';
  html += '            <div>';
  html += '                  <div class="main-element-icon"><img src="'+assets_path+'images/nexitor/icon-title.svg"></div>';
  html += '                  <div class="main-element-text">Title</div>';
  html += '              </div>';
  html += '          </div>';
  html += '          <div class="element-list" data-element="content" onclick="elementClick($(this))">';
  html += '            <div>';
  html += '                  <div class="main-element-icon"><img src="'+assets_path+'images/nexitor/icon-content.svg"></div>';
  html += '                  <div class="main-element-text">Content</div>';
  html += '              </div>';
  html += '          </div>';
  html += '          <div class="element-list" data-element="image" onclick="elementClick($(this))">';
  html += '            <div>';
  html += '                  <div class="main-element-icon"><img src="'+assets_path+'images/nexitor/icon-image.svg"></div>';
  html += '                  <div class="main-element-text">Image</div>';
  html += '              </div>';
  html += '          </div>';
  html += '          <div class="element-list" data-element="video" onclick="elementClick($(this))">';
  html += '            <div>';
  html += '                  <div class="main-element-icon"><img src="'+assets_path+'images/nexitor/icon-video.svg"></div>';
  html += '                  <div class="main-element-text">Video</div>';
  html += '              </div>';
  html += '          </div>';
  html += '      </div>';
  html += '  </div>';
  return html;
}
function loadNexitorTool( element ){
  var html = "";
  html += '<div class="nexitor-tools">';
  html += ' <ul>';
  html += '   <li><a href="javascript:void(0)" onClick="nexitorpopup( $(this) )" data-element="'+element+'"><img src="'+assets_path+'images/nexitor/icon-settings.svg" class="svg"></a></li>';
  html += '   <li><a href="javascript:void(0)"><img src="'+assets_path+'images/nexitor/icon-move.svg" class="svg"></a></li>';
  html += '   <li><a href="javascript:void(0)" class="close-nexitor" onclick="closeNexitor( $(this) );"><img src="'+assets_path+'images/nexitor/icon-cancel.svg" class="svg"></a></li>';
  html += ' </ul>';
  html += '</div>';
  return html;
}
function loadTitleHTML(){
  var html = "";
  html += '<div class="box-nexitor nexitor-type-title" data-type="title">';
  html += '    <div class="nexitor-head">';
  html += '        <div class="nexitor-headLeft">';
  html += '            <div class="nexitor-headLeft-logo"><img src="'+assets_path+'images/nexitor/icon-title.svg"></div>';
  html += '            <div class="nexitor-used">';
  html += '                <div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-h6.svg" class="svg"></div>';
  html += '              </div>';
  html += '          </div>';
  html += loadNexitorTool('title');
  html += '      </div>';
  html += '      <div class="nexitor-body" data-htag="h6" data-align="none" data-link="" data-bold="" data-italic="" data-underline="" data-color="">';
  html += '        <div class="nexitor-body-input"><input class="h6" name="text-title" type="text" placeholder="Please fill your head line"></div>';
  html += '      </div>';
  html += '  </div>';
  return html;
}
function loadContentHTML(){
  var html = "";
  html += '<div class="box-nexitor nexitor-type-content" data-type="content">';
  html += '    <div class="nexitor-head">';
  html += '        <div class="nexitor-headLeft"><div class="nexitor-headLeft-logo"><img src="'+assets_path+'images/nexitor/icon-content.svg"></div></div>';
  html += loadNexitorTool('content');
  html += '      </div>';
  html += '      <div class="nexitor-body">';
  html += '        <div class="nexitor-body-textarea"><div class="nexitor-textarea"></div></div>';
  html += '      </div>';
  html += '  </div>';
  return html;
}
function loadImageHTML(){
  var html = "";
  html += '<div class="box-nexitor nexitor-type-image" data-type="image">';
  html += '    <div class="nexitor-head">';
  html += '        <div class="nexitor-headLeft">';
  html += '            <div class="nexitor-headLeft-logo"><img src="'+assets_path+'images/nexitor/icon-image.svg"></div>';
  html += '              <div class="nexitor-used">';
  html += '              </div>';
  html += '          </div>';
  html += loadNexitorTool('image');
  html += '      </div>';
  html += '      <div class="nexitor-body" data-width="none" data-align="none" data-link="">';
  html += '        <!-- Dropify here -->';
  html += '        <input type="file" name="image_editor[]" class="dropify_editor" data-max-file-size="3000kb" data-default-file="" />';
  html += '      </div>';
  html += '  </div>';
  return html;
}
function loadVideoHTML(){
  var html = "";
  html += '<div class="box-nexitor nexitor-type-video" data-type="video">';
  html += '    <div class="nexitor-head">';
  html += '        <div class="nexitor-headLeft">';
  html += '            <div class="nexitor-headLeft-logo"><img src="'+assets_path+'images/nexitor/icon-video.svg"></div>';
  html += '              <div class="nexitor-used">';
  html += '                  <div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-ratio-16-9.svg" class="svg"></div>';
  html += '              </div>';
  html += '          </div>';
  html += loadNexitorTool('video');
  html += '      </div>';
  html += '      <div class="nexitor-body" data-width="none" data-align="none" data-ratio="16by9" >';
  html += '        <div class="nexitor-body-input"><input type="text" name="text-video" placeholder="Please fill your youtube video embed"></div>';
  html += '      </div>';
  html += '  </div>';
  return html;
}
function closePopup( $this ){
  $this.closest('.section-nexitorPopup').hide();
}
function loadPopupHTML(){
  var html = "";
  html += '<div class="section-nexitorPopup nexitorPopup-title">';
  html += '  	<div class="nexitorPopup-bg"></div>';
  html += '  	<div class="nexitorPopup-container">';
  html += '      	<div class="nexitorPopup-close" onclick="closePopup( $(this) )"><img src="'+assets_path+'images/nexitor/icon-cancel.svg" class="svg"></div>';
  html += '      	<div class="nexitorPopup-content">';
  html += '          	<div class="nexitorPopup-content-head"><div class="nexitorPopup-content-headtitle"><img src="'+assets_path+'images/nexitor/icon-title.svg"> <h2>Title</h2></div></div>';
  html += '              <div class="nexitorPopup-content-body">';
  html += '              	<div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Title :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice htag-choice htag-h1 active" data-tag="h1"><img src="'+assets_path+'images/nexitor/icon-h1.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice htag-choice htag-h2" data-tag="h2"><img src="'+assets_path+'images/nexitor/icon-h2.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice htag-choice htag-h3" data-tag="h3"><img src="'+assets_path+'images/nexitor/icon-h3.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice htag-choice htag-h4" data-tag="h4"><img src="'+assets_path+'images/nexitor/icon-h4.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice htag-choice htag-h5" data-tag="h5"><img src="'+assets_path+'images/nexitor/icon-h5.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice htag-choice htag-h6" data-tag="h6"><img src="'+assets_path+'images/nexitor/icon-h6.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '              	<div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Style :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice style-choice style-none" data-tag="none"><img src="'+assets_path+'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice style-choice style-font-weight-bold" data-tag="font-weight-bold"><img src="'+assets_path+'images/nexitor/icon-bold.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice style-choice style-font-italic" data-tag="font-italic"><img src="'+assets_path+'images/nexitor/icon-italic.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice style-choice style-font-underline" data-tag="font-underline"><img src="'+assets_path+'images/nexitor/icon-underline.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Alignment :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-none active " data-tag="none"><img src="'+assets_path+'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-left" data-tag="text-left"><img src="'+assets_path+'images/nexitor/icon-align-left.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-center" data-tag="text-center"><img src="'+assets_path+'images/nexitor/icon-align-center.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-right" data-tag="text-right"><img src="'+assets_path+'images/nexitor/icon-align-right.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Link :</div>';
  html += '                      <div class="nexitorPopup-content-body-input">';
  html += '                      	<input type="text" name="text-link" placeholder="Please fill your link">';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Color :</div>';
  html += '                      <div class="nexitorPopup-content-body-input">';
  html += '                      	<input type="text" name="text-color" placeholder="Please fill your color code">';
  html += '                      </div>';
  html += '                  </div>';
  html += '              </div>';
  html += '              <div class="nexitorPopup-content-footer">';
  html += '              	<div>';
  html += '                  	<button type="button" class="btn btn-secondary btn-fw" onclick="closePopup( $(this) )">Cancel</button>';
  html += '                    <button type="button" class="btn btn-dark btn-fw updateButton" onclick="updateElement( $(this) )" data-element="">Done</button>';
  html += '                  </div>';
  html += '              </div>';
  html += '          </div>';
  html += '      </div>';
  html += '  </div>';
  html += '  <div class="section-nexitorPopup nexitorPopup-content">';
  html += '  	<div class="nexitorPopup-bg"></div>';
  html += '  	<div class="nexitorPopup-container">';
  html += '      	<div class="nexitorPopup-close" onclick="closePopup( $(this) )"><img src="'+assets_path+'images/nexitor/icon-cancel.svg" class="svg"></div>';
  html += '      	<div class="nexitorPopup-content">';
  html += '          	<div class="nexitorPopup-content-head"><div class="nexitorPopup-content-headtitle"><img src="'+assets_path+'images/nexitor/icon-content.svg"> <h2>Content</h2></div></div>';
  html += '              <div class="nexitorPopup-content-body">';
  html += '              	<div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Title :</div>';
  html += '                      <div class="nexitorPopup-content-body-editor">';
  html += '                        <!-- Summernote here -->';
  html += '                        <textarea id="summernoteTH" class="form-control summernote" name="text-content" rows="10"></textarea>';
  html += '                      </div>';
  html += '                  </div>';
  html += '              </div>';
  html += '              <div class="nexitorPopup-content-footer">';
  html += '              	<div>';
  html += '                  	<button type="button" class="btn btn-secondary btn-fw" onclick="closePopup( $(this) )">Cancel</button>';
  html += '                    <button type="button" class="btn btn-dark btn-fw updateButton" onclick="updateElement( $(this) )" data-element="">Done</button>';
  html += '                  </div>';
  html += '              </div>';
  html += '          </div>';
  html += '      </div>';
  html += '  </div>';
  html += '  <div class="section-nexitorPopup nexitorPopup-image">';
  html += '  	<div class="nexitorPopup-bg"></div>';
  html += '  	<div class="nexitorPopup-container">';
  html += '      	<div class="nexitorPopup-close" onclick="closePopup( $(this) )"><img src="'+assets_path+'images/nexitor/icon-cancel.svg" class="svg"></div>';
  html += '      	<div class="nexitorPopup-content">';
  html += '          	<div class="nexitorPopup-content-head"><div class="nexitorPopup-content-headtitle"><img src="'+assets_path+'images/nexitor/icon-image.svg"> <h2>Image</h2></div></div>';
  html += '              <div class="nexitorPopup-content-body">';
  html += '              	<div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Width (%) :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-none active " data-tag="none"><img src="'+assets_path+'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-25" data-tag="w-25"><img src="'+assets_path+'images/nexitor/icon-width-25.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-50" data-tag="w-50"><img src="'+assets_path+'images/nexitor/icon-width-50.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-75" data-tag="w-75"><img src="'+assets_path+'images/nexitor/icon-width-75.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-100" data-tag="w-100"><img src="'+assets_path+'images/nexitor/icon-width-100.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Alignment :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-none active " data-tag="none"><img src="'+assets_path+'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-left" data-tag="text-left"><img src="'+assets_path+'images/nexitor/icon-align-left.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-center" data-tag="text-center"><img src="'+assets_path+'images/nexitor/icon-align-center.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-right" data-tag="text-right"><img src="'+assets_path+'images/nexitor/icon-align-right.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Link :</div>';
  html += '                      <div class="nexitorPopup-content-body-input">';
  html += '                      	<input type="text" name="text-link" placeholder="Please fill your link">';
  html += '                      </div>';
  html += '                  </div>';
  html += '              </div>';
  html += '              <div class="nexitorPopup-content-footer">';
  html += '              	<div>';
  html += '                  	<button type="button" class="btn btn-secondary btn-fw" onclick="closePopup( $(this) )">Cancel</button>';
  html += '                    <button type="button" class="btn btn-dark btn-fw updateButton" onclick="updateElement( $(this) )" data-element="">Done</button>';
  html += '                  </div>';
  html += '              </div>';
  html += '          </div>';
  html += '      </div>';
  html += '  </div>';
  html += '  <div class="section-nexitorPopup nexitorPopup-video">';
  html += '  	<div class="nexitorPopup-bg"></div>';
  html += '  	<div class="nexitorPopup-container">';
  html += '      	<div class="nexitorPopup-close" onclick="closePopup( $(this) )"><img src="'+assets_path+'images/nexitor/icon-cancel.svg" class="svg"></div>';
  html += '      	<div class="nexitorPopup-content">';
  html += '          	<div class="nexitorPopup-content-head"><div class="nexitorPopup-content-headtitle"><img src="'+assets_path+'images/nexitor/icon-video.svg"> <h2>Video</h2></div></div>';
  html += '              <div class="nexitorPopup-content-body">';
  html += '              	<div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Width (%) :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-none active " data-tag="none"><img src="'+assets_path+'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-25" data-tag="w-25"><img src="'+assets_path+'images/nexitor/icon-width-25.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-50" data-tag="w-50"><img src="'+assets_path+'images/nexitor/icon-width-50.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-75" data-tag="w-75"><img src="'+assets_path+'images/nexitor/icon-width-75.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-100" data-tag="w-100"><img src="'+assets_path+'images/nexitor/icon-width-100.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Alignment :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-none active " data-tag="none"><img src="'+assets_path+'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-left" data-tag="text-left"><img src="'+assets_path+'images/nexitor/icon-align-left.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-center" data-tag="text-center"><img src="'+assets_path+'images/nexitor/icon-align-center.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-right" data-tag="text-right"><img src="'+assets_path+'images/nexitor/icon-align-right.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Ratio :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice ratio-choice ratio-16by9 active" data-tag="16by9"><img src="'+assets_path+'images/nexitor/icon-ratio-16-9.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice ratio-choice ratio-4by3" data-tag="4by3"><img src="'+assets_path+'images/nexitor/icon-ratio-4-3.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice ratio-choice ratio-1by1" data-tag="1by1"><img src="'+assets_path+'images/nexitor/icon-ratio-1-1.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '              </div>';
  html += '              <div class="nexitorPopup-content-footer">';
  html += '              	<div>';
  html += '                  	<button type="button" class="btn btn-secondary btn-fw" onclick="closePopup( $(this) )">Cancel</button>';
  html += '                    <button type="button" class="btn btn-dark btn-fw updateButton" onclick="updateElement( $(this) )" data-element="">Done</button>';
  html += '                  </div>';
  html += '              </div>';
  html += '          </div>';
  html += '      </div>';
  html += '  </div>';
  return html;
}
function nexitorpopup( $this ){
  generateSVG();
  var element = $this.attr('data-element');
  activeElementPopup = $this;
  $popup = $('.nexitorPopup-'+element);
  $popup.find('.updateButton').attr('data-element', element );
  $nexitor_body = $this.closest('.box-nexitor').find('.nexitor-body');
  if( element == "title" ){
    var htag = $nexitor_body.attr('data-htag');
    var align = $nexitor_body.attr('data-align');

    var bold = $nexitor_body.attr('data-bold');
    var italic = $nexitor_body.attr('data-italic');
    var underline = $nexitor_body.attr('data-underline');

    var link = $nexitor_body.attr('data-link');
    var color = $nexitor_body.attr('data-color');

    $popup.find('.htag-choice').removeClass('active');
    $popup.find('.htag-'+htag).addClass('active');

    $popup.find('.align-choice').removeClass('active');
    $popup.find('.align-'+align).addClass('active');

    if( bold == "" && italic == "" && underline == "" ){
      $popup.find('.style-choice').removeClass('active');
      $popup.find('.style-none').addClass('active');
    }else{
      $popup.find('.style-'+bold).addClass('active');
      $popup.find('.style-'+italic).addClass('active');
      $popup.find('.style-'+underline).addClass('active');
    }
    $popup.find('input[name="text-link"]').val(link);
    $popup.find('input[name="text-color"]').val(color);
  }else if( element == "content" ){
    var content = $nexitor_body.find('.nexitor-textarea').html();
    $popup.find('.note-editable').html( content );
  }else if( element == "image" ){
    var width = $nexitor_body.attr('data-width');
    var align = $nexitor_body.attr('data-align');
    var link = $nexitor_body.attr('data-link');

    $popup.find('.width-choice').removeClass('active');
    $popup.find('.width-'+width).addClass('active');

    $popup.find('.align-choice').removeClass('active');
    $popup.find('.align-'+align).addClass('active');

    $popup.find('input[name="text-link"]').val(link);
  }else if( element == "video" ){
    var width = $nexitor_body.attr('data-width');
    var align = $nexitor_body.attr('data-align');
    var ratio = $nexitor_body.attr('data-ratio');

    $popup.find('.width-choice').removeClass('active');
    $popup.find('.width-'+width).addClass('active');

    $popup.find('.align-choice').removeClass('active');
    $popup.find('.align-'+align).addClass('active');

    $popup.find('.ratio-choice').removeClass('active');
    $popup.find('.ratio-'+ratio).addClass('active');
  }
  $popup.show();
}
function updateElement( $this ){
  var element = $this.attr('data-element');
  $popup = $this.closest('.section-nexitorPopup');
  $boxNexitor = activeElementPopup.closest('.box-nexitor');
  if( element == "title" ){
    var htag = $popup.find('.htag-choice.active').attr('data-tag');
    var align = $popup.find('.align-choice.active').attr('data-tag');
    var bold = ( $popup.find('.style-font-weight-bold').hasClass('active') ) ? $popup.find('.style-font-weight-bold').attr('data-tag') : '';
    var italic = ( $popup.find('.style-font-italic').hasClass('active') ) ? $popup.find('.style-font-italic').attr('data-tag') : '';
    var underline = ( $popup.find('.style-font-underline').hasClass('active') ) ? $popup.find('.style-font-underline').attr('data-tag') : '';
    var link = $popup.find('input[name="text-link"]').val();
    var color = $popup.find('input[name="text-color"]').val();

    $boxNexitor.find('input[name="text-title"]').attr('class','');
    $boxNexitor.find('input[name="text-title"]').addClass(htag);
    $boxNexitor.find('input[name="text-title"]').addClass(align);
    $boxNexitor.find('input[name="text-title"]').addClass(bold);
    $boxNexitor.find('input[name="text-title"]').addClass(italic);
    $boxNexitor.find('input[name="text-title"]').addClass(underline);
    $boxNexitor.find('input[name="text-title"]').css({'color':color});

    $boxNexitor.find('.nexitor-body').attr('data-htag', htag );
    $boxNexitor.find('.nexitor-body').attr('data-align', align );
    $boxNexitor.find('.nexitor-body').attr('data-bold', bold );
    $boxNexitor.find('.nexitor-body').attr('data-italic', italic );
    $boxNexitor.find('.nexitor-body').attr('data-underline', underline );
    $boxNexitor.find('.nexitor-body').attr('data-link', link );
    $boxNexitor.find('.nexitor-body').attr('data-color', color );
    var html = "";
    html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-'+htag+'.svg" class="svg"></div>';
    if( align != "none" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-align-'+align+'.svg" class="svg"></div>';
    }
    if( link != "" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-link.svg" class="svg"></div>';
    }
    if( color != "" ){
      html += '<div class="nexitor-used-list" style="background-color:'+color+'"><img src="'+assets_path+'images/nexitor/icon-paint.svg" class="svg"></div>';
    }
    $boxNexitor.find('.nexitor-used').html( html );

  }else if( element == "content" ){
    var content = $popup.find('.note-editable').html();
    $boxNexitor.find('.nexitor-textarea').html( content );
  }else if( element == "image" ){
    var width = $popup.find('.width-choice.active').attr('data-tag');
    var align = $popup.find('.align-choice.active').attr('data-tag');
    var link = $popup.find('input[name="text-link"]').val();

    $boxNexitor.find('.nexitor-body').attr('data-width', width );
    $boxNexitor.find('.nexitor-body').attr('data-align', align );
    $boxNexitor.find('.nexitor-body').attr('data-link', link );

    var html = "";
    if( width != "none" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-width-'+width+'.svg" class="svg"></div>';
    }
    if( align != "none" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-align-'+align+'.svg" class="svg"></div>';
    }
    if( link != "" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-link.svg" class="svg"></div>';
    }

    $boxNexitor.find('.nexitor-used').html( html );

  }
  else if( element == "video" ){
    var width = $popup.find('.width-choice.active').attr('data-tag');
    var align = $popup.find('.align-choice.active').attr('data-tag');
    var ratio = $popup.find('.ratio-choice.active').attr('data-tag');

    $boxNexitor.find('.nexitor-body').attr('data-width', width );
    $boxNexitor.find('.nexitor-body').attr('data-align', align );
    $boxNexitor.find('.nexitor-body').attr('data-ratio', ratio );

    var html = "";
    if( width != "none" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-width-'+width+'.svg" class="svg"></div>';
    }
    if( align != "none" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-align-'+align+'.svg" class="svg"></div>';
    }
    html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-ratio-'+ratio+'.svg" class="svg"></div>';

    $boxNexitor.find('.nexitor-used').html( html );

  }
  //$boxNexitor.find('input[name="text-title"]').val('Check This');


  generateSVG();
  $this.closest('.section-nexitorPopup').hide();
}
function nexitorClick( $this ){
  if( $this.hasClass('htag-choice') ){
    $('.htag-choice').removeClass('active');
    $this.addClass('active');
  }else if( $this.hasClass('style-choice') ){
    if( $this.hasClass('style-none') ){
      $('.style-choice').removeClass('active');
      $this.addClass('active');
    }else{
      $('.style-none').removeClass('active');
      $this.addClass('active');
    }
  }else if( $this.hasClass('align-choice') ){
    $('.align-choice').removeClass('active');
    $this.addClass('active');
  }else if( $this.hasClass('width-choice') ){
    $('.width-choice').removeClass('active');
    $this.addClass('active');
  }else if( $this.hasClass('ratio-choice') ){
    $('.ratio-choice').removeClass('active');
    $this.addClass('active');
  }
}
function closeContentRow( $this ){
  $this.closest('.content-row').remove();
}
function closeNexitor( $this ){
  $this.closest('.box-nexitor').remove();
}
function sortableRow(){
  $sortable = $( ".sortable-row" ).sortable({
    connectWith: ".sortable-row"
  });
  $sortable.disableSelection();
  $sortable.find(".ui-state-default").one("mouseenter",function(){
        $(this).addClass("sorting-initialize");
        $sortable.sortable('refresh');
    });
}

function sortableElement(){
  $( ".sortable-element" ).sortable({
    connectWith: ".sortable-element",
	create: function( event, ui ) {
		$(this).closest('.content-row').find('.add-element').addClass('hold-space');
		//$(this).closest('.content-row').find('.add-element').prepend('<span style="position:absolute; opacity:0; z-index:-1;">test</span>');
		console.log(1);
	},
	deactivate: function( event, ui ) {
		setTimeout(function () {
			//$('.add-element').removeClass('hold-space');
			//$('.nexitor-delete').remove();
		},0)
	},
  });
  $( ".sortable-element" ).disableSelection();
}


/* ================================================ */
/* SET SVG */
/* ================================================ */
generateSVG();
function generateSVG(){

    $('img.svg').each(function(){
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }
            $svg.attr('data-url', imgURL);
            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
}
