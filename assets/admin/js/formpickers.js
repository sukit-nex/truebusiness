jQuery(document).ready( function($){
  'use strict';
  // if ($("#timepicker-example").length) {
  //   $('#timepicker-example').datetimepicker({
  //     format: 'LT'
  //   });
  // }

  if ($("input[name=published_time]").length) {
    $('input[name=published_time]').datetimepicker({
      format: 'HH:mm'
    });
  }
  
  // if ($(".color-picker").length) {
  //   $('.color-picker').asColorPicker(); 
  // }
  if ($(".color-picker").length) {
    $('.color-picker').asColorPicker({'val':'#000000', mode: 'complex'}); 
  }

  /*if ($("#datepicker-popup").length) {
    $('#datepicker-popup').datepicker({
      enableOnReadonly: true,
      todayHighlight: true,
    });
  }*/
  if ($("#inline-datepicker").length) {
    $('#inline-datepicker').datepicker({
      enableOnReadonly: true,
      todayHighlight: true,
    });
  }
  if ($(".datepicker-autoclose").length) {
    $('.datepicker-autoclose').datepicker({
      autoclose: true
    });
  }
  if ($('input[name="date-range"]').length) {
    $('input[name="date-range"]').daterangepicker();
  }
  if ($('input[name="date-time-range"]').length) {
    $('input[name="date-time-range"]').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY h:mm A'
      }
    });
  }
});
