$( function() {

    $( ".sortable" ).sortable({
      connectWith: '.sortable'
    });
    $( ".sortable" ).disableSelection();
  } );
