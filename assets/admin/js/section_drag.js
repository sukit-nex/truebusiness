sortableSectionItem();
sortableRowItem();
function updateSectionDrag(ar_index, column_id){
	console.log(ar_index );
	var form_data = new FormData();
	form_data.append('ar_index', ar_index );
	form_data.append('column_id', column_id);
	form_data.append('token', $("input[name='token']").val());
	$.ajax({
		data: form_data,
		type: "POST",
		url: base_url + "admin/section/updateDragable",
		cache: false,
		contentType: false,
		processData: false,
		success: function (url) {
			console.log('sort complete');
		}
	});
}
function updateRowDrag(ar_index, page_code) {
	console.log(ar_index);
	var form_data = new FormData();
	form_data.append('ar_index', ar_index);
	form_data.append('page_code', page_code);
	form_data.append('token', $("input[name='token']").val());
	$.ajax({
		data: form_data,
		type: "POST",
		url: base_url + "admin/section/updateRowDragable",
		cache: false,
		contentType: false,
		processData: false,
		success: function (url) {
			console.log('sort complete');
		}
	});
}
function sortableSectionItem() {
	var section = $('.section-move').nestedSortable({
		connectWith: ".section-move",
		forcePlaceholderSize: true,
		items: 'li',
		// handle: 'a',
		placeholder: 'section-highlight',
		listType: 'ul',
		maxLevels: 1,
		opacity: .6,
		stop: function (event, ui) {
			//var place_position = ui.item.closest('.drag-list').attr('data-position');
			//console.log('Place position : ' + place_position);
			var position = ui.item.closest('.drag-list').attr('data-position');
			var column_id = ui.item.closest('.drag-section').attr('data-column');
			console.log('Position : ' + position);
			var ar_index = [];
			ui.item.closest('.section-move').find('.drag-list').each(function (index, value) {
				ar_index.push( $(this).attr('data-code') );
			});
			updateSectionDrag(ar_index, column_id);
		}
	});
	//console.log( section );
}
function sortableRowItem() {
	var section = $('.row-move').nestedSortable({
		forcePlaceholderSize: true,
		connectWith: ".row-move",
		items: 'li.drag-row',
		handle: 'i.mdi-cursor-move',
		placeholder: 'row-highlight',
		listType: 'ul',
		maxLevels: 1,
		opacity: .6,
		stop: function (event, ui) {
			var row_id = ui.item.closest('.drag-row').attr('data-row');
			var page_code = ui.item.closest('.row-move').attr('data-page');
			console.log( row_id );
			//console.log('Place position : ' + place_position);
			//var position = ui.item.closest('.drag-list').attr('data-position');
			//var column_id = ui.item.closest('.drag-section').attr('data-column');
			//console.log('Position : ' + position);
			var ar_index = [];
			ui.item.closest('.row-move').find('.drag-row').each(function (index, value) {
				ar_index.push($(this).attr('data-row'));
			});
			updateRowDrag(ar_index, page_code );
		}
	});
	//console.log( section );
}