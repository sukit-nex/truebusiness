$(document).ready( function(){
  smepack_sortableCateItem();
});

function smepack_activeCate( $this ){
  var code = $this.closest('.drag-list').attr('data-code');
  console.log( 'Active code is '+code );
  var token = $('input[name="token"]').val();
  var onoff = 0;
  if( $this.closest('.drag-list').hasClass('non-active') ){
    onoff = 1;
  }
  $.ajax( base_url+'admin/smepack_category/activeCate', {
    method: "POST",
    data:  {'token' : token ,
            'code' : code,
            'onoff' : onoff
          } ,
    dataType: 'json',
    success: function ( arresult ) {
      if(arresult.rs){
        if( onoff == 1 ){
          $this.closest('.drag-list').removeClass('non-active');
          $this.find('i').removeClass('mdi-eye-off');
          $this.find('i').addClass('mdi-eye');
        }else{
          $this.closest('.drag-list').addClass('non-active');
          $this.find('i').removeClass('mdi-eye');
          $this.find('i').addClass('mdi-eye-off');
        }
      }
    }
  });
}
function smepack_sortableCateItem(){
  $('.smepack-nexcate-move').nestedSortable({
		forcePlaceholderSize: true,
		items: 'li',
		handle: 'a',
		placeholder: 'drag-highlight',
		listType: 'ul',
		maxLevels: 3,
		opacity: .6,
    stop: function( event, ui ) {
        var drag_code = ui.item.closest('.drag-list').attr( 'data-code');
        var current_level = 0;

        console.log( 'drag code : '+drag_code);
        if( typeof( ui.item.parent().parent().closest('.smepack-nexcate-move').attr('data-level') ) !== "undefined" ){
          parent_level = ui.item.parent().parent().closest('.smepack-nexcate-move').attr('data-level');
          current_level = parseInt( parent_level ) + 1;
        }else{
          current_level = 0;
        }
        
        if( current_level == 0 ){
          ui.item.parent().closest('.smepack-nexcate-move').attr('data-level', current_level);
          ui.item.closest('.drag-list').removeClass('level-0');
          ui.item.closest('.drag-list').removeClass('level-1');
          ui.item.closest('.drag-list').removeClass('level-2');
          ui.item.closest('.drag-list').addClass('level-0');
          smepack_updateCateSort( drag_code );
        }else if( current_level == 1 ){
          ui.item.parent().closest('.smepack-nexcate-move').attr('data-level', current_level);
          ui.item.closest('.drag-list').removeClass('level-0');
          ui.item.closest('.drag-list').removeClass('level-1');
          ui.item.closest('.drag-list').removeClass('level-2');
          ui.item.closest('.drag-list').addClass('level-1');
          smepack_updateCateSort( drag_code );
        }else if( current_level == 2 ){
          ui.item.parent().closest('.smepack-nexcate-move').attr('data-level', current_level);
          ui.item.closest('.drag-list').removeClass('level-0');
          ui.item.closest('.drag-list').removeClass('level-1');
          ui.item.closest('.drag-list').removeClass('level-2');
          ui.item.closest('.drag-list').addClass('level-2');
          smepack_updateCateSort( drag_code );
        }

    }
	});
}

function smepack_updateCateSort( drag_code ){
  $('.loader').show();
  var token = $('input[name="token"]').val();

  // $.ajax( base_url+'admin/smepack_category/updateSort', {
  //   method: "POST",
  //   data:  {'token' : token ,
  //           'code' : drag_code ,
  //           'parent_code' : parent_code
  //         } ,
  //   dataType: 'json',
  //   success: function ( arresult ) {
  //     if(arresult.rs){
  //       console.log('complete');
  //     }
  //   }
  // });

  // Auto sorting There
  var sortArray = [];

  var max_level_0 = $('.level-0').length;
  if( max_level_0 > 0 ){
      var i = max_level_0;
      $('.level-0').each( function( index_lv0 ){
        if( i > 0 ){
          var ar_value = {
            'code' : $(this).attr('data-code'),
            'sticky' : i
          }
          sortArray.push( ar_value );
          // Level 1 zone
          var max_level_1 = $(this).find('.level-1').length
          if( max_level_1 > 0 ){
            var x = max_level_1;
            $(this).find('.level-1').each( function( index_lv1 ){
              if( x > 0 ){
              var ar_value = {
                'code' : $(this).attr('data-code'),
                'sticky' : x
              }
              sortArray.push( ar_value );
              // Level 2 zone
              var max_level_2 = $(this).find('.level-2').length
              console.log('Max level 2 is '+max_level_2);
              if( max_level_2 > 0 ){
                var y = max_level_2;
                $(this).find('.level-2').each( function( index_lv2 ){
                if( y > 0 ){
                    var ar_value = {
                      'code' : $(this).attr('data-code'),
                      'sticky' : y
                    }
                    sortArray.push( ar_value );
                    y--;
                  }
                });
              }
              // End level 2
              x--;
            }
            });
            console.log(' has level 1 ');
          }else{
            console.log(' no level 1 ');
          }
          // End level 1
          i--;
        }
      });
      // ajax call after push sorted array
      setTimeout( function(){
        $.ajax( base_url+'admin/smepack_category/updateSortList', {
          method: "POST",
          data:  {'token' : token ,
                  'catelists' : sortArray
                } ,
          dataType: 'json',
          success: function ( arresult ) {
            if(arresult.rs){
              console.log(' updatesortlist complete');
              $('.loader').hide();
            }
          }
        });
      },200);
    }
}


function initRemove() {
  $('.dropify-clear').click(function () {
    var code = $('input[name="cate_code"]').val();
    var token = $('input[name="token"]').val();
    var field_name = $(this).parent().find('input').attr('name');
    $.ajax(base_url + 'admin/smepack_category/removeImg', {
      method: "POST",
      data: {
        'token': token,
        'code': code,
        'field_name': field_name
      },
      dataType: 'json',
      success: function (arresult) {
        if (arresult.rs) {
          console.log('remove ' + field_name + ' of cate ' + code);
        }
      }
    });
  });
}
