$(document).ready( function(){

    $('.listing-approveForm').click( function(){
        swal({
            title: 'คุณแน่ใจใช่ไหม ?',
            //text: "อย่าลืมใส่เหตุผลในการไม่อนุมัตินะคะ",
            icon: 'warning',
            showCancelButton: true,
            // content: {
            //   element: "input",
            //   attributes: {
            //     placeholder: "Type the reason",
            //     type: "text",
            //     class: 'form-control',
            //     name: 'message'
            //   },
            // },
            buttons: {
              cancel: {
                text: "ยกเลิก",
                value: null,
                visible: true,
                className: "btn btn-danger",
                closeModal: true,
              },
              confirm: {
                text: "ยืนยัน",
                value: true,
                visible: true,
                className: "btn btn-primary",
                closeModal: true
              }
            }
          }).then((willDelete ) => {
            if (willDelete){
                $('input[name=is_approve]').val('1');
                setTimeout(
                    function() 
                    {
                        generateHtmlData('save');
                    }, 1000);
            }
          })
    });
});

function approve_cancel($this) {
	var code = $this.attr('data-code');
	swal({
		title: 'คุณแน่ใจใช่ไหม ?',
		//text: "กรุณาใส่เหตุผลในการไม่อนุมัติ",
		icon: 'warning',
		showCancelButton: true,
		//  content: {
		//    element: "input",
		//    attributes: {
		//      placeholder: "Type the reason",
		//      type: "text",
		//      class: 'form-control',
		//      name: 'approve_note'
		//    },
		//  },
		buttons: {
			cancel: {
				text: "ยกเลิก",
				value: null,
				visible: true,
				className: "btn btn-danger",
				closeModal: true,
			},
			confirm: {
				text: "ยืนยัน",
				value: true,
				visible: true,
				className: "btn btn-primary",
				closeModal: true
			}
		}
	}).then((willDelete) => {
		if (willDelete) {
			var formData = new FormData();
			formData.append('code', code);
			formData.append('token', $("input[name='token']").val());
			$.ajax(base_url + 'admin/section/cancelApprove', {
				method: "POST",
				processData: false,
				contentType: false,
				data: formData,
				dataType: 'json',
				success: function (arresult) {
					if (arresult.rs) {
						location.reload();
					} else {
						//
					}
				}
			});
		}
	})
}

function approve_content($this) {
	var code = $this.attr('data-code');
	swal({
		title: 'คุณแน่ใจใช่ไหม ?',
		//text: "กรุณาใส่เหตุผลในการไม่อนุมัติ",
		icon: 'warning',
		showCancelButton: true,
		//  content: {
		//    element: "input",
		//    attributes: {
		//      placeholder: "Type the reason",
		//      type: "text",
		//      class: 'form-control',
		//      name: 'approve_note'
		//    },
		//  },
		buttons: {
			cancel: {
				text: "ยกเลิก",
				value: null,
				visible: true,
				className: "btn btn-danger",
				closeModal: true,
			},
			confirm: {
				text: "ยืนยัน",
				value: true,
				visible: true,
				className: "btn btn-primary",
				closeModal: true
			}
		}
	}).then((willDelete) => {
		if (willDelete) {
			var formData = new FormData();
			formData.append('code', code);
			formData.append('token', $("input[name='token']").val());
			$.ajax(base_url + 'admin/section/approveContent', {
				method: "POST",
				processData: false,
				contentType: false,
				data: formData,
				dataType: 'json',
				success: function (arresult) {
					if (arresult.rs) {
						location.reload();
					} else {
						//
					}
				}
			});
		}
	})

	// function approve_preview($this) {
	// 	var url = $this.attr('data-url');
	// 	var code = $this.attr('data-code');
		
	// 	var formData = new FormData();
	// 		formData.append('code', code);
	// 		formData.append('token', $("input[name='token']").val());
	// 		$.ajax(base_url + 'admin/section/approve_Preview', {
	// 			method: "POST",
	// 			processData: false,
	// 			contentType: false,
	// 			data: formData,
	// 			dataType: 'json',
	// 			success: function (arresult) {
	// 				if (arresult.rs) {
	// 					window.open( url + arresult.preview_id, '_blank');
	// 					//location.reload();
	// 				} else {
	// 					//
	// 				}
	// 			}
	// 		});
	// }
}