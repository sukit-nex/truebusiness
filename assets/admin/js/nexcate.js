$(document).ready( function(){
  sortableCateItem();

  $('.update-cate').click( function(){
    update_cate('update-cate-form');
  });
});
function editCate( $this ){
  var code = $this.closest('.drag-list').attr('data-code');
  console.log( 'Edit code is '+code );
  var token = $('input[name="token"]').val();
  $.ajax( base_url+'admin/category/edit', {
    method: "POST",
    data:  {'token' : token ,
            'code' : code
          } ,
    dataType: 'json',
    success: function ( arresult ) {
      if(arresult.rs){
        console.log( arresult.details );
        $.each( arresult.details, function( key, value ) {
          console.log( key + ": " + value );
          $('input[name="cate_code"]').val( value.code );
          $('input[name="cate_for"]').val( value.cate_for );
          $('input[name="parent_code"]').val( value.parent_code );
          $('input[name="cate_color"]').val(value.cate_color);
          if ($(".color-picker").length) {
            $('.color-picker').asColorPicker('val',value.cate_color);
          }
          //$('input.asColorPicker-hex').val('#9ac5ee');
          $('select[name="brand_code"]').val( value.brand_code );

          $('.lang_'+key).find('.nameInput').val( value.name );

          $('.lang_' + key).find('.subTitleInput').val(value.sub_title);
          $('.lang_' + key).find('.shadowInput').val(value.shadow_text);
          $('.lang_' + key).find('.navTitleInput').val(value.nav_title);
          $('.lang_' + key).find('.navSubTitleInput').val(value.nav_sub_title);
          if (value.thumb != "" && value.thumb != null) {
            $('input[name="thumb"]').attr('data-default-file', upload_path + '' + value.thumb);
            $('.thumb-image .dropify-wrapper').addClass('has-preview');
            $('.thumb-image .dropify-render img').attr('src', upload_path + '' + value.thumb);
            $('.thumb-image .dropify-render').html('<img src="' + upload_path + '' + value.thumb + '" />');
            $('.thumb-image .dropify-filename-inner').html(value.thumb);
            $('.thumb-image .dropify-preview').show();
          } else {
            var drEvent = $('.thumb-image .dropify').dropify();
            if ($('.thumb-image .dropify').length > 0) {
              drEvent = drEvent.data('dropify');
              drEvent.resetPreview();
              drEvent.clearElement();
            }
          }
          if (value.thumb_hover != "" && value.thumb_hover != null) {
            $('input[name="thumb_hover"]').attr('data-default-file', upload_path + '' + value.thumb_hover);
            $('.thumb_hover-image .dropify-wrapper').addClass('has-preview');
            $('.thumb_hover-image .dropify-render img').attr('src', upload_path + '' + value.thumb_hover);
            $('.thumb_hover-image .dropify-render').html('<img src="' + upload_path + '' + value.thumb_hover + '" />');
            $('.thumb_hover-image .dropify-filename-inner').html(value.thumb_hover);
            $('.thumb_hover-image .dropify-preview').show();
          } else {
            var drEvent = $('.thumb_hover-image .dropify').dropify();
            if ($('.thumb_hover-image .dropify').length > 0) {
              drEvent = drEvent.data('dropify');
              drEvent.resetPreview();
              drEvent.clearElement();
            }
          }
          if( value.image != "" && value.image != null ){
            $('input[name="image"]').attr('data-default-file', upload_path+''+value.image );
            $('.desktop-image .dropify-wrapper').addClass('has-preview');
            $('.desktop-image .dropify-render img').attr('src', upload_path+''+value.image );
            $('.desktop-image .dropify-render').html('<img src="'+upload_path+''+value.image+'" />');
            $('.desktop-image .dropify-filename-inner').html( value.image );
            $('.desktop-image .dropify-preview').show();
          } else {
            var drEvent = $('.desktop-image .dropify').dropify();
            if ($('.desktop-image .dropify').length > 0 ){
              drEvent = drEvent.data('dropify');
              drEvent.resetPreview();
              drEvent.clearElement();
            }
          }
          if( value.image_m != "" && value.image_m != null ){
            $('input[name="image_m"]').attr('data-default-file', upload_path+''+value.image_m );
            $('.mobile-image .dropify-wrapper').addClass('has-preview');
            $('.mobile-image .dropify-render img').attr('src', upload_path+''+value.image_m );
            $('.mobile-image .dropify-render').html('<img src="'+upload_path+''+value.image_m+'" />');
            $('.mobile-image .dropify-filename-inner').html( value.image_m );
            $('.mobile-image .dropify-preview').show();
          } else {
            var drEventm = $('.mobile-image .dropify').dropify();
            if ($('.mobile-image .dropify').length > 0) {
              drEventm = drEventm.data('dropify');
              drEventm.resetPreview();
              drEventm.clearElement();
            }
          }
          if (value.image_navi != "" && value.image_navi != null) {
            $('input[name="image_navi"]').attr('data-default-file', upload_path + '' + value.image_navi);
            $('.navi-image .dropify-wrapper').addClass('has-preview');
            $('.navi-image .dropify-render img').attr('src', upload_path + '' + value.image_navi);
            $('.navi-image .dropify-render').html('<img src="' + upload_path + '' + value.image_navi + '" />');
            $('.navi-image .dropify-filename-inner').html(value.image_navi);
            $('.navi-image .dropify-preview').show();
          } else {
            var drEventna = $('.navi-image .dropify').dropify();
            if ($('.navi-image .dropify').length > 0) {
              drEventna = drEventna.data('dropify');
              drEventna.resetPreview();
              drEventna.clearElement();
            }
          }
          initRemove();
          $('input[name="image_alt"]').val( value.image_alt );
        });

      }
    }
  });
}
function activeCate( $this ){
  var code = $this.closest('.drag-list').attr('data-code');
  console.log( 'Active code is '+code );
  var token = $('input[name="token"]').val();
  var onoff = 0;
  if( $this.closest('.drag-list').hasClass('non-active') ){
    onoff = 1;
  }
  $.ajax( base_url+'admin/category/activeCate', {
    method: "POST",
    data:  {'token' : token ,
            'code' : code,
            'onoff' : onoff
          } ,
    dataType: 'json',
    success: function ( arresult ) {
      if(arresult.rs){
        if( onoff == 1 ){
          $this.closest('.drag-list').removeClass('non-active');
          $this.find('i').removeClass('mdi-eye-off');
          $this.find('i').addClass('mdi-eye');
        }else{
          $this.closest('.drag-list').addClass('non-active');
          $this.find('i').removeClass('mdi-eye');
          $this.find('i').addClass('mdi-eye-off');
        }
      }
    }
  });
}
function sortableCateItem(){
  $('.nexcate-move').nestedSortable({
		forcePlaceholderSize: true,
		items: 'li',
		handle: 'a',
		placeholder: 'drag-highlight',
		listType: 'ul',
		maxLevels: 3,
		opacity: .6,
    stop: function( event, ui ) {

        var drag_code = ui.item.closest('.drag-list').attr( 'data-code');
        var parent_code = ui.item.closest('.drag-list').parent().parent().attr( 'data-code') ;

        var parent_level = 0;
        var current_level = 0;
        console.log( 'drag code : '+drag_code);
        console.log( 'parent code : '+parent_code);
        if( typeof( ui.item.parent().parent().closest('.nexcate-move').attr('data-level') ) !== "undefined" ){
          parent_level = ui.item.parent().parent().closest('.nexcate-move').attr('data-level');
          current_level = parseInt( parent_level ) + 1;
        }else{
          current_level = 0;
        }
        if( current_level == 0 ){
          ui.item.parent().closest('.nexcate-move').attr('data-level', current_level);
          ui.item.closest('.drag-list').removeClass('level-0');
          ui.item.closest('.drag-list').removeClass('level-1');
          ui.item.closest('.drag-list').removeClass('level-2');
          ui.item.closest('.drag-list').addClass('level-0');
          updateCateSort( drag_code, parent_code );
        }else if( current_level == 1 ){
          ui.item.parent().closest('.nexcate-move').attr('data-level', current_level);
          ui.item.closest('.drag-list').removeClass('level-0');
          ui.item.closest('.drag-list').removeClass('level-1');
          ui.item.closest('.drag-list').removeClass('level-2');
          ui.item.closest('.drag-list').addClass('level-1');
          updateCateSort( drag_code, parent_code );
        }else if( current_level == 2 ){
          ui.item.parent().closest('.nexcate-move').attr('data-level', current_level);
          ui.item.closest('.drag-list').removeClass('level-0');
          ui.item.closest('.drag-list').removeClass('level-1');
          ui.item.closest('.drag-list').removeClass('level-2');
          ui.item.closest('.drag-list').addClass('level-2');
          updateCateSort( drag_code, parent_code );
        }

    }
	});
}

function updateCateSort( drag_code, parent_code ){
  $('.loader').show();
  var token = $('input[name="token"]').val();
  $.ajax( base_url+'admin/category/updateSort', {
    method: "POST",
    data:  {'token' : token ,
            'code' : drag_code ,
            'parent_code' : parent_code
          } ,
    dataType: 'json',
    success: function ( arresult ) {
      if(arresult.rs){
        console.log('complete');
      }
    }
  });
  // Auto sorting There
  var sortArray = [];

  var max_level_0 = $('.level-0').length;
  if( max_level_0 > 0 ){
      var i = max_level_0;
      $('.level-0').each( function( index_lv0 ){
        if( i > 0 ){
          var ar_value = {
            'code' : $(this).attr('data-code'),
            'sticky' : i
          }
          sortArray.push( ar_value );
          // Level 1 zone
          var max_level_1 = $(this).find('.level-1').length
          if( max_level_1 > 0 ){
            var x = max_level_1;
            $(this).find('.level-1').each( function( index_lv1 ){
              if( x > 0 ){
              var ar_value = {
                'code' : $(this).attr('data-code'),
                'sticky' : x
              }
              sortArray.push( ar_value );
              // Level 2 zone
              var max_level_2 = $(this).find('.level-2').length
              console.log('Max level 2 is '+max_level_2);
              if( max_level_2 > 0 ){
                var y = max_level_2;
                $(this).find('.level-2').each( function( index_lv2 ){
                if( y > 0 ){
                    var ar_value = {
                      'code' : $(this).attr('data-code'),
                      'sticky' : y
                    }
                    sortArray.push( ar_value );
                    y--;
                  }
                });
              }
              // End level 2
              x--;
            }
            });
            console.log(' has level 1 ');
          }else{
            console.log(' no level 1 ');
          }
          // End level 1
          i--;
        }
      });
      // ajax call after push sorted array
      setTimeout( function(){
        $.ajax( base_url+'admin/category/updateSortList', {
          method: "POST",
          data:  {'token' : token ,
                  'catelists' : sortArray
                } ,
          dataType: 'json',
          success: function ( arresult ) {
            if(arresult.rs){
              console.log(' updatesortlist complete');
              $('.loader').hide();
            }
          }
        });
      },200);
    }
}

function update_cate( form_class ){
  $('.loader').show();
  var form = $('.'+form_class )[0];
  var formData = new FormData(form);
  event.preventDefault();
  $.ajax( base_url+'admin/category/updateCate', {
    method: "POST",
    processData: false,
    contentType: false,
    data:  formData ,
    dataType: 'json',
    success: function ( arresult ) {
      console.log( arresult.rs );
      if(arresult.rs){
        $('.loader').hide();
        window.location.href = window.location.href;
      }
    }
  });
}
function initRemove() {
  $('.dropify-clear').click(function () {
    var code = $('input[name="cate_code"]').val();
    var token = $('input[name="token"]').val();
    var field_name = $(this).parent().find('input').attr('name');
    $.ajax(base_url + 'admin/category/removeImg', {
      method: "POST",
      data: {
        'token': token,
        'code': code,
        'field_name': field_name
      },
      dataType: 'json',
      success: function (arresult) {
        if (arresult.rs) {
          console.log('remove ' + field_name + ' of cate ' + code);
        }
      }
    });
  });
}
