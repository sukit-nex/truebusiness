$(document).on('click','.dropify-clear', function(){
  $(this).closest('.image-box').find('input.image_as_text').val('');
});
function initCheckBoxCount( $element , count ){
  $element.attr('disabled', false);
  var checked = 0;
  $element.each( function( index, value ){
    if( $(this).is(':checked') ){
      checked++;
    }
  });
  $element.each(function (index, value) {
    if ( !$(this).is(':checked')) {
      if (checked == count) {
        $(this).attr('disabled', true);
      }
    }
  });
}
function popup_message( type , message, title ){
	swal({
		type: type,
    icon: type,
		title: title,
		text: message,
		button: {
      text: "OK",
      value: true,
      visible: true,
      className: "btn btn-gradient-dark btn-fw-lg"
    }
	});
}
function sectionToggle( $this ){
  if ($this.is(':checked')) {
    $this.closest('.box-switch').find('.content-switch').slideDown();
    $this.closest('.box-switch').addClass('active');
  } else {
    $this.closest('.box-switch').find('.content-switch').slideUp();
    $this.closest('.box-switch').removeClass('active');
  }
}
function initRelateProduct(){
  $('input[name="product_cate_relate[]"]').each( function(index, value){
    var cate_code = $(this).val() ;
    if ($(this).is(':checked')) {
      $('input[name="product_relate_code[]"]').each(function (index, value) {
        var ar_data_cate = $(this).attr('data-cate');
        if (ar_data_cate == cate_code){
          $(this).closest('.product-relate-wrap').show();
          $(this).attr('disabled', false);
        }
      });
    }else{
      $('input[name="product_relate_code[]"]').each(function (index, value) {
        var ar_data_cate = $(this).attr('data-cate');
        if (ar_data_cate == cate_code) {
          $(this).closest('.product-relate-wrap').hide();
          $(this).attr('disabled', true);
        }
      });
    }
  });
}

function changeProductRelateSort($this) {
  var product_code = $this.val();
  var product_title = $this.attr('data-title');
  var product_sub_title = $this.attr('data-sub-title');
  var product_image = $this.parent().find('img').attr('src');
  var onoff = $this.attr('data-onoff');
  var textopa = '';
  if (onoff == 0) {
    textopa = '0.3';
  }
  if ($this.is(':checked')) {
    var html = "";
    html += '<div class="card-image-choose product-relate-choose" style="opacity:' + textopa + '">';
    html += '  <div class="card-image-choose-img"><img style="width:30px;" src="' + product_image + '" ></div>';
    html += '  <div class="card-image-choose-txt">' + product_title + '<br>' + product_sub_title + '</div>';
    html += '  <div class="card-image-choose-icon" onclick="removeRelate($(this))"><img src="' + real_path + 'assets/admin/images/nexitor/icon-cancel.svg"></div>';
    html += '  <input type="hidden" name="product_relate_code_sort[]" value="' + product_code + '" />';
    html += '</div>';
    $('.product-sort-warp').append(html);
  } else {
    $('.product-relate-choose').each(function (index, value) {
      if ($(this).find('input').val() == product_code) {
        $(this).remove();
      }
    });
  }

}
function removeRelate($this) {
  var product_code = $this.parent().find('input').val();
  $('input[name="product_relate_code[]"]').each(function (index, value) {
    var code = $(this).val();
    if (product_code == code) {
      $(this).prop('checked', false);
    }
  });
  $this.parent().remove();
}
function initProductRelateSort() {
  $('.product-sort-warp').sortable();
}
var editor = '';
$(document).ready( function(){
  if ($('#ace_css').length) {
    $(function () {
      editor = ace.edit("ace_css");
      editor.setTheme("ace/theme/monokai");
      editor.getSession().setMode("ace/mode/css");
      document.getElementById('ace_css');
    });
  }
  if ($(".js-example-basic-multiple").length) {
    $(".js-example-basic-multiple").select2();
  }
  ////////////////////////////////////////////////////
  //=================  Contact Setting
  ////////////////////////////////////////////////////
  $('input[name="contact_status_switch"]').on('change', function () {
    if ($(this).is(':checked')) {
      $('.contact-toggle-wrap').slideDown();
      $('input[name="contact_status"]').val('1');
    } else {
      $('.contact-toggle-wrap').slideUp();
      $('input[name="contact_status"]').val('0');
    }
  });

  $('.dropify-clear').click( function(){
    console.log('remove image');
    $(this).closest('.image-wrap').find('input.image_text').val('');
  });
  $(".summernote-only").summernote({
    toolbar: [
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['fontsize', ['height', 'color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['insert', ['link', 'table', 'hr']],
      ['misc', ['']],
      ['view', ['codeview']],
    ],
    height: 100,
    callbacks: {
      onImageUpload: function (files, editor, welEditable) {
        for (var i = files.length - 1; i >= 0; i--) {
          sendFile(files[i], this);
        }
      },
      onPaste: function (e) {
        if (document.queryCommandSupported("insertText")) {
          var text = $(e.currentTarget).html();
          var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

          setTimeout(function () {
            document.execCommand('insertText', false, bufferText);
          }, 10);
          e.preventDefault();
        } else { //IE
          var text = window.clipboardData.getData("text")
          if (trap) {
            trap = false;
          } else {
            trap = true;
            setTimeout(function () {
              document.execCommand('paste', false, text);
            }, 10);
            e.preventDefault();
          }
        }

      }
    }
  });

//============== Product property check =================
  $('.prop-check').on( 'change', function(){
    console.log('1');
    $(this).closest('.box-check-gray').find('.box-show').toggle();
  });
// ============= Check box for product =================== 
initCheckBoxCount( $('input[name="top_property[]"]') , 3 );
initCheckBoxCount( $('input[name="bottom_property[]"]'), 4 );

$('input[name="top_property[]"]').on('change', function(){
  initCheckBoxCount( $('input[name="top_property[]"]'), 3 );
});
$('input[name="bottom_property[]"]').on('change', function () {
  initCheckBoxCount( $('input[name="bottom_property[]"]'), 4 );
});
// ========================================================

  //============= Check box for product relate ==============
  initRelateProduct();
  initProductRelateSort();
  $('input[name="product_cate_relate[]"]').on('change', function () {
    initRelateProduct();
  });
  $('input[name="product_relate_code[]"]').on('change', function () {
    changeProductRelateSort($(this));
  });

//=========================================================

(function($) {
  'use strict';
  $(function() {
    /* Code for attribute data-custom-class for adding custom class to tooltip */
    if (typeof $.fn.popover.Constructor === 'undefined') {
      throw new Error('Bootstrap Popover must be included first!');
    }

    var Popover = $.fn.popover.Constructor;

    // add customClass option to Bootstrap Tooltip
    $.extend(Popover.Default, {
      customClass: ''
    });

    var _show = Popover.prototype.show;

    Popover.prototype.show = function() {

      // invoke parent method
      _show.apply(this, Array.prototype.slice.apply(arguments));

      if (this.config.customClass) {
        var tip = this.getTipElement();
        $(tip).addClass(this.config.customClass);
      }

    };

    $('[data-toggle="popover"]').popover()
  });
})(jQuery);
(function($) {
  'use strict';

  $(function() {
    /* Code for attribute data-custom-class for adding custom class to tooltip */
    if (typeof $.fn.tooltip.Constructor === 'undefined') {
      throw new Error('Bootstrap Tooltip must be included first!');
    }

    var Tooltip = $.fn.tooltip.Constructor;

    // add customClass option to Bootstrap Tooltip
    $.extend(Tooltip.Default, {
      customClass: ''
    });

    var _show = Tooltip.prototype.show;

    Tooltip.prototype.show = function() {

      // invoke parent method
      _show.apply(this, Array.prototype.slice.apply(arguments));

      if (this.config.customClass) {
        var tip = this.getTipElement();
        $(tip).addClass(this.config.customClass);
      }

    };
    $('[data-toggle="tooltip"]').tooltip();

  });
})(jQuery);
  ////////////////////////////////////////////////////
	//=================  SELECT 2
	////////////////////////////////////////////////////
  if ($(".js-example-basic-single").length) {
    $(".js-example-basic-single").select2();
  }
  ////////////////////////////////////////////////////
	//=================  Side Menu
	////////////////////////////////////////////////////
  $('.collapse').each( function(){
    if( $(this).find('.nav-link').hasClass('active') ){
      $(this).addClass('show');
      $(this).closest('.nav-item').addClass('active');
    }
  });

  ////////////////////////////////////////////////////
  //=================  SEO Setting
  ////////////////////////////////////////////////////
  $('input[name="seo_status_switch"]').on('change', function(){
    if( $(this).is(':checked') ){
      $('.seo-toggle-wrap').slideDown();
      $('input[name="seo_status"]').val('1');
    }else{
      $('.seo-toggle-wrap').slideUp();
      $('input[name="seo_status"]').val('0');
    }
  });


  $('.clear-dropzone').click( function(){
    $('.dz-image-preview').remove();
    $('.dz-message').show();
  });


  // Message view
  $('.table_viewmore').click(function(){
  	var val_table = $(this).find('span').html();
  	$('.modal-body .showmessage').html(val_table);
  });

// Init Datepicker
if ($(".datepicker").length) {
  $('.datepicker').datepicker({
    format:'yyyy-mm-dd',
    language:'th-th',
    enableOnReadonly: true,
    todayHighlight: true
  });
}
if ($(".datepicker_en").length) {
  $('.datepicker_en').datepicker({
    format:'yyyy-mm-dd',
    language:'th-en',
    enableOnReadonly: true,
    todayHighlight: true
  });
}
  //Summernote init
  if ($(".summernote").length) {
    $(".summernote").summernote({
      height: 200,
      callbacks:{
        onImageUpload : function( files, editor, welEditable ){
          for( var i = files.length - 1; i >= 0; i-- ){
            sendFile(files[i], this);
          }
        }
      }
    });
  }
  // Light gallery init
  if ($(".lightgallery").length) {
    $(".lightgallery").lightGallery();
  }

  // Dropzone show up
  $("select[name='select-cate']").on('change', function(){
    var cate_code = $("select[name='select-cate'] option:selected").val();
    if( cate_code != "" ){
      $("input[name='cate_code']").val( cate_code );
      $('.block_upload').fadeOut();
    }else{
      $("input[name='cate_code']").val( "" );
      $('.block_upload').show();
    }
  });

  // Delete item
  initDelete();
  /*$('.page-link').click( function(){
    initDelete();
  });*/


  $("#signupForm").validate({
    rules: {
      firstname: "required",
      lastname: "required",
      username: {
        required: true,
        minlength: 2
      },
      password: {
        required: true,
        minlength: 5
      },
      confirm_password: {
        required: true,
        minlength: 5,
        equalTo: "#password"
      },
      agree: "required"
    },
    messages: {
      firstname: "Please enter your firstname",
      lastname: "Please enter your lastname",
      username: {
        required: "Please enter a username",
        minlength: "Your username must consist of at least 2 characters"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      confirm_password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long",
        equalTo: "Please enter the same password as above"
      },
      email: "Please enter a valid email address",
      agree: "Please accept our policy",
      topic: "Please select at least 2 topics"
    },
    errorPlacement: function(label, element) {
      label.addClass('mt-2 text-danger');
      label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
      $(element).parent().addClass('has-danger')
      $(element).addClass('form-control-danger')
    }
  });

  

});

function sendFile( file, el){
  var form_data = new FormData();
  form_data.append('file',file);
	form_data.append('token',$("input[name='token']").val());
  $.ajax({
    data: form_data,
    type: "POST",
    url: base_url+"admin/upload/do_upload",
    cache: false,
    contentType: false,
    processData: false,
    success: function( url ){
			console.log( url );
			console.log( el );
      $(el).summernote('insertImage', url);
    }
  });
}

function removeCateFromSection( $this ){

    swal({
    title: 'Are you to remove this item ?',
    text: "This categry will remove only in this section",
    icon: 'warning',
    showCancelButton: true,
    buttons: {
        cancel: {
        text: "Cancel",
        value: null,
        visible: true,
        className: "btn btn-danger",
        closeModal: true,
        },
        confirm: {
        text: "Confirm",
        value: true,
        visible: true,
        className: "btn btn-primary",
        closeModal: true
        }
    }
    }).then((willDelete) => {
        if (willDelete) {
        var cate_code = $this.attr('data-code');
        var sec_code = $this.attr('data-sec-code');
        var token = $('input[name="token"]').val();
        $.ajax(base_url + 'admin/section/removeCateFromSection', {
            method: "POST",
            data: {
            'token': token,
            'cate_code': cate_code,
            'sec_code': sec_code,
            },
            dataType: 'json',
            success: function (arresult) {
            if (arresult.rs) {
                location.reload();
                // onoff complete
            }
            }
        });
        }
    })
}

function smepack_deleteItem ( $this ) {

    var pkg = $this.parents('.list-group-item').attr('data-pkg');

    $('#smepack_deleteModal').modal('show');

    $('.smepack-modal-delete-confirm').attr('data-pkg', pkg);
}

function deleteItem( $this ){
    var delete_by = $this.attr('data-delete-by');
    var table = $this.attr('data-table');
    var id = $this.attr('data-code');
    var delete_type = $this.attr('data-delete-type');
    //console.log(delete_by +' '+table+' '+id );
    $('.modal-delete-confirm').attr('data-delete-by', delete_by);
    $('.modal-delete-confirm').attr('data-table', table);
    $('.modal-delete-confirm').attr('data-code', id);
    $('.modal-delete-confirm').attr('data-delete-type', delete_type );
  if( delete_type == "cate"){
    $('.cate-message').show();
  }
  $('#deleteModal').modal('show');
}

function onoff( $this ) {
  var onoff = 0;
  if ($this.is(':checked') ) {
    onoff = 1;
  }
  var table = $this.attr('data-table');
  var code = $this.attr('data-code');
  var token = $('input[name="token"]').val();
  $.ajax(base_url + 'admin/dashboard/onoff', {
    method: "POST",
    data: {
      'token': token,
      'code': code,
      'table': table,
      'onoff': onoff
    },
    dataType: 'json',
    success: function (arresult) {
      if (arresult.rs) {
        // onoff complete
      }
    }
  });

}

function initDelete(){

  $('.modal-delete-confirm').click( function(){
    var delete_by = $(this).attr('data-delete-by');
    var table = $(this).attr('data-table');
    var id = $(this).attr('data-code');
    var token = $('input[name="token"]').val();
		var delete_type = "";
    delete_type = $(this).attr('data-delete-type');
    //console.log( delete_type );
    $.ajax( base_url+'admin/dashboard/deleteItem', {
      method: "POST",
      data:  {'token' : token ,
              'delete_by' : delete_by ,
              'table' : table,
              'id' : id
            } ,
      dataType: 'json',
      success: function ( arresult ) {
        if(arresult.rs){
         if( typeof(delete_type) == "undefined"){
            $('tr').each( function(){
              if( $(this).find('.delete-item').attr('data-table') == table && $(this).find('.delete-item').attr('data-code') == id ){
                $(this).remove();
              }
            });
          }
          if( delete_type == "cate" ){
            $('.drag-list').each( function(){
              if( $(this).attr('data-code') == id ){
                $(this).remove();
              }
            });
          }
          if( delete_type == "menu" ){
            $('.drag-list').each( function(){
              if( $(this).attr('data-code') == id ){
                $(this).remove();
              }
            });
          }
          if (delete_type == "row") {
            $('.drag-row').each(function () {
              if ($(this).attr('data-row') == id) {
                $(this).remove();
              }
            });
          }
          if (delete_type == "section") {
            $('.drag-list').each(function () {
              if ($(this).attr('data-code') == id) {
                $(this).remove();
              }
            });
          }
        }
      }
    });
  });
}
function initDelete(){

  $('.modal-delete-confirm').click( function(){
    var delete_by = $(this).attr('data-delete-by');
    var table = $(this).attr('data-table');
    var id = $(this).attr('data-code');
    var token = $('input[name="token"]').val();
		var delete_type = "";
    delete_type = $(this).attr('data-delete-type');
    //console.log( delete_type );
    $.ajax( base_url+'admin/dashboard/deleteItem', {
      method: "POST",
      data:  {'token' : token ,
              'delete_by' : delete_by ,
              'table' : table,
              'id' : id
            } ,
      dataType: 'json',
      success: function ( arresult ) {
        if(arresult.rs){
         if( typeof(delete_type) == "undefined"){
            $('tr').each( function(){
              if( $(this).find('.delete-item').attr('data-table') == table && $(this).find('.delete-item').attr('data-code') == id ){
                $(this).remove();
              }
            });
          }
          if( delete_type == "cate" ){
            $('.drag-list').each( function(){
              if( $(this).attr('data-code') == id ){
                $(this).remove();
              }
            });
          }
          if( delete_type == "menu" ){
            $('.drag-list').each( function(){
              if( $(this).attr('data-code') == id ){
                $(this).remove();
              }
            });
          }
          if (delete_type == "row") {
            $('.drag-row').each(function () {
              if ($(this).attr('data-row') == id) {
                $(this).remove();
              }
            });
          }
          if (delete_type == "section") {
            $('.drag-list').each(function () {
              if ($(this).attr('data-code') == id) {
                $(this).remove();
              }
            });
          }
        }
      }
    });
  });

  $('.smepack-modal-delete-confirm').click( function(){
    var pkg = '';
    pkg = $(this).attr('data-pkg');
    //console.log(pkg);
    $('.card-item-'+pkg).remove();
  });
}

////////////////////////////////////////////////////
//=================  Pages Type
////////////////////////////////////////////////////
function loadCateFor( $this, parent_code, level = 1 ){

    var token = $('input[name="token"]').val();
    var cate_for = $this.attr('data-cate-for');
    var cate_code = parent_code;
    $('input[name="page_type"]').val( cate_for );
    $('input[name="cate_code"]').val( cate_code );

  if( $this.find('input[type="radio"]').is(':checked') && level <= 3 ){
    $('.loader').show();
    if( cate_for != "" ){
      $.ajax( base_url+'admin/category/lists_ajax', {
        method: "POST",
        data:  {'token' : token, 'cate_for' : cate_for, 'parent_code' : parent_code } ,
        dataType: 'json',
        success: function ( arresult ) {
          if(arresult.rs){
            if( level == 2 ){
              $('.box-cate-3').html('');
            }
            if( level == 1 ){
              $('.box-cate-1').html('');
              $('.box-cate-2').html('');
              $('.box-cate-3').html('');
            }

            var nextLevel = level+1;
            //if( parent_code == 0 ){
              var html = "";
              $.each( arresult.lists , function( index, value ){
                html += '<div class="box-radio box-check-white" onclick="loadCateFor( $(this), '+value.code+', '+nextLevel+'  );" data-cate-for="'+cate_for+'">';
                html += '  <div class="form-check form-check-dark">';
                html += '    <label class="form-check-label">';
                html += '      <input type="radio" class="form-check-input" name="cate_code_'+level+'" value="'+value.code+'">';
                html += '      '+value.name+'';
                html += '      <i class="input-helper"></i>';
                html += '    </label>';
                html += '  </div>';
                html += '</div>';
              });
              $('.box-cate-'+level).html( html );
            //}
            console.log('level : '+level);
            $('.loader').hide();
          }
        }
      });
    }else{
      // page default
      $('.box-cate-1').html('');
      $('.box-cate-2').html('');
      $('.box-cate-3').html('');
      $('.loader').hide();
    }
  }
}

////////////////////////////////////////////////////
//=================  Gallery
////////////////////////////////////////////////////
function deleteImage( $this ){
  var id = $this.attr('data-id');
  var token = $('input[name="token"]').val();
  $this.closest('.img-wrap').fadeOut( "slow", function() {
    // Animation complete.
  });

  $.ajax( base_url+'admin/gallery/deleteImage', {
    method: "POST",
    data:  {'token' : token ,
            'id' : id
          } ,
    dataType: 'json',
    success: function ( arresult ) {
      if(arresult.rs){
        $this.closest('.img-wrap').remove();
      }
    }
  });

  //$this.closest('.img-wrap').remove();

}

function initToggle(){
  $('input[name="box_switch"]').bootstrapToggle('destroy')
  $('input[name="box_switch"]').bootstrapToggle()
} 


$(document).ready( function(){
  $('.listing-previewForm, .section-previewForm').click( function(){
    $('.loader').fadeIn();
    $('input[name=is_preview]').val('1');
    setTimeout(
      function() 
      {
        generateHtmlData('save');
      }, 1000);
  });

  $('.listingForm').submit(function(){
    var is_preview = $('input[name=is_preview]').val();
    if ( is_preview != 0 ) {
      var form_data = new FormData($(this)[0]);

      // $.each($("input[type='file']")[0].files, function(i, file) {
      //   form_data.append('file', file);
      // });

      $.ajax({
        data: form_data,
        type: "POST",
        url: base_url+"admin/section/previewListingForm",
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function( arresult ){
          if ( arresult.rs ) {
            //reset
            $('.loader').fadeOut();
            $('input[name=is_preview]').val('0');
            window.open( arresult.url, '_blank');
          }
        }
      });

      return false;
    } else {
      return true;
    }
  });

  $('.sectionDetailForm').submit(function(){
    if( editor != '' ){
      var css_code = editor.getValue();
      $('input[name="custom_css"]').val( css_code );
    }
    var is_preview = $('input[name=is_preview]').val();
    if ( is_preview != 0 ) {
      var form_data = new FormData($(this)[0]);


      $.ajax({
        data: form_data,
        type: "POST",
        url: base_url+"admin/section/previewSectionDetailForm",
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function( arresult ){
          if ( arresult.rs ) {
            //reset
            $('.loader').fadeOut();
            $('input[name=is_preview]').val('0');
            window.open( arresult.url, '_blank');
          }
        }
      });

      return false;
    } else {
      return true;
    }
  });

});

function roleCateRemove( $this ){
  var parent = $this.closest('.role-cate-list');
  swal({
    title: 'Are you to remove this item ?',
    //text: "This categry will remove only in this section",
    icon: 'warning',
    showCancelButton: true,
    buttons: {
      cancel: {
        text: "Cancel",
        value: null,
        visible: true,
        className: "btn btn-danger",
        closeModal: true,
      },
      confirm: {
        text: "Confirm",
        value: true,
        visible: true,
        className: "btn btn-primary",
        closeModal: true
      }
    }
  }).then((willDelete) => {
    if (willDelete) {
      var cate_code = $this.attr('data-cate-code');
      var role_code = $this.attr('data-role-code');
      var token = $('input[name="token"]').val();
      parent.remove();
      $.ajax(base_url + 'admin/permission/removeRoleCate', {
        method: "POST",
        data: {
          'token': token,
          'cate_code': cate_code,
          'role_code': role_code,
        },
        dataType: 'json',
        success: function (arresult) {
          if (arresult.rs) {
            parent.remove();
            popup_message('success', 'Category has been removed .', 'Success !');
            //location.reload();
            // onoff complete
          }
        }
      });
    }
  })
}

$(document).ready(function(){
    //activaTab('aaa');
    //packageSortable();
    
});

function packageSortable(){
    //$('#package-sortable').sortable();

    $('#package-sortable').sortable({ animation: 150, containment: "parent", cursor: "move", scroll: false, axis: "y", start: function(e, ui) { $(ui.item).find('textarea').each(function() { tinymce.execCommand('mceRemoveEditor', false, $(this).attr('id')); }); }, stop: function(e, ui) { $(ui.item).find('textarea').each(function() { tinymce.execCommand('mceAddEditor', true, $(this).attr('id')); }); } });
    
}

function activaTabNex(tab){
    $('.head-'+tab).tab('show');

    $('.tab-content-lang .tab-pane').removeClass('show active');
    $('.tab-content-lang .tab-'+tab).addClass('show active');
}

function feature_01() {

    $('.feature_01_manual_content').hide();

    $('#feature_01_template').click(function(){
        $('.feature_01_template_content').show();
        $('.feature_01_manual_content').hide();
    });

    $('#feature_01_template').click(function(){
        $('.feature_01_template_content').show();
        $('.feature_01_manual_content').hide();
    });
}

$(document).ready(function () {
  
    $('.smepack-linktype input[type=radio]').click(function () {
        if ( $(this).val() == 'no_link' ) {
            $('.showall-not-nolink').hide();
        } else {
            $('.showall-not-nolink').show();
        }
    });

    $('.special_offer-linktype input[type=radio]').click(function () {
        //console.log('mixer');
        if ( $(this).val() == 'no_link' ) {
            $('.showall-not-nolink').hide();
        } else {
            $('.showall-not-nolink').show();
        }
    });

    $('.btn-repeater-add').click( function(){
        var repeater_length = parseInt($('.repeater-item-list .list-group-item').length); //+ 1
        var repeater_html = $('.list-group-item-origin').html().replace(/rpt_number/g,repeater_length);
        repeater_html = '<div class="list-group-item d-flex mb-2 list-group-item-row-'+repeater_length+'" data-row="'+repeater_length+'">'+repeater_html+'</div>';

        $('.repeater-item-list').append( repeater_html );

        tinymce.init({
            selector: ".list-group-item-row-"+repeater_length+" .original-tinymce",
            theme: "modern",
            paste_data_images: true,
            plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
            ],
        });   

        $('.list-group-item-row-'+repeater_length+'.repeater-item-list').sortable({ animation: 150, containment: "parent", cursor: "move", scroll: false, axis: "y", start: function(e, ui) { $(ui.item).find('textarea').each(function() { tinymce.execCommand('mceRemoveEditor', false, $(this).attr('id')); }); }, stop: function(e, ui) { $(ui.item).find('textarea').each(function() { tinymce.execCommand('mceAddEditor', true, $(this).attr('id')); }); } });
    });

    $('.addFeature').click(function () {
        $("#package-sortable .list-group-item").children().length;
        var package_length = parseInt($('#package-sortable .list-group-item').length) + 1;
        var feature = $('select[name=sel-feature] option:selected').val();
        var feature_html = $('.data-original .feature-'+feature).html().replace(/pkg_number/g,package_length);
        feature_html = '<div class="card list-group-item feature-'+feature+' card-item-'+package_length+'" data-pkg="'+package_length+'">'+feature_html+'</div>';

        $('#package-sortable').append( feature_html );
        smepack_init();

        $('.card-item-'+package_length+' .button-switch input[type=checkbox]').bootstrapToggle();

        tinymce.init({
            selector: "#collapse-"+package_length+" .original-tinymce",
            theme: "modern",
            paste_data_images: true,
            plugins: [
              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars code fullscreen",
              "insertdatetime media nonbreaking save table contextmenu directionality",
              "emoticons template paste textcolor colorpicker textpattern"
            ],
        });

        $('#collapse-'+package_length+' .original-dropify').dropify(); 

        $('#collapse-'+package_length+' .file-upload-browse').on('click', function() {
          var file = $(this).parent().parent().parent().find('.file-upload-default');
          file.trigger('click');
        });

        wrapper_special_offer( '#collapse-'+package_length+' ' );


        $('html, body').animate({
            scrollTop: ($("#heading-"+package_length).offset().top - 80)
        }, 2000);
    });

    smepack_init();
    wrapper_special_offer();
    repeater_module();

    wrapper_check_select();
   
    $('input[name=highlight_sme_mainpage]').click(function () {
        var hl_main_content = $('.highlight_sme_mainpage_content');
        if ( $(this).is(":checked") ) {
            hl_main_content.show();
        } else {
            hl_main_content.hide();
        }
    });

    
})


function repeater_module (method = '') {
    if ( $(".list-group-item-origin").length ) {
      
        var repeater_length = parseInt($('.repeater-item-list .list-group-item').length); //+ 1
        var repeater_html = $('.list-group-item-origin').html().replace(/rpt_number/g,repeater_length);
        repeater_html = '<div class="list-group-item d-flex mb-2 list-group-item-row-'+repeater_length+'" data-row="'+repeater_length+'">'+repeater_html+'</div>';

        if ( (repeater_length == 0 && method == '') || (method == 'add') ) {
            $('.repeater-item-list').append( repeater_html );

            tinymce.init({
                selector: ".list-group-item-row-"+repeater_length+" .original-tinymce",
                theme: "modern",
                paste_data_images: true,
                plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
                ],
            });   

            $('.list-group-item-row-'+repeater_length+'.repeater-item-list').sortable({ animation: 150, containment: "parent", cursor: "move", scroll: false, axis: "y", start: function(e, ui) { $(ui.item).find('textarea').each(function() { tinymce.execCommand('mceRemoveEditor', false, $(this).attr('id')); }); }, stop: function(e, ui) { $(ui.item).find('textarea').each(function() { tinymce.execCommand('mceAddEditor', true, $(this).attr('id')); }); } });

        } 
        
        $('.btn-repeater-delete').click(function () {

            var row = $(this).parents('.list-group-item').attr('data-row');

            $('#repeater_deleteModal').modal('show');

            $('.repeater-modal-delete-confirm').attr('data-row', row);

        });   
        
        $('.repeater-modal-delete-confirm').click( function(){
            var row = '';
            row = $(this).attr('data-row');
            //console.log(pkg);
            $('.list-group-item-row-'+row).remove();
        });
    }
}


function wrapper_check_select (id = '') {

    $(id+'.wrapper_check_select input[type=checkbox]').click(function () {
        
        var id = $(this).val();
        var name = $(this).data('name');
        var select_name = $(this).data('select_name');
        var div_parents = $(this).closest('.wrapper_check_select').find('.gen-topup');
        //var pkg_number = $(this).closest('.list-group-item').data('pkg');

        var content  = '';
        content += ' <li class="drag-list level-0 sp-'+id+'"><input type="hidden" name="'+select_name+'[]" value="'+id+'"><div class="box-select-topup "><div class="box-txt-topup"><p>'+name+'</p></div><div class="icon-move"><i class="mdi mdi-cursor-move"></i></div>	</div></li>';
        
        if ( $(this).prop("checked") == true ) {
            div_parents.append( content );
            //console.log( 'checked' );
        } else {
            div_parents.find('.sp-'+id).remove();
            //console.log( 'not checked' );
        }
        
    });

    $('.wrapper_check_select .gen-topup').sortable();

    //div_parents.sortable();
}

function wrapper_special_offer (id = '') {

    $(id+'.wrapper_special_offer input[type=checkbox]').click(function () {
        
        var id = $(this).val();
        var name = $(this).data('name');
        var div_parents = $(this).closest('.wrapper_special_offer').find('.gen-topup');
        var pkg_number = $(this).closest('.list-group-item').data('pkg');

        var content  = '';
        content += ' <li class="drag-list level-0 sp-'+id+'"><input type="hidden" name="package[pkg_number][special_offer_select][]" value="'+id+'"><div class="box-select-topup "><div class="box-txt-topup"><p>'+name+'</p></div><div class="icon-move"><i class="mdi mdi-cursor-move"></i></div>	</div></li>';
        if ( pkg_number != '' ) {
            content = content.replace(/pkg_number/g,pkg_number);
        }
        
        if ( $(this).prop("checked") == true ) {
            div_parents.append( content );
            //console.log( 'checked' );
        } else {
            div_parents.find('.sp-'+id).remove();
            //console.log( 'not checked' );
        }
    });

    $('.special_offer_sortable').sortable();
}

function changeBoxContent($this){
  var getVal = $this.find('input.form-check-input')[0].value;
  var parent = $this.closest('.box-tab-switch');
  var findParent = parent.find('.box-form-inner');


   if($this.find('input.form-check-input')[0].type == 'radio'){
    $this.closest('.form-group').find('input[type="radio"]').prop('checked', false); 
    $this.find('input.form-check-input').prop('checked', true); 
  
    if(getVal != 'none'){
      if(findParent.not('.'+getVal ) || findParent.not('.is-tab' ) ){
        findParent.hide();
      }
      parent.find('.box-form-inner.'+getVal).show();
    }else{
      parent.find('.box-form-inner').hide();
    }
   }else{
     
     if($this.find('input.form-check-input')[0].checked == true){
      if(findParent.find('.'+getVal)){
        parent.find('.box-form-inner.'+getVal).show();
        console.log('check');
      }
     }else{
        parent.find('.box-form-inner'+'.'+getVal).hide();
     }
     
   }
}

function acc_toggle () {
    $('.acc-toggle').click(function(){
    var elem = $(this).text();
    //console.log(elem);
        if (elem == 'Show detail') {
            $(this).html("Hide detail<img src='/assets/admin/images/element/section/nx-open.png'/>");
        } else {
            $(this).html("Show detail<img src='/assets/admin/images/element/section/nx-close.png'/>");
        }
    });
}


function smepack_init(){

    packageSortable();

    acc_toggle();
    //selectTopup();

    // $('.is-tab .box-radio').click(function () {
    //     changeBoxContent($(this));
    // });

    $('.is-tab .box-radio').click(function () {
        changeBoxContent($(this));
    });

    $('.btn_tab').click(function () {
        $(this).tab('show');
        $(this).removeClass('active');
    });

    //smepackToggle();
}

function repeater_sortable(){
  $('.repeater-item-list.drag-item-list').sortable();
}

function smepackToggle(){
  // $('.button-switch input[type=checkbox]').bootstrapToggle('destroy')
  // $('.button-switch input[type=checkbox]').bootstrapToggle()
} 


function button_content_setting() {
    var btn_link_type = $('input[name=btn_link_type]:checked').val();
    var btn_display = $('input[name=btn_display]:checked').val();

    var row_btn_display = $('.row_btn_display');
    var row_btn_link = $('.row_btn_link');
    var row_btn_text = $('.row_btn_text');

    if ( (btn_link_type == 'auto' && btn_display == 'hide') || (btn_link_type == 'auto' && btn_display == 'show') ) {
        row_btn_display.show();
        row_btn_link.hide();
        row_btn_text.hide();
    } else if ( btn_link_type == 'no_link' ) {
        row_btn_display.hide();
        row_btn_link.hide();
        row_btn_text.hide();
    } else if ( (btn_link_type == 'external_link' && btn_display == 'hide') ) {
        row_btn_display.show();
        row_btn_link.show();
        row_btn_text.hide();
    } else if ( (btn_link_type == 'external_link' && btn_display == 'show') ) {
        row_btn_display.show();
        row_btn_link.show();
        row_btn_text.show();
    }
}

$(document).ready(function () {
    button_content_setting();
    $('input[name=btn_link_type], input[name=btn_display]').click( function(){
        button_content_setting();
    });
})