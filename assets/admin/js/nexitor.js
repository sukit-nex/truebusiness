// JavaScript Document

var activeElementPopup = '';
$( document ).ready( function(){
  $('.btn-addelement').hide();
  installNexitor();
  $('.saveForm').click( function(){
    $('.loader').fadeIn();
    generateHtmlData('save');
  });
  /*$('form').on('submit', function(){
    submitFrom();
  });*/

  $('.btn-nexitor-preview').click( function(){
	  $('.nexitor-preview-here').html('');
	  $('.nexitor-preview-here').removeClass('before_disable');
	  setTimeout(function(){
		  generateHtmlData('preview');
	  },1000)
  });
});
function generateHtmlData(action){

  var action_fn = action;
  $install = $('.install-nexitor');
  
  
  $install.each( function(index_in,value_in){ // Each nexitor
    // Add text area to install section
    $nexitor_row = $(this).find('.nexitor-row');
    $nexitor_col = $(this).find('.nexitor-col');
    var html = "";
    var name = $(this).attr('data-area');
    var textarea = '<textarea name="'+name+'[]" style="position:absolute;left:0;top:0;z-index:-20;"></textarea>';
    var textarea_editor = '<textarea name="'+name+'_editor[]" style="position:absolute;left:0;top:0;z-index:-20;"></textarea>';
    if( $(this).find('textarea[name="'+name+'[]"]').length == 0 ){
      $(this).append( textarea );
    }
    if ($(this).find('textarea[name="'+name+'_editor[]"]').length == 0 ){
      $(this).append( textarea_editor );
    }
    // Each nexitor row for create html of row
    $nexitor_row.each( function(index,value){
      html += '<div class="row">';
      $(this).find('.nexitor-col').each( function(index,value){
          html += '<div class="'+$(this).attr('class')+'">';
          $(this).find('.box-nexitor').each( function( index,value ){
            var type = $(this).attr('data-type');
            if( type == "title"){
              var text = $(this).find('input').val();
              var style = $(this).find('input').attr('style');
              var htag = $(this).find('.nexitor-body').attr('data-htag');
              var align = $(this).find('.nexitor-body').attr('data-align');
              var link = $(this).find('.nexitor-body').attr('data-link');
              var bold = $(this).find('.nexitor-body').attr('data-bold');
              var italic = $(this).find('.nexitor-body').attr('data-italic');
              var underline = $(this).find('.nexitor-body').attr('data-underline');

              if( link == "" ){
                html += '<div class="box-content '+align+'">';
                html += ' <'+htag+' class="'+bold+' '+italic+' '+underline+'" style="'+style+'">'+text+'</'+htag+'>';
                html += '</div>';
              }else{

                html += ' <div class="box-content '+align+'">';
				        html += '	<a href="'+link+'" target="_blank">';
                html += '   	<'+htag+' class="'+bold+' '+italic+' '+underline+'" style="'+style+'">'+text+'</'+htag+'>';
				        html += '	</a>';
                html += ' </div>';

              }
            }else if( type == "content"){
              var text = $(this).find('.nexitor-textarea').html();
              html += '<div class="box-content box-content-editor '+align+'">';
              html += text;
              html += '</div>';
            }else if( type == "image" ){
              if( $(this).find('.dropify-wrapper').hasClass('has-preview') ){

                var src_img = $(this).find('.dropify-wrapper').find('.dropify-render').find('img').attr('src');
                //console.log(' src img ' + src_img);
                var new_image = false;
                //console.log( src_img.indexOf("data:image") );
                if( src_img.indexOf("data:image") >= 0 ){
                  new_image = true;
                }
                var time = $('input[name="time"]').val();
                //console.log(new_image);
                //console.log( time )
                var filename = $(this).find('.dropify-filename-inner').html();
                if( new_image ){
                  file_basename = filename.split(".")[0];
                  file_ext = filename.split(".")[1];
                  filename = file_basename+'_'+time+'.'+file_ext;
                  //console.log( filename );
                }

                //====
                var width = $(this).find('.nexitor-body').attr('data-width');
                var align = $(this).find('.nexitor-body').attr('data-align');
                var link = $(this).find('.nexitor-body').attr('data-link');
        				if( action_fn == "save" ){
        					if( link == "" ){
        					  html += '<div class="box-content '+align+'">';
        					  html += ' <img src="'+upload_path+''+''+''+filename+'" class="'+width+'" />';
        					  html += '</div>';
        					}else{

        					  html += '<div class="box-content '+align+'">';
        					  html += '	<a href="'+link+'" target="_blank">';
                    html += '   <img src="' + upload_path + '' + '' + '' +filename+'" class="'+width+'" />';
        					  html += '	</a>';
        					  html += '</div>';

        					}
        				}else{
        					$(this).find('.dropify-render').find('img').addClass(width);
        					var img_html = $(this).find('.dropify-render').html();


        					//var src = $(this).find('.dropify-render').find('img').attr('src');

        					if( link == "" ){
        					  html += '<div class="box-content '+align+'">';
        					  html += img_html;
        					  //html += ' <img src="'+src+'" class="'+width+'" />';
        					  html += '</div>';
        					}else{
        					  html += '<div class="box-content '+align+'">';
        					  html += '	<a href="'+link+'" target="_blank">';
        					  html += img_html;
        					  //html += ' <img src="'+src+'" class="'+width+'" />';
        					  html += '	</a>';
        					  html += '</div>';
        					}
        				}
              }
            }else if(  type == "video" ){
              var text = $(this).find('input').val();
              var width = $(this).find('.nexitor-body').attr('data-width');
              var align = $(this).find('.nexitor-body').attr('data-align');
              var ratio = $(this).find('.nexitor-body').attr('data-ratio');

              html += '<div class="box-content '+align+'">';
              html += ' <div class="embed-responsive embed-responsive-'+ratio+' d-inline-block '+width+'">';
  						html += '		<iframe src="//www.youtube.com/embed/'+text+'" allowfullscreen=""></iframe>';
							html += '  </div>';
              html += '</div>';
            } else if (type == "button") {

              var button_type = "";
              if( $(this).find('.button-text').hasClass('active')){
                button_type = "text";
              } else if ($(this).find('.button-image').hasClass('active')) {
                button_type = "image";
              }
              var text = $(this).find('input[name="button-text"]').val();
              var link = $(this).find('input[name="button-link"]').val();
              var align = $(this).find('.nexitor-body').attr('data-align');
              var color = $(this).find('.nexitor-body').attr('data-color');
              var bg_color = $(this).find('.nexitor-body').attr('data-bg-color');
              var is_new_tab = $(this).find('.nexitor-body').attr('data-newtab');
              var is_inline = $(this).find('.nexitor-body').attr('data-inline');
              var column = $(this).find('.nexitor-body').attr('data-column');
              if( link == "" ){
                link = "javascript:void(0);";
              }
              var target = "";
              var inline_class = "";
              var column_class = "";
              if (is_new_tab == 1){
                target = "_blank";
              }
              if (is_inline == 1) {
                inline_class = "button-hoz";
                column_class = column;
              }
              var img_html = "";
              if ($(this).find('.dropify-wrapper').hasClass('has-preview')) {

                var src_img = $(this).find('.dropify-wrapper').find('.dropify-render').find('img').attr('src');
                var new_image = false;
                if (src_img.indexOf("data:image") >= 0) {
                  new_image = true;
                }

                var filename = $(this).find('.dropify-filename-inner').html();
                if (new_image) {
                  var time = $('input[name="time"]').val();
                  file_basename = filename.split(".")[0];
                  file_ext = filename.split(".")[1];
                  filename = file_basename + '_' + time + '.' + file_ext;
                }
                img_html = ' <img src="' + upload_path + '' +  filename + '" />';
              }else{
                $(this).find('.dropify-render').find('img').addClass(width);
                img_html = $(this).find('.dropify-render').html();
              }
              var span_style = "";
              if( color != ""){
                //span_style = '-webkit-text-fill-color: inherit;';
                span_style = '-webkit-text-fill-color: '+color+';';
              }
              if (button_type == "text" ){
                html += '<div class="box-content box-content-button ' + align + ' ' + inline_class + ' ' + column_class+'">';
                html += '  <a href="' + link + '" target="' + target +'" class="btn" style="color: ' +  color  + '; background: ' +  bg_color + '"> <span style="'+ span_style +'">' + text +'</span> </a>';
                html += '</div>';
              }else{
                html += '<div class="box-content box-content-button ' + align + ' ' + inline_class + ' ' + column_class +'">';
                html += '  <a href="' + link + '" target="' + target +'" class=""> ' + img_html +' </a>';
                html += '</div>';
              }
            } else if (type == "space") {
              html += '<div class="box-content box-content-space">';
              html += '</div>';
            }
          });
          html += '</div>';
      });
      html += '</div>';
    });
    $(this).find('textarea[name="'+name+'[]"]').val( html );
    $(this).find('input').each( function(){
      $(this).attr('value', $(this).val() );
    });
    if(action_fn == 'save'){
      $('.dropify_editor').each( function(){
      if( $(this).closest('.dropify-wrapper').hasClass('has-preview') ){
        var src_img = $(this).closest('.dropify-wrapper').find('.dropify-render').find('img').attr('src');
        var new_image = false;
        if( src_img.indexOf("data:image") >= 0 ){
          new_image = true;
        }
        setTimeout(function(){
          $(this).closest('.dropify-wrapper').find('.dropify-render').find('img').attr('src', '');
        },100);
        var nameoffile = $(this).closest('.dropify-wrapper').find('.dropify-filename-inner').html();
        if( nameoffile != ""){
          var time = $('input[name="time"]').val();
          //console.log('name of file'+ nameoffile)
          if( new_image ){
            var file_basename = nameoffile.split(".")[0];
            var file_ext = nameoffile.split(".")[1];
            //console.log('name of file ext' + file_basename + '_' + time + '.' + file_ext)
            $(this).attr('data-default-file', upload_path + '' + '' + '' +file_basename+'_'+time+'.'+file_ext );
          }
        }
      }else{
        $(this).attr('data-default-file','');
      }
      });
    }
    /*$('svg').each( function(){
      $(this).replaceWith('<img src="'+$(this).attr('data-url')+'" class="svg">');
    });*/
    $(this).find('textarea[name="'+name+'_editor[]"]').val( $(this).html() );

  });


  if(action_fn == 'save'){

	 submitForm();

  }else{
	$('.tab-pane').each(function(index, element) {
        if( $(this).hasClass('active') ){
			var name = $('.tab-pane').find( '.install-nexitor' ).attr('data-area');
			var html = $('.tab-pane').find( 'textarea[name="'+name+'[]"]' ).val();
			$('.nexitor-preview-here').html(html);
			$('.nexitor-preview-here').addClass('before_disable');
			$('.loader').fadeOut();
		}
    });

  }

}
function submitForm(){
  $('form.mainForm').submit();
}
function installNexitor(){
  $('.install-nexitor').each( function(){
    if( $(this).find('.box-layer').length == 0){
      $(this).append( loadInstallHTML() );
    }
  });
  $('body').prepend( loadPopupHTML() );
  $('.nexitor-choice').click( function(){
    nexitorClick( $(this) );
  });
  
  if ($(".tinyMce-nex").length) {
		tinymce.init({
		selector: ".tinyMce-nex",
		theme: "modern",
		paste_data_images: true,
		plugins: [
		  "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		  "searchreplace wordcount visualblocks visualchars code fullscreen",
		  "insertdatetime media nonbreaking save table contextmenu directionality",
		  "emoticons template paste textcolor colorpicker textpattern"
    ],
    paste_remove_styles_if_webkit: true,
		toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons",
    // without images_upload_url set, Upload tab won't show up
    //images_upload_url: base_url + "admin/upload/do_upload_tiny",
    //images_upload_base_path: '/some/basepath',
    image_advtab: true,
      images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;

        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', base_url + "admin/upload/do_upload_tiny");

        xhr.onload = function () {
          var json;

          if (xhr.status != 200) {
            failure('HTTP Error: ' + xhr.status);
            return;
          }

          json = JSON.parse(xhr.responseText);

          if (!json || typeof json.location != 'string') {
            failure('Invalid JSON: ' + xhr.responseText);
            return;
          }
          //console.log( xhr.responseText );
          success( json.location );
        };

        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
        formData.append('token', $("input[name='token']").val());
        xhr.send(formData);
      },
		/*file_picker_callback: function(callback, value, meta) {
		  if (meta.filetype == 'image') {
			  $('#upload-img-tiny').trigger('click');
        $('#upload-img-tiny').on('change', function() {
          var file = this.files[0];
          var reader = new FileReader();
          reader.onload = function(e) {
          callback(e.target.result, {
            alt: ''
          });
          };
          reader.readAsDataURL(file);
        });
		  }
		},*/
		templates: [{
		  title: 'Test template 1',
		  content: 'Test 1'
		}, {
		  title: 'Test template 2',
		  content: 'Test 2'
		}]
		});
	}
  
  //  var gArrayFonts = ['gt_walsheim_problack','gt_walsheim_probold','gt_walsheim_promedium','true_boldregular','true_bold_specialregular','true_lightregular','true_mediumregular'];
  $(".summernote-nex").summernote({
    // fontNamesIgnoreCheck: gArrayFonts,
    enterHtml: '<p><br></p>',
    
    /*colors: [
      ['red', 'green', 'blue'], //first line of colors
      ['#c4b540', '#1dd381', '#ba1cd2'] //second line of colors
    ],*/
    // fontname: [],
  	toolbar: [
      ['style', ['style','bold', 'italic', 'underline', 'clear']],
      ['fontsize', ['color']],
  	  ['para', [ 'ul', 'ol', 'paragraph']],
  	  ['insert', ['link','picture']],
      ['misc', ['']],
      ['view', ['codeview','help']],
    ],
    height: 500,
    callbacks:{
      onImageUpload : function( files, editor, welEditable ){
        for( var i = files.length - 1; i >= 0; i-- ){
          sendFile(files[i], this);
        }
      },
      onPaste: function (e) {
        if (document.queryCommandSupported("insertText")) {
          var text = $(e.currentTarget).html();
          var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

          setTimeout(function () {
            document.execCommand('insertText', false, bufferText);
          }, 10);
          e.preventDefault();
        } else {
          var text = window.clipboardData.getData("text")
          if (trap) {
            trap = false;
          } else {
            trap = true;
            setTimeout(function () {
              document.execCommand('paste', false, text);
            }, 10);
            e.preventDefault();
          }
        }

      }
    }
  });
  // $(document).on("DOMNodeInserted", '.note-editable', function (e) {
  //   if (e.target.tagName === "DIV") {
  //       // $(e.target).replaceWith($('<span>'));
  //       // $(e.target).replaceWith($(e.target).contents());    
  //       var getText = e.target;
  //       $(e.target).replaceWith(function() {
  //         return '<span'+getText+'>'+$(this).html()+'</span>';
  //       });
  //   }
     
 
  //});
  $('.dropify_editor').each( function(){
    var drop = $(this).dropify();
    drop = drop.data('dropify');
    drop.destroy();
    drop.destroy();
    drop.init();
  });
  sortableRow();
  sortableElement();
}
function addColumn( $this ){
  var column = $this.attr('data-columns');
  var html = loadLayoutHTML( column );
  $this.closest('.install-nexitor').find('.box-layer').append( html );
  generateSVG();
  sortableRow();
}
function elementClick( $this ){
  var element = $this.attr('data-element');
  var html = "";
  if( element == "title" ){
    html = loadTitleHTML();
  }else if( element == "content" ){
    html = loadContentHTML();
  }else if( element == "image" ){
    html = loadImageHTML();
  }else if( element == "video" ){
    html = loadVideoHTML();
  }else if (element == "button") {
    html = loadButtonHTML();
  }else if (element == "space") {
    html = loadSpaceHTML();
  }
  $this.closest('.nexitor-col').find('.add-element').append( html );
  //$(this).closest('.nexitor-col').find('.box-element').hide();
  $this.closest('.nexitor-col').find('.btn-addelement').show();
  generateSVG();
  sortableElement();
  if (element == "image" || element == "button" ){
    $this.closest('.nexitor-col').find('.dropify_editor').dropify();
  }
}
function loadInstallHTML(){
  var html = "";
  html += '<div class="box-layer sortable-row"></div>';
  html += '<div class="box-addcolumn ">';
  html += '    <div class="addcolumn">';
  html += '        <div class="addcolumn-list"><a href="javascript:void(0)" class="addcolumn-style addcolumn-1 addcolumn-choice" data-columns="1" onclick="addColumn( $(this) )"><span></span></a></div>';
  html += '          <div class="addcolumn-list">';
  html += '            <a href="javascript:void(0)" class="addcolumn-style addcolumn-2-11 "><span></span><span></span></a>';
  html += '              <div class="addcolumn-hover">';
  html += '                  <a href="javascript:void(0)" class="addcolumn-style addcolumn-2-11 addcolumn-choice" data-columns="211" onclick="addColumn( $(this) )"><span></span><span></span></a>';
  html += '                  <a href="javascript:void(0)" class="addcolumn-style addcolumn-2-13 addcolumn-choice" data-columns="213" onclick="addColumn( $(this) )"><span></span><span></span></a>';
  html += '                  <a href="javascript:void(0)" class="addcolumn-style addcolumn-2-31 addcolumn-choice" data-columns="231" onclick="addColumn( $(this) )"><span></span><span></span></a>';
  html += '              </div>';
  html += '          </div>';
  html += '          <div class="addcolumn-list"><a href="javascript:void(0)" class="addcolumn-style addcolumn-3 addcolumn-choice" data-columns="3" onclick="addColumn( $(this) )"><span></span><span></span><span></span></a></div>';
  html += '          <div class="addcolumn-list"><a href="javascript:void(0)" class="addcolumn-style addcolumn-4 addcolumn-choice" data-columns="4" onclick="addColumn( $(this) )"><span></span><span></span><span></span><span></span></a></div>';
  html += '      </div>';
  html += '      <div class="addcolumn-text">Choose columns</div>';
  html += '  </div>';
  return html;
}
function loadLayoutHTML( column ){
  var html = "";
   html += '<div class="content-row grid-margin">';
   html += '  <div class="content-row-control">';
   html += '    <ul>';
   html += '         <li><a href="javascript:void(0)" onClick="nexitorpopup( $(this) )"><img src="'+assets_path+'images/nexitor/icon-settings.svg" class="svg"></a></li>';
   html += '         <li><a href="javascript:void(0)"><img src="'+assets_path+'images/nexitor/icon-move.svg" class="svg"></a></li>';
   html += '         <li><a href="javascript:void(0)" class="close-content-row" onclick="closeContentRow( $(this) );"><img src="'+assets_path+'images/nexitor/icon-cancel.svg" class="svg"></a></li>';
   html += '    </ul>';
   html += '  </div>';
   html += '  <div class="row nexitor-row">';
        if( column == '1' ){
          html += '<div class="col col-12 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
        }
        else if( column == '211' ){
          html += '<div class="col col-12 col-sm-6 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-6 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
        }
        else if( column == '213' ){
          html += '<div class="col col-12 col-sm-3 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-9 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
        }
        else if( column == '231' ){
          html += '<div class="col col-12 col-sm-9 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-3 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
        }
        else if( column == '3' ){
          html += '<div class="col col-12 col-sm-4 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-4 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-4 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
        }
        else if( column == '4' ){
          html += '<div class="col col-12 col-sm-3 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-3 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-3 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
          html += '<div class="col col-12 col-sm-3 nexitor-col" data-col="'+column+'">';
          html += '<div class="add-element sortable-element"></div>';
          html += loadActionButtonHTML();
          html += '</div>';
        }
   html += '  </div>';
   html += '</div>';
   return html;
}

function loadActionButtonHTML(){
  var html = "";
  //html += '<a href="javascript:void(0)" class="btn-addelement" onclick="displayBoxElement($(this))"><span><img src="'+assets_path+'images/nexitor/icon-plus.svg" class="svg"></span></a>';
  html += '<div class="box-element show">';
  html += '    <div class="group-element">';
  html += '        <div class="element-list" data-element="title" onclick="elementClick($(this))">';
  html += '            <div>';
  html += '                  <div class="main-element-icon"><img src="'+assets_path+'images/nexitor/icon-title.svg"></div>';
  html += '                  <div class="main-element-text">Title</div>';
  html += '              </div>';
  html += '          </div>';
  html += '          <div class="element-list" data-element="content" onclick="elementClick($(this))">';
  html += '            <div>';
  html += '                  <div class="main-element-icon"><img src="'+assets_path+'images/nexitor/icon-content.svg"></div>';
  html += '                  <div class="main-element-text">Content</div>';
  html += '              </div>';
  html += '          </div>';
  html += '          <div class="element-list" data-element="image" onclick="elementClick($(this))">';
  html += '            <div>';
  html += '                  <div class="main-element-icon"><img src="'+assets_path+'images/nexitor/icon-image.svg"></div>';
  html += '                  <div class="main-element-text">Image</div>';
  html += '              </div>';
  html += '          </div>';
  html += '          <div class="element-list" data-element="video" onclick="elementClick($(this))">';
  html += '            <div>';
  html += '                  <div class="main-element-icon"><img src="'+assets_path+'images/nexitor/icon-video.svg"></div>';
  html += '                  <div class="main-element-text">Video</div>';
  html += '              </div>';
  html += '          </div>';
  html += '          <div class="element-list" data-element="button" onclick="elementClick($(this))">';
  html += '            <div>';
  html += '                  <div class="main-element-icon"><img src="' + assets_path + 'images/nexitor/icon-button.svg"></div>';
  html += '                  <div class="main-element-text">Button</div>';
  html += '              </div>';
  html += '          </div>';
  html += '          <div class="element-list" data-element="space" onclick="elementClick($(this))">';
  html += '            <div>';
  html += '                  <div class="main-element-icon"><img src="' + assets_path + 'images/nexitor/icon-space.svg"></div>';
  html += '                  <div class="main-element-text">Space</div>';
  html += '              </div>';
  html += '          </div>';
  html += '      </div>';
  html += '  </div>';
  return html;
}
function loadNexitorTool( element ){
  var html = "";
  html += '<div class="nexitor-tools">';
  html += ' <ul>';
  if (element != "space"){
    html += '   <li><a href="javascript:void(0)" onClick="nexitorpopup( $(this) )" data-element="'+element+'"><img src="'+assets_path+'images/nexitor/icon-settings.svg" class="svg"></a></li>';
  }
  html += '   <li><a href="javascript:void(0)"><img src="'+assets_path+'images/nexitor/icon-move.svg" class="svg"></a></li>';
  html += '   <li><a href="javascript:void(0)" class="close-nexitor" onclick="closeNexitor( $(this) );"><img src="'+assets_path+'images/nexitor/icon-cancel.svg" class="svg"></a></li>';
  html += ' </ul>';
  html += '</div>';
  return html;
}
function loadTitleHTML(){
  var html = "";
  html += '<div class="box-nexitor nexitor-type-title" data-type="title">';
  html += '    <div class="nexitor-head">';
  html += '        <div class="nexitor-headLeft">';
  html += '            <div class="nexitor-headLeft-logo"><img src="'+assets_path+'images/nexitor/icon-title.svg"></div>';
  html += '            <div class="nexitor-used">';
  html += '                <div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-h1.svg" class="svg"></div>';
  html += '              </div>';
  html += '          </div>';
  html += loadNexitorTool('title');
  html += '      </div>';
  html += '      <div class="nexitor-body" data-htag="h1" data-align="none" data-link="" data-bold="" data-italic="" data-underline="" data-color="">';
  html += '        <div class="nexitor-body-input"><input class="h1" name="text-title" type="text" placeholder="Please fill your head line"><label class="text-mute mt-2"> * ขึ้นบรรทัดใหม่กรุณาใส่ &lt;br&gt; หากต้องการให้ขึ้นบรรทัดใหม่เฉพาะในมือถือใส่ &lt;br class=\'d-sm-none\'&gt;</label></div>';
  html += '      </div>';
  html += '  </div>';
  return html;
}
function loadContentHTML(){
  var html = "";
  html += '<div class="box-nexitor nexitor-type-content" data-type="content">';
  html += '    <div class="nexitor-head">';
  html += '        <div class="nexitor-headLeft"><div class="nexitor-headLeft-logo"><img src="'+assets_path+'images/nexitor/icon-content.svg"></div></div>';
  html += loadNexitorTool('content');
  html += '      </div>';
  html += '      <div class="nexitor-body">';
  html += '        <div class="nexitor-body-textarea"><div class="nexitor-textarea"></div><div class="button-wrap w-100 text-center"><button type="button" class="btn btn-gradient-dark" onClick="nexitorpopup( $(this) )" data-element="content">Add Content</button></div></div>';
  html += '      </div>';
  html += '  </div>';
  return html;
}
function loadImageHTML(){
  var html = "";
  html += '<div class="box-nexitor nexitor-type-image" data-type="image">';
  html += '    <div class="nexitor-head">';
  html += '        <div class="nexitor-headLeft">';
  html += '            <div class="nexitor-headLeft-logo"><img src="'+assets_path+'images/nexitor/icon-image.svg"></div>';
  html += '              <div class="nexitor-used">';
  html += '              </div>';
  html += '          </div>';
  html += loadNexitorTool('image');
  html += '      </div>';
  html += '      <div class="nexitor-body" data-width="none" data-align="none" data-link="">';
  html += '        <!-- Dropify here -->';
  html += '        <input type="file" name="image_editor[]" class="dropify_editor" data-max-file-size="3M" data-default-file="" />';
  html += '      </div>';
  html += '  </div>';
  return html;
}

function loadVideoHTML(){
  var html = "";
  html += '<div class="box-nexitor nexitor-type-video" data-type="video">';
  html += '    <div class="nexitor-head">';
  html += '        <div class="nexitor-headLeft">';
  html += '            <div class="nexitor-headLeft-logo"><img src="'+assets_path+'images/nexitor/icon-video.svg"></div>';
  html += '              <div class="nexitor-used">';
  html += '                  <div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-ratio-16-9.svg" class="svg"></div>';
  html += '              </div>';
  html += '          </div>';
  html += loadNexitorTool('video');
  html += '      </div>';
  html += '      <div class="nexitor-body" data-width="none" data-align="none" data-ratio="16by9" >';
  html += '        <div class="nexitor-body-input"><input type="text" name="text-video" placeholder="Please fill your youtube Video ID"></div>';
  html += '        <label class="mt-2" for="">Video ID <small class="text-muted">( https://www.youtube.com/watch?v=<span style="color:red">tjZTVimUnd4</span> )</small></label>';
  html += '      </div>';
  html += '  </div>';
  return html;
}
function loadButtonHTML() {
  var html = "";
  html += '<div class="box-nexitor nexitor-type-button" data-type="button">';
  html += '    <div class="nexitor-head">';
  html += '        <div class="nexitor-headLeft">';
  html += '            <div class="nexitor-headLeft-logo"><img src="' + assets_path + 'images/nexitor/icon-button.svg"></div>';
  html += '            <div class="nexitor-used">';
  //html += '                <div class="nexitor-used-list"><img src="' + assets_path + 'images/nexitor/icon-h6.svg" class="svg"></div>';
  html += '              </div>';
  html += '          </div>';
  html += loadNexitorTool('button');
  html += '      </div>';
  html += '      <div class="nexitor-body" data-align="none" data-color="" data-bg-color="" data-newtab="" data-inline="" data-column="">';
  html += '       <div class="nexitor-body-type">';
  html += '         <div class="nexitor-body-type-list"><a href="javascript:void(0)" class="button-type-choice button-choice button-text active" data-tag="button-text" onclick="activeFormElement( $(this) )"><img src="' + assets_path + 'images/nexitor/icon-title.svg"><span>Button Text</span></a></div>';
  html += '         <div class="nexitor-body-type-list"><a href="javascript:void(0)" class="button-type-choice button-choice button-image" data-tag="button-image" onclick="activeFormElement( $(this) )"><img src="' + assets_path + 'images/nexitor/icon-image.svg"><span>Button Image</span></a></div>';
  html += '       </div>';
  html += '       <div class="nexitor-body-form">'
  html += '         <div class="nexitor-body-input button-text-input"><input class="h6" name="button-text" type="text" placeholder="Text on your button"></div>';
  html += '         <div class="button-image-input d-none"><input type="file" name="image_editor[]" class="dropify_editor" data-max-file-size="3M" data-default-file="" /></div>';
  html += '         <div class="nexitor-body-input"><input class="h6" name="button-link" type="text" placeholder="Link of your button"></div>';
  html += '       </div>';
  html += '      </div>';
  html += '  </div>';
  return html;
}
function loadSpaceHTML() {
  var html = "";
  html += '<div class="box-nexitor nexitor-space" data-type="space">';
  html += '    <div class="nexitor-head">';
  html += '        <div class="nexitor-headLeft"><div class="nexitor-headLeft-logo"><img src="' + assets_path + 'images/nexitor/icon-space.svg"></div></div>';
  html += loadNexitorTool('space');
  html += '      </div>';
  html += '      <div class="nexitor-body">';
  html += '        <div class="nexitor-body-space"><div class="nexitor-space"></div></div></div>';
  html += '      </div>';
  html += '  </div>';
  return html;
}
function closePopup( $this ){
  $this.closest('.section-nexitorPopup').hide();
}
function loadPopupHTML(){
  var html = "";
  html += '<div class="section-nexitorPopup nexitorPopup-title">';
  html += '  	<div class="nexitorPopup-bg"></div>';
  html += '  	<div class="nexitorPopup-container">';
  html += '      	<div class="nexitorPopup-close" onclick="closePopup( $(this) )"><img src="'+assets_path+'images/nexitor/icon-cancel.svg" class="svg"></div>';
  html += '      	<div class="nexitorPopup-content">';
  html += '          	<div class="nexitorPopup-content-head"><div class="nexitorPopup-content-headtitle"><img src="'+assets_path+'images/nexitor/icon-title.svg"> <h2>Title</h2></div></div>';
  html += '              <div class="nexitorPopup-content-body">';
  html += '              	<div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Title :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice htag-choice htag-h1 active" data-tag="h1"><img src="'+assets_path+'images/nexitor/icon-h1.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice htag-choice htag-h2" data-tag="h2"><img src="'+assets_path+'images/nexitor/icon-h2.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice htag-choice htag-h3" data-tag="h3"><img src="'+assets_path+'images/nexitor/icon-h3.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice htag-choice htag-h4" data-tag="h4"><img src="'+assets_path+'images/nexitor/icon-h4.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice htag-choice htag-h5" data-tag="h5"><img src="'+assets_path+'images/nexitor/icon-h5.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice htag-choice htag-h6" data-tag="h6"><img src="'+assets_path+'images/nexitor/icon-h6.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '              	<div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Style :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice style-choice style-none" data-tag="none"><img src="'+assets_path+'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice style-choice style-font-weight-bold" data-tag="font-weight-bold"><img src="'+assets_path+'images/nexitor/icon-bold.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice style-choice style-font-italic" data-tag="font-italic"><img src="'+assets_path+'images/nexitor/icon-italic.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice style-choice style-font-underline" data-tag="font-underline"><img src="'+assets_path+'images/nexitor/icon-underline.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Alignment :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-none active " data-tag="none"><img src="'+assets_path+'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-left" data-tag="text-left"><img src="'+assets_path+'images/nexitor/icon-align-left.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-center" data-tag="text-center"><img src="'+assets_path+'images/nexitor/icon-align-center.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-right" data-tag="text-right"><img src="'+assets_path+'images/nexitor/icon-align-right.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Link :</div>';
  html += '                      <div class="nexitorPopup-content-body-input">';
  html += '                      	<input type="text" name="text-link" placeholder="Please fill your link">';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Color :</div>';
  html += '                      <div class="nexitorPopup-content-body-input">';
  html += '                      	<input type="text" name="text-color" placeholder="Please fill your color code">';
  html += '                      </div>';
  html += '                  </div>';
  html += '              </div>';
  html += '              <div class="nexitorPopup-content-footer">';
  html += '              	<div>';
  html += '                  	<button type="button" class="btn btn-secondary btn-fw" onclick="closePopup( $(this) )">Cancel</button>';
  html += '                    <button type="button" class="btn btn-dark btn-fw updateButton" onclick="updateElement( $(this) )" data-element="">Done</button>';
  html += '                  </div>';
  html += '              </div>';
  html += '          </div>';
  html += '      </div>';
  html += '  </div>';
  // End title popup
  html += '  <div class="section-nexitorPopup nexitorPopup-content">';
  html += '  	<div class="nexitorPopup-bg"></div>';
  html += '  	<div class="nexitorPopup-container">';
  html += '      	<div class="nexitorPopup-close" onclick="closePopup( $(this) )"><img src="'+assets_path+'images/nexitor/icon-cancel.svg" class="svg"></div>';
  html += '      	<div class="nexitorPopup-content">';
  html += '          	<div class="nexitorPopup-content-head"><div class="nexitorPopup-content-headtitle"><img src="'+assets_path+'images/nexitor/icon-content.svg"> <h2>Content</h2></div></div>';
  html += '              <div class="nexitorPopup-content-body">';
  html += '              	<div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Title :</div>';
  html += '                      <div class="nexitorPopup-content-body-editor">';
  html += '                        <!-- Summernote here -->';
  //html += '                        <textarea id="" class="form-control summernote-nex " name="text-content" rows="10">Default text</textarea>';
  html += '                        <textarea name="text-content" class="tinyMce-nex">Edit your content here...</textarea>';
  html += '                      </div>';
  html += '                  </div>';
  html += '              </div>';
  html += '              <div class="nexitorPopup-content-footer">';
  html += '              	<div>';
  html += '                  	<button type="button" class="btn btn-secondary btn-fw" onclick="closePopup( $(this) )">Cancel</button>';
  html += '                    <button type="button" class="btn btn-dark btn-fw updateButton" onclick="updateElement( $(this) )" data-element="">Done</button>';
  html += '                  </div>';
  html += '              </div>';
  html += '          </div>';
  html += '      </div>';
  html += '  </div>';
  // End popup content
  html += '  <div class="section-nexitorPopup nexitorPopup-image">';
  html += '  	<div class="nexitorPopup-bg"></div>';
  html += '  	<div class="nexitorPopup-container">';
  html += '      	<div class="nexitorPopup-close" onclick="closePopup( $(this) )"><img src="'+assets_path+'images/nexitor/icon-cancel.svg" class="svg"></div>';
  html += '      	<div class="nexitorPopup-content">';
  html += '          	<div class="nexitorPopup-content-head"><div class="nexitorPopup-content-headtitle"><img src="'+assets_path+'images/nexitor/icon-image.svg"> <h2>Image</h2></div></div>';
  html += '              <div class="nexitorPopup-content-body">';
  html += '              	<div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Width (%) :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-none active " data-tag="none"><img src="'+assets_path+'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-25" data-tag="w-25"><img src="'+assets_path+'images/nexitor/icon-width-25.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-50" data-tag="w-50"><img src="'+assets_path+'images/nexitor/icon-width-50.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-75" data-tag="w-75"><img src="'+assets_path+'images/nexitor/icon-width-75.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-100" data-tag="w-100"><img src="'+assets_path+'images/nexitor/icon-width-100.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Alignment :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-none active " data-tag="none"><img src="'+assets_path+'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-left" data-tag="text-left"><img src="'+assets_path+'images/nexitor/icon-align-left.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-center" data-tag="text-center"><img src="'+assets_path+'images/nexitor/icon-align-center.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-right" data-tag="text-right"><img src="'+assets_path+'images/nexitor/icon-align-right.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Link :</div>';
  html += '                      <div class="nexitorPopup-content-body-input">';
  html += '                      	<input type="text" name="text-link" placeholder="Please fill your link">';
  html += '                      </div>';
  html += '                  </div>';
  html += '              </div>';
  html += '              <div class="nexitorPopup-content-footer">';
  html += '              	<div>';
  html += '                  	<button type="button" class="btn btn-secondary btn-fw" onclick="closePopup( $(this) )">Cancel</button>';
  html += '                    <button type="button" class="btn btn-dark btn-fw updateButton" onclick="updateElement( $(this) )" data-element="">Done</button>';
  html += '                  </div>';
  html += '              </div>';
  html += '          </div>';
  html += '      </div>';
  html += '  </div>';
  // End popup image
  html += '  <div class="section-nexitorPopup nexitorPopup-video">';
  html += '  	<div class="nexitorPopup-bg"></div>';
  html += '  	<div class="nexitorPopup-container">';
  html += '      	<div class="nexitorPopup-close" onclick="closePopup( $(this) )"><img src="'+assets_path+'images/nexitor/icon-cancel.svg" class="svg"></div>';
  html += '      	<div class="nexitorPopup-content">';
  html += '          	<div class="nexitorPopup-content-head"><div class="nexitorPopup-content-headtitle"><img src="'+assets_path+'images/nexitor/icon-video.svg"> <h2>Video</h2></div></div>';
  html += '              <div class="nexitorPopup-content-body">';
  html += '              	<div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Width (%) :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-none active " data-tag="none"><img src="'+assets_path+'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-25" data-tag="w-25"><img src="'+assets_path+'images/nexitor/icon-width-25.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-50" data-tag="w-50"><img src="'+assets_path+'images/nexitor/icon-width-50.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-75" data-tag="w-75"><img src="'+assets_path+'images/nexitor/icon-width-75.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice width-choice width-w-100" data-tag="w-100"><img src="'+assets_path+'images/nexitor/icon-width-100.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Alignment :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-none active " data-tag="none"><img src="'+assets_path+'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-left" data-tag="text-left"><img src="'+assets_path+'images/nexitor/icon-align-left.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-center" data-tag="text-center"><img src="'+assets_path+'images/nexitor/icon-align-center.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-right" data-tag="text-right"><img src="'+assets_path+'images/nexitor/icon-align-right.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Ratio :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice ratio-choice ratio-16by9 active" data-tag="16by9"><img src="'+assets_path+'images/nexitor/icon-ratio-16-9.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice ratio-choice ratio-4by3" data-tag="4by3"><img src="'+assets_path+'images/nexitor/icon-ratio-4-3.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice ratio-choice ratio-1by1" data-tag="1by1"><img src="'+assets_path+'images/nexitor/icon-ratio-1-1.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '              </div>';
  html += '              <div class="nexitorPopup-content-footer">';
  html += '              	<div>';
  html += '                  	<button type="button" class="btn btn-secondary btn-fw" onclick="closePopup( $(this) )">Cancel</button>';
  html += '                    <button type="button" class="btn btn-dark btn-fw updateButton" onclick="updateElement( $(this) )" data-element="">Done</button>';
  html += '                  </div>';
  html += '              </div>';
  html += '          </div>';
  html += '      </div>';
  html += '  </div>';
  // End popup video
  html += '<div class="section-nexitorPopup nexitorPopup-button">';
  html += '  	<div class="nexitorPopup-bg"></div>';
  html += '  	<div class="nexitorPopup-container">';
  html += '      	<div class="nexitorPopup-close" onclick="closePopup( $(this) )"><img src="' + assets_path + 'images/nexitor/icon-cancel.svg" class="svg"></div>';
  html += '      	<div class="nexitorPopup-content">';
  html += '          	<div class="nexitorPopup-content-head"><div class="nexitorPopup-content-headtitle"><img src="' + assets_path + 'images/nexitor/icon-button.svg"> <h2>Button</h2></div></div>';
  html += '              <div class="nexitorPopup-content-body">';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Text Alignment :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-none active " data-tag="none"><img src="' + assets_path + 'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-left" data-tag="text-left"><img src="' + assets_path + 'images/nexitor/icon-align-left.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-center" data-tag="text-center"><img src="' + assets_path + 'images/nexitor/icon-align-center.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice align-choice align-text-right" data-tag="text-right"><img src="' + assets_path + 'images/nexitor/icon-align-right.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Text Color :</div>';
  html += '                      <div class="nexitorPopup-content-body-input">';
  html += '                      	<input type="text" name="button-text-color" placeholder="Please fill your text color">';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  html += '                      <div class="nexitorPopup-content-body-title">Backgroud Color :</div>';
  html += '                      <div class="nexitorPopup-content-body-input">';
  html += '                      	<input type="text" name="button-bg-color" placeholder="Please fill your bg color">';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  //html += '                      <div class="nexitorPopup-content-body-title">Display in-line :</div>';
  html += '                      <div class="nexitorPopup-content-body-input">';
  html += '                      	<div class="form-check form-check-flat form-check-default mb-0"><label class="form-check-label"><input type="checkbox" name="button-inline" class="form-check-input" onclick="$(\'.mobile-width-config\').toggle()"> Same Line <i class="input-helper"></i></label></div>';
  html += '                      </div>';
  html += '                  </div>';
  html += '              	    <div class="nexitorPopup-content-list mobile-width-config" style="display:none">';
  html += '                      <div class="nexitorPopup-content-body-title">Mobile width config :</div>';
  html += '                      <div class="nexitorPopup-content-body-choice">';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice button-col-choice button-col-none active " data-tag="button-col-none"><img src="' + assets_path + 'images/nexitor/icon-do-not.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice button-col-choice button-col-1" data-tag="button-col-1"><img src="' + assets_path + 'images/nexitor/icon-button-col-1.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice button-col-choice button-col-2" data-tag="button-col-2"><img src="' + assets_path + 'images/nexitor/icon-button-col-2.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice button-col-choice button-col-3" data-tag="button-col-3"><img src="' + assets_path + 'images/nexitor/icon-button-col-3.svg" class="svg"></a>';
  html += '                          <a href="javascript:void(0)" class="nexitor-choice button-col-choice button-col-4" data-tag="button-col-4"><img src="' + assets_path + 'images/nexitor/icon-button-col-4.svg" class="svg"></a>';
  html += '                      </div>';
  html += '                  </div>';
  html += '                  <div class="nexitorPopup-content-list">';
  //html += '                      <div class="nexitorPopup-content-body-title">Open in new tab :</div>';
  html += '                      <div class="nexitorPopup-content-body-input">';
  html += '                      	<div class="form-check form-check-flat form-check-default mb-0"><label class="form-check-label"><input type="checkbox" name="button-new-tab" class="form-check-input"> Is new tab <i class="input-helper"></i></label></div>';
  html += '                      </div>';
  html += '                  </div>';
  html += '              </div>';
  html += '              <div class="nexitorPopup-content-footer">';
  html += '              	<div>';
  html += '                  	<button type="button" class="btn btn-secondary btn-fw" onclick="closePopup( $(this) )">Cancel</button>';
  html += '                    <button type="button" class="btn btn-dark btn-fw updateButton" onclick="updateElement( $(this) )" data-element="">Done</button>';
  html += '                  </div>';
  html += '              </div>';
  html += '          </div>';
  html += '      </div>';
  html += '  </div>';
  //End popup button
  return html;
}
function nexitorpopup( $this ){
  generateSVG();
  var element = $this.attr('data-element');
  activeElementPopup = $this;
  $popup = $('.nexitorPopup-'+element);
  $popup.find('.updateButton').attr('data-element', element );
  $nexitor_body = $this.closest('.box-nexitor').find('.nexitor-body');
  if( element == "title" ){
    var htag = $nexitor_body.attr('data-htag');
    var align = $nexitor_body.attr('data-align');

    var bold = $nexitor_body.attr('data-bold');
    var italic = $nexitor_body.attr('data-italic');
    var underline = $nexitor_body.attr('data-underline');

    var link = $nexitor_body.attr('data-link');
    var color = $nexitor_body.attr('data-color');

    $popup.find('.htag-choice').removeClass('active');
    $popup.find('.htag-'+htag).addClass('active');

    $popup.find('.align-choice').removeClass('active');
    $popup.find('.align-'+align).addClass('active');

    if( bold == "" && italic == "" && underline == "" ){
      $popup.find('.style-choice').removeClass('active');
      $popup.find('.style-none').addClass('active');
    }else{
      $popup.find('.style-'+bold).addClass('active');
      $popup.find('.style-'+italic).addClass('active');
      $popup.find('.style-'+underline).addClass('active');
    }
    $popup.find('input[name="text-link"]').val(link);
    $popup.find('input[name="text-color"]').val(color);
  }else if( element == "content" ){
    //var content = $popup.find('iframe#text-content_ifr').contents().find('body').html();
    var content = $nexitor_body.find('.nexitor-textarea').html();
    $popup.find('iframe#text-content_ifr').contents().find('body').html( content );
    //$popup.find('.note-editable').html( content );
  }else if( element == "image" ){
    var width = $nexitor_body.attr('data-width');
    var align = $nexitor_body.attr('data-align');
    var link = $nexitor_body.attr('data-link');

    $popup.find('.width-choice').removeClass('active');
    $popup.find('.width-'+width).addClass('active');

    $popup.find('.align-choice').removeClass('active');
    $popup.find('.align-'+align).addClass('active');

    $popup.find('input[name="text-link"]').val(link);
  }else if( element == "video" ){
    var width = $nexitor_body.attr('data-width');
    var align = $nexitor_body.attr('data-align');
    var ratio = $nexitor_body.attr('data-ratio');

    $popup.find('.width-choice').removeClass('active');
    $popup.find('.width-'+width).addClass('active');

    $popup.find('.align-choice').removeClass('active');
    $popup.find('.align-'+align).addClass('active');

    $popup.find('.ratio-choice').removeClass('active');
    $popup.find('.ratio-'+ratio).addClass('active');
  } else if (element == "button") {
    var color = $nexitor_body.attr('data-color');
    var align = $nexitor_body.attr('data-align');
    var bg_color = $nexitor_body.attr('data-bg-color');
    var inline = $nexitor_body.attr('data-inline');
    var column = $nexitor_body.attr('data-column');
    var newtab = $nexitor_body.attr('data-newtab');

    $popup.find('.align-choice').removeClass('active');
    $popup.find('.align-' + align).addClass('active');

    $popup.find('.button-col-choice').removeClass('active');
    if (column == "") { column = "button-col-none" }
    $popup.find('.' + column).addClass('active');

    $popup.find('input[name="button-text-color"]').val(color);
    $popup.find('input[name="button-bg-color"]').val(bg_color);

    $popup.find('.mobile-width-config').hide();

    if ( inline == 1 ){
      $popup.find('input[name="button-inline"]').prop("checked", true );
      $popup.find('.mobile-width-config').show();
    }else{
      $popup.find('input[name="button-inline"]').prop("checked", false)
    }

    if (newtab == 1) {
      $popup.find('input[name="button-new-tab"]').prop("checked", true)
    } else {
      $popup.find('input[name="button-new-tab"]').prop("checked", false)
    }
  }
  $popup.show();
  setTimeout(function () {
    //$(".summernote-nex").summernote('code', '<p></p>');
    $(".summernote-nex").summernote('codeview.toggle');
    $(".summernote-nex").summernote('codeview.toggle');
  }, 100);
  //$(".summernote-nex").summernote('codeview', '<p></p>');
  
}
function updateElement( $this ){
  var element = $this.attr('data-element');
  $popup = $this.closest('.section-nexitorPopup');
  $boxNexitor = activeElementPopup.closest('.box-nexitor');
  if( element == "title" ){
    var htag = $popup.find('.htag-choice.active').attr('data-tag');
    var align = $popup.find('.align-choice.active').attr('data-tag');
    var bold = ( $popup.find('.style-font-weight-bold').hasClass('active') ) ? $popup.find('.style-font-weight-bold').attr('data-tag') : '';
    var italic = ( $popup.find('.style-font-italic').hasClass('active') ) ? $popup.find('.style-font-italic').attr('data-tag') : '';
    var underline = ( $popup.find('.style-font-underline').hasClass('active') ) ? $popup.find('.style-font-underline').attr('data-tag') : '';
    var link = $popup.find('input[name="text-link"]').val();
    var color = $popup.find('input[name="text-color"]').val();

    $boxNexitor.find('input[name="text-title"]').attr('class','');
    $boxNexitor.find('input[name="text-title"]').addClass(htag);
    $boxNexitor.find('input[name="text-title"]').addClass(align);
    $boxNexitor.find('input[name="text-title"]').addClass(bold);
    $boxNexitor.find('input[name="text-title"]').addClass(italic);
    $boxNexitor.find('input[name="text-title"]').addClass(underline);
    $boxNexitor.find('input[name="text-title"]').css({'color':color});
    if( color != "" ){
      //$boxNexitor.find('input[name="text-title"]').css({ 'color': color, '-webkit-text-fill-color' : 'inherit', 'background' : 'none' });
      $boxNexitor.find('input[name="text-title"]').css({ 'color': color, '-webkit-text-fill-color' : color, 'background' : 'none' });
    }

    $boxNexitor.find('.nexitor-body').attr('data-htag', htag );
    $boxNexitor.find('.nexitor-body').attr('data-align', align );
    $boxNexitor.find('.nexitor-body').attr('data-bold', bold );
    $boxNexitor.find('.nexitor-body').attr('data-italic', italic );
    $boxNexitor.find('.nexitor-body').attr('data-underline', underline );
    $boxNexitor.find('.nexitor-body').attr('data-link', link );
    $boxNexitor.find('.nexitor-body').attr('data-color', color );
    var html = "";
    html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-'+htag+'.svg" class="svg"></div>';
    if( align != "none" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-align-'+align+'.svg" class="svg"></div>';
    }
    if( link != "" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-link.svg" class="svg"></div>';
    }
    if( color != "" ){
      html += '<div class="nexitor-used-list" style="background-color:'+color+';"><img src="'+assets_path+'images/nexitor/icon-paint.svg" class="svg"></div>';
    }
	if( bold != "" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-bold.svg" class="svg"></div>';
    }
	if( italic != "" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-italic.svg" class="svg"></div>';
    }
	if( underline != "" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-underline.svg" class="svg"></div>';
    }
	
    $boxNexitor.find('.nexitor-used').html( html );

  }else if( element == "content" ){
    //var content = $popup.find('.note-editable').html();
    //var content = $popup.find('.tinyMce-nex').val();
    var content = $popup.find('iframe#text-content_ifr').contents().find('body').html()
    $boxNexitor.find('.nexitor-textarea').html( content );
  }else if( element == "image" ){
    var width = $popup.find('.width-choice.active').attr('data-tag');
    var align = $popup.find('.align-choice.active').attr('data-tag');
    var link = $popup.find('input[name="text-link"]').val();

    $boxNexitor.find('.nexitor-body').attr('data-width', width );
    $boxNexitor.find('.nexitor-body').attr('data-align', align );
    $boxNexitor.find('.nexitor-body').attr('data-link', link );

    var html = "";
    if( width != "none" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-width-'+width+'.svg" class="svg"></div>';
    }
    if( align != "none" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-align-'+align+'.svg" class="svg"></div>';
    }
    if( link != "" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-link.svg" class="svg"></div>';
    }

    $boxNexitor.find('.nexitor-used').html( html );

  }
  else if( element == "video" ){
    var width = $popup.find('.width-choice.active').attr('data-tag');
    var align = $popup.find('.align-choice.active').attr('data-tag');
    var ratio = $popup.find('.ratio-choice.active').attr('data-tag');

    $boxNexitor.find('.nexitor-body').attr('data-width', width );
    $boxNexitor.find('.nexitor-body').attr('data-align', align );
    $boxNexitor.find('.nexitor-body').attr('data-ratio', ratio );

    var html = "";
    if( width != "none" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-width-'+width+'.svg" class="svg"></div>';
    }
    if( align != "none" ){
      html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-align-'+align+'.svg" class="svg"></div>';
    }
    html += '<div class="nexitor-used-list"><img src="'+assets_path+'images/nexitor/icon-ratio-'+ratio+'.svg" class="svg"></div>';

    $boxNexitor.find('.nexitor-used').html( html );

  } else if (element == "button") {
    
    var align = $popup.find('.align-choice.active').attr('data-tag');
    var bg_color = $popup.find('input[name="button-bg-color"]').val();
    var color = $popup.find('input[name="button-text-color"]').val();
    var is_new_tab = $popup.find('input[name="button-new-tab"]').is(":checked") ? 1 : 0;
    var is_inline = $popup.find('input[name="button-inline"]').is(":checked") ? 1 : 0;
    var column = $popup.find('.button-col-choice.active').attr('data-tag');

    $boxNexitor.find('.nexitor-body').attr('data-color', color);
    $boxNexitor.find('.nexitor-body').attr('data-align', align);
    $boxNexitor.find('.nexitor-body').attr('data-bg-color', bg_color);
    $boxNexitor.find('.nexitor-body').attr('data-newtab', is_new_tab);
    $boxNexitor.find('.nexitor-body').attr('data-inline', is_inline);
    $boxNexitor.find('.nexitor-body').attr('data-column', column);

    var html = "";
    if (align != "none") {
      html += '<div class="nexitor-used-list"><img src="' + assets_path + 'images/nexitor/icon-align-' + align + '.svg" class="svg"></div>';
    }
    if (bg_color != "") {
      html += '<div class="nexitor-used-list" style="background-color:' + bg_color + ';"><img src="' + assets_path + 'images/nexitor/icon-title.svg" class="svg"></div>';
    }
    if (color != "") {
      html += '<div class="nexitor-used-list" style="background-color:' + color + ';"><img src="' + assets_path + 'images/nexitor/icon-paint.svg" class="svg"></div>';
    }
    if ( is_new_tab == 1 ){
      html += '<div class="nexitor-used-list"><img src="' + assets_path + 'images/nexitor/icon-newtab.svg" class="svg"></div>';
      
    }
    if (is_inline == 1) {
      html += '<div class="nexitor-used-list"><img src="' + assets_path + 'images/nexitor/icon-inline.svg" class="svg"></div>';
      if (column != "button-col-none") {
        html += '<div class="nexitor-used-list"><img src="' + assets_path + 'images/nexitor/icon-' + column + '.svg" class="svg"></div>';
      }
    }
    

    $boxNexitor.find('.nexitor-used').html(html);

  } 
  //$boxNexitor.find('input[name="text-title"]').val('Check This');


  generateSVG();
  $this.closest('.section-nexitorPopup').hide();
}
function activeFormElement($this){
  if ($this.hasClass('button-choice')) {
    $this.closest('.nexitor-body').find('.button-choice').removeClass('active');
    $this.addClass('active');
    $this.closest('.nexitor-body').find('.button-image-input').removeClass('d-none');
    $this.closest('.nexitor-body').find('.button-text-input').removeClass('d-none');
    if ($this.hasClass('button-text')) {
      $this.closest('.nexitor-body').find('.button-image-input').addClass('d-none');
    } else if ($this.hasClass('button-image')) {
      $this.closest('.nexitor-body').find('.button-text-input').addClass('d-none');
    }
  }
}

function nexitorClick( $this ){
  if( $this.hasClass('htag-choice') ){
    $('.htag-choice').removeClass('active');
    $this.addClass('active');
  }else if( $this.hasClass('style-choice') ){
    if( $this.hasClass('style-none') ){
      $('.style-choice').removeClass('active');
      $this.addClass('active');
    }else{
      $('.style-none').removeClass('active');
      $this.addClass('active');
    }
  }else if( $this.hasClass('align-choice') ){
    $('.align-choice').removeClass('active');
    $this.addClass('active');
  }else if( $this.hasClass('width-choice') ){
    $('.width-choice').removeClass('active');
    $this.addClass('active');
  }else if( $this.hasClass('ratio-choice') ){
    $('.ratio-choice').removeClass('active');
    $this.addClass('active');
  }else if ($this.hasClass('button-col-choice')) {
    $('.button-col-choice').removeClass('active');
    $this.addClass('active');
  } 
}
function closeContentRow( $this ){
  $this.closest('.content-row').remove();
}
function closeNexitor( $this ){
  $this.closest('.box-nexitor').remove();
}
function sortableRow(){
  $sortable = $( ".sortable-row" ).sortable({
    connectWith: ".sortable-row"
  });
  $sortable.disableSelection();
  $sortable.find(".ui-state-default").one("mouseenter",function(){
        $(this).addClass("sorting-initialize");
        $sortable.sortable('refresh');
    });
}

function sortableElement(){
  $( ".sortable-element" ).sortable({
    connectWith: ".sortable-element",
	create: function( event, ui ) {
		$(this).closest('.content-row').find('.add-element').addClass('hold-space');
		//$(this).closest('.content-row').find('.add-element').prepend('<span style="position:absolute; opacity:0; z-index:-1;">test</span>');
	},
	deactivate: function( event, ui ) {
		setTimeout(function () {
			//$('.add-element').removeClass('hold-space');
			//$('.nexitor-delete').remove();
		},0)
	},
  });
  $( ".sortable-element" ).disableSelection();
}


/* ================================================ */
/* SET SVG */
/* ================================================ */
generateSVG();
function generateSVG(){

    $('img.svg').each(function(){
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }
            $svg.attr('data-url', imgURL);
            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
}
