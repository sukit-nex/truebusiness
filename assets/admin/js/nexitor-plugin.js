// JavaScript Document
(function ($) {
    // Nexitor คือชื่อของ plugin ที่เราต้องการ ในที่นี้ ใช้ว่า  Nexitor
    $.fn.Nexitor = function (options) { // กำหนดให้ plugin ของเราสามารถ รับค่าเพิ่มเติมได้ มี options

        // ส่วนนี้ สำหรับกำหนดค่าเริ่มต้น
        var defaults = {
            foreground: "red", // กำหนดสีข้อความ
            background: "yellow", // กำหนดสีพื้นหลัง ในค่าเริ่มต้นเป็น สีเหลือง
            fontStyle: null, // การกำหนด font-style เช่น ตัวเอียง
            fontWeight: null,        //  font-weight เช่น กำหนด อักษรตัวหนา
            complete: function () { }    // กำหนด ฟังก์ชั่น object
        };

        // ส่วนสำหรับ  เป็นต้วแปร รับค่า options หากมี หรือใช้ค่าเริ่มต้น ถ้ากำหนด
        var settings = $.extend({}, defaults, options);

        /// คืนค่ากลับ การทำงานของ plugin
        return this.each(function () {
            $(this).css({
                "background-color": settings.background,
                "color": settings.foreground
            });
            // ตรวจสอบค่า settings.fontStyle ว่ามีการกำหนดค่าหรือไม่
            if (settings.fontStyle) {
                $(this).css('font-style', settings.fontStyle);
            }
            // ตรวจสอบค่า settings.fontWeight ว่ามีการกำหนดค่าหรือไม่
            if (settings.fontWeight) {
                $(this).css('font-weight', settings.fontWeight);
            }
            // ตรวจสอบว่า ค่าที่ส่งเข้ามา ฟังก์ชั่นหรือไม่  
            if ($.isFunction(settings.complete)) {
                settings.complete.call(this); // เรียกใช้ฟังก์ชั่นที่ส่งเข้ามา
            }
            // โค้ตสำหรับ การทำงานของ plugin
        });

    };

})(jQuery);