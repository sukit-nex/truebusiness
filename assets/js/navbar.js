
function backToTop() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
function listener() {
  var mq = window.matchMedia("(min-width: 768px)");
  var heightNavMain = $(".nav-mainmenu").outerHeight(false);
  var heightNavTop = $(".nav-main-top").outerHeight(false);
  // $('.nav-mainmenu').removeAttr('style');
  // $('.slick-main-banner').removeAttr('style');

  // $(".slick-main-banner").css({ "padding-top": heightNavMain });
  // $(".nav-mainmenu").css({ "margin-top": 0 });
  // if (mq.matches) {
   
  //   $(".nav-mainmenu").css({ "margin-top": heightNavTop });
  //   $(".slick-main-banner").css({ "padding-top": heightNavMain + heightNavTop });
  // }
  $(".nav-mainmenu").css({ "margin-top": heightNavTop });
    $(".slick-main-banner").css({ "padding-top": heightNavMain + heightNavTop });
}

$(window).scroll(listener);
$(window).resize(listener);

$(document).ready(function () {
  listener();

  $(".navbar-toggler").on('click', function () {
    $(".animated-hamburger").toggleClass("open");
  });

});



var myCollapsible = document.getElementById('navbarsMainMenu');

myCollapsible.addEventListener('show.bs.collapse', function () {
  var heightNavMain = $(".nav-mainmenu").outerHeight(false);
  $('.nav-mainmenu .menu').css("height", "100vh");
  $('.nav-mainmenu .menu').css("paddingBottom", heightNavMain);
  $('.nav-mainmenu .menu').css("backgroundColor", "#fffffff5");
  $('body').addClass("overflow-hidden");

})

myCollapsible.addEventListener('hide.bs.collapse', function () {
  $('body').removeClass("overflow-hidden");
})