$(document).ready(function() {
	$('.st-banner').each(function() {
		var st_banner = new Swiper($(this).find('.swiper-container'), {
			navigation: {
				nextEl: $(this).find('.swiper-button-next'),
				prevEl: $(this).find('.swiper-button-prev'),
			},
			spaceBetween: 0,
			slidesPerView: 1,
			loop: false,
			speed: 800,
			simulateTouch: false,
			breakpoints: {
				991: {
					slidesPerView: 1,
					simulateTouch: true,
				},
			},
		});
	});
	
	$('.st-list').each(function() {
		var st_list_col = $(this).attr('data-slidecol');
		
		if ( $(this).find('.swiper-container').closest('.st-list').length == 1 ) {
			var st_list = new Swiper($(this).find('.swiper-container'), {
				navigation: {
					nextEl: $(this).find('.swiper-button-next'),
					prevEl: $(this).find('.swiper-button-prev'),
				},
				spaceBetween: 30,
				slidesPerView: st_list_col,
				loop: false,
				speed: 800,
				simulateTouch: false,
				breakpoints: {
					991: {
						slidesPerView: 1,
						simulateTouch: true,
					},
				},
			});
		}
	});
	
	$('.zscroll-blank').css({ height : $(window).height() });
	$('.st-zscroll .cont-contain').css({ height : $(window).height() });
	
	
	var controller_z = $.superscrollorama();

	$('.st-zscroll').each(function() {
		controller_z.pin($(this).find('.sec-zscroll'), 3000, {
			anim: ( new TimelineLite() )
			.append(
				TweenMax.fromTo($(this).find('.slide-z-1 .slide-z-img,.slide-z-1 .slide-z-txt'), .75, 
					{css:{transform:'scale(0.3)',opacity:0}, immediateRender:true}, 
					{css:{transform:'scale(0.9)',opacity:1}}),-1.5 // offset for better timing
			)
			.append(
				TweenMax.to($(this).find('.slide-z-1 .slide-z-img,.slide-z-1 .slide-z-txt'), 2, 
					{css:{transform:'scale(1)',opacity:1}})
			)
			.append(
				TweenMax.to($(this).find('.slide-z-1 .slide-z-img,.slide-z-1 .slide-z-txt'), 1, 
					{css:{transform:'scale(2)',opacity:0}})
			)
			.append(
				TweenMax.fromTo($(this).find('.slide-z-2 .slide-z-img,.slide-z-2 .slide-z-txt'), .75, 
					{css:{transform:'scale(0.3)',opacity:0}}, 
					{css:{transform:'scale(0.9)',opacity:1}}),0 // offset for better timing
			)
			.append(
				TweenMax.to($(this).find('.slide-z-2 .slide-z-img,.slide-z-2 .slide-z-txt'), 2, 
					{css:{transform:'scale(1)',opacity:1}})
			)
			.append(
				TweenMax.to($(this).find('.slide-z-2 .slide-z-img,.slide-z-2 .slide-z-txt'), 1, 
					{css:{transform:'scale(2)',opacity:0}})
			)
			.append(
				TweenMax.fromTo($(this).find('.slide-z-3 .slide-z-img,.slide-z-3 .slide-z-txt'), .75, 
					{css:{transform:'scale(0.3)',opacity:0}}, 
					{css:{transform:'scale(0.9)',opacity:1}}),0 // offset for better timing
			)
			.append(
				TweenMax.to($(this).find('.slide-z-3 .slide-z-img,.slide-z-3 .slide-z-txt'), 2, 
					{css:{transform:'scale(1)',opacity:1}})
			),
			// onPin: function(){
			// 	console.log(1);
			// 	$('.superscrollorama-pin-spacer').css({minHeight:'3889px!important'});
			// },
			// onUnpin: function(){
			// 	console.log(2);
			// 	$('.superscrollorama-pin-spacer').css({height:'3000px!important'});
			// },
		});
	});
	
});