var tl = new TimelineLite();
var st_banner;
var slideOnStop = 1;
var time_update_interval;
$(window).load(function(){
	genHeight();
	$('.loadpage').fadeOut();
	setTimeout(function(){ checkResize(); },10)
	
	if($('.ul-menu > li > .m-menu.active').hasClass('has-sub')){
		// $('body').addClass('menuClick')
		$('body').addClass('headfull')
	}
	if ($('.sidebar').lenght > 0) {
		loadSideBar();
	}

	listHighligh();
	swiperSlide();

	var limitPage = $('.btn-loadmore').attr('data-limit-page');
	if(limitPage == 1){
		$('.box-loadmore').hide();
	}
	if(limitPage == ''){
		$('.box-loadmore').hide();
	}
	genSlide();

	
	var sidebar = new StickySidebar('#sidebar', {
		containerSelector: '.content-inner',
		innerWrapperSelector: '.sidebar__inner',
		topSpacing: 317,
		bottomSpacing: 0
	});
	console.log(sidebar);

});
$(document).ready(function() {
	
	$('.box-jumpto').each(function() {
		$(this).find('.jumpto-item.jumpto-item-0 .btn-jump').addClass('active');
	});
	
	$('.box-smehighlight .smehighlight-list').hide();
	$('.box-smehighlight .smehighlight-list:first').show();

	$('.jumpto-item a').click(function(){
		var type_button = $(this).closest('.box-jumpto').attr('data-function')

		var refElement = $(this).attr("data-link") ;
		$(this).closest('.jumpto-group').removeClass('bg-gdcolor-orange bg-gdcolor-blue bg-gdcolor-pink bg-gdcolor-darkblue bg-gdcolor-red');
		if(refElement == 1){ $(this).closest('.jumpto-group').addClass('bg-gdcolor-orange')}
		if(refElement == 2){ $(this).closest('.jumpto-group').addClass('bg-gdcolor-blue')}
		if(refElement == 3){ $(this).closest('.jumpto-group').addClass('bg-gdcolor-pink')}
		if(refElement == 4){ $(this).closest('.jumpto-group').addClass('bg-gdcolor-darkblue')}
		if(refElement == 5){ $(this).closest('.jumpto-group').addClass('bg-gdcolor-red')}

		$(this).closest('.box-jumpto').find('.jumpto-item .btn-jump.active').removeClass('active');
		if(!$(this).hasClass('active')){
			$(this).addClass('active');
		}

		if( type_button == 'tabs'){
			$('.box-smehighlight .smehighlight-list').hide();
			$('.smehighlight-list-'+refElement).fadeIn();
			genSlide();
		}else if( type_button == 'scroll'){
			$('html, body').animate({
				scrollTop: $("#smunu-"+refElement).offset().top - $('.section-header').outerHeight()
			}, 1000);
		}else if( type_button == 'link-out'){

		}
	});

	//sec-smehighlight
	// setTimeout(function(){
	// 	var highlight_slide = $('.box-smehighlight').attr('data-slide');
	// 	$('.jumpto-item-'+(highlight_slide++)+' a').trigger( "click" );
	// 	console.log('test','.smehighlight-list-'+(highlight_slide++));
	
	// function auto_nextab(){
	// 	setTimeout(function(){
	// 		auto_nextab()
	// 	},2000);
	// }


	////////////////////////////
	//////// g-feature /////////
	///////////////////////////
	$('.g-feature-main .icon-toggle').each(function(){
        $(this).parent().addClass('has-toggle');
    });
    $('.g-feature.active').each(function(){
        $(this).find('.g-feature-toggle').slideDown();
    });
    $('.g-offer-topic').each(function(){
        if(!$(this).hasClass('active')){
            $(this).parent().find('.g-feature').slideUp();
        }
   });
    $('.feature-list .g-feature-main').click(function(){
        if($(this).find('div').hasClass('icon-toggle')){
            if(!$(this).closest('.g-feature').hasClass('active')){
                $(this).closest('.tablepack-body').find('.g-feature').removeClass('active');
                $(this).closest('.tablepack-body').find('.g-feature-toggle').slideUp();
                console.log('')
            }
            $(this).closest('.g-feature').toggleClass('active');
            $(this).closest('.g-feature').find('.g-feature-toggle').slideToggle();
        }
    });
    $('.feature-list .g-offer-topic').click(function(){
        if(!$(this).hasClass('active')){
            $(this).closest('.tablepack-body').find('.g-offer-topic').removeClass('active');
            $(this).closest('.tablepack-body').find('.g-offer-topic').parent().find('.g-feature').slideUp();
        }
        $(this).toggleClass('active');
        $(this).parent().find('.g-feature').slideToggle();
    });


	var smedetail_parent = $('.sec-smedetail').attr('data-parent');
	var smedetail_child = $('.sec-smedetail').attr('data-child');

	if(smedetail_child == "") {
		$('.smedetail-tab-list li:first a').addClass('active');
		$('.box-smedetail-listall .box-smedetail-list:first').show();
	}else{
		$('.smedetail-tab-list li a').removeClass('active');
		$('.smedetail-tab-list li a').each(function(){
			if($(this).attr('data-child') == smedetail_child){
				$(this).addClass('active')
			}
		});
		$('.box-smedetail-list').each(function(){
			if($(this).attr('data-child') == smedetail_child){
				$(this).show();
			}
		});
	}
	$('.smedetail-tab-list li a').click(function(){
		var tab_child = $(this).attr('data-child');
		$('.smedetail-tab-list li a').removeClass('active');
		$(this).addClass('active') 

		$('.box-smedetail-list').hide();
		$('.box-smedetail-list').each(function(){
			if($(this).attr('data-child') == tab_child){
				$(this).show();
			}
		});
		genSlide();
	});

	
	$('.jumpto-item a').each(function(){
		console.log($(this).attr('data-parent'),smedetail_parent);
		$(this).removeClass('active');
		if( typeof($(this).attr('data-parent')) !== "undefined" && $(this).attr('data-parent') == smedetail_parent){
			$(this).addClass('active');
		}
	});

	$('.jumpto-group').removeClass('bg-gdcolor-orange bg-gdcolor-blue bg-gdcolor-pink bg-gdcolor-darkblue bg-gdcolor-red');
	if(smedetail_parent == 'mobile'){ $('.jumpto-group').addClass('bg-gdcolor-orange')}
	else if(smedetail_parent == 'internet'){ $('.jumpto-group').addClass('bg-gdcolor-blue')}
	else if(smedetail_parent == 'tv'){ $('.jumpto-group').addClass('bg-gdcolor-pink')}
	else if(smedetail_parent == 'fixedline'){ $('.jumpto-group').addClass('bg-gdcolor-darkblue')}
	else if(smedetail_parent == 'value-set'){ $('.jumpto-group').addClass('bg-gdcolor-red')}
	else { $('.jumpto-group').addClass('bg-gdcolor-orange'); $('.jumpto-item:first a').addClass('active'); }
	

	//$('.sec-smedetail')

});
$(window).resize( function(){
	checkResize();
	addCustomStyle();
	rowPerColum();
	addCustomMg();
	addCustomPd();
	listHighligh();

});
addCustomStyle();
rowPerColum();
addCustomPd();
addCustomMg();
listHighligh();




		

function checkResize(){
    var winW = $(window).width();
    if(winW >= 1600 && winW <= 1920){
        genHeight('1920');
    }else if(winW >= 1440 && winW < 1600 ){
        genHeight('1600');
    }else if(winW >= 1200 && winW < 1440 ){
        genHeight('1440');
    }else if(winW >= 992 && winW < 1200 ){
		genHeight(1200);
	}else if(winW >= 768 && winW < 992){
		genHeight('992');
	}else if(winW < 768 ){
		genHeight('');
	}else if(winW > 1920 ){
		genHeight('2240');
	}
	
	$('.st-list .sec-lists').each(function() {
		var btn_swiper_this = $(this);
		setTimeout(function(){
			var thumb_height = btn_swiper_this.find('.thumb .thumb-img').outerHeight()/2;
			var arrow_height = btn_swiper_this.find('.swiper-button').outerHeight()/2;
			if(thumb_height != 0){
				// console.log(thumb_height + " - " + arrow_height);
				btn_swiper_this.find('.swiper-button').css({ top : thumb_height - arrow_height });
			}else{
				// console.log(thumb_height + " - " + arrow_height);
				btn_swiper_this.find('.swiper-button').css({ top : '50%', transform:'translateY(-50%)' });
			}
			
		},50)
	});

	$('.wrapper').css({paddingTop:$('.nav-main-top').outerHeight() + $('.nav-mainmenu').outerHeight()})
}

function listHighligh(){
	var winW = $(window).width();
	if( winW > 767 ){
		if ($('.list-highligh ').length > 0) {
			$('.list-highligh').addClass('thumb-hoz');
			$('.list-highligh').removeClass('thumb-ver');
		}
	}else{
		$('.list-highligh').addClass('thumb-ver');
		$('.list-highligh').removeClass('thumb-hoz');
	}
} 

  

function addCustomStyle(){
	var winW = $(window).width();

	if( winW > 575 ){
		if ($('.gen_bg').length > 0) {
			$('.gen_bg').each(function(index, value){
				var bg_desktop = $(this).attr('data-bg-desktop');
				var bg_color = $(this).attr('data-bg-color');
				
				var style = '';
				if( bg_desktop != ""){
					style += 'background-image:url( /uploads/'+bg_desktop+');';
				}
				if(bg_color != ""){
					style += 'background-color:'+bg_color+';';
				}


				$(this).attr('style',style);
			});
		}
	}else{
		if ($('.gen_bg').length > 0) {
			$('.gen_bg').each(function(index, value){
				var bg_mobile = $(this).attr('data-bg-mobile');
				var bg_color = $(this).attr('data-bg-color');

				var style = '';
				if( bg_mobile != ""){
					style += 'background-image:url( /uploads/'+bg_mobile+');';
				}
				if(bg_color != ""){
					style += 'background-color:'+bg_color+';';
				}

				$(this).attr('style',style);
			});
		}
	}
	
}
function addCustomPd(){
	var winW = $(window).width();
	// console.log('in addCustomPd');
	if( winW > 767 ){
		if ($('.gen_padding').length > 0) {
			$('.gen_padding').each(function(index, value){
				var pd_desktop = $(this).attr('data-pd-desktop');
				
				var style = '';
				if( pd_desktop != ""){
					style += pd_desktop;
				}

				$(this).attr('style',style);
			});
		}
	}else{
		if ($('.gen_padding').length > 0) {
			$('.gen_padding').each(function(index, value){
				var pd_mobile = $(this).attr('data-pd-mobile');

				var style = '';
				if( pd_mobile != ""){
					style += pd_mobile;
				}

				$(this).attr('style',style);
			});
		}
	}
	
}

function addCustomMg(){
	var winW = $(window).width();
	
	if( winW > 767 ){
		if ($('.gen_margin').length > 0) {
			$('.gen_margin').each(function(index, value){
				var mg_desktop = $(this).attr('data-mg-desktop');
				
				var style = '';
				if( mg_desktop != ""){
				style += mg_desktop;
				}

				$(this).attr('style',style);
			});
		}
	}else{
		if ($('.gen_margin').length > 0) {
			$('.gen_margin').each(function(index, value){
				var mg_mobile = $(this).attr('data-mg-mobile');

				var style = '';
				if( mg_mobile != ""){
				style += mg_mobile;
				}

				$(this).attr('style',style);
			});
		}
	}
	
}

function rowPerColum(){
	var winW = $(window).width();

	if( winW >= 1200 ){
		if ($('.row_cols').length > 0) {
			$('.row_cols').each(function(index, value){
				var per_colum_desktop = $(this).attr('data-row-cols-desktop');
				var row = 'row row_cols row';
				row += '-'+'cols'+'-'+per_colum_desktop;
				// console.log(row);
				$(this).attr('class',row);
			});
		}
	} 
	else if(winW < 1200 && winW > 992){
		if ($('.row_cols').length > 0) {
			$('.row_cols').each(function(index, value){
				var per_colum_desktop = $(this).attr('data-row-cols-desktop');
				var type_thumb = $(this).attr('data-row-cols-check');
				var row = 'row row_cols row';
				var st_row_list_1200 ="";
	
					if(type_thumb != 'horizontal'){
						if( per_colum_desktop == 6 ){
							st_row_list_1200 = 4;
						}else if( per_colum_desktop == 5 ){
							st_row_list_1200 = 3;
						}else if( per_colum_desktop == 4 ){
							st_row_list_1200 = 3;
						}else if( per_colum_desktop == 3 ){
							st_row_list_1200 = 2;
						}else if( per_colum_desktop == 2 ){
							st_row_list_1200 = 2;
						}
					}else if(type_thumb == 'horizontal'){
						if( per_colum_desktop == 6 ){
							st_row_list_1200 = 4;
						}else if( per_colum_desktop == 5 ){
							st_row_list_1200 = 3;
						}else if( per_colum_desktop == 4 ){
							st_row_list_1200 = 3;
						}else if( per_colum_desktop == 3 ){
							st_row_list_1200 = 2;
							st_row_list_991 = 2;
							st_row_list_768 = 2;
						}else if( per_colum_desktop == 2 ){
							st_row_list_1200 = 2;
						}
					}

				row += '-'+'cols'+'-'+st_row_list_1200;
				$(this).attr('class',row);
			});
		}
	}else if(winW < 992 && winW >= 768){
		if ($('.row_cols').length > 0) {
			$('.row_cols').each(function(index, value){
				var per_colum_desktop = $(this).attr('data-row-cols-desktop');
				var type_thumb = $(this).attr('data-row-cols-check');
				var row = 'row row_cols row';
				var st_row_list_991 ="";
	
				if(type_thumb != 'horizontal'){
					if( per_colum_desktop == 6 ){
						st_row_list_991 = 3;
					}else if( per_colum_desktop == 5 ){
						st_row_list_991 = 3;
					}else if( per_colum_desktop == 4 ){
						st_row_list_991 = 3;
					}else if( per_colum_desktop == 3 ){
						st_row_list_991 = 2;
					}else if( per_colum_desktop == 2 ){
						st_row_list_991 = 2;
					}
				}else if(type_thumb == 'horizontal'){
					st_row_list_991 = 1;
				}

				row += '-'+'cols'+'-'+st_row_list_991;
				$(this).attr('class',row);
			});
		}
		
		//  console.log('992',row);
	}else if(winW <  768 && winW > 576){
		if ($('.row_cols').length > 0) {
			$('.row_cols').each(function(index, value){
				var per_colum_desktop = $(this).attr('data-row-cols-desktop');
				var type_thumb = $(this).attr('data-row-cols-check');
				var row = 'row row_cols row';
				var st_row_list_768 ="";
				if(type_thumb != 'horizontal'){
					if( per_colum_desktop == 6 ){
						st_row_list_768 = 2;
					}else if( per_colum_desktop == 5 ){
						st_row_list_768 = 2;
					}else if( per_colum_desktop == 4 ){
						st_row_list_768 = 2;
					}else if( per_colum_desktop == 3 ){
						st_row_list_768 = 2;
					}else if( per_colum_desktop == 2 ){
						st_row_list_768 = 1;
					}
				}else if(type_thumb == 'horizontal'){
					st_row_list_768 = 1;
				}

				row += '-'+'cols'+'-'+st_row_list_768;
				$(this).attr('class',row);
			});
		}
	}
	else if(winW <= 576){
		if ($('.row_cols').length > 0) {
			$('.row_cols').each(function(index, value){
				var per_colum_mobile = $(this).attr('data-row-cols-mobile');
				var row = 'row row_cols row';
				row += '-'+'cols'+'-'+per_colum_mobile;
				// console.log(row);
				$(this).attr('class',row);
			});
		}
	}

}

////////////////////////////////////////////////////
//================= VIDEO CONTROL
////////////////////////////////////////////////////
function genHeight(widthCheckX){
	// Generate Height
	//console.log(widthCheckX);
	
	var winW = $(window).width();
	var source = "";
	var storeWidthCheck = widthCheckX;
	
	if ($('.gen-height').length > 0 ) {
		$('.gen-height').each(function (index, value) {
			
			var self_width = $(this).outerWidth(); 
			var widthCheck = storeWidthCheck
			data_column = $(this).attr('data-column');
			var ar_gen = [];
			ar_gen[index] = $(this);
			if( winW > 575 ){
				source = $(this).attr('data-image-desktop');
			}else{
				source = $(this).attr('data-image-mobile');
			}

			// console.log(source+'widthCheckX: '+widthCheckX);
			// console.log(source+'storeWidthCheck: '+storeWidthCheck);
			var column = data_column;
			var source2 = source;
			// console.log(source+': '+data_column);
			
			var height = "";
			if (source != "") {
				var ar_img = [];
				ar_img[index] = new Image();
				ar_img[index].onload = function () {


					window_width = $(window).width() / column;
					widthCheck = widthCheck / column;

					height = this.height;
					width = this.width;

					if(column > 1){
						height = (self_width * height) / width;
					}else{
						if(widthCheck == ""){
							height = (window_width * height) / width;
						}else{
							height = (widthCheck * height) / width;
						}
					}

					
					
					height = height + 'px';



					if( ar_gen[index].closest('.section').hasClass('st-banner') ){
						ar_gen[index].css({ height: height });
					}else{
						ar_gen[index].css({ minHeight: height });
					}
					
				}
				ar_img[index].src = source;
			}
		});
	}
	
}



/* ================================================ */
/* POP UP */
/* ================================================ */


function popin(popin_name,$this,){
	console.log($this);
	var content_on_popup = $this.next().html();
	if(popin_name == 'popup-content'){
		var html_popup = 
		'	<div class="global-popup section-popup '+popin_name+'">'
		+'		<div class="popup-bg popout" onclick="popout(\''+popin_name+'\')" data-popup="popup-content"></div>'
		+'		<div class="popup-container">'
		+'			<div class="popup-close popout" onclick="popout(\''+popin_name+'\')" data-popup="popup-content"><span></span><img src="/assets/img/skin/icon-close.svg" class="svg"></div>'
		+'				<div class="box-popup">'
		//+'				<div class="popup-head"><h4></h4></div>'
		+'			    <div class="popup-body scroll">'
		+'					<div class="box-content">'+content_on_popup+'</div>'
		+'				</div>'
		+'			</div>'
		+'		</div>'
		+'	</div>'
	}else if (popin_name == 'popup-form'){
		content_on_popup = $('.content_on_popup').html()
		var packageName = $this.attr('data-packageName');
		var formTopic = $this.attr('data-formTopic');
		var html_popup = 
		'	<div class="global-popup section-popup '+popin_name+'">'
		+'		<div class="popup-bg popout" onclick="popout(\''+popin_name+'\')" data-popup="popup-content"></div>'
		+'		<div class="popup-container">'
		+'			<div class="popup-close popout" onclick="popout(\''+popin_name+'\')" data-popup="popup-content"><span></span><img src="/assets/img/skin/icon-close.svg" class="svg"></div>'
		+'				<div class="box-popup">'
		+'				<div class="popup-head text-center"><div class="popup-topic"><b class="txt-gdcolor">'+formTopic+'</b></div><div class="popup-subtopic"><span><b>'+packageName+'</b></span></div></div>'
		+'			    <div class="popup-body scroll">'
		+'					<div class="box-content">'+content_on_popup+'</div>'
		+'				</div>'
		+'			</div>'
		+'		</div>'
		+'	</div>'
	}else if (popin_name == 'popup-alert'){
		var html_popup = 
		'	<div class="global-popup section-popup '+popin_name+'">'
		+'		<div class="popup-bg popout" onclick="popout(\''+popin_name+'\')" data-popup="popup-content"></div>'
		+'		<div class="popup-container">'
		+'			<div class="popup-close popout" onclick="popout(\''+popin_name+'\')" data-popup="popup-content"><span></span><img src="/assets/img/skin/icon-close.svg" class="svg"></div>'
		+'				<div class="box-popup">'
		+'				<div class="popup-head text-center"><h4></h4></div>'
		+'			    <div class="popup-body scroll">'
		+'					<div class="box-content">'+content_on_popup+'</div>'
		+'				</div>'
		+'			</div>'
		+'		</div>'
		+'	</div>'
	}


	$('body').append(html_popup);
	svg();
	
	$('section-popup').fadeOut();
	$('.'+popin_name).fadeIn();
	$('body').addClass('overflow-hidden');

	var height_popup_body = $('.'+popin_name+' .popup-container').height() - $('.'+popin_name+' .popup-container .popup-head').height(); 
	$('.'+popin_name+' .popup-body.scroll').css({height:height_popup_body});
}
function popout(popout_name){
	$('body').removeClass('overflow-hidden');
	$('.'+popout_name).fadeOut(function() {
		$('.section-popup').remove()
	});
}


$(document).ready(function() {

	//if ('scrollRestoration' in history) { history.scrollRestoration = 'manual'; }
  	//window.scrollTo(0,0);


	//$('.is-btn-area-filter').first().trigger('click');

	$('.file-input__input').on('change', function() {
		var fileName = $(this)[0].files[0].name;    
		// console.log($(this).closest('.form-group').find('.placeholder'));
		console.log($(this)[0].name);
		$(this).closest('.form-group').find('.placeholder').text(fileName);
	});

	var playerBanner;
	var dataVideo_banner;
	
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	
	/*$('.video-play').click(function(){
		dataVideo_banner = $(this).attr('data-video');
		$('.box-bannervideo').fadeIn();
		playVideo( dataVideo_banner );
	});*/
	
	$('.banner-closeVideo').click(function(){
		closeVideo();
	});
	
	
	var player_ganeral;
	var dataVideo_general;

	////////////////////////////////////////////////////
	//================= BUTTON MENU
	////////////////////////////////////////////////////
	$('.btn-menu').click(function(){
		if($('body').hasClass('menuClick')){
			// console.log('has class menuclick');
			$('.box-menu').slideUp();
			$('body').removeClass('menuClick');
		}else{
			// console.log('no class class menuclick');
			$('.box-menu').slideDown();
			$('body').addClass('menuClick');
		}
	});

	// $('.m-menu.m-parent.has-sub').click(function(){
	// 	if($('.m-menu.m-parent.has-sub').hasClass('active')){
	// 		$(this).parent().find('.sub-nav.sub-menu').slideUp();
	// 		$(this).removeClass('active');
	// 		console.log('close');
	// 	}else{
	// 		console.log('open');
	// 		$(this).addClass('active');
	// 		$(this).parent().find('.sub-nav.sub-menu').slideDown();
	// 	}
	// });

	$('.footer-menu-title').click(function(){
		if( $(this).hasClass('active') ){
			$(this).parent().find('.footer-list').slideUp();
			$(this).removeClass('active');
		}else{
			$(this).parent().find('.footer-list').slideDown();
			$(this).addClass('active');
		}
	});
	
});
$(window).load(function(){
	var html_scroll_down = 
	'	<div class="tablepack-scrollicon">'
	+'		<div class="tablepack-scrollicon-group">'
	+'			<img src="/assets/img/skin/icon-arrow-bottom.svg">'
	+'			<span>Scroll Down<span>'
	+'		</div>'
	+'	</div>';

	$('.box-smehighlight .tablepack-list .tablepack-body').each(function(){

	//var heightCSS = $(this).css("height");
	//console.log($(this).height())
	if($(this).height() > 227){
		$(this).addClass('pack-height-over');
		$(this).prepend(html_scroll_down);
	}
	});
	$('.tablepack-scrollicon').hover(function(){
		$(this).fadeOut();
	});
});
/* ================================================ */
/* SET SVG */
/* ================================================ */
svg();
function svg(){

    $('img.svg').each(function(){
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
}



$(document).ready(function() {
	if( $('.st-banner').length > 0 ){
		$('.st-banner').each(function() {
		st_banner = new Swiper($(this).find('.swiper-container'), {
				navigation: {
					nextEl: $(this).find('.swiper-button-next'),
					prevEl: $(this).find('.swiper-button-prev'),
				},
				pagination: {
					el: $(this).find('.swiper-pagination'),
					clickable: true,
				  },
				spaceBetween: 0,
				slidesPerView: 1,
				loop:false,
				autoplay: {
					delay: 5000,
					disableOnInteraction: true,
				},
				speed: 800,
				simulateTouch: false,
				breakpoints: {
					991: {
						slidesPerView: 1,
						simulateTouch: true,
					},
				}
				
			});

			st_banner = document.querySelector('.swiper-container').swiper
			$(".st-banner .swiper-container").on('mouseenter', function(e){
				if(slideOnStop == 1){
					st_banner.autoplay.stop();
				}
				
				
			})
			$(".st-banner .swiper-container").on('mouseleave', function(e){
				if(slideOnStop == 1){
					st_banner.autoplay.start();
				}
			})

			// st_banner.on('slideChange', function () {
			// 	// clickToPlaySlide = 1;
			// 	  $('.swiper-slide').each(function () {
			// 		var youtubePlayer = $(this).find('iframe').get(0);
			// 		if (youtubePlayer) {
			// 			// youtubePlayer.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
			// 			slideOnStop = 1;
			// 		}
			// 	  });
			// 		console.log('slide:',slideOnStop);
			//  });

			st_banner.on('slideChange', function () {
					slideOnStop = 1;
					console.log('slide:',slideOnStop);
			 });
		});
	}

	
	if( $('.sec-smepack-slide').length > 0 ){
		$('.sec-smepack-slide').each(function() {
			smePackSide = new Swiper($(this).find('.swiper-container'), {
				navigation: {
					nextEl: $(this).find('.swiper-button-next'),
					prevEl: $(this).find('.swiper-button-prev'),
				},
				pagination: {
					el: $(this).find('.swiper-pagination'),
					clickable: true,
				  },
				spaceBetween: 20,
				slidesPerView: 3,
				loop:false,
				speed: 800,
				simulateTouch: false,
			});
		});
	}

	if( $('.sec-smepack-list').length > 0 ){
		$('.sec-smepack-list').each(function() {
			smePackSide = new Swiper($(this).find('.swiper-container'), {
				navigation: {
					nextEl: $(this).find('.swiper-button-next'),
					prevEl: $(this).find('.swiper-button-prev'),
				},
				pagination: {
					el: $(this).find('.swiper-pagination'),
					clickable: true,
				  },
				spaceBetween: 20,
				slidesPerView: 3,
				loop:false,
				speed: 800,
				simulateTouch: false,
			});
		});
	}
	
	
	$('.cate-show').click(function(){
		$('.cate-select').slideToggle();

	});

	$('.cate-select li').click(function(){
		var content_select = $(this).html();
		$('.cate-show').html(content_select);
		$('.cate-select').slideUp();
	});

	$('.tab').hide();
	$('.tab').not(':first-child').show();
	
	$('.is-tab').click(function() {
		var tab_number = $(this).attr('data-tab');
		$(this).closest('.tab-wrapper').find('.tab').hide();
		$(this).closest('.tab-wrapper').find('.is-tab').removeClass('active');
		$(this).closest('.tab-wrapper').find('.tab_'+tab_number).fadeIn();
		$(this).addClass('active');	
	});

	$('.is-btn-area-filter').click(function() {
		$('.is-btn-area-filter').removeClass('active');
		$(this).addClass('active');	
	});
	////////////////////////////////////////////////////
	//================= ACCORDION
	////////////////////////////////////////////////////	
	$('.box-accordion').each(function() {
		$(this).find('.accor-list-0:nth-of-type(1)').addClass('active');
		$(this).find('.accor-list-0:nth-of-type(1) .accor-body').slideDown();
	});
	$('.accor-head').click(function(){
		if($(this).closest('.accor-list').hasClass('active')){
			$(this).closest('.accor-list').removeClass('active');
			$(this).closest('.accor-list').find('.accor-body').slideUp();
		}else{
			$(this).closest('.box-accordion').find('.accor-list').removeClass('active');
			$(this).closest('.box-accordion').find('.accor-body').slideUp();
			$(this).closest('.accor-list').addClass('active');
			$(this).closest('.accor-list').find('.accor-body').slideDown();
		}
	});
	

	$('.box-tablepack').each(function() {
		$(this).find('.tablepack-list-0:nth-of-type(1)').addClass('active');
		$(this).find('.tablepack-list-0:nth-of-type(1) .tablepack-body').slideDown();
	});
	$('.tablepack-head').click(function(){
		if($(window).width() < 768){
			if($(this).closest('.tablepack-list').hasClass('active')){
				$(this).closest('.tablepack-list').removeClass('active');
				$(this).closest('.tablepack-list').find('.tablepack-body').slideUp();
			}else{
				$(this).closest('.sec-benefit-table').find('.tablepack-list').removeClass('active');
				$(this).closest('.sec-benefit-table').find('.tablepack-body').slideUp();
				
				$(this).closest('.tablepack-list').find('.tablepack-body').slideDown();
				$(this).closest('.tablepack-list').addClass('active');
			}
		}
	});
	$('.box-tab').each(function() {
		
		//console.log( $(this) );
		$(this).find('.tab-nav-list:nth-of-type(1) .tab-button').addClass('active');
		
		$(this).find('.tab-content-list:nth-of-type(1)').filter(":first");
		
		var data_tab =  $(this).find('.tab-button.active').attr('data-tab');
		//console.log('data_tab',data_tab);
		$(this).find('.nav-link-'+ data_tab).filter(":first").show();
	});

	////////////////////////////////////////////////////
	//================= SECTION TAB
	////////////////////////////////////////////////////
	$('.tab-nav .tab-nav-list:first-child .tab-button').addClass('active');
	$('.tab-button').click(function(){
		
		var data_tab = $(this).attr('data-tab');
		if(data_tab == "0"){
			$(this).closest('.box-tab').find('.tab-button').removeClass('active');
			$(this).addClass('active');
			$(this).closest('.box-tab').find('.tab-content-list').removeClass('active-content').hide();
			$(this).closest('.box-tab').find('.nav-link-'+data_tab).fadeIn();
		}else{
			$(this).closest('.box-tab').find('.tab-button').removeClass('active');
			$(this).addClass('active');
			$(this).closest('.box-tab').find('.tab-content-list').removeClass('active-content').hide();
			$(this).closest('.box-tab').find('.nav-link-'+data_tab).fadeIn();
			$(this).closest('.box-tab').find('.nav-link-'+data_tab).filter(":first").addClass('active-content');
		}
		
		/*$('.st-list .sec-lists').each(function() {
			var btn_swiper_this = $(this);
			setTimeout(function(){
				var thumb_height = btn_swiper_this.find('.thumb .thumb-img').outerHeight()/2;
				var arrow_height = btn_swiper_this.find('.swiper-button').outerHeight()/2;
				// console.log(thumb_height + " - " + arrow_height);
				btn_swiper_this.find('.swiper-button').css({ top : thumb_height - arrow_height });
			},50)
		});*/
		$('.st-list .sec-lists').each(function() {
			var btn_swiper_this = $(this);
			setTimeout(function(){
				var thumb_height = btn_swiper_this.find('.thumb .thumb-img').outerHeight()/2;
				var arrow_height = btn_swiper_this.find('.swiper-button').outerHeight()/2;
				if(thumb_height != 0){
					// console.log(thumb_height + " - " + arrow_height);
					btn_swiper_this.find('.swiper-button').css({ top : thumb_height - arrow_height });
				}/*else{
					// console.log(thumb_height + " - " + arrow_height);
					btn_swiper_this.find('.swiper-button').css({ top : '50%', transform:'translateY(-50%)' });
				}*/
				
			},50)
		});
		swiperSlide();

	});
	// $('.tab-button').click(function(){
	// 	var data_tab = $(this).attr('data-tab');
	// 	if(data_tab == "all"){
	// 		$(this).find('.tab-content-list').show();
	// 		$(this).closest('.box-tab').find('.tab-button').removeClass('active');
	// 		$(this).addClass('active');
	// 		console.log('all');
	// 	}else{
	// 	$(this).find('.tab-content-list:nth-of-type(1)').show();
	// 	$(this).closest('.box-tab').find('.tab-button').removeClass('active');
	// 	$(this).addClass('active');
	// 	$(this).closest('.box-tab').find('.tab-content-list').hide();
	// 	$(this).closest('.box-tab').find('.nav-link-'+data_tab).fadeIn();
	// 	}
	// });
});
function genSlide(){

	
	if( $('.genSlide').length > 0 ){
		$('.genSlide').each(function(){
			
			var genSlide_space_xl = parseInt( $(this).attr('data-space-xl'));
			var genSlide_space_d = parseInt( $(this).attr('data-space-d') || 30 );
			var genSlide_space_t = parseInt( $(this).attr('data-space-t') || 30 );
			var genSlide_space_m = parseInt( $(this).attr('data-space-m') || 30 );
			var genSlide_perview_xl = parseFloat( $(this).attr('data-perview-xl') );
			var genSlide_perview_d = parseFloat( $(this).attr('data-perview-d') );
			var genSlide_perview_t = parseFloat( $(this).attr('data-perview-t') );
			var genSlide_perview_m = parseFloat( $(this).attr('data-perview-m') || 1 );
			var genSlide_percol_d = parseInt( $(this).attr('data-percol-d') || 1 );
			var genSlide_percol_t = parseInt( $(this).attr('data-percol-t') || 1 );
			var genSlide_percol_m = parseInt( $(this).attr('data-percol-m') || 1 );
			var genSlide_loop = Boolean( $(this).attr('data-loop') || false );
			var genSlide_center_d = Boolean( $(this).attr('data-center-d') || false );
			var genSlide_center_t = Boolean( $(this).attr('data-center-t') || false );
			var genSlide_center_m = Boolean( $(this).attr('data-center-m') || false );
			var genSlide_speed = parseInt( $(this).attr('data-speed') || 800 );
			if( isNaN(genSlide_perview_t)){
				if( genSlide_perview_d == 6 ){genSlide_perview_t = 3;}else if( genSlide_perview_d == 5 ){genSlide_perview_t = 3;}else if( genSlide_perview_d == 4 ){genSlide_perview_t = 2;}else if( genSlide_perview_d == 3 ){genSlide_perview_t = 2;}else if( genSlide_perview_d == 2 ){genSlide_perview_t = 1;}
			}
			if( isNaN(genSlide_perview_xl)){
				genSlide_perview_xl = genSlide_perview_d;
			}
			if( isNaN(genSlide_space_xl)){
				genSlide_space_xl = genSlide_space_d;
			}
			console.log(genSlide_center_m)
			const genSlide = new Swiper($(this).find('.swiper-container'), {
				
				slidesPerView: genSlide_perview_xl,
				spaceBetween: genSlide_space_xl,
				slidesPerColumn: genSlide_percol_d,
				loop: genSlide_loop,
				speed:  genSlide_speed,
				simulateTouch: false,
				centeredSlides : genSlide_center_d,
				navigation: {nextEl: $(this).find('.swiper-button-next'),prevEl: $(this).find('.swiper-button-prev'),},
				pagination: {el: $(this).find('.swiper-pagination'),clickable: true,},
				breakpoints: {
					575: {
						slidesPerView: genSlide_perview_m,
						slidesPerColumn: genSlide_percol_m,
						spaceBetween: genSlide_space_m,
						centeredSlides : genSlide_center_m,
						simulateTouch: true,
					},
					767: {
						slidesPerView: genSlide_perview_m,
						slidesPerColumn: genSlide_percol_m,
						spaceBetween: genSlide_space_m,
						centeredSlides : genSlide_center_m,
						simulateTouch: true,
					},
					991: {
						slidesPerView: genSlide_perview_t,
						slidesPerColumn: genSlide_percol_t,
						spaceBetween: genSlide_space_t,
						centeredSlides : genSlide_center_t,
						simulateTouch: true,
					},
					1199: {
						slidesPerView: genSlide_perview_d,
						slidesPerColumn: genSlide_percol_d,
						spaceBetween: genSlide_space_d,
						centeredSlides : genSlide_center_d,
					},
					1599: {
						slidesPerView: genSlide_perview_d,
						slidesPerColumn: genSlide_percol_d,
						spaceBetween: genSlide_space_d,
						centeredSlides : genSlide_center_d,
					},
					1920: {
						slidesPerView: genSlide_perview_xl,
						slidesPerColumn: genSlide_percol_d,
						spaceBetween: genSlide_space_xl,
						centeredSlides : genSlide_center_d,
					},
					
				},
			});
		});	
	}
}
function onPlayerReady(event) {
	event.target.playVideo();
	// console.log(event.target);
}
function onPlayerStateChange_banner(event) {
	console.log(event.data);
	if(event.data == YT.PlayerState.ENDED) {
		$('.box-bannervideo').fadeOut();
		st_banner.autoplay.start();
		playerBanner.destroy();
		$('.banner-video .click-to-play-video').removeClass('hidePreview');
	}
}

function onPlayerStateChange_general(event) {
	if(event.data == YT.PlayerState.ENDED) {
		player_general.destroy();
		$('.general-video .click-to-play-video').removeClass('hidePreview');
	}
}


// $('.click-to-play-video-loop').click(function () {
	
// 		$(this).find('.video-loop').play();
// 		$(this).find('.video-loop-space').addClass('loop-play');
// 		$(this).find('.videoplay-icon').addClass('loop-play');

// 		//$('video').each(function(){
// 		var aud = $(this).find('.video-loop')
// 		aud.onended = function() {
// 			$(this).closest('.click-to-play-video-loop').find('.video-loop-space').removeClass('loop-play');
// 			$(this).closest('.click-to-play-video-loop').find('.videoplay-icon').removeClass('loop-play');
// 		};
// 	//})
// });

function playVideo_mp4( $this ){	

	$this.find('.video-loop').get(0).play();
	$this.find('.video-loop-space').addClass('loop-play');
	$this.find('.videoplay-icon').addClass('loop-play');

	var aud = $this.find('.video-loop').get(0)

	aud.onended = function() {
		$this.closest('.click-to-play-video-loop').find('.video-loop-space').removeClass('loop-play');
		$this.closest('.click-to-play-video-loop').find('.videoplay-icon').removeClass('loop-play');
	};
}

function playVideo($this ){
	var $parentClick = $this;
	dataVideo_ganeral = $this.attr('data-video');
	section_type = $this.attr('section_type');
	data_code = $this.attr('data-code')
	// console.log(dataVideo_ganeral);
	$this.addClass('hidePreview');
	
	var str = 'player_ganeral_'+data_code+'_'+section_type;
	console.log(str);
	
	var videoPlayer = new YT.Player(str, {
		width : '100%',
		height : '100%',
		videoId : dataVideo_ganeral,
		playerVars: { 'autoplay': 1,'controls': 1,'showinfo': 0 ,'modestbranding':1,'iv_load_policy':3 },
		events : {
			'onReady' : function(event ){
				slideOnStop = 0;
				//   console.log('To play');
				console.log('onPlay',slideOnStop );
				event.target.playVideo();
				st_banner.autoplay.stop();
				console.log(st_banner);
			},
			'onStateChange' : function(event){
				
				st_banner.autoplay.stop();
				// if(event.data == 2 && slideOnStop == 1 ){
				// 	event.target.destroy();
				// 	$parentClick.removeClass('hidePreview');
				// 	st_banner.autoplay.start();
				// 	slideOnStop = 0
				// }
				
				clearInterval(time_update_interval);
				time_update_interval = setInterval(function () {
					if(event.data == YT.PlayerState.ENDED || slideOnStop == 1) {
						event.target.destroy();
						$parentClick.removeClass('hidePreview');
						st_banner.autoplay.start();
						slideOnStop = 0
						clearInterval(time_update_interval);
					}
				}, 100)
			
				
				console.log('time_update_interval',time_update_interval);
				console.log('To Close',slideOnStop);
				
				
			}
		}
	});
}


////////////////////////////////////////////////////
//================= Loadmore
////////////////////////////////////////////////////
function loadMore( $this ){	
	var cate_code =  $this.attr('data-cate-code');
	var cate_key =  $this.attr('data-cate-key');
	var p =  $this.attr('data-p');
	var data_list =  $this.attr('data-list');
	var data_limit =  $this.attr('data-limit');
	var sec_code =  $this.attr('data-sec-code');
	var limit_page =  $this.attr('data-limit-page');
	var btn_loadmore =  $this;
	var token = $('input[name="token"]').val();
	$.ajax(base_url + 'API/loadmore', {
		method: "GET",
		data: {
			'token': token,
			'page': p,
			'cate_code': cate_code,
			'cate_key': cate_key,
			'data_list': data_list,
			'sec_code':sec_code,
			'data_limit': data_limit
		},
		beforeSend: function () {
			$('.loader').show();
		}
	}).done(function (data) {
		if (data != "") {
			$('.append-' + data_list + '-list-' + sec_code + ' ' + ' .nav-link-' + cate_key +' .row'  ).append(data);
			// console.log($('.append-' + data_list + '-list-' + sec_code + ' ' + ' .nav-link-' + cate_key +' .row'  ));
			svg();
			btn_loadmore.attr('data-p', (parseInt(p) + 1));
			btn_loadmore.attr('data-limit-page', (parseInt(limit_page)-1));
			if ( btn_loadmore.attr('data-limit-page') <= 1 ) {
				btn_loadmore.closest('.box-loadmore').hide();
			}
		} else {box-loadmore
			btn_loadmore.l('.box-loadmore').hide();
		}
	});
}



// function playVideo($this){
// 		dataVideo_ganeral = $this.attr('data-video');
// 		data_code = $this.attr('data-code');
// 		console.log(dataVideo_ganeral);
// 		$this.addClass('hidePreview');
// 		var str = 'player_ganeral_'+data_code;
// 		player_ganeral = new YT.Player(str, {
// 			width : '100%',
// 			height : '100%',
// 			videoId : dataVideo_ganeral,
// 			playerVars: { 'autoplay': 1,'controls': 0,'showinfo': 0 ,'modestbranding':1,'iv_load_policy':3 },
// 			events : {
// 				'onReady' : function( event ){
// 					event.target.playVideo();
// 				},
// 				'onStateChange' : onPlayerStateChange_general
// 			}
// 		});
// }

// function playVideo( video_id ){
// 	$(".banner-video .click-to-play-video").addClass('hidePreview');
// 	console.log(video_id);
// 	playerBanner = new YT.Player('player_banner', {
// 		width : '100%',
// 		height : '100%',
// 		videoId : video_id,
// 		playerVars: { 'autoplay': 1,'controls': 0,'showinfo': 0 ,'modestbranding':1,'iv_load_policy':3 },
// 		events : {
// 			'onReady' : onPlayerReady,
// 			'onStateChange' : onPlayerStateChange_banner
// 		}
// 	});
// }
function closeVideo(){
	$('.box-bannervideo').fadeOut();
	if($(".box-bannervideo .video-container.click-to-play-video").hasClass('hidePreview')){
		playerBanner.destroy();
		$(".box-bannervideo .video-container.click-to-play-video").removeClass('hidePreview');
	}
}



function swiperSlide(){
	if( $('.st-list').length > 0 ){
		$('.st-list').each(function() {
			var $st_list = $(this);
			var st_list_1200 = 1;
			var st_list_991 = 1;
			var st_list_768 = 1;
			var st_list_view = $(this).attr('data-slidecol');
			var st_list_column = $(this).attr('data-slidepercolumn');
			if( st_list_view == 6 ){
				st_list_1200 = 4;
				st_list_991 = 3;
				st_list_768 = 2;
			}else if( st_list_view == 5 ){
				st_list_1200 = 3;
				st_list_991 = 3;
				st_list_768 = 2;
			}else if( st_list_view == 4 ){
				st_list_1200 = 3;
				st_list_991 = 3;
				st_list_768 = 2;
			}else if( st_list_view == 3 ){
				st_list_1200 = 3;
				st_list_991 = 2;
				st_list_768 = 2;
			}else if( st_list_view == 2 ){
				st_list_1200 = 2;
				st_list_991 = 2;
				st_list_768 = 1;
			}
			if ( $st_list.find('.cont-list-swiper-custom').length > 0 ) {
				$st_list.find('.swiper-container').each(function(index, value){
					var st_swiper = new Swiper($(this), {
						navigation: {
							nextEl: $(this).closest('.cont-list-swiper-custom').find('.swiper-button-next'),
							prevEl: $(this).closest('.cont-list-swiper-custom').find('.swiper-button-prev'),
						},
						// navigation: {
						// 	nextEl: '.swiper-button-next',
						// 	prevEl: '.swiper-button-prev',
						//   },
						autoHeight: true,
						spaceBetween: 30,
						slidesPerView: st_list_view,
						centerInsufficientSlides:true,
						//slidesPerColumn: st_list_column,
						loop: false,
						speed: 800,
						simulateTouch: false,
						breakpoints: {
							575: {
								slidesPerView: st_list_column,
								simulateTouch: true,
							},
							767: {
								slidesPerView: st_list_768,
								simulateTouch: true,
							},
							990: {
								slidesPerView: st_list_991,
								simulateTouch: true,
							},
							1199: {
								slidesPerView: st_list_1200,
								simulateTouch: false,
							},
						},
					});

				})
			}
		});
	}
}


$(".tab-nav ul li a").on("click", function() {
	$(this).closest('.box-tab').find('.tab-button').removeClass("active");
	$(this).addClass("active");
	$(this).closest(".tab-nav").scrollCenter(".active", 300);
});


var first_animate = 0;
if($('.st:first-child').height() < $(window).height() / 2) { first_animate = 1; }

$(window).load(function(){

	//tl.from($('.box-logo'), 2, {alpha:0,x:-200, ease:Power3.easeOut},0);
	//tl.from($('.section-header .main-menu'), 2, {alpha:0,y:-200, ease:Power3.easeOut},0);

	//tl.from($('.time-event'), 2, {alpha:0, ease:Power3.easeOut},1);
	//tl.from($('.box-share'), 2, {alpha:0,y:200, ease:Power3.easeOut},1);	
	//tl.from($('.sec-content'), 2, {alpha:0,y:200, ease:Power3.easeOut},1);	
	
	$('.st').each(function( index , value ) {

		if( index <= first_animate ){
			if( $(this).hasClass('st-content') ){
				tl.from($(this), 2, {alpha:0, ease:Power3.easeOut},0.2);	
				tl.from($(this).find('.sec-title'), 2, {alpha:0,y:200, ease:Power3.easeOut},0.5);	
				tl.from($(this).find('.cont-txt'), 2, {alpha:0,y:200, ease:Power3.easeOut},1.1);		
				tl.from($('.cont-img'), 2, {alpha:0,y:200, ease:Power3.easeOut},1.1);			
			}
			if( $(this).hasClass('st-list') ){
				tl.from($(this), 2, {alpha:0, ease:Power3.easeOut},0.2);
				tl.from($(this).find('.sec-title'), 2, {alpha:0,y:200, ease:Power3.easeOut},0.5);
				tl.from($(this).find('.sec-list-cate'), 2, {alpha:0,y:200, ease:Power3.easeOut},0.6);
				tl.from($(this).find('.box-tab'), 2, {alpha:0,y:200, ease:Power3.easeOut},0.7);	
				tl.from($(this).find('.sec-lists'), 2, {alpha:0,y:200, ease:Power3.easeOut},1);	
				tl.from($(this).find('.sec-list-content'), 2, {alpha:0,y:200, ease:Power3.easeOut},1);	
				tl.from($(this).find('.box-thumb-hl'), 2, {alpha:0,y:200, ease:Power3.easeOut},1);
				tl.from($(this).find('.btn-list-more'), 2, {alpha:0,y:200, ease:Power3.easeOut},1.2);
			}
			if( $(this).hasClass('st-banner') ){
				tl.from($(this), 2, {alpha:0, ease:Power3.easeOut},0.2);
				tl.from($(this).find('.sec-title'), 2, {alpha:0,y:200, ease:Power3.easeOut},0.5);
				tl.from($(this).find('.cont-txt'), 2, {alpha:0,y:200, ease:Power3.easeOut},1.1);		
				tl.from($(this).find('.cont-img'), 2, {alpha:0,y:200, ease:Power3.easeOut},1.1);	
			}
		}
	});

}); 

var controller = $.superscrollorama({
	reverse: false,
	playoutAnimations: false,
	offset: 10000
});

$(document).ready(function() {
	controller.addTween('.sec-animate-video',TweenMax.from('.sec-animate-video .box-video', 1, {ease:Power3.easeOut, alpha:0,y:200,delay:0.1}),0,0);

	$('.st').each(function( index , value ) {
		if( index > first_animate ){
			if( $(this).hasClass('st-content') ){
				//console.log('content');
				controller.addTween($(this), TweenMax.from($(this), 1, {ease: Power3.easeOut,alpha: 0,delay: 0}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.sec-title'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.1}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.cont-img'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.1}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.cont-txt'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.5}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.ele_img1'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.2}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.ele_img2'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.2}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.ele_img3'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.2}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.ele_img4'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.2}), 0, 0);
			}
			if( $(this).hasClass('st-list') ){
				//console.log('list');
				controller.addTween($(this), TweenMax.from($(this), 1, {ease: Power3.easeOut,alpha: 0,delay: 0}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.sec-title'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.1}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.sec-list-cate'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.2}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.box-tab'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.2}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.sec-lists'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.5}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.sec-list-content'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.5}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.box-thumb-hl'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.5}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.btn-list-more'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.7}), 0, 0);
			}
			if( $(this).hasClass('st-banner') ){
				//console.log('banner');
				controller.addTween($(this), TweenMax.from($(this), 1, {ease: Power3.easeOut,alpha: 0,delay: 0}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.sec-title'), 1, {ease: Power3.easeOut,alpha: 0,y: 100,delay: 0.1}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.cont-txt '), 1, {ease: Power3.easeOut,alpha: 0,y: 200,delay: 0.1}), 0, 0);
				controller.addTween($(this), TweenMax.from($(this).find('.cont-img'), 1, {ease: Power3.easeOut,alpha: 0,y: 200,delay: 0.5}), 0, 0);
			}
		}
	});

});

$( document ).ready(function() {
    $('.contact-us').click(function () {
        $form = $('#contact_form');
        if (validContact($form)) {
            var form = $form[0];
            var formData = new FormData(form);
            event.preventDefault();
            $.ajax(base_url + 'API/send_contact', {
                method: "POST",
                processData: false,
                contentType: false,
                data: formData,
                dataType: 'json',
                success: function (arresult) {
                    console.log(arresult.rs);
                    if (arresult.rs) {
                        console.log(' Form complete ');
                        popin('popup-alert');
                        $form.find('input[name="fullname"]').val('');
                        $form.find('input[name="lastname"]').val('');
                        $form.find('input[name="email"]').val('');
                        $form.find('input[name="phone"]').val('');
                        //$form.find('select[name="subject"]').val('');
                        $form.find('input[name="message"]').val('');
                        //$('.loader').hide();
                        //window.location.href = window.location.href;
                    }
                }
            });
        }
    });
});

function validContact($form) {
	var check = true;
	$fullname = $form.find('input[name="fullname"]');
	//$lastname = $form.find('input[name="lastname"]');
	//$subject = $form.find('select[name="subject"]');
	$email = $form.find('input[name="email"]');
	$phone = $form.find('input[name="phone"]');
	$message = $form.find('input[name="message"]');
	//$resume = $form.find('input[name="resume"]');
	//$transcript = $form.find('input[name="transcript"]');

	$fullname.removeClass('is-invalid');
	//$lastname.removeClass('is-invalid');
	//$subject.parent().removeClass('is-invalid');
	$email.removeClass('is-invalid');
	$phone.removeClass('is-invalid');
	$message.removeClass('is-invalid');

	if ($fullname.val() == "") {
		check = false;
		$fullname.addClass('is-invalid');
	}
	/*if ($lastname.val() == "") {
		check = false;
		$lastname.addClass('is-invalid');
	}*/
	/*if ($subject.val() == "") {
		check = false;
		$subject.parent().addClass('is-invalid');
	}*/
	if ($email.val() == "") {
		check = false;
		$email.addClass('is-invalid');
	} else {
		if (!validEmail($email.val())) {
			check = false;
			$email.addClass('is-invalid');
		}
	}
	if ($phone.val() == "") {
		check = false;
		$phone.addClass('is-invalid');
	}
	if ($message.val() == "") {
		check = false;
		$message.addClass('is-invalid');
	}

	return check;
}
function validEmail(value) {
	var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
	return (value.match(r) == null) ? false : true;
}

