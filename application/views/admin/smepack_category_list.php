<?php
	if ( sizeof($edit_cate_detail) > 0 ){
		$code = $edit_cate_detail[DEFAULT_LANG]['code'];

		$thumb = $edit_cate_detail[DEFAULT_LANG]['thumb'];
		$slug = $edit_cate_detail[DEFAULT_LANG]['slug'];
		$onoff = $edit_cate_detail[DEFAULT_LANG]['onoff'];

		$btn_text = 'Update'; 
	} else {
		$code = '';
		$thumb = '';
		$slug = '';
		$onoff = 1;

		$btn_text = 'Save';
	}
?>

<div class="content-wrapper">
	<div class="page-header">
		<h4 class="page-title"> Category for <?php echo $cate_detail[DEFAULT_LANG]['title']; ?> </h4>
	</div>
	<div class="row">
		<div class="col-12 col-md-6">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<?php echo form_open_multipart('admin/smepack_category/saveCategoryForm', ' class="update-cate-form"'); ?>
							<!-- Content here -->
							<input type="hidden" name="code" value="<?php echo $code; ?>">
							<input type="hidden" name="parent_code" value="<?php echo $cate_detail[DEFAULT_LANG]['code']; ?>">
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
							<div class="form-group <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
								<ul class="nav nav-tabs" role="tablist">
									<?php foreach( $langs as $lang ){ ?>
										<li class="nav-item">
											<a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo $lang['text']; ?>-tab" data-toggle="tab" href="#<?php echo $lang['text']; ?>" role="tab" aria-controls="<?php echo $lang['text']; ?>" aria-selected="true"><?php echo $lang['text']; ?></a>
										</li>
									<?php } ?>
								</ul>
								<div class="tab-content">
									<?php foreach( $langs as $lang ){ ?>
										<?php
										if( isset( $edit_cate_detail[ $lang['text'] ] ) ){
                                            $package_name = $edit_cate_detail[ $lang['text'] ]['package_name'];
											$short_description = $edit_cate_detail[ $lang['text'] ]['short_description'];
											$title = $edit_cate_detail[ $lang['text'] ]['title'];
											$description = $edit_cate_detail[ $lang['text'] ]['description'];
											$prefix_title = $edit_cate_detail[ $lang['text'] ]['prefix_title'];
										}else{
                                            $package_name = '';
											$short_description = '';
											$title = '';
											$description = '';
											$prefix_title = '';
										}
										?>
										<div class="<?php echo 'lang_'.$lang['text']; ?> tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo $lang['text']; ?>" role="tabpanel" aria-labelledby="<?php echo $lang['text']; ?>-tab">
                                           
                                            <div class="form-group mb-3">
												<label for="">Prefix Title </label>
												<input type="text" name="prefix_title[]" class="form-control nameInput"  placeholder="Prefix Title" value="<?php echo $prefix_title; ?>">
											</div>	
											<div class="form-group mb-3">
												<label for="">Title </label>
												<input type="text" name="title[]" class="form-control nameInput"  placeholder="Name" value="<?php echo $title; ?>">
											</div>
                                            <div class="form-group mb-3">
												<label for="">Package name </label>
												<input type="text" name="package_name[]" class="form-control nameInput"  placeholder="Package name" value="<?php echo $package_name; ?>">
											</div>
											<div class="form-group mb-3">
												<label for="">Short Description </label>
												<textarea name="short_description[]" class="form-control" rows="3" placeholder="Short Description" ><?php echo $short_description; ?></textarea>
											</div>
											<div class="form-group mb-3">
												<label for="">Description </label>
												<textarea name="description[]" class="form-control descriptionInput tinyMceExample" rows="7" placeholder="Description" ><?php echo $description; ?></textarea>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>

							<div class="form-group">
								<label for="">Slug</label>
								<input type="text" name="slug" class="form-control" placeholder="Slug" value="<?php echo $slug; ?>">
							</div>
							
							<div class="row">
								<div class="col-12 image-box mb-3">
									<div class="form-group">
										<label>Thumb category 
											<small class="text-muted"> (50x50)</small>
										</label>
										<input type="file" data-height="200" name="thumb" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $thumb != "" ? upload_path($thumb) : ''; ?>" />
										<input type="hidden" class="thumb_text" name="thumb_txt" value="<?php echo $thumb; ?>" />
									</div>
								</div>
								
								<div class="col-12 mb-3">
									<div class="form-group">
										<label for="">Status</label>
										<div class="form-check form-check-dark">
											<label class="form-check-label">
												<input type="radio" class="form-check-input" name="onoff" value="1" <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
										</div>
										<div class="form-check form-check-dark">
											<label class="form-check-label">
												<input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 ? 'checked' : ''; ?>> Unpublish </label>
										</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group mb-0">
										<div class="text-center card-button">
											<button type="submit" class="btn btn-gradient-dark mr-2 w-100"><?php echo $btn_text; ?></button>
										</div>
									</div>
								</div>
							</div>
				
							<!--<div class="form-group">
								<input type="text" name="image_alt" class="form-control"  placeholder="Image alt" value="">
							</div>-->
							
						<?php form_close(); ?>
						</div>
						
					</div>
				</div>
			</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-12 col-lg-12 col-md-12 col-sm-12 ">
							
							<h5 class="title">Manage Category</h5>
							<div class="drag-wrapper mb-3">
								<div class="drag-box">
									<ul class="drag-item-list smepack-nexcate-move">
										<?php foreach( $lists as $row ){ ?>
										<li class="drag-list level-0 <?php echo $row['onoff'] == 0 ? 'non-active' : ''; ?>" data-code="<?php echo $row['code']; ?>">
											<a href="javascript:void(0)"><?php echo $row['title'] ?></a>
											<div class="nexu-tools d-block">
												<div>
													<div class="nexu-tools-list"><a href="<?php echo base_url().'admin/smepack_category/lists/'.$cate_detail[DEFAULT_LANG]['code'].'/'.$row['code']; ?>"><i class="mdi mdi-pencil"></i></a></div>
													<div class="nexu-tools-list"><a href="javascript:void(0)" onClick="smepack_activeCate( $(this) )"><i class="mdi mdi-eye<?php echo $row['onoff'] == 0 ? '-off' : ''; ?>"></i></a></div>
													<div class="nexu-tools-list"><a href="javascript:void(0)" class="close-nexitor" onclick="deleteItem( $(this) );" data-delete-type="cate" data-delete-by="code" data-table="smepack_category" data-code="<?php echo $row['code'] ?>" data-toggle="modal" data-target="#deleteModal"><i class="mdi mdi-close"></i></a></div>
												</div>
											</div>

										</li>
									<?php } ?>
									</ul>
								</div> <!-- drag-box -->

							</div>

							<div class="text-center card-button mt-3">
								<a href="<?php echo base_url().'admin/smepack_category/lists/'.$cate_detail[DEFAULT_LANG]['code']; ?>" class="btn btn-gradient-dark mr-2 w-100">Add</a>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>

<!-- content-wrapper ends -->
