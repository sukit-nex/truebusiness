<?php
	if( sizeof( $detail ) > 0 ){
		$code = $detail[DEFAULT_LANG]['code'];
		$name = $detail[DEFAULT_LANG]['name'];
		$bg_d = $detail[DEFAULT_LANG]['bg_d'];
		$bg_m = $detail[DEFAULT_LANG]['bg_m'];

		$sticky = $detail[DEFAULT_LANG]['sticky'];
		$onoff = $detail[DEFAULT_LANG]['onoff'];
		
	}else{
		$code = $data_code;
		$name = "";
		$bg_d = '';
		$bg_m = '';

		$sticky = 0;
		$onoff = 1;
	}
?>
<div class="content-wrapper">
	<div class="page-header">
		<h3 class="page-title"> Solution Owner Form </h3>
		<a href="<?php echo base_url().'admin/solution_banner/'; ?>"
            class="btn btn-gradient-dark btn-fw">
            <- Back</a>
	</div>
	<?php echo form_open_multipart('admin/solution_banner/saveForm', ' class="mainForm"'); ?>
		<input type="hidden" name="code" value="<?php echo $code ?>" >
		<input type="hidden" name="time" value="<?php echo $time; ?>">

	<div class="card mt-4">
        <div class="card-body">
			<div class="form-group mb-3">
				<label for="">Banner Name </label>
				<input type="text" name="name" class="form-control"  placeholder="Banner Name" value="<?php echo $name; ?>">
			</div>

			<div class="row">
				<div class="col-12 col-md-6 image-box">
					<div class="form-group">
						<label><small class="text-muted">Background
								Desktop</small></label>
						<input type="file" data-height="190" name="bg_d"
							class="dropify" data-max-file-size="3000kb"
							data-default-file="<?php echo  $bg_d != "" ? upload_path($bg_d) : ''; ?>" />
						<input type="hidden" class="image_as_text" name="bg_d_txt"
							value="<?php echo $bg_d; ?>" />
					</div>
				</div>
				<div class="col-12 col-md-6 image-box">
					<div class="form-group">
						<label><small class="text-muted">Background
								Mobile</small></label>
						<input type="file" data-height="190" name="bg_m"
							class="dropify" data-max-file-size="3000kb"
							data-default-file="<?php echo  $bg_m != "" ? upload_path($bg_m) : ''; ?>" />
						<input type="hidden" class="image_as_text" name="bg_m_txt"
							value="<?php echo $bg_m; ?>" />
					</div>
				</div>
			</div>

            <div class="box-switch  my-3">
                <h4 class="card-title">Content</h4>
                <div class="content-switch mb-3">
                    <div class="content">
                        <div class="row">
                       
                            <div class="col col-12 col-md-12">
                                <div class="form-group">
                                    <div class=" w-100 <?php echo sizeof( $langs ) == 0 ? 'only-one-lang' : ''; ?>">
                                        <ul class="nav nav-tabs" role="tablist" style="">
                                            <?php foreach( $langs as $lang ){ ?>
                                            <li class="nav-item">
                                                <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                                    id="<?php echo $lang['text']; ?>-tab" data-toggle="tab"
                                                    href="#<?php echo $lang['text']; ?>" role="tab"
                                                    aria-controls="<?php echo $lang['text']; ?>"
                                                    aria-selected="true"><?php echo $lang['text']; ?></a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                        <div class="tab-content float-left w-100" style="">
                                            <?php foreach( $langs as $lang ){ ?>
                                            <?php
                                            if( isset( $detail[ $lang['text'] ] ) ){
                                                $content_editor = $detail[ $lang['text'] ]['content_editor'];
                                            }else{
                                                $content_editor = '';
                                            }
                                            ?>
                                            <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                                id="<?php echo $lang['text']; ?>" role="tabpanel"
                                                aria-labelledby="<?php echo $lang['text']; ?>-tab">

                                                <div class="form-group">
                                                    <div class="install-nexitor float-left" data-area="content">
                                                        <?php echo $content_editor; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

				<div class="form-group mb-0">
					<label for="">Order </label>
					<input type="text" name="sticky" class="form-control"  placeholder="Order" value="<?php echo $sticky; ?>">
				</div>
            </div>

        </div>
    </div>

	<div class="card my-3">
		<div class="card-body">
			<div class="form-group">
				<label for="">Status</label>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="onoff" value="1"  <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
				</div>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 ? 'checked' : ''; ?>> Non - Publish </label>
				</div>
			</div>

			<div class="row">
				<div class="col-12 text-right">
					<div class="text-center card-button">
						<button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<!-- content-wrapper ends -->
<?php include_once("inc/nexitor-modal.php"); ?>
