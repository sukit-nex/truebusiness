<?php
	if( sizeof( $detail ) > 0 ){
		$code = $detail[DEFAULT_LANG]['code'];
		$onoff = $detail[DEFAULT_LANG]['onoff'];
	}else{
		$code = '';
		$onoff = 1;
	}
?>
<div class="content-wrapper">
	<div class="page-header">
		<h3 class="page-title"> Solution : Brochure Form </h3>
		<!-- <a href="<?php //echo base_url().'admin/solution/brochure/lists'; ?>" class="btn btn-gradient-dark btn-fw"> <- Back</a> -->
	</div>
	<ul class="nav nav-tabs nav-tabs-lang" role="tablist" style="">
		<?php foreach( $langs as $lang ){ ?>
		<li class="nav-item">
			<a class="nav-link head-<?php echo $lang['text']; ?> <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" 
			href="javascript:activaTabNex('<?php echo $lang['text']; ?>');"><?php echo $lang['text']; ?></a>
		</li>
		<?php } ?>
	</ul>
	<?php echo form_open_multipart('admin/solution/brochure/saveForm', ' class="mainForm"'); ?>
		<input type="hidden" name="code" value="<?php echo $code ?>" >
		<input type="hidden" name="time" value="<?php echo $time; ?>">

	<div class="card">
        <div class="card-body">
			<div class="row">
				<div class="col-12 col-md-12">
					<div class="form-group">
						<div class="tab-content tab-content-lang">
							<?php foreach( $langs as $lang ){ ?>
								<?php
								if( isset( $detail[ $lang['text'] ] ) ) {
									$brochure_pdf = $detail[ $lang['text'] ]['brochure_pdf'];
								} else {
									$brochure_pdf = '';
								}
								?>
								<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
									<div class="form-group">
										<label>Brochure PDF</label>
										<input type="file" name="brochure_pdf[]" class="file-upload-default">
										<div class="input-group col-xs-12">
											<input type="text" class="form-control file-upload-info" disabled placeholder="Upload PDF" value="<?php echo $brochure_pdf; ?>">
											<span class="input-group-append">
												<button class="file-upload-browse btn  btn-gradient-dark" type="button">Upload</button>
											</span>
										</div>
										<input type="hidden" name="brochure_pdf_txt[]" value="<?php echo $brochure_pdf; ?>" >
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
					
				</div>
				
			</div>
        </div>
    </div>

	<div class="card my-3">
		<div class="card-body">
			<div class="form-group">
				<label for="">Status</label>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="onoff" value="1"  <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
				</div>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 ? 'checked' : ''; ?>> Non - Publish </label>
				</div>
			</div>

			<div class="row">
				<div class="col-12 text-right">
					<div class="text-center card-button">
						
						<button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<!-- content-wrapper ends -->
<?php include_once("inc/nexitor-modal.php"); ?>
