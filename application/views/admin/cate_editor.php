<div class="content-wrapper">
	<div class="page-header">
		<h4 class="page-title"> Editor Permission </h4>
		<?php /* <a href="#" data-toggle="modal" data-target="#nexuModal" class="btn btn-gradient-dark btn-fw">+ Add Menu</a> */ ?>
	</div>
	<div class="row">
		<div class="col-3">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
						<h5 class="title">Management</h5>
							<div class="">
							<?php echo form_open_multipart('admin/permission/saveEditorCate', ' class="mainForm"'); ?>
								<div class="form-group">
									<label>Role</label>
									<select class="form-control" name="role_code">
										<option value="">Select Role</option>
										<?php foreach( $permission_role as $key => $role ){ ?>
											<option value="<?php echo $role['role_code']; ?>"><?php echo $role['role_text'] ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label>Category</label>
									<select class="form-control" name="cate_code">
										<option value="">Select Category</option>
										<?php foreach( $cates as $cate ){ ?>
											<?php if( $cate['sec_code'] != "" ){ ?>
												<option value="<?php echo $cate['code']; ?>"><?php echo $cate['title']; ?></option>
											<?php } ?>
										<?php } ?>
									</select>
								</div>
								<div class="form-group mb-0">
									<div class="text-center card-button">
										<button class="btn btn-gradient-dark btn-fw" onclick="$('.loader').show();" type="submit">
											ADD
										</button>
									</div>
								</div>
							<?php echo form_close(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-9">
			<div class="row">
				<?php foreach( $permission_role as $key => $role ){ ?>
					<div class="col-12 col-md-4">
						<div class="card">
							<div class="card-body">
							<h5 class="title"><?php echo $role['role_text'] ?></h5>
								<div class="">
									<div class="role-cate-wrap">
										<?php foreach( $cates as $cate ){ ?>
											<?php if( hasPermission( $role['role_code'] , $cate ) ){ ?>
												<div class="role-cate-list">
													<h6><?php echo $cate['title']; ?></h6>
													<a class="role-cate-remove" href="javascript:void(0);" onclick="roleCateRemove( $(this) )" data-role-code="<?php echo $role['role_code']; ?>" data-cate-code="<?php echo $cate['code']; ?>"><i class="mdi mdi-close"></i><a>
												</div>
											<?php } ?>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>