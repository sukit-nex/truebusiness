<?php
  if( sizeof( $detail ) > 0 ){
    $code = $detail[DEFAULT_LANG]['code'];
    $title = $detail[DEFAULT_LANG]['title'];
    $sec_code = $detail[DEFAULT_LANG]['sec_code'];
    $page = $detail[DEFAULT_LANG]['page'];

    $image = $detail[DEFAULT_LANG]['image'];
	$image_m = $detail[DEFAULT_LANG]['image_m'];
    
    $type = $detail[ DEFAULT_LANG ]['type'];

    $video_type = $detail[ DEFAULT_LANG ]['video_type'];
    $video = $detail[ DEFAULT_LANG ]['video'];
    $video_file = $detail[ DEFAULT_LANG ]['video_file'];
    $video_thumb = $detail[ DEFAULT_LANG ]['video_thumb'];

    $content_position_hoz = $detail[ DEFAULT_LANG ]['content_position_hoz'];
    $content_position_ver = $detail[ DEFAULT_LANG ]['content_position_ver'];

    $content_position_hoz_m = $detail[ DEFAULT_LANG ]['content_position_hoz_m'];
    $content_position_ver_m = $detail[ DEFAULT_LANG ]['content_position_ver_m'];

    $sticky = $detail[DEFAULT_LANG]['sticky'];
    $onoff = $detail[DEFAULT_LANG]['onoff'];
  
    $onoff = $detail[DEFAULT_LANG]['onoff'];
    $link_type = $detail[DEFAULT_LANG]['link_type'];
    $page_internal_code = $detail[DEFAULT_LANG]['page_code'];
    $link_out = $detail[DEFAULT_LANG]['ex_link'];
  }else{
    $code = "";
    $title = "";
    $sec_code = $sec_detail[DEFAULT_LANG]['code'];

    $page = '';

    $image = '';
	$image_m = '';
    
    $type = 'image';

    $video_type = 'youtube';
    $video = '';
    $video_file = '';

    $content_position_hoz = 'center';
    $content_position_ver = 'middle';

    $content_position_hoz_m = 'center';
    $content_position_ver_m = 'middle';

    $sticky = 0;
    $onoff = 1;

    $video_thumb = '';
    $link_type = 'no';
    $link_out = '';
    $page_internal_code = '';
  }
?>

<div class="content-wrapper">
    <div class="page-header">
        <h4 class="page-title"> <?php echo strtoupper( 'Banner form of : '.$sec_detail[DEFAULT_LANG]['section_name'] ) ?></h4>
        <a href="<?php echo base_url().'admin/section/banner/lists/'.$sec_code; ?>" class="btn btn-gradient-dark btn-fw"><- Back</a>
    </div>
     <?php echo form_open_multipart('admin/banner/saveSectionBannerForm', ' class="mainForm"'); ?>
        <input type="hidden" name="code" value="<?php echo $code ?>">
        <input type="hidden" name="sec_code" value="<?php echo $sec_code ?>">
        <input type="hidden" name="page" value="<?php echo $page; ?>">
        <input type="hidden" name="time" value="<?php echo $time; ?>">
    <div class="card mb-3">
        <div class="card-body">
            <div class="form-group">
                <label for="">Banner name</label>
                <input type="text" name="title" class="form-control" placeholder="Title"
                    value="<?php echo $title; ?>">
            </div>
        </div>
    </div>

    <div class="card mt-4">
        <div class="card-body">
            <h4 class="card-title">Banner type</h4>
            <div class="form-group mb-0">
                <div class="row">
                    <?php /*
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="box-radio box-check-gray" onclick="" data-cate-for="">
                            <div class="form-check form-check-dark">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="type"
                                        onclick="$('.image').hide(); $('.video').hide();" value="none" checked>
                                    No-image
                                </label>
                            </div>
                        </div>
                    </div>
                    */ ?>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="box-radio box-check-gray" onclick="" data-cate-for="">
                            <div class="form-check form-check-dark">
                                <label class="form-check-label">
                                    <?php
                                    $display = '';
                                    if ( $link_type == 'no' ) {
                                        $display = '$(\'.content\').show();';
                                    } else if ( $link_type == 'internal_link' || $link_type == 'external_link' ) {
                                        $display = '$(\'.content\').hide();';
                                    }
                                    ?>
                                    <input type="radio" class="form-check-input" name="type" value="image"
                                        onclick="$('.image, .ytb_img').show();  $('.video').hide();  $('.viewmore-set').show(); <?php echo $display; ?>"
                                        <?php echo $type == 'image' ? 'checked' : ''; ?> >
                                    Image
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="box-radio box-check-gray" onclick="" data-cate-for="">
                            <div class="form-check form-check-dark">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="type" value="video"
                                
                                        onclick="$('.video').show(); $('.conten-show').show(); $('.viewmore-set').hide();  <?php  echo  $video_type == 'youtube' ? '$(\'.ytb_img\').show(); $(\'.content\').hide(); ': '' ?> <?php  echo  $video_type == 'loop' ? '$(\'.ytb_img\').hide(); $(\'.content\').show(); ': '' ?> "
                                        <?php echo $type == 'video' ? 'checked' : ''; ?> >
                                    Video
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="form-group video mt-4" style="display:<?php echo  $type == 'video' ? 'block' : 'none';?>;">-->
                
                <?php /*
                <div class="from-group youtube" style="display:<?php echo  $video_type == 'youtube' ? 'block' : 'none';?>;">
                    <label for="">Video Thumb</label>
                    <input type="file" data-height="250" name="video_thumb" class="dropify" data-max-width="1330"
                        data-max-height="748" data-max-file-size="3000kb"
                        data-default-file="<?php echo $video_thumb != "" ? upload_path($video_thumb) : ''; ?>" />
                        <input type="hidden" class="image_as_text" name="video_thumb_txt" value="<?php echo $video_thumb; ?>" />
                </div>  
                */ ?>
                
                
            <!--</div>-->
        </div>
    </div>

    <div class="card mt-4">
        <div class="card-body">
            <div class="form-group image" style="display:<?php echo  $type != 'none' ? 'block' : 'none';?>;">
                <div class="row">
                    <div class="col-12 col-md-4 video" style="display:<?php echo  $type == 'video' ? 'block' : 'none';?>;">
                        <div class="form-group">
                            <div class="card_header">
                                <h4 class="card-title">Video</h4>
                            </div>
                            <div class="form-group">
                                <label>Video Type</label>
                                <div class="">
                                    <ul class="alignment-position w-100" style="z-index:10;">
                                        <li class="alignment-position-item  w-50">
                                            <input type="radio" id="youtube" class="form-check-input" name="video_type" checked
                                                onclick="$('.youtube').show();$('.ytb_img').show();  $('.loop').hide(); $('.content').hide(); $('.content').removeClass('conten-show'); " value="youtube"
                                                <?php echo $video_type == 'youtube' ? 'checked' : ''; ?>>
                                            <label for="youtube" class="justify-content-center w-auto"> Video youtube </label>
                                        </li>
                                        <li class="alignment-position-item  w-50">
                                            <input type="radio" id="loop" class="form-check-input" name="video_type"
                                                onclick="$('.loop').show(); $('.youtube').hide();$('.ytb_img').hide();  $('.content').show(); $('.content').addClass('conten-show');" value="loop"
                                                <?php echo $video_type == 'loop' ? 'checked' : ''; ?>>
                                            <label for="loop" class="justify-content-center w-auto"> Video loop </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group mb-0 youtube" style="display:<?php echo  $video_type == 'youtube' ? 'block' : 'none';?>;">
                                <?php /* data-max-height="100" data-max-width="100" */ ?>
                                <label for="">Video ID <small class="text-muted">( https://www.youtube.com/watch?v=<span style="color:red">tjZTVimUnd4</span> )</small></label>
                                <input type="text" name="video" class="form-control" placeholder="Video ID" value="<?php echo $video; ?>">
                            </div>

                            <div class="from-group mb-0 loop image-box " style="display:<?php echo  $video_type == 'loop' ? 'block' : 'none';?>;">
                                <label for="">Video MP4</label>
                                <input type="file" data-height="105" name="video_file" class="dropify" data-max-width="1330"
                                    data-max-height="748" data-max-file-size="10M"
                                    data-default-file="<?php echo $video_file != "" ? upload_path($video_file) : ''; ?>" />
                                    <input type="hidden" class="image_as_text" name="video_file_txt" value="<?php echo $video_file; ?>" />
                            </div>
                            
                            <!--<div class="from-group loop mt-4" style="display:<?php echo  $video_type == 'loop' ? 'block' : 'none';?>;">
                                <div class="file-upload-wrapper">
                                    <div id="fileuploader">Upload</div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                    <!-- ($video_type != 'loop' || $type == 'image') ? 'block' : 'none' -->
                    <div class="col-12  col-md-8 ytb_img" style="display:<?php  if($type == 'image'){echo 'block';}elseif($type == 'video' && $video_type == 'youtube' ){echo 'block';}else{echo 'none';}  ?>;">
                        <div class="card_header">
                            <h4 class="card-title">Background</h4>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
                                <label>Background Desktop  <i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Banner : 1680 x 720 px" data-original-title="Image recommended"></i></label>
                                <input type="file" data-height="200" name="image" class="dropify" data-max-file-size="3000kb"
                                    data-default-file="<?php echo $image != "" ? upload_path($image) : ''; ?>" />
                                <input type="hidden" class="image_as_text" name="image_txt" value="<?php echo $image; ?>" />
                            </div>

                            <div class="col-12 col-md-6 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
                                <label>Background Mobile  <i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Banner : 760 x 1060 px" data-original-title="Image recommended"></i></label>
                                <input type="file" data-height="200" name="image_m" class="dropify" 
                                data-max-file-size="3000kb" data-default-file="<?php echo $image_m != "" ? upload_path($image_m) : ''; ?>" />
                                <input type="hidden" class="image_as_text" name="image_m_txt" value="<?php echo $image_m; ?>" />
                            </div>

 
                        </div>
                    </div>

                   
                </div>
            </div>

            <div class="card-group-inner mt-5 mb-5 viewmore-set" style="display:<?php echo  ($type == 'image') ? 'block' : 'none'?>;">
                <div class="form-group  mb-3 ">

                    <div class="form-group-choice mb-3">
                        <div class="row d-block d-md-flex align-items-center">
                            <div class="col col-12 col-md-1">
                                <label>Link Type</label>
                            </div>
                            <div class="col col-12 col-md-11">
                                <div class="fg-choice-group">
                                    <ul class="alignment-position">
                                        <li class="alignment-position-item">
                                            <input type="radio" id="link_type_no" name="link_type" value="no" <?php if( $link_type == 'no' || $link_type == ''  ){ echo 'checked'; } ?>>
                                            <label for="link_type_no" onclick=" $('.link-out').hide(); $('.internal-link').hide(); $('.button-text').hide(); $('.content').show();">
                                                <img class="svg"
                                                    src="/assets/admin/images/element/section/i-none.svg"
                                                    alt="">
                                                    <span>No Link</span>
                                            </label>
                                        </li>
                                        <li class="alignment-position-item">
                                            <input type="radio" id="external_link" name="link_type" value="external_link" <?php if( $link_type == 'external_link'  ){ echo 'checked'; } ?>>
                                            <label for="external_link" onclick="$('.link-out').show(); $('.internal-link').hide(); $('.button-text').show(); $('.content').hide();">
                                                <!-- <img class="svg" src="/assets/admin/images/element/section/content-layout-06.svg" alt=""> -->
                                                <img class="svg"
                                                    src="/assets/admin/images/element/section/i-link-external.svg"
                                                    alt="">
                                                <span>External link</span>
                                            </label>
                                        </li>
                                        <li class="alignment-position-item">
                                            <input type="radio" id="internal_link" name="link_type" value="internal_link"
                                                <?php echo $link_type == 'internal_link' ? 'checked' : ''; ?>>
                                            <label for="internal_link" onclick=" $('.internal-link').show(); $('.link-out').hide(); $('.button-text').show(); $('.content').hide();">
                                                <!-- <img class="svg" src="/assets/admin/images/element/section/content-layout-06.svg" alt=""> -->
                                                <img class="svg"
                                                    src="/assets/admin/images/element/section/i-link-internal.svg"
                                                    alt="">
                                                <span>Internal link</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group link-out" style="display:<?php if( $link_type == 'external_link'  ){ echo 'block';}else { echo 'none';}  ?>"  >
                        <div class="form-group-choice">
                            <div class="row d-block d-md-flex align-items-center">
                                <div class="col col-12 col-md-1">
                                    <label>Url Link<small></small></label>
                                </div>
                                <div class="col col-12 col-md-5">
                                    <div class="form-group">
                                        <input type="text" name="ex_link" class="form-control" placeholder="link out"
                                            value="<?php echo $link_out; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group internal-link " style="display:<?php if( $link_type == 'internal_link'  ){ echo 'block';}else { echo 'none';}  ?>">
                        <div class="form-group-choice">
                            <div class="row d-block d-md-flex align-items-center">
                                <div class="col col-12 col-md-1">
                                    <label>Internal Link<small></small></label>
                                </div>
                                <div class="col col-12 col-md-5">
                                    <select class="form-control" name="page_code">
                                        <option value="">Select the page</option>
                                        <?php foreach(  $pagesList as $page_menu ){ ?>
                                            <option value="<?php echo $page_menu['code']; ?>" <?php echo ( $code != '' and $page_menu['code'] == $page_internal_code )?'selected':''; ?>><?php  echo $page_menu['page_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php /*if($type == 'image' and $link_type == 'no'){ echo 'block'; }elseif($link_type == 'external_link' || $link_type == 'internal_link' ){echo 'none';}else{ if($video_type == 'youtube'){ echo 'none'; }else{ echo 'block'; }} */ ?>
            <?php
                $display = '';
                if ( $type == 'image' ) {
                    if ( $link_type == 'no' ) {
                        $display = 'block';
                    } else if ( $link_type == 'external_link' || $link_type == 'internal_link' ) {
                        $display = 'none';
                    }
                } else if ( $type == 'video' ) {
                    if ( $video_type == 'youtube' ) {
                        $display = 'none';
                    } else {
                        $display = 'block'; // loop
                    }
                }
            ?>
            <div class="form-group float-left w-100 content <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>"  style="display:<?php echo $display; ?>">
                <ul class="nav nav-tabs" role="tablist" style="">
                    <?php foreach( $langs as $lang ){ ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                            id="<?php echo $lang['text']; ?>-tab" data-toggle="tab" href="#<?php echo $lang['text']; ?>" role="tab"
                            aria-controls="<?php echo $lang['text']; ?>" aria-selected="true"><?php echo $lang['text']; ?></a>
                    </li>
                    <?php } ?>
                </ul>
                <div class="tab-content float-left w-100" style="">
                    <?php foreach( $langs as $lang ){ ?>
                    <?php
                        if( isset( $detail[ $lang['text'] ] ) ){
                            $content_editor = $detail[ $lang['text'] ]['content_editor'];
                        }else{
                            $content_editor = '';
                        }
                    ?>
                    <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                        id="<?php echo $lang['text']; ?>" role="tabpanel" aria-labelledby="<?php echo $lang['text']; ?>-tab">

                        <div class="form-group">
                            <label for="">Description </label>
                            <div class="install-nexitor float-left mb-3" data-area="content">
                                <?php echo $content_editor; ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="form-group float-left w-100">
                        <div class="float-left d-flex align-items-center ">
                            <label class="mr-4" for="content_position ">Content position (Desktop)</label>
                            <ul class="alignment-menu mx-2" style="z-index:10;">
                                <li class="alignment-menu-item">
                                    <input type="radio" id="left" class="form-check-input" name="content_position_hoz" value="left" <?php echo $content_position_hoz == 'left' ? 'checked' : ''; ?> >
                                    <label for="left" class="justify-content-center">Left</label>
                                </li>
                                <li class="alignment-menu-item">
                                    <input type="radio" id="center" class="form-check-input" name="content_position_hoz" value="center"
                                        <?php echo $content_position_hoz == 'center' ? 'checked' : ''; ?>>
                                    <label for="center" class="justify-content-center">Center</label>
                                </li>
                                <li class="alignment-menu-item">
                                    <input type="radio" id="right" class="form-check-input" name="content_position_hoz" value="right"
                                        <?php echo $content_position_hoz == 'right' ? 'checked' : ''; ?>>
                                    <label for="right" class="justify-content-center">Right</label>
                                </li>
                            </ul>

                            <ul class="alignment-menu mx-2" style="z-index:10;">
                                <li class="alignment-menu-item">
                                    <input type="radio" id="top" class="form-check-input" name="content_position_ver" value="top" checked >
                                    <label for="top" class="justify-content-center">Top</label>
                                </li>
                                <li class="alignment-menu-item">
                                    <input type="radio" id="middle" class="form-check-input" name="content_position_ver" value="middle"
                                        <?php echo $content_position_ver == 'middle' ? 'checked' : ''; ?>>
                                    <label for="middle" class="justify-content-center">Middle</label>
                                </li>
                                <li class="alignment-menu-item">
                                    <input type="radio" id="bottom" class="form-check-input" name="content_position_ver" value="bottom"
                                        <?php echo $content_position_ver == 'bottom' ? 'checked' : ''; ?>>
                                    <label for="bottom" class="justify-content-center">Bottom</label>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group float-left w-100">
                        <div class="float-left d-flex align-items-center ">
                            <label class="mr-4" for="content_position ">Content position (Mobile)</label>
                            <ul class="alignment-menu mx-2" style="z-index:10;">
                                <li class="alignment-menu-item">
                                    <input type="radio" id="left_m" class="form-check-input" name="content_position_hoz_m" value="left" <?php echo $content_position_hoz_m == 'left' ? 'checked' : ''; ?> >
                                    <label for="left_m" class="justify-content-center">Left</label>
                                </li>
                                <li class="alignment-menu-item">
                                    <input type="radio" id="center_m" class="form-check-input" name="content_position_hoz_m" value="center"
                                        <?php echo $content_position_hoz_m == 'center' ? 'checked' : ''; ?>>
                                    <label for="center_m" class="justify-content-center">Center</label>
                                </li>
                                <li class="alignment-menu-item">
                                    <input type="radio" id="right_m" class="form-check-input" name="content_position_hoz_m" value="right"
                                        <?php echo $content_position_hoz_m == 'right' ? 'checked' : ''; ?>>
                                    <label for="right_m" class="justify-content-center">Right</label>
                                </li>
                            </ul>

                            <ul class="alignment-menu mx-2" style="z-index:10;">
                                <li class="alignment-menu-item">
                                    <input type="radio" id="top_m" class="form-check-input" name="content_position_ver_m" value="top" <?php echo $content_position_ver_m == 'top' ? 'checked' : ''; ?> >
                                    <label for="top_m" class="justify-content-center">Top</label>
                                </li>
                                <li class="alignment-menu-item">
                                    <input type="radio" id="middle_m" class="form-check-input" name="content_position_ver_m" value="middle"
                                        <?php echo $content_position_ver_m == 'middle' ? 'checked' : ''; ?>>
                                    <label for="middle_m" class="justify-content-center">Middle</label>
                                </li>
                                <li class="alignment-menu-item">
                                    <input type="radio" id="bottom_m" class="form-check-input" name="content_position_ver_m" value="bottom"
                                        <?php echo $content_position_ver_m == 'bottom' ? 'checked' : ''; ?>>
                                    <label for="bottom_m" class="justify-content-center">Bottom</label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
            
            <div class="form-group float-left w-100">
                <label for="">Order</label>
                <input type="text" name="sticky" class="form-control" placeholder="Sticky" value="<?php echo $sticky; ?>">
            </div>
            <div class="form-group float-left w-100">
                <label for="">Status</label>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="1"
                            <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
                </div>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="0"
                            <?php echo $onoff == 0 ? 'checked' : ''; ?>> Unpublish </label>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <div class="text-center card-button">
                        <!--<button type="submit" class="btn btn-gradient-light btn-fw-lg mr-2">Cancle</button>-->
                        <?php /*if( sizeof( $detail ) > 0 ){ ?><a class="btn btn-gradient-light btn-fw-lg mr-2 btn-nexitor-preview"
                            data-toggle="modal" data-target="#nexitorModal">Preview</a><?php } */ ?>
                        <button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
                    </div>
                </div>
            </div>


            
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<!-- content-wrapper ends -->
<?php include_once("inc/nexitor-modal.php"); ?>