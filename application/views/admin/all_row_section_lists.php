<?php
//print_r( $lists );
if( sizeof( $detail ) > 0 ){
    $code = $detail[DEFAULT_LANG]['code'];
    $page_name = $detail[DEFAULT_LANG]['page_name'];
    $parent_code = $detail[DEFAULT_LANG]['parent_code'];
    $page_slug = $detail[DEFAULT_LANG]['page_slug'];

    $onoff = $detail[DEFAULT_LANG]['onoff'];

    $page_permission = $detail[DEFAULT_LANG]['page_permission'] == '' ? array() : explode(",",$detail[DEFAULT_LANG]['page_permission']);

  }else{
    $code = '';
    $page_name = '';
    $parent_code = 0;
    $page_slug = '';
    $onoff = 1;
    $page_permission = array('SA','A');
  }
  if( sizeof( $seo ) > 0 ){
    $share_img = $seo[DEFAULT_LANG]['share_img'];
  }else{
    $share_img = '';
  }
?>
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-dark text-white mr-2"><i class="mdi mdi-home"></i></span>PAGE :
            <?php echo strtoupper( $page_name) ?>
        </h3>
        <?php foreach( $pages_names as $pages_name ){ ?>
        <?php // echo $parent['code'] , $page_code ,$parent['parent_code'], $page ?>

        <?php if($pages_name['parent_code'] == 0){ ?>
        <a href="<?php echo base_url().'th/'.$pages_name['page_slug']?>" target="_blank"
            class="btn btn-gradient-light btn-fw">View page</a>
        <?php }else{ ?>
        <?php foreach( $parent_pages as $parent ){ ?>
        <?php if( $pages_name['parent_code'] == $parent['code'] ){ ?>
        <a href="<?php echo base_url().'th/'.$parent['page_slug'].'/'.$pages_name['page_slug']?>" target="_blank"
            class="btn btn-gradient-light btn-fw">View page</a>
        <?php } ?>
        <?php } ?>
        <?php } ?>
        <?php } ?>
    </div>

    <!-- <div class="card my-3">
        <div class="card-body">
            <div class="form-group">
                <label for="">Row</label>
                <div class=" row-panel">
                    <div class="row-list">
                        <div class="col-panel">
                            <div class="drag-section">
                                <div class="section-move">
                                    <div class="section-list">
                                        <?php // foreach($lists as $list){ ?>
                                            <div class="row">
                                                <div class="col-12">
                                                    <?php // echo $list['section_name'] ?>
                                                </div>
                                            </div>
                                        <?php // } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="card my-3">
        <div class="card-body">
            <div class="form-group">
                <label for="">Row</label>
                <div class="row-panel">
                    <ul class="row-move" data-page="<?php echo $code; ?>">
                        <?php foreach( $lists as $list ) {  ?>
                        <li class="drag-row" data-row="<?php echo $list['id']; ?>">
                            <div class="modal fade bgSetting" id="row-setting-<?php echo $list['id']; ?>" tabindex="-1"
                                role="dialog" aria-labelledby="row-setting-<?php echo $list['id']; ?>"
                                aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <?php echo form_open_multipart('admin/section/saveRowSetting', ' class="mainForm"'); ?>
                                        <input type="hidden" value="<?php echo $list['id']; ?>" name="id">
                                        <input type="hidden" value="<?php echo $code; ?>" name="page_code">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Row Setting</h5>
                                            <button type="button" class="close nexitorPopup-close" data-dismiss="modal"
                                                aria-label="Close">
                                                <img src="/assets/admin/images/nexitor/icon-cancel.svg" class="svg">
                                            </button>
                                        </div>
                                        <div class="modal-body bgSetting-body">
                                            <div class="row">
                                                <div class="col-12 col-md-6 image-box">
                                                    <div class="form-group">
                                                        <label><small class="text-muted">Background
                                                                Desktop</small></label>
                                                        <input type="file" data-height="190" name="row_bg"
                                                            class="dropify" data-max-file-size="3000kb"
                                                            data-default-file="<?php echo  $list['row_bg'] != "" ? upload_path($list['row_bg']) : ''; ?>" />
                                                        <input type="hidden" class="image_as_text" name="row_bg_txt"
                                                            value="<?php echo $list['row_bg']; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 image-box">
                                                    <div class="form-group">
                                                        <label><small class="text-muted">Background
                                                                Mobile</small></label>
                                                        <input type="file" data-height="190" name="row_bg_m"
                                                            class="dropify" data-max-file-size="3000kb"
                                                            data-default-file="<?php echo  $list['row_bg_m'] != "" ? upload_path($list['row_bg_m']) : ''; ?>" />
                                                        <input type="hidden" class="image_as_text" name="row_bg_m_txt"
                                                            value="<?php echo $list['row_bg_m']; ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mt-3">
                                                <div class="row d-block d-md-flex align-items-center">
                                                    <div class="col col-12 col-md-4"><label>Background Color</label>
                                                    </div>
                                                    <div class="col col-12 col-md-4">
                                                        <input type='text' name="bg_color"
                                                            class="color-picker form-control" data-mode="complex"
                                                            value="<?php echo $list['bg_color'] ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row d-block d-md-flex align-items-center">
                                                    <div class="col col-12 col-md-4"><label>Background position :
                                                        </label></div>
                                                    <div class="col col-12 col-md-8">
                                                        <div class="fg-choice-group">
                                                            <ul class="alignment-position fit-width">
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="bg_position_hoz_left_<?php echo $list['id']; ?>"
                                                                        name="bg_position_hoz" value="left"
                                                                        <?php echo $list['bg_position_hoz'] == 'left' ? 'checked' : ''; ?>>
                                                                    <label class="justify-content-center"
                                                                        for="bg_position_hoz_left_<?php echo $list['id']; ?>">Left</label>
                                                                </li>
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="bg_position_hoz_center_<?php echo $list['id']; ?>"
                                                                        name="bg_position_hoz" value="center" <?php echo $list['bg_position_hoz'] == 'center' ? 'checked' : ''; ?>>
                                                                    <label class="justify-content-center"
                                                                        for="bg_position_hoz_center_<?php echo $list['id']; ?>">Center</label>
                                                                </li>
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="bg_position_hoz_right_<?php echo $list['id']; ?>"
                                                                        name="bg_position_hoz" value="right"
                                                                        <?php echo $list['bg_position_hoz'] == 'right' ? 'checked' : ''; ?>>
                                                                    <label class="justify-content-center"
                                                                        for="bg_position_hoz_right_<?php echo $list['id']; ?>">Right</label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row d-block d-md-flex align-items-center ">
                                                    <div class="col col-12 col-md-4"><label></label></div>
                                                    <div class="col col-12 col-md-8">
                                                        <div class="fg-choice-group">
                                                            <ul class="alignment-position fit-width">
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="bg_position_ver_top_<?php echo $list['id']; ?>"
                                                                        name="bg_position_ver" value="top" checked>
                                                                    <label class="justify-content-center"
                                                                        for="bg_position_ver_top_<?php echo $list['id']; ?>">Top</label>
                                                                </li>
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="bg_position_ver_middle_<?php echo $list['id']; ?>"
                                                                        name="bg_position_ver" value="middle"
                                                                        <?php echo $list['bg_position_ver'] == 'middle' ? 'checked' : ''; ?>>
                                                                    <label class="justify-content-center"
                                                                        for="bg_position_ver_middle_<?php echo $list['id']; ?>">Middle </label>
                                                                </li>
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="bg_position_ver_bottom_<?php echo $list['id']; ?>"
                                                                        name="bg_position_ver" value="bottom"
                                                                        <?php echo $list['bg_position_ver'] == 'bottom' ? 'checked' : ''; ?>>
                                                                    <label class="justify-content-center"
                                                                        for="bg_position_ver_bottom_<?php echo $list['id']; ?>">Bottom</label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row d-block d-md-flex align-items-center">
                                                    <div class="col col-12 col-md-4"><label>Background repeat : </label>
                                                    </div>
                                                    <div class="col col-12 col-md-8">
                                                        <div class="fg-choice-group">
                                                            <ul class="alignment-position fit-width">
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="no_repeat_<?php echo $list['id']; ?>" name="bg_repeat"
                                                                        value="no_repeat" checked>
                                                                    <label class="justify-content-center"
                                                                        for="no_repeat_<?php echo $list['id']; ?>">No-repeat</label>
                                                                </li>
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="repeat_all_<?php echo $list['id']; ?>" name="bg_repeat"
                                                                        value="repeat_all"
                                                                        <?php echo $list['bg_repeat'] == 'repeat_all' ? 'checked' : ''; ?>>
                                                                    <label class="justify-content-center"
                                                                        for="repeat_all_<?php echo $list['id']; ?>">Repeat all </label>
                                                                </li>
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="repeat_x_<?php echo $list['id']; ?>" name="bg_repeat"
                                                                        value="repeat_x"
                                                                        <?php echo $list['bg_repeat'] == 'repeat_x' ? 'checked' : ''; ?>>
                                                                    <label class="justify-content-center"
                                                                        for="repeat_x_<?php echo $list['id']; ?>">Repeat X </label>
                                                                </li>
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="repeat_y_<?php echo $list['id']; ?>" name="bg_repeat"
                                                                        value="repeat_y"
                                                                        <?php echo $list['bg_repeat']== 'repeat_y' ? 'checked' : ''; ?>>
                                                                    <label class="justify-content-center"
                                                                        for="repeat_y_<?php echo $list['id']; ?>">Repeat Y </label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row d-block d-md-flex align-items-center">
                                                    <div class="col col-12 col-md-4"><label>Background fixed : </label>
                                                    </div>
                                                    <div class="col col-12 col-md-8">
                                                        <div class="fg-choice-group">
                                                            <ul class="alignment-position fit-width">
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="static_<?php echo $list['id']; ?>" name="bg_fixed"
                                                                        value="static" checked>
                                                                    <label class="justify-content-center"
                                                                        for="static_<?php echo $list['id']; ?>">Static</label>
                                                                </li>
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="fixed_<?php echo $list['id']; ?>" name="bg_fixed"
                                                                        value="fixed"
                                                                        <?php echo $list['bg_fixed'] == 'fixed' ? 'checked' : ''; ?>>
                                                                    <label class="justify-content-center"
                                                                        for="fixed_<?php echo $list['id']; ?>">Fixed</label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row d-block d-md-flex align-items-center">
                                                    <div class="col col-12 col-md-4"><label>Background size : </label>
                                                    </div>
                                                    <div class="col col-12 col-md-8">
                                                        <div class="fg-choice-group">
                                                            <ul class="alignment-position fit-width">
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="cover_<?php echo $list['id']; ?>" name="bg_size"
                                                                        value="cover" checked>
                                                                    <label class="justify-content-center"
                                                                        for="cover_<?php echo $list['id']; ?>">Cover</label>
                                                                </li>
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="contain_<?php echo $list['id']; ?>" name="bg_size"
                                                                        value="contain"
                                                                        <?php echo $list['bg_size'] == 'contain' ? 'checked' : ''; ?>>
                                                                    <label class="justify-content-center"
                                                                        for="contain_<?php echo $list['id']; ?>">Contain</label>
                                                                </li>
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="auto_<?php echo $list['id']; ?>" name="bg_size"
                                                                        value="auto"
                                                                        <?php echo $list['bg_size'] == 'auto' ? 'checked' : ''; ?>>
                                                                    <label class="justify-content-center"
                                                                        for="auto_<?php echo $list['id']; ?>">Auto</label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row d-block d-md-flex align-items-center">
                                                    <div class="col col-12 col-md-4"><label>Container layout: </label>
                                                    </div>
                                                    <div class="col col-12 col-md-8">
                                                        <div class="fg-choice-group">
                                                            <ul class="alignment-position fit-width">
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="is_full_<?php echo $list['id']; ?>" name="is_container"
                                                                        value="0" checked>
                                                                    <label class="justify-content-center"
                                                                        for="is_full_<?php echo $list['id']; ?>">fullwidth</label>
                                                                </li>
                                                                <li class="alignment-position-item">
                                                                    <input type="radio" id="is_con_<?php echo $list['id']; ?>" name="is_container"
                                                                        value="1"
                                                                        <?php echo $list['is_container'] == '1' ? 'checked' : ''; ?>>
                                                                    <label class="justify-content-center"
                                                                        for="is_con_<?php echo $list['id']; ?>">Container</label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="form-check mt-3 form-check-flat form-check-default">
                                                <label class="form-check-label"><input type="checkbox" value="1"
                                                        name="is_container" class="form-check-input" <?php // echo $list['is_container'] == '1' ? 'checked' : ''; ?>> Is container <i
                                                        class="input-helper"></i>
                                                </label>
                                            </div> -->
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <div class="form-check form-check-dark">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input" name="onoff"
                                                            value="1"
                                                            <?php  echo $list['onoff'] == 1 ? 'checked' : '';  ?>>
                                                        Publish </label>
                                                </div>
                                                <div class="form-check form-check-dark">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input" name="onoff"
                                                            value="0"
                                                            <?php echo $list['onoff'] == 0 ? 'checked' : '';  ?>> Unpublish </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-gradient-dark btn-fw">Submit</button>
                                        </div>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row-list">

                                <div class="row-control">
                                    <ul class="control-list">
                                    <?php  if( check_permission( $this->session->userdata('user_type'), 'edit_row') ){ ?>
                                        <li><i class="mdi mdi-settings" data-toggle="modal"
                                                data-target="#row-setting-<?php echo $list['id']; ?>"></i></li>
                                    
                                        <li class="icon-move"><i class="mdi mdi-cursor-move"></i></li>
                                    <?php } ?>
                                    <?php  if( check_permission( $this->session->userdata('user_type'), 'delete_row') ){ ?>
                                        <li><a href="javascript:void(0);" class="delete-item "
                                                onclick="deleteItem( $(this) );" data-delete-by="id" data-table="row"
                                                data-delete-type="row" data-code="<?php echo $list['id']; ?>"><i
                                                    class="mdi mdi-close"></i></a></li>
                                    <?php } ?>
                                    </ul>
                                </div>
                                <?php if( sizeof( $list['columns'] ) > 0 ){ ?>
                                <div class="row col-panel">
                                    <?php foreach ( $list['columns'] as $column ) { ?>
                                    <div
                                        class="col <?php echo sizeof($list['columns']) > 1 ? 'col-6 col-sm-6' : 'col-12 col-sm-12' ?> col-list">
                                        <div class="col-content w-100">
                                            <?php //if( sizeof( $column['sections'] ) > 0 ){ ?>
                                            <div class="drag-section" data-column=<?php echo $column['id']; ?>>
                                                <ul class="section-move">
                                                    <?php $i = 1; ?>
                                                    <?php foreach( $column['sections'] as $sec){ ?>
                                                    <li class="drag-list level-0  lists-box"
                                                        data-code="<?php echo $sec['code']; ?>" data-position="">
                                                        <div class="row align-items-center ">
                                                            <div class="col-3">
                                                                <img style="width:90%; height: auto; border-radius:0;"
                                                                    src="<?php  echo $sec['thumb'] == '' ? '/assets/admin/images/element/section/section_thumb.jpg' : upload_path($sec['thumb']); ?>">
                                                            </div>
                                                            <div class="col-2"><?php echo $sec['section_name'] ?></div>
                                                            
                                                            <div class="col-2">
                                                                <?php echo $sec['section_type']; ?>
                                                                <?php echo $sec['section_type'] == 'widget' ? ' : '.$sec['widget'] : ''; ?>
                                                            </div>

                                                            <div class="col-1 justify-content-center text-center">
                                                                <input type="checkbox"
                                                                    <?php echo $sec['onoff'] == 1 ? 'checked' : '' ?>
                                                                    data-toggle="toggle" data-onstyle="info"
                                                                    data-on="enable" data-off="disable"
                                                                    data-offstyle="outline-secondary" data-size="xs"
                                                                    name="onoff_switch" data-table="section"
                                                                    data-code="<?php echo $sec['code']; ?>"
                                                                    onchange="onoff( $(this) );">
                                                            </div>
                                                            
                                                            <div class="col-4">
                                                                <div style="padding: .4rem .4rem;" class="btn btn-info btn-inverse-dark"
                                                                    data-toggle="tooltip" data-placement="right"
                                                                    title="Created : <?php echo $sec['c_date']; ?> Updated : <?php echo $sec['u_date'];  ?>">
                                                                    <i class="mdi mdi-clock"></i>
                                                                </div>
                                                                <?php  if( check_permission( $this->session->userdata('user_type'), 'edit_section') ){ ?>
                                                                <a href="<?php echo base_url().'admin/section/edit/'.$list['page_code'].'/'.$sec['code']; ?>"
                                                                    class="btn btn-sm  btn-info" data-toggle="tooltip"
                                                                    data-placement="top" title="Edit"><i
                                                                        class="mdi mdi-lead-pencil"></i></a>
                                                                <?php } ?>
                                                                 <?php  if( check_permission( $this->session->userdata('user_type'), 'duplicate_section') ){ ?>    
                                                                <a href="<?php echo base_url().'admin/section/duplicate/'.$page_code.'/'.$sec['code']; ?>"
                                                                    class="btn btn-sm  btn-info" data-toggle="tooltip"
                                                                    data-placement="top" title="Duplicate"><i
                                                                        class="mdi mdi-content-duplicate"></i></a>
                                                                <?php } ?>
                                                                <input type="hidden" name="" value="" />
                                                                
                                                                <?php  if( check_permission( $this->session->userdata('user_type'), 'delete_section') ){ ?>
                                                                <a href="javascript:void(0);"
                                                                    class="btn btn-sm btn-danger delete-item "
                                                                    onclick="deleteItem( $(this) );"
                                                                    data-delete-by="code" data-table="section"
                                                                    data-delete-type="section"
                                                                    data-code="<?php echo $sec['code']; ?>"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    title="Delete"><i class="mdi mdi-delete"></i>
                                                                </a>
                                                                <?php } ?>
                                                                <?php if($sec['section_type'] == "listing"  ){?>
                                                                <a href="<?php echo base_url().'admin/section/categories/lists/'.$sec['code']; ?>
                                                                                " class="btn btn-sm btn-success"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    title="Category"><i
                                                                        class="mdi mdi-view-list"></i></a>
                                                                <?php } ?>
                                                                <?php if($sec['section_type'] == "banner"  ){?>
                                                                <a href="<?php echo base_url().'admin/section/banner/lists/'.$sec['code']; ?>
                                                                                " class="btn btn-sm btn-success"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    title="Banner"><i
                                                                        class="mdi mdi-image-area"></i></a>
                                                                <?php }else{ ?>

                                                                <?php } ?>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                            <!-- button -->
                                            <?php //} ?>
                                            <?php  if( check_permission( $this->session->userdata('user_type'), 'create_section') ){ ?>
                                            <div class="button-group mt-3 text-center w-100 float-left">
                                                <a href="<?php echo base_url().'admin/section/add/'.$code.'/'.$column['id']; ?>"
                                                    class="btn btn-gradient-dark btn-fw">+ Add section</a>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <?php  if( check_permission( $this->session->userdata('user_type'), 'create_row') ){ ?>
                    <div class="button-group mt-3 text-center w-100 float-left">
                        <a href="<?php echo base_url().'admin/pages/rows/add/'.$code.'/1'; ?>"
                            class="btn btn-gradient-dark btn-fw">+ Add Row 1 column</a>
                        <a href="<?php echo base_url().'admin/pages/rows/add/'.$code.'/2'; ?>"
                            class="btn btn-gradient-dark btn-fw">+ Add Row 2 column</a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>


    <?php echo form_open_multipart('admin/pages/saveForm', ' class="mainForm"'); ?>
    <input type="hidden" name="code" value="<?php echo $code; ?>">

    <input type="hidden" name="time" value="<?php echo $time; ?>">
    <div class="card mb-3" style="<?php  echo check_permission( $this->session->userdata('user_type'), 'edit_pages') ?  'display:block'  :  'display:none'  ?>">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="">Page Name</label>
                        <input type="text" name="page_name" class="form-control" placeholder="Page Name"
                            value="<?php echo $page_name;  ?>">
                    </div>
                    <!-- <div class="form-group">
                        <label for="">Page Slug</label>
                        <input type="text" value="<?php //echo  $page_slug; ?>" name="page_slug" />
                    </div> -->
                    
                    <?php if( $page_slug == ""){ ?>
                    <div class="form-group">
                        <label for="">Slug <span class="text-muted">( * Only character and number split with -
                                )</span></label>
                        <input type="text" name="page_slug" class="form-control"
                            placeholder="Ex : domain.com/your-friendly-slug" value="<?php echo $page_slug; ?>">
                    </div>
                    <?php }else{ ?>
                        <div class="form-group">
                        <label for="">Slug <span class="text-muted">( * Only character and number split with - )</span></label>
                    <input type="text"class="form-control"  value="<?php echo  $page_slug; ?>" name="page_slug" />
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="">Parent Page</label>
                        <select class="form-control" name="parent_code">
                            <option value=""> Select parent page </option>
                            <?php foreach( $parent_pages as $page ){ ?>
                            <option value="<?php echo $page['code'] ?>"
                                <?php echo $page['code'] == $parent_code ? 'selected' : '';  ?>>
                                <?php echo $page['page_name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Page Permission</label>
                        <?php $all_role = getRole(); ?>
                        <select class="js-example-basic-multiple form-control" multiple="multiple" name="page_permission[]" style="width:100%">
                            <?php foreach( $all_role as $role ){ ?>
                                <option value="<?php echo $role['role_code']; ?>" <?php echo in_array($role['role_code'], $page_permission ) ? 'selected' : ''; ?>><?php echo $role['role_text']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Order</label>
                        <input type="text" name="sticky" class="form-control" placeholder="order"
                                value="<?php echo  $page['sticky']; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="card mb-3" style="<?php  echo check_permission( $this->session->userdata('user_type'), 'page_seo') ?  'display:block'  :  'display:none'  ?>" >
        <div class="card-body">
            <h3 class="page-title mb-3"> SEO Page setting </h3>
            <div class="top-right-switch seo-switch">
                <input type="checkbox" <?php // echo $seo_status == 1 ? 'checked' : '' ?> data-toggle="toggle"
                    data-onstyle="info" data-on="Show" data-off="Hide" data-offstyle="outline-secondary" data-size="xs"
                    name="seo_status_switch">
                <input type="hidden" name="seo_status" value="<?php // echo $seo_status; ?>">
            </div>
            <div class="seo-toggle-wrap" style="display:none;">
                <div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
                    <ul class="nav nav-tabs" role="tablist" style="">
                        <?php foreach( $langs as $lang ){ ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
                                href="#<?php echo 'title_'.$lang['text']; ?>" role="tab"
                                aria-controls="<?php echo 'title_'.$lang['text']; ?>"
                                aria-selected="true"><?php echo $lang['text']; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content float-left w-100" style="">
                        <?php foreach( $langs as $lang ){ ?>
                        <?php
						if( isset( $detail[ $lang['text'] ] ) ){
							$page_title = $seo[ $lang['text'] ]['page_title'];
							$meta_title = $seo[ $lang['text'] ]['meta_title'];
							$meta_desc = $seo[ $lang['text'] ]['meta_desc'];
							$meta_keyword = $seo[ $lang['text'] ]['meta_keyword'];
						}else{
							$page_title = '';
							$meta_title = '';
							$meta_desc = '';
							$meta_keyword = '';
						}
						?>
                        <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                            id="<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
                            aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
                            <div class="form-group">
                                <label for="">Page title</label>
                                <input type="text" name="page_title[]" class="form-control" placeholder="Page title"
                                    value="<?php echo $page_title; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Meta title</label>
                                <input type="text" name="meta_title[]" class="form-control" placeholder="Meta title"
                                    value="<?php echo $meta_title; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Meta keyword </label>
                                <input type="text" name="meta_keyword[]" class="form-control meta_keyword" placeholder="Meta Keyword"
                                    value="<?php echo $meta_keyword; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Meta description</label>
                                <input type="text" name="meta_desc[]" class="form-control"
                                    placeholder="Meta Description" value="<?php echo $meta_desc; ?>">
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-12 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
                            <label>Share Image<small class="text-muted">( 1200 x 630px )</small></label>
                            <input type="file" data-height="250" name="share_img" class="dropify"
                                data-max-file-size="3000kb"
                                data-default-file="<?php echo $share_img != "" ? upload_path($share_img) : ''; ?>" />
                            <input type="hidden" class="image_as_text" name="share_img_txt"
                                value="<?php echo $share_img; ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <div class="form-group" style="<?php  echo check_permission( $this->session->userdata('user_type'), 'edit_pages') ?  'display:block'  :  'display:none'  ?>">
                <label for="">Status</label>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="1"
                            <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
                </div>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="0"
                            <?php echo $onoff == 0 ? 'checked' : '';  ?>> Unpublish </label>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <div class="text-center card-button">
                        <button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>

</div>