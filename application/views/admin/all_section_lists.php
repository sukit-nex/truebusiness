<?php
if( sizeof( $detail ) > 0 ){
    $code = $detail[DEFAULT_LANG]['code'];
    $page_name = $detail[DEFAULT_LANG]['page_name'];
    $parent_code = $detail[DEFAULT_LANG]['parent_code'];
    $page_slug = $detail[DEFAULT_LANG]['page_slug'];

    $onoff = $detail[DEFAULT_LANG]['onoff'];;
  }else{
    $code = '';
    $page_name = '';
    $parent_code = 0;
    $page_slug = '';
    $onoff = 1;
  }
  if( sizeof( $seo ) > 0 ){
    $share_img = $seo[DEFAULT_LANG]['share_img'];
  }else{
    $share_img = '';
  }
?>
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-dark text-white mr-2"><i class="mdi mdi-home"></i></span>PAGE :
            <?php echo strtoupper( $page_name) ?>
        </h3>
        <div class="">
            <a href="<?php echo base_url().'th/'.$page_slug; ?> " target="_blank"
                class="btn btn-gradient-light btn-fw">View page</a>
            <a href="<?php echo base_url().'admin/section/add/'.$code; ?>" class="btn btn-gradient-dark btn-fw">+ Add
                section</a>
        </div>

    </div>
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <div class="drag-section-wrapper">
                            <div class="hearder-table">
                                <div class="row">
                                    <div class="col-3">Section</div>
                                    <div class="col-3">Name</div>
                                    <div class="col-1">Post log</div>
                                    <div class="col-2 text-center">Status</div>
                                    <div class="col-3">Action</div>
                                </div>
                            </div>
                            <?php if( sizeof( $lists ) == 0 ){ ?>
                            <div class="row">
                                <div class="col-12 mt-3 text-center">
                                    <p>No data available in table</p>
                                </div>
                            </div>

                            <?php } ?>
                            <div class="drag-section">
                                <ul class="section-move">

                                    <?php $i = 1; ?>
                                    <?php foreach($lists as $list){ ?>
                                    <li class="drag-list level-0  lists-box" data-code="<?php echo $list['code']; ?>"
                                        data-position="<?php /* echo  $list['position']; */ ?>">
                                        <div class="row align-items-center ">
                                            <div class="col-3">
                                                <img style="width:90%; height: auto; border-radius:0;"
                                                    src="<?php  echo $list['thumb'] == '' ? '/assets/admin/images/element/section/section_thumb.jpg' : upload_path($list['thumb']); ?>">
                                            </div>
                                            <div class="col-3"><?php echo $list['section_name'] ?></div>

                                            <div class="col-1">
                                                <div class="badge badge-pill badge-danger btn-inverse-dark"
                                                    data-toggle="tooltip" data-placement="right"
                                                    title="Created : <?php echo $list['c_date']; ?> Updated : <?php echo $list['u_date'];  ?>">
                                                    <i class="mdi mdi-clock"></i> Date posted
                                                </div>
                                            </div>
                                            <div class="col-2 justify-content-center text-center">
                                                <input type="checkbox"
                                                    <?php echo $list['onoff'] == 1 ? 'checked' : '' ?>
                                                    data-toggle="toggle" data-onstyle="info" data-on="enable"
                                                    data-off="disable" data-offstyle="outline-secondary" data-size="xs"
                                                    name="onoff_switch" data-table="section"
                                                    data-code="<?php echo $list['code']; ?>"
                                                    onchange="onoff( $(this) );">
                                            </div>
                                            <div class="col-3">
                                                <a href="<?php echo base_url().'admin/section/duplicate/'.$list['page_code'].'/'.$list['code']; ?>"
                                                    class="btn btn-sm  btn-info">Duplicate</a>
                                                <a href="<?php echo base_url().'admin/section/edit/'.$list['page_code'].'/'.$list['code']; ?>"
                                                    class="btn btn-sm  btn-info">Edit</a>
                                                <input type="hidden" name="" value="" />

                                                <a href="javascript:void(0);" class="btn btn-sm btn-danger delete-item "
                                                    onclick="deleteItem( $(this) );" data-toggle="modal"
                                                    data-target="#deleteModal" data-delete-by="code"
                                                    data-table="section" data-code="<?php echo $list['code']; ?>">Delete
                                                </a>

                                                <?php if($list['section_type'] == "listing"  ){?>
                                                <a href="<?php echo base_url().'admin/section/categories/lists/'.$list['code']; ?>
                                                        " class="btn btn-sm btn-success">Category</a>
                                                <?php } ?>
                                                <?php if($list['section_type'] == "banner"  ){?>
                                                <a href="<?php echo base_url().'admin/section/banner/lists/'.$list['code']; ?>
                                                        " class="btn btn-sm btn-success">Banner</a>
                                                <?php }else{ ?>

                                                <?php } ?>

                                            </div>
                                        </div>
                                    </li>
                                    <?php $i++ ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php echo form_open_multipart('admin/pages/saveForm', ' class="mainForm"'); ?>
    <input type="hidden" name="code" value="<?php echo $code; ?>">

    <input type="hidden" name="time" value="<?php echo $time; ?>">
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="">Page Name</label>
                        <input type="text" name="page_name" class="form-control" placeholder="Page Name"
                            value="<?php echo $page_name;  ?>">
                    </div>
                    <?php if( $page_slug == ""){ ?>
                    <div class="form-group">
                        <label for="">Slug <span class="text-muted">( * Only character and number split with -
                                )</span></label>
                        <input type="text" name="page_slug" class="form-control"
                            placeholder="Ex : domain.com/your-friendly-slug" value="<?php echo $page_slug; ?>">
                    </div>
                    <?php }else{ ?>
                    <input type="hidden" value="<?php echo $page_slug; ?>" name="page_slug" />
                    <?php } ?>
                    <div class="form-group">
                        <label for="">Parent Page</label>
                        <select class="form-control" name="parent_code">
                            <option value=""> Select parent page </option>
                            <?php foreach( $parent_pages as $page ){ ?>
                            <option value="<?php echo $page['code'] ?>"
                                <?php echo $page['code'] == $parent_code ? 'selected' : '';  ?>>
                                <?php echo $page['page_name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="card mb-3">
        <div class="card-body">
            <h3 class="page-title mb-3"> SEO Page setting </h3>
            <div class="top-right-switch seo-switch">
                <input type="checkbox" <?php // echo $seo_status == 1 ? 'checked' : '' ?> data-toggle="toggle"
                    data-onstyle="info" data-on="Show" data-off="Hide" data-offstyle="outline-secondary" data-size="xs"
                    name="seo_status_switch">
                <input type="hidden" name="seo_status" value="<?php // echo $seo_status; ?>">
            </div>
            <div class="seo-toggle-wrap" style="display:none;">
                <div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
                    <ul class="nav nav-tabs" role="tablist" style="">
                        <?php foreach( $langs as $lang ){ ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
                                href="#<?php echo 'title_'.$lang['text']; ?>" role="tab"
                                aria-controls="<?php echo 'title_'.$lang['text']; ?>"
                                aria-selected="true"><?php echo $lang['text']; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content float-left w-100" style="">
                        <?php foreach( $langs as $lang ){ ?>
                        <?php
						if( isset( $detail[ $lang['text'] ] ) ){
							$page_title = $seo[ $lang['text'] ]['page_title'];
							$meta_title = $seo[ $lang['text'] ]['meta_title'];
							$meta_desc = $seo[ $lang['text'] ]['meta_desc'];
							$meta_keyword = $seo[ $lang['text'] ]['meta_keyword'];
						}else{
							$page_title = '';
							$meta_title = '';
							$meta_desc = '';
							$meta_keyword = '';
						}
						?>
                        <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                            id="<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
                            aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
                            <div class="form-group">
                                <label for="">Page title</label>
                                <input type="text" name="page_title[]" class="form-control" placeholder="Page title"
                                    value="<?php echo $page_title; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Meta title</label>
                                <input type="text" name="meta_title[]" class="form-control" placeholder="Meta title"
                                    value="<?php echo $meta_title; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Meta keyword </label>
                                <input type="text" name="meta_keyword[]" class="form-control meta_keyword" placeholder="Meta Keyword"
                                    value="<?php echo $meta_keyword; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Meta description</label>
                                <input type="text" name="meta_desc[]" class="form-control"
                                    placeholder="Meta Description" value="<?php echo $meta_desc; ?>">
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-12 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
                            <label>Share Image<small class="text-muted">( 1200 x 630px )</small></label>
                            <input type="file" data-height="250" name="share_img" class="dropify"
                                data-max-file-size="3000kb"
                                data-default-file="<?php echo $share_img != "" ? upload_path($share_img) : ''; ?>" />
                            <input type="hidden" class="image_as_text" name="share_img_txt"
                                value="<?php echo $share_img; ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <div class="form-group">
                <label for="">Status</label>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="1"
                            <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
                </div>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="0"
                            <?php echo $onoff == 0 ? 'checked' : '';  ?>> Unpublish </label>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <div class="text-center card-button">
                        <button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>

</div>