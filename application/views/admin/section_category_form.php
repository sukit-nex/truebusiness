<?php
  if( sizeof( $detail ) > 0 ){
    $code = $detail[DEFAULT_LANG]['code'];
    $page = $detail[DEFAULT_LANG]['page'];
	$sec_key = $detail[DEFAULT_LANG]['sec_key'];
	
	$image = $detail[DEFAULT_LANG]['image'];
	$sticky = $detail[DEFAULT_LANG]['sticky'];
    //$bg1_mobile = $detail[DEFAULT_LANG]['bg1_mobile'];

    //$img1_desktop = $detail[DEFAULT_LANG]['img1_desktop'];
	//$img1_mobile = $detail[DEFAULT_LANG]['img1_mobile'];
	//$link_out = $detail[DEFAULT_LANG]['link_out'];

	$onoff = $detail[DEFAULT_LANG]['onoff'];
	$slug = $detail[DEFAULT_LANG]['slug'];
	$cate_permission = $detail[DEFAULT_LANG]['cate_permission'] == '' ? array() : explode(",",$detail[DEFAULT_LANG]['cate_permission']);

	$must_be_approve = $detail[DEFAULT_LANG]['must_be_approve'];
	
	$bg_d = $detail[DEFAULT_LANG]['bg_d'];
	$bg_m = $detail[DEFAULT_LANG]['bg_m'];
	$thumb_image = $detail[DEFAULT_LANG]['thumb_image'];

	$banner_top_d = $detail[DEFAULT_LANG]['banner_top_d'];
	$banner_top_m = $detail[DEFAULT_LANG]['banner_top_m'];
  }else{
   	$code = '';
    $page = $param_page;
	$sec_key = $param_sec_key;
	
	$image = '';
	$sticky = 0;
    //$bg1_mobile = '';

    //$img1_desktop = '';
	//$img1_mobile = '';'
	//$link_out = '';

	$onoff = 1;
	$slug = time();
	$cate_permission = array();

	$must_be_approve = 0;

	$bg_d = '';
	$bg_m = '';
	$thumb_image = '';

	$banner_top_d = '';
	$banner_top_m = '';
  }
?>

<div class="content-wrapper">

  <div class="page-header">
    <h3 class="page-title"> <?php echo strtoupper( 'Categories form of '.$sec_detail[DEFAULT_LANG]['section_name'].' section') ?></h3>
	<div class="">
	<?php if( sizeof( $detail ) > 0 ){ ?>
		<a href="<?php echo base_url().'admin/section/listing/lists/'.$sec_detail[DEFAULT_LANG]['code'].'/'.$code; ?>" class="btn btn-success">Go to Lists</a>
	<?php } ?>
		<a href="<?php echo base_url().'admin/section/categories/lists/'.$sec_detail[DEFAULT_LANG]['code']; ?>" class="btn btn-gradient-dark btn-fw"><- Back</a>
	</div>
    <?php /* <a href="<?php echo base_url().'admin/banner/add' ?>" class="btn btn-gradient-dark btn-fw">+ Add more</a> */ ?>
  </div>
  <?php if ( $code == '' ) { ?>
  	<div class="card mb-3">
		<div class="card-body">
			<div class="form-group">
				<div class="row">
                    <div class="col-md-3 col-sm-2 col-xs-12">
                        <div class="box-radio box-check-gray" onclick="" data-cate-for="">
                            <div class="form-check form-check-dark">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="categoty_type" value="exiting"
                                        onclick="$('.exiting-mode').show(); $('.form-mode').hide();" >
                                    Category from exiting
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="box-radio box-check-gray" onclick="" data-cate-for="">
                            <div class="form-check form-check-dark">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="categoty_type" value="new"
                                        onclick="$('.exiting-mode').hide(); $('.form-mode').show();" <?php echo ($method == 'edit' || $method == 'add')? 'checked' : ''; ?>>
                                    New Category
                                </label>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
  	<div class="form-mode" style="">
		<?php echo form_open_multipart('admin/section/saveCategoryForm', ' class="mainForm"'); ?>
		<input type="hidden" name="code" value="<?php echo $code; ?>" >

		<input type="hidden" name="sec_code" value="<?php echo $sec_detail[DEFAULT_LANG]['code']; ?>" >
		<input type="hidden" name="page" value="<?php echo $page; ?>" >
		<input type="hidden" name="sec_key" value="<?php echo $sec_key; ?>" >

		<input type="hidden" name="time" value="<?php echo $time; ?>" >

		<input type="hidden" name="section_template" value="<?php echo $sec_detail[DEFAULT_LANG]['section_template']; ?>" >

		<?php //include_once("inc/banner_content.php"); ?>
		
		
		<div class="card mb-3">
			<div class="card-body">
				<div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
					<ul class="nav nav-tabs" role="tablist" style="">
						<?php foreach( $langs as $lang ){ ?>
							<li class="nav-item">
								<a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab" href="#<?php echo 'title_'.$lang['text']; ?>" role="tab" aria-controls="<?php echo 'title_'.$lang['text']; ?>" aria-selected="true"><?php echo $lang['text']; ?></a>
							</li>
						<?php } ?>
					</ul>
					<div class="tab-content float-left w-100" style="">
						<?php foreach( $langs as $lang ){ ?>
							<?php
							//print_r($detail);
							if( isset( $detail[ $lang['text'] ] ) ){
								$title = $detail[ $lang['text'] ]['title'];
								$sub_title = $detail[ $lang['text'] ]['sub_title'];

								$prefix_title = $detail[ $lang['text'] ]['prefix_title'];
							}else{
								$title = '';
								$sub_title = '';

								$prefix_title = '';
							}
							?>
						<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo 'title_'.$lang['text']; ?>" role="tabpanel" aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
							<?php if ( $sec_detail[DEFAULT_LANG]['section_template'] == 'solution' ) { ?>
							<div class="form-group">
								<label for="">Prefix Title</label>
								<input type="text" name="prefix_title[]" class="form-control"  placeholder="Prefix Title" value="<?php echo $prefix_title; ?>">
							</div>
							<?php } ?>
							<div class="form-group">
								<label for="">Title</label>
								<input type="text" name="title[]" class="form-control"  placeholder="Title" value="<?php echo $title; ?>">
							</div>
							<div class="form-group">
								<label for="">Sub Title </label>
								<input type="text" name="sub_title[]" class="form-control"  placeholder="Sub-Title" value="<?php echo $sub_title; ?>">
							</div>
						</div>
						<?php } ?>
					</div>
				</div>

				<div class="form-group">
					<label for="">Slug</label>
					<input type="text" name="slug" class="form-control" placeholder="Slug"
						value="<?php echo $slug; ?>">
				</div>

				<div class="form-group form-group-choice">
					<div class="row d-block d-md-flex align-items-center">
						<div class="col col-12 col-md-2"><label>Must be approve</label></div>
						<div class="col col-12 col-md-10">
							<div class="">
								<ul class="alignment-position sm-width" style="z-index:10;">
									<li class="alignment-position-item">
										<input type="radio" id="is_approve" class="form-check-input" name="must_be_approve" value="1" checked>
										<label class="justify-content-center" for="is_approve">
											Yes
										</label>
									</li>
									<li class="alignment-position-item">
										<input type="radio" id="is_not_approve" class="form-check-input" name="must_be_approve" value="0"
											<?php echo $must_be_approve == 0 ? 'checked' : ''; ?>>
										<label class="justify-content-center" for="is_not_approve">
											No
										</label>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<?php if ( $sec_detail[DEFAULT_LANG]['section_template'] == 'solution' ) { ?>
		<div class="card mb-3">
			<div class="card-body">
				<div class="row mb-3">
					<div class="col-12 col-md-6 image-box">
						<div class="form-group">
							<label>
								Banner Top Desktop
								<i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Image : 000 x 000 px" data-original-title="Image recommended"></i>
							</label>
							<input type="file" data-height="190" name="banner_top_d"
								class="dropify" data-max-file-size="3000kb"
								data-default-file="<?php echo  $banner_top_d != "" ? upload_path($banner_top_d) : ''; ?>" />
							<input type="hidden" class="image_as_text" name="banner_top_d_txt"
								value="<?php echo $banner_top_d; ?>" />
						</div>
					</div>
					<div class="col-12 col-md-6 image-box">
						<div class="form-group">
							<label>
								Banner Top Mobile
								<i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Image : 000 x 000 px" data-original-title="Image recommended"></i>
							</label>
							<input type="file" data-height="190" name="banner_top_m"
								class="dropify" data-max-file-size="3000kb"
								data-default-file="<?php echo  $banner_top_m != "" ? upload_path($banner_top_m) : ''; ?>" />
							<input type="hidden" class="image_as_text" name="banner_top_m_txt"
								value="<?php echo $banner_top_m; ?>" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-md-4 image-box mb-3"> <?php /* data-max-height="100" data-max-width="100" */ ?>
                        <label>
							Thumb Image
							<i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Image : 000 x 000 px" data-original-title="Image recommended"></i>
						</label>
                        <input type="file" data-height="250" name="thumb_image" class="dropify"
                            data-max-file-size="3M"
                            data-default-file="<?php echo $thumb_image != "" ? upload_path($thumb_image) : ''; ?>" />
                        <input type="hidden" class="image_as_text" name="thumb_image_txt" value="<?php echo $thumb_image; ?>" />
                    </div>

					<div class="col-12 col-md-4 image-box">
						<div class="form-group">
							<label>
								Background Desktop
								<i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Image : 000 x 000 px" data-original-title="Image recommended"></i>
							</label>
							<input type="file" data-height="190" name="bg_d"
								class="dropify" data-max-file-size="3000kb"
								data-default-file="<?php echo  $bg_d != "" ? upload_path($bg_d) : ''; ?>" />
							<input type="hidden" class="image_as_text" name="bg_d_txt"
								value="<?php echo $bg_d; ?>" />
						</div>
					</div>
					<div class="col-12 col-md-4 image-box">
						<div class="form-group">
							<label>
								Background Mobile
								<i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Image : 000 x 000 px" data-original-title="Image recommended"></i>
							</label>
							<input type="file" data-height="190" name="bg_m"
								class="dropify" data-max-file-size="3000kb"
								data-default-file="<?php echo  $bg_m != "" ? upload_path($bg_m) : ''; ?>" />
							<input type="hidden" class="image_as_text" name="bg_m_txt"
								value="<?php echo $bg_m; ?>" />
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<?php } ?>

		<div class="card mb-3">
			<div class="card-body">
				<div class="form-group">
					<?php 
						$text_mute_d = "";
						$data_text_d = "";
						if( $param_sec_key == "experience"){
							$text_mute_d = '( 330px × 160px )';
							$data_text_d = 'data-max-width="330" data-max-height="160"';
						}elseif($param_sec_key == "seetheunseen"){
							$text_mute_d = '( 226px x 128px)';
							$data_text_d = "";
						}
					?>
					<div class="row">
					<?php /*
						<div class="col-12 image-box"> 
							<label>Image<small class="text-muted"><?php echo $text_mute_d; ?></small></label>
							<input type="file" data-height="250" name="image" class="dropify" <?php echo $data_text_d; ?> data-max-file-size="3000kb" data-default-file="<?php echo $image != "" ? upload_path($image) : ''; ?>" />
							<input type="hidden" class="image_as_text" name="image_txt" value="<?php echo $image; ?>" />
						</div>
					*/ ?>
					<input type="hidden" class="image_as_text" name="image_txt" value="<?php echo $image; ?>" />
						<!-- <div class="col-12 mt-3">
							<label for="">Link out</label>
							<input type="text" name="link_out" class="form-control" placeholder="Link out"
								value="<?php //echo $link_out; ?>">
						</div> -->
						<div class="col-12 mt-3">
							<label for="">Order</label>
							<input type="text" name="sticky" class="form-control"  placeholder="Order" value="<?php echo $sticky; ?>">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card mt-3">
			<div class="card-body">
				<div class="form-group">
					<label for="">Status</label>
					<div class="form-check form-check-dark">
						<label class="form-check-label"><input type="radio" class="form-check-input" name="onoff" value="1"  <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
					</div>
					<div class="form-check form-check-dark">
						<label class="form-check-label"><input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 ? 'checked' : ''; ?>> Unpublish </label>
					</div>
				</div>
				
				<div class="row">
					<div class="col-12 text-right">
						<div class="text-center card-button">
							<button type="submit" class="btn btn-gradient-dark btn-fw-lg mr-2">Save <?php echo $method  ?></button>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>

	<div class="exiting-mode" style="display:none;">
		<?php echo form_open_multipart('admin/section/useExitingCate', ' class="mainForm"'); ?>
			<input type="hidden" name="sec_code" value="<?php echo $sec_detail[DEFAULT_LANG]['code']; ?>" >
			<input type="hidden" name="page" value="<?php echo $page; ?>" >
			<input type="hidden" name="sec_key" value="<?php echo $sec_key; ?>" >
			<input type="hidden" name="time" value="<?php echo $time; ?>" >
			
			<div class="card mb-3">
				<div class="card-body">
				<?php /*
					<div class="form-group">
						<label for="">Slug</label>
						<input type="text" name="slug" class="form-control" placeholder="Slug"
							value="<?php echo $slug; ?>">
					</div>
					*/ ?>
					<div class="form-group">
						<select class="form-control" name="cate_code">
							<?php foreach( $categories as $cate ){ ?>
							<option value="<?php echo $cate['code']; ?>"><?php echo $cate['title']; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-12 text-right">
					<div class="text-center card-button">
						<button type="submit" class="btn btn-gradient-dark btn-fw-lg mr-2">Save</button>
					</div>
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>





</div>
<?php //include_once("inc/nexitor-modal.php"); ?>