<?php
if( sizeof( $detail ) > 0 ){
    $code = $detail[DEFAULT_LANG]['code'];
    $page_name = $detail[DEFAULT_LANG]['page_name'];
    $parent_code = $detail[DEFAULT_LANG]['parent_code'];
    $page_slug = $detail[DEFAULT_LANG]['page_slug'];

    $onoff = $detail[DEFAULT_LANG]['onoff'];
    $sticky = $detail[DEFAULT_LANG]['sticky'];

    $page_permission = $detail[DEFAULT_LANG]['page_permission'] == '' ? array() : explode(",",$detail[DEFAULT_LANG]['page_permission']);
  }else{
    $code = '';
    $page_name = '';
    $parent_code = 0;
    $page_slug = '';
    $onoff = 1;
    $sticky = '';
    $page_permission = array('SA','A');
  }
  if( sizeof( $seo ) > 0 ){
    $share_img = $seo[DEFAULT_LANG]['share_img'];
  }else{
    $share_img = '';
  }
?>
<div class="content-wrapper">
    <div class="mb-4">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-dark text-white mr-2"><i class="mdi mdi-home"></i></span>Page Form
        </h3>
    </div>

 <?php echo form_open_multipart('admin/pages/saveForm', ' class="mainForm"'); ?>
     <input type="hidden" name="code" value="<?php echo $code; ?>" >

     <input type="hidden" name="time" value="<?php echo $time; ?>" >
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="">Page Name</label>
                        <input type="text" name="page_name" class="form-control" placeholder="Page Name"
                            value="<?php echo $page_name;  ?>">
                    </div>
                    <?php if( $page_slug == ""){ ?>
                        <div class="form-group">
                            <label for="">Slug <span class="text-muted">( * Only character and number split with - )</span></label>
                            <input type="text" name="page_slug" class="form-control" placeholder="Ex : domain.com/your-friendly-slug"
                                value="<?php echo $page_slug; ?>">
                        </div>
                    <?php }else{ ?>
                        <input type="hidden" value="<?php echo $page_slug; ?>" name="page_slug" />
                    <?php } ?>
                    <div class="form-group">
                        <label for="">Parent Page</label>
                        <select class="form-control" name="parent_code">
                            <option value=""> Select parent page </option>
                            <?php foreach( $parent_pages as $page ){ ?>
                                <option value="<?php echo $page['code'] ?>" <?php echo $page['code'] == $parent_code ? 'selected' : '';  ?>><?php echo $page['page_name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Page Permission</label>
                        <?php $all_role = getRole(); ?>
                        <select class="js-example-basic-multiple form-control" multiple="multiple" name="page_permission[]" style="width:100%">
                            <?php foreach( $all_role as $role ){ ?>
                                <option value="<?php echo $role['role_code']; ?>" <?php echo in_array($role['role_code'], $page_permission ) ? 'selected' : ''; ?>><?php echo $role['role_text']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Order</label>
                        <input type="text" name="sticky" class="form-control" placeholder="order"
                                value="<?php echo $sticky; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="card mb-3">
        <div class="card-body">
            <h3 class="page-title mb-3"> SEO Page setting </h3>
            <div class="top-right-switch seo-switch">
                <input type="checkbox" <?php // echo $seo_status == 1 ? 'checked' : '' ?> data-toggle="toggle"
                    data-onstyle="info" data-on="Show" data-off="Hide" data-offstyle="outline-secondary" data-size="xs" name="seo_status_switch">
                <input type="hidden" name="seo_status" value="<?php // echo $seo_status; ?>">
            </div>
            <div class="seo-toggle-wrap" style="display:none;">
                <div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
				<ul class="nav nav-tabs" role="tablist" style="">
				<?php foreach( $langs as $lang ){ ?>
					<li class="nav-item">
						<a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab" href="#<?php echo 'title_'.$lang['text']; ?>" role="tab" aria-controls="<?php echo 'title_'.$lang['text']; ?>" aria-selected="true"><?php echo $lang['text']; ?></a>
					</li>
				<?php } ?>
				</ul>
				<div class="tab-content float-left w-100" style="">
					<?php foreach( $langs as $lang ){ ?>
						<?php
						if( isset( $detail[ $lang['text'] ] ) ){
							$page_title = $seo[ $lang['text'] ]['page_title'];
							$meta_title = $seo[ $lang['text'] ]['meta_title'];
							$meta_desc = $seo[ $lang['text'] ]['meta_desc'];
							$meta_keyword = $seo[ $lang['text'] ]['meta_keyword'];
						}else{
							$page_title = '';
							$meta_title = '';
							$meta_desc = '';
							$meta_keyword = '';
						}
						?>
					<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo 'title_'.$lang['text']; ?>" role="tabpanel" aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
						<div class="form-group">
							<label for="">Page title</label>
							<input type="text" name="page_title[]" class="form-control"  placeholder="Page title" value="<?php echo $page_title; ?>">
						</div>
						<div class="form-group">
							<label for="">Meta title</label>
							<input type="text" name="meta_title[]" class="form-control"  placeholder="Meta title" value="<?php echo $meta_title; ?>">
						</div>
						<div class="form-group">
							<label for="">Meta keyword </label>
							<input type="text" name="meta_keyword[]" class="form-control meta_keyword"  placeholder="Meta Keyword" value="<?php echo $meta_keyword; ?>">
						</div>
						<div class="form-group">
							<label for="">Meta description</label>
							<input type="text" name="meta_desc[]" class="form-control"  placeholder="Meta Description" value="<?php echo $meta_desc; ?>">
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-12 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
						<label>Share Image<small class="text-muted">( 1200 x 630px )</small></label>
						<input type="file" data-height="250" name="share_img" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $share_img != "" ? upload_path($share_img) : ''; ?>" />
						<input type="hidden" class="image_as_text" name="share_img_txt" value="<?php echo $share_img; ?>" />
					</div>
				</div>
			</div>
            </div>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <div class="form-group">
                <label for="">Status</label>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="1"
                            <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
                </div>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="0"
                            <?php echo $onoff == 0 ? 'checked' : '';  ?>> Unpublish </label>
                </div>
            </div>

            <div class="row">
                <div class="col-12 text-right">
                    <div class="text-center card-button">
                        <button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo form_close(); ?>

</div>