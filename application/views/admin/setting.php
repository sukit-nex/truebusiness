<?php
  if( sizeof( $detail ) > 0 ){
    $code = $detail[DEFAULT_LANG]['code'];
    $seo_status = $detail[DEFAULT_LANG]['seo_status'];
    $seo_id = $detail[DEFAULT_LANG]['seo_id'];

    $logo_top = $detail[DEFAULT_LANG]['logo_top'];
    $logo_foot = $detail[DEFAULT_LANG]['logo_foot'];
    $contact_banner = $detail[DEFAULT_LANG]['contact_banner'];

    $com_name = $detail[DEFAULT_LANG]['com_name'];
    $com_address = $detail[DEFAULT_LANG]['com_address'];
    $link_to_map = $detail[DEFAULT_LANG]['link_to_map'];
    $com_phone = $detail[DEFAULT_LANG]['com_phone'];
    $phone_call = $detail[DEFAULT_LANG]['phone_call'];
    $com_email = $detail[DEFAULT_LANG]['com_email'];
    $facebook = $detail[DEFAULT_LANG]['facebook'];
    $facebook2 = $detail[DEFAULT_LANG]['facebook2'];
    $twitter = $detail[DEFAULT_LANG]['twitter'];
    $line = $detail[DEFAULT_LANG]['line'];
    $youtube = $detail[DEFAULT_LANG]['youtube'];
    $instagram = $detail[DEFAULT_LANG]['instagram'];

    $corp_map = $detail[DEFAULT_LANG]['corp_map'];

  }else{
    $code = '';
    $seo_id = '';
    $seo_status = 1;

    $logo_top = '';
    $logo_foot = '';
    $contact_banner = '';

    $com_name = '';
    $com_address = '';
    $link_to_map = '';
    $com_phone = '';
    $phone_call = '';
    $com_email = '';
    $facebook = '';
    $facebook2 = '';
    $twitter = '';
    $line = '';
    $youtube = '';
    $instagram = '';
    $corp_map = '';
  }
?>
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title"> Setting Form </h3>
  </div>
  <?php echo form_open_multipart('admin/setting/saveForm', ' class="mainForm"'); ?>
  <div class="card mb-3">
    <div class="card-body">

      <input type="hidden" name="code" value="<?php echo $code ?>" >
      <input type="hidden" name="seo_id" value="<?php echo $seo_id; ?>" >
      <div class="form-group">
        <div class="row">
          <div class="col-6">
            <label>Logo Header <small class="text-muted">( 300px x 200px )</small></label>
            <input type="file" data-height="250" name="logo_top" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $logo_top != "" ? upload_path($logo_top) : ''; ?>" />
          </div>
          <div class="col-6 mb-3">
            <label>Logo Footer <small class="text-muted">( 300px x 200px )</small></label>
            <input type="file" data-height="250" name="logo_foot" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $logo_foot != "" ? upload_path($logo_foot) : ''; ?>" />
          </div>
          <div class="col-12">
            <label>Contact Banner <small class="text-muted">( 1160px x 865px )</small></label>
            <input type="file" data-height="250" name="contact_banner" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $contact_banner != "" ? upload_path($contact_banner) : ''; ?>" />
          </div>
        </div>
      </div>

    </div>
  </div>

  <div class="card mb-3">
    <div class="card-body">
      <h3 class="page-title mb-3"> Footer Information </h3>
      <div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
        <ul class="nav nav-tabs" role="tablist">
          <?php foreach( $langs as $lang ){ ?>
            <li class="nav-item">
              <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo $lang['text']; ?>-tab" data-toggle="tab" href="#<?php echo $lang['text']; ?>" role="tab" aria-controls="<?php echo $lang['text']; ?>" aria-selected="true"><?php echo $lang['text']; ?></a>
            </li>
          <?php } ?>
        </ul>
        <div class="tab-content">
          <?php foreach( $langs as $lang ){ ?>
            <?php
            if( isset( $detail[ $lang['text'] ] ) ){
      			  $com_name = $detail[ $lang['text'] ]['com_name'];
      			  $com_address = $detail[ $lang['text'] ]['com_address'];
              $copy_right = $detail[ $lang['text'] ]['copy_right'];
            }else{
              $com_name = '';
      			  $com_address = '';
              $copy_right = '';
            }
             ?>
          <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo $lang['text']; ?>" role="tabpanel" aria-labelledby="<?php echo $lang['text']; ?>-tab">
            <div class="form-group">
              <label for="">Company Name</label>
              <input type="text" name="com_name[]" class="form-control"  placeholder="Name" value="<?php echo $com_name; ?>">
            </div>

            <div class="form-group">
              <label for="">Company Address</label>
              <input type="text" name="com_address[]" class="form-control"  placeholder="Address" value="<?php echo $com_address; ?>">
            </div>
            <div class="form-group mb-0">
              <label for="">Copy right</label>
              <input type="text" name="copy_right[]" class="form-control"  placeholder="Copy right" value="<?php echo $copy_right; ?>">
            </div>

          </div>
          <?php } ?>

        </div>
      </div>
      <div class="form-group">
        <label for="">Phone 1</label>
        <input type="text" name="com_phone" class="form-control"  placeholder="Phone Number" value="<?php echo $com_phone; ?>">
      </div>
      <div class="form-group">
        <label for="">Phone 2</label>
        <input type="text" name="phone_call" class="form-control"  placeholder="Phone Number" value="<?php echo $phone_call; ?>">
      </div>
      <div class="form-group">
        <label for="">Map <span class="text-muted">( * Iframe google map )</span></label>
        <input type="text" name="corp_map" class="form-control"  placeholder="Iframe Map" value='<?php echo $corp_map; ?>'>
      </div>
      <?php /*
      <div class="form-group">
        <label for="">Link to contact us</label>
        <input type="text" name="com_email" class="form-control"  placeholder="Link to contact us" value="<?php echo $com_email; ?>">
      </div>
      <div class="form-group">
        <label for="">Link to Map <span class="text-muted">( * Link to google map )</span></label>
        <input type="text" name="link_to_map" class="form-control"  placeholder="URL Map" value="<?php echo $link_to_map; ?>">
      </div>
      <div class="form-group">
        <label for="">Facebook 2 <span class="text-muted">( * Link to facebook page 2 )</span></label>
        <input type="text" name="facebook2" class="form-control"  placeholder="Facebook Page 2" value="<?php echo $facebook2; ?>">
      </div>
      */ ?>
      <div class="form-group">
        <label for="">Facebook <span class="text-muted">( * Link to facebook page )</span></label>
        <input type="text" name="facebook" class="form-control"  placeholder="Facebook Page" value="<?php echo $facebook; ?>">
      </div>
      <div class="form-group">
        <label for="">Youtube <span class="text-muted">( * Link to youtube channel )</span></label>
        <input type="text" name="youtube" class="form-control"  placeholder="Youtube Chanel" value="<?php echo $youtube; ?>">
      </div>
      <?php /*
      <div class="form-group">
        <label for="">Twitter <span class="text-muted">( * Link to twitter page )</span></label>
        <input type="text" name="twitter" class="form-control"  placeholder="Twitter" value="<?php echo $twitter; ?>">
      </div>
      <div class="form-group">
        <label for="">Instagram <span class="text-muted">( * Link to instagram )</span></label>
        <input type="text" name="instagram" class="form-control"  placeholder="Instagram" value="<?php echo $instagram; ?>">
      </div>
      <div class="form-group">
        <label for="">Line <span class="text-muted">( * Link to line )</span></label>
        <input type="text" name="line" class="form-control"  placeholder="Line" value="<?php echo $line; ?>">
      </div>
      */ ?>
    </div>
  </div>

  <?php include_once("inc/seo_setting.php"); ?>

  <div class="card mb-3">
    <div class="card-body">
      <div class="row">
        <div class="col-12 text-right">
          <div class="text-center card-button">
            <button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?>
</div>
<!-- content-wrapper ends -->
<?php //include_once("inc/nexitor-modal.php"); ?>
