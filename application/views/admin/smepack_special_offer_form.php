<?php
	if( sizeof( $detail ) > 0 ){
		$code = $detail[DEFAULT_LANG]['code'];
		$name = $detail[DEFAULT_LANG]['name'];
		$icon_image = $detail[DEFAULT_LANG]['icon_image'];
		$category = $detail[DEFAULT_LANG]['category'];

		$btn_text = $detail[DEFAULT_LANG]['btn_text'];
		$btn_link_type = $detail[DEFAULT_LANG]['btn_link_type'];
		$btn_internal_page_code = $detail[DEFAULT_LANG]['btn_internal_page_code'];
		$btn_external_link = $detail[DEFAULT_LANG]['btn_external_link'];
		$btn_pdf_file = $detail[DEFAULT_LANG]['btn_pdf_file'];
		$btn_content_popup = $detail[DEFAULT_LANG]['btn_content_popup'];

		$onoff = $detail[DEFAULT_LANG]['onoff'];
		
	}else{
		$code = '';
		$name = '';
		$icon_image = '';
		$category = 'get_free';

		$btn_text = '';
		$btn_link_type = 'no_link';
		$btn_internal_page_code = '';
		$btn_external_link = '';
		$btn_pdf_file = '';
		$btn_content_popup = '';

		$onoff = 1;
	}
?>
<div class="content-wrapper">
	<div class="page-header">
		<h3 class="page-title"> Special offer Form </h3>
		<a href="<?php echo base_url().'admin/smepack/special_offer/lists'; ?>" class="btn btn-gradient-dark btn-fw"> <- Back</a>
	</div>
	<ul class="nav nav-tabs nav-tabs-lang" role="tablist" style="">
		<?php foreach( $langs as $lang ){ ?>
		<li class="nav-item">
			<a class="nav-link head-<?php echo $lang['text']; ?> <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" 
			href="javascript:activaTabNex('<?php echo $lang['text']; ?>');"><?php echo $lang['text']; ?></a>
		</li>
		<?php } ?>
	</ul>
	<?php echo form_open_multipart('admin/smepack/special_offer/saveForm', ' class="mainForm"'); ?>
		<input type="hidden" name="code" value="<?php echo $code ?>" >
		<input type="hidden" name="time" value="<?php echo $time; ?>">

	<div class="card">
        <div class="card-body">
			<div class="form-group mb-3">
				<label for="">Name </label>
				<input type="text" name="name" class="form-control"  placeholder="Name" value="<?php echo $name; ?>">
			</div>

			<div class="row">
				<div class="col-12 col-md-3 image-box">
					<div class="form-group">
						<label>Icon / Image<small class="text-muted"> (50x50) </small></label>
						<input type="file" data-height="190" name="icon_image"
							class="dropify" data-max-file-size="3000kb"
							data-default-file="<?php echo  $icon_image != "" ? upload_path($icon_image) : ''; ?>" />
						<input type="hidden" class="image_as_text" name="icon_image_txt"
							value="<?php echo $icon_image; ?>" />
					</div>
				</div>
				<div class="col-12 col-md-6 image-box">
					<div class="form-group <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
						<div class="tab-content tab-content-lang">
							<?php foreach( $langs as $lang ){ ?>
								<?php
								if( isset( $detail[ $lang['text'] ] ) ) {
									$description = $detail[ $lang['text'] ]['description'];
								} else {
									$description = '';
								}
								?>
								<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
									<div class="form-group mb-3">
										<label for="">Description </label>
										<textarea name="description[]" class="form-control descriptionInput tinyMceExample" rows="7" placeholder="Description" ><?php echo $description; ?></textarea>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-3 image-box">
					<div class="form-group">
						<label>Category</label>
						<div class="box-form-inner">
							<div class="form-check form-check-dark">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="category" value="get_free"  <?php echo $category == 'get_free' ? 'checked' : ''; ?>> รับฟรี </label>
							</div>
							<div class="form-check form-check-dark">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="category" value="top_up" <?php echo $category == 'top_up' ? 'checked' : ''; ?>> ซื้อเพิ่ม </label>
							</div>
							<div class="form-check form-check-dark">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="category" value="discounts_privileges" <?php echo $category == 'discounts_privileges' ? 'checked' : ''; ?>> ส่วนลด สิทธิพิเศษ </label>
							</div>
						</div>
					</div>

				</div>
			</div>
        </div>
    </div>
	
	<div class="card my-3">
		<div class="card-body">
			<h4 class="card-title">Link button</h4>
			<div class="ิborder p-3">
				<div class="form-group form-group-choice special_offer-linktype">
					
					<div class="row d-block d-md-flex align-items-center my-3 ">
						<div class="col col-12 col-md-2"><label>Link Type</label></div>
						<div class="col col-12 col-md-10">
							<div class="d-md-flex align-items-center nav" role="tablist">
								<div class="form-check form-check-dark mr-3 mb-0">
									<label class="form-check-label">
										<input type="radio" class="form-check-input btn_tab" name="btn_link_type" value="no_link" <?php echo $btn_link_type == 'no_link' ? 'checked' : ''; ?>  data-target="#no_link_content"> No Link </label>
								</div>
								<div class="form-check form-check-dark mr-3 mb-0">
									<label class="form-check-label">
										<input type="radio" class="form-check-input btn_tab" name="btn_link_type" value="internal_link"  <?php echo $btn_link_type == 'internal_link' ? 'checked' : ''; ?>  data-target="#internal_link_content" > Internal Link </label>
								</div>
								<div class="form-check form-check-dark mr-3 mb-0">
									<label class="form-check-label">
										<input type="radio" class="form-check-input btn_tab" name="btn_link_type" value="external_link" <?php echo $btn_link_type == 'external_link' ? 'checked' : ''; ?>  data-target="#external_link_content"> External Link </label>
								</div>
								<div class="form-check form-check-dark mr-3 mb-0">
									<label class="form-check-label">
										<input type="radio" class="form-check-input btn_tab" name="btn_link_type" value="pdf" <?php echo $btn_link_type == 'pdf' ? 'checked' : ''; ?>  data-target="#pdf_content"> PDF </label>
								</div>
								<div class="form-check form-check-dark mb-0">
									<label class="form-check-label">
										<input type="radio" class="form-check-input btn_tab" name="btn_link_type" value="content" <?php echo $btn_link_type == 'content' ? 'checked' : ''; ?>  data-target="#content_content"> Content </label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="showall-not-nolink" style="<?php echo ($btn_link_type == 'no_link')?'display:none':'display:block'; ?>">
						<div class="row d-block d-md-flex align-items-center">
							<div class="col col-12 col-md-2"><label>Text button</label></div>
							<div class="col col-12 col-md-10">
								<div class="tab-content tab-content-lang">
									<?php foreach( $langs as $lang ){ ?>
										<?php
										if( isset( $detail[ $lang['text'] ] ) ) {
											$btn_text = $detail[ $lang['text'] ]['btn_text'];
										} else {
											$btn_text = '';
										}
										?>
										<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
											<div class="form-group mb-3">
												<input type="text" name="btn_text[]" class="form-control" rows="7" placeholder="" value="<?php echo $btn_text; ?>" >
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>

					
					<div class="tab-content border-0 p-0">
						<div class="tab-pane <?php echo $btn_link_type == 'no_link' ? 'active' : ''; ?>" id="no_link_content" >
							
						</div>
						<div class="tab-pane <?php echo $btn_link_type == 'internal_link' ? 'active' : ''; ?>" id="internal_link_content" >
							<div class="row d-block d-md-flex align-items-center my-3">
								<div class="col col-12 col-md-2"><label>Internal Link</label></div>
								<div class="col col-12 col-md-10">
									<select class="form-control" name="btn_internal_page_code">
										<option value="">Select the page</option>
										<?php foreach(  $pagesList as $page_menu ){ ?>
											<option value="<?php echo $page_menu['code']; ?>" <?php echo ( $code != '' and $page_menu['code'] == $btn_internal_page_code )?'selected':''; ?>><?php  echo $page_menu['page_name']; ?></option>
										<?php } ?>
                                    </select>
								</div>
							</div>
						</div>
						<div class="tab-pane <?php echo $btn_link_type == 'external_link' ? 'active' : ''; ?>" id="external_link_content" >
							<div class="row d-block d-md-flex align-items-center my-3">
								<div class="col col-12 col-md-2"><label>External Link</label></div>
								<div class="col col-12 col-md-10">
									<input type="text" name="btn_external_link" class="form-control"  placeholder="" value="<?php echo $btn_external_link; ?>">
								</div>
							</div>
						</div>
						<div class="tab-pane <?php echo $btn_link_type == 'pdf' ? 'active' : ''; ?>" id="pdf_content" >
							<div class="row d-block d-md-flex align-items-center my-3">
								<div class="col col-12 col-md-2"><label>PDF</label></div>
								<div class="col col-12 col-md-10">
									
                                    <div class="tab-content tab-content-lang">
										<?php foreach( $langs as $lang ){ ?>
                                        <?php
                                            if( isset( $detail[ $lang['text'] ] ) ) {
                                                $btn_pdf_file = $detail[ $lang['text'] ]['btn_pdf_file'];
                                            } else {
                                                $btn_pdf_file = '';
                                            }
                                        ?>
                                        <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
                                            <div class="form-group">
                                                <input type="file" name="btn_pdf_file[]" class="file-upload-default">
                                                <div class="input-group col-xs-12">
                                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload PDF" value="<?php echo $btn_pdf_file; ?>">
                                                    <span class="input-group-append">
                                                        <button class="file-upload-browse btn  btn-gradient-dark " type="button">Upload</button>
                                                    </span>
                                                    <input type="hidden" class="image_as_text" name="btn_pdf_file_txt[]" value="<?php echo $btn_pdf_file; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
									</div>
                                    
								</div>
							</div>
						</div>
						<div class="tab-pane <?php echo $btn_link_type == 'content' ? 'active' : ''; ?>" id="content_content" >
							<div class="row d-block d-md-flex align-items-center my-3">
								<div class="col col-12 col-md-2"><label>Content</label></div>
								<div class="col col-12 col-md-10">

									<div class="tab-content tab-content-lang">
										<?php foreach( $langs as $lang ){ ?>
											<?php
												if( isset( $detail[ $lang['text'] ] ) ) {
													$btn_content_popup = $detail[ $lang['text'] ]['btn_content_popup'];
												} else {
													$btn_content_popup = '';
												}
											?>
											<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
												<div class="form-group mb-3">
													<textarea name="btn_content_popup[]" class="form-control descriptionInput tinyMceExample" rows="7" placeholder="" ><?php echo $btn_content_popup; ?></textarea>
												</div>
											</div>
										<?php } ?>
									</div>
							
								</div>
							</div>
						</div>
					</div>


				</div>					
			</div>
		</div>
    </div>
	
	<div class="card my-3">
		<div class="card-body">
			<div class="form-group">
				<label for="">Status</label>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="onoff" value="1"  <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
				</div>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 ? 'checked' : ''; ?>> Non - Publish </label>
				</div>
			</div>

			<div class="row">
				<div class="col-12 text-right">
					<div class="text-center card-button">
						
						<button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<!-- content-wrapper ends -->
<?php include_once("inc/nexitor-modal.php"); ?>
