<div class="content-wrapper">
    <div class="page-header">
        <h4 class="page-title"> <?php echo strtoupper( 'Listing of category : '.$cate_detail[DEFAULT_LANG]['title']) ?>
        </h4>
        <?php if($param_sec_key != "touchthebeyond" ){ ?>
            <div>
                <?php  if( check_permission( $this->session->userdata('user_type'), 'create_content') ){ ?>
                    <a href="<?php echo base_url().'admin/section/listing/add/'.$sec_detail[DEFAULT_LANG]['code'].'/'.$cate_detail[DEFAULT_LANG]['code']; ?>" class="btn btn-gradient-dark btn-fw">+ Add more</a>
                <?php } ?>
                <?php if( !isEditor( $this->session->userdata('user_type') ) ){ ?>
                    <a href="<?php echo base_url().'admin/section/categories/lists/'.$sec_detail[DEFAULT_LANG]['code']; ?>" class="btn btn-gradient-dark btn-fw"><- Back</a>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
    
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table id="order-listing" class="table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Sticky</th>
                                    <?php if($sec_detail[DEFAULT_LANG]['design_type'] != 'accordion'){ ?>
                                    <th>Image</th>
                                    <?php } ?>
                                    <th>Title</th>
                                    <?php  if ( $cate_detail[DEFAULT_LANG]['must_be_approve'] == '1') {?>
                                    <th>Approve</th>
                                    <?php } ?>
                                    <th>Status</th>
                                    <th>Published</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach( $lists as $list ){ ?>
                                    <?php
                                        $cate_code = ( $list['cate_code'] != '' )? json_decode($list['cate_code']) : '';
                                        if ( sizeof($cate_code) ) {
                                            $cate_code = $cate_code[0];
                                        }
                                    ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $list['sticky']; ?></td>
                                    
                                    <?php if($sec_detail[DEFAULT_LANG]['design_type'] != 'accordion'){ ?>
                                    <td style="white-space: normal;width: 100px;">
                                        <span class="float-left" style="width:100px; line-height:1.4;"><img
                                                style="width: 100px; height: auto; border-radius: 0;"
                                                src="<?php echo $list['image'] ? upload_path($list['image']) : '/assets/img/template/image-default.jpg'; ?>" alt=""></span>
                                    </td>
                                    <?php } ?>
                                    <td style="white-space: normal;">
                                        <span class="float-left" style="width:200px; line-height:1.4;"><?php echo $list['title']; ?></span>
                                    </td>
                                    <?php  if ( $cate_detail[DEFAULT_LANG]['must_be_approve'] == '1') {?>
                                    <td><span><?php echo getApproveStatusTxt( $list['approve_status']); ?></span></td>
                                    <?php } ?>
                                    <td>
                                    <?php 
                                        if( $list['onoff'] == 1 ){ 
                                            if( $list['published_date'] > date('Y-m-d H:i:s') ){
                                                echo '<div class="badge badge-pill badge-outline-warning"><i class="mdi mdi-check mr-1"></i>Scheduled</div>';
                                            } else {
                                                echo '<div class="badge badge-pill badge-outline-success"><i class="mdi mdi-check mr-1"></i>Enable</div>';
                                            }
                                        } else {
                                            echo '<div class="badge badge-pill badge-outline-danger"><i class="mdi mdi-check mr-1"></i>Disable</div>';
                                        } 
                                    ?>
                                    </td>
                                    <td><?php echo $list['published_date']; ?>
                                        <!-- <div class="badge badge-pill badge-danger btn-inverse-dark"
                                            data-toggle="tooltip" data-placement="right"
                                            title="<?php echo $list['published_date']; ?>">
                                            <i class="mdi mdi-clock"></i> Date Published</div> -->
                                    </td>
                                    
                                    <!-- <td>
                                        <div class="badge badge-pill badge-danger btn-inverse-dark"
                                            data-toggle="tooltip" data-placement="right"
                                            title="Created : <?php echo $list['c_date']; ?> Updated : <?php echo $list['u_date']; ?>">
                                            <i class="mdi mdi-clock"></i> Date posted</div>
                                    </td> -->
                                    <td>
                                        <div style="padding: .3rem .4rem;" class="btn btn-info btn-inverse-dark" data-toggle="tooltip" data-placement="left" title="Created : <?php echo $list['c_date']; ?> Updated : <?php echo $list['u_date']; ?>">
                                            <i class="mdi mdi-clock"></i></div>
                                        <?php  if( check_permission( $this->session->userdata('user_type'), 'edit_content') ){ ?>
                                        <a href="<?php echo base_url().'admin/section/listing/edit/'.$sec_detail[DEFAULT_LANG]['code'].'/'.$cate_code.'/'.$list['code']; ?>"
                                            class="btn btn-info">Edit</a>
                                        <?php } ?>
                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>"
                                            value="<?php echo $this->security->get_csrf_hash();?>" />
                                        <?php if($param_sec_key != "touchthebeyond"){?>
                                            <?php  if( check_permission( $this->session->userdata('user_type'), 'delete_content') ){ ?>
                                        <a href="javascript:void(0);" class="btn btn-danger delete-item"
                                            onclick="deleteItem( $(this) );" data-toggle="modal"
                                            data-target="#deleteModal" data-delete-by="code" data-table="section_lists"
                                            data-code="<?php echo $list['code']; ?>">Delete</a>
                                            <?php } ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- content-wrapper ends -->