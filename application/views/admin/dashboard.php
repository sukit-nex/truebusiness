
        <div class="content-wrapper">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
          <?php if( $this->session->userdata('user_type') == 'SA'){ ?>
          <div class="page-header">
            <h3 class="page-title">
              <span class="page-title-icon bg-gradient-dark text-white mr-2"><i class="mdi mdi-home"></i></span>Dashboard
            </h3>
          </div>
          <?php } ?>
          <style>
		  	.page-position-management img { transition:all .2s ease; filter: grayscale(100%); opacity:0.8; float:left; width:100%;}
		  	.page-position-management a:hover img { transform:scale(1.1);box-shadow:0 0 0 2px rgba(0,0,0,0.5);filter: grayscale(0%); position:relative; z-index:2; opacity:1;}
		  </style>
        <?php if( $this->session->userdata('user_type') == 'SA' ){ ?>

        <?php } ?>
        <?php if(isApprover($this->session->userdata('user_type'))){ ?>
          <div class="page-header">
            <h3 class="page-title"><span class="page-title-icon btn-rounded bg-gradient-info text-white mr-2"><i class="mdi mdi-newspaper"></i></span>Approve content</h3>
          </div>
          <div class="card mb-3">
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                  <table id="order-listing" class="table">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th class="text-center" style="width:30%">Content</th>
                        <th class="text-center" style="width:20%">Category</th>
                        <th class="text-center">Created By</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    <?php foreach( $lists as $list ){ ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $list['title']; ?></td>
                        <td> <?php echo $list['cate_name']; ?></td>
                        <td> <?php echo $list['create_name'].' '.$list['create_lastname']; ?></td>
                        <td> <?php echo getApproveStatusTxt( $list['approve_status'] ); ?></td>
                        <td>
                          <a target="_blank" href="<?php echo base_url().'th/content/'.$list['cate_slug'].'/'.$list['slug']; ?>" class="btn btn-success">View</a>
                          <a href="<?php echo base_url().'admin/section/listing/edit/'.$list['sec_code'].'/'.$list['cate_code'].'/'.$list['code']; ?>" class="btn btn-warning">Edit</a>
                          <a data-code="<?php echo $list['code']; ?>" href="javascript:void(0)" onclick="approve_reject( $(this) );" class="btn btn-danger">Reject</a>
                          <a data-code="<?php echo $list['code']; ?>" href="javascript:void(0)" onclick="approve_confirm( $(this) );" class="btn btn-info">Approve & Public</a>
                        </td>
                      </tr>
                    <?php $i++; } ?>
                    </tbody>
                  </table>
                </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
        <?php if(check_permission( $this->session->userdata('user_type'), 'pages')){ ?>
          <div class="row mb-3 ">
           <?php if(check_permission( $this->session->userdata('user_type'), 'total_page')){ ?>
            <div class="col-12 col-xl-4 col-lg-6 col-md-6">
              <div class="row ">
                <div class="col-12">
                  <div class="card-body stat">
                    <div class="row">
                      <div class="col-md-12 stretch-card mb-3">
                        <div class="card bg-gradient-dark card-img-holder text-white">
                          <div class="card-body">
                            <h5 class="font-weight-normal mb-1 text-uppercase">TOTAL PAGES<i class="mdi mdi-chart-line mdi-16px float-right"></i></h5>
                            <h1 class="font-weight-bold mb-4"><?php echo sizeof($pageLists) + 4; ?></h1>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            <div class="col-12  col-xl-4 col-lg-6 col-md-6">
                <div class="row ">
                  <div class="col-md-12 col-12 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <h4 class="card-title font-weight-bold">TRUE 5G</h4>
                        <!-- <p class="card-description">Use class <code>.accordion</code> for basic accordion</p> -->
                        <div class="mt-4">
                          <div class="accordion" id="accordion-2" role="tablist">
                            <?php // print_r($pageLists) ?>
                              <?php  foreach($pageLists as $d_page_key => $d_page ){ ?>
                                <?php if( hasPagePermission( $this->session->userdata('user_type') , $d_page ) ){ ?>
                                <div class="card">
                                  <div class="card-header" role="tab" id="heading-<?php echo  "0".$d_page_key; ?>">
                                    <h6 class="mb-0">
                                      <a data-toggle="collapse" href="#collapse-<?php echo  "0".$d_page_key; ?>" aria-expanded="false" aria-controls="collapse-<?php echo  "0".$d_page_key; ?>"><?php echo $d_page['page_name']; ?></a>
                                    </h6>
                                  </div>
                                  
                                    <div id="collapse-<?php echo  "0".$d_page_key; ?>" class="collapse " role="tabpanel" aria-labelledby="heading-<?php echo  "0".$d_page_key; ?>" data-parent="#accordion-2">
                                      <div class="card-body">
                                        <div class="page-position-management">
                                          <div class="row">
                                            <?php  foreach($d_page['rows'] as $d_row_key => $d_row ){ ?>
                                              <?php  foreach($d_row['columns'] as $d_col_key => $d_col ){ ?>
                                                <div class="col-12 <?php echo sizeof($d_row['columns'] ) > 1 ? 'col-md-6' : 'col-md-12'; ?> ">
                                                  <?php $sections = $d_col['sections']; ?>
                                                  <?php if(sizeof($sections) > 0){  ?>
                                                      <?php foreach($sections as $sec_key => $section){ ?>
                                                          <a href="<?php echo base_url().'admin/section/edit/'.$section['page_code'].'/'.$section['code']; ?>"><img src="<?php  echo $section['thumb'] == '' ? '/assets/admin/images/element/section/section_thumb.jpg' : upload_path($section['thumb']); ?>"></a>
                                                      <?php } ?> 
                                                  <?php } ?>
                                                </div>
                                              <?php } ?>
                                            <?php } ?>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                <?php } ?>
                              <?php } ?>
              
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              </div>
                <?php if( check_permission( $this->session->userdata('user_type'), 'immersive') || check_permission( $this->session->userdata('user_type'), 'xr') || check_permission( $this->session->userdata('user_type'), 'ar') || check_permission( $this->session->userdata('user_type'), 'vr')){ ?>
                <div class="col-12  col-xl-4 col-lg-6 col-md-6">
                  <div class="row ">
                    <div class="col-md-12 col-12 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title font-weight-bold">IMMERSIVE EXPERIENCE </h4>
                          
                          <div class="mt-4">
                          <div class="accordion " id="accordion" role="tablist">
                              <?php // print_r($pageLists) ?>
                              <?php if( check_permission( $this->session->userdata('user_type'), 'immersive') ){  ?>
                              <div class="card">
                                <div class="card-header" role="tab" id="heading-10">
                                  <h6 class="mb-0">
                                    <a data-toggle="collapse" href="#collapse-10" aria-expanded="false" aria-controls="collapse-10">IMMERSIVE</a>
                                  </h6>
                                </div>
                                <div id="collapse-10" class="collapse " role="tabpanel" aria-labelledby="heading-10" data-parent="#accordion">
                                    <div class="card-body">
                                      <div class="row">
                                        <div class="col-12">
                                          <div class="page-position-management">
                                            <a href="<?php echo base_url().'admin/banner/lists/immersive'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_IMMERSIVE_desktop_02.jpg"></a>
                                            <a href="<?php echo base_url().'admin/section/detail/immersive/xr'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_IMMERSIVE_desktop_03.jpg"></a>
                                            <a href="<?php echo base_url().'admin/section/detail/immersive/vr'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_IMMERSIVE_desktop_04.jpg"></a>
                                            <a href="<?php echo base_url().'admin/section/detail/immersive/ar'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_IMMERSIVE_desktop_05.jpg"></a>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                </div>
                              </div>
                              <?php } ?>
                              <?php if( check_permission( $this->session->userdata('user_type'), 'xr') ){  ?>
                              <div class="card">
                                <div class="card-header" role="tab" id="heading-11">
                                  <h6 class="mb-0">
                                    <a data-toggle="collapse" href="#collapse-11" aria-expanded="false" aria-controls="collapse-11">Page XR</a>
                                  </h6>
                                </div>
                                  <div id="collapse-11" class="collapse " role="tabpanel" aria-labelledby="heading-11" data-parent="#accordion">
                                    <div class="card-body">
                                      <div class="row">
                                        <div class="col-12">
                                          <div class="page-position-management">
                                              <a href="<?php echo base_url().'admin/section/detail/xr/phenomenon'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/True5G_XR_desktop-2_02.jpg"></a>
                                              <a href="<?php echo base_url().'admin/section/detail/xr/studiovideo'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/True5G_XR_desktop-2_03.jpg"></a>
                                              <a href="<?php echo base_url().'admin/section/detail/xr/studiocontent'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/True5G_XR_desktop-2_04.jpg"></a>
                                              <a href="<?php echo base_url().'admin/section/detail/xr/musicspace'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/True5G_XR_desktop-2_05.jpg"></a>
                                              <a href="<?php echo base_url().'admin/section/detail/xr/experience'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/True5G_XR_desktop-2_06.jpg"></a>
                                              <a href="<?php echo base_url().'admin/section/category/lists/xr/experience'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/True5G_XR_desktop-2_07.jpg"></a>
                                              <a href="<?php echo base_url().'admin/section/content/lists/xr/experience'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/True5G_XR_desktop-2_08.jpg"></a>
                                              <a href="<?php echo base_url().'admin/section/detail/xr/calendar'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/True5G_XR_desktop-2_09.jpg"></a>
                                              <a href="<?php echo base_url().'admin/section/content/lists/xr/calendar'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/True5G_XR_desktop-2_10.jpg"></a>
                                              <a href="<?php echo base_url().'admin/section/detail/xr/application'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/True5G_XR_desktop-2_11.jpg"></a>
                                              <a href="<?php echo base_url().'admin/section/detail/xr/alliance'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/True5G_XR_desktop-2_12.jpg"></a>
                                              <a href="<?php echo base_url().'admin/section/footer/xr'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/True5G_XR_desktop-2_13.jpg"></a>
                                        
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              <?php } ?>
                              <?php if( check_permission( $this->session->userdata('user_type'), 'ar') ){  ?>
                              <div class="card">
                                <div class="card-header" role="tab" id="heading-2">
                                  <h6 class="mb-0">
                                    <a data-toggle="collapse" href="#collapse-12" aria-expanded="false" aria-controls="collapse-12">Page AR</a>
                                  </h6>
                                </div>
                                <div id="collapse-12" class="collapse " role="tabpanel" aria-labelledby="heading-12" data-parent="#accordion">
                                  <div class="card-body">
                                      <div class="row">
                                        <div class="col-12">
                                          <div class="page-position-management">
                                            <a href="<?php echo base_url().'admin/section/detail/ar/limitlessexperience'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_AR_desktop_02.jpg"></a>
                                            <a href="<?php echo base_url().'admin/section/detail/ar/limitlessexperiencevideo'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_AR_desktop_03.jpg"></a>
                                            <a href="<?php echo base_url().'admin/section/detail/ar/truearapp'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_AR_desktop_04.jpg"></a>
                                            <a href="<?php echo base_url().'admin/section/detail/ar/virtualworld'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_AR_desktop_05.jpg"></a>
                                            <a href="<?php echo base_url().'admin/section/detail/ar/smartbenefits'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_AR_desktop_06.jpg"></a>
                                            <a href="<?php echo base_url().'admin/section/content/lists/ar/truecatalog'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_AR_desktop_07.jpg"></a>
                                            <a href="<?php echo base_url().'admin/section/detail/ar/argame'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_AR_desktop_08.jpg"></a>
                                            <a href="<?php echo base_url().'admin/section/footer/ar'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_AR_desktop_09.jpg"></a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              <?php } ?>
                              <?php if( check_permission( $this->session->userdata('user_type'), 'vr') ){  ?>
                              <div class="card">
                                <div class="card-header" role="tab" id="heading-13">
                                  <h6 class="mb-0">
                                    <a data-toggle="collapse" href="#collapse-13" aria-expanded="false" aria-controls="collapse-13">Page VR</a>
                                  </h6>
                                </div>
                                <div id="collapse-13" class="collapse " role="tabpanel" aria-labelledby="heading-13" data-parent="#accordion">
                                  <div class="card-body">
                                    <div class="row">
                                      <div class="col-12">
                                        <div class="page-position-management">
                                          <a href="<?php echo base_url().'admin/section/detail/vr/limitlessimaginnation'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_VR_desktop_02.jpg"></a>
                                          <a href="<?php echo base_url().'admin/section/detail/vr/limitlessimaginnationvideo'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_VR_desktop_03.jpg"></a>
                                          <a href="<?php echo base_url().'admin/section/content/lists/vr/touchthebeyond'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_VR_desktop_04.jpg"></a>
                                          <a href="<?php echo base_url().'admin/section/content/lists/vr/seetheunseen'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_VR_desktop_05.jpg"></a>
                                          <a href="<?php echo base_url().'admin/section/content/lists/vr/vrproduct'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_VR_desktop_06.jpg"></a>
                                          <a href="<?php echo base_url().'admin/section/footer/vr'; ?>"><img src="<?php echo FRONT_ASSET; ?>admin/images/dashboard/page/TRUE5G_VR_desktop_07.jpg"></a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php } ?>

            </div>
          </div>  
        <?php } ?>
        <?php if( isEditor( $this->session->userdata('user_type') )){ ?>
            <?php foreach( $cates as $cate ){ ?>
              <?php if( $cate['cate_permission'] != "" ){ ?>
                  <?php $ar_permission = explode(",",$cate['cate_permission'] ); $ar_section = $cate['sec_code'] != '' ? json_decode( $cate['sec_code'] , true) : array() ;?>
                  <?php if( hasPermission( $this->session->userdata('user_type'), $cate ) ){ ?>
                  <div class="page-header">
                    <h3 class="page-title"><span class="page-title-icon btn-rounded bg-gradient-info text-white mr-2"><i class="mdi mdi-newspaper"></i></span><?php echo $cate['title']; ?></h3>
                    <a href="<?php echo base_url().'admin/section/listing/lists/'.$ar_section[0]."/".$cate['code']?>" class="btn btn-gradient-dark btn-fw float-right ml-auto mr-2">View all</a>
                    <a href="<?php echo base_url().'admin/section/listing/add/'.$ar_section[0]."/".$cate['code']?>" class="btn btn-gradient-info btn-fw float-right">Add Content</a>
                  </div>
                  <?php } ?>
              <?php } ?>
          <?php } ?>
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                  <table id="order-listing" class="table">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th class="text-center" style="width:30%">Content</th>
                        <th class="text-center" style="width:20%">Category</th>
                        <th class="text-center">Created By</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Message</th>
                        <th class="text-center">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    <?php foreach( $lists as $list ){ ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $list['title']; ?></td>
                        <td> <?php echo $list['cate_name']; ?></td>
                        
                        <td> <?php echo $list['create_name'].' '.$list['create_lastname']; ?></td>
                        
                        <td class="<?php echo ($list['approve_status']=='3')? 'text-danger' : ''; ?>"> <?php echo getApproveStatusTxt( $list['approve_status'] ); ?></td>
                        <td>
                          <?php if( $list['approve_note'] != "" and $list['approve_status']=='3' ){?>
                              <a href="javascript:void(0)" class="table_viewmore" data-toggle="modal" data-target="#exampleModal"><div class="badge badge-pill badge-outline-info"><i class="mdi mdi-message-text mr-1"></i>Message</div>
                              <span style="display:none;"><?php echo $list['approve_note']; ?></span></a>
                          <?php }else{ ?>
                              -
                          <?php } ?>
                        </td>
                        <td>
                        <a target="_blank" href="<?php echo base_url().'th/content/'.$list['cate_slug'].'/'.$list['slug']; ?>" class="btn btn-success">View</a>
                          <a href="<?php echo base_url().'admin/section/listing/edit/'.$list['sec_code'].'/'.$list['cate_code'].'/'.$list['code']; ?>" class="btn btn-warning">Edit</a>
                          <?php if ( $list['approve_status'] == '1' ) { ?>
                          <a data-code="<?php echo $list['code']; ?>" href="javascript:void(0)" onclick="approve_cancel( $(this) );" class="btn btn-danger">Cancel</a>
                          <?php } ?>
                         
                        </td>
                      </tr>
                    <?php $i++; } ?>
                    </tbody>
                  </table>
                </div>
                </div>
              </div>
            </div>
          </div>
          
          
        <?php } ?>
        </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" style="background-color:#fff;">
                  <p class="mb-0 showmessage"></p>
              </div>
            </div>
          </div>
        </div>
