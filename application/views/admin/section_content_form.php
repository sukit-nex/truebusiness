<?php
  if( sizeof( $detail ) > 0 ){
	$code = $detail[DEFAULT_LANG]['code'];
	$cate_code = $detail[DEFAULT_LANG]['cate_code'];
    $page = $detail[DEFAULT_LANG]['page'];
	$sec_key = $detail[DEFAULT_LANG]['sec_key'];
	
	$image = $detail[DEFAULT_LANG]['image'];
	$image2 = $detail[DEFAULT_LANG]['image2'];
	$post_date = $detail[DEFAULT_LANG]['post_date'];
	$post_time = $detail[DEFAULT_LANG]['post_time'];
	$sticky = $detail[DEFAULT_LANG]['sticky'];
    //$bg1_mobile = $detail[DEFAULT_LANG]['bg1_mobile'];

    //$img1_desktop = $detail[DEFAULT_LANG]['img1_desktop'];
	//$img1_mobile = $detail[DEFAULT_LANG]['img1_mobile'];

	$onoff = $detail[DEFAULT_LANG]['onoff'];
    $link_out = $detail[DEFAULT_LANG]['link_out'];
  }else{
	$code = '';
	$cate_code = '';
    $page = $param_page;
	$sec_key = $param_sec_key;
	$image = '';
	$image2 = '';
	$post_date = '';
	$post_time = '';
	$sticky = 0;
    //$bg1_mobile = '';

    //$img1_desktop = '';
	//$img1_mobile = '';

	$onoff = 1;
    $link_out = '';
  }
?>
<div class="content-wrapper">

    <div class="page-header">
        <h3 class="page-title"> <?php echo strtoupper( 'Content : '.$page.' - '.$sec_key.' form') ?> </h3>
        <a href="<?php echo base_url().'admin/section/content/lists/'.$param_page.'/'.$param_sec_key ?>" class="btn btn-gradient-dark btn-fw"><- Back</a> 
        <?php /* <a href="<?php echo base_url().'admin/banner/add' ?>" class="btn btn-gradient-dark btn-fw">+ Add
        more</a> */ ?>
    </div>
    <?php echo form_open_multipart('admin/section/saveContentForm', ' class="mainForm"'); ?>
    <input type="hidden" name="code" value="<?php echo $code; ?>">
    <input type="hidden" name="page" value="<?php echo $page; ?>">
    <input type="hidden" name="sec_key" value="<?php echo $sec_key; ?>">

    <input type="hidden" name="time" value="<?php echo $time; ?>">

    <?php //include_once("inc/banner_content.php"); ?>
    <?php if( $param_sec_key == "seetheunseen" || $param_sec_key == "touchthebeyond" || $param_sec_key == "vrproduct" ){ ?> 
	<?php }else { ?>
        <div class="card mb-3">
        <div class="card-body">
            <div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
                <ul class="nav nav-tabs" role="tablist" style="">
                    <?php foreach( $langs as $lang ){ ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                            id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
                            href="#<?php echo 'title_'.$lang['text']; ?>" role="tab"
                            aria-controls="<?php echo 'title_'.$lang['text']; ?>"
                            aria-selected="true"><?php echo $lang['text']; ?></a>
                    </li>
                    <?php } ?>
                </ul>
                <div class="tab-content float-left w-100" style="">
                    <?php foreach( $langs as $lang ){ ?>
                    <?php
					if( isset( $detail[ $lang['text'] ] ) ){
						$title = $detail[ $lang['text'] ]['title'];
              			$sub_title = $detail[ $lang['text'] ]['sub_title'];
					}else{
						$title = '';
              			$sub_title = '';
					}
					?>
                    <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                        id="<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
                        aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
                        <div class="form-group">
                            <label for="">Title</label>
                            <input type="text" name="title[]" class="form-control" placeholder="Title"
                                value="<?php echo $title; ?>">
                        </div>
                        <?php if($param_sec_key != "vrpartner"){ ?>
                        <div class="form-group">
                            <label for=""><?php if($param_sec_key == "truecatalog" ){ ?> Price <?php }else{ ?>Sub Title <?php } ?> </label>
                            
                            <input type="text" name="sub_title[]" class="form-control" placeholder="Sub-Title"
                                value="<?php echo $sub_title; ?>" maxlength="<?php echo $param_sec_key == 'calendar' ? '100' : ''; ?>" >

                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
    <?php } ?>
   
    <div class="card mb-3">
        <div class="card-body">
            <div class="form-group">
                <?php 
					  $text_mute_d = "";
                      $data_text_d = "";
					  if( $param_sec_key == "experience"){
						$text_mute_d = '( 417px x 313px )';
                        $data_text_d = 'data-max-width="417" data-max-height="313"';
					  }elseif($param_sec_key == "calendar"){
                        $text_mute_d = '( 664px x 415px )';
                        $data_text_d = 'data-max-width="664" data-max-height="415"';
                      }
                      elseif($param_sec_key == "truecatalog"){
                        $text_mute_d = '( 240px x 240px )';
                        $data_text_d = 'data-max-width="240" data-max-height="240"';
                    }
                    elseif($param_sec_key == "touchthebeyond"){
                        $text_mute_d = '( 817px x 680px )';
                        $data_text_d = 'data-max-width="817" data-max-height="680"';

                    }elseif($param_sec_key == "seetheunseen"){
                        $text_mute_d = '( 320px x 200px )';
                        $data_text_d = 'data-max-width="320" data-max-height="200"';
                    }elseif($param_sec_key == "vrproduct"){

                        $text_mute_d = '(Info icon size 60px x 60px , Product size  704px x 500px )';
                        $data_text_d = 'data-max-width="704" data-max-height="500"';
                    }
				  ?>
                <div class="row">
                    <div class="col-12 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
                        <label>Image<small class="text-muted"> <?php echo $text_mute_d; ?></small></label>
                        <input type="file" data-height="250" name="image" class="dropify"  <?php echo $data_text_d; ?> data-max-file-size="3000kb"
                            data-default-file="<?php echo $image != "" ? upload_path($image) : ''; ?>" />
                        <input type="hidden" class="image_as_text" name="image_txt" value="<?php echo $image; ?>" />
                    </div>
                    <?php if($param_sec_key == "touchthebeyond"){ ?>
                    <div class="col-12 image-box mt-3">
                        <label>Image 2<small class="text-muted">( 885px x 387px )</small></label>
                        <input type="file" data-height="250" name="image2" class="dropify" data-max-width="885" data-max-height="387" data-max-file-size="3000kb"
                            data-default-file="<?php echo $image2 != "" ? upload_path($image2) : ''; ?>" />
                        <input type="hidden" class="image_as_text" name="image2_txt" value="<?php echo $image2; ?>" />
                    </div>
                    <?php } ?>
                    <?php if( $param_sec_key == "calendar" ){ ?>
                    <div class="col-12 mt-3">
                        <label for="">Event Date</label>
                        <input class="input-medium form-control datepicker" name="post_date" type="text"
                            data-date-language="th-th" autocomplete="off" value="<?php echo $post_date; ?>">
                    </div>
                    <div class="col-12 mt-3">
                        <label for="">Event Time</label>
                        <input type="text" name="post_time" class="form-control" placeholder="Event Time"
                            value="<?php echo $post_time; ?>">
                    </div>
                    <?php } ?>
                    <?php if($param_sec_key == "experience" || $param_sec_key == "calendar"){ ?>
                    <div class="col-12 mt-3">
                        <label for="">Link out</label>
                        <input type="text" name="link_out" class="form-control" placeholder="Link out"
                            value="<?php echo $link_out; ?>">
                    </div>
                    <?php } ?>
                    <?php if($param_sec_key == "touchthebeyond" || $param_sec_key == "calendar" || $param_sec_key == "truecatalog" || $param_sec_key == "vrproduct" || $param_sec_key == "vrinfo" || $param_sec_key == "vrpartner"){?>
                    <div class="col-12 mt-3">
                        <label for="">Order</label>
                        <input type="text" name="sticky" class="form-control" placeholder="Order"
                            value="<?php echo $sticky; ?>">
                    </div>
                   <?php }else{ ?>
                    <div class="col-8 mt-3">
                        <label for="">Category</label>
                        <select class="form-control" name="cate_code">
                            <option value="">เลือกหมวดหมู่</option>
                            <?php foreach( $categories as $cate ){ ?>
                            <option value="<?php echo $cate['code']; ?>"
                                <?php echo $cate['code'] == $cate_code ? 'selected' : ''; ?>>
                                <?php echo $cate['title']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-4 mt-3">
                        <label for="">Order</label>
                        <input type="text" name="sticky" class="form-control" placeholder="Order"
                            value="<?php echo $sticky; ?>">
                    </div>
                   <?php }  ?>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="card mt-3">
        <div class="card-body">
            <div class="form-group">
                <label for="">Status</label>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="1"
                            <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
                </div>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="0"
                            <?php echo $onoff == 0 ? 'checked' : ''; ?>> Unpublish </label>
                </div>
            </div>

            <div class="row">
                <div class="col-12 text-right">
                    <div class="text-center card-button">
                        <button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?php //include_once("inc/nexitor-modal.php"); ?>