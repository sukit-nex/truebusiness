<?php
if( sizeof( $detail ) > 0 ){
	$code = $detail[DEFAULT_LANG]['code'];
	$page = $detail[DEFAULT_LANG]['page'];
	$sec_key = $detail[DEFAULT_LANG]['sec_key'];
	$slug = $detail[DEFAULT_LANG]['slug'];
	
	$share_img = $detail[DEFAULT_LANG]['share_img'];
}else{
	$code = '';
	$page = $param_page;
	$sec_key = $param_sec_key;
	$slug = time();
	$share_img = '';
}
?>
<div class="content-wrapper">

  <div class="page-header">
    <h3 class="page-title"> <?php echo strtoupper( $page.' - SEO') ?> </h3>
    <?php /* <a href="<?php echo base_url().'admin/banner/add' ?>" class="btn btn-gradient-dark btn-fw">+ Add more</a> */ ?>
  </div>
  <?php echo form_open_multipart('admin/seo/saveDetailForm', ' class="mainForm"'); ?>
     <input type="hidden" name="code" value="<?php echo $code; ?>" >
     <input type="hidden" name="page" value="<?php echo $page; ?>" >
     <input type="hidden" name="sec_key" value="<?php echo $sec_key; ?>" >

     <input type="hidden" name="time" value="<?php echo $time; ?>" >

    <?php //include_once("inc/banner_content.php"); ?>
	<div class="card mb-3">
  		<div class="card-body">
  			<div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
				<ul class="nav nav-tabs" role="tablist" style="">
				<?php foreach( $langs as $lang ){ ?>
					<li class="nav-item">
						<a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab" href="#<?php echo 'title_'.$lang['text']; ?>" role="tab" aria-controls="<?php echo 'title_'.$lang['text']; ?>" aria-selected="true"><?php echo $lang['text']; ?></a>
					</li>
				<?php } ?>
				</ul>
				<div class="tab-content float-left w-100" style="">
					<?php foreach( $langs as $lang ){ ?>
						<?php
						if( isset( $detail[ $lang['text'] ] ) ){
							$page_title = $detail[ $lang['text'] ]['page_title'];
							$meta_title = $detail[ $lang['text'] ]['meta_title'];
							$meta_desc = $detail[ $lang['text'] ]['meta_desc'];
							$meta_keyword = $detail[ $lang['text'] ]['meta_keyword'];
						}else{
							$page_title = '';
							$meta_title = '';
							$meta_desc = '';
							$meta_keyword = '';
						}
						?>
					<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo 'title_'.$lang['text']; ?>" role="tabpanel" aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
						<div class="form-group">
							<label for="">Page title</label>
							<input type="text" name="page_title[]" class="form-control"  placeholder="Page title" value="<?php echo $page_title; ?>">
						</div>
						<div class="form-group">
							<label for="">Meta title</label>
							<input type="text" name="meta_title[]" class="form-control"  placeholder="Meta title" value="<?php echo $meta_title; ?>">
						</div>
						<div class="form-group">
							<label for="">Meta keyword</label>
							<input type="text" name="meta_keyword[]" class="form-control meta_keyword"  placeholder="Meta Keyword" value="<?php echo $meta_keyword; ?>">
						</div>
						<div class="form-group">
							<label for="">Meta description</label>
							<input type="text" name="meta_desc[]" class="form-control"  placeholder="Meta Description" value="<?php echo $meta_desc; ?>">
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-12 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
						<label>Share Image<small class="text-muted">( 1200 x 630px )</small></label>
						<input type="file" data-height="250" name="share_img" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $share_img != "" ? upload_path($share_img) : ''; ?>" />
						<input type="hidden" class="image_as_text" name="share_img_txt" value="<?php echo $share_img; ?>" />
					</div>
				</div>
			</div>
		</div>
	</div>
    
	<div class="card mt-3">
		<div class="card-body">
		<div class="row">
			<div class="col-12 text-right">
			<div class="text-center card-button">
				<button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
			</div>
			</div>
		</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>