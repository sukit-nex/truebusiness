
<div class="content-wrapper">
	<div class="page-header">
		<h4 class="page-title"> Menu Management </h4>
		<?php /* <a href="#" data-toggle="modal" data-target="#nexuModal" class="btn btn-gradient-dark btn-fw">+ Add Menu</a> */ ?>
	</div>

		<div class="row">
			<div class="col-12 col-md-4">
			<div class="card">
				<div class="card-body">
						<div class="">
							<?php echo form_open_multipart('', ' class="update-menu-form"'); ?>
							<div class="nexu-form-here before_enable" style="">
								<input type="hidden" name="menu_code" value="">
								<input type="hidden" name="parent_code" value="0">

								<div class="form-group">
									<ul class="nav nav-tabs" role="tablist">
										<?php foreach( $langs as $lang ){ ?>
											<li class="nav-item">
												<a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo $lang['text']; ?>-tab" data-toggle="tab" href="#<?php echo $lang['text']; ?>" role="tab" aria-controls="<?php echo $lang['text']; ?>" aria-selected="true"><?php echo $lang['text']; ?></a>
											</li>
										<?php } ?>
									</ul>
									<div class="tab-content">
										<?php foreach( $langs as $lang ){ ?>
											<div class="<?php echo 'lang_'.$lang['text']; ?> tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo $lang['text']; ?>" role="tabpanel" aria-labelledby="<?php echo $lang['text']; ?>-tab">
												<div class="form-group mb-0">
													<label for="">Menu Name</label>
													<input type="text" name="name[]" class="form-control nameInput"  placeholder="Menu name" value="">
												</div>
											</div>
										<?php } ?>
									</div>
								</div>

								<div class="form-group">
									<label>Menu Type</label>
									<input type="hidden" name="type" value="link" />
									<div class="form-group menu-type">
										<ul class="nav nav-tabs" role="tablist">
												<li class="nav-item">
													<a class="nav-type nav-link active" id="type-link-tab" data-toggle="tab" href="#type-link" role="tab" aria-controls="type-link" aria-selected="true" data-value="link">Link</a>
												</li>
												<li class="nav-item">
													<a class="nav-type nav-link" id="type-page-tab" data-toggle="tab" href="#type-page" role="tab" aria-controls="type-page" aria-selected="true" data-value="page">Page</a>
												</li>
												<li class="nav-item">
													<a class="nav-type nav-link" id="type-slug-tab" data-toggle="tab" href="#type-slug" role="tab" aria-controls="type-slug" aria-selected="true" data-value="slug">Slug</a>
												</li>
													<?php /*
												<li class="nav-item">
													<a class="nav-type nav-link" id="type-brand-tab" data-toggle="tab" href="#type-brand" role="tab" aria-controls="type-brand" aria-selected="true" data-value="brand">Brand</a>
												</li>
											
												<li class="nav-item">
													<a class="nav-type nav-link" id="type-text-tab" data-toggle="tab" href="#type-text" role="tab" aria-controls="type-text" aria-selected="true" data-value="text">Text</a>
												</li> */ ?>
										</ul>
										<div class="tab-content">
												<div class="tab-type tab-pane fade show active" id="type-link" role="tabpanel" aria-labelledby="type-link-tab">
													<div class="form-group mb-0">
														<input type="text" name="link" class="form-control"  placeholder="Enter the link" value="">
													</div>
												</div>
												<div class=" tab-type tab-pane fade show " id="type-page" role="tabpanel" aria-labelledby="type-page-tab">
													<div class="form-group mb-0">
														<select class="form-control" name="page_code">
															<option value="">Select the page</option>
															<?php foreach( $pagesList as $page_menu ){ ?>
																<option value="<?php echo $page_menu['code']; ?>"><?php echo $page_menu['page_name']; ?></option>
															<?php } ?>
														</select>
													</div>
												</div>
												<div class=" tab-type tab-pane fade show " id="type-slug" role="tabpanel" aria-labelledby="type-slug-tab">
													<div class="form-group mb-0">
														<input type="text" name="slug" class="form-control"  placeholder="Enter the slug" value="">
													</div>
												</div>
												<?php /*
												<div class="tab-type tab-pane fade show " id="type-brand" role="tabpanel" aria-labelledby="type-brand-tab">
													<div class="form-group mb-0">
														<p class="text-muted">* Auto load all brand. <br>( Use brand system to manage ).</p>
													</div>
												</div>
												
												<div class="tab-type tab-pane fade show " id="type-text" role="tabpanel" aria-labelledby="type-text-tab">

												</div>
												*/ ?>
										</div>
									</div>
								</div>
								<?php /*
								<div class="form-group">
									<label>Menu Position</label>
									<select class="form-control" name="position">
										<option value="top">Top Menu</option>
										<option value="footer">Footer Menu</option>
									</select>
								</div>
								*/ ?>
								<input type="hidden" name="position" value="top">

								<div class="form-group">
									<label for="">Is new tab</label>
									<div class="row">
										<div class="col-3 col-md-6 col-sm-12">
											<div class="form-check form-check-dark">
												<label class="form-check-label">
												<input type="radio" class="form-check-input is-new-tab-yes" name="is_newtab" value="1"> Yes </label>
											</div>
										</div>
										<div class="col-3 col-md-6 col-sm-12">
											<div class="form-check form-check-dark">
												<label class="form-check-label">
													<input type="radio" class="form-check-input is-new-tab-no" name="is_newtab" value="0" checked > No </label>
											</div>
										</div>
										<div class="col-3">
										</div>
										<div class="col-3">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="">Status</label>
									<div class="row">
										<div class="col-3 col-md-6 col-sm-12">
											<div class="form-check form-check-dark">
												<label class="form-check-label">
												<input type="radio" class="form-check-input" name="onoff" value="1" checked> Publish </label>
											</div>
										</div>
										<div class="col-4 col-md-6 col-sm-12">
											<div class="form-check form-check-dark">
												<label class="form-check-label">
													<input type="radio" class="form-check-input" name="onoff" value="0"> Unpublish </label>
											</div>
										</div>
										<div class="col-3">
										</div>
										<div class="col-3">
										</div>
									</div>
								</div>
								<div class="form-group mb-0">
									<div class="text-center card-button">
										<button class="btn btn-gradient-dark btn-fw update-menu" type="button"  <?php /* onclick="setMenu()" */ ?> >
											Confirm
										</button>
									</div>
								</div>
							</div>
							<?php form_close(); ?>
								<?php /*
								<div class="nexu-box-btn-bottom">
									<a href="javascript:void(0)" class="nexu-btn-addmenu" data-toggle="modal" data-target="#nexuModal" data-cate-code="<?php echo $cat['code']; ?>"  onclick=""><span><img src="<?php echo ADMIN_ASSET; ?>images/nexitor/icon-plus.svg" class="svg"></span></a>
								</div> */ ?>
						</div>
			</div>
				 </div>
			</div>
			<div class="col-12 col-md-8">
		<div class="card">
				<div class="card-body">
					<div class="row">
					<div class="col-12 col-lg-12 col-md-12 col-sm-12 drag-column"  data-position="top">
						<h5 class="title">Menu</h5>

						<div class="drag-wrapper">

								<div class="drag-box">
									<ul class="drag-item-list nexu-move">
										<?php foreach( $top_lists as $list ){ ?>
										<li class="drag-list level-0 <?php echo $list['onoff'] == 0 ? 'non-active' : ''; ?>" data-code="<?php echo $list['code']; ?>" data-position="<?php echo $list['position']; ?>">
											<a href="javascript:void(0)"><?php echo $list['name'] ?></a>
											<div class="nexu-tools">
												<div>
													<div class="nexu-tools-list drag-cusor"><a href="javascript:void(0)"><i class="mdi mdi-cursor-move"></i></a></div>
													<div class="nexu-tools-list"><a href="javascript:void(0)" data-element="" onClick="editMenu( $(this) )"><i class="mdi mdi-pencil"></i></a></div>
													<div class="nexu-tools-list"><a href="javascript:void(0)" onClick="activeMenu( $(this) )"><i class="mdi mdi-eye<?php echo $list['onoff'] == 0 ? '-off' : ''; ?>"></i></a></div>
													<div class="nexu-tools-list"><a href="javascript:void(0)" class="close-nexitor" onclick="deleteItem( $(this) );" data-delete-type="menu" data-delete-by="code" data-table="menu" data-code="<?php echo $list['code'] ?>" data-toggle="modal" data-target="#deleteModal"><i class="mdi mdi-close"></i></a></div>
												</div>
											</div>
											<?php if( sizeof( $list['sub_level1'] ) > 0 ){ ?>
											<ul class="submenu-list">
											<?php foreach( $list['sub_level1'] as $list1 ){ ?>
													<li class="drag-list level-1 <?php echo $list1['onoff'] == 0 ? 'non-active' : ''; ?>" data-code="<?php echo $list1['code']; ?>" data-position="<?php echo $list1['position']; ?>">
														<a href="javascript:void(0)"><?php echo $list1['name'] ?></a>
														<div class="nexu-tools">
															<div>
																<div class="nexu-tools-list drag-cusor"><a href="javascript:void(0)"><i class="mdi mdi-cursor-move"></i></a></div>
																<div class="nexu-tools-list"><a href="javascript:void(0)" data-element="" onClick="editMenu( $(this) )"><i class="mdi mdi-pencil"></i></a></div>
																<div class="nexu-tools-list"><a href="javascript:void(0)" onClick="activeMenu( $(this) )"><i class="mdi mdi-eye<?php echo $list1['onoff'] == 0 ? '-off' : ''; ?>"></i></a></div>
																<div class="nexu-tools-list"><a href="javascript:void(0)" class="close-nexitor" onclick="deleteItem( $(this) );" data-delete-type="menu" data-delete-by="code" data-table="menu" data-code="<?php echo $list1['code'] ?>" data-toggle="modal" data-target="#deleteModal"><i class="mdi mdi-close"></i></a></div>
															</div>
														</div>
														<?php if( sizeof( $list1['sub_level2'] ) > 0 ){ ?>
														<ul class="submenu-list">
														<?php foreach( $list1['sub_level2'] as $list2 ){ ?>
																<li class="drag-list level-2 <?php echo $list2['onoff'] == 0 ? 'non-active' : ''; ?>" data-code="<?php echo $list2['code']; ?>" data-position="<?php echo $list2['position']; ?>">
																	<a href="javascript:void(0)"><?php echo $list2['name'] ?></a>
																	<div class="nexu-tools">
																		<div>
																			<div class="nexu-tools-list drag-cusor"><a href="javascript:void(0)"><i class="mdi mdi-cursor-move"></i></a></div>
																			<div class="nexu-tools-list"><a href="javascript:void(0)" data-element="" onClick="editMenu( $(this) )"><i class="mdi mdi-pencil"></i></a></div>
																			<div class="nexu-tools-list"><a href="javascript:void(0)" onClick="activeMenu( $(this) )"><i class="mdi mdi-eye<?php echo $list2['onoff'] == 0 ? '-off' : ''; ?>"></i></a></div>
																			<div class="nexu-tools-list"><a href="javascript:void(0)" class="close-nexitor" onclick="deleteItem( $(this) );" data-delete-type="menu" data-delete-by="code" data-table="menu" data-code="<?php echo $list2['code'] ?>" data-toggle="modal" data-target="#deleteModal"><i class="mdi mdi-close"></i></a></div>
																		</div>
																	</div>
																</li>
														<?php } ?>
														</ul>
													<?php } ?>
													</li>
											<?php } ?>
											</ul>
										<?php } ?>
										</li>
									<?php } ?>
									</ul>
								</div> <!-- menu-box -->

							</div>
						</div>

					</div>

			</div>
		</div>



		</div>
	</div>
</div>

</div>


</div>
</div>
</div>
<!-- content-wrapper ends -->
