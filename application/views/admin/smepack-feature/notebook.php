<?php
$feature_name = 'notebook';

$tinymce = 'descriptionInput tinyMceExample';
$dropify = 'dropify';
$collapse_show = ''; // show / hide

$fields = $fields[$feature_name];

$row = array();
foreach ($fields['field'] as $key => $field) {
	$row[$key] = $field;
}

foreach ($fields['file'] as $key => $field) {
	$row[$key] = $field;
}

if ( $method != '' ) {
	
	if( sizeof( $detail ) > 0 ){
		foreach ($fields['field'] as $key => $field) {
			$row[$key] = isset($detail[DEFAULT_LANG][$key])? $detail[DEFAULT_LANG][$key] : '';
		}

		foreach ($fields['file'] as $key => $field) {
			$row[$key] = isset($detail[DEFAULT_LANG][$key])? $detail[DEFAULT_LANG][$key] : '';
		}
	}

} else {
	$tinymce = 'original-tinymce';
	$dropify = 'original-dropify';
	$collapse_show = 'show';
}

$inc_data = array(
	'feature_name' => $feature_name,
	'tinymce' => $tinymce,
	'dropify' => $dropify,
	'fields' => $fields,
	'pkg_number' => $pkg_number,
	'langs'	=> $langs,
	'pagesList'	=> $pagesList,
	'detail'	=> $detail,
	'collapse_show'	=> $collapse_show
);
?>

<?php sme_accordion_header($inc_data, $row ); ?>

		<div class="row">
			<div class="col-12">

				<?php sme_header_box($inc_data, $row ); ?>

				<div class="box-tab-switch">
					<div class="card-title">Main Information</div>
					<div class="form-group is-tab">
						<div class="row">
							<div class="col-md-4 col-sm-2 col-xs-12">
								<div class="box-radio box-check-white-bd">
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][main_type]" value="template" <?php echo ($row['main_type'] == 'template')?'checked':''; ?> >
											Template
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-2 col-xs-12-bd">
								<div class="box-radio box-check-white-bd">
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][main_type]" value="manual" <?php echo ($row['main_type'] == 'manual')?'checked':''; ?>>
											Manual
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="box-form-inner template" <?php echo ($row['main_type'] == 'template')?'':'style="display:none;"'; ?>>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>Icon / Image (50x50) </label></div>
								<div class="col col-12 col-md-9">	
									<input type="file" data-height="190" name="package[<?php echo $pkg_number; ?>][display_icon_image]"
									class="<?php echo $dropify; ?>" data-max-file-size="3000kb"
									data-default-file="<?php echo $row['display_icon_image'] != "" ? upload_path($row['display_icon_image']) : ''; ?>" />
									<input type="hidden" class="image_as_text" name="package[<?php echo $pkg_number; ?>][display_icon_image_txt]"
									value="<?php echo $row['display_icon_image']; ?>" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>Notebook ยี่ห้อ</label></div>
								<div class="col col-12 col-md-9">
									<input type="text" name="package[<?php echo $pkg_number; ?>][main_template_brand]" class="form-control" value="<?php echo $row['main_template_brand']; ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>คำอธิบายรุ่น</label></div>
								<div class="col col-12 col-md-9">
									<div class="tab-content tab-content-lang">
									<?php foreach( $langs as $lang ){ ?>
										<?php
											if( isset( $detail[ $lang['text'] ] ) ) {
												$main_template_model_description = $detail[ $lang['text'] ]['main_template_model_description'];
											} else {
												$main_template_model_description = '';
											}
										?>
										<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
											<input type="text" name="package[<?php echo $pkg_number; ?>][main_template_model_description][]" class="form-control" value="<?php echo $main_template_model_description; ?>">
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						
					</div>
					
					<div class="box-form-inner manual" <?php echo ($row['main_type'] == 'manual')?'':'style="display:none;"'; ?>>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>รายละเอียด</label></div>
								<div class="col col-12 col-md-9">

										<div class="tab-content tab-content-lang">
											<?php foreach( $langs as $lang ){ ?>
												<?php
													if( isset( $detail[ $lang['text'] ] ) ) {
														$main_manual_description = $detail[ $lang['text'] ]['main_manual_description'];
													} else {
														$main_manual_description = '';
													}
												?>
												<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
													<div class="form-group mb-3">
														<textarea name="package[<?php echo $pkg_number; ?>][main_manual_description][]" class="form-control <?php echo $tinymce; ?>" rows="7" placeholder="Description" ><?php echo $main_manual_description; ?></textarea>
													</div>
												</div>
											<?php } ?>
										</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="box-tab-switch">
					<div class="card-title">Detail in toggle</div>
					<div class="form-group is-tab">
						<div class="row">
							<div class="col-md-4 col-sm-2 col-xs-12-bd">
								<div class="box-radio box-check-white-bd">
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][detail_type]" value="manual" <?php echo ($row['detail_type'] == 'manual')?'checked':''; ?>>
											Manual
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-2 col-xs-12-bd">
								<div class="box-radio box-check-white-bd">
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][detail_type]" value="none" <?php echo ($row['detail_type'] == 'none')?'checked':''; ?> >
											None
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="box-form-inner manual" <?php echo ($row['detail_type'] == 'manual')?'':'style="display:none;"'; ?>>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>รายละเอียด</label></div>
								<div class="col col-12 col-md-9">
									
									<div class="tab-content tab-content-lang">
										<?php foreach( $langs as $lang ){ ?>
											<?php
												if( isset( $detail[ $lang['text'] ] ) ) {
													$detail_manual_description = $detail[ $lang['text'] ]['detail_manual_description'];
												} else {
													$detail_manual_description = '';
												}
											?>
											<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
												<div class="form-group mb-3">
													<textarea name="package[<?php echo $pkg_number; ?>][detail_manual_description][]" class="form-control <?php echo $tinymce; ?>" rows="7" placeholder="Description" ><?php echo $detail_manual_description; ?></textarea>
												</div>
											</div>
										<?php } ?>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php sme_link_button($inc_data, $row ); ?>

				<?php sme_display_content($inc_data, $row ); ?>
			</div>
		</div>
	</div>
</div>