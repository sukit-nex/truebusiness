<?php
$feature_name = 'top_up';

$tinymce = 'descriptionInput tinyMceExample';
$dropify = 'dropify';
$collapse_show = ''; // show / hide

$fields = $fields[$feature_name];

$row = array();
foreach ($fields['field'] as $key => $field) {
	$row[$key] = $field;
}

foreach ($fields['file'] as $key => $field) {
	$row[$key] = $field;
}

if ( $method != '' ) {
	
	if( sizeof( $detail ) > 0 ){
		foreach ($fields['field'] as $key => $field) {
			$row[$key] = isset($detail[DEFAULT_LANG][$key])? $detail[DEFAULT_LANG][$key] : '';
		}

		foreach ($fields['file'] as $key => $field) {
			$row[$key] = isset($detail[DEFAULT_LANG][$key])? $detail[DEFAULT_LANG][$key] : '';
		}
	}

} else {
	$tinymce = 'original-tinymce';
	$dropify = 'original-dropify';
	$collapse_show = 'show';
}

$inc_data = array(
	'feature_name' => $feature_name,
	'tinymce' => $tinymce,
	'dropify' => $dropify,
	'fields' => $fields,
	'pkg_number' => $pkg_number,
	'langs'	=> $langs,
	'pagesList'	=> $pagesList,
	'detail'	=> $detail,
	'collapse_show'	=> $collapse_show
);
?>

<?php sme_accordion_header($inc_data, $row ); ?>

		<div class="row">
			<div class="col-12">
                
				<?php sme_header_box($inc_data, $row ); ?>

				
				<div class="row mb-3 wrapper_special_offer">
					<div class="col-4">
						<div class="card-title">Select Topup</div>
						
						<div class="box-cate">
                            <div class="box-cate-wrap"> 
                                <?php 
								if ( isset( $sp_top_up ) && sizeof($sp_top_up) > 0 ) {
									if ( $row['special_offer_select'] != '' ) {
										$special_offer_ar = explode(',',$row['special_offer_select']);
									} else {
										$special_offer_ar = array();
									}

									foreach( $sp_top_up as $sp_row ) {
								?>
                                    <div class="box-level box-level-0">
                                        <div class="form-check form-check-dark">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" <?php echo in_array( $sp_row['code'], $special_offer_ar ) ? 'checked' : ''; ?> name="special_offer[]" data-name="<?php echo $sp_row['name']; ?>" value="<?php echo $sp_row['code']; ?>"> <?php echo $sp_row['name']; ?>
                                            </label> 
                                        </div>
                                    </div> <!-- end level 0 -->
                                <?php 
									}
								} 
								?>
                            </div>
                        </div>

					</div>
					<div class="col-8">
						<div class="result_special_offer">
								<ul class="gen-topup drag-item-list list-group special_offer_sortable">
									<?php
									
									$sos_ar = explode(',',$row['special_offer_select']);
									if ( isset( $sos_ar ) && sizeof($sos_ar) > 0 ) {
										foreach( $sos_ar as $sos_row ) {

											foreach( $sp_top_up as $sp_row ) {
												if ( $sos_row == $sp_row['code'] ) {
									?>
									<li class="drag-list level-0 sp-<?php echo $sp_row['code']; ?>">
										<input type="hidden" name="package[<?php echo $pkg_number; ?>][special_offer_select][]" value="<?php echo $sp_row['code']; ?>">
										<div class="box-select-topup ">
											<div class="box-txt-topup">
												<p><?php echo $sp_row['name']; ?></p>
											</div>
											<div class="icon-move">
												<i class="mdi mdi-cursor-move"></i>
											</div>	
										</div>
									</li>
									<?php
												}
											}

										}
									}
									?>
								</ul>
						</div>
					</div>
				</div>

				

				<?php sme_display_content($inc_data, $row ); ?>
			</div>
		</div>
	</div>
</div>