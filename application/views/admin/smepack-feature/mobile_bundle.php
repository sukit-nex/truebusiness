<?php
$feature_name = 'mobile_bundle';

$tinymce = 'descriptionInput tinyMceExample';
$dropify = 'dropify';
$collapse_show = ''; // show / hide

$fields = $fields[$feature_name];

$row = array();
foreach ($fields['field'] as $key => $field) {
	$row[$key] = $field;
}

foreach ($fields['file'] as $key => $field) {
	$row[$key] = $field;
}

if ( $method != '' ) {
	
	if( sizeof( $detail ) > 0 ){
		foreach ($fields['field'] as $key => $field) {
			$row[$key] = isset($detail[DEFAULT_LANG][$key])? $detail[DEFAULT_LANG][$key] : '';
		}

		foreach ($fields['file'] as $key => $field) {
			$row[$key] = isset($detail[DEFAULT_LANG][$key])? $detail[DEFAULT_LANG][$key] : '';
		}
	}

} else {
	$tinymce = 'original-tinymce';
	$dropify = 'original-dropify';
	$collapse_show = 'show';
}

$inc_data = array(
	'feature_name' => $feature_name,
	'tinymce' => $tinymce,
	'dropify' => $dropify,
	'fields' => $fields,
	'pkg_number' => $pkg_number,
	'langs'	=> $langs,
	'pagesList'	=> $pagesList,
	'detail'	=> $detail,
	'collapse_show'	=> $collapse_show
);
?>

<?php sme_accordion_header($inc_data, $row ); ?>

		<div class="row">
			<div class="col-12">

				<?php sme_header_box($inc_data, $row ); ?>

				<div class="box-tab-switch">
					<div class="card-title">Main Information</div>
					<div class="form-group is-tab">
						<div class="row">
							<div class="col-md-4 col-sm-2 col-xs-12">
								<div class="box-radio box-check-white-bd">
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][main_type]" value="template" <?php echo ($row['main_type'] == 'template')?'checked':''; ?> >
											Template
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-2 col-xs-12-bd">
								<div class="box-radio box-check-white-bd">
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][main_type]" value="manual" <?php echo ($row['main_type'] == 'manual')?'checked':''; ?>>
											Manual
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="box-form-inner template" <?php echo ($row['main_type'] == 'template')?'':'style="display:none;"'; ?>>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>จำนวนการโทร</label></div>
								<div class="col-md-9 d-md-flex align-items-center">
									<div class="form-check form-check-dark mr-3">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][main_template_type_calls]" value="อั้น" <?php echo ($row['main_template_type_calls'] == 'อั้น')?'checked':''; ?>>
											อั้น
										</label>
									</div>
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][main_template_type_calls]" value="ไม่อั้น" <?php echo ($row['main_template_type_calls'] == 'ไม่อั้น')?'checked':''; ?>>
											ไม่อั้น
										</label>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>ระบุจำนวนการโทร (นาที)</label></div>
								<div class="col col-12 col-md-9">
									<input type="text" name="package[<?php echo $pkg_number; ?>][main_template_number_calls]" class="form-control" value="<?php echo $row['main_template_number_calls']; ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>ช่วงเวลาโทรฟรี</label></div>
								<div class="col col-12 col-md-9">
									<div class="tab-content tab-content-lang">
									<?php foreach( $langs as $lang ){ ?>
										<?php
											if( isset( $detail[ $lang['text'] ] ) ) {
												$main_template_time_calls = $detail[ $lang['text'] ]['main_template_time_calls'];
											} else {
												$main_template_time_calls = '';
											}
										?>
										<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
											<input type="text" name="package[<?php echo $pkg_number; ?>][main_template_time_calls][]" class="form-control" value="<?php echo $main_template_time_calls; ?>">
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>จำนวนการโทร</label></div>
								<div class="col-md-9 d-md-flex align-items-center">
									<div class="form-check form-check-dark mr-3">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][main_template_type_networks]" value="ทุกเครือข่าย" <?php echo ($row['main_template_type_networks'] == 'ทุกเครือข่าย')?'checked':''; ?>>
											ทุกเครือข่าย
										</label>
									</div>
									<div class="form-check form-check-dark mr-3">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][main_template_type_networks]" value="นอกเครือข่าย" <?php echo ($row['main_template_type_networks'] == 'นอกเครือข่าย')?'checked':''; ?>>
											นอกเครือข่าย
										</label>
									</div>
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][main_template_type_networks]" value="ในเครือข่าย" <?php echo ($row['main_template_type_networks'] == 'ในเครือข่าย')?'checked':''; ?>>
											ในเครือข่าย
										</label>
									</div>
								</div>
								
							</div>
						</div>

						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>จำนวนการใช้อินเทอร์เน็ต</label></div>
								<div class="col-md-9 d-md-flex align-items-center">
									<div class="form-check form-check-dark mr-3">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][main_template_internet_type_use]" value="อั้น" <?php echo ($row['main_template_internet_type_use'] == 'อั้น')?'checked':''; ?>>
											อั้น
										</label>
									</div>
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][main_template_internet_type_use]" value="ไม่อั้น" <?php echo ($row['main_template_internet_type_use'] == 'ไม่อั้น')?'checked':''; ?>>
											ไม่อั้น
										</label>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>ระบุจำนวนใช้งาน (GB)</label></div>
								<div class="col col-12 col-md-9">
									<input type="text" name="package[<?php echo $pkg_number; ?>][main_template_usage_amount]" class="form-control" value="<?php echo $row['main_template_usage_amount']; ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>ความเร็วสูงสุด (Mbps)</label></div>
								<div class="col col-12 col-md-9">
									<input type="text" name="package[<?php echo $pkg_number; ?>][main_template_maximum_speed]" class="form-control" value="<?php echo $row['main_template_maximum_speed']; ?>">
								</div>
							</div>
						</div>

					</div>
					
					<div class="box-form-inner manual" <?php echo ($row['main_type'] == 'manual')?'':'style="display:none;"'; ?>>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>รายละเอียด</label></div>
								<div class="col col-12 col-md-9">

										<div class="tab-content tab-content-lang">
											<?php foreach( $langs as $lang ){ ?>
												<?php
													if( isset( $detail[ $lang['text'] ] ) ) {
														$main_manual_description = $detail[ $lang['text'] ]['main_manual_description'];
													} else {
														$main_manual_description = '';
													}
												?>
												<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
													<div class="form-group mb-3">
														<textarea name="package[<?php echo $pkg_number; ?>][main_manual_description][]" class="form-control <?php echo $tinymce; ?>" rows="7" ><?php echo $main_manual_description; ?></textarea>
													</div>
												</div>
											<?php } ?>
										</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="box-tab-switch">
					<div class="card-title">Detail in toggle</div>
					<div class="form-group is-tab">
						<div class="row">
							<div class="col-md-4 col-sm-2 col-xs-12">
								<div class="box-radio box-check-white-bd">
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][detail_type]" value="template" <?php echo ($row['detail_type'] == 'template')?'checked':''; ?> >
											Template
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-2 col-xs-12-bd">
								<div class="box-radio box-check-white-bd">
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][detail_type]" value="manual" <?php echo ($row['detail_type'] == 'manual')?'checked':''; ?>>
											Manual
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-2 col-xs-12-bd">
								<div class="box-radio box-check-white-bd">
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][detail_type]" value="none" <?php echo ($row['detail_type'] == 'none')?'checked':''; ?> >
											None
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box-form-inner template" <?php echo ($row['detail_type'] == 'template')?'':'style="display:none;"'; ?>>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>ค่าบริการส่วนเกิน (บาท/นาที)</label></div>
								<div class="col col-12 col-md-9">
									<input type="text" name="package[<?php echo $pkg_number; ?>][detail_template_service_fee]" class="form-control" value="<?php echo $row['detail_template_service_fee']; ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>เงื่อนไขโทรฟรี</label></div>
								<div class="col col-12 col-md-9">
									<div class="tab-content tab-content-lang">
										<?php foreach( $langs as $lang ){ ?>
										<?php
											if( isset( $detail[ $lang['text'] ] ) ) {
												$detail_template_freecall_conditions = $detail[ $lang['text'] ]['detail_template_freecall_conditions'];
											} else {
												$detail_template_freecall_conditions = '';
											}
										?>
										<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
											<input type="text" name="package[<?php echo $pkg_number; ?>][detail_template_freecall_conditions][]"  class="form-control" value="<?php echo $detail_template_freecall_conditions; ?>">
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>4G HD Voice</label></div>
								<div class="col col-12 col-md-9">
									<div class="tab-content tab-content-lang">
										<?php foreach( $langs as $lang ){ ?>
											<?php
												if( isset( $detail[ $lang['text'] ] ) ) {
													$detail_template_4g_hd_voice = $detail[ $lang['text'] ]['detail_template_4g_hd_voice'];
												} else {
													$detail_template_4g_hd_voice = '';
												}
											?>
										<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
											<input type="text" name="package[<?php echo $pkg_number; ?>][detail_template_4g_hd_voice][]" class="form-control" value="<?php echo $detail_template_4g_hd_voice; ?>">
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>ใช้งานครบ-ความเร็วสูงสุด (Kbps)</label></div>
								<div class="col col-12 col-md-9">
									<input type="text" name="package[<?php echo $pkg_number; ?>][detail_template_use_full_speed]" class="form-control" value="<?php echo $row['detail_template_use_full_speed']; ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>จำนวนการใช้ WiFi</label></div>
								<div class="col-md-9 d-md-flex align-items-center">
									<div class="form-check form-check-dark mr-3">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][detail_template_wifi_type_use]" value="อั้น" <?php echo ($row['detail_template_wifi_type_use'] == 'อั้น')?'checked':''; ?>>
											อั้น
										</label>
									</div>
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][detail_template_wifi_type_use]" value="ไม่อั้น" <?php echo ($row['detail_template_wifi_type_use'] == 'ไม่อั้น')?'checked':''; ?>>
											ไม่อั้น
										</label>
									</div>
								</div>	
							</div>
						</div>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>ระบุจำนวนใช้งาน (GB)</label></div>
								<div class="col col-12 col-md-9">
									<input type="text" name="package[<?php echo $pkg_number; ?>][detail_template_usage_amount]" class="form-control" value="<?php echo $row['detail_template_usage_amount']; ?>">
								</div>
							</div>
						</div>						
					</div>
					<div class="box-form-inner manual" <?php echo ($row['detail_type'] == 'manual')?'':'style="display:none;"'; ?>>
						<div class="form-group">
							<div class="row d-block d-md-flex align-items-center">
								<div class="col col-12 col-md-3"><label>รายละเอียด</label></div>
								<div class="col col-12 col-md-9">
									
									<div class="tab-content tab-content-lang">
										<?php foreach( $langs as $lang ){ ?>
											<?php
												if( isset( $detail[ $lang['text'] ] ) ) {
													$detail_manual_description = $detail[ $lang['text'] ]['detail_manual_description'];
												} else {
													$detail_manual_description = '';
												}
											?>
											<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
												<div class="form-group mb-3">
													<textarea name="package[<?php echo $pkg_number; ?>][detail_manual_description][]" class="form-control <?php echo $tinymce; ?>" rows="7" ><?php echo $detail_manual_description; ?></textarea>
												</div>
											</div>
										<?php } ?>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php sme_link_button($inc_data, $row ); ?>

				<?php sme_display_content($inc_data, $row ); ?>
			</div>
		</div>
	</div>
</div>