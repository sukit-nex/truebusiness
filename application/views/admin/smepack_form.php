<?php
	if( sizeof( $detail ) > 0 ){
		$code = $detail[DEFAULT_LANG]['code'];
		$child_cate_code = $detail[DEFAULT_LANG]['child_cate_code'];
		$highlight_sme_mainpage = $detail[DEFAULT_LANG]['highlight_sme_mainpage'];
		$highlight_homepage = $detail[DEFAULT_LANG]['highlight_homepage'];
		
		$name = $detail[DEFAULT_LANG]['name'];
		$price = $detail[DEFAULT_LANG]['price'];
		$tc_pdf = $detail[DEFAULT_LANG]['tc_pdf'];

		$sticky = $detail[DEFAULT_LANG]['sticky'];
		$onoff = $detail[DEFAULT_LANG]['onoff'];

	}else{
		$code = $data_code;
		$child_cate_code = '';
		$highlight_sme_mainpage = '';
		$highlight_homepage = '';

		$name = '';
		$price = '';
		$tc_pdf = '';

		$sticky = 0;
		$onoff = 1;
	}
?>
<div class="content-wrapper">
	<div class="row">
		<div class="col-12">
			<div class="page-header">
			<h3 class="page-title"> SME Pack Form : <?php echo $cate_detail[DEFAULT_LANG]['title']; ?></h3>
			</div>
			<ul class="nav nav-tabs nav-tabs-lang" role="tablist" style="">
				<?php foreach( $langs as $lang ){ ?>
				<li class="nav-item">
					<a class="nav-link head-<?php echo $lang['text']; ?> <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" 
					href="javascript:activaTabNex('<?php echo $lang['text']; ?>');"><?php echo $lang['text']; ?></a>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>

	<?php echo form_open_multipart('admin/smepack/saveForm', ' class="mainForm"'); ?>
		<input type="hidden" name="code" value="<?php echo $code ?>" >
		<input type="hidden" name="cate_code" value="<?php echo $cate_code ?>" >
		<input type="hidden" name="time" value="<?php echo $time; ?>" >
		<div class="row">
			<div class="col-12 col-md-8">
				<div class="card mb-3 b-r-tl">
					<div class="card-body">
						<div class="row">
							<div class="col-8">
								<div class="row">
									<div class="col-12">
										<div class="form-group mb-3">
											<label>Package name</label>
											<input type="text" name="name" class="form-control"   placeholder="Package name" value="<?php echo $name; ?>">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group mb-3">
											<label>Price</label>
											<input type="text" name="price" class="form-control"  placeholder="Price" value="<?php echo $price; ?>">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group mb-3">
											<label>Billing cycle</label>
											<div class="tab-content tab-content-lang">
												<?php foreach( $langs as $lang ){ ?>
													<?php
														if( isset( $detail[ $lang['text'] ] ) ) {
															$price_billing_cycle = $detail[ $lang['text'] ]['price_billing_cycle'];
														} else {
															$price_billing_cycle = '';
														}
													?>	
												<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
													<input type="text" name="price_billing_cycle[]" placeholder="Billing cycle" class="form-control" value="<?php echo $price_billing_cycle; ?>">
												</div>

												<?php } ?>
											</div>

										</div>
									</div>
									<div class="col-12">
                                        <div class="tab-content tab-content-lang">
                                        <?php foreach( $langs as $lang ){ ?>
                                            <?php
                                                if( isset( $detail[ $lang['text'] ] ) ) {
                                                    $tc_pdf = $detail[ $lang['text'] ]['tc_pdf'];
                                                } else {
                                                    $tc_pdf = '';
                                                }
                                            ?>	
                                                <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">                 
                                                    <div class="form-group">
                                                        <label>T&C PDF</label>
                                                        <input type="file" name="tc_pdf[]" class="file-upload-default">
                                                        <div class="input-group col-xs-12">
                                                            <input type="text" class="form-control file-upload-info" disabled placeholder="Upload PDF" value="<?php echo $tc_pdf; ?>">
                                                            <span class="input-group-append">
                                                                <button class="file-upload-browse btn  btn-gradient-dark" type="button">Upload</button>
                                                            </span>
                                                        </div>
                                                        <input type="hidden" name="tc_pdf_txt[]" value="<?php echo $tc_pdf; ?>" >
                                                    </div>
                                                </div>

                                            <?php } ?>
                                        </div>

									</div>
								</div>		
							</div>
							<div class="col-4">
								<label>Category</label>
								<div class="box-cate">
									<div class="box-cate-wrap"> 
										<?php foreach( $cate_lists as $key => $row ){ ?>
											<div class="box-level box-level-0">
												<div class="form-check form-check-dark">
													<label class="form-check-label">
														<input class="form-check-input" type="radio" <?php echo ( $row['code'] == $child_cate_code ) ? 'checked' : ''; ?> name="child_cate_code" value="<?php echo $row['code']; ?>"> <?php echo $row['title']; ?> 
													</label>
												</div>
											</div> <!-- end level 0 -->
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- </div>

					<div class="card mb-3"> -->
					<div class="card-body">
						<h4 class="card-title">Package detail</h4>
						<div class="drag-wrapper">
							<div class="box-feature">
								<div class="row">
									<div class="col col-12">
										<div class="package-data">
											<div class="accordion accordion-solid-header " id="accordion-5" role="tablist">
												<div class="drag-item-list list-group" id="package-sortable"> 
													<?php 
														if ( sizeof($default_feature) > 0 ) {
															//print_r($default_feature);
															foreach ($default_feature as $key => $val) {
																echo $val;
															}
														}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="card mb-3">
					<div class="card-body">
						<div class="box-tab-switch">
							<div class="form-group ">
								<div class="form-check form-check-dark">
									<label class="form-check-label"><input class="form-check-input" type="checkbox"  name="highlight_homepage" value="1" <?php echo ( $highlight_homepage == 1 ) ? 'checked' : ''; ?> >Highlight to homepage</label>
								</div>
								<div class="form-check form-check-dark">
									<label class="form-check-label"><input class="form-check-input" type="checkbox"  name="highlight_sme_mainpage" value="1" <?php echo ( $highlight_sme_mainpage == 1 ) ? 'checked' : ''; ?> >Highlight to SME Main page</label>
								</div>
							</div>

							<div class="box-form-inner highlight_sme_mainpage_content" <?php echo ($highlight_sme_mainpage == '1')?'':'style="display:none;"'; ?>>
								<div class="form-group">
									<div class="row d-block d-md-flex align-items-center">
										<div class="col col-12">

												<div class="tab-content tab-content-lang">
													<?php foreach( $langs as $lang ){ ?>
														<?php
															if( isset( $detail[ $lang['text'] ] ) ) {
																$highlight_main_desciption = $detail[ $lang['text'] ]['highlight_main_desciption'];
															} else {
																$highlight_main_desciption = '';
															}
														?>
														<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
															<div class="form-group mb-3">
																<textarea type="text" name="highlight_main_desciption[]" class="form-control descriptionInput tinyMceExample" ><?php echo $highlight_main_desciption; ?></textarea>
															</div>
														</div>
													<?php } ?>
												</div>
										
										</div>
									</div>
								</div>
								
							</div>


							<div class="box-form-inner form-nobr" >
								<div class="form-group">
									<label>เลือก Feature</label>
									<select class="form-control mb-5" name="sel-feature">
										<?php foreach (sme_feature_list() as $key => $row) { ?>
											<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<div class="text-center">
									<button type="button" class="btn btn-gradient-dark btn-fw-lg addFeature">Add</button>
								</div>
							</div>
						</div>
						
						
						
					</div>
				</div>
			</div>
		</div>

		<div class="card mb-3">
			<div class="card-body">
				<div class="form-group mb-3">
					<label for="">Order </label>
					<input type="text" name="sticky" class="form-control"  placeholder="Order" value="<?php echo $sticky; ?>">
				</div>

				<div class="form-group">
					<label for="">Status</label>
					<div class="form-check form-check-dark">
						<label class="form-check-label">
							<input type="radio" class="form-check-input" name="onoff" value="1"  <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
					</div>
					<div class="form-check form-check-dark">
						<label class="form-check-label">
							<input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 ? 'checked' : ''; ?>> Non - Publish </label>
					</div>
				</div>

				<div class="row">
					<div class="col-12 text-right">
						<div class="text-center card-button">
							<button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php echo form_close(); ?>
</div>
<!-- content-wrapper ends -->
<?php include_once("inc/nexitor-modal.php"); ?>

<div class="d-none data-original">
	<?php 
	if ( isset($original_feature) && sizeof($original_feature) > 0 ) {
		//print_r( $original_feature );
		foreach ($original_feature as $key => $row) {
			echo $row;
		}
	}
	?>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="smepack_deleteModal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete item ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body cate-message" style="display:none;">
        <p class="modal-title" id="exampleModalLabel">Warning : This category will be gone from all section</p>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-gradient-dark smepack-modal-delete-confirm" data-pkg="" id="modal-btn-si">Yes</button>
        <button type="button" data-dismiss="modal" class="btn btn-danger" id="modal-btn-no">No</button>
      </div>
    </div>
  </div>
</div>
