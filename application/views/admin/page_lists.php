<?php
  $detail = array();
  if( sizeof( $detail ) > 0 ){
    $code = $detail[DEFAULT_LANG]['code'];
    $cate_code = $detail[DEFAULT_LANG]['cate_code'];
    $page_type = $detail[DEFAULT_LANG]['page_type'];
    $seo_id = $detail[DEFAULT_LANG]['seo_id'];
    $seo_status = $detail[DEFAULT_LANG]['seo_status'];

    $section_type = $detail[DEFAULT_LANG]['section_type'];
    $banner_cate = $detail[DEFAULT_LANG]['banner_cate'];

    $onoff = $detail[DEFAULT_LANG]['onoff'];

    $tag = $detail[DEFAULT_LANG]['tag'];
    $image = $detail[DEFAULT_LANG]['image'];
    $image_m = $detail[DEFAULT_LANG]['image_m'];

    $add_on_page = $detail[DEFAULT_LANG]['add_on_page'];



    $sec_name = $detail[DEFAULT_LANG]['sec_name'];

  }else{
    $code = '';
    $cate_code = "";
    $page_type = "default";
    
    $seo_id = '';
    $seo_status = 0;

    $section_type = 0;
    $banner_cate = "";

    $onoff = 1;

    $tag = "";
    $image = "";
    $image_m = "";
    $add_on_page = "";

    $sec_name = "";
    $title = "";
    $subtitle = "";
    $video_id = "";
    $bg1_desktop = "";
  }

?>
<div class="content-wrapper">
    <div class="page-header">
        <h4 class="page-title">Page Lists</h4>
        <a href="<?php   echo base_url().'admin/pages/add'?>" class="btn btn-gradient-dark btn-fw">+ Add more</a>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">

                        <table id="order-listing" class="table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Sticky</th>
                                    <th style="width:20%;">Page</th>
                                    <th>Status</th>
                                    <th>Post log</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach( $lists as $list ){ ?>
                                <tr>
                                    <td><?php echo $list['code']; ?></td>
                                    <td><?php echo $list['sticky']; ?></td>
                                    <td style="white-space: normal;">
                                        <span class="float-left"
                                            style="width:300px; line-height:1.4;"><?php echo $list['page_name']; ?></span>
                                    </td>
                                    <td>
                                        <?php if( $list['onoff'] == 1 ){  ?>
                                        <div class="badge badge-pill badge-outline-success"><i
                                                class="mdi mdi-check mr-1"></i>Enable</div>
                                        <?php }else{  ?>
                                        <div class="badge badge-pill badge-outline-danger"><i
                                                class="mdi mdi-check mr-1"></i>Disable</div>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <div class="badge badge-pill badge-danger btn-inverse-dark"
                                            data-toggle="tooltip" data-placement="right"
                                            title="Created : <?php  echo $list['c_date']; ?> Updated : <?php echo $list['u_date'];  ?>">
                                            <i class="mdi mdi-clock"></i> Date posted</div>
                                    </td>
                                    <td>
                                        <a href="<?php   echo base_url().'admin/section/allrow/'.$list['code']; ?>"
                                            class="btn btn-info">Edit</a>
                                        <input type="hidden"
                                            name="<?php  echo $this->security->get_csrf_token_name(); ?>"
                                            value="<?php  echo $this->security->get_csrf_hash();  ?>" />
                                    <?php  if( check_permission( $this->session->userdata('user_type'), 'delete_page') ){ ?>
                                        <a href="javascript:void(0);" class="btn btn-danger delete-item"
                                            onclick="deleteItem( $(this) );" data-toggle="modal"
                                            data-target="#deleteModal" data-delete-by="code" data-table="pages"
                                            data-code="<?php  echo $list['code'];  ?>">Delete</a>
                                    <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                                <?php /*  $i++; } */  ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- content-wrapper ends -->