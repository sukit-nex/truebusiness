<div class="content-wrapper">
  <div class="page-header">
    <h4 class="page-title"> <?php echo strtoupper( 'Content : '.$param_page.' - '.$param_sec_key) ?> </h4>
    <?php if($param_sec_key != "touchthebeyond" ){ ?>
    <a href="<?php echo base_url().'admin/section/content/add/'.$param_page.'/'.$param_sec_key ?>" class="btn btn-gradient-dark btn-fw">+ Add more</a> 
    <?php } ?>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <div class="table-responsive">

          <table id="order-listing" class="table">
            <thead>
              <tr>
                <th>No.</th>
                <th>Sticky</th>
                <th style="width:20%;">Title</th>
                <th>Status</th>
                <th>Post log</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach( $lists as $list ){ ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $list['sticky']; ?></td>
                  <td style="white-space: normal;">
  
                    <?php if($param_sec_key == "seetheunseen" || $param_sec_key == "touchthebeyond" || $param_sec_key == "vrproduct" || $param_sec_key == "vrpartner"  ){?>
                      <span class="float-left" style="width:300px; line-height:1.4;"><img style="width: 100px; height: auto; border-radius: 0;" src="<?php echo upload_path($list['image']); ?>" alt=""></span>
                    <?php }else{ ?>
                    <span class="float-left" style="width:300px; line-height:1.4;"><?php echo $list['title']; ?></span>
                    <?php } ?>
                  </td>
                  <td>
                    <?php if( $list['onoff'] == 1 ){ ?>
                      <div class="badge badge-pill badge-outline-success"><i class="mdi mdi-check mr-1"></i>Enable</div>
                    <?php }else{ ?>
                      <div class="badge badge-pill badge-outline-danger"><i class="mdi mdi-check mr-1"></i>Disable</div>
                    <?php } ?>
                  </td>
                  <td><div class="badge badge-pill badge-danger btn-inverse-dark"  data-toggle="tooltip" data-placement="right" title="Created : <?php echo $list['c_date']; ?> Updated : <?php echo $list['u_date']; ?>"><i class="mdi mdi-clock"></i> Date posted</div></td>
                  <td>
                    <a href="<?php echo base_url().'admin/section/content/edit/'.$param_page.'/'.$param_sec_key.'/'.$list['code']; ?>" class="btn btn-info">Edit</a>
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                    <?php if($param_sec_key != "touchthebeyond"){?>
                      <a href="javascript:void(0);" class="btn btn-danger delete-item" onclick="deleteItem( $(this) );" data-toggle="modal" data-target="#deleteModal" data-delete-by="code" data-table="section_lists" data-code="<?php echo $list['code']; ?>">Delete</a>
                    <?php } ?>
                  </td>
                </tr>
              <?php $i++; } ?>
            </tbody>
          </table>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
