<?php
  if( sizeof( $detail ) > 0 ){
    $id = $detail[0]['id'];
    $name = $detail[0]['name'];
    $lastname = $detail[0]['lastname'];
    $position = $detail[0]['position'];
    $thumb = $detail[0]['thumb'];
    $username = $detail[0]['username'];
    $password = $detail[0]['password'];
    $email = $detail[0]['email'];
    $user_type = $detail[0]['user_type'];
    $onoff = $detail[0]['onoff'];
  }else{
    $id = '';
    $name = '';
    $lastname = '';
    $position = '';
    $thumb = '';
    $username = '';
    $password = '';
    $email = '';
    $user_type = '';
    $onoff = 1;
  }
?>
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title"> User Form </h3>
    <?php if( $id !== "" ){ ?><a href="" class="btn btn-gradient-dark btn-fw" data-toggle="modal" data-target="#changepasswordModal">Change Password</a><?php } ?>
  </div>
  <div class="card">
    <div class="card-body">
   <?php echo form_open_multipart('admin/user/saveForm', 'id="signupForm"'); ?>
      <input type="hidden" name="id" value="<?php echo $id; ?>" >
      <div class="form-group">
        <div class="row">
          <div class="col-12">
            <label>Profile Image <small class="text-muted">( 200px x 200px )</small></label>
            <input type="file" data-height="250" name="thumb" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $thumb != "" ? upload_path($thumb) : ''; ?>" />
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="">Name *</label>
        <input type="text" name="firstname" class="form-control"  placeholder="Name" value="<?php echo $name; ?>">
      </div>
      <div class="form-group">
        <label for="">Lastname *</label>
        <input type="text" name="lastname" class="form-control"  placeholder="Lastname" value="<?php echo $lastname; ?>">
      </div>
      <div class="form-group">
        <label for="">Position</label>
        <input type="text" name="position" class="form-control"  placeholder="Position" value="<?php echo $position; ?>">
      </div>
      <div class="form-group">
        <label for="">Username *</label>
        <input type="text" name="username" class="form-control"  placeholder="Username" value="<?php echo $username; ?>">
      </div>
      <?php if( $id == "" ){ ?>
        <div class="form-group">
          <label for="">Password *</label>
          <input type="password" name="password" id="password" class="form-control"  placeholder="Password" value="<?php echo $password; ?>">
        </div>
        <div class="form-group">
          <label for="">Confirm Password *</label>
          <input type="password" name="confirm_password" class="form-control"  placeholder="Confirm Password" value="">
        </div>
      <?php } ?>
      
        <div class="form-group">
          <label>User type</label>
          <select class="form-control" name="user_type">
            <?php $all_role = getRole(); ?>
            <?php foreach( $all_role as $key => $role ){ ?>
              <?php if( $this->session->userdata('user_type') == "SA" && $key == 0 ) { ?>
                <option value="<?php echo $role['role_code']; ?>" <?php echo $user_type == $role['role_code'] ? 'selected' : ''; ?>><?php echo $role['role_text']; ?></option>
              <?php } ?>
              <?php if( ( $this->session->userdata('user_type') == "SA" || $this->session->userdata('user_type') == "A") && $key != 0 ) { ?>
                <option value="<?php echo $role['role_code']; ?>" <?php echo $user_type == $role['role_code'] ? 'selected' : ''; ?>><?php echo $role['role_text']; ?></option>
              <?php } ?>
            <?php } ?>
          </select>
        </div>
      
      <?php if( $id != $this->session->userdata('id') ){ ?>
        <div class="form-group">
          <label for="">Status</label>
          <div class="form-check">
            <label class="form-check-label">
              <input type="radio" class="form-check-input" name="onoff" value="1"  <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
          </div>
          <div class="form-check">
            <label class="form-check-label">
              <input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 ? 'checked' : ''; ?>> Unpublish </label>
          </div>
        </div>
      <?php }else{ ?>
        <input type="hidden" value="1" name="onoff" />
      <?php } ?>

      <div class="text-center card-button">
        <button type="submit" class="btn btn-gradient-dark btn-fw-lg mr-2">Save</button>
      </div>
    <?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="modal fade nexitor-modal" id="changepasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <img src="<?php echo ADMIN_ASSET; ?>images/nexitor/icon-cancel.svg" class="svg">
    </button>
    <div class="modal-content">
        <?php echo form_open_multipart('admin/user/changePass', 'id="passwordForm"'); ?>
          <div class="modal-body ">
            <input type="hidden" name="id" value="<?php echo $id; ?>" >
            <div class="form">
              <h6 for="">New password *</h6>
              <input type="password" name="password" id="password" class="form-control"  placeholder="Password" value="" required>
            </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-gradient-light btn-fw" data-dismiss="modal" aria-label="Close">Cancel</button>
            <button type="submit" class="btn btn-gradient-dark btn-fw">Submit</button>
          </div>
        <?php echo form_close(); ?>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
