<?php
	if ( sizeof($detail) > 0 ){
		$code = $detail[DEFAULT_LANG]['code'];

		$bg_section = $detail[DEFAULT_LANG]['bg_section'];
		$banner_d = $detail[DEFAULT_LANG]['banner_d'];
    	$banner_m = $detail[DEFAULT_LANG]['banner_m'];
		$icon_image = $detail[DEFAULT_LANG]['icon_image'];
		//$brochure_pdf = $detail[DEFAULT_LANG]['brochure_pdf'];
		
		$slug = $detail[DEFAULT_LANG]['slug'];
		$onoff = $detail[DEFAULT_LANG]['onoff'];
		$sticky = $detail[DEFAULT_LANG]['sticky'];

		$btn_text = 'Update';
	} else {
		$code = '';
		$bg_section = '';
		$banner_d = '';
    	$banner_m = '';
		$icon_image = '';
		//$brochure_pdf = '';
		$slug = '';
		$onoff = 0;
		$sticky = 0;

		$btn_text = 'Save';
	}

	if( sizeof( $seo ) > 0 ){
		$share_img = $seo[DEFAULT_LANG]['share_img'];
	}else{
		$share_img = '';
	}

?>
<div class="content-wrapper">
	<div class="page-header">
		<h4 class="page-title"> Category for <?php echo $detail[DEFAULT_LANG]['title']; ?> </h4>
	</div>
	<?php echo form_open_multipart('admin/smepack_category/saveCategoryContentForm', ' class="update-cate-form"'); ?>

		<input type="hidden" name="code" value="<?php echo $code; ?>">
		<input type="hidden" name="parent_code" value="<?php echo $detail[DEFAULT_LANG]['code']; ?>">
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

	<div class="row">
		<div class="col-12 col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="row">

							<div class="col-12 col-md-3 image-box mb-3">
								<div class="form-group">
									<label>Icon image 
										<small class="text-muted">(50x50)</small>
									</label>
									<input type="file" data-height="200" name="icon_image" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $icon_image != "" ? upload_path($icon_image) : ''; ?>" />
									<input type="hidden" class="image_as_text" name="icon_image_txt" value="<?php echo $icon_image; ?>" />
								</div>
							</div>
							<div class="col-12 col-md-9 mb-3">
								<div class="form-group <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
									<ul class="nav nav-tabs" role="tablist">
										<?php foreach( $langs as $lang ){ ?>
											<li class="nav-item">
												<a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo $lang['text']; ?>-tab" data-toggle="tab" href="#<?php echo $lang['text']; ?>" role="tab" aria-controls="<?php echo $lang['text']; ?>" aria-selected="true"><?php echo $lang['text']; ?></a>
											</li>
										<?php } ?>
									</ul>
									<div class="tab-content mb-3">
										<?php foreach( $langs as $lang ){ ?>
											<?php
											if( isset( $detail[ $lang['text'] ] ) ){
												$prefix_title = $detail[ $lang['text'] ]['prefix_title'];
												$sub_title = $detail[ $lang['text'] ]['sub_title'];
												$title = $detail[ $lang['text'] ]['title'];
												$description = $detail[ $lang['text'] ]['description'];
                                                $brochure_pdf = $detail[ $lang['text'] ]['brochure_pdf'];
											}else{
												$prefix_title = '';
												$sub_title = '';
												$title = '';
												$description = '';
                                                $brochure_pdf = '';
											}
											?>
											<div class="<?php echo 'lang_'.$lang['text']; ?> tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo $lang['text']; ?>" role="tabpanel" aria-labelledby="<?php echo $lang['text']; ?>-tab">
												<div class="row">
													<div class="form-group mb-3 col-md-6">
														<label for="">Prefix title </label>
														<input type="text" name="prefix_title[]" class="form-control"  placeholder="Prefix title" value="<?php echo $prefix_title; ?>">
													</div>
													<div class="form-group mb-3 col-md-6">
														<label for="">Title </label>
														<input type="text" name="title[]" class="form-control"  placeholder="Name" value="<?php echo $title; ?>">
													</div>
												</div>

												<div class="form-group mb-3">
													<label for="">Sub Title </label>
													<textarea name="sub_title[]" class="form-control " rows="3" placeholder="Sub Title" ><?php echo $sub_title; ?></textarea>
												</div>

                                                <div class="form-group mb-3">
													<label for="">Description</label>
													<textarea name="description[]" class="form-control " rows="3" placeholder="Description" ><?php echo $description; ?></textarea>
												</div>
												
                                                <div class="form-group">
                                                    <label>Brochure PDF</label>
                                                    <input type="file" name="brochure_pdf[]" class="file-upload-default">
                                                    <div class="input-group col-xs-12">
                                                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload PDF" value="<?php echo $brochure_pdf; ?>">
                                                        <span class="input-group-append">
                                                            <button class="file-upload-browse btn  btn-gradient-dark" type="button">Upload</button>
                                                        </span>
                                                    </div>
                                                    <input type="hidden" name="brochure_pdf_txt[]" value="<?php echo $brochure_pdf; ?>" >
                                                </div>

											</div>
										<?php } ?>
									</div>

								</div>
							</div>			
						
					</div>
				</div>
			</div>

			<div class="card mt-3">
				<div class="card-body">
					<div class="row">
						<!-- <div class="col-12 col-md-4 image-box">
							<div class="form-group">
								<label>Background section
									<small class="text-muted">(50x50)</small>
								</label>
								<input type="file" data-height="200" name="bg_section" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $bg_section != "" ? upload_path($bg_section) : ''; ?>" />
								<input type="hidden" class="image_as_text" name="bg_section_txt" value="<?php echo $bg_section; ?>" />
							</div>
						</div> -->
						<div class="col-12 col-md-6 image-box">
							<div class="form-group">
								<label>Banner Desktop
									<small class="text-muted">(50x50)</small>
								</label>
								<input type="file" data-height="200" name="banner_d" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $banner_d != "" ? upload_path($banner_d) : ''; ?>" />
								<input type="hidden" class="image_as_text" name="banner_d_txt" value="<?php echo $banner_d; ?>" />
							</div>
						</div>
						<div class="col-12 col-md-6 image-box mb-3">
							<div class="form-group">
								<label>Banner Mobile
									<small class="text-muted">(50x50)</small>
								</label>
								<input type="file" data-height="200" name="banner_m" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $banner_m != "" ? upload_path($banner_m) : ''; ?>" />
								<input type="hidden" class="image_as_text" name="banner_m_txt" value="<?php echo $banner_m; ?>" />
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="card mt-3">
				<div class="card-body">
					<div class="box-switch">
					<h3 class="card-title"> SEO Page setting </h3>
						<div class="button-switch seo-switch">
						<?php
						if( isset( $detail[ $lang['text'] ] ) ){
							if ( sizeof($seo) > 0 ) {
								$seo_code = $seo[ $lang['text'] ]['code'];
							} else {
								$seo_code = '';
							}
							
						}else{
							$seo_code = '';
						}
						?>
							<input type="checkbox" <?php // echo $seo_status == 1 ? 'checked' : '' ?> data-toggle="toggle"
								data-onstyle="info" data-on="Show" data-off="Hide" data-offstyle="outline-secondary" data-size="xs"
								name="seo_status_switch">
							<input type="hidden" name="seo_code" value="<?php echo $seo_code; ?>">
							<input type="hidden" name="seo_type" value="listing">
							

						</div>
						<div class="content-switch seo-toggle-wrap" style="display:none;">
							<div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
								<ul class="nav nav-tabs" role="tablist" style="">
									<?php foreach( $langs as $lang ){ ?>
									<li class="nav-item">
										<a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
											id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
											href="#seo-<?php echo 'title_'.$lang['text']; ?>" role="tab"
											aria-controls="<?php echo 'title_'.$lang['text']; ?>"
											aria-selected="true"><?php echo $lang['text']; ?></a>
									</li>
									<?php } ?>
								</ul>
								<div class="tab-content float-left w-100" style="">
									<?php foreach( $langs as $lang ){ ?>
									<?php
									if( isset( $detail[ $lang['text'] ] ) ){
										if ( sizeof($seo) > 0 ) {
											$page_title = $seo[ $lang['text'] ]['page_title'];
											$meta_title = $seo[ $lang['text'] ]['meta_title'];
											$meta_desc = $seo[ $lang['text'] ]['meta_desc'];
											$meta_keyword = $seo[ $lang['text'] ]['meta_keyword'];
										} else {
											$page_title = '';
											$meta_title = '';
											$meta_desc = '';
											$meta_keyword = '';
										}
										
									}else{
										$page_title = '';
										$meta_title = '';
										$meta_desc = '';
										$meta_keyword = '';
									}
									?>
									<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
										id="seo-<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
										aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
										<div class="form-group">
											<label for="">Page title</label>
											<input type="text" name="page_title[]" class="form-control" placeholder="Page title"
												value="<?php echo $page_title; ?>">
										</div>
										<div class="form-group">
											<label for="">Meta title</label>
											<input type="text" name="meta_title[]" class="form-control" placeholder="Meta title"
												value="<?php echo $meta_title; ?>">
										</div>
										<div class="form-group">
											<label for="">Meta keyword </label>
											<input type="text" name="meta_keyword[]" class="form-control meta_keyword" placeholder="Meta Keyword"
												value="<?php echo $meta_keyword; ?>">
										</div>
										<div class="form-group">
											<label for="">Meta description</label>
											<input type="text" name="meta_desc[]" class="form-control"
												placeholder="Meta Description" value="<?php echo $meta_desc; ?>">
										</div>
									</div>
									<?php } ?>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-12 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
										<label>Share Image<small class="text-muted">( 1200 x 630px )</small></label>
										<input type="file" data-height="250" name="share_img" class="dropify"
											data-max-file-size="3M"
											data-default-file="<?php echo $share_img != "" ? upload_path($share_img) : ''; ?>" />
										<input type="hidden" class="image_as_text" name="share_img_txt"
											value="<?php echo $share_img; ?>" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="card my-3">
				<div class="card-body">
					
					<div class="row mb-3">
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="">Slug</label>
								<input type="text" name="slug" class="form-control" placeholder="Slug" value="<?php echo $slug; ?>">
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="">Order</label>
								<input type="text" name="sticky" class="form-control" placeholder="Order" value="<?php echo $sticky; ?>">
							</div>
						</div>
					</div>
					

					<div class="form-group">
						<label for="">Status</label>
						<div class="form-check form-check-dark">
							<label class="form-check-label">
								<input type="radio" class="form-check-input" name="onoff" value="1"  <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
						</div>
						<div class="form-check form-check-dark">
							<label class="form-check-label">
								<input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 ? 'checked' : ''; ?>> Non - Publish </label>
						</div>
					</div>

					<div class="row">
						<div class="col-12 text-right">
							<div class="text-center card-button">					
								<button type="submit" class="btn btn-gradient-dark mr-2 w-100"><?php echo $btn_text; ?></button>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<?php form_close(); ?>
</div>

<!-- content-wrapper ends -->
