<div class="content-wrapper">
  <div class="page-header">
    <h4 class="page-title"> <?php echo strtoupper( 'Categories of '.$sec_detail[DEFAULT_LANG]['section_name'] ) ?> SECTION</h4>
    <div>
    <a href="<?php echo base_url().'admin/'.$menu_active; ?>" class="btn btn-gradient-light btn-fw">Back</a>
      <?php  if( check_permission( $this->session->userdata('user_type'), 'create_category') ){ ?>
        <a href="<?php echo base_url().'admin/section/categories/add/'.$sec_detail[DEFAULT_LANG]['code']; ?>" class="btn btn-gradient-dark btn-fw">+ Add more</a>
      <?php } ?> 
      
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <div class="table-responsive">

          <table id="order-listing" class="table">
            <thead>
              <tr>
                <th>No.</th>
                <th>Sticky</th>
                <th style="width:20%;">Title</th>
                <th>Status</th>
                <th>Log Post</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach( $lists as $list ){ ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $list['sticky']; ?></td>
                  <td style="white-space: normal;"><span class="float-left" style="width:300px; line-height:1.4;"><?php echo $list['title']; ?></span></td>
                  <td>
                    <?php if( $list['onoff'] == 1 ){ ?>
                      <div class="badge badge-pill badge-outline-success"><i class="mdi mdi-check mr-1"></i>Enable</div>
                    <?php }else{ ?>
                      <div class="badge badge-pill badge-outline-danger"><i class="mdi mdi-check mr-1"></i>Disable</div>
                    <?php } ?>
                  </td>
                  <td><div class="badge badge-pill badge-danger btn-inverse-dark"  data-toggle="tooltip" data-placement="right" title="Created : <?php echo $list['c_date']; ?> Update : <?php echo $list['u_date']; ?>"><i class="mdi mdi-clock"></i> Date post</div></td>
                  <td>
                    <?php  if( check_permission( $this->session->userdata('user_type'), 'edit_category') ){ ?>
                      <a href="<?php echo base_url().'admin/section/categories/edit/'.$sec_detail[DEFAULT_LANG]['code'].'/'.$list['code']; ?>" class="btn btn-info">Edit</a>
                    <?php } ?>
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                    <?php  if( check_permission( $this->session->userdata('user_type'), 'delete_category') ){ ?>
                      <a href="javascript:void(0);" class="btn btn-danger delete-item" onclick="deleteItem( $(this) );" data-toggle="modal" data-delete-type="cate" data-target="#deleteModal" data-delete-by="code" data-table="section_cate" data-code="<?php echo $list['code']; ?>">Delete</a>
                    <?php } ?>
                    <?php  if( check_permission( $this->session->userdata('user_type'), 'edit_category') ){ ?>
                      <a href="javascript:void(0);" class="btn btn-warning" onclick="removeCateFromSection( $(this) );" data-code="<?php echo $list['code']; ?>" data-sec-code="<?php echo $sec_detail[DEFAULT_LANG]['code']; ?>">Remove</a>
                    <?php } ?>
                    <a href="<?php echo base_url().'admin/section/listing/lists/'.$sec_detail[DEFAULT_LANG]['code'].'/'.$list['code']; ?>" class="btn btn-success">Go to Lists</a>
                  </td>
                </tr>
              <?php $i++; } ?>
            </tbody>
          </table>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
