<?php
  if( sizeof( $detail ) > 0 ){
    $code = $detail[DEFAULT_LANG]['code'];
    $column_id = $detail[DEFAULT_LANG]['column_id'];
    $page = $detail[DEFAULT_LANG]['page'];

    $section_name = $detail[DEFAULT_LANG]['section_name'];
    $section_type = $detail[DEFAULT_LANG]['section_type'];
    $content_layout = $detail[DEFAULT_LANG]['content_layout'];
    $m_content_layout = $detail[DEFAULT_LANG]['m_content_layout'];
    $design_type = $detail[DEFAULT_LANG]['design_type'];
    $design_template = $detail[DEFAULT_LANG]['design_template'];
    $column_per_view = $detail[DEFAULT_LANG]['column_per_view'];
    $display_per_column = $detail[DEFAULT_LANG]['display_per_column'];
    $thumb = $detail[DEFAULT_LANG]['thumb'];
    $video_type = $detail[ DEFAULT_LANG ]['video_type'];
	$sec_key = $detail[DEFAULT_LANG]['sec_key'];
    $acc_type = $detail[DEFAULT_LANG]['acc_type'];
   $acc_switch = $detail[DEFAULT_LANG]['acc_switch'];
    $link_type = $detail[DEFAULT_LANG]['link_type'];
    $page_internal_code = $detail[DEFAULT_LANG]['page_internal_code'];
    $link_out = $detail[DEFAULT_LANG]['link_out'];
    $btn_text = $detail[DEFAULT_LANG]['btn_text'];
    
    $bg1_desktop = $detail[DEFAULT_LANG]['bg1_desktop'];
    $bg1_mobile = $detail[DEFAULT_LANG]['bg1_mobile'];
    $pd_desktop = $detail[DEFAULT_LANG]['pd_desktop'];
    $pd_mobile = $detail[DEFAULT_LANG]['pd_mobile'];
    $mg_desktop = $detail[DEFAULT_LANG]['mg_desktop'];
    $mg_mobile = $detail[DEFAULT_LANG]['mg_mobile'];
    $bg_size = $detail[DEFAULT_LANG]['bg_size'];

    $list_limit = $detail[DEFAULT_LANG]['list_limit'];

    $img1_desktop = $detail[DEFAULT_LANG]['img1_desktop'];
	$img1_mobile = $detail[DEFAULT_LANG]['img1_mobile'];

    $video_thumb = $detail[DEFAULT_LANG]['video_thumb'];
    $video_thumb_m = $detail[DEFAULT_LANG]['video_thumb_m'];
    $video_id = $detail[DEFAULT_LANG]['video_id'];

    $tab_show = $detail[DEFAULT_LANG]['tab_show'];
    $tab_setting = $detail[DEFAULT_LANG]['tab_setting'];
    $hl_set = $detail[DEFAULT_LANG]['hl_set'];
    $btn_more = $detail[DEFAULT_LANG]['btn_more'];

	$onoff = $detail[DEFAULT_LANG]['onoff'];
	
    $custom_class = $detail[DEFAULT_LANG]['custom_class'];
    $ele_img1 = $detail[DEFAULT_LANG]['ele_img1'];
    $ele_img2 = $detail[DEFAULT_LANG]['ele_img2'];
    $ele_img3 = $detail[DEFAULT_LANG]['ele_img3'];
    $ele_img4 = $detail[DEFAULT_LANG]['ele_img4'];

    $video_type = $detail[DEFAULT_LANG]['video_type'];
    $video_file = $detail[DEFAULT_LANG]['video_file'];

    $section_layout = $detail[DEFAULT_LANG]['section_layout'];
    $sec_title_alignment = $detail[DEFAULT_LANG]['sec_title_alignment'];
    $sec_content_position_ver = $detail[DEFAULT_LANG]['sec_content_position_ver'];
    $sec_content_position_hoz = $detail[DEFAULT_LANG]['sec_content_position_hoz'];
    $bg_color = $detail[DEFAULT_LANG]['bg_color'];
    $bg_position_hoz = $detail[DEFAULT_LANG]['bg_position_hoz'];
    $bg_position_ver = $detail[DEFAULT_LANG]['bg_position_ver'];
    $bg_repeat = $detail[DEFAULT_LANG]['bg_repeat'];
    $bg_fixed = $detail[DEFAULT_LANG]['bg_fixed'];

    $gen_height = $detail[DEFAULT_LANG]['gen_height'];
    $widget = $detail[DEFAULT_LANG]['widget'];

    $listing_alignment =  $detail[DEFAULT_LANG]['listing_alignment'];
    $cate_title_alignment =  $detail[DEFAULT_LANG]['cate_title_alignment'];
    $is_tags = $detail[DEFAULT_LANG]['is_tags'];

    $u_date = $detail[DEFAULT_LANG]['u_date'];

    $custom_css = $detail[DEFAULT_LANG]['custom_css'];
    $sort_option = $detail[DEFAULT_LANG]['sort_option'];

    $section_template = $detail[DEFAULT_LANG]['section_template'];

  }else{
   	$code = '';
    $column_id = '';
    $page = $param_page;
	$sec_key = $param_sec_key;
    $video_type = '';
	$section_name = '';
    $section_type = 'content';
    $content_layout = 'verticle';
    $m_content_layout = '';
    $design_type = 'box';
    $design_template = '';
    $column_per_view = 0;
    $display_per_column = 0;
    $pd_desktop = '';
    $pd_mobile = '';
    $mg_desktop = '';
    $mg_mobile = '';
    $sec_title_alignment = '';
    $acc_type = '';
    $acc_switch = '';
    $link_type = '';
    $page_internal_code = '';
    $link_out = '';
    $btn_text = '';
    $thumb = '';

    $bg1_desktop = '';
    $bg1_mobile = '';

    $img1_desktop = '';
	$img1_mobile = '';

    $list_limit = '';
    $video_thumb = '';
    $video_thumb_m= '';
    $video_id = '';
    $tab_show = '';
    $tab_setting = '';
    $hl_set = 'hl_set_none';
    $btn_more = '';

	$onoff = 1;

    $custom_class = '';
    $ele_img1 = '';
    $ele_img2 = '';
    $ele_img3 = '';
    $ele_img4 = '';

    $video_type = 'youtube';
    $video_file = '';
    $section_layout = 'container';

    $bg_position_hoz='center';
    $bg_position_ver='top';
    $bg_repeat='no_repeat';
    $bg_fixed='static';
    $bg_size = 'cover';

    $sec_content_position_ver = '';
    $sec_content_position_hoz = '';
    $bg_color = '';
    
    $gen_height = 0;
    $widget = '';

    $listing_alignment = 'center';
    $is_tags = '0';
    $u_date = '';

    $custom_css = '';
    $sort_option = 'asc';

    $section_template = '';
  }
?>
<div class="content-wrapper">
    <?php echo form_open_multipart('admin/section/saveSectionDetailForm', ' class="mainForm sectionDetailForm"'); ?>
    <input type="hidden" name="code" value="<?php echo $code; ?>">
    <input type="hidden" name="column_id" value="<?php echo $column_id; ?>">
    <input type="hidden" name="page" value="<?php echo $page; ?>">
    <input type="hidden" name="page_code" value="<?php echo $page_code; ?>">
    <input type="hidden" name="time" value="<?php echo $time; ?>">

    <input type="hidden" name="preview" value="<?php echo time(); ?>">
    <input type="hidden" name="is_preview" value="0">
    <!-- <div class="mb-4">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-dark text-white mr-2"><i class="mdi mdi-home"></i></span>Section Form
        </h3>
    </div> -->
    <div class="page-header">
        <h4 class="page-title">Section Form</h4>
        <div class="">
            <?php if($code != ''){ ?>
            <a class="btn btn-gradient-light btn-fw" href="<?php echo base_url().'admin/section/allrow/'.$page_code;  ?>">Back section</a>
            <?php foreach( $pages_names as $pages_name ){ ?>
                <?php // echo $parent['code'] , $page_code ,$parent['parent_code'], $page ?>
                
                <?php if($pages_name['parent_code'] == 0){ ?>
                    <a href="<?php echo base_url().'th/'.$pages_name['page_slug']?>" target="_blank" class="btn btn-gradient-light btn-fw">View page</a>
                <?php }else{ ?>
                    <?php foreach( $parent_pages as $parent ){ ?>
                        <?php if( $pages_name['parent_code'] == $parent['code'] ){ ?>
                            <a href="<?php echo base_url().'th/'.$parent['page_slug'].'/'.$pages_name['page_slug']?>" target="_blank" class="btn btn-gradient-light btn-fw">View page</a>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            
            
            <?php if($section_type == 'listing'){ ?>
            <a href="<?php echo base_url().'admin/section/categories/lists/'.$code; ?> "
                class="btn btn-gradient-dark btn-fw">+ Add category</a>
            <?php }elseif($section_type == 'banner'){ ?>
            <a href="<?php echo base_url().'admin/section/banner/lists/'.$code; ?> "
                class="btn btn-gradient-dark btn-fw">+ Add banner</a>
            <?php } ?>
            <?php } ?>
        </div>
    </div>

    <div class="card mt-4">
        <div class="card-body">
            <h4 class="card-title">Section Detail</h4>
            <div class="row">
                <div class="col col-12 col-md-4">
                    <div class="form-group image-box">
                        <label>Section display</label>
                        <input type="file" data-height="130" name="thumb" class="dropify" data-max-file-size="3M"
                            data-default-file="<?php  echo $thumb != "" ? upload_path($thumb) : '';  ?>" />
                        <input type="hidden" class="image_as_text" name="thumb_txt" value="<?php echo $thumb; ?>" />
                    </div>

                </div>
                <div class="col col-12 col-md-8">
                    <div class="form-group">
                        <label>Section name</label>
                        <input type="text" name="section_name" class="form-control" placeholder="Section Name"
                            value="<?php echo $section_name; ?>">
                    </div>
                    <div class="form-group" style="<?php  echo check_permission( $this->session->userdata('user_type'), 'section_gen_height') ?  'display:block'  :  'display:none'  ?>">
                        <label for="">Section Gen Height</label>
                        <div class="">
                            <ul class="alignment-position" style="z-index:10;">
                                <li class="alignment-position-item">
                                    <input type="radio" id="auto-height" class="form-check-input" name="gen_height" value="0" checked >
                                    <label for="auto-height" class="justify-content-center"> Auto Height </label>
                                </li>
                                <li class="alignment-position-item">
                                    <input type="radio" id="fix-height" class="form-check-input" name="gen_height" value="1" <?php echo $gen_height == 1 ? 'checked' : ''; ?>>
                                    <label for="fix-height" class="justify-content-center"> Fix Height </label>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="card mt-4">
        <div class="card-body">
            <div class="row">
                <div class="col col-12 col-md-5">
                    <h4 class="card-title">Topic</h4>
                    <div
                        class="form-group float-left w-100 mb-0 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
                        <ul class="nav nav-tabs" role="tablist" style="">
                            <?php foreach( $langs as $lang ){ ?>
                            <li class="nav-item">
                                <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                    id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
                                    href="#<?php echo 'title_'.$lang['text']; ?>" role="tab"
                                    aria-controls="<?php echo 'title_'.$lang['text']; ?>"
                                    aria-selected="true"><?php echo $lang['text']; ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content float-left w-100" style="">
                            <?php foreach( $langs as $lang ){ ?>
                            <?php
                            if( isset( $detail[ $lang['text'] ] ) ){
                                $title = $detail[ $lang['text'] ]['title'];
                                $sub_title = $detail[ $lang['text'] ]['sub_title'];
                            }else{
                                $title = '';
                                $sub_title = '';
                            }
                        ?>
                            <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                id="<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
                                aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" name="title[]" class="form-control" placeholder="Title"
                                        value="<?php echo $title; ?>">
                                </div>
                                <div class="form-group mb-0">
                                    <label for="">Sub Title </label>
                                    <input type="text" name="sub_title[]" class="form-control" placeholder="Sub-Title"
                                        value="<?php echo $sub_title; ?>">
                                </div>
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <div class="col col-12 col-md-7">
                    <div class="box-setting">
                        <div class="card_header">
                            <h4 class="card-title">Background</h4>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#bgSetting"><img src="/assets/admin/images/nexitor/icon-settings.svg" class="svg" alt=""></a>
                        </div>
                        <div class="border-box-setting">
                            <div class="row">
                                <div class="col-12 col-md-6 image-box">
                                    <?php /* data-max-height="100" data-max-width="100" */ ?>
                                    <div class="form-group">
                                        <label><small class="text-muted">Background Desktop</small>
                                            <?php /*<small class="text-muted">( 1920px x 1345px )</small> */ ?></label>
                                        <input type="file" data-height="190" name="bg1_desktop" class="dropify" data-max-file-size="3M" data-default-file="<?php echo $bg1_desktop != "" ? upload_path($bg1_desktop) : ''; ?>" />
                                        <input type="hidden" class="image_as_text" name="bg1_desktop_txt"
                                            value="<?php echo $bg1_desktop; ?>" />
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 image-box">
                                    <div class="form-group">
                                        <label><small class="text-muted">Background Mobile</small>
                                            <?php /*<small class="text-muted">( 760px x 892px )</small> */ ?></label>
                                        <input type="file" data-height="190" name="bg1_mobile" class="dropify"
                                             data-max-file-size="3M"
                                            data-default-file="<?php echo $bg1_mobile != "" ? upload_path($bg1_mobile) : ''; ?>" />
                                        <input type="hidden" class="image_as_text" name="bg1_mobile_txt"
                                            value="<?php echo $bg1_mobile; ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if($section_type == 'listing' || $section_type == 'banner'  ){?>
    <div class="card mt-4" style="<?php  echo check_permission( $this->session->userdata('user_type'), 'section_type') ?  'display:none'  :  'display:block'  ?>"">
        <div class="card-body">
            <div class="d-flex align-items-center">
            <h4 class="card-title mb-0">Add <?php echo $section_type; ?>  : </h4>
            <div class="form-group ml-auto">
                <?php if($section_type == 'listing'){ ?>
                <a href="<?php echo base_url().'admin/section/categories/lists/'.$code; ?> "
                    class="btn btn-gradient-dark btn-fw">Go to category</a>
                <?php }elseif($section_type == 'banner'){ ?>
                <a href="<?php echo base_url().'admin/section/banner/lists/'.$code; ?> "
                    class="btn btn-gradient-dark btn-fw">Go to banner</a>
                <?php } ?>
            </div>
            </div>
        </div>
        
    </div>
    <?php } ?>
    <div class="card mt-4" style="<?php  echo check_permission( $this->session->userdata('user_type'), 'section_type') ?  'display:block'  :  'display:none'  ?>"">
        <div class="card-body">
            <h4 class="card-title">Section type</h4>
            <div class="form-group mb-0">

                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="box-radio box-check-gray" onclick="" data-cate-for="">
                            <div class="form-check form-check-dark">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="section_type" value="content"
                                        onclick="$('.content').show(); $('.layer_content').show(); $('.content-check').addClass('col-md-8'); $('.content-check').removeClass('col-md-12'); $('.content-hero-image').show(); $('.content-setting').show();  $('.listing-setting').hide();  $('.video').hide();$('.banner').hide(); $('.listing').hide(); initToggle(); $('.listing_toggle').bootstrapToggle('off'); $('.widget').hide();"
                                        checked>
                                    Content
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="box-radio box-check-gray" onclick="" data-cate-for="">
                            <div class="form-check form-check-dark">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="section_type" value="listing"
                                        onclick="$('.content').show(); $('.layer_content').show(); $('.content-setting').hide();$('.content-hero-image').hide();$('.content-check').addClass('col-md-12');$('.content-check').removeClass('col-md-8');  $('.listing-setting').show(); $('.video').hide();$('.listing').show(); $('.banner').hide();initToggle(); $('.listing_toggle').bootstrapToggle('on'); $('.widget').hide();"
                                        <?php echo $section_type == 'listing' ? 'checked' : ''; ?>>
                                    Listing
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="box-radio box-check-gray" onclick="" data-cate-for="">
                            <div class="form-check form-check-dark">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="section_type" value="video"
                                        onclick="$('.video').show();$('.layer_content').show(); $('.content-setting').hide(); $('.listing-setting').hide(); $('.content-hero-image').hide();  $('.content').hide();$('.listing').hide(); $('.banner').hide(); initToggle(); $('.listing_toggle').bootstrapToggle('off'); $('.widget').hide();"
                                        <?php echo $section_type == 'video' ? 'checked' : ''; ?>>
                                    Video
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="box-radio box-check-gray" onclick="" data-cate-for="">
                            <div class="form-check form-check-dark">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="section_type" value="banner"
                                        onclick="$('.layer_content').hide(); $('.video').hide(); $('.content-setting').hide();  $('.listing-setting').hide();$('.content-hero-image').hide();  $('.content').hide();$('.listing').hide(); $('.banner').hide(); initToggle(); $('.listing_toggle').bootstrapToggle('on'); $('.widget').hide();"
                                        <?php echo $section_type == 'banner' ? 'checked' : ''; ?>>
                                    Banner
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="box-radio box-check-gray" onclick="" data-cate-for="">
                            <div class="form-check form-check-dark">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="section_type" value="widget"
                                        onclick="$('.layer_content').hide(); $('.video').hide(); $('.content-setting').hide(); $('.listing-setting').hide();   $('.content').hide();$('.listing').hide(); $('.banner').hide(); initToggle(); $('.listing_toggle').bootstrapToggle('off'); $('.widget').show();"
                                        <?php echo $section_type == 'widget' ? 'checked' : ''; ?>>
                                    Widget
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card mt-4 widget" style="display:<?php echo $section_type == "widget" ? 'block' : 'none'; ?>">
        <div class="card-body">
            <div class="form-group">
                <div class="row d-block d-md-flex align-items-center">
                    <div class="col col-12 col-md-2"><label>Select Widget</label></div>
                    <div class="col col-12 col-md-10">
                        <select name="widget" class="form-control">
                            <option value="">Please select</option>
                            <option value="smepack_highlight" <?php echo $widget == "smepack_highlight" ? 'selected' : ''; ?>>Smepack Highlight</option>
                            <option value="smepack_cate_menu" <?php echo $widget == "smepack_cate_menu" ? 'selected' : ''; ?>>Smepack Menu</option>
                            <option value="smepack_cate" <?php echo $widget == "smepack_cate" ? 'selected' : ''; ?>>Smepack Cate</option>
                            
                            <option value="solution_auto" <?php echo $widget == "solution_auto" ? 'selected' : ''; ?>>Solution Auto</option>
                            <option value="solution_slide" <?php echo $widget == "solution_slide" ? 'selected' : ''; ?>>Solution Slide</option>

                            <option value="experience_auto" <?php echo $widget == "experience_auto" ? 'selected' : ''; ?>>Experience Auto</option>
                            <option value="experience_slide" <?php echo $widget == "experience_slide" ? 'selected' : ''; ?>>Experience Slide</option>
                            <option value="experience_logo" <?php echo $widget == "experience_logo" ? 'selected' : ''; ?>>Experience Logo</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
    .card-group-inner {
        border: 1px dotted rgba(0,0,0,0.1);
        width: 100%;
        background-color: #eeeff3;
        padding: 1.25rem 1.25rem;
        border-radius: 5px;
        margin-bottom: 1.5rem;
    }
    </style>
    <!-- TYPE CHECK : CONTENT-->
    <!-- <div class="content" style="display:<?php // echo $section_type == 'content' ? 'block' : 'none';?>;"> -->
        <div class="card mt-4" style="<?php  echo check_permission( $this->session->userdata('user_type'), 'section_display') ?  'display:block'  :  'display:none'  ?>">
            <div class="card-body">
                <div class="box-switch">
                    <h4 class="card-title">Display</h4>
                    <div class="button-switch">
                        <input type="checkbox" data-toggle="toggle" data-onstyle="info" data-on="Show" data-off="Hide"
                            data-offstyle="outline-secondary" data-size="xs" name="box_switch"
                            onchange="sectionToggle($(this))" class="listing_toggle" <?php echo $code != "" && $u_date == "" ? 'checked' : ''; ?>>
                    </div>
                    <div class="content-switch"  style="display:<?php echo $code != "" && $u_date == "" ? 'block' : 'none'; ?>;">
                        <div class="card-group-inner">
                            <h4 class="card-title">Layout Setting</h4>
                            <div class="form-group form-group-choice">
                                <div class="row d-block d-md-flex align-items-center">
                                    <div class="col col-12 col-md-2"><label>Container layout</label></div>
                                    <div class="col col-12 col-md-10">
                                        <div class="">
                                            <ul class="alignment-position" style="z-index:10;">
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="container" class="form-check-input"
                                                        name="section_layout" value="container" checked>
                                                    <label for="container">
                                                        <img class="svg"
                                                            src="/assets/admin/images/element/section/content-container.svg"
                                                            alt="">
                                                        <span>Container</span>
                                                    </label>
                                                </li>
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="fullwidth" class="form-check-input"
                                                        name="section_layout" value="fullwidth"
                                                        <?php echo $section_layout == 'fullwidth' ? 'checked' : ''; ?>>
                                                    <label for="fullwidth">
                                                        <img class="svg"
                                                            src="/assets/admin/images/element/section/content-fluid.svg"
                                                            alt="">
                                                        <span>Full width</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        <div class="content-setting form-group" style="display:<?php echo ( $section_type != 'content' ) ? 'none' : 'block';?>;">
                            <div class="form-group content" style="display:<?php echo $section_type == 'content' ? 'block' : 'none';?>;">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Content layout (Desktop)</label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="content_layout_vertical" name="content_layout" value="vertical"
                                                            checked>
                                                        <label for="content_layout_vertical" onclick="$('.content_layout').hide();">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/content-layout-01.svg"
                                                                alt="">
                                                            <span>Image first</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="con_content_first" name="content_layout"
                                                            value="content_first"
                                                            <?php echo $content_layout == "content_first" ? 'checked' : ''; ?>>
                                                        <label for="con_content_first" onclick="$('.content_layout').hide();">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/content-layout-02.svg"
                                                                alt="">
                                                            <span>Content first</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="center" name="content_layout"
                                                            value="hoz_left"
                                                            <?php echo $content_layout == "hoz_left" ? 'checked' : ''; ?>>
                                                        <label for="center" onclick="$('.content_layout').show();">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/content-layout-03.svg"
                                                                alt="">
                                                            <span>Horizontal left</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="right" name="content_layout"
                                                            value="hoz_right"
                                                            <?php echo $content_layout == "hoz_right" ? 'checked' : ''; ?>>
                                                        <label for="right" onclick="$('.content_layout').show();">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/content-layout-04.svg"
                                                                alt="">
                                                            <span>Horizontal right</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                
                            </div><?php //echo $content_layout == 'content_first' ||  $content_layout == 'vertical'  ? 'none' : 'block';?>
                            <div class="form-group  content_layout" style="display:<?php echo $content_layout == 'content_first' ||  $content_layout == 'vertical' || $content_layout == ''  ? 'none' : 'block';?>">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Content layout (Mobile)</label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="top" name="m_content_layout"
                                                            value="image_first" checked="checked">
                                                        <label for="top">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/content-layout-01.svg"
                                                                alt="">
                                                            <span>Image first</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="bottom" name="m_content_layout"
                                                            value="content_first"
                                                            <?php echo $m_content_layout == "content_first" ? 'checked' : ''; ?>>
                                                        <label for="bottom">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/content-layout-02.svg"
                                                                alt="">
                                                            <span>Content first</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                            <div class="form-group ">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Section Title Alignment</label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="form-group">
                                                <ul class="alignment-position sm-width">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="sec_title_alignment_left" name="sec_title_alignment" value="left" checked >
                                                        <label class="justify-content-center" for="sec_title_alignment_left">Left</label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="sec_title_alignment_center" name="sec_title_alignment" value="center" <?php echo $sec_title_alignment == 'center' ? 'checked' : ''; ?> >
                                                        <label class="justify-content-center" for="sec_title_alignment_center">Center</label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="sec_title_alignment_right" name="sec_title_alignment" value="right" <?php echo $sec_title_alignment == 'right' ? 'checked' : ''; ?>>
                                                        <label class="justify-content-center" for="sec_title_alignment_right">Right</label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                        
                        <div class="listing-setting" style="display:<?php echo $section_type != 'listing' ? 'none' : 'block';?>;">
                            <div class="card-group-inner">
                            <h4 class="card-title">Listing type</h4>
                            <div class="form-group listing" style="display:<?php echo $section_type == 'listing' ? 'block' : 'none';?>;">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Listing Type</label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="box" name="design_type" value="box"
                                                            onclick="$('.d-template').show(); $('.c-perview').show();$('.accordion-type').hide();  $('.design-template').show(); $('.tab-show').show(); $('.hl-content').show(); $('.load-more').show(); $('.dp-group-thumbdesign').show();"
                                                            value="0" checked>
                                                        <label for="box">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-listtype-box.svg"
                                                                alt="">
                                                            <span>Box</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="slides" name="design_type" value="slides"
                                                            onclick="$('.d-template').show(); $('.c-perview').show();$('.accordion-type').hide();  $('.design-template').show(); $('.tab-show').show(); $('.hl-content').show(); $('.load-more').hide(); $('.dp-group-thumbdesign').show();"
                                                            value="0"
                                                            <?php echo $design_type == 'slides' ? 'checked' : ''; ?>>
                                                        <label for="slides">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-listtype-slide.svg"
                                                                alt="">
                                                            <span>Slides</span>
                                                        </label>
                                                    </li>
                                                    <!-- <li class="alignment-position-item">
                                                        <input type="radio" id="tabs" name="design_type" value="tabs"
                                                            onclick="$('.d-template').show(); $('.c-perview').show();"
                                                            value="0"
                                                            <?php // echo $design_type == 'tabs' ? 'checked' : ''; ?>>
                                                        <label for="tabs">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/content-layout-09.svg"
                                                                alt="">
                                                            <span>Tabs</span>
                                                        </label>
                                                    </li> -->
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="accordion" name="design_type"
                                                            value="accordion"
                                                            onclick="$('.d-template').hide(); $('.accordion-type').show();  $('.c-perview').hide(); $('.design-template').hide(); $('.tab-show').show(); $('.hl-content').hide(); $('.load-more').hide(); $('.dp-group-thumbdesign').hide();"
                                                            value="0"
                                                            <?php  echo $design_type == 'accordion' ? 'checked' : ''; ?>>
                                                        <label for="accordion">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-listtype-accordion.svg"
                                                                alt="">
                                                            <span>Accordion</span>
                                                        </label>
                                                    </li> 
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="card-group-inner">
                            <h4 class="card-title">Listing Sort</h4>
                            <div class="form-group listing" style="display:<?php echo $section_type == 'listing' ? 'block' : 'none';?>;">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Publish date</label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="asc" name="sort_option"  value="asc" checked>
                                                        <label for="asc">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-asc.svg"
                                                                alt="">
                                                            <span>Less to more</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="desc" name="sort_option" value="desc" <?php echo $sort_option == 'desc' ? 'checked' : ''; ?>>
                                                        <label for="desc">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-desc.svg"
                                                                alt="">
                                                            <span>More to less</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="card-group-inner  accordion-type" style="display:<?php echo $design_type == 'accordion' ? 'block' : 'none';?>;">
                                <h4 class="card-title">Accordion Setting</h4>

                                <div class="form-group listing " >
                                    <div class="form-group-choice">
                                        <div class="row d-block d-md-flex align-items-center">
                                            <div class="col col-12 col-md-2"><label>Accordion Type</label></div>
                                            <div class="col col-12 col-md-10">
                                                <div class="fg-choice-group">
                                                    <ul class="alignment-position">
                                                        <li class="alignment-position-item">
                                                            <input type="radio" id="acc_faq" name="acc_type"
                                                                value="0" checked>
                                                            <label for="acc_faq">
                                                                <img class="svg"
                                                                    src="/assets/admin/images/element/section/i-acc-faq.svg"
                                                                    alt="">
                                                                <span>FAQ</span>
                                                            </label>
                                                        </li>
                                                        <li class="alignment-position-item">
                                                            <input type="radio" id="acc_default" name="acc_type"
                                                                value="1"
                                                                <?php echo $acc_type == '1' ? 'checked' : ''; ?>>
                                                            <label for="acc_default">
                                                                <img class="svg"
                                                                    src="/assets/admin/images/element/section/i-acc-default.svg"
                                                                    alt="">
                                                                <span>Default</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group listing " >
                                    <div class="form-group-choice">
                                        <div class="row d-block d-md-flex align-items-center">
                                            <div class="col col-12 col-md-2"><label>Accordion Switch</label></div>
                                            <div class="col col-12 col-md-10">
                                                <div class="fg-choice-group">
                                                    <ul class="alignment-position">
                                                        <li class="alignment-position-item">
                                                            <input type="radio" id="acc_on" name="acc_switch"
                                                                value="0" checked>
                                                            <label for="acc_on">
                                                                <img class="svg"
                                                                    src="/assets/admin/images/element/section/i-acc-open.svg"
                                                                    alt="">
                                                                <span>First show</span>
                                                            </label>
                                                        </li>
                                                        <li class="alignment-position-item">
                                                            <input type="radio" id="acc_off" name="acc_switch"
                                                                value="1"
                                                                <?php echo $acc_switch == '1' ? 'checked' : ''; ?>>
                                                            <label for="acc_off">
                                                                <img class="svg"
                                                                    src="/assets/admin/images/element/section/i-acc-close.svg"
                                                                    alt="">
                                                                <span>All close</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-group-inner dp-group-thumbdesign" style="display:<?php echo $design_type != 'accordion' ? 'block' : 'none';?>;">
                            <h4 class="card-title">Thumb design</h4>

                            <div class="form-group listing design-template" style="display:<?php echo $design_type != 'accordion' ? 'block' : 'none';?>;">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Thumb Design <?php echo $design_type ?></label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="vertical" name="design_template"
                                                            value="vertical" checked>
                                                        <label for="vertical" onclick="$('.hl-content').show();">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/content-layout-05.svg"
                                                                alt="">
                                                            <span>Vertical</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="horizontal" name="design_template"
                                                            value="horizontal"
                                                            <?php echo $design_template == 'horizontal' ? 'checked' : ''; ?>>
                                                        <label for="horizontal" onclick="$('.hl-content').show();">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/content-layout-06.svg"
                                                                alt="">
                                                            <span>Horizontal</span>
                                                        </label>
                                                    </li>
                                                    <!-- <li class="alignment-position-item">
                                                        <input type="radio" id="text_in_card" name="design_template"
                                                            value="text_in_card"
                                                            <?php //echo $design_template == 'text_in_card' ? 'checked' : ''; ?>>
                                                        <label for="text_in_card" onclick="$('.hl-content').show();">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/content-layout-11.svg"
                                                                alt="">
                                                            <span>Text in card</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="card_table" name="design_template"
                                                            value="table" 
                                                            <?php //echo $design_template == 'table' ? 'checked' : ''; ?>>
                                                        <label for="card_table" onclick="$('.hl-content').hide();">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/content-layout-11.svg"
                                                                alt="">
                                                            <span>Table</span>
                                                        </label>
                                                    </li> -->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group  hl-content" style="display:<?php echo $design_type != 'accordion' && $design_template != 'table' ? 'block' : 'none';?>;">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Highlight Content</label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="hl_set_none" name="hl_set" value="hl_set_none" <?php  echo $hl_set == 'hl_set_none' ? 'checked' : ''; ?>  onclick="$('.hl_set_show').show()">
                                                        <label for="hl_set_none">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-none.svg"
                                                                alt="">
                                                            <span>none</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="hl_set_show" name="hl_set" value="hl_set_show"  <?php  echo $hl_set == 'hl_set_show' ? 'checked' : ''; ?>  onclick="$('.hl_set_show').show()">
                                                        <label for="hl_set_show">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-listtype-highlight.svg"
                                                                alt="">
                                                            <span>Highlight</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="hl_set_show2" name="hl_set" value="hl_set_show2" <?php  echo $hl_set == 'hl_set_show2' ? 'checked' : ''; ?> onclick="$('.hl_set_show').hide()">
                                                        <label for="hl_set_show2">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-listtype-highlight.svg"
                                                                alt="">
                                                            <span>Highlight 2</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group listing hl-content hl_set_show" style="display:<?php echo ($design_type != 'accordion'  && $design_template != 'table') && $hl_set != 'hl_set_show2' ? 'block' : 'none';?>;">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Show Tags</label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="tags_set_none" name="is_tags" value="0"
                                                            checked>
                                                        <label for="tags_set_none">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-none.svg"
                                                                alt="">
                                                            <span>none</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="tags_set_show" name="is_tags" value="1"
                                                            
                                                            <?php  echo $is_tags == '1' ? 'checked' : ''; ?>>
                                                        <label for="tags_set_show">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-thumb-tag.svg"
                                                                alt="">
                                                            <span>Show Tags</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group listing hl_set_show" style="display:<?php echo $section_type != 'listing' || $hl_set == 'hl_set_show2' ? 'none' : 'block';?>;">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Columns per view</label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position sm-width">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="1" name="column_per_view" value="1" checked>
                                                        <label class="justify-content-center" for="1">1 </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="2" name="column_per_view" value="2"
                                                            <?php echo $column_per_view == 2 ? 'checked' : ''; ?>>
                                                        <label class="justify-content-center" for="2">2 </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="3" name="column_per_view" value="3"
                                                            <?php echo $column_per_view == 3 ? 'checked' : ''; ?>>
                                                        <label class="justify-content-center" for="3">3 </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="4" name="column_per_view" value="4"
                                                            <?php echo $column_per_view == 4 ? 'checked' : ''; ?>>
                                                        <label class="justify-content-center" for="4">4 </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="5" name="column_per_view" value="5"
                                                            <?php echo $column_per_view == 5 ? 'checked' : ''; ?>>
                                                        <label class="justify-content-center" for="5">5 </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="6" name="column_per_view" value="6"
                                                            <?php echo $column_per_view == 6 ? 'checked' : ''; ?>>
                                                        <label class="justify-content-center" for="6">6 </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group listing hl_set_show" style="display:<?php echo $section_type != 'listing' || $hl_set == 'hl_set_show2' ? 'none' : 'block';?>;">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Display per column <small>(Mobile)</small></label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position sm-width">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="display_per_column_1" name="display_per_column" value="1" checked>
                                                        <label class="justify-content-center" for="display_per_column_1">1 </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="display_per_column_2" name="display_per_column" value="2"
                                                            <?php echo $display_per_column == 2 ? 'checked' : ''; ?>>
                                                        <label class="justify-content-center" for="display_per_column_2">2 </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group listing hl_set_show" style="display:<?php echo $section_type != 'listing' || $hl_set == 'hl_set_show2' ? 'none' : 'block';?>;">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Listing Card Alignment</label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position sm-width">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="listing_alignment_left" name="listing_alignment" value="left" checked >
                                                        <label class="justify-content-center" for="listing_alignment_left">Left</label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="listing_alignment_center" name="listing_alignment" value="center" <?php echo $listing_alignment == 'center' ? 'checked' : ''; ?> >
                                                        <label class="justify-content-center" for="listing_alignment_center">Center</label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="listing_alignment_right" name="listing_alignment" value="right" <?php echo $listing_alignment == 'right' ? 'checked' : ''; ?>>
                                                        <label class="justify-content-center" for="listing_alignment_right">Right</label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group listing hl_set_show" style="display:<?php echo $section_type != 'listing' || $hl_set == 'hl_set_show2' ? 'none' : 'block';?>;">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Limit content<small></small></label></div>
                                        <div class="col col-12 col-md-5">
                                            <div class="form-group">
                                                <input type="text" name="list_limit" class="form-control" placeholder="Limit"
                                                    value="<?php echo $list_limit; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>


                            <div class="card-group-inner">
                            <h4 class="card-title">Category Setting</h4>

                            <div class="form-group  listing tab-show">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Muti Category</label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="default" name="tab_show" value="default" checked onclick="$('.tab-setting').hide(); $('.title-cate-set').show();">
                                                        <label for="default">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-cate-split.svg"
                                                                alt="">
                                                            <span>Auto category</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="tabs" name="tab_show" value="tabs" <?php echo $tab_show == 'tabs' ? 'checked' : ''; ?>  onclick="$('.tab-setting').show();$('.title-cate-set').show();">
                                                        <label for="tabs">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-cate-tab.svg"
                                                                alt="">
                                                            <span>Tab category</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="merge" name="tab_show" value="merge" <?php echo $tab_show == 'merge' ? 'checked' : ''; ?>  onclick="$('.tab-setting').hide();$('.title-cate-set').hide();">
                                                        <label for="merge">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-cate-merge.svg"
                                                                alt="">
                                                            <span>Merge category</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group  listing tab-setting" style="<?php if($tab_show == 'tabs' ){ echo 'display:block;';}else if($tab_show == ' ' ){ echo 'display:none;';}else if($tab_show != 'tabs' ){ echo 'display:none;';} ?>">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2">
                                            <label  class="all-button-tab">Tab category</label>
                                        </div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="only_has" name="tab_setting" value="only_has"
                                                            value="0"checked>
                                                        <label for="only_has">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-catetab-has.svg"
                                                                alt="">
                                                            <span>Only has cate</span>
                                                        </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="all_content" name="tab_setting" value="all_content"
                                                            value="0"  <?php  echo $tab_setting == 'all_content' ? 'checked' : ''; ?>>
                                                        <label for="all_content">
                                                            <img class="svg"
                                                                src="/assets/admin/images/element/section/i-catetab-all.svg"
                                                                alt="">
                                                            <span  class="all-button-tab">All cate button</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group title-cate-set" style="display:<?php echo $tab_show == 'merge' ? 'none' : 'block';?>;">
                                <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Category Title Alignment</label></div>
                                        <div class="col col-12 col-md-10">
                                            <div class="fg-choice-group">
                                                <ul class="alignment-position sm-width">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="cate_title_alignment_left" name="cate_title_alignment" value="left" checked >
                                                        <label class="justify-content-center" for="cate_title_alignment_left">Left</label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="cate_title_alignment_center" name="cate_title_alignment" value="center" <?php echo $cate_title_alignment == 'center' ? 'checked' : ''; ?> >
                                                        <label class="justify-content-center" for="cate_title_alignment_center">Center</label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="cate_title_alignment_right" name="cate_title_alignment" value="right" <?php echo $cate_title_alignment == 'right' ? 'checked' : ''; ?>>
                                                        <label class="justify-content-center" for="cate_title_alignment_right">Right</label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>

                            <div class="card-group-inner">
                                <h4 class="card-title">Templates</h4>
                                <div class="form-group">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Select Template</label></div>
                                        <div class="col col-12 col-md-10">
                                            <select name="section_template" class="form-control">
                                                <option value="" >Please select</option>
                                                <option value="article" <?php echo $section_template == "article" ? 'selected' : ''; ?>>Article</option>
                                                <option value="solution" <?php echo $section_template == "solution" ? 'selected' : ''; ?>>Solution</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-group-inner">
                                <h4 class="card-title">Button & Link Setting</h4>
                                <div class="form-group listing btn-more">
                                    <div class="form-group-choice">
                                        <div class="row d-block d-md-flex align-items-center">
                                            <div class="col col-12 col-md-2"><label>Button on section</label></div>
                                            <div class="col col-12 col-md-10">
                                                <div class="fg-choice-group">
                                                    <ul class="alignment-position">
                                                        <li class="alignment-position-item">
                                                            <input type="radio" id="btn_more_none" name="btn_more" value="btn_more_none"
                                                                checked>
                                                            <label for="btn_more_none" onclick="$('.viewmore-set').hide();  $('.link-out').hide(); $('.internal-link').hide(); $('.button-text').hide();">
                                                                <img class="svg"
                                                                    src="/assets/admin/images/element/section/i-none.svg"
                                                                    alt="">
                                                                <span>none</span>
                                                            </label>
                                                        </li>
                                                        <li class="alignment-position-item">
                                                            <input type="radio" id="btn_more_show" name="btn_more" value="btn_more_show"
                                                            <?php  echo $btn_more == 'btn_more_show' ? 'checked' : ''; ?>>
                                                            <label for="btn_more_show" onclick="$('.viewmore-set').show();  $('.button-text').show();  if($('.viewmore-set').find('#internal_link').prop( 'checked' )) { $('.internal-link').show();$('.link-out').hide(); } else { $('.internal-link').hide(); $('.link-out').show(); } ">
                                                                <img class="svg"
                                                                    src="/assets/admin/images/element/section/i-button-link.svg"
                                                                    alt="">
                                                                <span>Button link</span>
                                                            </label>
                                                        </li>
                                                        <li class="alignment-position-item load-more" style="display:<?php if( $design_type == 'box' ){ echo 'block';}else if($design_type == ''){ echo 'block';}else if($design_type != 'box'){ echo 'none';}  ?>">
                                                            <input type="radio" id="btn_load_more" name="btn_more" value="btn_load_more"
                                                                
                                                                <?php  echo $btn_more == 'btn_load_more' ? 'checked' : ''; ?>>
                                                            <label for="btn_load_more" onclick="$('.viewmore-set').hide();  $('.link-out').hide(); $('.internal-link').hide(); $('.button-text').hide() ">
                                                                <img class="svg"
                                                                    src="/assets/admin/images/element/section/i-button-load.svg"
                                                                    alt="">
                                                                <span>Load more</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                                

                            

                                <div class="form-group  mb-3 viewmore-set" style="display:<?php echo $btn_more != 'btn_more_show' ? 'none' : 'block';?>;" >
                                    <div class="form-group-choice">
                                        <div class="row d-block d-md-flex align-items-center">
                                            <div class="col col-12 col-md-2"><label>Link Type</label></div>
                                            <div class="col col-12 col-md-10">
                                                <div class="fg-choice-group">
                                                    <ul class="alignment-position">
                                                        <!-- <li class="alignment-position-item">
                                                            <input type="radio" id="link_type_no" name="link_type" value="no" checked>
                                                            <label for="link_type_no" onclick=" $('.link-out').hide(); $('.internal-link').hide(); $('.button-text').hide();">
                                                                <span>No Link</span>
                                                            </label>
                                                        </li> -->
                                                        <li class="alignment-position-item">
                                                            <input type="radio" id="external_link" name="link_type" value="external_link"  checked>
                                                            <label for="external_link" onclick="$('.link-out').show(); $('.internal-link').hide(); $('.button-text').show();">
                                                                <!-- <img class="svg" src="/assets/admin/images/element/section/content-layout-06.svg" alt=""> -->
                                                                <img class="svg"
                                                                    src="/assets/admin/images/element/section/i-link-external.svg"
                                                                    alt="">
                                                                <span>External link</span>
                                                            </label>
                                                        </li>
                                                        <li class="alignment-position-item">
                                                            <input type="radio" id="internal_link" name="link_type" value="internal_link"
                                                                <?php echo $link_type == 'internal_link' ? 'checked' : ''; ?>>
                                                            <label for="internal_link" onclick=" $('.internal-link').show(); $('.link-out').hide(); $('.button-text').show();">
                                                                <!-- <img class="svg" src="/assets/admin/images/element/section/content-layout-06.svg" alt=""> -->
                                                                <img class="svg"
                                                                    src="/assets/admin/images/element/section/i-link-internal.svg"
                                                                    alt="">
                                                                <span>Internal link</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="form-group link-out" style="display:<?php if( $link_type == 'external_link' && $btn_more == 'btn_more_show' ){ echo 'block';}else if( $link_type == 'external_link' && $btn_more != 'btn_more_show' ){ echo 'none';}else if($link_type == ''){ echo 'none';}else if($link_type != 'external_link' ){ echo 'none';}  ?>"  >
                                    <div class="form-group-choice">
                                        <div class="row d-block d-md-flex align-items-center">
                                            <div class="col col-12 col-md-2"><label>Url Link<small></small></label></div>
                                            <div class="col col-12 col-md-5">
                                                <div class="form-group">
                                                    <input type="text" name="link_out" class="form-control" placeholder="link out"
                                                        value="<?php echo $link_out; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group internal-link " style="display:<?php echo $btn_more == 'btn_load_more' ||  $btn_more == ''  ? "none" : "block"  ?>;">
                                    <div class="form-group-choice">
                                    <div class="row d-block d-md-flex align-items-center">
                                        <div class="col col-12 col-md-2"><label>Internal Link<small></small></label></div>
                                        <div class="col col-12 col-md-5">
                                            <select class="form-control" name="page_internal_code">
                                                <option value="">Select the page</option>
                                                <?php foreach(  $pagesList as $page_menu ){ ?>
                                                    <option value="<?php echo $page_menu['code']; ?>" <?php echo ( $code != '' and $page_menu['code'] == $page_internal_code )?'selected':''; ?>><?php  echo $page_menu['page_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group button-text w-100  <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?> button-text" style="display:<?php echo $btn_more == 'btn_more_none' || $btn_more == 'btn_load_more' || $btn_more == '' ? 'none;' : 'block;' ?>">
                                    <div class="row d-block d-md-flex align-items-start">
                                        <div class="col col-12 col-md-2"><label>Button Text<small></small></label></div>
                                        <div class="col col-12 col-md-10">
                                            <ul class="nav nav-tabs" role="tablist" style="">
                                                <?php foreach( $langs as $lang ){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                                        id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
                                                        href="#<?php echo 'title_'.$lang['text']; ?>" role="tab"
                                                        aria-controls="<?php echo 'title_'.$lang['text']; ?>"
                                                        aria-selected="true"><?php echo $lang['text']; ?></a>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                            <div class="tab-content float-left w-100" style="">
                                                <?php foreach( $langs as $lang ){ ?>
                                                <?php
                                                if( isset( $detail[ $lang['text'] ] ) ){
                                                    $btn_text = $detail[ $lang['text'] ]['btn_text'];
                                                }else{
                                                    $btn_text = '';
                                                }
                                                ?>
                                                <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                                    id="<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
                                                    aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
                                                    <div class="row">
                                                        <div class="col-12 mt-3">
                                                            <div class="form-group">
                                                                <label for="">Button Text</label>
                                                                <input type="text" name="btn_text[]" class="form-control" placeholder="Button Text"
                                                                    value="<?php echo $btn_text; ?>"
                                                                    maxlength="<?php echo $param_sec_key == 'calendar' ? '100' : ''; ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php // if($param_sec_key != "vrpartner"){ ?>

                                                    <?php //} ?>
                                                </div>
                                                <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            
                            
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
        
    <!-- <div class="listing" style="display:<?php //echo $section_type == 'listing' ? 'block' : 'none';?>;">
        <div class="card mt-4">
            <div class="card-body">
                <div class="box-switch">
                    <h4 class="card-title">Display</h4>
                    <div class="button-switch">
                        <input type="checkbox" checked data-toggle="toggle" data-onstyle="info" data-on="Show"
                            data-off="Hide" data-offstyle="outline-secondary" data-size="xs" name="box_switch"
                            onchange="sectionToggle($(this))">
                    </div>
                    <div class="content-switch">
                        <div class="form-group">
                            <div class="form-group-choice">
                                <div class="row d-block d-md-flex align-items-center">
                                    <div class="col col-12 col-md-2"><label>Listing Type</label></div>
                                    <div class="col col-12 col-md-10">
                                        <div class="fg-choice-group">
                                            <ul class="alignment-position">
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="box" name="design_type" value="box"
                                                        onclick="$('.d-template').show(); $('.c-perview').show();"
                                                        value="0" checked>
                                                    <label for="box">
                                                        <img class="svg"
                                                            src="/assets/admin/images/element/section/content-layout-07.svg"
                                                            alt="">
                                                        <span>Box</span>
                                                    </label>
                                                </li>
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="slides" name="design_type" value="slides"
                                                        onclick="$('.d-template').show(); $('.c-perview').show();"
                                                        value="0"
                                                        <?php //echo $design_type == 'slides' ? 'checked' : ''; ?>>
                                                    <label for="slides">
                                                        <img class="svg"
                                                            src="/assets/admin/images/element/section/content-layout-10.svg"
                                                            alt="">
                                                        <span>Slides</span>
                                                    </label>
                                                </li>
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="tabs" name="design_type" value="tabs"
                                                        onclick="$('.d-template').show(); $('.c-perview').show();"
                                                        value="0"
                                                        <?php //echo $design_type == 'tabs' ? 'checked' : ''; ?>>
                                                    <label for="tabs">
                                                        <img class="svg"
                                                            src="/assets/admin/images/element/section/content-layout-09.svg"
                                                            alt="">
                                                        <span>Tabs</span>
                                                    </label>
                                                </li>
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="accordion" name="design_type"
                                                        value="accordion"
                                                        onclick="$('.d-template').hide(); $('.c-perview').hide();"
                                                        value="0"
                                                        <?php //echo $design_type == 'accordion' ? 'checked' : ''; ?>>
                                                    <label for="accordion">
                                                        <img class="svg"
                                                            src="/assets/admin/images/element/section/content-layout-12.svg"
                                                            alt="">
                                                        <span>Accordion</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="form-group-choice">
                                <div class="row d-block d-md-flex align-items-center">
                                    <div class="col col-12 col-md-2"><label>Thumb Design</label></div>
                                    <div class="col col-12 col-md-10">
                                        <div class="fg-choice-group">
                                            <ul class="alignment-position">
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="vertical" name="design_template"
                                                        value="vertical" checked>
                                                    <label for="vertical">
                                                        <img class="svg"
                                                            src="/assets/admin/images/element/section/content-layout-05.svg"
                                                            alt="">
                                                        <span>Vertical</span>
                                                    </label>
                                                </li>
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="horizontal" name="design_template"
                                                        value="horizontal"
                                                        <?php //echo $design_template == 'horizontal' ? 'checked' : ''; ?>>
                                                    <label for="horizontal">
                                                        <img class="svg"
                                                            src="/assets/admin/images/element/section/content-layout-06.svg"
                                                            alt="">
                                                        <span>Horizontal</span>
                                                    </label>
                                                </li>
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="text_in_card" name="design_template"
                                                        value="text_in_card"
                                                        <?php //echo $design_template == 'text_in_card' ? 'checked' : ''; ?>>
                                                    <label for="text_in_card">
                                                        <img class="svg"
                                                            src="/assets/admin/images/element/section/content-layout-11.svg"
                                                            alt="">
                                                        <span>Text in card</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group-choice">
                                <div class="row d-block d-md-flex align-items-center">
                                    <div class="col col-12 col-md-2"><label>Columns per view</label></div>
                                    <div class="col col-12 col-md-10">
                                        <div class="fg-choice-group">
                                            <ul class="alignment-position sm-width">
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="1" name="column_per_view" value="1" checked>
                                                    <label class="justify-content-center" for="1">1 </label>
                                                </li>
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="2" name="column_per_view" value="2"
                                                        <?php //echo $column_per_view == 2 ? 'checked' : ''; ?>>
                                                    <label class="justify-content-center" for="2">2 </label>
                                                </li>
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="3" name="column_per_view" value="3"
                                                        <?php //echo $column_per_view == 3 ? 'checked' : ''; ?>>
                                                    <label class="justify-content-center" for="3">3 </label>
                                                </li>
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="4" name="column_per_view" value="4"
                                                        <?php //echo $column_per_view == 4 ? 'checked' : ''; ?>>
                                                    <label class="justify-content-center" for="4">4 </label>
                                                </li>
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="5" name="column_per_view" value="5"
                                                        <?php //echo $column_per_view == 5 ? 'checked' : ''; ?>>
                                                    <label class="justify-content-center" for="5">5 </label>
                                                </li>
                                                <li class="alignment-position-item">
                                                    <input type="radio" id="6" name="column_per_view" value="6"
                                                        <?php //echo $column_per_view == 6 ? 'checked' : ''; ?>>
                                                    <label class="justify-content-center" for="6">6 </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="layer_content" style="display:<?php echo $section_type == 'banner'  ? 'none' : 'block';?>;">
        <div class="card mt-4">
            <div class="card-body">
                <div class="box-switch">
                    <h4 class="card-title">Content</h4>
                    <div class="button-switch">
                        <input type="checkbox" data-toggle="toggle" data-onstyle="info" data-on="Show" data-off="Hide"
                            data-offstyle="outline-secondary" data-size="xs" name="box_switch" checked
                            onchange="sectionToggle($(this))">
                    </div>
                    <div class="content-switch">
                        <div class="content"
                            style="display:<?php echo $section_type == 'content' || $section_type == 'listing' ? 'block' : 'none';?>;">
                            <div class="row">
                                <div class="col col-12 col-md-4 image-box content-hero-image" style="<?php echo $section_type == 'content' ? 'display:block;' : 'display:none;' ?> ">
                                    <div class="form-group">
                                        <label>Image on content</label>
                                        <input type="file" data-height="215" name="img1_desktop" class="dropify"
                                            data-max-file-size="3M"
                                            data-default-file="<?php  echo $img1_desktop != "" ? upload_path($img1_desktop) : '';  ?>" />
                                        <input type="hidden" class="image_as_text" name="img1_desktop_txt"
                                            value="<?php echo $img1_desktop; ?>" />
                                    </div>
                                </div>
                                <div class="col col-12 content-check <?php echo $section_type == 'listing' ? 'col-md-12' : 'col-md-8' ?> ">
                                    <div class="form-group">
                                        <label>Content</label>
                                        <div class=" w-100 <?php echo sizeof( $langs ) == 0 ? 'only-one-lang' : ''; ?>">
                                            <ul class="nav nav-tabs" role="tablist" style="">
                                                <?php foreach( $langs as $lang ){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                                        id="<?php echo $lang['text']; ?>-tab" data-toggle="tab"
                                                        href="#<?php echo $lang['text']; ?>" role="tab"
                                                        aria-controls="<?php echo $lang['text']; ?>"
                                                        aria-selected="true"><?php echo $lang['text']; ?></a>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                            <div class="tab-content float-left w-100" style="">
                                                <?php foreach( $langs as $lang ){ ?>
                                                <?php
                                            if( isset( $detail[ $lang['text'] ] ) ){
                                                $content_editor = $detail[ $lang['text'] ]['content_editor'];
                                            }else{
                                                $content_editor = '';
                                            }
                                            ?>
                                                <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                                    id="<?php echo $lang['text']; ?>" role="tabpanel"
                                                    aria-labelledby="<?php echo $lang['text']; ?>-tab">

                                                    <div class="form-group">
                                                        <div class="install-nexitor float-left" data-area="content">
                                                            <?php echo $content_editor; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <div class="float-left d-flex align-items-center mt-4 ">
                                                    <label class="mr-4" for="content_position ">Content position</label>
                                                    <ul class="alignment-menu mx-2" style="z-index:10;">
                                                        <li class="alignment-menu-item">
                                                            <input type="radio" id="sec_content_position_hoz_left" class="form-check-input"
                                                                name="sec_content_position_hoz" value="left" checked>
                                                            <label for="sec_content_position_hoz_left"
                                                                class="justify-content-center">Left</label>
                                                        </li>
                                                        <li class="alignment-menu-item">
                                                            <input type="radio" id="sec_content_position_hoz_center" class="form-check-input"
                                                                name="sec_content_position_hoz" value="center" <?php echo $sec_content_position_hoz == 'center' ? 'checked' : ''; ?>>
                                                            <label for="sec_content_position_hoz_center"
                                                                class="justify-content-center">Center</label>
                                                        </li>
                                                        <li class="alignment-menu-item">
                                                            <input type="radio" id="sec_content_position_hoz_right" class="form-check-input"
                                                                name="sec_content_position_hoz" value="right" <?php echo $sec_content_position_hoz == 'right' ? 'checked' : ''; ?>>
                                                            <label for="sec_content_position_hoz_right"
                                                                class="justify-content-center">Right</label>
                                                        </li>
                                                    </ul>

                                                    <ul class="alignment-menu mx-2" style="z-index:10;">
                                                        <li class="alignment-menu-item">
                                                            <input type="radio" id="sec_content_position_ver_top" class="form-check-input"
                                                                name="sec_content_position_ver" value="top" checked>
                                                            <label for="sec_content_position_ver_top" class="justify-content-center">Top</label>
                                                        </li>
                                                        <li class="alignment-menu-item">
                                                            <input type="radio" id="sec_content_position_ver_middle" class="form-check-input"
                                                                name="sec_content_position_ver" value="middle" <?php echo $sec_content_position_ver == 'middle' ? 'checked' : ''; ?>>
                                                            <label for="sec_content_position_ver_middle"
                                                                class="justify-content-center">Middle</label>
                                                        </li>
                                                        <li class="alignment-menu-item">
                                                            <input type="radio" id="sec_content_position_ver_bottom" class="form-check-input"
                                                                name="sec_content_position_ver" value="bottom" <?php echo $sec_content_position_ver == 'bottom' ? 'checked' : ''; ?>>
                                                            <label for="sec_content_position_ver_bottom"
                                                                class="justify-content-center">Bottom</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>



                        <div class="video mb-0"
                            style="display:<?php echo  $section_type == 'video' ? 'block' : 'none';?>;">
                            <div class="">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">Video Type</label>
                                            <div class="">
                                                <ul class="alignment-position" style="z-index:10;">
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="youtube" class="form-check-input"
                                                            name="video_type"
                                                            onclick="$('.youtube').show(); $('.loop').hide(); "
                                                            value="youtube" checked
                                                            <?php //echo $video_type == 'youtube' ? 'checked' : ''; ?>>
                                                        <label for="youtube" class="justify-content-center"> Video
                                                            youtube </label>
                                                    </li>
                                                    <li class="alignment-position-item">
                                                        <input type="radio" id="loop" class="form-check-input"
                                                            name="video_type"
                                                            onclick="$('.loop').show(); $('.youtube').hide(); " value="loop"
                                                            <?php echo $video_type == 'loop' ? 'checked' : ''; ?>>
                                                        <label for="loop" class="justify-content-center"> Video
                                                            loop </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 image-box mt-4">
                                        <?php /* data-max-height="100" data-max-width="100" */ ?>
                                        <div class="form-group">
                                            <!-- <label>Thumb Video Desktop <small class="text-muted">( 1680px x 720px)</small></label> -->
                                            <label>Thumb Video Desktop <i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Thumb Video Desktop : 1680 x 720 px" data-original-title="Image recommended"></i><!-- <small class="text-muted">( 609px x 457px )</small> --></label>

                                            <input type="file" data-height="250" name="video_thumb" class="dropify"
                                                data-max-width="1680" data-max-height="720" data-max-file-size="3M"
                                                data-default-file="<?php echo $video_thumb != "" ? upload_path($video_thumb) : ''; ?>" />
                                            <input type="hidden" class="image_as_text" name="video_thumb_txt"
                                                value="<?php echo $video_thumb; ?>" />
                                        </div>

                                    </div>
                                    <div class="col-6 image-box mt-4">
                                        <?php /* data-max-height="100" data-max-width="100" */ ?>
                                        <div class="form-group">
                                            <!-- <label>Thumb Video Mobile <small class="text-muted"></small></label> -->
                                            <label>Thumb Video Mobile <i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Thumb Video Desktop : 760 x 1060 px" data-original-title="Image recommended"></i><!-- <small class="text-muted">( 609px x 457px )</small> --></label>
                                            
                                            <input type="file" data-height="250" name="video_thumb_m" class="dropify"
                                                data-max-file-size="3M"
                                                data-default-file="<?php echo $video_thumb_m != "" ? upload_path($video_thumb_m) : ''; ?>" />
                                            <input type="hidden" class="image_as_text" name="video_thumb_m_txt"
                                                value="<?php echo $video_thumb_m; ?>" />
                                        </div>

                                    </div>
                                    <div class="col-12 mt-4 youtube"
                                        style="display:<?php echo  $video_type == 'youtube' ? 'block' : 'none';?>;">
                                        <div class="form-group">
                                            <label for="">Video ID <small class="text-muted">( https://www.youtube.com/watch?v=<span style="color:red">tjZTVimUnd4</span> )</small></label>
                                            <input type="text" name="video_id" class="form-control"
                                                placeholder="Video ID" value="<?php echo $video_id; ?>">
                                        </div>
                                    </div>
                                    <div class="col-6 loop mt-4"
                                        style="display:<?php echo  $video_type == 'loop' ? 'block' : 'none';?>;">
                                        <div class="form-group">
                                            <label for="">Video MP4</label>
                                            <input type="file" data-height="250" name="video_file" class="dropify"
                                                data-max-file-size="30000kb"
                                                data-default-file="<?php echo $video_file != "" ? upload_path($video_file) : ''; ?>" />
                                            <input type="hidden" class="image_as_text" name="video_file_txt"
                                                value="<?php echo $video_file; ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
    
    <?php $ar_pos = array('top','right','bottom','left'); ?>
    <div class="card  mt-4" style="<?php  echo check_permission( $this->session->userdata('user_type'), 'section_advance') ?  'display:block'  :  'display:none'  ?>">
        <div class="card-body">
            <div class="box-switch">
                <h4 class="card-title">Advance Custom</h4>
                <div class="button-switch">
                    <input type="checkbox" data-toggle="toggle" data-onstyle="info" data-on="Show" data-off="Hide"
                        data-offstyle="outline-secondary" data-size="xs" name="box_switch"
                        onchange="sectionToggle($(this))">
                </div>
                <div class="content-switch" style="display:none;">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Padding Desktop</label>
                                    <div class="row">
                                        <?php  $ar_set = ar_decode($pd_desktop);?>
                                        <?php foreach( $ar_set as $key => $set ){ ?>
                                            <div class="col-3">
                                                <input type="text" name="pd_desktop[]" placeholder="<?php echo $ar_pos[$key]; ?>" class="form-control" value="<?php echo $set; ?>">
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Margin Desktop</label>
                                    <div class="row">
                                        <?php  $ar_set = ar_decode($mg_desktop);?>
                                        <?php foreach( $ar_set as $key => $set  ){ ?>
                                            <div class="col-3">
                                                <input type="text" name="mg_desktop[]"  placeholder="<?php echo $ar_pos[$key]; ?>" class="form-control" value="<?php echo $set; ?>">
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Padding Mobile</label>
                                    <div class="row">
                                        
                                        <?php  $ar_set = ar_decode($pd_mobile);?>
                                        <?php foreach( $ar_set as $key => $set ){ ?>
                                            <div class="col-3">
                                                <input type="text" name="pd_mobile[]"  placeholder="<?php echo $ar_pos[$key]; ?>" class="form-control" value="<?php echo $set; ?>">
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Margin Mobile</label>
                                    <div class="row">
                                        <?php  $ar_set = ar_decode($mg_mobile);?>
                                        <?php foreach( $ar_set as $key => $set  ){ ?>
                                            <div class="col-3">
                                                <input type="text" name="mg_mobile[]"  placeholder="<?php echo $ar_pos[$key]; ?>" class="form-control" value="<?php echo $set; ?>">
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>Custom Class</label>
                        <input type="text" name="custom_class" class="form-control"  placeholder="Custom css class"
                            value="<?php echo $custom_class; ?>">
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="form-group image-box">
                                <label>Element image 1</label>
                                <input type="file" data-height="150" name="ele_img1" class="dropify"
                                    data-max-file-size="3M"
                                    data-default-file="<?php  echo $ele_img1 != "" ? upload_path($ele_img1) : '';  ?>" />
                                <input type="hidden" class="image_as_text" name="ele_img1_txt"
                                    value="<?php echo $ele_img1; ?>" />
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group image-box">
                                <label>Element image 2</label>
                                <input type="file" data-height="150" name="ele_img2" class="dropify"
                                    data-max-file-size="3M"
                                    data-default-file="<?php  echo $ele_img2 != "" ? upload_path($ele_img2) : '';  ?>" />
                                <input type="hidden" class="image_as_text" name="ele_img2_txt"
                                    value="<?php echo $ele_img2; ?>" />
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group image-box">
                                <label>Element image 3</label>
                                <input type="file" data-height="150" name="ele_img3" class="dropify"
                                    data-max-file-size="3M"
                                    data-default-file="<?php  echo $ele_img3 != "" ? upload_path($ele_img3) : '';  ?>" />
                                <input type="hidden" class="image_as_text" name="ele_img3_txt"
                                    value="<?php echo $ele_img3; ?>" />
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group image-box">
                                <label>Element image 4</label>
                                <input type="file" data-height="150" name="ele_img4" class="dropify"
                                    data-max-file-size="3M"
                                    data-default-file="<?php  echo $ele_img4 != "" ? upload_path($ele_img4) : '';  ?>" />
                                <input type="hidden" class="image_as_text" name="ele_img4_txt"
                                    value="<?php echo $ele_img4; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-4">
                        <label>Custom CSS</label>
                        <input type="hidden" name="custom_css" value="<?php echo $custom_css; ?>" />
                        <textarea class="form-control" name="" class="ace-editor" id="ace_css" rows=15><?php echo $custom_css; ?></textarea>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="card mt-3">
        <div class="card-body">
            <div class="form-group">
                <label for="">Status</label>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="1"
                            <?php  echo $onoff == 1 ? 'checked' : '';  ?>> Publish </label>
                </div>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="0"
                            <?php echo $onoff == 0 ? 'checked' : '';  ?>> Unpublish </label>
                </div>
            </div>

            <div class="row">
                <div class="col-12 text-right">
                    <div class="text-center card-button">
                        <button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
                        <button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 section-previewForm">Preview Changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade bgSetting" id="bgSetting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Background setting</h5>
                    <button type="button" class="close nexitorPopup-close" data-dismiss="modal" aria-label="Close">
                        <img src="/assets/admin/images/nexitor/icon-cancel.svg" class="svg">
                    </button>
    	            <!-- <div class="nexitorPopup-close p-2"><img src="/assets/admin/images/nexitor/icon-cancel.svg" class="svg"></div> -->

                </div>
                <div class="modal-body bgSetting-body">
                        <div class="form-group">
                            <div class="row d-block d-md-flex align-items-center">
                                <div class="col col-12 col-md-4"><label>Background Color</label></div>
                                <div class="col col-12 col-md-4">
                                    <input type='text' name="bg_color" class="color-picker form-control" data-mode="complex" value="<?php echo $bg_color ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row d-block d-md-flex align-items-center">
                                <div class="col col-12 col-md-4"><label>Background position : </label></div>
                                <div class="col col-12 col-md-8">
                                    <div class="fg-choice-group">
                                        <ul class="alignment-position fit-width">
                                            <li class="alignment-position-item">
                                                <input type="radio" id="bg_position_hoz_left" name="bg_position_hoz" value="left" 
                                                <?php echo $bg_position_hoz == 'left' ? 'checked' : ''; ?>>
                                                <label class="justify-content-center" for="bg_position_hoz_left">Left</label>
                                            </li>
                                            <li class="alignment-position-item">
                                                <input type="radio" id="bg_position_hoz_center" name="bg_position_hoz" value="center" checked >
                                                <label class="justify-content-center" for="bg_position_hoz_center">Center</label>
                                            </li>
                                            <li class="alignment-position-item">
                                                <input type="radio" id="bg_position_hoz_right" name="bg_position_hoz" value="right"
                                                    <?php echo $bg_position_hoz == 'right' ? 'checked' : ''; ?>>
                                                <label class="justify-content-center" for="bg_position_hoz_right">Right</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <div class="row d-block d-md-flex align-items-center ">
                                <div class="col col-12 col-md-4"><label></label></div>
                                <div class="col col-12 col-md-8">
                                    <div class="fg-choice-group">
                                        <ul class="alignment-position fit-width">
                                            <li class="alignment-position-item">
                                                <input type="radio" id="bg_position_ver_top" name="bg_position_ver" value="top" checked>
                                                <label class="justify-content-center" for="bg_position_ver_top">Top</label>
                                            </li>
                                            <li class="alignment-position-item">
                                                <input type="radio" id="bg_position_ver_middle" name="bg_position_ver" value="middle"
                                                    <?php echo $bg_position_ver == 'middle' ? 'checked' : ''; ?>>
                                                <label class="justify-content-center" for="bg_position_ver_middle">Middle </label>
                                            </li>
                                            <li class="alignment-position-item">
                                                <input type="radio" id="bg_position_ver_bottom" name="bg_position_ver" value="bottom"
                                                    <?php echo $bg_position_ver == 'bottom' ? 'checked' : ''; ?>>
                                                <label class="justify-content-center" for="bg_position_ver_bottom">Bottom</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row d-block d-md-flex align-items-center">
                                <div class="col col-12 col-md-4"><label>Background repeat : </label></div>
                                <div class="col col-12 col-md-8">
                                    <div class="fg-choice-group">
                                        <ul class="alignment-position fit-width">
                                            <li class="alignment-position-item">
                                                <input type="radio" id="no_repeat" name="bg_repeat" value="no_repeat" checked>
                                                <label class="justify-content-center" for="no_repeat">No-repeat</label>
                                            </li>
                                            <li class="alignment-position-item">
                                                <input type="radio" id="repeat_all" name="bg_repeat" value="repeat_all"
                                                    <?php echo $bg_repeat == 'repeat_all' ? 'checked' : ''; ?>>
                                                <label class="justify-content-center" for="repeat_all">Repeat all </label>
                                            </li>
                                            <li class="alignment-position-item">
                                                <input type="radio" id="repeat_x" name="bg_repeat" value="repeat_x"
                                                    <?php echo $bg_repeat == 'repeat_x' ? 'checked' : ''; ?>>
                                                <label class="justify-content-center" for="repeat_x">Repeat X </label>
                                            </li>
                                            <li class="alignment-position-item">
                                                <input type="radio" id="repeat_y" name="bg_repeat" value="repeat_y"
                                                    <?php echo $bg_repeat == 'repeat_y' ? 'checked' : ''; ?>>
                                                <label class="justify-content-center" for="repeat_y">Repeat Y </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row d-block d-md-flex align-items-center">
                                <div class="col col-12 col-md-4"><label>Background fixed : </label></div>
                                <div class="col col-12 col-md-8">
                                    <div class="fg-choice-group">
                                        <ul class="alignment-position fit-width">
                                            <li class="alignment-position-item">
                                                <input type="radio" id="static" name="bg_fixed" value="static" checked>
                                                <label class="justify-content-center" for="static">Static</label>
                                            </li>
                                            <li class="alignment-position-item">
                                                <input type="radio" id="fixed" name="bg_fixed" value="fixed"
                                                    <?php echo $bg_fixed == 'fixed' ? 'checked' : ''; ?>>
                                                <label class="justify-content-center" for="fixed">Fixed</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row d-block d-md-flex align-items-center">
                                <div class="col col-12 col-md-4"><label>Background size : </label></div>
                                <div class="col col-12 col-md-8">
                                    <div class="fg-choice-group">
                                        <ul class="alignment-position fit-width">
                                            <li class="alignment-position-item">
                                                <input type="radio" id="cover" name="bg_size" value="cover" checked>
                                                <label class="justify-content-center" for="cover">Cover</label>
                                            </li>
                                            <li class="alignment-position-item">
                                                <input type="radio" id="contain" name="bg_size" value="contain"
                                                    <?php echo $bg_size == 'contain' ? 'checked' : ''; ?>>
                                                <label class="justify-content-center" for="contain">Contain</label>
                                            </li>
                                            <li class="alignment-position-item">
                                                <input type="radio" id="auto" name="bg_size" value="auto"
                                                    <?php echo $bg_size == 'auto' ? 'checked' : ''; ?>>
                                                <label class="justify-content-center" for="auto">Auto</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-gradient-dark btn-fw" data-dismiss="modal">Done</button>
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>

  

    <?php echo form_close(); ?>
</div>
<?php include_once("inc/nexitor-modal.php"); ?>