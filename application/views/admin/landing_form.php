<?php
	if( sizeof( $detail ) > 0 ){
		$code = $detail[DEFAULT_LANG]['code'];
		$banner_image_d = $detail[DEFAULT_LANG]['banner_image_d'];
		$banner_image_m = $detail[DEFAULT_LANG]['banner_image_m'];
		$slug = $detail[DEFAULT_LANG]['slug'];
		$onoff = $detail[DEFAULT_LANG]['onoff'];

        $scripts_header = $detail[DEFAULT_LANG]['scripts_header'];
        $scripts_body = $detail[DEFAULT_LANG]['scripts_body'];
        $scripts_footer = $detail[DEFAULT_LANG]['scripts_footer'];

        
	}else{
		$code = '';
		$banner_image_d = '';
		$banner_image_m = '';
		$onoff = 1;
		$slug = '';

        $scripts_header = '';
        $scripts_body = '';
        $scripts_footer = '';
	}

	if( sizeof( $seo ) > 0 ) {
		$share_img = $seo[DEFAULT_LANG]['share_img'];
	} else {
		$share_img = '';
	}
?>
<div class="content-wrapper">
	<div class="page-header">
		<h3 class="page-title"> Landing Form </h3>
		<a href="<?php echo base_url().'admin/landing/lists'; ?>" class="btn btn-gradient-dark btn-fw"> <- Back</a>
	</div>
	<ul class="nav nav-tabs nav-tabs-lang" role="tablist" style="">
		<?php foreach( $langs as $lang ){ ?>
		<li class="nav-item">
			<a class="nav-link head-<?php echo $lang['text']; ?> <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" 
			href="javascript:activaTabNex('<?php echo $lang['text']; ?>');"><?php echo $lang['text']; ?></a>
		</li>
		<?php } ?>
	</ul>
	<?php echo form_open_multipart('admin/landing/saveForm', ' class="mainForm"'); ?>
		<input type="hidden" name="code" value="<?php echo $code ?>" >
		<input type="hidden" name="time" value="<?php echo $time; ?>">
	
	<div class="card mb-3">
        <div class="card-body">
			<div class="row">
				<div class="col-12 col-md-6 image-box">
					<div class="form-group">
						<label>Banner desktop image <small class="text-muted">Size : 1920 x ..... px</small></label>
						<input type="file" data-height="190" name="banner_image_d"
							class="dropify" data-max-file-size="3000kb"
							data-default-file="<?php echo  $banner_image_d != "" ? upload_path($banner_image_d) : ''; ?>" />
						<input type="hidden" class="image_as_text" name="banner_image_d_txt"
							value="<?php echo $banner_image_d; ?>" />
					</div>
				</div>
				<div class="col-12 col-md-6 image-box">
					<div class="form-group">
						<label>Banner mobile image <small class="text-muted">Size : 1920 x ..... px</small></label>
						<input type="file" data-height="190" name="banner_image_m"
							class="dropify" data-max-file-size="3000kb"
							data-default-file="<?php echo  $banner_image_m != "" ? upload_path($banner_image_m) : ''; ?>" />
						<input type="hidden" class="image_as_text" name="banner_image_m_txt"
							value="<?php echo $banner_image_m; ?>" />
					</div>
				</div>
			</div>
        </div>
    </div>

	<div class="card">
        <div class="card-body">
			<div class="row">
				<div class="col-12 col-md-12">
					<div class="form-group">
						<div class="tab-content tab-content-lang">
							<?php foreach( $langs as $lang ){ ?>
								<?php
								if( isset( $detail[ $lang['text'] ] ) ) {
									$title = $detail[ $lang['text'] ]['title'];
								} else {
									$title = '';
								}
								?>
								<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
									<div class="form-group mb-3">
										<label for="">Title </label>
										<input type="text" name="title[]" class="form-control" value="<?php echo $title; ?>">
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
					
				</div>
			</div>
        </div>
    </div>

	<div class="card my-3">
        <div class="card-body">
            <div class="box-switch">
                <h4 class="card-title">Content</h4>
                <div class="button-switch">
                    <input type="checkbox" data-toggle="toggle" data-onstyle="info" data-on="Show" data-off="Hide"
                        data-offstyle="outline-secondary" data-size="xs" name="box_switch" checked
                        onchange="sectionToggle($(this))">
                </div>
                <div class="content-switch">
                    <div class="content">
                        <div class="row">
                            <div class="col col-12  col-md-12">
                                <div class="form-group">
                                    <div class=" w-100 <?php echo sizeof( $langs ) == 0 ? 'only-one-lang' : ''; ?>">
                                        
										<div class="tab-content tab-content-lang">
                                            <?php foreach( $langs as $lang ){ ?>
                                            <?php
                                            if( isset( $detail[ $lang['text'] ] ) ){
                                                $content_editor = $detail[ $lang['text'] ]['content_editor'];
                                            }else{
                                                $content_editor = '';
                                            }
                                            ?>

											<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">

                                                <div class="form-group">
                                                    <div class="install-nexitor float-left" data-area="content">
                                                        <?php echo $content_editor; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="card my-3">
        <div class="card-body">
        <div class="box-switch">
			    <h3 class="card-title"> Insert Scripts setting (GA, Facebook Pixel)</h3>
				<div class="button-switch seo-switch">
                    <input type="checkbox" data-toggle="toggle" data-onstyle="info" data-on="Show" data-off="Hide"
                        data-offstyle="outline-secondary" data-size="xs" name="box_switch"
                        onchange="sectionToggle($(this))">
				</div>
				<div class="content-switch" style="display:none;">   
                    <div class="form-group">
                        <label class="font-weight-bold">Scripts in Header</label>
                        <textarea name="scripts_header" class="form-control" rows="8" placeholder="These scripts will be printed in the &#x3C;head&#x3E; section."><?php echo $scripts_header; ?></textarea>
                        <p class="pt-2">These scripts will be printed in the &#x3C;head&#x3E; section.</p>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold">Scripts in Body</label>
                        <textarea name="scripts_body" class="form-control" rows="8" placeholder="These scripts will be printed just below the opening &#x3C;body&#x3E; tag."><?php echo $scripts_body; ?></textarea>
                        <p class="pt-2">These scripts will be printed just below the opening &#x3C;body&#x3E; tag.</p>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold">Scripts in Footer</label>
                        <textarea name="scripts_footer" class="form-control" rows="8" placeholder="These scripts will be printed above the closeing  &#x3C;/body&#x3E; tag."><?php echo $scripts_footer; ?></textarea>
                        <p class="pt-2">These scripts will be printed above the closeing  &#x3C;/body&#x3E; tag.</p>
                    </div>
					
					
				</div>
			</div>
            
        </div>
    </div>

	<div class="card mt-3">
		<div class="card-body">
			<div class="box-switch">
			    <h3 class="card-title"> SEO Page setting </h3>
				<div class="button-switch seo-switch">
				<?php
				if( isset( $detail[ $lang['text'] ] ) ){
					if ( sizeof($seo) > 0 ) {
						$seo_code = $seo[ $lang['text'] ]['code'];
					} else {
						$seo_code = '';
					}
					
				}else{
					$seo_code = '';
				}
				?>
					<input type="checkbox" <?php // echo $seo_status == 1 ? 'checked' : '' ?> data-toggle="toggle"
						data-onstyle="info" data-on="Show" data-off="Hide" data-offstyle="outline-secondary" data-size="xs"
						name="seo_status_switch">
					<input type="hidden" name="seo_code" value="<?php echo $seo_code; ?>">
					<input type="hidden" name="seo_type" value="listing">
					

				</div>
				<div class="content-switch seo-toggle-wrap" style="display:none;">
					<div class="row">
						<div class="col-12 col-md-8">
							<div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
								<ul class="nav nav-tabs" role="tablist" style="">
									<?php foreach( $langs as $lang ){ ?>
									<li class="nav-item">
										<a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
											id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
											href="#seo-<?php echo 'title_'.$lang['text']; ?>" role="tab"
											aria-controls="<?php echo 'title_'.$lang['text']; ?>"
											aria-selected="true"><?php echo $lang['text']; ?></a>
									</li>
									<?php } ?>
								</ul>
								<div class="tab-content float-left w-100" style="">
									<?php foreach( $langs as $lang ){ ?>
									<?php
									if( isset( $detail[ $lang['text'] ] ) ){
										if ( sizeof($seo) > 0 ) {
											$page_title = $seo[ $lang['text'] ]['page_title'];
											$meta_title = $seo[ $lang['text'] ]['meta_title'];
											$meta_desc = $seo[ $lang['text'] ]['meta_desc'];
											$meta_keyword = $seo[ $lang['text'] ]['meta_keyword'];
										} else {
											$page_title = '';
											$meta_title = '';
											$meta_desc = '';
											$meta_keyword = '';
										}
										
									}else{
										$page_title = '';
										$meta_title = '';
										$meta_desc = '';
										$meta_keyword = '';
									}
									?>
									<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
										id="seo-<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
										aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
										<div class="form-group">
											<label for="">Page title</label>
											<input type="text" name="page_title[]" class="form-control" placeholder="Page title"
												value="<?php echo $page_title; ?>">
										</div>
										<div class="form-group">
											<label for="">Meta title</label>
											<input type="text" name="meta_title[]" class="form-control" placeholder="Meta title"
												value="<?php echo $meta_title; ?>">
										</div>
										<div class="form-group">
											<label for="">Meta keyword </label>
											<input type="text" name="meta_keyword[]" class="form-control meta_keyword" placeholder="Meta Keyword"
												value="<?php echo $meta_keyword; ?>">
										</div>
										<div class="form-group">
											<label for="">Meta description</label>
											<input type="text" name="meta_desc[]" class="form-control"
												placeholder="Meta Description" value="<?php echo $meta_desc; ?>">
										</div>
									</div>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-4">
							<div class="form-group">
								<div class="row">
									<div class="col-12 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
										<label>Share Image<small class="text-muted">( 1200 x 630px )</small></label>
										<input type="file" data-height="250" name="share_img" class="dropify"
											data-max-file-size="3M"
											data-default-file="<?php echo $share_img != "" ? upload_path($share_img) : ''; ?>" />
										<input type="hidden" class="image_as_text" name="share_img_txt"
											value="<?php echo $share_img; ?>" />
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="card my-3">
		<div class="card-body">
			<div class="row">
				<div class="col-12">
					<div class="form-group mb-3">
						<label for="">Slug</label>
						<input type="text" name="slug" class="form-control" placeholder="Slug"
							value="<?php echo $slug; ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="">Status</label>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="onoff" value="1"  <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
				</div>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 ? 'checked' : ''; ?>> Non - Publish </label>
				</div>
			</div>

			<div class="row">
				<div class="col-12 text-right">
					<div class="text-center card-button">
						
						<button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<!-- content-wrapper ends -->
<?php include_once("inc/nexitor-modal.php"); ?>