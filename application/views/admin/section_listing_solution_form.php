<?php
  if( sizeof( $detail ) > 0 ){
    //print_r($detail[DEFAULT_LANG]);
	$code = $detail[DEFAULT_LANG]['code'];
    $cate_code = $cate_detail[DEFAULT_LANG]['code'];
	//$cate_code = $detail[DEFAULT_LANG]['cate_code'];
    $cate_code_ar = $detail[DEFAULT_LANG]['cate_code'];

    $usecase_code_ar = $detail[DEFAULT_LANG]['usecase_code'];
    $banner_code_ar = $detail[DEFAULT_LANG]['banner_code'];
    
    $page = $detail[DEFAULT_LANG]['page'];
	$sec_key = $detail[DEFAULT_LANG]['sec_key'];
	
	$image = $detail[DEFAULT_LANG]['image'];
	$post_date = $detail[DEFAULT_LANG]['post_date'];
	$post_time = $detail[DEFAULT_LANG]['post_time'];
	$sticky = $detail[DEFAULT_LANG]['sticky'];
    $hero_banner = $detail[DEFAULT_LANG]['hero_banner'];
    $hero_banner_m = $detail[DEFAULT_LANG]['hero_banner_m'];
    //$bg1_mobile = $detail[DEFAULT_LANG]['bg1_mobile'];
    //$img1_desktop = $detail[DEFAULT_LANG]['img1_desktop'];
	//$img1_mobile = $detail[DEFAULT_LANG]['img1_mobile'];
	$onoff = $detail[DEFAULT_LANG]['onoff'];
    $link_out = $detail[DEFAULT_LANG]['link_out'];
    $link_type = $detail[DEFAULT_LANG]['link_type'];
    $btn_show = $detail[DEFAULT_LANG]['btn_show'];
    $slug = $detail[DEFAULT_LANG]['slug'];
    $tags = $detail[DEFAULT_LANG]['tags'];
    $is_highlight = $detail[DEFAULT_LANG]['is_highlight'];

    $approve_status = $detail[DEFAULT_LANG]['approve_status'];

    $published_date = $detail[DEFAULT_LANG]['published_date'];
    if ( $published_date != '' ) {
        $p_date = substr($published_date, 0, 10);
        $p_date = explode('-', $p_date);
        $p_date = ($p_date[0]+543).'-'.$p_date[1].'-'.$p_date[2];
        $p_time = substr($published_date, 11, 8);
    } else {
        $p_date = '';
        $p_time = '';
    }
    $image_product = $detail[DEFAULT_LANG]['image_product'];
    $image_detail = $detail[DEFAULT_LANG]['image_detail'];
    
  }else{
	$code = '';
	$cate_code = $cate_detail[DEFAULT_LANG]['code'];
    $cate_code_ar = json_encode(array($cate_code));

    $usecase_code_ar = json_encode(array());
    $banner_code_ar = json_encode(array());

    $page = $param_page;
	$sec_key = $param_sec_key;
	$image = '';
	$post_date = '';
	$post_time = '';
	$sticky = 0;
    $hero_banner ='';
    $hero_banner_m ='';
    $btn_text='';
    $price = '';
    $btn_show = 0;
    $link_out ='';
    $link_type ='auto';
    //$bg1_mobile = '';

    //$img1_desktop = '';
	//$img1_mobile = '';

	$onoff = 0;
    $slug = time();
    $tags = '';
    $is_highlight = '0';
    $approve_status = 0;
    $p_date = '';
    $p_time = '';

    $image_product = '';
    $image_detail = '';
    
  }
  if( sizeof( $seo ) > 0 ){
    $share_img = $seo[DEFAULT_LANG]['share_img'];
  }else{
    $share_img = '';
  }
//echo 'must_be_approve='.$cate_detail[DEFAULT_LANG]['must_be_approve'];
?>
<div class="content-wrapper">

    <div class="page-header">
        <h3 class="page-title">
            <?php echo strtoupper( 'Listing item form of '.$cate_detail[DEFAULT_LANG]['title'].' category') ?> </h3>
        <a href="<?php echo base_url().'admin/section/listing/lists/'.$sec_detail[DEFAULT_LANG]['code'].'/'.$cate_code; ?>"
            class="btn btn-gradient-dark btn-fw">
            <- Back</a>
                <?php /* <a href="<?php echo base_url().'admin/banner/add' ?>" class="btn btn-gradient-dark btn-fw">+
                Add
                more
        </a> */ ?>
    </div>
    <?php echo form_open_multipart('admin/section/saveListingSolutionForm', ' class="mainForm listingForm"'); ?>
    <input type="hidden" name="code" value="<?php echo $code; ?>">
    <input type="hidden" name="page" value="<?php echo $page; ?>">
    <input type="hidden" name="sec_key" value="<?php echo $sec_key; ?>">
    <input type="hidden" name="sec_code" value="<?php echo $sec_detail[DEFAULT_LANG]['code'] ?>" >
    <input type="hidden" name="cate_code_one" value="<?php echo $cate_code; ?>">
    
 
    <input type="hidden" name="time" value="<?php echo $time; ?>">

    <input type="hidden" name="preview" value="<?php echo time(); ?>">
    <input type="hidden" name="is_preview" value="0">

    <input type="hidden" name="is_approve" value="0">

    <?php if ( (isApprover($this->session->userdata('user_type')) or $cate_detail[DEFAULT_LANG]['must_be_approve'] == '0') and $code == '' ) { ?>
    <input type="hidden" name="approve_status" value="2">
    <?php } else { ?>
    <input type="hidden" name="approve_status" value="<?php echo $approve_status; ?>">
    <?php } ?>

    <?php //include_once("inc/banner_content.php"); ?>

    <div class="card my-3">
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-12"> 
                        <h4 class="card-title">Background & Thumb Image</h4>
                    </div>
                    <div class="col-12 col-md-4 image-box"> 
                        <label>Banner Desktop <i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Solution : 000 x 000 px" data-original-title="Image recommended"></i></label>
                        <input type="file" data-height="200" name="hero_banner" class="dropify" data-max-file-size="3M"
                            data-default-file="<?php echo $hero_banner != "" ? upload_path($hero_banner) : ''; ?>" />
                        <input type="hidden" class="image_as_text" name="hero_banner_txt" value="<?php echo $hero_banner; ?>" />
                    </div>
                    <div class="col-12 col-md-4 image-box">
                        <label>Banner Mobile <i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Solution : 000 x 000 px" data-original-title="Image recommended"></i></label>
                        <input type="file" data-height="200" name="hero_banner_m" class="dropify" data-max-file-size="3M"
                        data-default-file="<?php echo $hero_banner_m != "" ? upload_path($hero_banner_m) : ''; ?>" />
                        <input type="hidden" class="image_as_text" name="hero_banner_m_txt" value="<?php echo $hero_banner_m; ?>" />
                    </div>
                    <div class="col-12 col-md-4 image-box">
                        <label>Thumb Image <i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Solution : 000 x 000 px" data-original-title="Image recommended"></i></label>
                        <input type="file" data-height="200" name="image_product" class="dropify" data-max-file-size="3M"
                            data-default-file="<?php echo $image_product != "" ? upload_path($image)_product : ''; ?>" />
                        <input type="hidden" class="image_as_text" name="image_product_txt" value="<?php echo $image_product; ?>" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card my-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-4 image-box">
                    <div class="row">
                        <div class="col-12 mb-3">
                            <label>Image Detail <i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="Solution : 000 x 000 px" data-original-title="Image recommended"></i></label>
                            <input type="file" data-height="200" name="image_detail" class="dropify" data-max-file-size="3M"
                            data-default-file="<?php echo $image_detail != "" ? upload_path($image)_detail : ''; ?>" />
                            <input type="hidden" class="image_as_text" name="image_detail_txt" value="<?php echo $image_detail; ?>" />
                        </div>
                        <div class="col-12 mb-3">
                            <label>Category</label>
                            <?php if( $cate_code_ar != "" ){ $ar_cate_code = json_decode( $cate_code_ar, true ); }else{ $ar_cate_code = array(); } ?>
                            <div class="box-cate">
                                <div class="box-cate-wrap"> 
                                    <?php foreach( $cate_lists as $cate ){ ?>
                                        <div class="box-level box-level-0">
                                            <div class="form-check form-check-dark">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" <?php echo in_array( $cate['code'], $ar_cate_code ) ? 'checked' : ''; ?> name="cate_code[]" value="<?php echo $cate['code']; ?>"> <?php echo $cate['title']; ?> 
                                                </label>
                                            </div>
                                        </div> <!-- end level 0 -->
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
                        <ul class="nav nav-tabs" role="tablist" style="">
                            <?php foreach( $langs as $lang ){ ?>
                            <li class="nav-item">
                                <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                    id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
                                    href="#<?php echo 'title_'.$lang['text']; ?>" role="tab"
                                    aria-controls="<?php echo 'title_'.$lang['text']; ?>"
                                    aria-selected="true"><?php echo $lang['text']; ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content float-left w-100" style="">
                            <?php foreach( $langs as $lang ){ ?>
                            <?php
                            if( isset( $detail[ $lang['text'] ] ) ){
                                $prefix_title = $detail[ $lang['text'] ]['prefix_title'];
                                $title = $detail[ $lang['text'] ]['title'];
                                $sub_title = $detail[ $lang['text'] ]['sub_title'];
                                $description = $detail[ $lang['text'] ]['description'];
                            }else{
                                $prefix_title = '';
                                $title = '';
                                $sub_title = '';
                                $description = '';
                            }
                            ?>
                            <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                id="<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
                                aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
                                <div class="row">
                                    <div class="col-12 mt-3">
                                        <div class="form-group">
                                            <label for="">Prefix Title</label>
                                            <input type="text" name="prefix_title[]" class="form-control" placeholder="Prefix Title"
                                                value="<?php echo $prefix_title; ?>">
                                        </div>
                                    </div>
                                    <div class="col-12 mt-3">
                                        <div class="form-group">
                                            <label for="">Title</label>
                                            <input type="text" name="title[]" class="form-control" placeholder="Title"
                                                value="<?php echo $title; ?>">
                                        </div>
                                    </div>
                                    <div class="col-12 mt-3">
                                        <div class="form-group">
                                            <label for="">Sub Title</label>
                                            <input type="text" name="sub_title[]" class="form-control" placeholder="Sub-Title"
                                                value="<?php echo $sub_title; ?>"
                                                maxlength="<?php echo $param_sec_key == 'calendar' ? '100' : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="col-12 mt-3">
                                        <div class="form-group">
                                            <label for="">Description</label>
                                            <textarea name="description[]" class="form-control tinyMceExample" rows="7"><?php echo $description; ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <?php // if($param_sec_key != "vrpartner"){ ?>

                                <?php //} ?>
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label for="">Slug</label>
                <input type="text" name="slug" class="form-control" placeholder="Slug"
                    value="<?php echo $slug; ?>">
            </div>
        </div>
    </div>
    
    <div class="card my-3">
        <div class="card-body">
            <div class="box-switch">
                <h4 class="card-title">Content</h4>
                <div class="button-switch">
                    <input type="checkbox" data-toggle="toggle" data-onstyle="info" data-on="Show" data-off="Hide"
                        data-offstyle="outline-secondary" data-size="xs" name="box_switch" checked
                        onchange="sectionToggle($(this))">
                </div>
                <div class="content-switch">
                    <div class="content">
                        <div class="row">
                       
                            <div class="col col-12  col-md-12">
                                <div class="form-group">
                                    <div class=" w-100 <?php echo sizeof( $langs ) == 0 ? 'only-one-lang' : ''; ?>">
                                        <ul class="nav nav-tabs" role="tablist" style="">
                                            <?php foreach( $langs as $lang ){ ?>
                                            <li class="nav-item">
                                                <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                                    id="<?php echo $lang['text']; ?>-tab" data-toggle="tab"
                                                    href="#<?php echo $lang['text']; ?>" role="tab"
                                                    aria-controls="<?php echo $lang['text']; ?>"
                                                    aria-selected="true"><?php echo $lang['text']; ?></a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                        <div class="tab-content float-left w-100" style="">
                                            <?php foreach( $langs as $lang ){ ?>
                                            <?php
                                            if( isset( $detail[ $lang['text'] ] ) ){
                                                $content_editor = $detail[ $lang['text'] ]['content_editor'];
                                            }else{
                                                $content_editor = '';
                                            }
                                            ?>
                                            <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                                id="<?php echo $lang['text']; ?>" role="tabpanel"
                                                aria-labelledby="<?php echo $lang['text']; ?>-tab">

                                                <div class="form-group">
                                                    <div class="install-nexitor float-left" data-area="content">
                                                        <?php echo $content_editor; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="card my-3">
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-12">
                        <label for="">Banefit </label>
                        <style>
                        /* Repeater Item */
                        .repeater .ui-sortable .drag-cusor {
                            display: flex;
                            align-items: center;
                            padding-right: 15px;
                        }
                        </style>                    
                    

                        <div class="repeater repeater-features"> 
                            
                            <div data-repeater-list="features" class="drag-item-list list-group" id="sortable" >
                                <?php
                                if ( $code != '' ) {
                                    $features = $detail[DEFAULT_LANG]['features'];
                                    if ( $features != '' ) {
                                        $features = json_decode( $features, true );
                                    }
                                } else {
                                    $features = '';
                                } 

                                if( $features) {
                                    foreach ($features as $key => $row) {
                                        if ( isset($row['image']) && $row['image'] != "" ) {
                                            $features_image = $row['image'];
                                            if ( isset($row['image_txt']) && $row['image_txt'] != "" && $features_image == '' ) {
                                                $features_image = $row['image_txt'];
                                            }
                                        } else {
                                            $features_image = '';
                                            if ( isset($row['image_txt']) && $row['image_txt'] != "" && $features_image == '' ) {
                                                $features_image = $row['image_txt'];
                                            }
                                        }

                                ?>
                                <div data-repeater-item="" class="d-flex mb-2 list-group-item">
                                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                        <div class="input-group-prepend">
                                            <div class="nexu-tools-list drag-cusor ui-sortable-handle">
                                                <a href="javascript:void(0)"><i class="mdi mdi-cursor-move"></i></a>
                                            </div>
                                        </div>
                                        <div class="mb-3 d-flex" style="width:95%;">
                                            <div class="w-25">
                                                <div>
                                                    <label>Image</label>
                                                    <input type="file" data-height="200" name="image" class="dropify" data-max-file-size="3M"
                                                data-default-file="<?php echo $features_image != "" ? upload_path($features_image) : ''; ?>" />
                                                    <input type="hidden" class="image_as_text" name="image_txt" value="<?php echo $features_image; ?>" />
                                                </div>
                                            </div>
                                            <div class="w-75 mr-2">
                                                <ul class="nav nav-tabs" role="tablist" style="">
                                                    <?php foreach( $langs as $lang ){ ?>
                                                    <li class="nav-item">
                                                        <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                                            id="<?php echo 'features_'.$lang['text'].'-'.$key; ?>-tab" data-toggle="tab"
                                                            href="#<?php echo 'features_'.$lang['text'].'-'.$key; ?>" role="tab"
                                                            aria-controls="<?php echo 'features_'.$lang['text'].'-'.$key; ?>"
                                                            aria-selected="true"><?php echo $lang['text']; ?></a>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                                <div class="tab-content float-left w-100" style="">
                                                    <?php foreach( $langs as $lang ){ ?>
                                                    <?php
                                                    $features_lang = $detail[$lang['text']]['features'];
                                                    if ( $features_lang != '' ) {
                                                        $features_lang = json_decode( $features_lang, true );
                                                        $row_features = $features_lang[$key];
                                                    }
                                                    ?>
                                                    <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo 'features_'.$lang['text'].'-'.$key; ?>" role="tabpanel" aria-labelledby="<?php echo 'features_'.$lang['text'].'-'.$key; ?>-tab">
                                                        <div class="mb-3">
                                                            <label>Title</label>
                                                            <input type="text" class="form-control form-control-sm w-100" name="title_<?php echo $lang['text']; ?>" placeholder="Title" value="<?php echo $row_features['title']; ?>">
                                                        </div>
                                                        <div class="w-100">
                                                            <div>
                                                                <label>Description</label>
                                                                <textarea class="form-control form-control-sm" name="description_<?php echo $lang['text']; ?>" placeholder="Description" rows="3"><?php echo $row_features['description']; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?> 
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <button data-repeater-delete type="button" class="btn btn-gradient-danger btn-sm icon-btn ml-2">
                                    <i class="mdi mdi-delete"></i>
                                    </button>
                                </div>
                                <?php
                                    }
                                } else {
                                ?>
                                <div data-repeater-item="" class="d-flex mb-2 list-group-item">
                                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                        <div class="input-group-prepend">
                                            <div class="nexu-tools-list drag-cusor ui-sortable-handle">
                                                <a href="javascript:void(0)"><i class="mdi mdi-cursor-move"></i></a>
                                            </div>
                                        </div>
                                        <div class="mb-3 d-flex" style="width:95%;">
                                            <div class="w-25 mr-2">
                                                <div>
                                                    <label>Image</label>
                                                    <input type="file" data-height="200" name="image" class="dropify" data-max-file-size="3M"
                                                data-default-file="" />
                                                    <input type="hidden" class="image_as_text" name="image_txt" value="" />
                                                </div>
                                            </div>

                                            <div class="w-75 mr-2">
                                                <ul class="nav nav-tabs" role="tablist" style="">
                                                    <?php foreach( $langs as $lang ){ ?>
                                                    <li class="nav-item">
                                                        <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                                            id="<?php echo 'features_'.$lang['text'].'-0'; ?>-tab" data-toggle="tab"
                                                            href="#<?php echo 'features_'.$lang['text'].'-0'; ?>" role="tab"
                                                            aria-controls="<?php echo 'features_'.$lang['text'].'-0'; ?>"
                                                            aria-selected="true"><?php echo $lang['text']; ?></a>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                                <div class="tab-content float-left w-100" style="">
                                                    <?php foreach( $langs as $lang ){ ?>
                                                
                                                    <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" id="<?php echo 'features_'.$lang['text'].'-0'; ?>" role="tabpanel" aria-labelledby="<?php echo 'features_'.$lang['text'].'-0'; ?>-tab">
                                                        <div class="mb-3">
                                                            <label>Title</label>
                                                            <input type="text" class="form-control form-control-sm w-100" name="title_<?php echo $lang['text']; ?>" placeholder="Title" value="">
                                                        </div>
                                                        <div class="w-100">
                                                            <div>
                                                                <label>Description</label>
                                                                <textarea class="form-control form-control-sm" name="description_<?php echo $lang['text']; ?>" placeholder="Description" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?> 
                                                </div>
                                            </div>
                                            
                                        </div>

                                    </div>
                                    <button data-repeater-delete type="button" class="btn btn-gradient-dark btn-sm icon-btn ml-2">
                                    <i class="mdi mdi-delete"></i>
                                    </button>
                                </div>


                                <?php 
                                }
                                ?>
                            </div>
                            
                            <button data-repeater-create="features" type="button" class="btn btn-gradient-dark btn-fw icon-btn mb-2">
                                <i class="mdi mdi-plus"></i> Add Row
                            </button>
                        </div>  
                               
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    <div class="card my-3">
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-12">
                        <label>Use Case Location</label>
                        <?php if( $usecase_code_ar != "" ){ $ar_usecase_code = json_decode( $usecase_code_ar, true ); }else{ $ar_usecase_code = array(); } ?>
                        <div class="box-cate">
                            <div class="box-cate-wrap"> 
                                <?php foreach( $usecase_lists as $usecase ){ ?>
                                    <div class="box-level box-level-0">
                                        <div class="form-check form-check-dark">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" <?php echo in_array( $usecase['code'], $ar_usecase_code ) ? 'checked' : ''; ?> name="usecase_code[]"    value="<?php echo $usecase['code']; ?>"> <?php echo $usecase['title']; ?>
                                            </label>
                                        </div>
                                    </div> <!-- end level 0 -->
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
        </div>
    </div>
    
    <div class="card my-3">
        <div class="card-body">
            <div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
                <label for="">Solution Contact </label>
                <ul class="nav nav-tabs" role="tablist" style="">
                    <?php foreach( $langs as $lang ){ ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                            id="<?php echo 'inquire_'.$lang['text']; ?>-tab" data-toggle="tab"
                            href="#<?php echo 'inquire_'.$lang['text']; ?>" role="tab"
                            aria-controls="<?php echo 'inquire_'.$lang['text']; ?>"
                            aria-selected="true"><?php echo $lang['text']; ?></a>
                    </li>
                    <?php } ?>
                </ul>
                <div class="tab-content float-left w-100" style="">
                    <?php foreach( $langs as $lang ){ ?>
                    <?php
					if( isset( $detail[ $lang['text'] ] ) ){
                        $btn1_text = $detail[ $lang['text'] ]['btn1_text'];
						$btn1_link = $detail[ $lang['text'] ]['btn1_link'];
              			$btn2_text = $detail[ $lang['text'] ]['btn2_text'];
                        $btn2_link = $detail[ $lang['text'] ]['btn2_link'];
					}else{
                        $btn1_text = '';
						$btn1_link = '';
              			$btn2_text = '';
                        $btn2_link = '';
					}
					?>
                    <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                        id="<?php echo 'inquire_'.$lang['text']; ?>" role="tabpanel"
                        aria-labelledby="<?php echo 'inquire_'.$lang['text']; ?>-tab">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">Button 1</label>
                                    <input type="text" name="btn1_text[]" class="form-control mb-1" placeholder="Button 1"
                                        value="<?php echo $btn1_text; ?>">

                                    <input type="text" name="btn1_link[]" class="form-control" placeholder="Link 1"
                                        value="<?php echo $btn1_link; ?>">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">Button 2</label>
                                    <input type="text" name="btn2_text[]" class="form-control mb-1" placeholder="Button 2"
                                        value="<?php echo $btn2_text; ?>">

                                    <input type="text" name="btn2_link[]" class="form-control" placeholder="Link 2"
                                        value="<?php echo $btn2_link; ?>">
                                </div>
                            </div>
                        </div>

                        <?php // if($param_sec_key != "vrpartner"){ ?>

                        <?php //} ?>
                    </div>
                    <?php } ?>

                </div>
            </div>
            
        </div>
    </div>
    
    <div class="card my-3">
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-12">
                        <label>Solution Owner</label>
                        <?php if( $banner_code_ar != "" ){ $ar_banner_code = json_decode( $banner_code_ar, true ); }else{ $ar_banner_code = array(); } ?>
                        <div class="box-cate">
                            <div class="box-cate-wrap only-one"> 
                                <?php foreach( $banner_lists as $banner ){ ?>
                                    <div class="box-level box-level-0">
                                        <div class="form-check form-check-dark">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" <?php echo in_array( $banner['code'], $ar_banner_code ) ? 'checked' : ''; ?> name="banner_code[]" value="<?php echo $banner['code']; ?>"> <?php echo $banner['name']; ?>
                                            </label> 
                                        </div>
                                    </div> <!-- end level 0 -->
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
        </div>
    </div>

    <div class="card my-3">
        <div class="card-body">
          
			<div class="form-group mb-0">
				<label for="">Order </label>
				<input type="text" name="sticky" class="form-control"  placeholder="Order" value="<?php echo $sticky; ?>">
			</div>
        </div>
    </div>

    <div class="card my-3">
        <div class="card-body">
            <div class="box-switch">
            <h3 class="card-title"> SEO Page setting </h3>
                <div class="button-switch seo-switch">
                <?php
                if( isset( $detail[ $lang['text'] ] ) ){
                    if ( sizeof($seo) > 0 ) {
                        $seo_code = $seo[ $lang['text'] ]['code'];
                    } else {
                        $seo_code = '';
                    }
                    
                }else{
                    $seo_code = '';
                }
                ?>
                    <input type="checkbox" <?php // echo $seo_status == 1 ? 'checked' : '' ?> data-toggle="toggle"
                        data-onstyle="info" data-on="Show" data-off="Hide" data-offstyle="outline-secondary" data-size="xs"
                        name="seo_status_switch">
                    <input type="hidden" name="seo_code" value="<?php echo $seo_code; ?>">
                    <input type="hidden" name="seo_type" value="listing">
                    

                </div>
                <div class="content-switch seo-toggle-wrap" style="display:none;">
                    <div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
                        <ul class="nav nav-tabs" role="tablist" style="">
                            <?php foreach( $langs as $lang ){ ?>
                            <li class="nav-item">
                                <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                    id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
                                    href="#seo-<?php echo 'title_'.$lang['text']; ?>" role="tab"
                                    aria-controls="<?php echo 'title_'.$lang['text']; ?>"
                                    aria-selected="true"><?php echo $lang['text']; ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content float-left w-100" style="">
                            <?php foreach( $langs as $lang ){ ?>
                            <?php
                            if( isset( $detail[ $lang['text'] ] ) ){
                                if ( sizeof($seo) > 0 ) {
                                    $page_title = $seo[ $lang['text'] ]['page_title'];
                                    $meta_title = $seo[ $lang['text'] ]['meta_title'];
                                    $meta_desc = $seo[ $lang['text'] ]['meta_desc'];
                                    $meta_keyword = $seo[ $lang['text'] ]['meta_keyword'];
                                } else {
                                    $page_title = '';
                                    $meta_title = '';
                                    $meta_desc = '';
                                    $meta_keyword = '';
                                }
                            }else{
                                $page_title = '';
                                $meta_title = '';
                                $meta_desc = '';
                                $meta_keyword = '';
                            }
                            ?>
                            <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                id="seo-<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
                                aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
                                <div class="form-group">
                                    <label for="">Page title</label>
                                    <input type="text" name="page_title[]" class="form-control" placeholder="Page title"
                                        value="<?php echo $page_title; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="">Meta title</label>
                                    <input type="text" name="meta_title[]" class="form-control" placeholder="Meta title"
                                        value="<?php echo $meta_title; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="">Meta keyword </label>
                                    <input type="text" name="meta_keyword[]" class="form-control meta_keyword" placeholder="Meta Keyword"
                                        value="<?php echo $meta_keyword; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="">Meta description</label>
                                    <input type="text" name="meta_desc[]" class="form-control"
                                        placeholder="Meta Description" value="<?php echo $meta_desc; ?>">
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-12 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
                                <label>Share Image<small class="text-muted">( 1200 x 630px )</small></label>
                                <input type="file" data-height="250" name="share_img" class="dropify"
                                    data-max-file-size="3M"
                                    data-default-file="<?php echo $share_img != "" ? upload_path($share_img) : ''; ?>" />
                                <input type="hidden" class="image_as_text" name="share_img_txt"
                                    value="<?php echo $share_img; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card my-3">
        <div class="card-body">
            <div class="row">
                <div class="col-6 mt-3">
                <div class="form-group">
                <label for="">Publish Date</label>
                    <input class="input-medium form-control datepicker" name="published_date" type="text"
                        data-date-language="th-th" autocomplete="off" value="<?php echo $p_date; ?>">
                </div>
                </div>
                <div class="col-6 mt-3">
                    <div class="form-group">
                        <label for="">Publish Time</label>
                        <input type="text" name="published_time" class="form-control" value="<?php echo $p_time; ?>">
                    </div>
                </div>
            </div> 
        </div>
    </div> 

    <div class="card my-3">
        <div class="card-body">
        
            <div class="form-group">
                <label for="">Status</label>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="1" <?php echo $onoff == 1 || $approve_status == 1 ? 'checked' : ''; ?>> Publish </label>
                </div>
                <div class="form-check form-check-dark">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 && $approve_status != 1 ? 'checked' : ''; ?>> Unpublish </label>
                </div>
            </div> 

            <div class="row">
                <div class="col-12 text-right">
                    <div class="text-center card-button">
                        <button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
                        <!-- <button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 listing-previewForm">Preview Changes</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo form_close(); ?>
</div>

<?php //include_once("inc/nexitor-modal.php"); ?>