<script>
$(function() {
/* ChartJS
 * -------
 * Data and config for chartjs
 */
'use strict';

var areaData = {
labels: ["2013", "2014", "2015", "2016", "2017", "2018"],
datasets: [{
  label: '# of Votes',
  data: [6, 11, 3, 5, 24, 3],
  backgroundColor: ['rgba(255, 99, 132, 0.2)'],
  borderColor: ['rgba(255,99,132,1)'],
  borderWidth: 2,
  fill: true,
},{
  label: '# of Votes',
  data: [50, 20, 30, 65, 33, 66],
  backgroundColor: ['rgba(54, 162, 235, 0.2)'],
  borderColor: ['rgba(54, 162, 235, 1)'],
  borderWidth: 2,
  fill: true, // 3: no fill
}
]
};

if ($("#areaChart").length) {
var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
var areaChart = new Chart(areaChartCanvas, {
  type: 'line',
  data: areaData,
});
}
});
</script>
