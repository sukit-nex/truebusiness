<script>
  var base_url = '<?php echo base_url(); ?>';
  var assets_path = '<?php echo REAL_PATH; ?>assets/admin/';
  var upload_path = '<?php echo upload_path(); ?>';
  var real_path = '<?php echo REAL_PATH; ?>';
</script>
<!-- plugins:js -->

<script src="<?php echo ADMIN_ASSET; ?>vendors/js/vendor.bundle.base.js?v=<?php echo rand(1,999); ?>"></script>
<script src="<?php echo ADMIN_ASSET; ?>vendors/js/vendor.bundle.addons.js?v=<?php echo rand(1,999); ?>"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/bootstrap-datepicker.thai.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/bootstrap-datepicker.th.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/bootstrap4-toggle.min.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/bootstrap-datetimepicker.js"></script>


<?php if( isset( $menu) ){ ?>
  <?php if( $menu == "gall" ){ ?>
    <script src="<?php echo ADMIN_ASSET; ?>js/dropzoneinit.js"></script>
  <?php } ?>
<?php } ?>
<!-- endplugins -->
<!-- Custom js -->

<script src="<?php echo ADMIN_ASSET; ?>js/off-canvas.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/misc.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/dashboard.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>vendors/lightgallery/js/lightgallery-all.min.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/data-table.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/tabs.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/dropify.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/file-upload.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>vendors/tinymce/tinymce.min.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>vendors/tinymce/themes/modern/theme.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>vendors/summernote/dist/summernote-bs4.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/jquery-ui.min.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/jquery.mjs.nestedSortable.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/formpickers.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/form-addons.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/initdrag.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/nexu.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/nexcate.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/nex-smepack-cate.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/nexitor.js"></script>
<?php if( isset( $is_section ) ){ ?>
	<script src="<?php echo ADMIN_ASSET; ?>js/section_drag.js"></script>
<?php } ?>
<?php /* <script src="<?php echo ADMIN_ASSET; ?>js/nexitor-class.js"></script> */ ?>
<script src="<?php echo ADMIN_ASSET; ?>js/bootstrap.bundle.min.js"></script>


<!-- include codemirror (codemirror.css, codemirror.js, xml.js, formatting.js) -->
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js"></script>

<script src="<?php echo ADMIN_ASSET; ?>vendors/ace-builds/src-min/ace.js"></script>

<script src="<?php echo ADMIN_ASSET; ?>js/main.js?v=<?php echo rand(1,999); ?>"></script>

<script src="<?php echo ADMIN_ASSET; ?>js/editor_approve.js?v=<?php echo rand(1,999); ?>"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/admin_approve.js?v=<?php echo rand(1,999); ?>"></script>

<?php if( $this->session->userdata('alert') == '1' ){ ?>

<script>
	popup_message('<?php echo $this->session->userdata('alert_type'); ?>' , '<?php echo $this->session->userdata('alert_msg'); ?>', '<?php echo $this->session->userdata('alert_title'); ?>');
</script>
<?php $this->session->set_userdata('alert', ''); ?>
<?php } ?>
<script>
$(document).ready( function(){
	console.log(12);
	
		if ($(".tinyMceExample").length) {
			tinymce.init({
			selector: ".tinyMceExample",
			theme: "modern",
			paste_data_images: true,
			plugins: [
			  "advlist autolink lists link image charmap print preview hr anchor pagebreak",
			  "searchreplace wordcount visualblocks visualchars code fullscreen",
			  "insertdatetime media nonbreaking save table contextmenu directionality",
			  "emoticons template paste textcolor colorpicker textpattern"
			],
			toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
			toolbar2: "print preview media | forecolor backcolor emoticons",
			image_advtab: true,
			file_picker_callback: function(callback, value, meta) {
			  if (meta.filetype == 'image') {
				$('#upload').trigger('click');
				$('#upload').on('change', function() {
				  var file = this.files[0];
				  var reader = new FileReader();
				  reader.onload = function(e) {
					callback(e.target.result, {
					  alt: ''
					});
				  };
				  reader.readAsDataURL(file);
				});
			  }
			},
			templates: [{
			  title: 'Test template 1',
			  content: 'Test 1'
			}, {
			  title: 'Test template 2',
			  content: 'Test 2'
			}]
		  	});
	  	}
	});
</script>

</body>

</html>
