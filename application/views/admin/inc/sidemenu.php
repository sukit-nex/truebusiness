<div class="container-fluid page-body-wrapper">
    <!-- partial:partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
            <li class="nav-item nav-profile">
                <a href="#" class="nav-link">
                    <div class="nav-profile-image">
                        <img src="<?php echo $this->session->userdata('thumb') == "" ? upload_path('Super-Admin.png') : upload_path($this->session->userdata('thumb')); ?>" alt="profile">
                        <span class="login-status online"></span>
                        <!--change to offline or busy as needed-->
                    </div>
                    <div class="nav-profile-text d-flex flex-column">
                        <span
                            class="font-weight-bold mb-2"><?php echo $this->session->userdata('name').' '.$this->session->userdata('lastname'); ?></span>
                        <span class="text-secondary text-small"><?php echo getRole( $this->session->userdata('user_type') ); ?></span>
                    </div>
                    <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
                </a>
            </li> 
            <?php  if( check_permission( $this->session->userdata('user_type'), 'dashboard') ){ ?>
                <li class="nav-item <?php echo $menu_active == "dashboard" ? 'active' : ''; ?>">
                    <a class="nav-link" href="<?php echo base_url().'admin/dashboard'; ?>">
                        <span class="menu-title">Dashboard</span>
                        <i class="mdi mdi-home menu-icon"></i>
                    </a>
                </li>
            <?php } ?>
            
            
            
            <?php $has_approve = 0 ; $has_editor_permission = 0 ;
                foreach( $cates as $cate ){ 
                    if( $cate['must_be_approve'] == 1){
                        $has_approve++;
                    }
                    if( hasPermission($this->session->userdata('user_type'), $cate ) ){
                        $has_editor_permission++;
                    }
                }
            ?>
            <?php if( $has_approve > 0 ){ ?>
                <?php if( isApprover($this->session->userdata('user_type'))){ ?>
                <li class="nav-item <?php echo $menu_active == "content" ? 'active' : ''; ?>">
                    <a class="nav-link" data-toggle="collapse" href="#content-layouts" aria-expanded="false"
                        aria-controls="content-layouts">
                        <span class="menu-title">Approve Content</span>
                        <i class="menu-arrow"></i>
                        <i class="mdi mdi-settings menu-icon"></i>
                    </a>
                    <div class="collapse" id="content-layouts">
                        <ul class="nav flex-column sub-menu">
                            <?php foreach( $cates as $cate ){ ?>
                                <?php //if( $cate['cate_permission'] != "" ){ ?>
                                    <?php $ar_section = $cate['sec_code'] != '' ? json_decode( $cate['sec_code'] , true) : array() ;?>
                                    <?php if( $cate['must_be_approve'] == 1 ){ ?>
                                    <li class="nav-item ">
                                        <a class="nav-link <?php echo $menu_editor_active == "section/listing/lists/".$ar_section[0]."/".$cate['code'] ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/listing/lists/'.$ar_section[0]."/".$cate['code']; ?>"><?php echo $cate['title']; ?></a>
                                    </li>
                                    <?php } ?>
                                <?php //} ?>
                            <?php } ?>
                        </ul>
                    </div>
                </li>
                <?php } ?>
            <?php } ?>
            <?php if( $has_editor_permission > 0 ){ ?>
                <?php if( isEditor( $this->session->userdata('user_type')) ){ ?>
                <li class="nav-item <?php echo $menu_active == "content" ? 'active' : ''; ?>">
                    <a class="nav-link" data-toggle="collapse" href="#content-layouts" aria-expanded="false"
                        aria-controls="content-layouts">
                        <span class="menu-title">Content</span>
                        <i class="menu-arrow"></i>
                        <i class="mdi mdi-settings menu-icon"></i>
                    </a>
                    <div class="collapse" id="content-layouts">
                        <ul class="nav flex-column sub-menu">
                            <?php foreach( $cates as $cate ){ ?>
                                <?php if( $cate['cate_permission'] != "" ){ ?>
                                    <?php $ar_section = $cate['sec_code'] != '' ? json_decode( $cate['sec_code'] , true) : array() ;?>
                                    <?php if( hasPermission($this->session->userdata('user_type'), $cate ) ){ ?>
                                    <li class="nav-item ">
                                        <a class="nav-link <?php echo $menu_editor_active == "section/listing/lists/".$ar_section[0]."/".$cate['code'] ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/listing/lists/'.$ar_section[0]."/".$cate['code']; ?>"><?php echo $cate['title']; ?></a>
                                    </li>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </li>
                <?php } ?>
            <?php } ?>
            <?php if( $this->session->userdata('user_type') == 'SA' || $this->session->userdata('user_type') == 'A'){ ?>
            <li class="nav-item <?php echo $menu_active == "setting" ? 'active' : ''; ?>">
                <a class="nav-link" data-toggle="collapse" href="#setting-layouts" aria-expanded="false"
                    aria-controls="setting-layouts">
                    <span class="menu-title">Setting</span>
                    <i class="menu-arrow"></i>
                    <i class="mdi mdi-settings menu-icon"></i>
                </a>
                <div class="collapse" id="setting-layouts">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "seo/detail/global" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/seo/detail/global'; ?>">Global SEO</a>
                        </li>
                        <?php  if( check_permission( $this->session->userdata('user_type'), 'menu') ){ ?>
                        <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "menu" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/menu'; ?>">Menu</a></li>
                        <?php } ?>
                        <?php if(  check_permission( $this->session->userdata('user_type'), 'user') ){ ?>
                        <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "user" ? 'active' : ''; ?>"
                                href="<?php echo base_url().'admin/user/lists'; ?>">Admin roles</a></li>
                        <?php }else{ ?>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "user" ? 'active' : ''; ?>"
                                href="<?php echo base_url().'admin/user/edit/'.$this->session->userdata('id'); ?>">Edit profile</a></li>
                        <?php } ?>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "permission/editor" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/permission/editor'; ?>">Editor Permission</a>
                        </li>
                        <?php /*?><li class="nav-item"> <a class="nav-link <?php echo $menu_active == "wording" ? 'active' : ''; ?>"
                        href="<?php echo base_url().'admin/setting/wording'; ?>">Wording</a></li><?php */?>
                    </ul>
                </div>
            </li>
            <?php } ?>
            <!-- <?php  if( check_permission( $this->session->userdata('user_type'), 'coverage') ){ ?>
                <li class="nav-item <?php echo $menu_active == "widget" ? 'active' : ''; ?>">
                    <a class="nav-link" data-toggle="collapse" href="#widget-layouts" aria-expanded="false"
                        aria-controls="widget-layouts">
                        <span class="menu-title">Widget</span>
                        <i class="menu-arrow"></i>
                        <i class="mdi mdi-brightness-auto menu-icon"></i>
                    </a>
                    <div class="collapse" id="widget-layouts">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "coverage" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/coverage'; ?>">Coverage</a></li>
                        </ul>
                    </div>
                </li>
            <?php } ?> -->


    
        <!-- <?php  if( check_permission( $this->session->userdata('user_type'), 'immersive') ){ ?>
            <div class="section-seperate "><span class="font-weight-bold mb-2">Phase 1</span></div>
            <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#immersive-layouts" aria-expanded="false" aria-controls="immersive-layouts">
                <span class="menu-title">Immersive Experience</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-apps menu-icon"></i>
            </a>
            <div class="collapse" id="immersive-layouts">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item ">
                        <a class="nav-link <?php echo $menu_active == "seo/detail/immersive" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/seo/detail/immersive'; ?>">SEO</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link <?php echo $menu_active == "banner/lists/immersive" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/banner/lists/immersive'; ?>">Banner</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link <?php echo $menu_active == "section/lists/immersive" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/lists/immersive'; ?>">Section</a>
                    </li>
                </ul>
            </div>
            </li>
        <?php } ?>
        <?php  if( check_permission( $this->session->userdata('user_type'), 'xr') ){ ?>
            <li class="nav-item ">
                <a class="nav-link" data-toggle="collapse" href="#xr-layouts" aria-expanded="false" aria-controls="xr-layouts">
                    <span class="menu-title">True5G XR</span>
                    <i class="menu-arrow"></i>
                    <i class="mdi mdi-apps menu-icon"></i>
                </a>
                <div class="collapse" id="xr-layouts">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "seo/detail/xr" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/seo/detail/xr'; ?>">SEO</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/xr/phenomenon" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/xr/phenomenon'; ?>">Highlight</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/xr/studiovideo" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/xr/studiovideo'; ?>">Video</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/xr/studiocontent" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/xr/studiocontent'; ?>">XR Studio</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/xr/musicspace" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/xr/musicspace'; ?>">Music X space</a>
                        </li>
                        <li class="nav-item menu-nolink"> 
                            <a class="nav-link">Experience</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/detail/xr/experience" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/detail/xr/experience'; ?>">Title</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/content/xr/experience" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/content/lists/xr/experience'; ?>">Content List</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/category/xr/experience" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/category/lists/xr/experience'; ?>">Content Category</a>
                        </li>
                        <li class="nav-item menu-nolink"> <a class="nav-link">Calendar</a></li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/detail/xr/calendar" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/detail/xr/calendar'; ?>">Title</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/content/xr/calendar" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/content/lists/xr/calendar'; ?>">Content List</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/xr/application" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/xr/application'; ?>">Application</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/xr/alliance" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/xr/alliance'; ?>">XR Alliance</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/footer/xr" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/footer/xr'; ?>">Footer</a>
                        </li>
                    </ul>
                </div>
            </li>
        <?php } ?>
        <?php  if( check_permission( $this->session->userdata('user_type'), 'ar') ){ ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#ar-layouts" aria-expanded="false" aria-controls="ar-layouts">
                    <span class="menu-title">True5G AR</span>
                    <i class="menu-arrow"></i>
                    <i class="mdi mdi-apps menu-icon"></i>
                </a>
                <div class="collapse" id="ar-layouts">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "seo/detail/ar" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/seo/detail/ar'; ?>">SEO</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/ar/limitlessexperience" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/ar/limitlessexperience'; ?>">Hightlight</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/ar/limitlessexperiencevideo" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/ar/limitlessexperiencevideo'; ?>">Video</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/ar/truearapp" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/ar/truearapp'; ?>">True5G AR App</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/ar/virtualworld" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/ar/virtualworld'; ?>">AR Filter</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/ar/smartbenefits" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/ar/smartbenefits'; ?>">Privilege</a>
                        </li>
                        <li class="nav-item menu-nolink"> <a class="nav-link">AR Smart Shelf</a></li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/detail/ar/truecatalog" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/detail/ar/truecatalog'; ?>">Content</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/content/ar/truecatalog" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/content/lists/ar/truecatalog'; ?>">Product List</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/ar/argame" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/ar/argame'; ?>">AR Game</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/footer/ar" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/footer/ar'; ?>">Footer</a>
                        </li>
                    </ul>
                </div>
            </li>
        <?php } ?>
        <?php  if( check_permission( $this->session->userdata('user_type'), 'vr') ){ ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#vr-layouts" aria-expanded="false" aria-controls="vr-layouts">
                    <span class="menu-title">True5G VR</span>
                    <i class="menu-arrow"></i>
                    <i class="mdi mdi-apps menu-icon"></i>
                </a>
                <div class="collapse" id="vr-layouts">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "seo/detail/vr" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/seo/detail/vr'; ?>">SEO</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/vr/limitlessimaginnation" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/vr/limitlessimaginnation'; ?>">Higtlight</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/detail/vr/limitlessimaginnationvideo" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/detail/vr/limitlessimaginnationvideo'; ?>">Video</a>
                        </li>
                        <li class="nav-item menu-nolink"> <a class="nav-link">Touch The Beyond</a></li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/detail/vr/touchthebeyond" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/detail/vr/touchthebeyond'; ?>">Content</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/content/vr/touchthebeyond" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/content/lists/vr/touchthebeyond'; ?>">Content List</a>
                        </li>

                        <li class="nav-item menu-nolink"> <a class="nav-link">See The Unseen</a></li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/detail/vr/seetheunseen" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/detail/vr/seetheunseen'; ?>">Content</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/category/vr/seetheunseen" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/category/lists/vr/seetheunseen'; ?>">Content Category</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/content/vr/seetheunseen" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/content/lists/vr/seetheunseen'; ?>">Content List</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/content/vr/vrpartner" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/content/lists/vr/vrpartner'; ?>">Partner List</a>
                        </li>

                        <li class="nav-item menu-nolink"> <a class="nav-link">VR Product</a></li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/detail/vr/vrproduct" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/detail/vr/vrproduct'; ?>">Content</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/content/vr/vrinfo" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/content/lists/vr/vrinfo'; ?>">Info List</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "section/content/vr/vrproduct" ? 'active' : ''; ?>"
                            href="<?php echo base_url().'admin/section/content/lists/vr/vrproduct'; ?>">Gallery List</a>
                        </li>
                        
                    <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "section/footer/vr" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/footer/vr'; ?>">Footer</a>
                        </li>
                    </ul>
                </div>
            </li>
        <?php } ?> -->
            <div class="section-seperate "><span class="font-weight-bold mb-2">Content Management</span></div>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#smepack" aria-expanded="false" aria-controls="smepack">
                    <span class="menu-title">SME Pack content</span>
                    <i class="menu-arrow"></i>
                    <i class="mdi mdi-apps menu-icon"></i>
                </a>
                <div class="collapse" id="smepack">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url().'admin/smepack_category/main_content'; ?>" ><span class="menu-title">Main content</span></a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "smepack_category/content/1" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/content/1'; ?>">Mobile</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack/lists/1" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/lists/1'; ?>">All Package</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack/add/1" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/add/1'; ?>">Create Package</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack_category/lists/1" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/lists/1'; ?>">Category</a>
                        </li>

                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "smepack_category/content/2" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/content/2'; ?>">Internet</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack/lists/2" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/lists/2'; ?>">All Package</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack/add/2" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/add/2'; ?>">Create Package</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack_category/lists/2" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/lists/2'; ?>">Category</a>
                        </li>

                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "smepack_category/content/3" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/content/3'; ?>">Digital TV</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack/lists/3" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/lists/3'; ?>">All Package</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack/add/3" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/add/3'; ?>">Create Package</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack_category/lists/3" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/lists/3'; ?>">Category</a>
                        </li>

                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "smepack_category/content/4" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/content/4'; ?>">Fixedline</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack/lists/4" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/lists/4'; ?>">All Package</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack/add/4" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/add/4'; ?>">Create Package</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack_category/lists/4" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/lists/4'; ?>">Category</a>
                        </li>

                        <li class="nav-item ">
                            <a class="nav-link <?php echo $menu_active == "smepack_category/content/5" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/content/5'; ?>">Value set</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack/lists/5" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/lists/5'; ?>">All Package</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack/add/5" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/add/5'; ?>">Create Package</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack_category/lists/5" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/lists/5'; ?>">Category</a>
                        </li>


                        <li class="nav-item menu-nolink">
                            <a class="nav-link" >Special offer</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack/special_offer/lists" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/special_offer/lists'; ?>">All Special offer</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "smepack/special_offer/add" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/special_offer/add'; ?>">Create Special offer</a>
                        </li>
                    </li>
                    </ul>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#solution" aria-expanded="false" aria-controls="solution">
                    <span class="menu-title">Solution content</span>
                    <i class="menu-arrow"></i>
                    <i class="mdi mdi-apps menu-icon"></i>
                </a>
                <div class="collapse" id="solution">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item menu-nolink">
                            <a class="nav-link">Solution</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "solution/lists" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/solution/lists'; ?>">All solution</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "solution/add" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/solution/add'; ?>">Created solution</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "solution/category/lists" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/solution/category/lists'; ?>">Category</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "solution/brochure/edit" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/solution/brochure/edit/1'; ?>">Upload brochure</a>
                        </li>

                        <li class="nav-item menu-nolink">
                            <a class="nav-link">Business type</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "solution/business_type/lists" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/solution/business_type/lists'; ?>">All business type</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "solution/business_type/add" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/solution/business_type/add'; ?>">Created business type</a>
                        </li>

                        
                        <!-- <li class="nav-item menu-nolink">
                            <a class="nav-link">Video testimonial</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "XXX" ? 'active' : ''; ?>" href="#">All testimonial</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "XXX" ? 'active' : ''; ?>" href="#">Created testimonial</a>
                        </li> -->
                        
                    </ul>
                </div>
            </li>



            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#experience" aria-expanded="false" aria-controls="experience">
                    <span class="menu-title">Experience content</span>
                    <i class="menu-arrow"></i>
                    <i class="mdi mdi-apps menu-icon"></i>
                </a>
                <div class="collapse" id="experience">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item">
                        <a class="nav-link <?php echo $menu_active == "experience/lists" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/experience/lists'; ?>">All experience</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link <?php echo $menu_active == "experience/add" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/experience/add'; ?>">Created experience</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link <?php echo $menu_active == "experience/category/lists" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/experience/category/lists'; ?>">Category</a>
                        </li>

                    </ul>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#landing" aria-expanded="false" aria-controls="landing">
                    <span class="menu-title">Landing content</span>
                    <i class="menu-arrow"></i>
                    <i class="mdi mdi-apps menu-icon"></i>
                </a>
                <div class="collapse" id="landing">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "landing/lists" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/landing/lists'; ?>">All landing</a>
                        </li>
                        <li class="nav-item menu-sub">
                            <a class="nav-link <?php echo $menu_active == "landing/add" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/landing/add'; ?>">Created landing</a>
                        </li>

                    </ul>
                </div>
            </li>
            

            <!-- <?php if( $this->session->userdata('user_type') == 'SA' || $this->session->userdata('user_type') == 'A'){ ?>
                <li class="nav-item <?php echo $menu_active == "smepack-main-content" ? 'active' : ''; ?>">
                    <a class="nav-link" href="<?php echo base_url().'admin/smepack_category/main_content'; ?>" >
                        <span class="menu-title">SME Main content</span>
                    </a>
                </li>
            <?php } ?>

            <?php if( $this->session->userdata('user_type') == 'SA' || $this->session->userdata('user_type') == 'A'){ ?>
                <li class="nav-item <?php echo $menu_active == "smepack-mobile" ? 'active' : ''; ?>">
                    <a class="nav-link" data-toggle="collapse" href="#smepack-mobile-layouts" aria-expanded="false"
                        aria-controls="smepack-mobile-layouts">
                        <span class="menu-title">Mobile</span>
                        <i class="menu-arrow"></i>
                        <i class="mdi mdi-brightness-auto menu-icon"></i>
                    </a>
                    <div class="collapse" id="smepack-mobile-layouts">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack_category/content/1" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/content/1'; ?>">Main Content</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack/lists/1" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/lists/1'; ?>">All Package</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack/add/1" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/add/1'; ?>">Create Package</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack_category/lists/1" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/lists/1'; ?>">Category</a></li>
                        </ul>
                    </div>
                </li>
            <?php } ?>

            <?php if( $this->session->userdata('user_type') == 'SA' || $this->session->userdata('user_type') == 'A'){ ?>
                <li class="nav-item <?php echo $menu_active == "smepack-internet" ? 'active' : ''; ?>">
                    <a class="nav-link" data-toggle="collapse" href="#smepack-internet-layouts" aria-expanded="false"
                        aria-controls="smepack-internet-layouts">
                        <span class="menu-title">Internet</span>
                        <i class="menu-arrow"></i>
                        <i class="mdi mdi-brightness-auto menu-icon"></i>
                    </a>
                    <div class="collapse" id="smepack-internet-layouts">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack_category/content/2" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/content/2'; ?>">Main Content</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack/lists/2" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/lists/2'; ?>">All Package</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack/add/2" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/add/2'; ?>">Create Package</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack_category/lists/2" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/lists/2'; ?>">Category</a></li>
                        </ul>
                    </div>
                </li>
            <?php } ?>

            <?php if( $this->session->userdata('user_type') == 'SA' || $this->session->userdata('user_type') == 'A'){ ?>
                <li class="nav-item <?php echo $menu_active == "smepack-tv" ? 'active' : ''; ?>">
                    <a class="nav-link" data-toggle="collapse" href="#smepack-tv-layouts" aria-expanded="false"
                        aria-controls="smepack-tv-layouts">
                        <span class="menu-title">TV</span>
                        <i class="menu-arrow"></i>
                        <i class="mdi mdi-brightness-auto menu-icon"></i>
                    </a>
                    <div class="collapse" id="smepack-tv-layouts">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack_category/content/3" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/content/3'; ?>">Main Content</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack/lists/3" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/lists/3'; ?>">All Package</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack/add/3" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/add/3'; ?>">Create Package</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack_category/lists/3" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/lists/3'; ?>">Category</a></li>
                        </ul>
                    </div>
                </li>
            <?php } ?>

            <?php if( $this->session->userdata('user_type') == 'SA' || $this->session->userdata('user_type') == 'A'){ ?>
                <li class="nav-item <?php echo $menu_active == "smepack-fixedline" ? 'active' : ''; ?>">
                    <a class="nav-link" data-toggle="collapse" href="#smepack-fixedline-layouts" aria-expanded="false"
                        aria-controls="smepack-fixedline-layouts">
                        <span class="menu-title">Fixedline</span>
                        <i class="menu-arrow"></i>
                        <i class="mdi mdi-brightness-auto menu-icon"></i>
                    </a>
                    <div class="collapse" id="smepack-fixedline-layouts">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack_category/content/4" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/content/4'; ?>">Main Content</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack/lists/4" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/lists/4'; ?>">All Package</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack/add/4" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/add/4'; ?>">Create Package</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack_category/lists/4" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/lists/4'; ?>">Category</a></li>
                        </ul>
                    </div>
                </li>
            <?php } ?>

            <?php if( $this->session->userdata('user_type') == 'SA' || $this->session->userdata('user_type') == 'A'){ ?>
                <li class="nav-item <?php echo $menu_active == "smepack-bundle" ? 'active' : ''; ?>">
                    <a class="nav-link" data-toggle="collapse" href="#smepack-bundle-layouts" aria-expanded="false"
                        aria-controls="smepack-bundle-layouts">
                        <span class="menu-title">Bundle</span>
                        <i class="menu-arrow"></i>
                        <i class="mdi mdi-brightness-auto menu-icon"></i>
                    </a>
                    <div class="collapse" id="smepack-bundle-layouts">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack_category/content/5" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/content/5'; ?>">Main Content</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack/lists/5" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/lists/5'; ?>">All Package</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack/add/5" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/add/5'; ?>">Create Package</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack_category/lists/5" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack_category/lists/5'; ?>">Category</a></li>
                        </ul>
                    </div>
                </li>
            <?php } ?> 

            <?php if( $this->session->userdata('user_type') == 'SA' || $this->session->userdata('user_type') == 'A'){ ?>
                <li class="nav-item <?php echo $menu_active == "smepack-special-offer" ? 'active' : ''; ?>">
                    <a class="nav-link" data-toggle="collapse" href="#smepack-special-offer-layouts" aria-expanded="false"
                        aria-controls="smepack-special-offer-layouts">
                        <span class="menu-title">Special offer</span>
                        <i class="menu-arrow"></i>
                        <i class="mdi mdi-brightness-auto menu-icon"></i>
                    </a>
                    <div class="collapse" id="smepack-special-offer-layouts">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack/special_offer/lists" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/special_offer/lists'; ?>">All Special offer</a></li>
                            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "smepack/special_offer/add" ? 'active' : ''; ?>" href="<?php echo base_url().'admin/smepack/special_offer/add'; ?>">Create Special offer</a></li>
                        </ul>
                    </div>
                </li>
            <?php } ?>-->
            
            <?php  if( check_permission( $this->session->userdata('user_type'), 'pages') ){ ?>
                <div class="section-seperate "><span class="font-weight-bold mb-2">Page Management</span></div>
                <?php if( $this->session->userdata('user_type') == 'SA'){ ?>
                    <li class="nav-item <?php echo $menu_active == "admin/pages" ? 'active' : ''; ?>">
                        <a class="nav-link" href="<?php echo base_url().'admin/pages'; ?>">
                            <span class="menu-title">All page</span>
                            <i class="mdi mdi mdi-note-outline menu-icon"></i>
                        </a>
                    </li>
                <?php } ?>
                <?php foreach( $parent_pages as $parent ){ ?> 
                    <?php if( sizeof($parent['sub_pages']) == 0 ){ ?>
                    <?php if( hasPagePermission( $this->session->userdata('user_type'), $parent ) ){ ?>
                        <li class="nav-item <?php echo $menu_active == "section/allrow/".$parent['code'] ? 'active' : ''; ?>">
                            <a class="nav-link" href="<?php echo base_url().'admin/section/allrow/'.$parent['code']; ?>">
                                <span class="menu-title"><?php echo $parent['page_name']; ?></span>
                                <i class="mdi mdi-apps menu-icon"></i>
                            </a>
                        </li>
                    <?php } ?>
                    <?php }else{ ?>
                    <?php if( hasPagePermission( $this->session->userdata('user_type'), $parent ) ){ ?>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#page-<?php echo $parent['code']; ?>-layouts" aria-expanded="false" aria-controls="page-<?php echo $parent['code']; ?>-layouts">
                                <span class="menu-title"><?php echo $parent['page_name']; ?></span>
                                <i class="menu-arrow"></i>
                                <i class="mdi mdi-apps menu-icon"></i>
                            </a>
                            <div class="collapse" id="page-<?php echo $parent['code']; ?>-layouts">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item ">
                                        <a class="nav-link <?php echo $menu_active == "section/allrow/".$parent['code'] ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/allrow/'.$parent['code']; ?>"><?php echo $parent['page_name']; ?></a>
                                    </li>
                                    <?php /*
                                    <li class="nav-item menu-sub">
                                        <a class="nav-link <?php echo $menu_active == "section/allrow/".$parent['code'] ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/allrow/'.$parent['code']; ?>">All section</a>
                                    </li>
                                    <?php foreach( $section_list as $section ){ ?>
                                        <?php if($section['page_code'] == $parent['code']){ ?> 
                                            <li class="nav-item menu-sub">
                                                <a class="nav-link <?php echo $menu_active == "section/edit/".$section['page_code'].'/'.$section['code'] ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/add/'.$section['page_code'].'/'.$section['code']; ?>"><?php echo $section['section_name'] ?></a> 
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                    */ ?>
                                    <?php foreach( $parent['sub_pages'] as $sub ){ ?>
                                        <?php if( hasPagePermission( $this->session->userdata('user_type'), $sub ) ){ ?>
                                        <li class="nav-item ">
                                            <a class="nav-link <?php echo $menu_active == "section/allrow/".$sub['code'] ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/allrow/'.$sub['code']; ?>"><?php echo $sub['page_name']; ?></a>
                                        </li>
                                        <?php } ?>
                                    <?php /*
                                    <li class="nav-item menu-sub">
                                        <a class="nav-link <?php echo $menu_active == "section/allrow/".$sub['code'] ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/allrow/'.$sub['code']; ?>">All section</a>
                                    </li>
                                    <?php foreach( $section_list as $section ){ ?>
                                        <?php if($section['page_code'] == $sub['code']){ ?> 
                                            <li class="nav-item menu-sub">
                                                <a class="nav-link <?php echo $menu_active == "section/edit/".$section['page_code'].'/'.$section['code'] ? 'active' : ''; ?>" href="<?php echo base_url().'admin/section/add/'.$section['page_code'].'/'.$section['code']; ?>"><?php echo $section['section_name'] ?></a> 
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                    */ ?>
                                    <?php } ?>

                                    
                                </ul>
                            </div>
                        </li>
                        <?php } ?>
                    <?php } ?>
                
                <?php } ?>
            <?php } ?>
        
        




<?php /* // collapse menu 3 level : Not working
      <li class="nav-group-title nav-item">
        <a class="nav-link" data-toggle="collapse" href="#xr-layouts" aria-expanded="false" aria-controls="xr-layouts">
           <span class="menu-title">True XR</span>
           <i class="menu-arrow"></i>
           <i class="mdi mdi-image-area menu-icon"></i>
         </a>
         <div class="collapse" id="xr-layouts">
           <ul class="nav flex-column sub-menu">
             <li class="nav-item"> 
               <a class="nav-link" data-toggle="collapse" href="#xr1-layouts" aria-expanded="false" aria-controls="xr1-layouts">
                <span class="menu-title">Section 1</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-image-area menu-icon"></i>
              </a>
              <div class="collapse" id="xr1-layouts">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "banner" ? 'active' : ''; ?>"
href="<?php echo base_url().'admin/banner/lists'; ?>">Category</a></li>
<li class="nav-item"> <a class="nav-link <?php echo $menu_active == "category/lists/banner" ? 'active' : ''; ?>"
        href="<?php echo base_url().'admin/category/lists/banner'; ?>">List</a></li>
</ul>
</div>
</li>
</ul>
</div>
</li>
*/ ?>

<?php /*
      <div class="box-content-button d-inline-block text-left">
        <a class="btn text-center" href="" style="color:#f10000; background-color:#fff">Text</a>
      </div>
      <div class="box-content-button d-inline-block text-left">
        <a class="btn d-inline-block text-center" href="" style="color:#f10000; background-color:#fff">Text</a>
      </div>
      
      <li class="nav-item <?php echo $menu_active == "banner" ? 'active' : ''; ?>">
<a class="nav-link" href="<?php echo base_url().'admin/banner/lists'; ?>">
    <span class="menu-title">Home Banner</span>
    <i class="mdi mdi-image-area menu-icon"></i>
</a>
</li>
*/ ?>

<?php /*
      <li class="nav-item">
         <a class="nav-link" data-toggle="collapse" href="#banner-layouts" aria-expanded="false" aria-controls="banner-layouts">
           <span class="menu-title">Banner</span>
           <i class="menu-arrow"></i>
           <i class="mdi mdi-image-area menu-icon"></i>
         </a>
         <div class="collapse" id="banner-layouts">
           <ul class="nav flex-column sub-menu">
             <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "banner" ? 'active' : ''; ?>"
href="<?php echo base_url().'admin/banner/lists'; ?>">Banner</a></li>
<li class="nav-item"> <a class="nav-link <?php echo $menu_active == "category/lists/banner" ? 'active' : ''; ?>"
        href="<?php echo base_url().'admin/category/lists/banner'; ?>">Category</a></li>
</ul>
</div>
</li>
*/ ?>

<?php /*
       <li class="nav-item">
          <a class="nav-link" data-toggle="collapse" href="#pages-layouts" aria-expanded="false" aria-controls="pages-layouts">
            <span class="menu-title">Pages</span>
            <i class="menu-arrow"></i>
            <i class="mdi mdi-book-open-page-variant menu-icon"></i>
          </a>
          <div class="collapse" id="pages-layouts">
            <ul class="nav flex-column sub-menu">
              <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "admin/pages" ? 'active' : ''; ?>"
href="<?php echo base_url().'admin/pages/'; ?>">All page</a></li>
<li class="nav-item"> <a class="nav-link <?php echo $menu_active == "admin/pages/add" ? 'active' : ''; ?>"
        href="<?php echo base_url().'admin/pages/add'; ?>">Create page</a></li>
</ul>
</div>
</li>

<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#content-layouts" aria-expanded="false"
        aria-controls="content-layouts">
        <span class="menu-title">Content</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-clipboard-text menu-icon"></i>
    </a>
    <div class="collapse" id="content-layouts">
        <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "content" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/content/'; ?>">All content</a></li>
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "content/add" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/content/add'; ?>">Create content</a></li>
            <li class="nav-item"> <a
                    class="nav-link <?php echo $menu_active == "category/lists/content" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/category/lists/content'; ?>">Category</a></li>
        </ul>
    </div>
</li>
<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#product-layouts" aria-expanded="false"
        aria-controls="product-layouts">
        <span class="menu-title">Product</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-package-variant menu-icon"></i>
    </a>
    <div class="collapse" id="product-layouts">
        <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "product" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/product/'; ?>">All product</a></li>
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "product/add" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/product/add'; ?>">Create product</a></li>
            <li class="nav-item"> <a
                    class="nav-link <?php echo $menu_active == "category/lists/product" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/category/lists/product'; ?>">Category</a></li>
        </ul>
    </div>
</li>
<?php /*<li class="nav-item <?php echo $menu_active == "brand" ? 'active' : ''; ?>">
<a class="nav-link" href="<?php echo base_url().'admin/brand'; ?>">
    <span class="menu-title">Brand</span>
    <i class="mdi mdi-shopping menu-icon"></i>4
</a>
</li>

<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#gallery-layouts" aria-expanded="false"
        aria-controls="gallery-layouts">
        <span class="menu-title">Gallery</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-folder-multiple-image menu-icon"></i>
    </a>
    <div class="collapse" id="gallery-layouts">
        <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "gallery" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/gallery/'; ?>">All gallery</a></li>
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "gallery/add" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/gallery/add'; ?>">Create gallery</a></li>
            <li class="nav-item"> <a
                    class="nav-link <?php echo $menu_active == "category/lists/gallery" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/category/lists/gallery'; ?>">Category</a></li>
        </ul>
    </div>
</li>

<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#about-layouts" aria-expanded="false"
        aria-controls="about-layouts">
        <span class="menu-title">About</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-menu menu-icon"></i>
    </a>
    <div class="collapse" id="about-layouts">
        <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "history" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/about/history'; ?>">History</a></li>
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "vision" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/about/vision'; ?>">Vision</a></li>
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "structure" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/about/structure'; ?>">Structure</a></li>
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "award" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/award'; ?>">Award</a></li>
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "csv" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/about/csv'; ?>">CSV (Partner)</a></li>
        </ul>
    </div>
</li>
<li class="nav-item <?php echo $menu_active == "faq" ? 'active' : ''; ?>">
    <a class="nav-link" href="<?php echo base_url().'admin/faq'; ?>">
        <span class="menu-title">FAQ</span>
        <i class="mdi mdi-comment-question-outline menu-icon"></i>
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#career-layouts" aria-expanded="false"
        aria-controls="career-layouts">
        <span class="menu-title">Career</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-account-multiple-plus menu-icon"></i>
    </a>
    <div class="collapse" id="career-layouts">
        <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "career_add_on" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/career'; ?>">Career Add on</a></li>
            <?php if( $this->session->userdata('user_type') == 'SA' || $this->session->userdata('user_type') == 'A'){ ?>
            <li class="nav-item"> <a
                    class="nav-link <?php echo $menu_active == "category/lists/position" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/category/lists/position'; ?>">Position Group</a></li>
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "position" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/career/position'; ?>">Position List</a></li>
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "career_submit" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/career/submit_list'; ?>">Career Submit</a></li>
            <?php } ?>
        </ul>
    </div>
</li>


<?php if( $this->session->userdata('user_type') == 'SA' || $this->session->userdata('user_type') == 'A'){ ?>
<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#contact-layouts" aria-expanded="false"
        aria-controls="contact-layouts">
        <span class="menu-title">Contact</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-phone menu-icon"></i>
    </a>
    <div class="collapse" id="contact-layouts">
        <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "contact_list" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/contact/lists'; ?>">Contact List</a></li>

        </ul>
    </div>
</li>
<?php } ?>
<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#addon-layouts" aria-expanded="false"
        aria-controls="addon-layouts">
        <span class="menu-title">Add ON</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-book-open-page-variant menu-icon"></i>
    </a>
    <div class="collapse" id="addon-layouts">
        <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "addon/home" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/addon/home'; ?>">Home</a></li>
            <li class="nav-item"> <a class="nav-link <?php echo $menu_active == "addon/about" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/addon/about'; ?>">About</a></li>
        </ul>
    </div>
</li>
*/ ?>
<?php /*?><?php if( $this->session->userdata('user_type') == "SA" ){ ?>
<li class="nav-item <?php echo $menu_active == "user" ? 'active' : ''; ?>">
    <a class="nav-link" href="<?php echo base_url().'admin/user/lists'; ?>">
        <span class="menu-title">Users</span>
        <i class="mdi mdi-account-settings-variant menu-icon"></i>
    </a>
</li>
<?php } ?><?php */?>


<?php /* foreach( $cats as $cat ){ ?>

<?php if($cat['cate_for'] == 'contact'){?>
<li class="nav-item <?php echo $menu_active == "contactinfo/edit/1" ? 'active' : ''; ?>">
    <a class="nav-link" href="<?php echo base_url().'admin/contactinfo/edit/1'; ?>">
        <span class="menu-title"><?php echo $cat['name']; ?></span>
        <i class="mdi mdi-email menu-icon"></i>
    </a>
</li>

<?php }else{ ?>

<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#link-<?php echo $cat['code']; ?>" aria-expanded="false"
        aria-controls="page-layouts">
        <span class="menu-title"><?php echo $cat['name']; ?></span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-apps menu-icon"></i>
    </a>
    <div class="collapse" id="link-<?php echo $cat['code']; ?>">
        <ul class="nav flex-column sub-menu">
            <?php foreach( $menus as $menu ){ ?>
            <?php if( $menu['cate_code'] == $cat['code'] ){ ?>
            <?php if( $menu['is_parent'] == 0 ){ ?>
            <?php if( $menu['code'] == 45){ ?>
            <li class="nav-item"><a class="nav-link <?php echo $menu_active == "contactinfo/edit/2" ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/contactinfo/edit/2'; ?>"><?php echo $menu['title']; ?></a></li>
            <?php }else{?>
            <li class="nav-item">
                <?php if( $menu['page_type'] == "page"){ ?>
                <a class="nav-link <?php echo $menu_active == 'pages/edit/'.$menu['code'] ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/pages/edit/'.$menu['code']; ?>">
                    <?php }else{ ?>
                    <a class="nav-link <?php echo $menu_active == 'pages/lists/'.$menu['code'] ? 'active' : ''; ?>"
                        href="<?php echo base_url().'admin/pages/lists/'.$menu['code']; ?>">
                        <?php } ?>
                        <span class="menu-title"><?php echo $menu['title']; ?></span>
                    </a>
            </li>
            <?php } ?>
            <?php }else{ ?>
            <li class="nav-item menu-nolink"> <a class="nav-link"><?php echo $menu['title']; ?></a></li>
            <?php foreach( $sub_menus as $sub ){ ?>
            <?php if( $sub['parent_code'] == $menu['code'] ){ ?>
            <?php if( $sub['page_type'] == "page"){ ?>
            <li class="nav-item menu-sub"><a
                    class="nav-link  <?php echo $menu_active == 'pages/edit/'.$sub['code'] ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/pages/edit/'.$sub['code']; ?>"><?php echo $sub['title']; ?></a>
            </li>
            <?php }else{ ?>
            <li class="nav-item menu-sub"><a
                    class="nav-link  <?php echo $menu_active == 'pages/lists/'.$sub['code'] ? 'active' : ''; ?>"
                    href="<?php echo base_url().'admin/pages/lists/'.$sub['code']; ?>"><?php echo $sub['title']; ?></a>
            </li>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
</li>
<?php } ?>
<?php } */ ?>
<?php /* if( $this->session->userdata('user_type') == "SA" || $this->session->userdata('user_type') == "A" ){ ?>
<li class="nav-item sidebar-actions">
    <span class="nav-link">
        <a href="<?php echo base_url().'admin/pages/add' ?>" class="btn btn-gradient-dark btn-fw w-100">+ Add page</a>
    </span>
</li>
<?php } */ ?>
</ul>
</nav>
<!-- partial -->
<div class="main-panel">