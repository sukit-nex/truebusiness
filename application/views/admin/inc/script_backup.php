<script>
  var base_url = '<?php echo base_url(); ?>';
  var assets_path = '<?php echo REAL_PATH; ?>assets/admin/';
  var upload_path = '<?php echo upload_path(); ?>';
  var real_path = '<?php echo REAL_PATH; ?>';
</script>
<!-- plugins:js -->

<script src="<?php echo ADMIN_ASSET; ?>vendors/js/vendor.bundle.base.js?v=<?php echo rand(1,999); ?>"></script>
<script src="<?php echo ADMIN_ASSET; ?>vendors/js/vendor.bundle.addons.js?v=<?php echo rand(1,999); ?>"></script>

<?php if( isset( $menu) ){ ?>
  <?php if( $menu == "gall" ){ ?>
    <script src="<?php echo ADMIN_ASSET; ?>js/dropzoneinit.min.js"></script>
  <?php } ?>
<?php } ?>
<!-- endplugins -->
<!-- Custom js -->


<script src="<?php echo ADMIN_ASSET; ?>vendors/lightgallery/js/lightgallery-all.min.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>vendors/tinymce/tinymce.min.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>vendors/tinymce/themes/modern/theme.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>vendors/summernote/dist/summernote-bs4.js"></script>
<script src="<?php echo ADMIN_ASSET; ?>js/bundle.min.js?v=<?php echo rand(1,999); ?>"></script>


<?php if( isset( $is_section ) ){ ?>
    <script src="<?php echo ADMIN_ASSET; ?>js/section_drag.min.js"></script>
<?php } ?>
<?php /* <script src="<?php echo ADMIN_ASSET; ?>js/nexitor-class.js"></script> */ ?>
<script src="<?php echo ADMIN_ASSET; ?>js/bootstrap.bundle.min.js"></script>


<!-- include codemirror (codemirror.css, codemirror.js, xml.js, formatting.js) -->
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js"></script>

<script src="<?php echo ADMIN_ASSET; ?>vendors/ace-builds/src-min/ace.js"></script>

<script src="<?php echo ADMIN_ASSET; ?>js/form-repeater.js"></script>

<?php if( $this->session->userdata('alert') == '1' ){ ?>

<script>
    popup_message('<?php echo $this->session->userdata('alert_type'); ?>' , '<?php echo $this->session->userdata('alert_msg'); ?>', '<?php echo $this->session->userdata('alert_title'); ?>');
</script>
<?php $this->session->set_userdata('alert', ''); ?>
<?php } ?>
<script>
$(document).ready( function(){
    console.log(12);
    
        if ($(".tinyMceExample").length) {
            tinymce.init({
            selector: ".tinyMceExample",
            theme: "modern",
            paste_data_images: true,
            plugins: [
              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars code fullscreen",
              "insertdatetime media nonbreaking save table contextmenu directionality",
              "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor emoticons",
            image_advtab: true,
            file_picker_callback: function(callback, value, meta) {
              if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                  var file = this.files[0];
                  var reader = new FileReader();
                  reader.onload = function(e) {
                    callback(e.target.result, {
                      alt: ''
                    });
                  };
                  reader.readAsDataURL(file);
                });
              }
            },
            templates: [{
              title: 'Test template 1',
              content: 'Test 1'
            }, {
              title: 'Test template 2',
              content: 'Test 2'
            }]
            });
        }
    });
</script>

<script>
jQuery( document ).ready(function($) {
  $(".drag-item-list").sortable();
});
</script>
<script>
jQuery( document ).ready(function($) {
    $(".only-one input:checkbox").on('click', function() {
    // in the handler, 'this' refers to the box clicked on
    var $box = $(this);
    if ($box.is(":checked")) {
        // the name of the box is retrieved using the .attr() method
        // as it is assumed and expected to be immutable
        var group = "input:checkbox[name='" + $box.attr("name") + "']";
        // the checked state of the group/box on the other hand will change
        // and the current value is retrieved using .prop() method
        $(group).prop("checked", false);
        $box.prop("checked", true);
    } else {
        $box.prop("checked", false);
    }
    });
});
</script>

</body>

</html>

