<div class="card mb-3">
  <div class="card-body">

    <div class="form-group mb-0">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="box-radio box-check-gray" onclick="" data-cate-for="">
            <div class="form-check form-check-dark">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="banner_type" onclick="$('.has-banner').hide(); $('.single-banner').hide();" value="0"  <?php echo $banner_type == 0 ? 'checked' : ''; ?>>
                Non - Banner
              </label>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="box-radio box-check-gray" onclick="" data-cate-for="">
            <div class="form-check form-check-dark">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="banner_type" value="1" onclick="$('.single-banner').show(); $('.has-banner').hide();"  <?php echo $banner_type == 1 ? 'checked' : ''; ?>>
                Single - Banner
              </label>
            </div>
          </div>
        </div>
        <?php /* <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="box-radio box-check-gray" onclick="" data-cate-for="">
            <div class="form-check form-check-dark">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="banner_type" value="2" onclick="$('.has-banner').show(); $('.single-banner').hide();"  <?php echo $banner_type == 2 ? 'checked' : ''; ?>>
                Slide - Banner
              </label>
            </div>
          </div>
        </div> */ ?>
      </div>
    </div>
    <div class="form-group has-banner mt-4 mb-0" style="display:<?php echo  $banner_type == 2 ? 'block' : 'none';?>;">
      <label for="bannerCate">Banner Category</label>
      <select class="js-example-basic-single form-control" id="bannerCate" name="banner_cate" style="width:100%">
        <?php foreach( $banner_cates as $banner_cate_set ){ ?>
          <option value="<?php echo $banner_cate_set['code']; ?>" <?php echo $banner_cate_set['code'] == $banner_cate ? 'selected' : '';  ?>><?php echo $banner_cate_set['name']; ?></option>
        <?php } ?>
      </select>
    </div>

    <div class="form-group single-banner mt-4 mb-0" style="display:<?php echo  $banner_type == 1 ? 'block' : 'none';?>;">
      <div class="row">
        <div class="col-8">
          <label>Desktop Image <small class="text-muted">( 1920px x 546px )</small></label>
          <input type="file" data-height="250" name="image" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $image != "" ? upload_path($image) : ''; ?>" />
        </div>
        <div class="col-4">
          <label>Mobile Image <small class="text-muted">( 750px x 710px )</small></label>
          <input type="file" data-height="250" name="image_m" class="dropify" data-max-file-size="3000kb" data-default-file="<?php echo $image_m != "" ? upload_path($image_m) : ''; ?>" />
        </div>
      </div>
    </div>

  </div>
</div>
