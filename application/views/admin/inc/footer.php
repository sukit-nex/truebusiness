<!-- partial:partials/_footer.html -->
<footer class="footer">
  <div class="d-sm-flex justify-content-center justify-content-sm-between">
    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">©True Corporation Public Company Limited All rights reserved.</span>
    
  </div>
</footer>
<!-- partial -->
</div>
<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="deleteModal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete item ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body cate-message" style="display:none;">
        <p class="modal-title" id="exampleModalLabel">Warning : This category will be gone from all section</p>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-gradient-dark modal-delete-confirm" data-table="" data-delete-by="" data-code="" id="modal-btn-si">Yes</button>
        <button type="button" data-dismiss="modal" class="btn btn-danger" id="modal-btn-no">No</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="repeater_deleteModal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Are you sure to delete item ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body cate-message" style="display:none;">
        <p class="modal-title" id="exampleModalLabel">Warning : This category will be gone from all section</p>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-gradient-dark repeater-modal-delete-confirm" data-row="" id="modal-btn-si">Yes</button>
        <button type="button" data-dismiss="modal" class="btn btn-danger" id="modal-btn-no">No</button>
      </div>
    </div>
  </div>
</div>
<?php /* <input name="image" type="file" id="upload-img-tiny" class="hidden" onchange="" style="opacity:0; position:absolute; z-index:-20;"> */ ?>