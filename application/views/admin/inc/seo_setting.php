<?php
if( sizeof( $seo ) > 0 ){
  $slug = $seo[0]['slug'];
  $meta_title = $seo[0]['meta_title'];
  $meta_desc = $seo[0]['meta_desc'];
  $meta_keyword = $seo[0]['meta_keyword'];
  $share_img = $seo[0]['share_img'];
}else{
  $slug = time();
  $meta_title = '';
  $meta_desc = '';
  $meta_keyword = '';
  $share_img = '';
}

?>
<div class="card mb-3">
    <div class="card-body">
        <h3 class="page-title mb-3"> SEO Page setting </h3>
        <div class="top-right-switch seo-switch">
            <input type="checkbox" <?php echo $seo_status == 1 ? 'checked' : '' ?> data-toggle="toggle"
                data-onstyle="info" data-offstyle="outline-secondary" data-size="xs" name="seo_status_switch">
            <input type="hidden" name="seo_status" value="<?php echo $seo_status; ?>">
        </div>
        <div class="seo-toggle-wrap" style="<?php echo $seo_status == 0 ? 'display:none;' : '' ?>">
            <?php if( $menu_active != 'setting' ){ ?>
            <div class="form-group">
                <label for="">Slug <span class="text-muted">( * Only character and number split with - )</span></label>
                <input type="text" name="slug" class="form-control" placeholder="Ex : domain.com/your-friendly-slug"
                    value="<?php echo $slug; ?>">
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="">META : Title </label>
                <input type="text" name="meta_title" class="form-control" placeholder="Your meta title"
                    value="<?php echo $meta_title; ?>">
            </div>
            <div class="form-group">
                <label for="">META : Description </label>
                <input type="text" name="meta_desc" class="form-control" placeholder="Your meta description"
                    value="<?php echo $meta_desc; ?>">
            </div>
            <div class="form-group">
                <label for="">META : Keyword</label>
                <input name="meta_keyword" id="meta_keyword" value="<?php echo $meta_keyword; ?>" />
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-12">
                        <label>Share Image <small class="text-muted">( 1200 x 630px )</small></label>
                        <input type="file" data-height="250" name="share_img" class="dropify"
                            data-max-file-size="3000kb"
                            data-default-file="<?php echo $share_img != "" ? upload_path($share_img) : ''; ?>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>