<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>System Admin</title>

  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>vendors/iconfonts/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>vendors/summernote/dist/summernote-bs4.css">
  <?php if( isset( $menu) ){ ?>
    <?php if( $menu == "gall" ){ ?>
      <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>css/dropzone.css">
    <?php } ?>
  <?php } ?>
  <!-- End plugin css for this page -->
  <link href="https://fonts.googleapis.com/css?family=Kanit:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>css/jquery-ui.min.css">
  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>css/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>css/bootstrap-tagsinput.css">
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>vendors/css/vendor.bundle.addons.css">

  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>css/bundle.min.css">


  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>css/nexitor.css">
  
  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>css/style.css?v=<?php echo rand(1,999); ?>">


  <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>css/custom.css?v=<?php echo rand(1,999); ?>">

  <link rel="stylesheet" href="<?php echo FRONT_ASSET; ?>fonts/fontface.css">


  <link rel="shortcut icon" href="<?php echo FRONT_ASSET; ?>img/skin/favicon.png" type="image/x-icon"/>
</head>
<body>
