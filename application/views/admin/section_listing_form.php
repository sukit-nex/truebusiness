<?php
  if( sizeof( $detail ) > 0 ){
	$code = $detail[DEFAULT_LANG]['code'];
	$cate_code = $detail[DEFAULT_LANG]['cate_code'];
    $page = $detail[DEFAULT_LANG]['page'];
	$sec_key = $detail[DEFAULT_LANG]['sec_key'];
	
	$image = $detail[DEFAULT_LANG]['image'];
	$image2 = $detail[DEFAULT_LANG]['image2'];
	$post_date = $detail[DEFAULT_LANG]['post_date'];
	$post_time = $detail[DEFAULT_LANG]['post_time'];
	$sticky = $detail[DEFAULT_LANG]['sticky'];
    $hero_banner = $detail[DEFAULT_LANG]['hero_banner'];
    $hero_banner_m = $detail[DEFAULT_LANG]['hero_banner_m'];
    //$bg1_mobile = $detail[DEFAULT_LANG]['bg1_mobile'];

    //$img1_desktop = $detail[DEFAULT_LANG]['img1_desktop'];
	//$img1_mobile = $detail[DEFAULT_LANG]['img1_mobile'];
    $price = $detail[DEFAULT_LANG]['price'];
	$onoff = $detail[DEFAULT_LANG]['onoff'];
    $link_out = $detail[DEFAULT_LANG]['link_out'];
    $link_type = $detail[DEFAULT_LANG]['link_type'];
    $btn_show = $detail[DEFAULT_LANG]['btn_show'];
    $slug = $detail[DEFAULT_LANG]['slug'];
    $tags = $detail[DEFAULT_LANG]['tags'];
    $is_highlight = $detail[DEFAULT_LANG]['is_highlight'];

    $approve_status = $detail[DEFAULT_LANG]['approve_status'];

    $published_date = $detail[DEFAULT_LANG]['published_date'];
    if ( $published_date != '' ) {
        $p_date = substr($published_date, 0, 10);
        $p_date = explode('-', $p_date);
        $p_date = ($p_date[0]+543).'-'.$p_date[1].'-'.$p_date[2];
        $p_time = substr($published_date, 11, 8);
    } else {
        $p_date = '';
        $p_time = '';
    }
    
  }else{
	$code = '';
	$cate_code = $cate_detail[DEFAULT_LANG]['code'];
    $page = $param_page;
	$sec_key = $param_sec_key;
	$image = '';
	$image2 = '';
	$post_date = '';
	$post_time = '';
	$sticky = 0;
    $hero_banner ='';
    $hero_banner_m ='';
    $btn_text='';
    $price = '';
    $btn_show = 0;
    $link_out ='';
    $link_type ='auto';
    //$bg1_mobile = '';

    //$img1_desktop = '';
	//$img1_mobile = '';

	$onoff = 0;
    $slug = time();
    $tags = '';
    $is_highlight = '0';
    $approve_status = 0;
    $p_date = '';
    $p_time = '';
    
  }
  if( sizeof( $seo ) > 0 ){
    $share_img = $seo[DEFAULT_LANG]['share_img'];
  }else{
    $share_img = '';
  }
//echo 'must_be_approve='.$cate_detail[DEFAULT_LANG]['must_be_approve'];
?>
<div class="content-wrapper">

    <div class="page-header">
        <h3 class="page-title">
            <?php echo strtoupper( 'Listing item form of '.$cate_detail[DEFAULT_LANG]['title'].' category') ?> </h3>
        <a href="<?php echo base_url().'admin/section/listing/lists/'.$sec_detail[DEFAULT_LANG]['code'].'/'.$cate_code; ?>"
            class="btn btn-gradient-dark btn-fw">
            <- Back</a>
                <?php /* <a href="<?php echo base_url().'admin/banner/add' ?>" class="btn btn-gradient-dark btn-fw">+
                Add
                more
        </a> */ ?>
    </div>
    <?php echo form_open_multipart('admin/section/saveListingForm', ' class="mainForm listingForm"'); ?>
    <input type="hidden" name="code" value="<?php echo $code; ?>">
    <input type="hidden" name="page" value="<?php echo $page; ?>">
    <input type="hidden" name="sec_key" value="<?php echo $sec_key; ?>">
    <input type="hidden" name="sec_code" value="<?php echo $sec_detail[DEFAULT_LANG]['code'] ?>" >
 
    <input type="hidden" name="time" value="<?php echo $time; ?>">

    <input type="hidden" name="preview" value="<?php echo time(); ?>">
    <input type="hidden" name="is_preview" value="0">

    <input type="hidden" name="is_approve" value="0">

    <?php if ( (isApprover($this->session->userdata('user_type')) or $cate_detail[DEFAULT_LANG]['must_be_approve'] == '0') and $code == '' ) { ?>
    <input type="hidden" name="approve_status" value="2">
    <?php } else { ?>
    <input type="hidden" name="approve_status" value="<?php echo $approve_status; ?>">
    <?php } ?>

    <?php //include_once("inc/banner_content.php"); ?>
    <?php if( $param_sec_key == "seetheunseen" || $param_sec_key == "touchthebeyond" || $param_sec_key == "vrproduct" ){ ?>
    <?php }else { ?>
        <div class="card mb-3">
            <div class="card-body">
            <div class="form-group listing hl-content" style="display:block;">
                <div class="form-group-choice">
                    <div class="row d-block d-md-flex align-items-center">
                        <!-- <div class="col col-12 col-md-2"><label>Highlight Content</label></div> -->
                        <div class="col col-12 col-md-12">
                            <div class="fg-choice-group">
                                <ul class="alignment-position">
                                    <li class="alignment-position-item">
                                        <input type="radio" id="hl_set_none" name="is_highlight" value="0"
                                        onclick="$('.des-hl').hide(); "    checked>
                                        <label for="hl_set_none">
                                            <span>Default</span>
                                        </label>
                                    </li>
                                    <li class="alignment-position-item">
                                        <input type="radio" id="hl_set_show" name="is_highlight" value="1"
                                        onclick="$('.des-hl').show(); " 
                                            <?php  echo $is_highlight == '1' ? 'checked' : ''; ?>>
                                        <label for="hl_set_show">
                                            <span>Highlight</span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
                <ul class="nav nav-tabs" role="tablist" style="">
                    <?php foreach( $langs as $lang ){ ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                            id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
                            href="#<?php echo 'title_'.$lang['text']; ?>" role="tab"
                            aria-controls="<?php echo 'title_'.$lang['text']; ?>"
                            aria-selected="true"><?php echo $lang['text']; ?></a>
                    </li>
                    <?php } ?>
                </ul>
                <div class="tab-content float-left w-100" style="">
                    <?php foreach( $langs as $lang ){ ?>
                    <?php
					if( isset( $detail[ $lang['text'] ] ) ){
						$title = $detail[ $lang['text'] ]['title'];
              			$sub_title = $detail[ $lang['text'] ]['sub_title'];
                        $price = $detail[ $lang['text'] ]['price'];
                        $description = $detail[ $lang['text'] ]['description'];
					}else{
						$title = '';
              			$sub_title = '';
                        $price = '';
                        $description = '';
					}
					?>
                    <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                        id="<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
                        aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
                        <div class="row">
                            <div class=" <?php echo $sec_detail[DEFAULT_LANG]['design_type'] != 'accordion' ? 'col-6' : 'col-12' ?>">
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" name="title[]" class="form-control" placeholder="Title"
                                        value="<?php echo $title; ?>">
                                </div>
                            </div>
                            <?php  if($sec_detail[DEFAULT_LANG]['design_type'] != 'accordion' ){ ?>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">Sub Title</label>
                                    <input type="text" name="sub_title[]" class="form-control" placeholder="Sub-Title"
                                        value="<?php echo $sub_title; ?>"
                                        maxlength="<?php echo $param_sec_key == 'calendar' ? '100' : ''; ?>">
                                </div>
                            </div>
                            <?php  } ?>
                           
                            
                            <?php  if($sec_detail[DEFAULT_LANG]['design_type'] != 'accordion' ){ ?>
                                <div class="col-12 mt-3 des-hl" style="<?php echo  $is_highlight == '0' ||  $is_highlight == '' ? 'display:none;' : 'display:block;' ?>">
                                    <div class="form-group">
                                        <label for="">Description</label>
                                        <textarea name="description[]" class="form-control" rows="7"><?php echo $description; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-12 mt-3">
                                    <div class="form-group">
                                        <label for="">Price</label>
                                        <input type="text" name="price[]" class="form-control" placeholder="Price"
                                            value="<?php echo $price; ?>">
                                    </div>
                                </div>
                            <?php  } ?>
                        </div>

                        <?php // if($param_sec_key != "vrpartner"){ ?>

                        <?php //} ?>
                    </div>
                    <?php } ?>

                </div>
            </div>
            <div class="form-group">
                <label for="">Slug</label>
                <input type="text" name="slug" class="form-control" placeholder="Slug"
                    value="<?php echo $slug; ?>">
            </div>
        </div>
    </div>
    <?php } ?>
    <?php  if($sec_detail[DEFAULT_LANG]['design_type'] != 'accordion' ){ ?>
    <div class="card mb-3">
        <div class="card-body">
            <h4 class="card-title">Button Setting</h4>
            <div class="form-group">
                <div class="">
                
                    <div class="form-group-choice mb-3">
                        <div class="row d-block d-md-flex align-items-center">
                            <div class="col col-12 col-md-2 "><label>Link Type</label></div>
                            <div class="col col-12 col-md-10">
                                <div class="fg-choice-group">
                                    <ul class="alignment-position">
                                        <li class="alignment-position-item">
                                            <input type="radio" id="link_type_auto" name="link_type" value="auto" checked>
                                            <label for="link_type_auto" onclick="$('.blank').hide(); $('.status_btn').show(); $('.link_out').hide(); if( $('input[name=btn_show]:checked').val() == 1 ){ $('.button-text').show(); }else{ $('.button-text').hide(); }" >
                                                <!-- <img class="svg" src="/assets/admin/images/element/section/content-layout-06.svg" alt=""> -->
                                                <span>Auto</span>
                                            </label>
                                        </li>
                                        <li class="alignment-position-item">
                                            <input type="radio" id="link_type_no" name="link_type" value="no" <?php echo $link_type == 'no' ? 'checked' : ''; ?>>
                                            <label for="link_type_no" onclick="$('.blank').hide(); $('.status_btn').hide();">
                                                <!-- <img class="svg" src="/assets/admin/images/element/section/content-layout-05.svg"  alt=""> -->
                                                <span>No Link</span>
                                            </label>
                                        </li>
                                        <li class="alignment-position-item">
                                            <input type="radio" id="link_type_blank" name="link_type" value="blank"
                                                <?php echo $link_type == 'blank' ? 'checked' : ''; ?>>
                                            <label for="link_type_blank" onclick="$('.blank').show(); $('.status_btn').show(); $('.link-out').show(); if( $('input[name=btn_show]:checked').val() == 0 ){ $('.button-text').hide(); }else{$('.button-text').show();}">
                                                <!-- <img class="svg" src="/assets/admin/images/element/section/content-layout-06.svg" alt=""> -->
                                                <span>External Link</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-choice mb-3  status_btn" style="display:<?php echo $link_type != 'no' ? 'block' : 'none';?>;">
                        <div class="row d-block d-md-flex align-items-center">
                            <div class="col col-12 col-md-2"><label>Show Button</label></div>
                            <div class="col col-12 col-md-10">
                                <div class="fg-choice-group">
                                    <ul class="alignment-position">

                                        <li class="alignment-position-item">
                                            <input type="radio" id="btn_show_0" name="btn_show" value="0" onclick="$('.button-text').hide();" <?php echo $btn_show == '0' ? 'checked' : ''; ?>>
                                            <label for="btn_show_0">
                                                <!-- <img class="svg" src="/assets/admin/images/element/section/content-layout-05.svg"  alt=""> -->
                                                <span>Hidden Button</span>
                                            </label>
                                        </li>
                                        <li class="alignment-position-item">
                                            <input type="radio" id="btn_show_1" name="btn_show" value="1" <?php echo $btn_show == '1' ? 'checked' : ''; ?>   onclick="$('.button-text').show(); " >
                                            <label for="btn_show_1">
                                                <!-- <img class="svg" src="/assets/admin/images/element/section/content-layout-06.svg" alt=""> -->
                                                <span>Show Button</span>
                                            </label>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php //if($btn_show == '1' ){ echo 'block;';}else if($btn_show == ' ' ){ echo 'none;';}else if($btn_show != '1' ){ echo 'none;';}?>
                        <div class="row blank link-out" style="display:<?php echo $link_type == 'auto' || $link_type == 'no' || $link_type == 'blank' || $btn_show == '0' ? 'none;': 'block;'?>">
                            <div class="col-12">
                                <div class="form-group">

                                    <label for="">Link Out</label>
                                    <input type="text" name="link_out" class="form-control" placeholder="Button Text"
                                        value="<?php echo $link_out; ?>">
                                </div>
                            </div>
                        </div>


                        <!--  -->
                    
                </div>
            </div>

            <?php //if($btn_show == '1' ){ echo 'block;';}else if($btn_show == ' ' ){ echo 'none;';}else if($btn_show != '1' ){ echo 'none;';}else if($btn_show =="0"){echo 'none';}?>
            <div class="form-group button-text status_btn float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>" style="display:<?php echo  $link_type != 'no' && $btn_show != 0  ? 'block;' : 'none;' ?>">
                <ul class="nav nav-tabs" role="tablist" style="">
                    <?php foreach( $langs as $lang ){ ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                            id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
                            href="#<?php echo 'title_'.$lang['text']; ?>" role="tab"
                            aria-controls="<?php echo 'title_'.$lang['text']; ?>"
                            aria-selected="true"><?php echo $lang['text']; ?></a>
                    </li>
                    <?php } ?>
                </ul>
                <div class="tab-content float-left w-100" style="">
                    <?php foreach( $langs as $lang ){ ?>
                    <?php
					if( isset( $detail[ $lang['text'] ] ) ){
                        $btn_text = $detail[ $lang['text'] ]['btn_text'];
					}else{
              			$btn_text = '';
					}
					?>
                    <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                        id="<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
                        aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
                        <div class="row">
                            <div class="col-12 mt-3">
                                <div class="form-group">
                                    <label for="">Button Text</label>
                                    <input type="text" name="btn_text[]" class="form-control" placeholder="Button Text"
                                        value="<?php echo $btn_text; ?>"
                                        maxlength="<?php echo $param_sec_key == 'calendar' ? '100' : ''; ?>">
                                </div>
                            </div>
                        </div>

                        <?php // if($param_sec_key != "vrpartner"){ ?>

                        <?php //} ?>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
<?php } ?>
    <div class="card mb-3">
        <div class="card-body">
            <div class="form-group">
                <?php 
					  $text_mute_d = "";
                      $data_text_d = "";
					  if( $param_sec_key == "experience"){
						$text_mute_d = '( 417px x 313px )';
                        $data_text_d = 'data-max-width="417" data-max-height="313"';
					  }elseif($param_sec_key == "calendar"){
                        $text_mute_d = '( 664px x 415px )';
                        $data_text_d = 'data-max-width="664" data-max-height="415"';
                      }
                      elseif($param_sec_key == "truecatalog"){
                        $text_mute_d = '( 240px x 240px )';
                        $data_text_d = 'data-max-width="240" data-max-height="240"';
                    }
                    elseif($param_sec_key == "touchthebeyond"){
                        $text_mute_d = '( 817px x 680px )';
                        $data_text_d = 'data-max-width="817" data-max-height="680"';

                    }elseif($param_sec_key == "seetheunseen"){
                        $text_mute_d = '( 320px x 200px )';
                        $data_text_d = 'data-max-width="320" data-max-height="200"';
                    }elseif($param_sec_key == "vrproduct"){

                        $text_mute_d = '(Info icon size 60px x 60px , Product size  704px x 500px )';
                        $data_text_d = 'data-max-width="704" data-max-height="500"';
                    }
				  ?>
                <div class="row">
                <?php  if($sec_detail[DEFAULT_LANG]['design_type'] != 'accordion' ){ ?>
                    <div class="col-4 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
                        <label>Thumb Image  <i class="fa fa-info-circle" data-toggle="popover" data-html="true"  data-content="News & Event : 600 x 450 px" data-original-title="Image recommended"></i><!-- <small class="text-muted">( 609px x 457px )</small> --></label>

                        <input type="file" data-height="250" name="image" class="dropify" <?php echo $data_text_d; ?>
                            data-max-file-size="3M"
                            data-default-file="<?php echo $image != "" ? upload_path($image) : ''; ?>" />
                        <input type="hidden" class="image_as_text" name="image_txt" value="<?php echo $image; ?>" />
                    </div>
                    <!--
                        <?php  if($sec_detail[DEFAULT_LANG]['design_type'] != 'accordion' ){ ?>
                            <div class="col col-12 col-md-4 image-box">
                                <div class="form-group">
                                    <label>Banner Image Desktop <i class="fa fa-info-circle" data-toggle="popover" data-html="true" data-content="News & Event : 1200 x 250 px" data-original-title="Image recommended"></i></label>
                                    <input type="file" data-height="250" name="hero_banner" class="dropify"
                                        data-max-file-size="3M"
                                        data-default-file="<?php  echo $hero_banner != "" ? upload_path($hero_banner) : '';  ?>" />
                                    <input type="hidden" class="image_as_text" name="hero_banner_txt"
                                        value="<?php echo $hero_banner; ?>" />
                                </div>
                            </div>
                            <div class="col col-12 col-md-4 image-box">
                                <div class="form-group">
                                    <label>Banner Image Mobile <i class="fa fa-info-circle" data-toggle="popover" data-html="true" data-content="News & Event : 760 x 545 px" data-original-title="Image recommended"></i></label>
                                    <input type="file" data-height="250" name="hero_banner_m" class="dropify"
                                        data-max-file-size="3M"
                                        data-default-file="<?php  echo $hero_banner_m != "" ? upload_path($hero_banner_m) : '';  ?>" />
                                    <input type="hidden" class="image_as_text" name="hero_banner_m_txt"
                                        value="<?php echo $hero_banner_m; ?>" />
                                </div>
                            </div>
                    <?php } ?>
                    -->
                    <?php if($param_sec_key == "touchthebeyond"){ ?>
                    <div class="col-12 image-box mt-3">
                        <label>Image 2<small class="text-muted">( 885px x 387px )</small></label>
                        <input type="file" data-height="250" name="image2" class="dropify" data-max-width="885"
                            data-max-height="387" data-max-file-size="3M"
                            data-default-file="<?php echo $image2 != "" ? upload_path($image2) : ''; ?>" />
                        <input type="hidden" class="image_as_text" name="image2_txt" value="<?php echo $image2; ?>" />
                    </div>
                    <?php } ?>
                    <?php if( $param_sec_key == "calendar" ){ ?>
                    <div class="col-12 mt-3">
                        <label for="">Event Date</label>
                        <input class="input-medium form-control datepicker" name="post_date" type="text"
                            data-date-language="th-th" autocomplete="off" value="<?php echo $post_date; ?>">
                    </div>
                    <div class="col-12 mt-3">
                        <label for="">Event Time</label>
                        <input type="text" name="post_time" class="form-control" placeholder="Event Time"
                            value="<?php echo $post_time; ?>">
                    </div>
                    <?php } ?>
                    <?php if($param_sec_key == "experience" || $param_sec_key == "calendar"){ ?>
                    <div class="col-12 mt-3">
                        <label for="">Link out</label>
                        <input type="text" name="link_out" class="form-control" placeholder="Link out"
                            value="<?php echo $link_out; ?>">
                    </div>
                    <?php } ?>
                <?php } ?>
                    <?php if($param_sec_key == "touchthebeyond" || $param_sec_key == "calendar" || $param_sec_key == "truecatalog" || $param_sec_key == "vrproduct" || $param_sec_key == "vrinfo" || $param_sec_key == "vrpartner"){?>
                    <div class="col-12 mt-3">
                        <label for="">Order</label>
                        <input type="text" name="sticky" class="form-control" placeholder="Order"
                            value="<?php echo $sticky; ?>">
                    </div>
                    <?php }else{ ?>
                    <div class="col col-12 col-md-8 image-box">
                        <div class="row">
                            <div class="col-8 mt-3">
                                <label for="">Category</label>
                                <select class="form-control" name="cate_code">
                                    <option value="">เลือกหมวดหมู่</option>
                                    <?php foreach( $cate_lists as $cate ){ ?>
                                    <option value="<?php echo $cate['code']; ?>"
                                        <?php echo $cate['code'] == $cate_code ? 'selected' : ''; ?>>
                                        <?php echo $cate['title']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-4 mt-3">
                                <label for="">Order</label>
                                <input type="text" name="sticky" class="form-control" placeholder="Order"
                                    value="<?php echo $sticky; ?>">
                            </div>
                        </div>
                        <?php }  ?>
                        <div class="row">                 
                            <div class="col-12 mt-3">
                                <div class="form-group">
                                    <label for="">Tags </label>
                                    <input type="text" name="tags" class="form-control tags" placeholder="Tags"
                                        value="<?php echo $tags; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card mt-4">
        <div class="card-body">
            <div class="box-switch">
                <h4 class="card-title">Content</h4>
                <div class="button-switch">
                    <input type="checkbox" data-toggle="toggle" data-onstyle="info" data-on="Show" data-off="Hide"
                        data-offstyle="outline-secondary" data-size="xs" name="box_switch" checked
                        onchange="sectionToggle($(this))">
                </div>
                <div class="content-switch">
                    <div class="content">
                        <div class="row">
                       
                            <div class="col col-12  col-md-12">
                                <div class="form-group">
                                    <div class=" w-100 <?php echo sizeof( $langs ) == 0 ? 'only-one-lang' : ''; ?>">
                                        <ul class="nav nav-tabs" role="tablist" style="">
                                            <?php foreach( $langs as $lang ){ ?>
                                            <li class="nav-item">
                                                <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                                    id="<?php echo $lang['text']; ?>-tab" data-toggle="tab"
                                                    href="#<?php echo $lang['text']; ?>" role="tab"
                                                    aria-controls="<?php echo $lang['text']; ?>"
                                                    aria-selected="true"><?php echo $lang['text']; ?></a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                        <div class="tab-content float-left w-100" style="">
                                            <?php foreach( $langs as $lang ){ ?>
                                            <?php
                                            if( isset( $detail[ $lang['text'] ] ) ){
                                                $content_editor = $detail[ $lang['text'] ]['content_editor'];
                                            }else{
                                                $content_editor = '';
                                            }
                                            ?>
                                            <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                                id="<?php echo $lang['text']; ?>" role="tabpanel"
                                                aria-labelledby="<?php echo $lang['text']; ?>-tab">

                                                <div class="form-group">
                                                    <div class="install-nexitor float-left" data-area="content">
                                                        <?php echo $content_editor; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php /*
    <div class="card">
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-12 col-md-6 image-box"> 
                        <label>Hero Banner Desktop</label>
                        <input type="file" data-height="200" name="image" class="dropify" data-max-file-size="3M"
                            data-default-file="<?php echo $image != "" ? upload_path($image) : ''; ?>" />
    <input type="hidden" class="image_as_text" name="image_txt" value="<?php echo $image; ?>" />
</div>
<div class="col-12 col-md-6 image-box">
    <label>Hero Banner Mobile</label>
    <input type="file" data-height="200" name="image_m" class="dropify" data-max-file-size="3M"
        data-default-file="<?php echo $image_m != "" ? upload_path($image_m) : ''; ?>" />
    <input type="hidden" class="image_as_text" name="image_m_txt" value="<?php echo $image_m; ?>" />
</div>
</div>
</div>
</div>
</div>
*/ ?>

<div class="card mt-3">
    <div class="card-body">
        <div class="box-switch">
        <h3 class="card-title"> SEO Page setting </h3>
            <div class="button-switch seo-switch">
            <?php
            if( isset( $detail[ $lang['text'] ] ) ){
                if ( sizeof($seo) > 0 ) {
                    $seo_code = $seo[ $lang['text'] ]['code'];
                } else {
                    $seo_code = '';
                }
                
            }else{
                $seo_code = '';
            }
            ?>
                <input type="checkbox" <?php // echo $seo_status == 1 ? 'checked' : '' ?> data-toggle="toggle"
                    data-onstyle="info" data-on="Show" data-off="Hide" data-offstyle="outline-secondary" data-size="xs"
                    name="seo_status_switch">
                <input type="hidden" name="seo_code" value="<?php echo $seo_code; ?>">
                <input type="hidden" name="seo_type" value="listing">
                

            </div>
            <div class="content-switch seo-toggle-wrap" style="display:none;">
                <div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
                    <ul class="nav nav-tabs" role="tablist" style="">
                        <?php foreach( $langs as $lang ){ ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                                id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
                                href="#seo-<?php echo 'title_'.$lang['text']; ?>" role="tab"
                                aria-controls="<?php echo 'title_'.$lang['text']; ?>"
                                aria-selected="true"><?php echo $lang['text']; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content float-left w-100" style="">
                        <?php foreach( $langs as $lang ){ ?>
                        <?php
                        if( isset( $detail[ $lang['text'] ] ) ){
                            if ( sizeof($seo) > 0 ) {
                                $page_title = $seo[ $lang['text'] ]['page_title'];
                                $meta_title = $seo[ $lang['text'] ]['meta_title'];
                                $meta_desc = $seo[ $lang['text'] ]['meta_desc'];
                                $meta_keyword = $seo[ $lang['text'] ]['meta_keyword'];
                            } else {
                                $page_title = '';
                                $meta_title = '';
                                $meta_desc = '';
                                $meta_keyword = '';
                            }
                            
                        }else{
                            $page_title = '';
                            $meta_title = '';
                            $meta_desc = '';
                            $meta_keyword = '';
                        }
                        ?>
                        <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
                            id="seo-<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
                            aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
                            <div class="form-group">
                                <label for="">Page title</label>
                                <input type="text" name="page_title[]" class="form-control" placeholder="Page title"
                                    value="<?php echo $page_title; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Meta title</label>
                                <input type="text" name="meta_title[]" class="form-control" placeholder="Meta title"
                                    value="<?php echo $meta_title; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Meta keyword </label>
                                <input type="text" name="meta_keyword[]" class="form-control meta_keyword" placeholder="Meta Keyword"
                                    value="<?php echo $meta_keyword; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Meta description</label>
                                <input type="text" name="meta_desc[]" class="form-control"
                                    placeholder="Meta Description" value="<?php echo $meta_desc; ?>">
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-12 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
                            <label>Share Image<small class="text-muted">( 1200 x 630px )</small></label>
                            <input type="file" data-height="250" name="share_img" class="dropify"
                                data-max-file-size="3M"
                                data-default-file="<?php echo $share_img != "" ? upload_path($share_img) : ''; ?>" />
                            <input type="hidden" class="image_as_text" name="share_img_txt"
                                value="<?php echo $share_img; ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card mt-3">
    <div class="card-body">
        <div class="row">
            <div class="col-6 mt-3">
              <div class="form-group">
              <label for="">Publish Date</label>
                <input class="input-medium form-control datepicker" name="published_date" type="text"
                    data-date-language="th-th" autocomplete="off" value="<?php echo $p_date; ?>">
              </div>
            </div>
            <div class="col-6 mt-3">
                <div class="form-group">
                    <label for="">Publish Time</label>
                    <input type="text" name="published_time" class="form-control" value="<?php echo $p_time; ?>">
                </div>
            </div>
        </div> 
    </div>
</div> 

<div class="card mt-3">
    <div class="card-body">
        <?php if( isApprover($this->session->userdata('user_type')) or $cate_detail[DEFAULT_LANG]['must_be_approve'] == '0' ){ ?>

        <div class="form-group">
            <label for="">Status</label>
            <div class="form-check form-check-dark">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="onoff" value="1" <?php echo $onoff == 1 || $approve_status == 1 ? 'checked' : ''; ?>> Publish </label>
            </div>
            <div class="form-check form-check-dark">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 && $approve_status != 1 ? 'checked' : ''; ?>> Unpublish </label>
            </div>
        </div> 

        <?php } else { ?>
            <input type="hidden" name="onoff" value="<?php echo $onoff; ?>">
        <?php } ?>
        <div class="row">
            <div class="col-12 text-right">
                <div class="text-center card-button">
                <?php
                     if( isEditor($this->session->userdata('user_type')) and $cate_detail[DEFAULT_LANG]['must_be_approve'] == '1'  ){ 

                        if ( $approve_status == '0' or $approve_status == '3' ) {
                    ?>
                            <button type="button" class="btn btn-gradient-success btn-fw-lg mr-2 listing-approveForm pl-2 pr-2">Save & Send to Approve</button>
                    <?php
                        } else if ( $approve_status == '1' ) {
                    ?>
                            <button type="button" class="btn btn-gradient-danger btn-fw-lg mr-2 listing-cancelForm" data-code="<?php echo $code; ?>" onclick="approve_cancel( $(this) );">Cancel for Edit</button>
                    <?php
                        }
                    }
                    ?>

                    <?php if( isApprover($this->session->userdata('user_type')) and $cate_detail[DEFAULT_LANG]['must_be_approve'] == '1' and $code != '' and $approve_status != '2'  ){  ?>
                        <button type="button" class="btn btn-gradient-danger btn-fw-lg mr-2 pl-2 pr-2" data-code="<?php echo $code; ?>" onclick="approve_reject( $(this) );">Reject</button>
                        <button type="button" class="btn btn-gradient-success btn-fw-lg mr-2 pl-2 pr-2" data-code="<?php echo $code; ?>" onclick="approve_confirm( $(this), $('input[name=onoff]:checked').val() );">Approve</button>
                    <?php } ?>
                    
                    <?php if ( ($approve_status != '1') or isApprover($this->session->userdata('user_type'))  ) { ?>
                        <button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
                        <button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 listing-previewForm">Preview Changes</button>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
</div>

<?php //include_once("inc/nexitor-modal.php"); ?>