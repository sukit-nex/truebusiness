<?php
	if( sizeof( $detail ) > 0 ){
		$code = $detail[DEFAULT_LANG]['code'];
		$thumb_image = $detail[DEFAULT_LANG]['thumb_image'];
		$banner_desktop_image = $detail[DEFAULT_LANG]['banner_desktop_image'];
		$banner_mobile_image = $detail[DEFAULT_LANG]['banner_mobile_image'];
		$cate_code_ar = $detail[DEFAULT_LANG]['cate_code'];
		if( $cate_code_ar != "" ){ 
			$ar_cate_code = json_decode( $cate_code_ar, true ); 
		}else{ 
			$ar_cate_code = array();
		}

		$business_type_code = $detail[DEFAULT_LANG]['business_type_code'];
        // print_r( $business_type_code );
        // exit;
		if ( $business_type_code != '' ) {
			$business_type_ar = json_decode($business_type_code, true);
		} else {
			$business_type_ar = array();
		}
		
		foreach( $langs as $lang ){
			$benefit[$lang['text']] = json_decode($detail[$lang['text']]['benefit'], true);
		}
		
		$btn_link_type = $detail[DEFAULT_LANG]['btn_link_type'];
		$btn_display = $detail[DEFAULT_LANG]['btn_display'];
		$btn_link = $detail[DEFAULT_LANG]['btn_link'];

		$slug = $detail[DEFAULT_LANG]['slug'];
		$sticky = $detail[DEFAULT_LANG]['sticky'];
		$onoff = $detail[DEFAULT_LANG]['onoff'];
	}else{
		$code = '';
		$thumb_image = '';
		$banner_desktop_image = '';
		$banner_mobile_image = '';
		$ar_cate_code = array();
		$business_type_code = array();
		$business_type_ar = array();
		$benefit = '';
		$btn_link_type = 'auto';
		$btn_display = 'hide';
		$btn_link = '';

		$onoff = 1;
		$sticky = 0;
		$slug = '';
	}

	if( sizeof( $seo ) > 0 ) {
		$share_img = $seo[DEFAULT_LANG]['share_img'];
	} else {
		$share_img = '';
	}

	
?>
<div class="content-wrapper">
	<div class="page-header">
		<h3 class="page-title"> Solution Form </h3>
		<a href="<?php echo base_url().'admin/solution/lists'; ?>" class="btn btn-gradient-dark btn-fw"> <- Back</a>
	</div>
	<ul class="nav nav-tabs nav-tabs-lang" role="tablist" style="">
		<?php foreach( $langs as $lang ){ ?>
		<li class="nav-item">
			<a class="nav-link head-<?php echo $lang['text']; ?> <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" 
			href="javascript:activaTabNex('<?php echo $lang['text']; ?>');"><?php echo $lang['text']; ?></a>
		</li>
		<?php } ?>
	</ul>
	<?php echo form_open_multipart('admin/solution/saveForm', ' class="mainForm"'); ?>
		<input type="hidden" name="code" value="<?php echo $code ?>" >
		<input type="hidden" name="time" value="<?php echo $time; ?>">

	<div class="card">
        <div class="card-body">
			<div class="row">
				<div class="col-12 col-md-9 image-box">
					<div class="form-group">
						<div class="tab-content tab-content-lang">
							<?php foreach( $langs as $lang ){ ?>
								<?php
								if( isset( $detail[ $lang['text'] ] ) ) {
									$title = $detail[ $lang['text'] ]['title'];
									$description = $detail[ $lang['text'] ]['description'];
									$brochure_pdf = $detail[ $lang['text'] ]['brochure_pdf'];
								} else {
									$title = '';
									$description = '';
									$brochure_pdf = '';
								}
								?>
								<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
									<div class="form-group mb-3">
										<label for="">Title </label>
										<input type="text" name="title[]" class="form-control" value="<?php echo $title; ?>">
									</div>
									<div class="form-group mb-3">
										<label for="">Description </label>
										<textarea type="text" name="description[]" class="form-control descriptionInput tinyMceExample" ><?php echo $description; ?></textarea>
									</div>
									<div class="form-group">
										<label>Brochure PDF</label>
										<input type="file" name="brochure_pdf[]" class="file-upload-default">
										<div class="input-group col-xs-12">
											<input type="text" class="form-control file-upload-info" disabled placeholder="Upload PDF" value="<?php echo $brochure_pdf; ?>">
											<span class="input-group-append">
												<button class="file-upload-browse btn  btn-gradient-dark" type="button">Upload</button>
											</span>
										</div>
										<input type="hidden" name="brochure_pdf_txt" value="<?php echo $brochure_pdf; ?>" >
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
					
				</div>
				<div class="col-12 col-md-3">
					<div class="form-group">
						<div class="row">
							<div class="col-12">
								<label>Category</label>
								<div class="box-cate">
									<div class="box-cate-wrap only-one"> 
										<?php foreach( $category_lists as $cate ){ ?>
											<div class="box-level box-level-0">
												<div class="form-check form-check-dark">
													<label class="form-check-label">
														<input class="form-check-input" type="checkbox" <?php echo in_array( $cate['code'], $ar_cate_code ) ? 'checked' : ''; ?> name="cate_code[]" value="<?php echo $cate['code']; ?>"> <?php echo $cate['title']; ?>
													</label> 
												</div>
											</div> <!-- end level 0 -->
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
        </div>
    </div>

	<div class="card my-3">
        <div class="card-body">
			<div class="row">
				<div class="col-12 col-md-4 image-box">
					<div class="form-group">
						<label>Thumb image <small class="text-muted">Size : 1920 x ..... px</small></label>
						<input type="file" data-height="190" name="thumb_image"
							class="dropify" data-max-file-size="3000kb"
							data-default-file="<?php echo  $thumb_image != "" ? upload_path($thumb_image) : ''; ?>" />
						<input type="hidden" class="image_as_text" name="thumb_image_txt"
							value="<?php echo $thumb_image; ?>" />
					</div>
				</div>
				<div class="col-12 col-md-4 image-box">
					<div class="form-group">
						<label>Banner desktop image <small class="text-muted">Size : 1920 x ..... px</small></label>
						<input type="file" data-height="190" name="banner_desktop_image"
							class="dropify" data-max-file-size="3000kb"
							data-default-file="<?php echo  $banner_desktop_image != "" ? upload_path($banner_desktop_image) : ''; ?>" />
						<input type="hidden" class="image_as_text" name="banner_desktop_image_txt"
							value="<?php echo $banner_desktop_image; ?>" />
					</div>
				</div>
				<div class="col-12 col-md-4 image-box">
					<div class="form-group">
						<label>Banner mobile image <small class="text-muted">Size : 1920 x ..... px</small></label>
						<input type="file" data-height="190" name="banner_mobile_image"
							class="dropify" data-max-file-size="3000kb"
							data-default-file="<?php echo  $banner_mobile_image != "" ? upload_path($banner_mobile_image) : ''; ?>" />
						<input type="hidden" class="image_as_text" name="banner_mobile_image_txt"
							value="<?php echo $banner_mobile_image; ?>" />
					</div>
				</div>
			</div>
        </div>
    </div>

	<div class="card my-3">
        <div class="card-body">
            <div class="box-switch">
                <h4 class="card-title">Content</h4>
                <div class="button-switch">
                    <input type="checkbox" data-toggle="toggle" data-onstyle="info" data-on="Show" data-off="Hide"
                        data-offstyle="outline-secondary" data-size="xs" name="box_switch" checked
                        onchange="sectionToggle($(this))">
                </div>
                <div class="content-switch">
                    <div class="content">
                        <div class="row">
                            <div class="col col-12  col-md-12">
                                <div class="form-group">
                                    <div class=" w-100 <?php echo sizeof( $langs ) == 0 ? 'only-one-lang' : ''; ?>">
                                        
										<div class="tab-content tab-content-lang">
                                            <?php foreach( $langs as $lang ){ ?>
                                            <?php
                                            if( isset( $detail[ $lang['text'] ] ) ){
                                                $content_editor = $detail[ $lang['text'] ]['content_editor'];
                                            }else{
                                                $content_editor = '';
                                            }
                                            ?>

											<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">

                                                <div class="form-group">
                                                    <div class="install-nexitor float-left" data-area="content">
                                                        <?php echo $content_editor; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

	<div class="card my-3">
        <div class="card-body">
            <div class="form-group">
				<h3 class="card-title"> Benefit </h3>
				<div class="repeater-item-list drag-item-list list-group">
					<?php 
					if ( $benefit != '') {
						foreach ($benefit[DEFAULT_LANG] as $key => $row) {
					?>
					<div class="list-group-item d-flex mb-2 list-group-item-row-<?php echo $key; ?>" data-row="<?php echo $key; ?>" >
						<div class="input-group mb-2 mr-sm-2 mb-sm-0">
							<div class="input-group-prepend">
								<div class="nexu-tools-list drag-cusor ui-sortable-handle">
									<a href="javascript:void(0)"><i class="mdi mdi-cursor-move"></i></a>
								</div>
							</div>
							<div class="mb-3 d-flex" style="width:95%;">
								<div class="w-100 mr-2">
									
									<div class="tab-content tab-content-lang">
										<?php foreach( $langs as $lang ){ ?>
										<?php

										$benefit_title = $benefit[$lang['text']][$key]['title'];
										$benefit_description = $benefit[$lang['text']][$key]['description'];

										?>
										<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>"  role="tabpanel" >
											<div class="mb-3">
												<label>Title</label>
												<input type="text" class="form-control form-control-sm w-100" name="benefit[<?php echo $key; ?>][title][]" placeholder="Title" value="<?php echo $benefit_title; ?>">
											</div>
											<div class="w-100">
												<div>
													<label>Description</label>
													<textarea class="form-control form-control-sm descriptionInput tinyMceExample" name="benefit[<?php echo $key; ?>][description][]" placeholder="Description" rows="3"><?php echo $benefit_description; ?></textarea>
												</div>
											</div>
										</div>
										<?php } ?> 
									</div>

								</div>
								
							</div>

						</div>
						<button type="button" class="btn btn-gradient-dark btn-sm icon-btn ml-2 btn-repeater-delete">
							<i class="mdi mdi-delete"></i>
						</button>					
					</div>
					<?php 
						}
					}
					?>

				</div>

				<button type="button" class="btn btn-gradient-dark btn-fw-lg my-3 mx-auto btn-repeater-add d-block">
					<i class="mdi mdi-plus"></i> Add Row
				</button>
			</div>
 		</div>
    </div>

	<div class="card my-3">
        <div class="card-body">
			<div class="row mb-3 wrapper_check_select">
				<div class="col-4">
					<div class="card-title">Business type</div>
					
					<div class="box-cate">
						<div class="box-cate-wrap"> 
							<?php 
							if ( isset( $business_type_lists ) && sizeof($business_type_lists) > 0 ) {
								
								foreach( $business_type_lists as $sp_row ) {
							?>
								<div class="box-level box-level-0">
									<div class="form-check form-check-dark">
										<label class="form-check-label">
											<input class="form-check-input" type="checkbox" <?php echo in_array( $sp_row['code'], $business_type_ar ) ? 'checked' : ''; ?> name="business_type[]" data-select_name="business_type_code" data-name="<?php echo $sp_row['title']; ?>" value="<?php echo $sp_row['code']; ?>"> <?php echo $sp_row['title']; ?>
										</label> 
									</div>
								</div> <!-- end level 0 -->
							<?php 
								}
							} 
							?>
						</div>
					</div>

				</div>
				<div class="col-8">
					
					<ul class="gen-topup drag-item-list list-group check_select_sortable">
						<?php
						
						//$sos_ar = explode(',',$row['special_offer_select']);
						$sos_ar = $business_type_ar;
						if ( isset( $sos_ar ) && sizeof($sos_ar) > 0 ) {
							foreach( $sos_ar as $sos_row ) {

								foreach( $business_type_lists as $sp_row ) {
									if ( $sos_row == $sp_row['code'] ) {
						?>
						<li class="drag-list level-0 sp-<?php echo $sp_row['code']; ?>">
							<input type="hidden" name="business_type_code[]" value="<?php echo $sp_row['code']; ?>">
							<div class="box-select-topup ">
								<div class="box-txt-topup">
									<p><?php echo $sp_row['title']; ?></p>
								</div>
								<div class="icon-move">
									<i class="mdi mdi-cursor-move"></i>
								</div>	
							</div>
						</li>
						<?php
									}
								}

							}
						}
						?>
					</ul>
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="card my-3">
		<div class="card-body">
			<div class="card-title">Button content setting</div>
			<div class="row d-block d-md-flex align-items-center ">
				<div class="col-12 col-md-3">
					<label>Link type</label>
				</div>
				<div class="col-12 col-md-9">
					<div class="row">
						<div class="col-md-3 col-sm-2 col-xs-12">
							<div class="box-radio box-check-white-bd">
								<div class="form-check form-check-dark">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="btn_link_type" value="auto" <?php echo $btn_link_type == 'auto' ?'checked':'';?>>
										Auto
									<i class="input-helper"></i></label>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-2 col-xs-12">
							<div class="box-radio box-check-white-bd">
								<div class="form-check form-check-dark">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="btn_link_type" value="no_link" <?php echo $btn_link_type == 'no_link' ?'checked':'';?>>
										No link
									<i class="input-helper"></i></label>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-2 col-xs-12">
							<div class="box-radio box-check-white-bd">
								<div class="form-check form-check-dark">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="btn_link_type" value="external_link" <?php echo $btn_link_type == 'external_link' ?'checked':'';?>>
										External link
									<i class="input-helper"></i></label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row d-block d-md-flex align-items-center mb-3">
				<div class="col-12 col-md-3 row_btn_display">
					<label>Display button</label>
				</div>
				<div class="col-12 col-md-9 row_btn_display">
					<div class="row">
						<div class="col-md-3 col-sm-2 col-xs-12">
							<div class="box-radio box-check-white-bd">
								<div class="form-check form-check-dark">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="btn_display" value="hide" <?php echo $btn_display == 'hide' ?'checked':'';?>>
										Hidden button
									<i class="input-helper"></i></label>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-2 col-xs-12">
							<div class="box-radio box-check-white-bd">
								<div class="form-check form-check-dark">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="btn_display" value="show" <?php echo $btn_display == 'show' ?'checked':'';?>>
										Show button
									<i class="input-helper"></i></label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row d-block d-md-flex align-items-center ">
				<div class="col-12 col-md-3 row_btn_link">
					<label>Link URL</label>
				</div>
				<div class="col-12 col-md-9 row_btn_link">
					<div class="form-group mb-3">
						<input type="text" name="btn_link" class="form-control" value="<?php echo $btn_link; ?>">
					</div>
				</div>
			</div>
			<div class="row d-block d-md-flex align-items-center ">
				<div class="col-12 col-md-3 row_btn_text">
					<label>Button message</label>
				</div>
				<div class="col-12 col-md-9 row_btn_text">

						<div class="tab-content tab-content-lang">
							<?php foreach( $langs as $lang ){ ?>
							<?php
							if( isset( $detail[ $lang['text'] ] ) ){
								$btn_text = $detail[ $lang['text'] ]['btn_text'];
							}else{
								$btn_text = '';
							}
							?>

							<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
								<div class="form-group mb-3">
									<input type="text" name="btn_text" class="form-control" value="<?php echo $btn_text; ?>">
								</div>
							</div>
							<?php } ?>
						</div>
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="card my-3">
		<div class="card-body">
			<div class="box-switch">
			<h3 class="card-title"> SEO Page setting </h3>
				<div class="button-switch seo-switch">
				<?php
				if( isset( $detail[ $lang['text'] ] ) ){
					if ( sizeof($seo) > 0 ) {
						$seo_code = $seo[ $lang['text'] ]['code'];
					} else {
						$seo_code = '';
					}
				}else{
					$seo_code = '';
				}
				?>
					<input type="checkbox" <?php // echo $seo_status == 1 ? 'checked' : '' ?> data-toggle="toggle"
						data-onstyle="info" data-on="Show" data-off="Hide" data-offstyle="outline-secondary" data-size="xs"
						name="seo_status_switch">
					<input type="hidden" name="seo_code" value="<?php echo $seo_code; ?>">
					<input type="hidden" name="seo_type" value="listing">
					

				</div>
				<div class="content-switch seo-toggle-wrap" style="display:none;">
					<div class="row">
						<div class="col-12 col-md-8">
							<div class="form-group float-left w-100 <?php echo sizeof( $langs ) == 1 ? 'only-one-lang' : ''; ?>">
								<ul class="nav nav-tabs" role="tablist" style="">
									<?php foreach( $langs as $lang ){ ?>
									<li class="nav-item">
										<a class="nav-link <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
											id="<?php echo 'title_'.$lang['text']; ?>-tab" data-toggle="tab"
											href="#seo-<?php echo 'title_'.$lang['text']; ?>" role="tab"
											aria-controls="<?php echo 'title_'.$lang['text']; ?>"
											aria-selected="true"><?php echo $lang['text']; ?></a>
									</li>
									<?php } ?>
								</ul>
								<div class="tab-content float-left w-100" style="">
									<?php foreach( $langs as $lang ){ ?>
									<?php
									if( isset( $detail[ $lang['text'] ] ) ){
										if ( sizeof($seo) > 0 ) {
											$page_title = $seo[ $lang['text'] ]['page_title'];
											$meta_title = $seo[ $lang['text'] ]['meta_title'];
											$meta_desc = $seo[ $lang['text'] ]['meta_desc'];
											$meta_keyword = $seo[ $lang['text'] ]['meta_keyword'];
										} else {
											$page_title = '';
											$meta_title = '';
											$meta_desc = '';
											$meta_keyword = '';
										}
										
									}else{
										$page_title = '';
										$meta_title = '';
										$meta_desc = '';
										$meta_keyword = '';
									}
									?>
									<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>"
										id="seo-<?php echo 'title_'.$lang['text']; ?>" role="tabpanel"
										aria-labelledby="<?php echo 'title_'.$lang['text']; ?>-tab">
										<div class="form-group">
											<label for="">Page title</label>
											<input type="text" name="page_title[]" class="form-control" placeholder="Page title"
												value="<?php echo $page_title; ?>">
										</div>
										<div class="form-group">
											<label for="">Meta title</label>
											<input type="text" name="meta_title[]" class="form-control" placeholder="Meta title"
												value="<?php echo $meta_title; ?>">
										</div>
										<div class="form-group">
											<label for="">Meta keyword </label>
											<input type="text" name="meta_keyword[]" class="form-control meta_keyword" placeholder="Meta Keyword"
												value="<?php echo $meta_keyword; ?>">
										</div>
										<div class="form-group">
											<label for="">Meta description</label>
											<input type="text" name="meta_desc[]" class="form-control"
												placeholder="Meta Description" value="<?php echo $meta_desc; ?>">
										</div>
									</div>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-4">
							<div class="form-group">
								<div class="row">
									<div class="col-12 image-box"> <?php /* data-max-height="100" data-max-width="100" */ ?>
										<label>Share Image<small class="text-muted">( 1200 x 630px )</small></label>
										<input type="file" data-height="250" name="share_img" class="dropify"
											data-max-file-size="3M"
											data-default-file="<?php echo $share_img != "" ? upload_path($share_img) : ''; ?>" />
										<input type="hidden" class="image_as_text" name="share_img_txt"
											value="<?php echo $share_img; ?>" />
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="card my-3">
		<div class="card-body">
			<div class="row">
				<div class="col-6">
					<div class="form-group mb-3">
						<label for="">Slug</label>
						<input type="text" name="slug" class="form-control" placeholder="Slug"
							value="<?php echo $slug; ?>">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label for="">Order</label>
						<input type="text" name="sticky" class="form-control" placeholder="Sticky" value="<?php echo $sticky; ?>">
					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="">Status</label>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="onoff" value="1"  <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
				</div>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 ? 'checked' : ''; ?>> Non - Publish </label>
				</div>
			</div>

			<div class="row">
				<div class="col-12 text-right">
					<div class="text-center card-button">
						
						<button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<!-- content-wrapper ends -->
<?php include_once("inc/nexitor-modal.php"); ?>

<div style="display:none;">
	<div class="list-group-item-origin list-group-item d-flex mb-2 list-group-item-row-rpt_number" data-row="rpt_number" >
		<div class="input-group mb-2 mr-sm-2 mb-sm-0">
			<div class="input-group-prepend">
				<div class="nexu-tools-list drag-cusor ui-sortable-handle">
					<a href="javascript:void(0)"><i class="mdi mdi-cursor-move"></i></a>
				</div>
			</div>
			<div class="mb-3 d-flex" style="width:95%;">
				<div class="w-100 mr-2">
					
					<div class="tab-content tab-content-lang">
						<?php foreach( $langs as $lang ){ ?>
						<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>"  role="tabpanel" >
							<div class="mb-3">
								<label>Title</label>
								<input type="text" class="form-control form-control-sm w-100" name="benefit[rpt_number][title][]" placeholder="Title" value="">
							</div>
							<div class="w-100">
								<div>
									<label>Description</label>
									<textarea class="form-control form-control-sm original-tinymce" name="benefit[rpt_number][description][]" placeholder="Description" rows="3"></textarea>
								</div>
							</div>
						</div>
						<?php } ?> 
					</div>

				</div>
				
			</div>

		</div>
		<button type="button" class="btn btn-gradient-dark btn-sm icon-btn ml-2 btn-repeater-delete">
			<i class="mdi mdi-delete"></i>
		</button>					
	</div>
</div>



