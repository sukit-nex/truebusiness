<div class="content-wrapper">
	<div class="page-header">
		<h4 class="page-title"> Solution Owner </h4>
		<a href="<?php echo base_url().'admin/solution_banner/add' ?>" class="btn btn-gradient-dark btn-fw">+ Add more</a>
	</div>
	<div class="card">
		<div class="card-body">
			<div class="row">
				<div class="col-12">
					<div class="table-responsive">

					<table id="order-listing" class="table">
						<thead>
							<tr>
								<th>No.</th>
								<th>Order</th>
								<th style="width:20%;">Banner Name</th>
								<th>Actions</th>
							</tr>
						</thead> 
						<tbody>
							<?php $i = 1; ?>
							<?php foreach( $lists as $list ){ ?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $list['sticky']; ?></td>
									<td style="white-space: normal;"><span class="float-left" style="width:300px; line-height:1.4;"><?php echo $list['name']; ?></span></td>
									<td><div class="badge badge-pill badge-danger btn-inverse-dark"  data-toggle="tooltip" data-placement="right" title="Created : <?php echo $list['c_date']; ?> Update : <?php echo $list['u_date']; ?>"><i class="mdi mdi-clock"></i> Date post</div></td>
									<td>
										<a href="<?php echo base_url().'admin/solution_banner/edit/'.$list['code']; ?>" class="btn btn-info">Edit</a>
										<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
										<a href="javascript:void(0);" class="btn btn-danger delete-item" onclick="deleteItem( $(this) );" data-toggle="modal" data-target="#deleteModal" data-delete-by="code" data-table="solution_banner" data-code="<?php echo $list['code']; ?>">Delete</a>
									</td>
								</tr>
							<?php $i++; } ?>
						</tbody>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- content-wrapper ends -->
