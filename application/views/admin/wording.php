<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title"> Wording list </h3>
    <?php /* <a href="<?php echo base_url().'admin/setting/wording/add' ?>" class="btn btn-gradient-dark btn-fw">+ Add more</a> */ ?>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <div class="table-responsive">

          <table id="order-listing" class="table">
            <thead>
              <tr>
                <th>No.</th>
                <th>Label</th>
                <th>Thailand</th>
                <?php /* <th>English</th> */ ?>
                <th>Log Post</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach( $lists as $list ){ ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $list['label']; ?></td>
                  <td><?php echo $list['thailand']; ?></td>
                  <?php /* <td><?php echo $list['english']; ?></td> */ ?>
                  <td>
                  	<div class="badge badge-pill badge-danger btn-inverse-dark"  data-toggle="tooltip" data-placement="right" title="Created : <?php echo $list['c_date']; ?> Update : <?php echo $list['u_date']; ?>"><i class="mdi mdi-clock"></i> Date post</div>
                  </td>
                  <td>
                    <a href="<?php echo base_url().'admin/setting/wording/edit/'.$list['id']; ?>" class="btn btn-info">Edit</a>
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                    <?php /* <a href="javascript:void(0);" class="btn btn-danger delete-item"  onclick="deleteItem( $(this) );" data-toggle="modal" data-target="#deleteModal" data-delete-by="id" data-table="wording" data-code="<?php echo $list['id']; ?>">Delete</a> */?>
                  </td>
                </tr>
              <?php $i++; } ?>
            </tbody>
          </table>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
