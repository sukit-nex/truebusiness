<?php
	if( sizeof( $detail ) > 0 ){
		$code = $detail[DEFAULT_LANG]['code'];
		$icon_image = $detail[DEFAULT_LANG]['icon_image'];
		$onoff = $detail[DEFAULT_LANG]['onoff'];
	}else{
		$code = '';
		$icon_image = '';
		$onoff = 1;
	}
?>
<div class="content-wrapper">
	<div class="page-header">
		<h3 class="page-title"> Solution : Business type Form </h3>
		<a href="<?php echo base_url().'admin/solution/business_type/lists'; ?>" class="btn btn-gradient-dark btn-fw"> <- Back</a>
	</div>
	<ul class="nav nav-tabs nav-tabs-lang" role="tablist" style="">
		<?php foreach( $langs as $lang ){ ?>
		<li class="nav-item">
			<a class="nav-link head-<?php echo $lang['text']; ?> <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?>" 
			href="javascript:activaTabNex('<?php echo $lang['text']; ?>');"><?php echo $lang['text']; ?></a>
		</li>
		<?php } ?>
	</ul>
	<?php echo form_open_multipart('admin/solution/business_type/saveForm', ' class="mainForm"'); ?>
		<input type="hidden" name="code" value="<?php echo $code ?>" >
		<input type="hidden" name="time" value="<?php echo $time; ?>">

	<div class="card">
        <div class="card-body">
			
			<div class="row">
				<div class="col-12 col-md-3 image-box">
					<div class="form-group">
						<label>Icon / Image<small class="text-muted"> (50x50) </small></label>
						<input type="file" data-height="190" name="icon_image"
							class="dropify" data-max-file-size="3000kb"
							data-default-file="<?php echo  $icon_image != "" ? upload_path($icon_image) : ''; ?>" />
						<input type="hidden" class="image_as_text" name="icon_image_txt"
							value="<?php echo $icon_image; ?>" />
					</div>
				</div>
				<div class="col-12 col-md-9 image-box">
					<div class="form-group">
						<div class="tab-content tab-content-lang">
							<?php foreach( $langs as $lang ){ ?>
								<?php
								if( isset( $detail[ $lang['text'] ] ) ) {
									$title = $detail[ $lang['text'] ]['title'];
								} else {
									$title = '';
								}
								?>
								<div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
									<div class="form-group mb-3">
										<label for="">Title </label>
										<input type="text" name="title[]" class="form-control" value="<?php echo $title; ?>">
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				
			</div>
        </div>
    </div>
	
	<div class="card my-3">
		<div class="card-body">
			<div class="form-group">
				<label for="">Status</label>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="onoff" value="1"  <?php echo $onoff == 1 ? 'checked' : ''; ?>> Publish </label>
				</div>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="onoff" value="0" <?php echo $onoff == 0 ? 'checked' : ''; ?>> Non - Publish </label>
				</div>
			</div>

			<div class="row">
				<div class="col-12 text-right">
					<div class="text-center card-button">
						
						<button type="button" class="btn btn-gradient-dark btn-fw-lg mr-2 saveForm">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<!-- content-wrapper ends -->
<?php include_once("inc/nexitor-modal.php"); ?>
