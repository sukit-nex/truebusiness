<?php
  if( sizeof( $detail ) > 0 ){
    $id = $detail['id'];
    $label = $detail['label'];
    $thailand = $detail['thailand'];
    $english = $detail['english'];

  }else{
    $id = '';
    $label = '';
    $thailand = '';
    $english = '';
  }
?>
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title"> Wording Form </h3>
  </div>
  <div class="card">
    <div class="card-body">
   <?php echo form_open_multipart('admin/setting/saveWord', ''); ?>
      <input type="hidden" name="id" value="<?php echo $id ?>" >
      <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12">
          <div class="form-group">
            <label for="">Label</label>
            <input type="text" name="label" class="form-control"  placeholder="Label" value="<?php echo $label; ?>" <?php echo $id != "" ? 'disabled' : ''; ?>>
          </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
          <div class="form-group">
            <label for="">Thailand</label>
            <input type="text" name="thailand" class="form-control"  placeholder="Thailand" value="<?php echo $thailand; ?>">
          </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
          <div class="form-group">
            <label for="">English</label>
            <input type="text" name="english" class="form-control"  placeholder="English" value="<?php echo $english; ?>">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 text-right">
          <div class="text-center card-button">
            <!--<button type="submit" class="btn btn-gradient-light btn-fw-lg mr-2">Cancle</button>-->
            <button type="submit" class="btn btn-gradient-dark btn-fw-lg mr-2">Save</button>
          </div>
        </div>
      </div>
    <?php echo form_close(); ?>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
