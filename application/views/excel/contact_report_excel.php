<?php
$filename = $mem_type.'order_report';
  header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
  header("Content-Disposition: attachment; filename=contact_report.xls");
  header("Pragma: no-cache");
  header("Expires: 0");
?>
<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Activity</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
              </xml>
    <![endif]-->
</head>

<body>
    <table border="1">
      <thead>
      <tr>
        <th class="center">No</th>
        <th class="center">ID</th>
        <th class="center">Fullname</th>
        <th class="center">Phone</th>
        <th class="center">Email</th>
        <th class="center">Message</th>
        <th class="center">Request</th>
        <th class="center">Create Date</th>
      </tr>

      </thead>
      <tbody>
        <?php if( sizeof( $lists ) > 0 ){ $i = 1; ?>
        <?php foreach( $lists as $list ){ ?>
          <?php
            $random = $list['id'];
            for( $x = strlen( $random ) ; $x < 6 ; $x++ ){
              $random = '0'.$random;
            }
          ?>
          <tr>
            <td class="center"><?php echo $i; ?></td>
            <td class="center"><?php echo "'".$random; ?></td>
            <td class=""><?php echo $list['fullname']; ?></td>
            <td class=""><?php echo $list['phone']; ?></td>
            <td class=""><?php echo $list['email']; ?></td>
            <td class=""><?php echo $list['message']; ?></td>
            <td class=""><?php echo $list['request']; ?></td>
            <td class=""><?php echo $list['c_date']; ?></td>
          </tr>
        <?php $i++; } ?>
      <?php }else{ ?>
      <?php } ?>
      </tbody>
    </table>
</body>
</html>
