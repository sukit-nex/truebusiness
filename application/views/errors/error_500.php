<?php header("HTTP/1.1 500 internal server error"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Page : 500</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?php echo ADMIN_ASSET; ?>css/style.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/style.css" />
	<link type="text/css" rel="stylesheet" href="/assets/css/responsive.css" />
    <!-- endinject -->
    <link rel="shortcut icon" href="/assets/img/skin/favicon.png" />
  </head>
  <style>
  	body { color:#fff;}
	.btn { font-family:unset;}
	.content-wrapper { background:url(../../../assets/img/skin/menu-bottom-mobile.png) right bottom no-repeat #fff;}
	.content-wrapper h1 { font-weight:600; font-size:36px; display:block; margin-bottom:10px;}
	.content-wrapper h2 { font-size:16px;}
	.err-inforcontact {transform: scale(0.8); opacity: 0.8; border-top: 1px solid rgba(255,255,255,0.3)}
	.footer-detail-text { width:100%;}
	@media (max-width: 991.98px) { 
		.content-wrapper  { background-size:50%;}
	}
	@media (max-width: 767.98px) {	
		.content-wrapper  { background-size:50%;}
		.content-wrapper h1 { font-size: 24px; line-height: 1.4;}
		.err-inforcontact {transform: scale(0.9); opacity: 0.8; border-top: 1px solid rgba(255,255,255,0.3)}
	}
  </style>
  <body>
  	<div class="section-header">	
        <div class="logo-bg"><span class="logo-bg-color"></span><img src="../../../assets/img/skin/bg-logo.png"></div>
        <div class="box-logo">
            <div class="logo-image"><a href="<?php echo base_url(); ?>"><img src="../../../assets/img/skin/logo.png"></a></div>
        </div> 
    </div>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper" >
        <div class="content-wrapper d-flex align-items-center text-center error-page" style="background-color:#0636a9">
          <div class="row flex-grow">
            <div class="col-12 col-lg-9 mx-auto">
              <div class="row align-items-center d-flex flex-row">
                <div class="col-lg-12 text-lg-center pr-lg-4">
                  <h1 class="display-1">ขออภัย <br class="d-block d-md-none">Internal server error</h1> 
                </div>
                <?php /*?><div class="col-lg-12 error-page-divider text-lg-center pl-lg-4">
                  <h2>หน้าเว็บไซต์ดังกล่าวอาจมีการเปลี่ยนที่ เปลี่ยนชื่อ หรือพิมพ์ URL ไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง</h2>
                </div><?php */?>
                
              </div>
              <?php /*?><div class="row mt-3 mb-4">
                <div class="col-12 text-center mt-xl-2">
                  <a class="btn btn-secondary btn-font btn-250" href="<?php echo base_url(); ?>">กลับสู่หน้าหลัก</a>
                </div>
              </div><?php */?>
              <div class="row pt-5 err-inforcontact">
                  <div class="col col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 footer-group footer-menu">
                      <div class="footer-title">สอบถามรายละเอียดเพิ่มเติม</div>
                      <div class="footer-detail">
                          <div class="footer-detail-text">
                          ติดต่อ true call center  :<br class="d-block d-md-none">
                          <a href="tel:1242">1242</a></div>
                      </div>
                  </div>
              </div>
              
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?php echo ADMIN_ASSET; ?>vendors/js/vendor.bundle.base.js"></script>
    <script src="<?php echo ADMIN_ASSET; ?>vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="<?php echo ADMIN_ASSET; ?>js/off-canvas.js"></script>
    <script src="<?php echo ADMIN_ASSET; ?>js/hoverable-collapse.js"></script>
    <script src="<?php echo ADMIN_ASSET; ?>js/misc.js"></script>
    <script src="<?php echo ADMIN_ASSET; ?>js/settings.js"></script>
    <script src="<?php echo ADMIN_ASSET; ?>js/todolist.js"></script>
    <!-- endinject -->
  </body>
</html>
