<div class="loadpage"></div>
    <?php  //print_r($cate_detail); ?>

    <?php  //print_r($content_detail); ?>

    <?php  //print_r($solution_business_type); ?>

    <?php //print_r($solution_benefit); ?>
    
    
    <div class="wrapper">
        <div class="section cont bg-fullwidth st st-content contlay-ver ">
            <div class="bg-fullwidth fw  gen_bg" data-bg-desktop="<?php echo $content_detail['banner_desktop_image'];  ?>" data-bg-mobile="<?php echo $content_detail['banner_mobile_image'];  ?>" >
                <div class="container container-1600 gen-height text-middle"  data-image-desktop="<?php echo $content_detail['banner_desktop_image'] != ''  ? upload_path($content_detail['banner_desktop_image']) : '/assets/img/skin/blank.png' ; ?> "
                data-image-mobile="<?php echo $content_detail['banner_mobile_image'] != ''  ? upload_path($content_detail['banner_mobile_image']) : '' ; ?>" data-column="1">
                    <div class="position-relative fw">
                        <div class="cont-contain pd text-center">
                            <!-- <div class="cont-img">
                                <img src="/assets/img/ar/sec-2-img.png">
                            </div> -->
                            
                            <div class="cont-txt ">
                                <div class="row justify-content-center">
                                    <div class="col col-12 col-md-7">
                                        <div class="box-content"> 
                                            <h2 class="txt-gdcolor"><b><u><?php echo $content_detail['title']; ?></u></b></h2>
                                            <h5><b><?php echo $content_detail['sub_title']; ?></b></h5>
                                            <p><?php echo $content_detail['description']; ?></p>
                                            <?php if( isset($content_detail['brochure_pdf'] ) ){ ?>
                                            <a href="<?php echo upload_path().'/'.$content_detail['brochure_pdf']  ?>"target="_blank"  class="btn btn-text btn-icon mt-3 btn-download"><span><img src="/assets/img/skin/arrow-right-1.svg" class="svg"><span><b>ดาวน์โหลดโบรชัวร์</b></span></span></a>

                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if(sizeof($content_detail) > 0){ ?>
        <div id="" class="section cont solution-title-gr mg st st-list gen_margin" >
            <div class="bg-fullwidth fw  bg-py-t bg-px-c bg-rp-no bg-fx-st bg-sz-cv" >
                <div class="container container-1600  " >
                    <div class="position-relative fw  pd" >
                        <div class="cont-contain ml-0 mr-0 w-100">
                            <?php echo $content_detail['content'] ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <?php } ?>
        <div id="" class="section cont mg st st-list gen_margin  solution-thumb-ver  " data-mg-desktop="" data-mg-mobile="" data-slidecol="1" data-slidepercolumn="1" style="">
            <div class="bg-fullwidth fw gen_bg bg-py-t bg-px-c bg-rp-no bg-fx-st bg-sz-cv" data-bg-desktop="" data-bg-mobile="" data-bg-color="" style="">
            
                <div class="container container-1600  " data-image-desktop="/assets/img/skin/blank.png" data-image-mobile="" data-column="1">
                    <div class="position-relative fw gen_padding pd" data-pd-desktop="" data-pd-mobile="" style="">
                        <div class="cont-contain ml-0 mr-0 w-100">
                            <div class="sec-title text-center" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                                <div class="box-content">
                                    <h2 class="font-weight-bold"><span class="txt-gdcolor"><b>ประโยชน์ที่ธุรกิจหรือองค์กรได้รับจากการใช้งาน</b></span></h2>
                                </div>
                            </div>

                            <div class="sec-benefit-table">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-md-10">
                                        <div class="row">
                                            <?php foreach($solution_benefit as $key => $st_benefit_content){ ?>
                                                <div class="col col-12 col-md-6 col-lg-4">
                                                    <div class="box-tablepack ">
                                                        <div class="tablepack-list tablepack-list-<?php echo $key ?> ">
                                                            <div class="tablepack-head bg-gdcolor-orange">
                                                                <div class="box-content text-md-center text-left light-content">
                                                                    <h5 class="py-md-3 py-1"><b><?php echo $st_benefit_content['title']?></b></h5>
                                                                </div>
                                                                <div class="icon-toggle d-md-none d-block"></div>
                                                            </div>
                                                            <div class="tablepack-body ">
                                                                <div class="feature-list p-3">
                                                                    <div class="g-feature">
                                                                        <div class="box-content text-left">
                                                                            <?php echo $st_benefit_content['description']?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="" class="section cont mg st st-list gen_margin  solution-thumb-ver  " data-mg-desktop="" data-mg-mobile="" data-slidecol="1" data-slidepercolumn="1" style="">
            <div class="bg-fullwidth fw gen_bg bg-py-t bg-px-c bg-rp-no bg-fx-st bg-sz-cv" data-bg-desktop="" data-bg-mobile="" data-bg-color="" style="">
               
                <div class="container container-1600  " data-image-desktop="/assets/img/skin/blank.png" data-image-mobile="" data-column="1">
                    <div class="position-relative fw gen_padding pd" data-pd-desktop="" data-pd-mobile="" style="">
                   
                        <div class="cont-contain ml-0 mr-0 w-100">
                            <div class="sec-experienceSlide">
                                <div class="">
                                    <div class="inner-solution position-relative ">
                                    
                                        <div class="sec-title text-center zindex-2 position-relative">
                                            <div class="box-content">
                                                <h2 class="font-weight-bold"><span class="txt-gdcolor"><b>รูปแบบธุรกิจที่เหมาะในการใช้งาน</b></span></h2>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="genSlide " data-perview-d="5" data-perview-t="4" data-perview-m="2" data-space="60"  data-space-t="40" data-space-m="10">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <?php 
                                                    $img_name = ['experince-icon-financial.png','experince-icon-retail.png','experince-icon-healthcare.png','experince-icon-industrial.png','experince-icon-property.png','experince-icon-technology.png','experince-icon-goverment.png','experince-icon-education.png','experince-icon-5gexperiences.png'];
                                                    $title_name = ['ธุรกิจการเงิน','ธุรกิจค้าปลีก','ธุรกิจความงามและสุขภาพ','ธุรกิจอุตสาหกรรม','ธุรกิจอสังหาริมทรัพย์','ธุรกิจเทคโนโลยี','งานราชการ','การศึกษา','ประสบการณ์ 5G'];
                                                    for($i=0;$i<9;$i++){ 
                                                    ?>
                                                    <div class="swiper-slide">
                                                        <div class="box-thumb fw position-relative text-left">
                                                            <a href="https://true5gdev.digitalnex.com/th/content/1622624239/1622612664">
                                                            <div class="thumb thumb-ver">
                                                                <div class="thumb-group">
                                                                    <div class="thumb-img"><img src="/assets/img/demo/<?php echo $img_name[$i]; ?>" class="w-100 float-left"></div>
                                                                    <div class="thumb-txt text-center">
                                                                        <div class="thumb-txt-h"><span><?php echo $title_name[$i]; ?></span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </a>
                                                        </div>    
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <!-- <div class="swiper-pagination"></div> -->
                                            <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></div>
                                            <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/arrow-left-1.svg" class="svg"></div>
                                        </div>
                                        <img src="/assets/img/element/business-bg-graphic.png" alt="" class="slt-bg-graphic abs fw">
                                        <img src="/assets/img/element/business-bg-line.png" alt="" class="slt-bg-outline abs fw">
                                    </div>
                                </div>
                            </div>


                            <div class="sec-information">
                                <div class="box-info bg-gdcolor">
                                    <div class="sec-title text-center" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                                        <div class="box-content light-content">
                                            <h2 class="font-weight-bold"><span><b>สอบถาม<br class="d-md-none d-block">ข้อมูลเพิ่มเติมได้ที่</b></span></h2>
                                        </div>
                                    </div>
                                    <div class="box-mirror">
                                        <div class="box-mirror-t"><b>ศูนย์บริการลูกค้า</b> <span><b>1239</b></span></div>
                                        <div class="box-mirror-s"><b>หรือ</b></div>
                                        <div class="box-mirror-b"><a href="javascript:void(0)" class="btn btn-secondary"><span>ดูรายละเอียด</span></a></div>
                                    </div>
                                    <img src="/assets/img/skin/ele-buble-thumb-2.png" alt="" class="ele-buble-1">
                                    <img src="/assets/img/skin/vactor-hand.png" alt="" class="ele-hand">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div  class="section cont mg st st-list" >
            <div class="bg-fullwidth fw gen_bg bg-py-t bg-px-c bg-rp-no bg-fx-st bg-sz-cv" >
                <div class="container container-fluid ">
                    <div class="position-relative fw  pd" >
                        <div class="cont-contain ml-0 mr-0 w-100">
                            <div class="sec-title text-center" >
                                <div class="box-content">
                                    <h2 class="font-weight-bold"><span class="txt-gdcolor">โซลูชันอื่นๆ</span></h2>
                                    <div class="title-sub">ผลักดันองค์กรของคุณสู่ความสำเร็จ</div>
                                </div>
                            </div>

                            <div class="sec-solutionSlide">
                                <div class="box-solutionSlide">
                                    <div class="genSlide" data-perview-xl="5.5" data-perview-d="4.5" data-perview-t="3.5" data-perview-m="1.2" data-space-d="10" data-space-t="10" data-space-m="20">
                                        <div class="swiper-container">
                                            <div class="swiper-wrapper">
                                                <?php 
                                                $img_name = ['solution-thumb-ver-network.jpg','solution-thumb-ver-voice.jpg','solution-thumb-ver-mobility.jpg','solution-thumb-ver-iot.jpg','solution-thumb-ver-marketing.jpg','solution-thumb-ver-productivity.jpg','solution-thumb-ver-5g-infra.jpg','solution-thumb-ver-cyber-security.jpg','solution-thumb-ver-data.jpg'];
                                                $title_name = ['Network','Voice & Conferencing','Mobility','5G Infrastructure','IOT and Digital','Marketing','Productivity & Collaboration','Data Center and Cloud','Cyber Security'];
                                                for($i=0;$i<9;$i++){ 
                                                ?>
                                                <div class="swiper-slide">
                                                    <div class="box-thumb box-shadow fw position-relative text-left">
                                                        <a href="https://true5gdev.digitalnex.com/th/content/1622624239/1622612664">
                                                        <div class="thumb thumb-incard">
                                                            <div class="thumb-group">
                                                                <div class="thumb-img"><img src="/assets/img/demo/<?php echo $img_name[$i]; ?>" class="w-100 float-left"></div>
                                                                <div class="thumb-txt text-left">
                                                                    <div class="thumb-txt-h txt-upper"><b><?php echo $title_name[$i]; ?></b></div>
                                                                    <div class="thumb-txt-p txt-upper t-light">Solution</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </a>
                                                    </div>    
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <!-- <div class="swiper-pagination"></div> -->
                                        <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></div>
                                        <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/arrow-left-1.svg" class="svg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       


    </div>