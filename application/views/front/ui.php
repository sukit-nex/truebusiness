<style>
	.uitopic { float:left; width:100%; margin-bottom:10px;}
	.uibody { float:left; width:100%; background-color:#333; border-radius:5px; padding:15px; margin-bottom:50px;}
</style>
<script>
// $(document).ready(function(){
//     $('.g-feature-main .icon-toggle').each(function(){
//         $(this).parent().addClass('has-toggle');
//     });
//     $('.g-feature.active').each(function(){
//         $(this).find('.g-feature-toggle').slideDown();
//     });
//     $('.g-offer-topic').each(function(){
//         if(!$(this).hasClass('active')){
//             $(this).parent().find('.g-feature').slideUp();
//         }
//    });
//     $('.feature-list .g-feature-main').click(function(){
//         if($(this).find('div').hasClass('icon-toggle')){
//             if(!$(this).closest('.g-feature').hasClass('active')){
//                 $(this).closest('.tablepack-body').find('.g-feature').removeClass('active');
//                 $(this).closest('.tablepack-body').find('.g-feature-toggle').slideUp();
//                 console.log('')
//             }
//             $(this).closest('.g-feature').toggleClass('active');
//             $(this).closest('.g-feature').find('.g-feature-toggle').slideToggle();
//         }
//     });
//     $('.feature-list .g-offer-topic').click(function(){
//         if(!$(this).hasClass('active')){
//             $(this).closest('.tablepack-body').find('.g-offer-topic').removeClass('active');
//             $(this).closest('.tablepack-body').find('.g-offer-topic').parent().find('.g-feature').slideUp();
//         }
//         $(this).toggleClass('active');
//         $(this).parent().find('.g-feature').slideToggle();
//     });
// });
</script>


<div class="wrapper" style="background-color:#eee;">
    <div class="sec section-news box-greenline mb-0">
        <div class="sec mb-0" style="position:relative;">
            <div class="container" style=" padding:60px 0 0;">

                <div class="uitopic box-content"><h3 style="color:#131415">Cate box</h3></div>
                <div class="uibody" style="background:#fff;">
                    <div class="row">
                        <?php for($i=0;$i<3;$i++) { ?>
                        <div class="col col-12 col-md-4 ">
                            <div class="box-tablecate">
                                <div class="tablecate-list">
                                    <div class="tablecate-head">
                                        <div class="tablecate-head-group">
                                            <div class="tablecate-head-group-topic t-bold">แพ็กเกจ เน้นโทร</div>
                                            <div class="tablecate-head-group-detail">Business Digital Talk | คุยธุรกิจได้จุใจ เลือกได้ทั้งโทรผ่านเบอร์ หรือโทรผ่านแอป</div>
                                        </div>
                                    </div>
                                    <div class="tablecate-image">
                                        <img class="fw" src="/assets/img/package/sme-cate-image.jpg">
                                    </div>
                                    <div class="tablecate-detail">
                                        <div class="tablecate-detail-price">ราคา <b>399</b> บาท/เดือน</div>    
                                        <div class="tablecate-detail-group">
                                            <div class="tablecate-detail-group-list"><div>โทรฟรี 200 นาที</div></div>  
                                            <div class="tablecate-detail-group-list"><div>เน็ต 6GB</div></div>  
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="clearfix"></div>

                

                <div class="uitopic box-content"><h3 style="color:#131415">Table Price</h3></div>
                <div class="uibody" style="background:#fff;">
                    <div class="row">
                        <?php for($i=0;$i<3;$i++) { ?>
                        <div class="col col-12 col-md-4 ">
                            <div class="box-tablepack">
                                <div class="tablepack-list">
                                    <div class="tablepack-highlight"><span>แพ็กเกจแนะนำ</span></div>
                                    <div class="tablepack-head bg-gdcolor-orange">
                                        <div class="pack-price-group">
                                            <div class="pack-price t-black">399</div>
                                            <div class="pack-circle"> บาท/เดือน</div>
                                        </div>
                                    </div>
                                    <div class="tablepack-body">
                                        <div class="feature-list">
                                            <div class="g-feature active">
                                                <div class="g-feature-headline">เลือกได้ 1 รายการ</div>
                                                <div class="g-feature-main">
                                                    <div class="icon-toggle"></div>
                                                    
                                                    <div class="g-feature-icon"><img class="fw" src="/assets/img/package/icon-mobile.png"></div>
                                                    <div class="g-feature-detail"><b>โทรฟรี</b> <span><b>200</b></span> นาที<br>ทุกเครือข่าย</div>
                                                </div>
                                                <div class="g-feature-toggle">
                                                    <div class="g-feature-toggle-list">ค่าบริการส่วนเกิน 0.99 บาท/นาที</div>
                                                    <div class="g-feature-toggle-list">โทรฟรีระหว่างเบอร์</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="feature-list">
                                            <div class="g-feature">
                                                <div class="g-feature-main">
                                                    <div class="icon-toggle"></div>
                                                    <div class="g-feature-icon"><img class="fw" src="/assets/img/package/icon-mobile.png"></div>
                                                    <div class="g-feature-detail"><b>เน็ต</b> <span><b>6</b></span> GB<br>ที่ความเร็วสูงสุด 300 Mbps</div>
                                                </div>
                                                <div class="g-feature-toggle">
                                                    <div class="g-feature-toggle-list">ค่าบริการส่วนเกิน 0.99 บาท/นาที</div>
                                                    <div class="g-feature-toggle-list">โทรฟรีระหว่างเบอร์</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="feature-list">
                                            <div class="g-feature">
                                                <div class="g-feature-main">
                                                    <div class="g-feature-icon"><img class="fw" src="/assets/img/package/icon-mobile.png"></div>
                                                    <div class="g-feature-detail"><b>WiFi ไม่อั้น</b></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="feature-list">
                                            <div class="g-offer-topic bg-gdcolor-pink active">
                                                <div class="g-offer-topic-txt t-black">รับฟรี</div>
                                                <div class="icon-toggle"></div>
                                            </div>
                                            <div class="g-feature">
                                                <div class="g-feature-main">
                                                    <div class="g-feature-icon"><img src="/assets/img/package/icon-mobile.png"></div>
                                                    <div class="g-feature-detail">
                                                        <div class="box-content">ซื้อ Google Nest Mini พร้อมรับเน็ต 3G/4G เพิ่ม 2GB เพียง 99 บาท</div>
                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))">ดูรายละเอียด</a>
                                                        <div class="content-on-popup" style="display:none">
                                                            <h1>H1 Header</h1>
                                                            <h2>H2 Header</h2>
                                                            <h3>H3 Header</h3>
                                                            <h4>H4 Header</h4>
                                                            <h5>H5 Header</h5>
                                                            <h6>H6 Header</h6>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                            <ul>
                                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                                            </ul>
                                                            <ol>
                                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="g-feature">
                                                <div class="g-feature-main">
                                                    <div class="g-feature-detail">
                                                        <div class="box-content">ซื้อ Google Nest Mini พร้อมรับเน็ต 3G/4G เพิ่ม 2GB เพียง 99 บาท</div>
                                                        <a class="g-feature-detail-link" href="#">ดูรายละเอียด</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="feature-list">
                                            <div class="g-offer-topic bg-gdcolor-pink">
                                                <div class="g-offer-topic-txt t-black">ซื้อเพิ่ม ราคาพิเศษ</div>
                                                <div class="icon-toggle"></div>
                                            </div>
                                            <div class="g-feature">
                                                <div class="g-feature-main">
                                                    <div class="g-feature-icon"><img src="/assets/img/package/icon-mobile.png"></div>
                                                    <div class="g-feature-detail">
                                                        <div class="box-content">ซื้อ Google Nest Mini พร้อมรับเน็ต 3G/4G เพิ่ม 2GB เพียง 99 บาท</div>
                                                        <a class="g-feature-detail-link" href="#">ดูรายละเอียด</a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="g-feature">
                                                <div class="g-feature-main">
                                                    <div class="g-feature-detail">
                                                        <div class="box-content">ซื้อ Google Nest Mini พร้อมรับเน็ต 3G/4G เพิ่ม 2GB เพียง 99 บาท</div>
                                                        <a class="g-feature-detail-link" href="#">ดูรายละเอียด</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="feature-list">
                                            <div class="g-offer-topic bg-gdcolor-pink">
                                                <div class="g-offer-topic-txt t-black">ส่วนลดและสิทธิพิเศษ</div>
                                                <div class="icon-toggle"></div>
                                            </div>
                                            <div class="g-feature">
                                                <div class="g-feature-main">
                                                    <div class="g-feature-icon"><img src="/assets/img/package/icon-mobile.png"></div>
                                                    <div class="g-feature-detail">
                                                        <div class="box-content">ซื้อ Google Nest Mini พร้อมรับเน็ต 3G/4G เพิ่ม 2GB เพียง 99 บาท</div>
                                                        <a class="g-feature-detail-link" href="#">ดูรายละเอียด</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="g-feature">
                                                <div class="g-feature-main">
                                                    <div class="g-feature-detail">
                                                        <div class="box-content">ซื้อ Google Nest Mini พร้อมรับเน็ต 3G/4G เพิ่ม 2GB เพียง 99 บาท</div>
                                                        <a class="g-feature-detail-link" href="#">ดูรายละเอียด</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tablepack-foot">
                                        <hr>
                                        <a href="javascript:void(0)" class="btn w-100" onclick="popin('popup-content')"><span>ขอข้อมูลเพิ่มเติม</span></a>
                                        <a href="#" class="btn btn-text w-100"><span>ข้อกำหนดและเงื่อนไข</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="container container-1600" style=" padding:60px 0;">
                <div class="row">
                    <div class="col-12 col-md-12">
                        <div class="uitopic box-content"><h3 style="color:#131415">Head line</h3></div>
                        <div class="row">
                            <div class="col-12 col-md-12 col-xl-12">
                                <div class="uibody" style="background:#fff">
                                    <div class="sec-title text-center">
                                        <div class="box-content">
                                            <h1 class="title-main font-weight-bold"><span class="txt-gdcolor">โซลูชันเพื่อลูกค้าธุรกิจ</span></h1>
                                            <div class="title-sub">ผลักดันองค์กรของคุณสู่ความสำเร็จ</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-12 col-md-12 col-xl-12">
                                <div class="uibody">
                                    <div class="sec-title text-center">
                                        <div class="box-content light-content">
                                            <h1 class="title-main font-weight-bold"><span class="txt-gdcolor">ผลงานของพวกเรา</span></h1>
                                            <div class="title-sub">ประสบการณ์ความเชี่ยวชาญในธุรกิจต่างๆ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="col-12 col-md-12">
                        <div class="uitopic box-content"><h3 style="color:#131415">Content Detail</h3></div>
                        <div class="row">
                            <div class="col-12 col-md-12 col-xl-6">
                                <div class="uibody" style="background:#fff">
                                    <div class="box-content">
                                        <h1>H1 Header</h1>
                                        <h2>H2 Header</h2>
                                        <h3>H3 Header</h3>
                                        <h4>H4 Header</h4>
                                        <h5>H5 Header</h5>
                                        <h6>H6 Header</h6>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                        <ul>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                        </ul>
                                        <ol>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                        </ol>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 col-xl-6">
                                <div class="uibody">
                                    <div class="box-content light-content">
                                        <h1>H1 Header</h1>
                                        <h2>H2 Header</h2>
                                        <h3>H3 Header</h3>
                                        <h4>H4 Header</h4>
                                        <h5>H5 Header</h5>
                                        <h6>H6 Header</h6>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                        <ul>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                        </ul>
                                        <ol>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</li>
                                        </ol>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12">
                        <div class="uitopic box-content"><h3 style="color:#131415">Color</h3></div>
                        <div class="uibody"  style="background:#fff">
                            <div class="row">
                                <div class="col col-12 col-md-2"><b>Primary color</b><br>.txt-gdcolor</div>
                                <div class="col col-12 col-md-5">
                                    <div class="box-content">
                                        <h1><span class="txt-gdcolor"><b>Primary color</b></span></h1>
                                    </div>
                                </div>
                                <div class="col col-12 col-md-5">
                                    .bg-gdcolor 
                                    <div class="bg-gdcolor">.....</div>
                                </div>
                                <div class="box-content"><hr></div>
                                <div class="col col-12 col-md-2"><b>Blue color</b><br>.txt-gdcolor-blue</div>
                                <div class="col col-12 col-md-5">
                                    <div class="box-content">
                                        <h1><span class="txt-gdcolor-blue"><b>Blue color</b></span></h1>
                                    </div>
                                </div>
                                <div class="col col-12 col-md-5">
                                    .bg-gdcolor-blue
                                    <div class="bg-gdcolor-blue">.....</div>
                                </div>

                                <div class="col col-12 col-md-2"><b>Orange color</b><br>.txt-gdcolor-orange</div>
                                <div class="col col-12 col-md-5">
                                    <div class="box-content">
                                        <h1><span class="txt-gdcolor-orange "><b>Orange color</b></span></h1>
                                    </div>
                                </div>
                                <div class="col col-12 col-md-5">
                                    .bg-gdcolor-orange
                                    <div class="bg-gdcolor-orange">.....</div>
                                </div>

                                <div class="col col-12 col-md-2"><b>Pink color</b><br>.txt-gdcolor-pink</div>
                                <div class="col col-12 col-md-5">
                                    <div class="box-content">
                                        <h1><span class="txt-gdcolor-pink "><b>Pink color</b></span></h1>
                                    </div>
                                </div>
                                <div class="col col-12 col-md-5">
                                    .bg-gdcolor-pink
                                    <div class="bg-gdcolor-pink">.....</div>
                                </div>
                                
                                <div class="col col-12 col-md-2"><b>Dark blue color</b><br>.txt-gdcolor-darkblue</div>
                                <div class="col col-12 col-md-5">
                                    <div class="box-content">
                                        <h1><span class="txt-gdcolor-darkblue "><b>Dark blue color</b></span></h1>
                                    </div>
                                </div>
                                <div class="col col-12 col-md-5">
                                    .bg-gdcolor-darkblue
                                    <div class="bg-gdcolor-darkblue">.....</div>
                                </div>

                                <div class="col col-12 col-md-2"><b>Red color</b><br>.txt-gdcolor-red</div>
                                <div class="col col-12 col-md-5">
                                    <div class="box-content">
                                        <h1><span class="txt-gdcolor-red"><b>Red color</b></span></h1>
                                    </div>
                                </div>
                                <div class="col col-12 col-md-5">
                                    .bg-gdcolor-red
                                    <div class="bg-gdcolor-red">.....</div>
                                </div>
                                <div class="col col-12 col-md-5 mt-5">
                                    <div class="box-shadow" ><div class="position-relative d-flex align-items-center justify-content-center" style="min-height:150px; width:100%; border-radius:30px; background-color:#fff; border-radius:10px;"><p>.box-shadow</p></div></div> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-12">
                        <div class="uitopic box-content"><h3 style="color:#131415">Font style</h3></div>
                        <div class="uibody"  style="background:#fff">
                            <div class="row">
                                <div class="col col-12 col-md-3 box-content">
                                    <h4 class="t-light">Light</h4>
                                    <p>.t-light<br>font-family: 'true_lightregular';</p>
                                </div>
                                <div class="col col-12 col-md-3 box-content">
                                    <h4 class="t-medium">Medium</h4>
                                    <p>.t-medium<br>font-family: 'true_mediumregular';</p>
                                </div>
                                <div class="col col-12 col-md-3 box-content">
                                    <h4 class="t-bold">Bold</h4>
                                    <p>.t-bold or tag b,stong<br>font-family: 'true_boldregular';</p>
                                </div>
                                <div class="col col-12 col-md-3 box-content">
                                    <h4 class="t-black">Black</h4>
                                    <p>.t-black<br>font-family: 'true_bold_specialregular';</p>
                                </div>
                                <div class="col col-12 col-md-3 mt-5 box-content">
                                    <h4 class="txt-upper">Uppercase</h4>
                                    <p>.txt-upper or tag u<br>font-family: 'true_mediumregular';</p>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-12">
                        <div class="uitopic box-content"><h3 style="color:#131415">Button</h3></div>
                        <div class="uibody" style="background:#fff; color:#000">
                            <div class="row">
                                <div class="col col-12 col-md-6">
                                    <p class=" mb-3">Button Primary</p>
                                    <a href="javascript:void(0)" class="btn"><span>ดูรายละเอียด</span></a>
                                    <br><br>
                                    <p class=" mb-3">Button Size</p>
                                    <a href="#" class="btn btn-sm"><span>ดูรายละเอียด</span></a>
                                    <a href="#" class="btn"><span>ดูรายละเอียด</span></a>
                                    <a href="#" class="btn btn-lg"><span>ดูรายละเอียด</span></a>
                                    <p class=" mb-3">Button Icon</p>
                                    <a href="#" class="btn btn-icon"><span><span>ดูรายละเอียด</span><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></span></a>
                                    
                                    <a href="#" class="btn btn-icon"><span><img src="/assets/img/skin/arrow-left-1.svg" class="svg"><span>ดูรายละเอียด</span></span></a>
                                    
                                    <p class="mt-3 mb-3">Text link</p>
                                    <a href="#" class="btn btn-text"><span>ดูรายละเอียด<span></a>
                                    <a href="#" class="btn btn-text btn-icon"><span><span>ดูรายละเอียด</span><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></span></a>
                                    <a href="#" class="btn btn-text btn-icon btn-download"><span><img src="/assets/img/skin/icon-download.svg" class="svg"><span>ดูรายละเอียด</span></span></a>
                                    <p class="mt-3 mb-3">Tag</p>
                                    <a href="#" class="btn btn-red btn-sm"><span>Button Tag 1<span></a>
                                    <a href="#" class="btn btn-red btn-sm btn-blue"><span>Button Tag 2<span></a>
                                    <a href="#" class="btn btn-red btn-sm btn-outline"><span>Button Tag 3<span></a>
                                    <br><br>
                                    <p class="mt-3 mb-3">On content</p>
                                    <div class="box-content text-left float-left w-100  position-relative btn-list-more"  style="z-index:10;">
                                        <a target="_blank"  href="" class="btn btn-skew btn-loadmore" ><span>ดูเพิ่มเติม</span></a>
                                    </div>

                                </div>
                                <div class="col col-12 col-md-6">
                                    <p class=" mb-3">Button Secondary</p>
                                    <a href="javascript:void(0)" class="btn btn-secondary"><span>ดูรายละเอียด</span></a>
                                    <br><br>
                                    <p class=" mb-3">Button Size</p>
                                    <a href="#" class="btn btn-sm btn-secondary"><span>ดูรายละเอียด</span></a>
                                    <a href="#" class="btn btn-secondary"><span>ดูรายละเอียด</span></a>
                                    <a href="#" class="btn btn-lg btn-secondary"><span>ดูรายละเอียด</span></a>
                                    <p class=" mb-3">Button Icon</p>
                                    <a href="#" class="btn btn-icon btn-secondary"><span><span>ดูรายละเอียด</span><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></span></a>
                                    
                                    <a href="#" class="btn btn-icon btn-secondary"><span><img src="/assets/img/skin/arrow-left-1.svg" class="svg"><span>ดูรายละเอียด</span></span></a>
                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="col-12 col-md-12">
                        <div class="uitopic box-content"><h3 style="color:#131415">Form</h3></div>
                        <div class="uibody"  style="background:#fff">
                            <!-- <div class="box-search-sidebar">
                                <div class="form-group">
                                    <input type="text" name="search" class="input-list" placeholder="ค้นหาบทความ">
                                    <button class="button-search"><img src="assets/img/skin/icon-search.svg" class="svg"></button>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col col-12">
                                    <form action="">
                                        <div class="row">
                                            <div class="col col-12 col-md-6">
                                                <div class="form-group">
                                                    <label for="">ชื่อ *</label>
                                                    <input type="text" data-required data-error-message="กรุณาระบุข้อมูล">
                                                </div>
                                            </div>
                                            <div class="col col-12 col-md-6">
                                                <div class="form-group">
                                                    <label for="">นามสกุล *</label>
                                                    <input type="text" data-required data-error-message="กรุณาระบุข้อมูล">
                                                </div>
                                            </div>
                                            <div class="col col-12 col-md-6">
                                                <div class="form-group">
                                                    <label for="">เบอร์โทรศัพท์มือถือ *</label>
                                                    <input type="text" data-required data-error-message="กรุณาระบุข้อมูล">
                                                </div>
                                            </div>
                                            <div class="col col-12 col-md-6">
                                                <div class="form-group">
                                                    <label for="">อีเมล *</label>
                                                    <input type="text" data-required data-error-message="กรุณาระบุข้อมูล" >
                                                </div>
                                            </div>
                                            <div class="col col-12 col-md-6 ">
                                                <div class="form-group ">
                                                    <label for="">ตำแหน่งงาน</label>
                                                    <select class="selectpicker select-default" title="Select Topic" data-required data-error-message="กรุณาระบุข้อมูล">
                                                        <option>ประเภทงานทั้งหมด</option>
                                                        <option>Ketchup</option>
                                                        <option>Barbecue</option>
                                                        <option>Ketchup</option>
                                                        <option>Barbecue</option>
                                                        <option>Ketchup</option>
                                                        <option>Barbecue</option>
                                                        <option>Ketchup</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col col-12 col-md-6">
                                                <div class="form-group">
                                                    <label for="">อีเมล *</label>
                                                    <input type="text" data-required data-error-message="กรุณาระบุข้อมูล" >
                                                </div>
                                            </div>
                                            <div class="col col-12 col-md-6">
                                                <div class="form-group">
                                                    <label for="">แนบเอกสาร *</label>
                                                    <label class="file-input__label" for="file-1">
                                                        <span class="placeholder">เลือกไฟล์</span>
                                                        <span class="upload-icon">
                                                            <img class="svg" src="/assets/img/skin/icon-upload.svg" alt="">
                                                        </span>
                                                    </label >
                                                    <input type="file" name="file-1" id="file-1" class="file-input__input" data-required data-error-message="กรุณาระบุข้อมูล" />

                                                    <div class="info-small">.doc, .docx, .ppt, .pptx, .pdf, .jpg, .jpeg, .png</div>
                                                </div>
                                            </div>
                                            <div class="col col-12 col-md-6">
                                                <div class="form-group">
                                                    <label for="">วิดีโอ *</label>
                                                    <label class="file-input__label" for="fill-2">
                                                        <span class="placeholder">เลือกไฟล์</span>
                                                        <span class="upload-icon">
                                                            <img class="svg" src="/assets/img/skin/icon-upload.svg" alt="">
                                                        </span>
                                                    </label >
                                                    <input type="file" name="fill-2" id="fill-2" class="file-input__input" data-required data-error-message="กรุณาระบุข้อมูล" />

                                                    <div class="info-small">.mp4, .mov, .avi</div>
                                                </div>
                                            </div>
                                            <div class="col col-12 col-md-12">
                                                <div class="form-group">
                                                    <div class="check-group">
                                                        <div class="check-list">
                                                            <input id="ca0" type="checkbox" name="prefer-check" value="phone call" class="check" >
                                                            <label for="ca0">ข้าพเจ้ายอมรับ <a href="http://" class="" target="_blank" rel="noopener noreferrer">นโยบายส่วนบุคคล</a> และขอรับรองว่าคำตอบและข้อความทั้งหมดที่ข้าพเจ้าให้ไว้ในใบสมัครของบริษัทกรุงศรี คอนซูมเมอร์นี้เป็นความจริงทุกประการ 
                                                        หากว่าทางบริษัทฯ รับข้าพเจ้าเป็นพนักงานแล้วและต่อมาภายหลังพบว่าข้อความตอนหนึ่งตอนใดเป็นเท็จหรือไม่ตรงกับความจริง ข้าพเจ้ายินยอมที่จะให้บริษัทฯ 
                                                        ปลดข้าพเจ้าออกจากการเป็นพนักงานทันทีโดยมิต้องจ่ายค่าชดเชยใดๆ</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    </form>
                                </div>
                                
                            </div>
                            <div class="row">
                                
                                <div class="col col-12 col-md-12 form-group mt-3">
                                    <div class="radio-group radioinline">
                                        <div class="radio-title">Radio : Horizontal</div>
                                        <div class="radio-list">
                                            <input id="ra1" type="radio" name="prefer-radio" value="email" class="prefer" checked>
                                            <label for="ra1">Email</label>
                                        </div>
                                        <span class="radio-line">|</span>
                                        <div class="radio-list">
                                            <input id="ra2" type="radio" name="prefer-radio" value="phone call" class="prefer">
                                            <label for="ra2">Phone Call</label>
                                        </div>
                                    </div> 
                                </div>
                                <div class="col col-12 col-md-6 form-group">
                                    <div class="radio-group">
                                        <div class="radio-title">Radio : vertical</div>
                                        <div class="radio-list">
                                            <input id="ra3" type="radio" name="prefer-radio2" value="email" class="prefer" checked>
                                            <label for="ra3">Email</label>
                                        </div>
                                        <div class="radio-list">
                                            <input id="ra4" type="radio" name="prefer-radio2" value="phone call" class="prefer">
                                            <label for="ra4">Phone Call</label>
                                        </div>
                                    </div> 
                                </div>
                                <div class="col col-12 col-md-6 form-group">
                                    <div class="check-group">
                                        <div class="check-title">Checkbox : vertical</div>
                                        <div class="check-list">
                                            <input id="ca1" type="checkbox" name="prefer-check" value="email" class="check" checked>
                                            <label for="ca1">Email</label>
                                        </div>
                                        <div class="check-list">
                                            <input id="ca2" type="checkbox" name="prefer-check" value="phone call" class="check">
                                            <label for="ca2">Phone Call</label>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="uibody" style="background:#fff">
                            <div class="row justify-content-center">
                                <div class="col col-12 col-md-9">
                                    <div class="form-group form-group-hoz">
                                        <label for="">ชื่อ *</label>
                                        <input type="text" data-required data-error-message="กรุณาระบุข้อมูล">
                                    </div>
                                    <div class="form-group form-group-hoz">
                                        <label for="">ชื่อ *</label>
                                        <textarea name="" id="" cols="30" rows="3"></textarea>
                                    </div>
                                    <div class="form-group form-group-hoz ">
                                        <label for="">ตำแหน่งงาน</label>
                                        <select class="selectpicker select-default" title="Select Topic" data-required data-error-message="กรุณาระบุข้อมูล">
                                            <option>ประเภทงานทั้งหมด</option>
                                            <option>Ketchup</option>
                                            <option>Barbecue</option>
                                            <option>Ketchup</option>
                                            <option>Barbecue</option>
                                            <option>Ketchup</option>
                                            <option>Barbecue</option>
                                            <option>Ketchup</option>
                                        </select>
                                    </div>
                                    <div class="form-group form-group-hoz">
                                        <label for="">วิดีโอ * <div class="info-small">.mp4, .mov, .avi</div></label>
                                        <label class="file-input__label" for="fill-2">
                                            <span class="placeholder">เลือกไฟล์</span>
                                            <span class="upload-icon">
                                                <img class="svg" src="/assets/img/skin/icon-upload.svg" alt="">
                                            </span>
                                        </label >
                                        <input type="file" name="fill-2" id="fill-2" class="file-input__input" data-required data-error-message="กรุณาระบุข้อมูล" />

                                        
                                    </div>
                                    <div class="form-group form-group-hoz">
                                        <div class="radio-group radioinline">
                                            <div class="radio-title"></div>
                                            <div class="radio-list">
                                                <input id="ra1" type="radio" name="prefer-radio" value="email" class="prefer" checked>
                                                <label for="ra1">Email</label>
                                            </div>
                                            <span class="radio-line">|</span>
                                            <div class="radio-list">
                                                <input id="ra2" type="radio" name="prefer-radio" value="phone call" class="prefer">
                                                <label for="ra2">Phone Call</label>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="form-group form-group-hoz">
                                        <div class="check-group checkinline">
                                            <div class="check-title"></div>
                                            <div class="check-list">
                                                <input id="ca1" type="checkbox" name="prefer-check" value="email" class="check" checked>
                                                <label for="ca1">Email</label>
                                            </div>
                                            <span class="check-line">|</span>
                                            <div class="check-list">
                                                <input id="ca2" type="checkbox" name="prefer-check" value="phone call" class="check">
                                                <label for="ca2">Phone Call</label>
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="form-group form-group-hoz">
                                        <div class="check-group checkinline">
                                            <div class="check-title"></div>
                                            <div class="check-list">
                                                <input id="ca0" type="checkbox" name="prefer-check" value="phone call" class="check" >
                                                <label for="ca0">ข้าพเจ้ายอมรับ <a href="http://" class="" target="_blank" rel="noopener noreferrer">นโยบายส่วนบุคคล</a> และขอรับรองว่าคำตอบและข้อความทั้งหมดที่ข้าพเจ้าให้ไว้ในใบสมัครของบริษัทกรุงศรี คอนซูมเมอร์นี้เป็นความจริงทุกประการ 
                                            หากว่าทางบริษัทฯ รับข้าพเจ้าเป็นพนักงานแล้วและต่อมาภายหลังพบว่าข้อความตอนหนึ่งตอนใดเป็นเท็จหรือไม่ตรงกับความจริง ข้าพเจ้ายินยอมที่จะให้บริษัทฯ 
                                            ปลดข้าพเจ้าออกจากการเป็นพนักงานทันทีโดยมิต้องจ่ายค่าชดเชยใดๆ</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center fw"><button class="btn"><span>ส่งข้อมูล</span></button></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-12">
                        <div class="uitopic box-content"><h3 style="color:#131415">Button</h3></div>
                        <div class="uibody" style="background:#fff; color:#000">

                            <div class="row">
                                <div class="col col-12">
                                    <div class="sec-acc-group position-relative fw">
                                            
                                        <div class="sec-title text-center">
                                            <div class="box-content ">
                                                <h1 class="title-main font-weight-bold"><span class="txt-gdcolor">Accordion</span></h1>
                                                <p class="title-sub">รูปแบบการใช้ Slide Content แบบ Accordion</p>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="tab-content fw">
                                            <div class="fw">
                                                <div class="box-accordion fw mb-4">
                                                    <div class="accor-list box-content default">
                                                        <div class="accor-head">
                                                            <div class="accor-icon"><p><strong>Q</strong></p></div>
                                                            <div class="accor-head-topic"><p><span><strong>ฮัมดีลเลอร์เฟรชสะกอม</strong></span><p></div>
                                                            <div class="accor-head-symbol"></div>
                                                        </div>
                                                        
                                                        <div class="accor-body">
                                                            <div class="accor-icon"><p><strong>A</strong></p></div>
                                                            <div class="accor-body-content">คาวบอยเซ็กส์แอปเปิล แล็บไฮไลท์วอล์กมาร์ตวอลนัต ซาดิสม์ไทเฮาลิสต์เปเปอร์ ฮิปโป ฮัมดีลเลอร์เฟรชสะกอม มอคค่าชีสจิ๊กโก๋สตาร์รูบิก มะกันอึ้มแรงดูด พงษ์ทัวริสต์ ลีกสเก็ตช์เทรลเล่อร์ตุ๊ดคอร์ปอเรชั่น สัมนาเครปฟีเวอร์นิวส์โฮลวีต เวณิกาคาสิโนสุนทรีย์เช็งเม้ง ไทยแลนด์ แมคเคอเรลสวีทกัมมันตะแยมโรลเรต ซะ สวีทไอซียูกิมจิ ลิมูซีนคีตราชันแรงดูดเยลลี่คอนแท็ค สามแยกหมวยเอาท์บัลลาสต์ สารขัณฑ์ฟลุท แจมรีพอร์ทแซลมอน คอนเซปต์ความหมาย ผู้นำ สามแยกอาร์พีจี โทรโข่งวอล์กออร์เดอร์โฮปบ็อกซ์ ซีนอันตรกิริยา เต๊ะทับซ้อน ราชานุญาต เซ็กซ์อินเตอร์โปลิศ ยากูซ่าคอร์ส ไพลินสต๊อค เลคเชอร์ คอมเพล็กซ์แรงดูด</div>
                                                        </div>
                                                    </div>
                                                    <div class="accor-list box-content default">
                                                        <div class="accor-head">
                                                            <div class="accor-icon"><p><strong>Q</strong></p></div>
                                                            <div class="accor-head-topic"><p><span><strong>ฮัมดีลเลอร์เฟรชสะกอม</strong></span><p></div>
                                                            <div class="accor-head-symbol"></div>
                                                        </div>
                                                        
                                                        <div class="accor-body">
                                                            <div class="accor-icon"><p><strong>A</strong></p></div>
                                                            <div class="accor-body-content">คาวบอยเซ็กส์แอปเปิล แล็บไฮไลท์วอล์กมาร์ตวอลนัต ซาดิสม์ไทเฮาลิสต์เปเปอร์ ฮิปโป ฮัมดีลเลอร์เฟรชสะกอม มอคค่าชีสจิ๊กโก๋สตาร์รูบิก มะกันอึ้มแรงดูด พงษ์ทัวริสต์ ลีกสเก็ตช์เทรลเล่อร์ตุ๊ดคอร์ปอเรชั่น สัมนาเครปฟีเวอร์นิวส์โฮลวีต เวณิกาคาสิโนสุนทรีย์เช็งเม้ง ไทยแลนด์ แมคเคอเรลสวีทกัมมันตะแยมโรลเรต ซะ สวีทไอซียูกิมจิ ลิมูซีนคีตราชันแรงดูดเยลลี่คอนแท็ค สามแยกหมวยเอาท์บัลลาสต์ สารขัณฑ์ฟลุท แจมรีพอร์ทแซลมอน คอนเซปต์ความหมาย ผู้นำ สามแยกอาร์พีจี โทรโข่งวอล์กออร์เดอร์โฮปบ็อกซ์ ซีนอันตรกิริยา เต๊ะทับซ้อน ราชานุญาต เซ็กซ์อินเตอร์โปลิศ ยากูซ่าคอร์ส ไพลินสต๊อค เลคเชอร์ คอมเพล็กซ์แรงดูด</div>
                                                        </div>
                                                    </div>
                                                    <div class="accor-list box-content default">
                                                        <div class="accor-head">
                                                            <div class="accor-icon"><p><strong>Q</strong></p></div>
                                                            <div class="accor-head-topic"><p><span><strong>ฮัมดีลเลอร์เฟรชสะกอม</strong></span><p></div>
                                                            <div class="accor-head-symbol"></div>
                                                        </div>
                                                        
                                                        <div class="accor-body">
                                                            <div class="accor-icon"><p><strong>A</strong></p></div>
                                                            <div class="accor-body-content">คาวบอยเซ็กส์แอปเปิล แล็บไฮไลท์วอล์กมาร์ตวอลนัต ซาดิสม์ไทเฮาลิสต์เปเปอร์ ฮิปโป ฮัมดีลเลอร์เฟรชสะกอม มอคค่าชีสจิ๊กโก๋สตาร์รูบิก มะกันอึ้มแรงดูด พงษ์ทัวริสต์ ลีกสเก็ตช์เทรลเล่อร์ตุ๊ดคอร์ปอเรชั่น สัมนาเครปฟีเวอร์นิวส์โฮลวีต เวณิกาคาสิโนสุนทรีย์เช็งเม้ง ไทยแลนด์ แมคเคอเรลสวีทกัมมันตะแยมโรลเรต ซะ สวีทไอซียูกิมจิ ลิมูซีนคีตราชันแรงดูดเยลลี่คอนแท็ค สามแยกหมวยเอาท์บัลลาสต์ สารขัณฑ์ฟลุท แจมรีพอร์ทแซลมอน คอนเซปต์ความหมาย ผู้นำ สามแยกอาร์พีจี โทรโข่งวอล์กออร์เดอร์โฮปบ็อกซ์ ซีนอันตรกิริยา เต๊ะทับซ้อน ราชานุญาต เซ็กซ์อินเตอร์โปลิศ ยากูซ่าคอร์ส ไพลินสต๊อค เลคเชอร์ คอมเพล็กซ์แรงดูด</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12">
                        <div class="uitopic box-content"><h3 style="color:#131415">Thumb Verticle</h3></div>
                        <div class="uibody" style="background:#fff; color:#000">
                            <div class="row st">
                                <?php for($i=0;$i<3;$i++){ ?>
                                <div class="col col-12 col-md-4">
                                    <div class="box-thumb fw position-relative text-left">
                                        <a href="https://true5gdev.digitalnex.com/th/content/1622624239/1622612664">
                                        <div class="thumb thumb-ver">
                                            <div class="thumb-group">
                                                <div class="thumb-img"><img src="/assets/img/template/image-default.jpg" class="w-100 float-left"></div>
                                                <div class="thumb-txt text-left">
                                                    <div class="thumb-txt-h"><span>No.01 3 โหมดการใช้งาน เพื่อคอเกมตัวจริง</span></div>
                                                    <div class="thumb-txt-p">บัญชีสมาชิค Netboom เดิมสามารถใช้งานได้ รองรับเฉพาะระบบ Android เท่านั้น</div>
                                                    <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span>16 มิ.ย. 60</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        </a>
                                    </div>    
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php /* ?>               
                    <div class="col-12 col-md-12">
                        <div class="uitopic box-content"><h3 style="color:#131415">Button</h3></div>
                        <div class="uibody" style="background:#fff; color:#000">
                            <div class="row st">
                                <?php for($i=0;$i<3;$i++){ ?>
                                <div class="col col-12 col-md-4">
                                    <div class="box-thumb fw position-relative text-left">
                                        <a href="https://true5gdev.digitalnex.com/th/content/1622624239/1622612664">
                                        <div class="thumb thumb-incard">
                                            <div class="thumb-group">
                                                <div class="thumb-img"><img src="/assets/img/template/image-default.jpg" class="w-100 float-left"></div>
                                                <div class="thumb-txt text-left">
                                                    <div class="thumb-txt-h"><span>No.01 3 โหมดการใช้งาน เพื่อคอเกมตัวจริง</span></div>
                                                    <div class="thumb-txt-p">บัญชีสมาชิค Netboom เดิมสามารถใช้งานได้ รองรับเฉพาะระบบ Android เท่านั้น</div>
                                                    <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span>16 มิ.ย. 60</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        </a>
                                    </div>    
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php */ ?>   
                    

                    <div class="col-12 col-md-12">
                        <div class="uitopic box-content"><h3 style="color:#131415">Thumb Horizontal</h3></div>
                        <div class="uibody" style="background:#fff; color:#000">
                            <div class="row st">
                                <?php for($i=0;$i<4;$i++){ ?>
                                <div class="col col-12 col-md-6">
                                    <div class="box-thumb fw position-relative text-left">
                                        <a href="https://true5gdev.digitalnex.com/th/content/1622624239/1622612664">
                                        <div class="thumb thumb-hoz">
                                            <div class="thumb-group">
                                                <div class="thumb-img"><img src="/assets/img/template/image-default.jpg" class="w-100 float-left"></div>
                                                <div class="thumb-txt text-left">
                                                    <div class="thumb-txt-h"><span>No.01 3 โหมดการใช้งาน เพื่อคอเกมตัวจริง</span></div>
                                                    <div class="thumb-txt-p">บัญชีสมาชิค Netboom เดิมสามารถใช้งานได้ รองรับเฉพาะระบบ Android เท่านั้น</div>
                                                    <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span>16 มิ.ย. 60</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        </a>
                                    </div>    
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-12">
                        <div class="uitopic box-content"><h3 style="color:#131415">Slide</h3></div>
                        <div class="uibody st" style="background:#fff; color:#000">
                                <p>data-space-xl="30"</p>
                                <p>data-space-d="30"</p>
                                <p>data-space-t="30"</p>
                                <p>data-space-m="30"</p>
                                <p>data-perview-xl</p>
                                <p>data-perview-d</p>
                                <p>data-perview-t</p>
                                <p>data-perview-m="1"</p>
                                <p>data-percol="1"</p>
                                <p>data-percol-t="1"</p>
                                <p>data-percol-m="1"</p>            
                                <p>data-loop="false"</p>
                                <p>data-speed="800"</p>
                                <div class="clearfix"></div>
                            <div class="genSlide position-relative" data-perview-d="4" data-perview-t="2" data-perview-m="1" data-space-d="10" data-percol-d="1" data-loop="false" data-speed="800">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <?php for($i=0;$i<10;$i++){ ?>
                                        <div class="swiper-slide">
                                            <div class="box-thumb fw position-relative text-left">
                                                <a href="https://true5gdev.digitalnex.com/th/content/1622624239/1622612664">
                                                <div class="thumb thumb-ver">
                                                    <div class="thumb-group">
                                                        <div class="thumb-img"><img src="/assets/img/template/image-default.jpg" class="w-100 float-left"></div>
                                                        <div class="thumb-txt text-left">
                                                            <div class="thumb-txt-h"><span>No.01 3 โหมดการใช้งาน เพื่อคอเกมตัวจริง</span></div>
                                                            <div class="thumb-txt-p">บัญชีสมาชิค Netboom เดิมสามารถใช้งานได้ รองรับเฉพาะระบบ Android เท่านั้น</div>
                                                            <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span>16 มิ.ย. 60</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </a>
                                            </div>    
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="swiper-pagination"></div>
                                <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></div>
                                <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/arrow-left-1.svg" class="svg"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sec section-news box-greenline mb-0 st">
        <div class="sec mb-0" style="position:relative;">
            <div class="container container-1600">
                <?php for($i=0;$i<4;$i++){ ?>
                <div class="box-thumb fw position-relative text-left">
                    <div class="thumb thumb-hoz thumb-lr">
                        <div class="thumb-group">
                            <div class="thumb-img <?php echo ($i%2 == 0)? 'order-first order-md-2':''; ?>"><img src="/assets/img/template/image-default.jpg" class="w-100 float-left"></div>
                            <div class="thumb-txt text-left <?php echo ($i%2 == 0)? 'order-2 order-md-first':''; ?>">
                                <div class="thumb-txt-h"><span class="txt-gdcolor">โซลูชันด้านโครงข่ายเน็ตเวิร์ก</span></div>
                                <div class="thumb-txt-s"><b>มั่นใจทุกการเชื่อมต่อ และรับ-ส่งข้อมูลออนไลน์</b></div>
                                <div class="thumb-txt-p">สร้างระบบโครงข่ายความเร็วสูงที่มีเสถียรภาพ สำหรับแลกเปลี่ยนข้อมูลระหว่างสาขาในองค์กร และเพิ่มโอกาสการทำธุรกิจให้กว้างไกลระดับสากล ด้วยเทคโนโลยีการเชื่อมต่ออันหลากหลาย ที่พร้อมตอบโจทย์ความต้องการของธุรกิจทุกขนาด</div>
                                <div class="thumb-txt-a">
                                    <a class="btn" target="_blank" href="https://store.truecorp.co.th/packages?type=all"><span>รายละเอียดเพิ่มเติม</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <?php } ?>
            </div>
        </div>
    </div>
</div>