<div class="loadpage"></div>
<div class="wrapper">

    <!--
    |*******************************************************************************************
    | SECTION Z-SCROLL
    |*******************************************************************************************
    -->
    <div class="section cont st st-zscroll">
        <div class="position-relative fw d-none d-lg-block">
            <div class="sec-zscroll" style="background-image:url(/assets/img/vr/bg-section-3-d.jpg);">
                <div class="container container-1600 ">
                    <div class="position-relative fw">
                        <div class="cont-contain">
                            <div class="box-slide-z slide-z-1" >
                                <div class="slide-z-img"><img src="/assets/img/vr/sec-3-img-1.png"></div>
                                <div class="slide-z-txt"><img src="/assets/img/vr/sec-3-txt-1.png"></div>
                            </div>
                            <div class="box-slide-z slide-z-2" >
                                <div class="slide-z-img"><img src="/assets/img/vr/sec-3-img-2.png"></div>
                                <div class="slide-z-txt"><img src="/assets/img/vr/sec-3-txt-2.png"></div>
                            </div>
                            <div class="box-slide-z slide-z-3">
                                <div class="slide-z-img"><img src="/assets/img/vr/sec-3-img-3.png"></div>
                                <div class="slide-z-txt"><img src="/assets/img/vr/sec-3-txt-3.png"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="zscroll-blank position-relative fw "></div>
        </div>
    </div>

    <!--
    |*******************************************************************************************
    | SECTION CONTENT
    |*******************************************************************************************
    -->
	<div class="section cont bg-fullwidth st st-content contlay-hoz-left" style="background-image:url(/assets/img/template/bg-d.jpg);">
        <div class="container container-1600">
            <div class="position-relative fw">
            	<div class="cont-contain">
                    <div class="cont-img">
                    	<img src="/assets/img/ar/sec-2-img.png">
                    </div>
                	<div class="cont-txt text-center">
                    	<div class="row" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                        	<div class="col col-12 nexitor-col">
                            	<div class="box-content box-content-editor undefined"><p>ความสนุกครั้งใหม่กับ</p></div>
                                <div class="box-content box-content-space"></div>
                                <div class="box-content none"> <h1 class="font-weight-bold  " style="undefined">โลกเสมือนจริง <br>อัจฉริยะ</h1></div>
                                <div class="box-content box-content-space"></div>
                                <div class="box-content box-content-editor undefined">
                                	<p>ครั้งแรกกับประสบการณ์ <span style="color: #ff0000;" data-mce-style="color: #ff0000;">AR Filter</span> สุดล้ำ ไม่เหมือนใคร<br>สนุกได้ทุกที่ไม่มีเหงา ถ่ายรูปแบบล้ำๆด้วยระบบ <span style="color: #ff0000;" data-mce-style="color: #ff0000;">AR 3 มิติ</span><br>ที่จะทำให้การถ่ายรูปของคุณไม่น่าเบื่ออีกต่อไป</p>
                                </div>
                                <div class="box-content box-content-space"></div>
                                <div class="box-content none"> <img src="/uploads/logo-true5g-ar_1612172872.png" class="w-50"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section cont bg-fullwidth st st-content contlay-hoz-right" style="background-image:url(/assets/img/template/bg-d.jpg);">
        <div class="container container-1600">
            <div class="position-relative fw">
            	<div class="cont-contain">
                    <div class="cont-img">
                    	<img src="/assets/img/ar/sec-2-img.png">
                    </div>
                	<div class="cont-txt text-center">
                    	<div class="row" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                        	<div class="col col-12 nexitor-col">
                            	<div class="box-content box-content-editor undefined"><p>ความสนุกครั้งใหม่กับ</p></div>
                                <div class="box-content box-content-space"></div>
                                <div class="box-content none"> <h1 class="font-weight-bold  " style="undefined">โลกเสมือนจริง <br>อัจฉริยะ</h1></div>
                                <div class="box-content box-content-space"></div>
                                <div class="box-content box-content-editor undefined">
                                	<p>ครั้งแรกกับประสบการณ์ <span style="color: #ff0000;" data-mce-style="color: #ff0000;">AR Filter</span> สุดล้ำ ไม่เหมือนใคร<br>สนุกได้ทุกที่ไม่มีเหงา ถ่ายรูปแบบล้ำๆด้วยระบบ <span style="color: #ff0000;" data-mce-style="color: #ff0000;">AR 3 มิติ</span><br>ที่จะทำให้การถ่ายรูปของคุณไม่น่าเบื่ออีกต่อไป</p>
                                </div>
                                <div class="box-content box-content-space"></div>
                                <div class="box-content none"> <img src="/uploads/logo-true5g-ar_1612172872.png" class="w-50"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section cont bg-fullwidth st st-content contlay-ver" style="background-image:url(/assets/img/template/bg-d.jpg);">
        <div class="container container-1600">
            <div class="position-relative fw">
            	<div class="cont-contain">
                    <div class="cont-img">
                    	<img src="/assets/img/template/graphic-circle-1.png">
                    </div>
                	<div class="cont-txt">
                    	<div class="row" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                        	<div class="col col-12 nexitor-col">
                                <div class="box-content none"> <h1 class="font-weight-bold  " style="undefined">โลกเสมือนจริง อัจฉริยะ</h1></div>
                                <div class="box-content box-content-space"></div>
                                <div class="box-content box-content-editor undefined">
                                	<p>ครั้งแรกกับประสบการณ์ <span style="color: #ff0000;" data-mce-style="color: #ff0000;">AR Filter</span> สุดล้ำ ไม่เหมือนใคร<br>สนุกได้ทุกที่ไม่มีเหงา ถ่ายรูปแบบล้ำๆด้วยระบบ <span style="color: #ff0000;" data-mce-style="color: #ff0000;">AR 3 มิติ</span><br>ที่จะทำให้การถ่ายรูปของคุณไม่น่าเบื่ออีกต่อไป</p>
                                </div>
                                <div class="box-content box-content-space"></div>
                                <div class="box-content none"> <img src="/uploads/logo-true5g-ar_1612172872.png" class="w-25"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--
    |*******************************************************************************************
    | SECTION BANNER SLIDE
    |*******************************************************************************************
    -->


    <?php
        $imagebg_d = array('immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg');
        $imagebg_m = array('immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg');
        $content_position = array('text-left bancont-top','text-left bancont-middle','text-left bancont-bottom','text-center bancont-top','text-center bancont-middle','text-center bancont-bottom','text-right bancont-top','text-right bancont-middle','text-right bancont-bottom');
    ?>
    <div class="section cont bg-fullwidth st st-banner contlay-ver" style="background-image:url(/assets/img/template/bg-d.jpg);">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php for( $i=0; $i<count($imagebg_d); $i++ ){ ?>
                <div class="swiper-slide bg-fullwidth" style=" background-image:url(/assets/img/template/<?php echo $imagebg_d[$i]; ?>);">
                    <div class="container container-1600 container-fullwidth pd-m-0">
                        <div class="position-relative fw sec-ar-1-herobanner">
                            <div class="blank-png d-sm-block d-none"><img class="fw" src="/assets/img/immersive/blank-banner-1.png"></div>
                        
                            <div class="blank-png d-sm-none"><img class="fw" src="/assets/img/template/<?php echo $imagebg_m[$i]; ?>"></div>
                            
                            <div class="cont-contain cont-abs <?php echo $content_position[$i]; ?>">
                                <div class="cont-txt">
                                <div class="row"><div class="col col-12 nexitor-col"><div class="box-content none"> <h1 class="font-weight-bold  " style="color: rgb(255, 255, 255); -webkit-text-fill-color: inherit; background: none;">IMMERSIVE EXPERIENCE</h1></div><div class="box-content box-content-editor undefined"><p><strong><span style="color: rgb(255, 0, 0);" data-mce-style="color: #ff0000;">#COMTRUE<span style="color: rgb(255, 255, 255);" data-mce-style="color: #ffffff;">WITH</span>TRUE5G</span></strong></p></div></div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php if( count($imagebg_d) > 1 ){ ?>
        <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/icon-arrow2-right.png"></div>
        <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/icon-arrow2-left.png"></div>
        <?php } ?>
    </div>


    <!--
    |*******************************************************************************************
    | SECTION LIST : BOX
    |*******************************************************************************************
    -->
    <div class="section cont bg-fullwidth st st-list" style="background-image:url(/assets/img/template/bg-d.jpg);">
        <div class="container container-1600">
            <div class="position-relative fw">
                <div class="cont-contain ml-0 mr-0 w-100">
                    <div class="row">
                        <?php for( $i=0; $i<3; $i++ ){ ?>
                        <div class="col col-12 col-md-4">
                            <div class="thumb thumb-ver">
                                <div class="thumb-group">
                                    <div class="thumb-img"><img src="/assets/img/template/oc-1.png" class="w-100 float-left"></div>
                                    <div class="thumb-txt">
                                        <div class="thumb-txt-h">Test 1</div>
                                        <div class="thumb-txt-p">Lorem ipsum test Yummy yumme</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div> 
                </div> 
            </div>
        </div>
    </div>
    <div class="section cont bg-fullwidth st st-list" style="background-image:url(/assets/img/template/bg-d.jpg);">
        <div class="container container-1600">
            <div class="position-relative fw">
                <div class="cont-contain ml-0 mr-0 w-100">
                    <div class="row">
                        <?php for( $i=0; $i<3; $i++ ){ ?>
                        <div class="col col-12 col-md-4">
                            <div class="thumb thumb-hoz">
                                <div class="thumb-group">
                                    <div class="thumb-img"><img src="/assets/img/template/oc-1.png" class="w-100 float-left"></div>
                                    <div class="thumb-txt">
                                        <div class="thumb-txt-h">Test 1</div>
                                        <div class="thumb-txt-p">Lorem ipsum test Yummy yumme</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div> 
                </div> 
            </div>
        </div>
    </div>
    <div class="section cont bg-fullwidth st st-list" style="background-image:url(/assets/img/template/bg-d.jpg);">
        <div class="container container-1600">
            <div class="position-relative fw">
                <div class="cont-contain ml-0 mr-0 w-100">
                    <div class="row">
                        <?php for( $i=0; $i<3; $i++ ){ ?>
                        <div class="col col-12 col-md-4">
                            <div class="thumb thumb-incard">
                                <div class="thumb-group">
                                    <div class="thumb-img"><img src="/assets/img/template/oc-1.png" class="w-100 float-left"></div>
                                    <div class="thumb-txt">
                                        <div class="thumb-txt-h">Test 1</div>
                                        <div class="thumb-txt-p">Lorem ipsum test Yummy yumme</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div> 
                </div> 
            </div>
        </div>
    </div>

    <div class="section cont bg-fullwidth st st-list" style="background-image:url(/assets/img/template/bg-d.jpg);">
        <div class="container container-1600">
            <div class="position-relative fw">
                <div class="cont-contain ml-0 mr-0 w-100">
                    <div class="row row-cols-5">
                        <?php for( $i=0; $i<8; $i++ ){ ?>
                        <div class="col">
                            <div class="thumb thumb-incard">
                                <div class="thumb-group">
                                    <div class="thumb-img"><img src="/assets/img/template/oc-1.png" class="w-100 float-left"></div>
                                    <div class="thumb-txt">
                                        <div class="thumb-txt-h">Test 1</div>
                                        <div class="thumb-txt-p">Lorem ipsum test Yummy yumme</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div> 
                </div> 
            </div>
        </div>
    </div>
    
    <div class="section cont bg-fullwidth st st-list" style="background-image:url(/assets/img/template/bg-d.jpg);">
        <div class="container container-1600">
            <div class="position-relative fw">
                <div class="cont-contain ml-0 mr-0 w-100">
                    <div class="row row-cols-6">
                        <?php for( $i=0; $i<6; $i++ ){ ?>
                        <div class="col">
                            <div class="thumb thumb-incard">
                                <div class="thumb-group">
                                    <div class="thumb-img"><img src="/assets/img/template/oc-1.png" class="w-100 float-left"></div>
                                    <div class="thumb-txt">
                                        <div class="thumb-txt-h">Test 1</div>
                                        <div class="thumb-txt-p">Lorem ipsum test Yummy yumme</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div> 
                </div> 
            </div>
        </div>
    </div>
    <!--
    |*******************************************************************************************
    | SECTION LIST : SLIDE
    |*******************************************************************************************
    -->
    <?php
        $list_thumb = array('oc-1.png','oc-1.png','oc-1.png');
        $list_title = array('Topic 1','Topic 2','Topic 3');
        $list_desc = array('Desc 1','Desc 2','Desc 3');
        $list_perview = 2;
    ?>
	<div class="section cont bg-fullwidth st st-list" style="background-image:url(/assets/img/template/bg-d.jpg);" data-slidecol="<?php echo $list_perview; ?>">
        <div class="container container-1600">
            <div class="position-relative fw">
                <div class="cont-contain ml-0 mr-0 w-100">
                	<div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php for( $i=0; $i<3; $i++ ){ ?>
                            <div class="swiper-slide bg-fullwidth">
                            	<div class="thumb thumb-incard">
                                    <div class="thumb-group">
                                        <div class="thumb-img"><img src="/assets/img/template/<?php echo $list_thumb[$i]; ?>" class="w-100 float-left"></div>
                                        <div class="thumb-txt">
                                            <div class="thumb-txt-h"><?php echo $list_title[$i]; ?></div>
                                            <div class="thumb-txt-p"><?php echo $list_desc[$i]; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php if( count($list_thumb) > $list_perview ){ ?>
                    <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/icon-arrow2-right.png"></div>
                    <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/icon-arrow2-left.png"></div>
                    <?php } ?>
                </div> 
            </div>
        </div>
    </div>
    
    <!--
    |*******************************************************************************************
    | SECTION VIDEO
    |*******************************************************************************************
    -->
    <?php
        $video_id = "s0_JTHy0zmg";
        $video_thumb = "video-thumb.jpg";
    ?>
    <div class="section cont bg-fullwidth st st-list" style="background-image:url(/assets/img/template/bg-d.jpg);">
        <div class="container container-1600">
            <div class="position-relative fw">
                <div class="cont-contain ml-0 mr-0 w-100">
                    <div class="box-video">
                        <div class="ganeral-video">
                            <div class="video-container inactive-state click-to-play-video" data-video="<?php echo $video_id; ?>">
                                <div class="video-put"><div id="player_ganeral" class="player_ganeral"></div></div>
                                <div class="video-space"><img class="radius float-left w-100" src="/assets/img/template/<?php echo $video_thumb; ?>">
                                </div>
                                <?php if( $video_thumb != '' ){ ?>  
                                    <div class="videoplay-icon"><img class="" src="/assets/img/xr/button-play.png"> </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section cont bg-fullwidth st st-list" style="background-image:url(/assets/img/template/bg-d.jpg);">
        <div class="container container-1600">
            <div class="position-relative fw">
                <div class="cont-contain ml-0 mr-0 w-100">
                    <div class="box-video">
                        <video class="video-container" muted  autoplay loop>
                            <source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4" type="video/mp4">
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </div>

   


    <!--
    |*******************************************************************************************
    | SECTION Z-SCROLL
    |*******************************************************************************************
    -->
    <!-- <div class="section cont st st-zscroll">
        <div class="position-relative fw d-none d-lg-block">
            <div class="sec-zscroll" style="background-image:url(/assets/img/vr/bg-section-3-d.jpg);">
                <div class="container container-1600 ">
                    <div class="position-relative fw">
                        <div class="blank-png d-md-block d-none"><img class="fw" src="/assets/img/vr/sec-3-content-blank.png"></div>
                        <div class="blank-png d-md-none d-block"><img class="fw" src="/assets/img/vr/sec-3-content-blank-m.png"></div>
                        <div class="cont-contain cont-abs">
                            <div class="box-slide-z slide-z-1" >
                                <div class="slide-z-img"><img src="/assets/img/vr/sec-3-img-1.png"></div>
                                <div class="slide-z-txt"><img src="/assets/img/vr/sec-3-txt-1.png"></div>
                            </div>
                            <div class="box-slide-z slide-z-2" >
                                <div class="slide-z-img"><img src="/assets/img/vr/sec-3-img-2.png"></div>
                                <div class="slide-z-txt"><img src="/assets/img/vr/sec-3-txt-2.png"></div>
                            </div>
                            <div class="box-slide-z slide-z-3">
                                <div class="slide-z-img"><img src="/assets/img/vr/sec-3-img-3.png"></div>
                                <div class="slide-z-txt"><img src="/assets/img/vr/sec-3-txt-3.png"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="zscroll-blank position-relative fw "></div>
        </div>
    </div> -->
    
    <!-- <div class="section cont st st-zscroll">
        <div class="position-relative fw">
            <div class="sec-scroll-z" style="background-image:url(/assets/img/vr/bg-section-3-d.jpg);">
                <div class="container container-1600 ">
                    <div class="position-relative fw">
                        <div class="blank-png d-md-block d-none"><img class="fw" src="/assets/img/vr/sec-3-content-blank.png"></div>
                        <div class="blank-png d-md-none d-block"><img class="fw"  src="/assets/img/vr/sec-3-content-blank-m.png"></div>
                        <div class="cont-contain cont-abs">
                            <div class="box-slide-z slide-z-1 slide-zimg-right" >
                                <div class="slide-z-img"><img src="/assets/img/vr/sec-3-img-1.png"></div>
                                <div class="slide-z-txt"><img src="/assets/img/vr/sec-3-txt-1.png"></div>
                            </div>
                            <div class="box-slide-z slide-z-2 slide-zimg-right" >
                                <div class="slide-z-img"><img src="/assets/img/vr/sec-3-img-2.png"></div>
                                <div class="slide-z-txt"><img src="/assets/img/vr/sec-3-txt-2.png"></div>
                            </div>
                            <div class="box-slide-z slide-z-3 slide-zimg-right">
                                <div class="slide-z-img"><img src="/assets/img/vr/sec-3-img-3.png"></div>
                                <div class="slide-z-txt"><img src="/assets/img/vr/sec-3-txt-3.png"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="position-relative fw zscroll-blank"></div>
        </div>
    </div> -->
    <!-- <div class="section cont st st-zscroll">
        <div class="position-relative fw d-none d-lg-block">
            <div class="sec-zscroll" style="background-image:url(/assets/img/vr/bg-section-3-d.jpg);">
                <div class="container container-1600 ">
                    <div class="position-relative fw">
                        <div class="cont-contain">
                            <div class="box-slide-z slide-z-1" >
                                <div class="slide-z-img"><img src="/assets/img/vr/sec-3-img-1.png"></div>
                                <div class="slide-z-txt"><img src="/assets/img/vr/sec-3-txt-1.png"></div>
                            </div>
                            <div class="box-slide-z slide-z-2" >
                                <div class="slide-z-img"><img src="/assets/img/vr/sec-3-img-2.png"></div>
                                <div class="slide-z-txt"><img src="/assets/img/vr/sec-3-txt-2.png"></div>
                            </div>
                            <div class="box-slide-z slide-z-3">
                                <div class="slide-z-img"><img src="/assets/img/vr/sec-3-img-3.png"></div>
                                <div class="slide-z-txt"><img src="/assets/img/vr/sec-3-txt-3.png"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="zscroll-blank position-relative fw "></div>
        </div>
    </div> -->
    <!--
    |*******************************************************************************************
    | SECTION BANNER SLIDE
    |*******************************************************************************************
    -->

</div>