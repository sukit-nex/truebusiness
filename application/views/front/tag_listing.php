<div class="loadpage"></div>
<?php //include('inc/inc_header.php');?>
    <div class="wrapper">
        <div class="row-wrap bg-fullwidth fw gen_bg  bg-py-t bg-px-c bg-rp-no bg-fx-st bg-sz-cv sec-tag-search-list"
            data-bg-desktop="bg-page-new-and-event.png" data-bg-mobile="bg-page-new-and-event1.png"
            data-bg-color="transparent" style="background-color:transparent;">
            <div class="container container-fluid">
                <div class="row">
                    <div class="col col-12 col-md-12 ">
                        <div class="section cont mg st st-banner custom-listing-banner search-tag " >
                            <div class=" bg-fullwidth fw  position-relative">
                                <div class="container-fluid">
                                    <div class="position-relative">
                                        <img class="d-none d-md-block w-100"src="/assets/img/template/content-blank.png">
                                        <img class="d-block d-md-none w-100"  src="/assets/img/template/content-blank-m.png">
                                        <div class="container container-1600">
                                            <div class="cont-contain pd cont-abs bancont-middle text-center">
                                                <div class="cont-txt ">
                                                    <div class="row">
                                                        <div class="col col-12 nexitor-col">
                                                            <div class="box-content text-center">
                                                                <h1 class="font-weight-bold  " style="undefined">ผลการค้นหาจาก</h1>
                                                            </div>
                                                            <div class="box-content text-center">
                                                                <h2 class="font-weight-bold  " style="color:#434040; -webkit-text-fill-color: #434040; background: none;text-transform: capitalize; ">"<?php echo (isset($_GET['tag']))? $_GET['tag'] : ''; ?>"</h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $tag_search =  (isset($_GET['tag']))? $_GET['tag'] : ''; ?>
                        
                        <?php //print_r($content_detail); ?>
                        <div class="section cont mg st st-list gen_margin tag-listing custom-listing-search-tag">
                            <div class="bg-fullwidth fw  ">
                                <div class="container container-1600">
                                    <div class="position-relative fw gen_padding pd">
                                                <img class="ele_img1 abs" src="/uploads/bg-listing-news-and-event8.png" alt="">
                                                <img class="ele_img2 abs" src="/uploads/ele-bubble-color-21.png" alt="">
                                        <div class="sec-lists">
                                            <div class="row row_cols  " data-row-cols-desktop="3<?php // echo $section['column_per_view'] ?>" data-row-cols-mobile="1<?php // echo $section['display_per_column'] ?>" data-row-cols-check="incard">
                                                <?php foreach($content_detail as $list_key => $listing){  ?>

                                                        <!-- Content normal -->
                                                        <div class="col">
                                                            <div class="box-thumb fw position-relative">
                                                            <?php if($listing['link_type'] != 'no'){?>
                                                                        <?php if($listing['btn_show'] == 0){?>
                                                                    <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"  >
                                                                    <?php } ?>
                                                                    <?php } ?>
                                                                    <div class="thumb thumb-incard">
                                                                            <div class="thumb-group">
                                                                                <div class="thumb-img">
                                                                                    <img src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : '/assets/img/template/image-default.jpg'; ?>"
                                                                                        class="w-100 float-left">
                                                                                </div>
                                                                                <div class="thumb-txt">
                                                                                    <?php if($listing['title'] != ''){ ?>
                                                                                    <div class="thumb-txt-h"><?php echo $listing['title'] ?></div>
                                                                                    <?php } ?>
                                                                                    <?php if($listing['sub_title'] != ''){ ?>
                                                                                    <div class="thumb-txt-p"><?php echo $listing['sub_title'] ?></div>
                                                                                    <?php } ?>
                                                                                    <?php if($listing['price'] != ''){ ?>
                                                                                    <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                                    <?php } ?> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php if($listing['link_type'] != 'no'){?>
                                                                        <?php if($listing['btn_show'] == 0){?>
                                                                    </a>
                                                                    <?php } ?>
                                                                    <?php } ?>
                                                                <div class="thumb-tag">
                                                                    <?php if( $listing['cate_title'] != ""){ ?>
                                                                    <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                    <?php } ?>
                                                                        <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                        <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                            <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                                
                                                                                <?php if( $tag != "" & $tag_key < 1 ){ ?>
                                                                                    <?php if($tag_search  != $tag){ ?>
                                                                                        <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                                <?php if($tag == $tag_search ){ ?>
                                                                                    <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>

                                                        <!-- End Content normal -->
                                                   
                                                <?php  } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>