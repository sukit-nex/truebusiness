<?php 
// echo '<br>Parent Active : '. $active_cate_parent;
// echo '<br>Child Active : '. $active_cate_child;
// echo '<br>';

//print_r( $cate_detail ); 
?>
<div class="wrapper">
    <div class="sec-smedetail smedetail-cate-<?php echo $active_cate_parent; ?>" data-parent="<?php echo $active_cate_parent; ?>" data-child="<?php echo $active_cate_child; ?>">
        <div class="box-jumpto" data-function="linkout">
            <div class="container container-1440">
                <div class="position-relative fw">
                    <div class="jumpto-group d-inline-block d-md-flex align-items-center bg-gdcolor-orange position-relative">
                    <div class="jumpto-title d-none d-lg-block"><b>คุณกำลังมองหา<span style="display:inline-block">แพ็กเกจ ?</span></b></div>
                    <div class="jumpto-list border-gradient d-none d-md-flex align-items-center justify-content-between">
                        <?php foreach($cate_list as $key => $catelist_val){ ?>
                            <div class="jumpto-item jumpto-item-<?php echo $key; ?> solution-color-<?php echo $catelist_val['code']; ?>" ><a href="<?php echo base_url().$lang_path.'/smepack/'.$catelist_val['slug'];?>"  data-link="<?php echo $catelist_val['code'] ?>" data-parent="<?php echo $catelist_val['slug']; ?>" class="btn btn-jump"><span><?php echo $catelist_val['title'] ?></span></a></div>
                        <?php } ?>
                    </div>
                    <div class="jumpto-select d-block d-md-none">
                        <div class="group-cate">
                            <div class="cate-show"><span class="cate-txt">โทรศัพท์มือถือ</span></div>
                            <ul class="cate-select">
                                <?php foreach($cate_list as $key => $catelist_val){ ?>
                                <li><a href="javascript:void(0)" class="select-cate-1"><span class="cate-txt"><?php echo $catelist_val['title'] ?></span></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <?php foreach($cate_detail as $cate_row){?>
        <div class="box-smedetail-banner section cont bg-fullwidth st st-content contlay-hoz-right">
            <div class="bg-fullwidth fw gen_bg" data-bg-desktop="<?php echo $cate_row['banner_d'] != '' ? $cate_row['banner_d'] : 'blank-sme-banner-d.jpg' ;  ?>" data-bg-mobile="<?php echo $cate_row['banner_m'];  ?>" >
                <div class="container container-1600 gen-height text-md-bottom text-bottom"  
                    data-image-desktop="<?php echo $cate_row['banner_d'] != ''  ? upload_path($cate_row['banner_d']) : '/assets/img/blank/blank-sme-banner-d.jpg' ; ?> "
                    data-image-mobile="<?php echo $cate_row['banner_m'] != ''  ? upload_path($cate_row['banner_m']) : '' ; ?>" data-column="1">
                    <div class="position-relative fw">
                        <div class="cont-contain pd">
                            <div class="cont-txt text-center">
                                <div class="smedetail-banner-detail">
                                    <div class="box-content light-content ">
                                        <div class="smedetail-banner-topic">
                                            <img class="svg" src="<?php echo UPLOAD_PATH.$cate_row['icon_image']; ?>" alt="<?php echo $cate_row['title']?>">
                                            <h1><b><?php echo $cate_row['title'] ?></b></h1>
                                        </div>
                                        <p><?php echo $cate_row['description'] ?></p>
                                        <hr>
                                        <div class="">
                                            <?php if( isset($cate_row['brochure_pdf'] ) ){ ?>
                                                <a href="<?php echo upload_path().'/'.$cate_row['brochure_pdf'] ?>" target="_blank" class="btn btn-text btn-download btn-icon"><span><img src="/assets/img/skin/icon-download.svg" class="svg"><span><b><?php echo $words['download_brochure'] != "" ? $words['download_brochure'] :'' ?></b></span></span></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <?php } ?>

        <div class="sec-smeall fw pd">
            <div class="container">
                <div class="box-smeall">
                    <?php foreach($cate_detail as $cate_row){ ?>
                        <div class="box-smedetail-tab">
                            <div class="smedetail-tab-list">
                                <ul>
                                <?php foreach($cate_row['cate_list'] as $cate_list_key => $cate_list){?>
                                        <li><a class="" href="javascript:void(0)"  data-child="<?php echo $cate_list['slug']; ?>"><b><?php echo $cate_list['title'] ?></b></a></li>
                                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="box-smedetail-listall fw">
                    <?php foreach($cate_row['cate_list'] as $cate_list_key => $cate_list){  ?>
                    
                        <div class="box-smedetail-list smedetail-list-<?php echo $cate_list_key; ?>  genSlide" data-perview-d="3" data-perview-t="2" data-perview-m="1.1" data-space-d="30" data-space-t="20" data-space-m="20" data-center-m="true" data-child="<?php echo $cate_list['slug']; ?>">
                            <div class="box-smedetail-info">
                                <div class="box-content text-center">
                                    <h3 class="title-main t-black txt-upper"><span><?php echo $cate_list['package_name'] ?></span></h3>
                                    <div class="title-sub"><?php echo $cate_list['short_description'] ?></div>
                                </div>
                                <div class="swiper-button swiper-button-next d-none d-sm-block"><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></div>
                                <div class="swiper-button swiper-button-prev d-none d-sm-block"><img src="/assets/img/skin/arrow-left-1.svg" class="svg"></div>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="fw position-relative">
                                <div class="swiper-button swiper-button-next d-block d-sm-none"><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></div>
                                <div class="swiper-button swiper-button-prev d-block d-sm-none"><img src="/assets/img/skin/arrow-left-1.svg" class="svg"></div>
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <?php  foreach($cate_list['smepack'] as $cate_smepack){ ?>
                                            <?php if ( $cate_smepack['package']!= '' ) { $features  = json_decode($cate_smepack['package'],true ); } ?>
                                            <div class="swiper-slide">
                                                <div class="box-tablepack">
                                                    <div class="tablepack-list">
                                                        <?php if($cate_smepack['highlight_homepage'] == 1){ ?>
                                                            <div class="tablepack-highlight"><span><?php echo $words['pack_recommend'] != "" ? $words['pack_recommend'] :'' ?></span></div>
                                                        <?php } ?>
                                                        <div class="tablepack-head 
                                                            <?php if($active_cate_parent == 'mobile'){echo 'bg-gdcolor-orange'; 
                                                            }elseif($active_cate_parent == 'internet'){echo 'bg-gdcolor-blue'; 
                                                            }elseif($active_cate_parent == 'tv'){echo 'bg-gdcolor-pink'; 
                                                            }elseif($active_cate_parent == 'fixedline'){echo 'bg-gdcolor-darkblue' ;
                                                            }elseif($active_cate_parent == 'value-set'){echo 'bg-gdcolor-red'; }
                                                        
                                                            ?>">
                                                            <div class="pack-price-group">
                                                                <?php if($cate_smepack['price'] != ""){ ?>
                                                                    <div class="pack-price t-black"><?php echo $cate_smepack['price'] ?></div>
                                                                <?php } ?>
                                                                <?php if($cate_smepack['price_billing_cycle'] !=  ""){ ?>
                                                                <div class="pack-circle"><?php echo $cate_smepack['price_billing_cycle'] ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div class="tablepack-body">
                                                            <?php  foreach($features as $feature){ ?>
                                                                <?php  if($feature['feature'] == "mobile" ){ ?>
                                                                    <div class="feature-list">
                                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                                            <?php  if($feature['header_message']){ ?>
                                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="g-feature-main">
                                                                                <?php if($feature['detail_type'] == "template"  ){ ?>
                                                                                    <?php if($feature['detail_template_service_fee'] != "" || $feature['detail_template_freecall_conditions'] != "" || $feature['detail_template_4g_hd_voice'] != ""  ){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>
                                                                                <?php if($feature['main_type'] == 'template' ){ ?>
                                                                                    <?php if( $feature['main_template_type_calls'] == "อั้น" ){ ?>
                                                                                        <div class="g-feature-detail"><span><b><?php echo $words['free_call'] != "" ? $words['free_call'] :'' ?> <?php echo $feature['main_template_number_calls'] ?></b></span> <?php echo $words['time_minute'] != "" ? $words['time_minute'] :'' ?><br><?php echo $feature['main_template_type_networks'] ?>
                                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                            <br> <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                            <br> <a class="g-feature-detail-link" target="<?php echo $feature['btn_external_link'] == "" ?'': '_blank'?>"   href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                            <br> <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                        </div>
                                                                                    <?php } elseif( $feature['main_template_type_calls'] == "ไม่อั้น" ){?>
                                                                                        <div class="g-feature-detail"><b><?php echo $words['free_call'] != "" ? $words['free_call'] :'' ?> <?php echo $words['unlimited'] != "" ? $words['unlimited'] :'' ?> <?php echo $feature['main_template_type_networks'] ?></b><br><?php echo $feature['main_template_time_calls']; ?>
                                                                                            <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                                <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                                <br> <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                            <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                                <br> <a class="g-feature-detail-link" target="<?php echo $feature['btn_external_link'] == "" ?'': '_blank'?>"   href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                            <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                                <br> <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                            <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                                <br> <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                            <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php }elseif($feature['main_type'] == "manual"){ ?>
                                                                                    <div class="g-feature-detail">
                                                                                        <?php echo $feature['main_manual_description'] ?>
                                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                            <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                            <a class="g-feature-detail-link" target="<?php echo $feature['btn_external_link'] == "" ?'': '_blank'?>"   href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                            <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                            
                                                                            <div class="g-feature-toggle">
                                                                                <?php if($feature['detail_type'] == "manual" ){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif($feature['detail_type'] == "template" ){ ?>
                                                                                    <?php if($feature['detail_template_service_fee'] != ""){ ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $words['service_charge'] != "" ? $words['service_charge'] :'' ?>  <?php echo $feature['detail_template_service_fee']; ?> <?php echo $words['baht_per_month'] != "" ?  $words['baht_per_month'] : '' ?></div>
                                                                                    <?php } ?>
                                                                                    <?php if($feature['detail_template_freecall_conditions'] != ""){ ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_template_freecall_conditions']; ?></div>
                                                                                    <?php } ?>
                                                                                    <?php if($feature['detail_template_4g_hd_voice'] != ""){ ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_template_4g_hd_voice']; ?></div>
                                                                                        
                                                                                    <?php } ?>
                                                                                <?php }else{} ?>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php }elseif($feature['feature'] == "mobile_internet" ){ ?>
                                                                    <div class="feature-list">
                                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                                            <?php  if($feature['header_message']){ ?>
                                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="g-feature-main">
                                                                                <?php if($feature['detail_type'] == "template"  ){ ?>
                                                                                    <?php if($feature['detail_template_use_full_speed'] != ""  ){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>

                                                                                <?php if($feature['main_type'] == "template"){ ?>
                                                                                    <?php if($feature['main_template_type_use'] == "อั้น"){ ?>
                                                                                        <div class="g-feature-detail"><b><?php echo $words['internet'] != "" ?  $words['internet'] : '' ?></b> <span><b><?php echo $feature['main_template_usage_amount']; ?></b></span>GB<br><?php echo $words['net_top_speed'] != "" ? $words['net_top_speed'] :'' ?> <?php echo $feature['main_template_maximum_speed']; ?> Mbps
                                                                                        
                                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                            <br> <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                            <br> <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                            <br><a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                        </div>
                                                                                    <?php } if($feature['main_template_type_use'] == "ไม่อั้น"){ ?>
                                                                                        <div class="g-feature-detail"><b><?php echo $words['internet'] != "" ? $words['internet'] :'' ?> <?php echo $words['unlimited'] != "" ? $words['unlimited'] :'' ?></b> <br><?php echo $words['net_top_speed'] != "" ? $words['net_top_speed'] :'' ?> <?php echo $feature['main_template_maximum_speed']; ?> Mbps 
                                                                                        
                                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                            <br>  <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                            <br><a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                            <br> <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php } elseif($feature['main_type'] == "manual") { ?>
                                                                                        <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                                        
                                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                            <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                        ><a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                        </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                            <div class="g-feature-toggle">
                                                                                <?php if($feature['detail_type'] == "template"  ){ ?>
                                                                                    <?php if($feature['detail_template_use_full_speed'] != ""  ){ ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_template_use_full_speed']; ?></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                                <?php  }
                                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php }elseif($feature['feature'] == "wifi" ){  ?>
                                                                    <div class="feature-list">
                                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                                            <?php  if($feature['header_message']){ ?>
                                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="g-feature-main">
                                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>
                                                                                <?php if($feature['main_type'] == "template"){ ?>
                                                                                    <?php if($feature['main_template_type_use'] == "อั้น"){ ?>
                                                                                        <div class="g-feature-detail"><b>WiFi</b> <span><b><?php echo $feature['main_template_usage_amount']; ?></b></span>GB
                                                                                        
                                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                            <br><a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                            <br> <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                            <br> <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                        </div>
                                                                                    <?php } if($feature['main_template_type_use'] == "ไม่อั้น"){ ?>
                                                                                        <div class="g-feature-detail"><b>WiFi <?php echo $words['unlimited'] != "" ? $words['unlimited'] :'' ?></b>
                                                                                        
                                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                            <br><a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                            <br><a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                            <br><a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php } elseif($feature['main_type'] == "manual") { ?>
                                                                                    <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                                    
                                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                        <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                            <div class="g-feature-toggle">
                                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                                <?php  }
                                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php }elseif($feature['feature'] == "internet" ){  ?>
                                                                    <div class="feature-list">
                                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                                            <?php  if($feature['header_message']){ ?>
                                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="g-feature-main">
                                                                                <?php if($feature['detail_type'] == "template"){ ?>
                                                                                    <?php if($feature['detail_template_upload_speed'] != ""){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>

                                                                                <?php if($feature['main_type'] == "template"){ ?>
                                                                                    <div class="g-feature-detail"><?php echo $words['max_download_speed'] != "" ? $words['max_download_speed'] :'' ?><br><span><b><?php echo $feature['main_template_download_speed']; ?></b></span> Mbps
                                                                                        
                                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                            <br> <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                            <br><a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                            <br> <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                    </div>
                                                                                <?php } elseif($feature['main_type'] == "manual") { ?>
                                                                                    <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                                    
                                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                        <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                            <div class="g-feature-toggle">
                                                                                <?php if($feature['detail_type'] == "template"){ ?>
                                                                                    <?php if($feature['detail_template_upload_speed'] != ""){ ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $words['max_upload_speed'] != "" ? $words['max_upload_speed'] :'' ?><?php echo $feature['detail_template_upload_speed']; ?>Mbps</div>
                                                                                    <?php } ?>
                                                                                <?php }elseif($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                                <?php  }
                                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php }elseif($feature['feature'] == "smart_wifi" ){  ?>
                                                                    <div class="feature-list">
                                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                                            <?php  if($feature['header_message']){ ?>
                                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="g-feature-main">
                                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>
                                                                                    <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                        <br>
                                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                        <br>
                                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                        <br>
                                                                                        <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                    </div>
                                                                            </div>
                                                                            <div class="g-feature-toggle">
                                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                                <?php  }
                                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php }elseif($feature['feature'] == "true_visions" ){  ?>
                                                                    <div class="feature-list">
                                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                                            <?php  if($feature['header_message']){ ?>
                                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="g-feature-main">
                                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>

                                                                                <?php if($feature['main_type'] == "template") { ?>
                                                                                    <div class="g-feature-detail"><b><?php echo $feature['main_template_number_boxes']; ?> <?php echo $words['true_box'] != "" ? $words['true_box'] :'' ?></b><br><span><?php echo $feature['main_template_channel_details']; ?> </span>
                                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                        <br>
                                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                        <br>
                                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                        <br>
                                                                                        <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                    </div>
                                                                                <?php }elseif($feature['main_type'] == "manual") { ?>
                                                                                    <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                        <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                            <div class="g-feature-toggle">
                                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                                <?php  }
                                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php }elseif($feature['feature'] == "notebook" ){  ?>
                                                                    <div class="feature-list">
                                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                                            <?php  if($feature['header_message']){ ?>
                                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="g-feature-main">
                                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                                

                                                                                <?php if($feature['main_type'] == "template"){ ?>
                                                                                    <div>
                                                                                        <div class="g-feature-thumb"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>
                                                                                        <div class="g-feature-detail text-center"><span><b><?php echo $feature['main_template_brand']; ?></b></span><br> <?php echo $feature['main_template_model_description']; ?>
                                                                                            <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                                <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                                <br> <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                            <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                                <br><a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                            <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                                <br> <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                            <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                                <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                            <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                        </div>
                                                                                    </div>
                                                                                <?php } elseif($feature['main_type'] == "manual") { ?>
                                                                                    <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>
                                                                                    <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                                    
                                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                        <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                            <div class="g-feature-toggle">
                                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                                <?php  }
                                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php }elseif($feature['feature'] == "fixedline" ){  ?>
                                                                    <div class="feature-list">
                                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                                            <?php  if($feature['header_message']){ ?>
                                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="g-feature-main">
                                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>

                                                                                <?php if($feature['main_type'] == "template") { ?>
                                                                                    <div class="g-feature-detail"><b><?php echo $words['call_cost'] != "" ? $words['call_cost'] :'' ?> <?php echo $feature['main_template_call_price']; ?> <?php echo $words['baht_per_month'] != "" ? $words['baht_per_month'] :''?></b><br><span><?php echo $feature['main_template_freecall_conditions']; ?> </span>
                                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                        <br>
                                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                        <br>
                                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                        <br>
                                                                                        <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                    </div>
                                                                                <?php }elseif($feature['main_type'] == "manual") { ?>
                                                                                    <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                        <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                            <div class="g-feature-toggle">
                                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                                <?php  }
                                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php }elseif($feature['feature'] == "mobile_bundle" ){ ?>
                                                                    <div class="feature-list">
                                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                                            <?php  if($feature['header_message']){ ?>
                                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="g-feature-main">
                                                                                <?php if($feature['detail_type'] == "template"  ){ ?>
                                                                                    <?php if($feature['detail_template_service_fee'] != "" || $feature['detail_template_freecall_conditions'] != "" || $feature['detail_template_4g_hd_voice'] != "" || $feature['detail_template_use_full_speed'] || $feature['detail_template_usage_amount']){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif($feature['detail_type'] == "manual"){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                                        <div class="icon-toggle"></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>
                                                                                <div class="g-feature-detail">
                                                                                    <?php if($feature['main_type'] == 'template' ){ ?>
                                                                                        <?php if( $feature['main_template_type_calls'] == "อั้น" ){ ?>
                                                                                            <b>โทรฟรี</b> <span><b><?php echo $feature['main_template_number_calls'] ?></b></span> นาที <?php echo $feature['main_template_type_networks'] ?><br>
                                                                                        <?php } elseif( $feature['main_template_type_calls'] == "ไม่อั้น" ){?>
                                                                                            <b>โทรฟรี <?php echo $feature['main_template_type_networks'] ?></b><br><?php echo $feature['main_template_time_calls']; ?><br>
                                                                                        <?php } ?>

                                                                                        <?php if( $feature['main_template_internet_type_use'] == "อั้น" ){ ?>
                                                                                            <b>เน็ต</b> <span>(4G/5G)<b><?php echo $feature['main_template_usage_amount'] ?>GB</b></span>
                                                                                        <?php } elseif( $feature['main_template_internet_type_use'] == "ไม่อั้น" ){?>
                                                                                            <b>เน็ต</b> <span>(4G/5G)<b> <?php echo $words['unlimited'] != "" ? $words['unlimited'] :''?></b></span>
                                                                                        <?php } ?>
                                                                                    <?php }elseif($feature['main_type'] == "manual"){ ?>
                                                                                        <?php echo $feature['main_manual_description'] ?>
                                                                                    <?php } ?>
                                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                            <br> <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                            <br> <a class="g-feature-detail-link" target="<?php echo $feature['btn_external_link'] == "" ?'': '_blank'?>"   href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                            <br> <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$feature['btn_pdf_file'];?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="g-feature-toggle">
                                                                                <?php if($feature['detail_type'] == "manual" ){ ?>
                                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                                    <?php } ?>
                                                                                <?php }elseif($feature['detail_type'] == "template" ){ ?>
                                                                                    <?php if($feature['detail_template_service_fee'] != ""){ ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $words['service_charge'] != "" ? $words['service_charge'] :''?> <?php echo $feature['detail_template_service_fee']; ?> <?php echo $words['baht_per_month'] != "" ? $words['baht_per_month'] :''?> </div>
                                                                                    <?php } ?>
                                                                                    <?php if($feature['detail_template_freecall_conditions'] != ""){ ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_template_freecall_conditions']; ?></div>
                                                                                    <?php } ?>
                                                                                    <?php if($feature['detail_template_4g_hd_voice'] != ""){ ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_template_4g_hd_voice']; ?></div>
                                                                                        
                                                                                    <?php } ?>
                                                                                    <?php if($feature['detail_template_use_full_speed'] != ""  ){ ?>
                                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_template_use_full_speed']; ?></div>
                                                                                    <?php } ?>

                                                                                    <?php if($feature['detail_template_wifi_type_use'] == "อั้น"  ){ ?>
                                                                                        <div class="g-feature-toggle-list">WiFi <?php echo $feature['detail_template_usage_amount'] ?>GB</div>
                                                                                    <?php }else{ ?>
                                                                                        <div class="g-feature-toggle-list">WiFi <?php echo $words['unlimited'] != "" ? $words['unlimited'] :''?> </div>
                                                                                    <?php } ?>
                                                                                    
                                                                                <?php }else{} ?>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php }elseif($feature['feature'] == "top_up" || $feature['feature'] == "get_free" || $feature['feature'] == "discounts_privileges"){ ?>
                                                                    <div class="feature-list">
                                                                        <div class="g-offer-topic bg-gdcolor-pink <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                                            <?php  if($feature['header_message'] != ""){ ?>
                                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="g-offer-topic-txt t-black">
                                                                                <?php if($feature['feature'] == "top_up"){ echo $words['top_up'] != "" ? $words['top_up']  : ""; }
                                                                                elseif ($feature['feature'] == "get_free") { echo $words['get_free'] != "" ? $words['get_free']  : ""; }
                                                                                elseif ($feature['feature'] == "discounts_privileges") { echo $words['discounts_privileges'] != "" ? $words['discounts_privileges']  : ""; } ?>
                                                                            </div>
                                                                            <?php if(isset($feature['special_offer_select']) && $feature['special_offer_select'] != ''){ ?>
                                                                                <div class="icon-toggle"></div>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <?php
                                                                            //echo 'mixer';
                                                                            //print_r($feature);
                                                                            if ( isset($feature['special_offer_select']) && $feature['special_offer_select'] != '' ) {
                                                                                $special_offer_select = explode(',', $feature['special_offer_select']);
                                                                                foreach ($special_offer_select as $key => $sos_code) {
                                                                                    $row_sp = get_special_offer( $sos_code, $special_offer_list);
                                                                                    // print_r($row_sp);
                                                                            
                                                                        ?>
                                                                        <div class="g-feature">
                                                                            <div class="g-feature-main">
                                                                                <?php if($row_sp['icon_image']  != "" ){ ?>
                                                                                <div class="g-feature-icon"><img src="<?php echo  $row_sp['icon_image']  != ''  ? UPLOAD_PATH.$row_sp['icon_image'] : ''; ?>"></div>
                                                                                <?php } ?>
                                                                                <div class="g-feature-detail">
                                                                                    <div class="box-content"><?php echo $row_sp['description']; ?></div>
                                                                                    
                                                                                        <?php if($row_sp['btn_link_type'] == 'internal_link'){  ?>
                                                                                            <?php  $uri = getSlug( $row_sp['btn_internal_page_code'] , $pages ); ?>
                                                                                            <a class="g-feature-detail-link" href="<?php echo $row_sp['btn_internal_page_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $row_sp['btn_text'] == '' ? 'ดูรายละเอียด' : $row_sp['btn_text'] ?></a>
                                                                                        <?php }else if($row_sp['btn_link_type'] == 'external_link'){  ?> 
                                                                                            <a class="g-feature-detail-link" target="<?php echo $row_sp['btn_external_link'] == "" ?'': '_blank'?>"   href="<?php echo $row_sp['btn_external_link'] == '' ? 'javascript:void(0);' : $row_sp['btn_external_link']; ?>"><?php echo $row_sp['btn_text'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                                        <?php }else if($row_sp['btn_link_type'] == 'pdf'){  ?>
                                                                                            <a href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path().'/'.$row_sp['btn_pdf_file'];?>" target="<?php echo $row_sp['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $row_sp['btn_text'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                                        <?php }else if($row_sp['btn_link_type'] == 'content'){  ?>
                                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $row_sp['btn_text'] == '' ? 'ดูรายละเอียด' : $row_sp['btn_text'] ?></a>
                                                                                        <?php }else if($row_sp['btn_link_type'] == 'no_link'){ } ?>
                                                                                    <?php if($row_sp['btn_content_popup'] != ""){  ?>
                                                                                        <div class="content-on-popup" style="display:none">
                                                                                            <?php echo $row_sp['btn_content_popup'] ?>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                                }
                                                                            }
                                                                        ?>
                                                                    </div>
                                                                <?php }?>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="tablepack-foot">
                                                            <hr>
                                                            <a href="javascript:void(0)" class="btn w-100" onclick="popin('popup-form',$(this))" data-formTopic="ขอรายละเอียดเพิ่มเติม" data-packageName="<?php echo $cate_list['prefix_title'].' '.$cate_list['package_name'].' '.$cate_smepack['price'] ?>"><span><?php echo $words['request_more_information'] != "" ?  $words['request_more_information'] : '' ?></span></a>
                                                            <?php if( isset($cate_list['tc_pdf'] ) ){ ?>
                                                                <a href="<?php echo upload_path().'/'.$cate_list['tc_pdf'] ?>" class="btn btn-text w-100"><span><?php echo $words['terms_and_conditions'] != "" ?  $words['terms_and_conditions'] : '' ?></span></a>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    <?php } ?>
                    </div>
            </div>
        </div>
    </div>
</div>
