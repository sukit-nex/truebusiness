    <div class="loadpage"></div>
    <?php //include('inc/inc_header.php');?>
    <div class="wrapper">

    <div class="sec-list-detail fw bg-fullwidth">

        <div class="sec-content">
            <div class="fw listing-ne-read ">
            <div class="blocks-content">
                <div class="blocks-content-detail fw position-relative">
                    <div class="container container-1600">
                        <div class="content-inner">
                            <div class="box-share d-none d-lg-block " id="sidebar">
                                <div class="sidebar__inner">
                                    <div class="share-title">SHARE</div>
                                    <ul>
                                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank" class="share-fb"><img src="/assets/img/skin/icon-fb.svg" class="svg"></a></li>
                                        <li><a href="https://twitter.com/intent/tweet?text=<?php echo $content_detail['title']; ?>&url=<?php echo "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank" class="share-tw" ><img src="/assets/img/skin/icon-tw.svg" class="svg"></a></li>
                                    </ul>
                                </div>
                            </div>
                        

                            <div class="sec-content-read">
                                <div class="row justify-content-center">
                                    <div class="col-12  col-md-12 col-lg-11">
                                        <div class="box-detail-topic">
                                            <h1><b class="txt-gdcolor"><?php echo $content_detail['title'] ?></b></h1>
                                            <div class="box-detail-info">
                                                <?php if($content_detail['published_date'] != ''){ ?>
                                                    <div class="date-post dark-color"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($content_detail['published_date'] , $content_detail['lang']); ?></span></div>
                                                <?php } ?>
                                            
                                                <div class="box-share d-flex d-lg-none">
                                                    <div class="share-title">SHARE</div>
                                                    <ul>
                                                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank" class="share-fb"><img src="/assets/img/skin/icon-fb.svg" class="svg"></a></li>
                                                        <li><a href="https://twitter.com/intent/tweet?text=<?php echo $content_detail['title']; ?>&url=<?php echo "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank" class="share-tw" ><img src="/assets/img/skin/icon-tw.svg" class="svg"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-detail-content">
                                            <?php echo $content_detail['content'] ?>
                                        </div>

                                        <?php if(isset($content_detail['tags'])){ ?>
                                            <?php if($content_detail['tags'] != ""){ ?>
                                                <div class="tag-group">
                                                    <ul class="box-tag">
                                                        <div class="tag-title">TAG : </div>
                                                        <?php $ar_tag = explode( ",", $content_detail['tags'] ); ?>
                                                        <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                            <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                <?php if( $tag != ""){ ?>
                                                                    <li><a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>" class="btn btn-red btn-sm"><span><?php echo $tag; ?></span></a></li>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <?php if( !isset($_GET['preview_id']) ){ ?>                                                                
            <?php if( isset($relate_lists) ){ ?>
                <?php if(sizeof($relate_lists) > 0 ){ ?>
                <div class="section cont mg st st-list pd sec-relate" data-slidecol="3" data-slidepercolumn="1">
                    <div class="position-relative fw gen_padding pd">
                        <img class="ele_img1 abs" src="/assets/img/skin/ele-bubble-color-2.png" alt="">
                        <img class="ele_img2 abs" src="/assets/img/skin/ele-radius-bottom-left-1.png" alt="">
                        <div class="container container-1600">
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-11">
                                    <div class="box-content text-center mb-4">
                                        <h2><b class="txt-gdcolor">ข่าวสารและกิจกรรมที่เกี่ยวข้อง</b></h2>
                                    </div>
                                    <div class="clearfix"></div>
                                        <div class="cont-list-swiper-custom sec-lists">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <?php foreach($relate_lists as $relate_list){ ?>
                                                    <div class="swiper-slide bg-fullwidth">
                                                        <div class="box-thumb fw position-relative ">
                                                            <a  href="<?php  echo linkType( $relate_list['link_type'],$relate_list['link_out'],$relate_list['cate_slug'],$relate_list['slug'],$page_slug  )?>">
                                                                <div class="thumb thumb-ver">
                                                                    <div class="thumb-group">
                                                                        <div class="thumb-img"><img
                                                                                src="<?php echo $relate_list['image'] ? upload_path($relate_list['image']) : '/assets/img/template/image-default.jpg'; ?>"
                                                                                class="w-100 float-left"></div>
                                                                        <div class="thumb-txt">
                                                                            <div class="thumb-txt-h"><?php echo $relate_list['title'] ?><?php echo $relate_list['sub_title'] ?></div>
                                                                            <div class="thumb-txt-p"><?php echo $relate_list['description'] ?></div>
                                                                            <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($relate_list['published_date'] , $relate_list['lang']); ?></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                                
                                            </div>
                                            <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/icon-arrow2-right.png">
                                            </div>
                                            <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/icon-arrow2-left.png">
                                            </div>
                                        </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        </div>
    </div>

