

<div id="sec_id_<?php echo $section['code'] ?>" class="section cont mg st st-list gen_margin  <?php echo $section['acc_type'] == 0 ? '' : 'st-list-acc-default'?>  <?php echo $section['custom_class'] != '' ?  customClass($section['custom_class']) : '' ?> "
    data-mg-desktop="<?php echo ar_decodeFront($section['mg_desktop'],'margin');?>"
    data-mg-mobile="<?php echo ar_decodeFront($section['mg_mobile'],'margin');?>"
    data-slidecol="<?php echo $section['column_per_view']; ?>"
    data-slidepercolumn="<?php echo $section['display_per_column']; ?>">
    <div class="bg-fullwidth fw gen_bg <?php echo getBackgroundSettingClass($section['bg_position_ver'],$section['bg_position_hoz'],$section['bg_repeat'],$section['bg_fixed'], $section['bg_size']);?>"
        data-bg-desktop="<?php echo $section['bg1_desktop'];  ?>"
        data-bg-mobile="<?php echo $section['bg1_mobile'];  ?>" data-bg-color="<?php echo $section['bg_color']; ?>">
        <div class="container <?php echo $section['section_layout'] == "fullwidth" ? 'container-fluid' : 'container-1600' ?> <?php echo $section['gen_height'] == 0 ? '' : 'gen-height' ?> "
            data-image-desktop="<?php echo $section['bg1_desktop'] != ''  ? upload_path($section['bg1_desktop']) : '/assets/img/skin/blank.png' ; ?>"
            data-image-mobile="<?php echo $section['bg1_mobile'] != ''  ? upload_path($section['bg1_mobile']) : '' ; ?>"
            data-column="<?php echo sizeof($row['columns'] ); ?>">
            <div class="position-relative fw gen_padding pd"
                data-pd-desktop="<?php echo ar_decodeFront($section['pd_desktop'],'padding');?>"
                data-pd-mobile="<?php echo ar_decodeFront($section['pd_mobile'],'padding');?>">

                <?php if ( $section['custom_class']  !=  '' ) { ?>
                    <?php if ( $section['ele_img1']  !=  '' ) { ?>
                        <img class="ele_img1 abs" src="<?php echo isset( $section['ele_img1'] ) ? upload_path($section['ele_img1']) : ''; ?>" alt="">
                    <?php } ?>
                    <?php if ( $section['ele_img2']  !=  '' ) { ?>
                        <img class="ele_img2 abs" src="<?php echo isset( $section['ele_img2'] ) ? upload_path($section['ele_img2']) : ''; ?>" alt="">
                    <?php } ?>
                <?php } ?>
            
                <?php include('inc_graphic.php') ?>

                <?php if($section['design_type'] == 'box'){ ?>
                    <?php include('list_type_box.php') ?>
                <?php }else if($section['design_type'] == 'slides'){ ?>
                    <?php include('list_type_slide.php');?>
                <?php }else if($section['design_type'] == 'accordion'){ ?>
                    <?php include('list_type_acc.php');?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>