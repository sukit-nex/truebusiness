<?php if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' || $section['design_template'] == 'table' ){ ?>
    <?php if($section['tab_show'] == 'default') { ?>
        <div class="cont-contain ml-0 mr-0 w-100">
   
            <?php if($section['title'] != '' || $section['sub_title'] != '' || $section['content'] != ''){ ?>
                <div class="sec-title <?php echo getSecPositionHoz( $section['sec_title_alignment'] )?>">
                    <div class="box-content">
                        <?php if($section['title'] != ''){ ?>
                            <h1 class="font-weight-bold"><?php echo  $section['title'] ?></h1>
                        <?php } ?>
                        <?php if($section['sub_title'] != ''){ ?><h4 class="font-weight-bold">
                            <?php echo $section['sub_title'] ?></h4>
                        <?php } ?>
                    </div>
                </div>
                <?php   if(sizeof($section['categories']) > 1){ ?>
                    <?php if($section['content'] != '' ){ ?>
                        <div class="sec-list-content">
                            <?php echo $section['content'] ?>
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>

            <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                
                <div class="clearfix"></div>
                <!-- default highlight  -->
                <?php //if($section['tab_setting'] == 'only_has'){ ?>
                    
                    <?php if($cate_key > 0){ ?>
                        <?php if(sizeof($section['categories']) > 2 && $cate_key > 0){ ?>
                            <?php if( $category['title'] != '' || $category['sub_title'] != ''){ ?>
                                <div class="sec-list-cate">
                                    <div class="box-content <?php echo getSecPositionHoz( $section['cate_title_alignment'] )?>">
                                        <?php if($category['title'] != ''){ ?>
                                            <h4 class="font-weight-bold"><?php echo  $category['title'] ?></h4>
                                        <?php } ?>
                                        <?php if($category['sub_title'] != ''){ ?>
                                            <p class=""><?php echo  $category['sub_title'] ?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="clearfix"></div>
                        <?php } ?>
                        <?php  foreach($category['listings'] as $list_key => $listing){  ?>
                            <?php if($section['hl_set'] == 'hl_set_show'){ ?>
                                <?php if($list_key == 0){?>
                                    <div class="box-thumb-hl fw">
                                        <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative">
                                            <div class="thumb  list-highligh ">
                                                <div class="thumb-group">
                                                <?php if($listing['link_type'] != 'no'){?>
                                                <?php if($listing['btn_show'] == 0){?>
                                                        <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                    <?php } ?>
                                                <?php } ?>
                                                    <div class="thumb-img"><img
                                                            src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : '/assets/img/template/image-default.jpg'; ?>"
                                                            class="w-100 float-left"></div>
                                                    <?php if($listing['link_type'] != 'no'){?>
                                                    <?php if($listing['btn_show'] == 0){?>
                                                        </a>
                                                    <?php } ?>
                                                    <?php } ?>
                                                    <div class="thumb-txt">
                                                        <?php if($listing['title'] != ''){ ?>
                                                        <div class="thumb-txt-h">
                                                            <span><?php echo $listing['title'] ?><br><?php if($listing['sub_title'] != ''){ ?> <span><?php echo $listing['sub_title'] ?></span><?php } ?></span>
                                                        </div>
                                                        <?php } ?>
                                                        <?php if($listing['price'] != ''){ ?>
                                                            <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                        <?php } ?>
                                                        <div class="thumb-tag">
                                                            <?php if( $listing['cate_title'] != ""){ ?>
                                                            <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                            <?php } ?>
                                                                <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                    <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                        <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                            <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                        </div>

                                                        <div class="position-relative fw">
                                                            <img class="ele-list-thumb-1 "
                                                                src="/assets/img/skin/line-light.png" alt="">
                                                        </div>

                                                        <?php if($listing['description'] != ''){ ?>
                                                        <div class="thumb-txt-p">
                                                            <?php echo $listing['description'] ?></div>
                                                        <?php } ?>
                                                        <?php if($listing['btn_show'] == 0 || $listing['btn_show'] == 1 ){?>
                                                            <div class="box-content box-content-button btn-hl">
                                                                <a class="btn-more"
                                                                    <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?>
                                                                    href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != "" ? $listing['btn_text'] : 'อ่านเพิ่มเติม' ?></a>
                                                            </div>
                                                        <?php } ?>

                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>

                        <!-- default highlight  -->

                        <div class="clearfix "></div>
                        
                        <?php $y = 1 ?>
                        <div class="cont-list-swiper-custom sec-lists perview_<?php echo $section['design_template'] ?>_<?php echo $section['column_per_view']; ?> ">
                            <div class="swiper-container"
                                style=" <?php echo sizeof($section['categories']) > 1  ? 'margin-bottom:20px;' : '' ?> ">
                                <div class="swiper-wrapper">
                                    <?php foreach( $category['listings'] as $list_key => $listing){ ?>
                                    
                                        <?php if($section['hl_set'] != 'hl_set_show'){ ?>
                                            <?php $y++ ?>
                                            <div class="swiper-slide <?php echo $section['design_template'] == 'table' ? 'h-100' : '' ?>  bg-fullwidth ">
                                                <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                    <?php if($listing['btn_show'] == 0){?>
                                                        <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                    <?php } ?>
                                                <?php } ?>
                                                        <?php if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){ ?>
                                                            <div
                                                                class="thumb  thumb-<?php echo designTemplate( $section['design_template'] );?>">
                                                                <div class="thumb-group">
                                                                    <?php if($listing['image'] != ""){ ?>
                                                                    <div class="thumb-img"><img
                                                                            src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : ''; ?>"
                                                                            class="w-100 float-left"></div>
                                                                    <?php } ?>
                                                                    <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                        <?php if($listing['title'] != ''){ ?>
                                                                            <div class="thumb-txt-h"><?php echo $listing['title'] ?></div>
                                                                        <?php } ?>
                                                                        <?php if($listing['sub_title'] != ''){ ?>
                                                                            <div class="thumb-txt-p"><?php echo $listing['sub_title'] ?></div>
                                                                        <?php } ?>
                                                                        <?php if($listing['price'] != ''){ ?>
                                                                            <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                        <?php } ?>
                                                                        <?php if( $section['design_template'] != "vertical"){ ?>
                                                                            <?php if( $section['link_type'] != "no"){ ?>
                                                                                <?php if($listing['btn_show'] == 1) { ?>
                                                                                    <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                        <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php }else if($section['design_template'] == 'table' ){ ?>
                                                            <div class="thumb">
                                                                <div class="thumb-group">
                                                                    <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                        <?php echo $listing['content'] ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                    <?php if($listing['btn_show'] == 0){?>
                                                        </a>
                                                    <?php } ?>
                                                    <?php } ?>

                                                    <?php if(isset($section['is_tags'])){ ?>
                                                        <?php if($section['is_tags'] == 1){ ?>
                                                            <div class="thumb-tag">
                                                                <?php if( $listing['cate_title'] != ""){ ?>
                                                                    <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                <?php } ?>
                                                                <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                    <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                        <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                            <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if( $section['design_template'] == "vertical"){ ?>
                                                        <?php if( $section['link_type'] != "no"){ ?>
                                                            <?php if($listing['btn_show'] == 1) { ?>
                                                                <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                    <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } else{ ?>
                                            <?php if($list_key > 0){ ?>
                                                <?php $y++ ?>
                                                <div class="swiper-slide <?php echo $section['design_template'] == 'table' ? 'h-100' : '' ?>  bg-fullwidth ">
                                                    <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                    <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                        <?php if($listing['btn_show'] == 0){?>
                                                            <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                        <?php } ?>
                                                    <?php } ?>
                                                            <?php if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){ ?>
                                                                <div
                                                                    class="thumb  thumb-<?php echo designTemplate( $section['design_template'] );?>">
                                                                    <div class="thumb-group">
                                                                        <?php if($listing['image'] != ""){ ?>
                                                                        <div class="thumb-img"><img
                                                                                src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : ''; ?>"
                                                                                class="w-100 float-left"></div>
                                                                        <?php } ?>
                                                                        <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                            <?php if($listing['title'] != ''){ ?>
                                                                                <div class="thumb-txt-h"><?php echo $listing['title'] ?></div>
                                                                            <?php } ?>
                                                                            <?php if($listing['sub_title'] != ''){ ?>
                                                                                <div class="thumb-txt-p"><?php echo $listing['sub_title'] ?></div>
                                                                            <?php } ?>
                                                                            <?php if($listing['price'] != ''){ ?>
                                                                                <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                            <?php } ?>
                                                                            <?php if( $section['design_template'] != "vertical"){ ?>
                                                                                <?php if( $section['link_type'] != "no"){ ?>
                                                                                    <?php if($listing['btn_show'] == 1) { ?>
                                                                                        <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                            <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php }else if($section['design_template'] == 'table' ){ ?>
                                                                <div class="thumb">
                                                                    <div class="thumb-group">
                                                                        <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                            <?php echo $listing['content'] ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                        <?php if($listing['btn_show'] == 0){?>
                                                            </a>
                                                        <?php } ?>
                                                        <?php } ?>

                                                        <?php if(isset($section['is_tags'])){ ?>
                                                            <?php if($section['is_tags'] == 1){ ?>
                                                                <div class="thumb-tag">
                                                                    <?php if( $listing['cate_title'] != ""){ ?>
                                                                        <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                    <?php } ?>
                                                                    <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                    <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                        <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                            <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        <?php if( $section['design_template'] == "vertical"){ ?>
                                                            <?php if( $section['link_type'] != "no"){ ?>
                                                                <?php if($listing['btn_show'] == 1) { ?>
                                                                    <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                        <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div>

                                    
                            </div>
                            <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/icon-arrow2-right.png">
                            </div>
                            <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/icon-arrow2-left.png">
                            </div>
                        </div>
                    <?php } ?>
                <?php // } ?>
                
            
            
            
            <?php if($section['tab_setting'] == 'only_has'){ ?>
                <?php if($cate_key > 0){ ?>
                    <?php if($section['btn_more'] == 'btn_more_none'){ ?>

                    <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                        <?php if($section['link_type'] == 'no'){ ?>
                        
                        <?php }else if($section['link_type'] == 'external_link'){  ?>
                            <div class="box-content text-center float-left w-100 position-relative btn-list-more"  style="z-index:10;">
                                <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                            </div>
                        <?php }else if($section['link_type'] == 'internal_link'){  ?>
                            <div class="box-content text-center float-left w-100  position-relative btn-list-more"  style="z-index:10;">
                                <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                        
                            </div>
                        <?php } ?>
                    <?php }else if($section['btn_more'] == 'btn_load_more'){  ?>  
                        
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        <?php } ?>
            
        </div>
    <?php }else if($section['tab_show'] == 'tabs'){ ?>
        <div class="cont-contain ml-0 mr-0 w-100">
            <?php if($section['title'] != '' || $section['sub_title'] != '' || $section['content'] != ''){ ?>
                <div class="sec-title <?php echo getSecPositionHoz( $section['sec_title_alignment'] )?>">
                    <div class="box-content">
                        <?php if($section['title'] != ''){ ?>
                            <h1 class="font-weight-bold"><?php echo  $section['title'] ?></h1>
                        <?php } ?>
                        <?php if($section['sub_title'] != ''){ ?><h4 class="font-weight-bold">
                            <?php echo $section['sub_title'] ?></h4><?php } ?>
                    </div>
                    <?php if($section['content'] != '' ){ ?>
                        <?php echo $section['content'] ?>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php   if(sizeof($section['categories']) > 1){ ?>
            <div class="box-tab tab-style-1 fw">
                <div class="tab-nav  <?php echo getSecPositionHoz( $section['sec_title_alignment'] )?>">
                    <ul>
                        <?php if($section['tab_setting'] == 'all_content'){  ?>
                        <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                        <?php if($cate_key == 0){ ?>
                        <li class="tab-nav-list"><a href="javascript:void(0)" data-tab="<?php echo $cate_key ?>"
                                class="tab-button box-content">All </a></li>
                        <?php }else{ ?>
                        <li class="tab-nav-list"><a href="javascript:void(0)"
                                data-tab="<?php echo $cate_key; ?>"
                                class="tab-button box-content"><?php echo  $category['title'] ?></a></li>
                        <?php } ?>
                        <?php } ?>
                        <?php }else if($section['tab_setting'] == 'only_has'){ ?>
                        <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                        <?php if($cate_key != 0){ ?>
                        <li class="tab-nav-list"><a href="javascript:void(0)"
                                data-tab="<?php echo $cate_key; ?>"
                                class="tab-button box-content"><?php echo  $category['title'] ?></a></li>
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
               
                
                <div class="sec-lists append-content-list-<?php echo $section['code'] ?>">
                    <div class="tab-content">
                        <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                            <?php // print_r($section['categories']) ?>
                            <?php $content_data = 1 ?>
                            <div class="tab-content-list fw nav-link-<?php echo $cate_key ?>" style="display:none;">
                            <?php if(sizeof($section['categories']) > 2 && $cate_key > 0){ ?>
                                <?php if( $category['sub_title'] != ''){ ?>
                                    <div class="sec-list-cate">
                                        <div class="box-content <?php echo getSecPositionHoz( $section['cate_title_alignment'] )?>">
                                            <?php if($category['sub_title'] != ''){ ?>
                                                <p class=""><?php echo  $category['sub_title'] ?></p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="clearfix"></div>
                            <?php } ?>
                            <div class="clearfix"></div>
                            <div class="row row_cols <?php echo $section['design_template'] == 'table' ? 'row-eq-height ' : '' ?>"
                            data-row-cols-desktop="<?php echo $section['column_per_view'] ?>" data-row-cols-check="<?php echo sizeof($category['listings']) ?>"
                            data-row-cols-mobile="<?php echo $section['display_per_column'] ?>">
                                <?php  foreach($category['listings'] as $list_key => $listing){  ?>
                                    <?php if($section['hl_set'] == 'hl_set_show'){ ?>
                                        <?php if($list_key == 0){?>
                                            <div class="col-12">
                                                <div class="box-thumb-hl fw">
                                                    <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative">
                                                        <div class="thumb  list-highligh ">
                                                            <div class="thumb-group">
                                                            <?php if($listing['link_type'] != 'no'){?>
                                                            <?php if($listing['btn_show'] == 0){?>
                                                                <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                            <?php } ?>
                                                            <?php } ?>
                                                                <div class="thumb-img"><img
                                                                        src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : '/assets/img/template/image-default.jpg'; ?>"
                                                                        class="w-100 float-left"></div>
                                                                <?php if($listing['link_type'] != 'no'){?>
                                                                <?php if($listing['btn_show'] == 0){?>
                                                                        </a>                                                        
                                                                <?php } ?>
                                                                <?php } ?>
                                                                <div class="thumb-txt">
                                                                    <?php if($listing['title'] != ''){ ?>
                                                                    <div class="thumb-txt-h">
                                                                        <span><?php echo $listing['title'] ?><br><?php if($listing['sub_title'] != ''){ ?> <span><?php echo $listing['sub_title'] ?></span><?php } ?></span>
                                                                    </div>
                                                                    <?php } ?>
                                                                    <?php if($listing['price'] != ''){ ?>
                                                                        <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                    <?php } ?>
                                                                    <div class="thumb-tag">
                                                                        <?php if( $listing['cate_title'] != ""){ ?>
                                                                        <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                        <?php } ?>
                                                                            <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                        <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                            <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                                <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                    <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </div>

                                                                    <div class="position-relative fw">
                                                                        <img class="ele-list-thumb-1 "
                                                                            src="/assets/img/skin/line-light.png" alt="">
                                                                    </div>

                                                                    <?php if($listing['description'] != ''){ ?>
                                                                    <div class="thumb-txt-p">
                                                                        <?php echo $listing['description'] ?></div>
                                                                    <?php } ?>
                                                                    <?php if($listing['btn_show'] == 1 ){?>
                                                                        <div class="box-content box-content-button btn-hl">
                                                                            <a class="btn-more"
                                                                                <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?>
                                                                                href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != "" ? $listing['btn_text'] : 'อ่านเพิ่มเติม' ?></a>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                                <div class="cont-list-swiper-custom sec-lists perview_<?php echo $section['design_template'] ?>_<?php echo $section['column_per_view']; ?> ">
                                    <div class="swiper-container"
                                        style=" <?php echo sizeof($section['categories']) > 1  ? 'margin-bottom:20px;' : '' ?> ">

                                        <div class="swiper-wrapper">
                                            <?php foreach( $category['listings'] as $list_key => $listing){ ?>
                                                <?php if($section['hl_set'] == 'hl_set_show'){ ?>
                                                    <?php if($list_key > 0){?>
                                                        <div class="swiper-slide <?php echo $section['design_template'] == 'table' ? 'h-100' : '' ?>  bg-fullwidth">
                                                            <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                            <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                                <?php if($listing['btn_show'] == 0){?>
                                                                    <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                                <?php } ?>
                                                            <?php } ?>
                                                                <?php if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){ ?>
                                                                    <div class="thumb thumb-<?php echo designTemplate( $section['design_template'] );?>">
                                                                        <div class="thumb-group">
                                                                            <?php if($listing['image'] != ""){ ?>
                                                                            <div class="thumb-img">
                                                                                <img src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : ''; ?>"
                                                                                    class="w-100 float-left">
                                                                            </div>
                                                                            <?php } ?>
                                                                            <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                <?php if($listing['title'] != ''){ ?>
                                                                                    <div class="thumb-txt-h"><?php echo $listing['title'] ?></div>
                                                                                <?php } ?>
                                                                                <?php if($listing['sub_title'] != ''){ ?>
                                                                                    <div class="thumb-txt-p"><?php echo $listing['sub_title'] ?></div>
                                                                                <?php } ?>
                                                                                <?php if($listing['price'] != ''){ ?>
                                                                                    <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                                <?php } ?>
                                                                                <?php if( $section['design_template'] != "vertical"){ ?>
                                                                                    <?php if( $section['link_type'] != "no"){ ?>
                                                                                        <?php if($listing['btn_show'] == 1) { ?>
                                                                                            <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                                <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                                            </div>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php }else if($section['design_template'] == 'table' ){ ?>
                                                                <div class="thumb">
                                                                    <div class="thumb-group">
                                                                        <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                            <?php echo $listing['content'] ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                                <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                                <?php if($listing['btn_show'] == 0){?>
                                                                    </a>                                        
                                                                <?php } ?>
                                                                <?php } ?>
                                                                <?php if(isset($section['is_tags'])){ ?>
                                                                    <?php if($section['is_tags'] == 1){ ?>
                                                                        <div class="thumb-tag">
                                                                            <?php if( $listing['cate_title'] != ""){ ?>
                                                                                <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                            <?php } ?>
                                                                            <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                            <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                                <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                                    <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                        <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                <?php if( $section['design_template'] == "vertical"){ ?>
                                                                    <?php if( $section['link_type'] != "no"){ ?>
                                                                        <?php if($listing['btn_show'] == 1) { ?>
                                                                            <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                            </div>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php }else{ ?>
                                                    
                                                    <?php if($list_key >= 0){?>
                                                    <div class="swiper-slide <?php echo $section['design_template'] == 'table' ? 'h-100' : '' ?>  bg-fullwidth">
                                                        <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                            <?php ?>
                                                            <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                            <?php if($listing['btn_show'] == 0){?>
                                                                <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                            <?php } ?>
                                                            <?php } ?>
                                                            <?php  if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){ ?>
                                                            <div class="thumb thumb-<?php echo designTemplate( $section['design_template'] );?>">
                                                                <div class="thumb-group">
                                                                    <?php if($listing['image'] != ""){ ?>
                                                                    <div class="thumb-img">
                                                                        <img src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : ''; ?>"
                                                                            class="w-100 float-left">
                                                                    </div>
                                                                    <?php } ?>
                                                                    <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                        <?php if($listing['title'] != ''){ ?>
                                                                            <div class="thumb-txt-h"> <?php echo $listing['title'] ?></div>
                                                                        <?php } ?>
                                                                        <?php if($listing['sub_title'] != ''){ ?>
                                                                            <div class="thumb-txt-p"><?php echo $listing['sub_title'] ?></div>
                                                                        <?php } ?>
                                                                        <?php if($listing['price'] != ''){ ?>
                                                                            <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                        <?php } ?>
                                                                        <?php if( $section['design_template'] != "vertical"){ ?>
                                                                            <?php if( $section['link_type'] != "no"){ ?>
                                                                                <?php if($listing['btn_show'] == 1) { ?>
                                                                                    <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                        <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php }else if($section['design_template'] == 'table' ){ ?>
                                                                <div class="thumb">
                                                                    <div class="thumb-group">
                                                                        <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                            <?php echo $listing['content'] ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                            <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                            <?php if($listing['btn_show'] == 0){?>
                                                                </a>                                        
                                                            <?php } ?>
                                                            <?php } ?>
                                                            <?php if(isset($section['is_tags'])){ ?>
                                                                <?php if($section['is_tags'] == 1){ ?>
                                                                    <div class="thumb-tag">
                                                                        <?php if( $listing['cate_title'] != ""){ ?>
                                                                            <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                        <?php } ?>
                                                                        <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                        <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                            <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                                <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                    <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <?php if( $section['design_template'] == "vertical"){ ?>
                                                                <?php if( $section['link_type'] != "no"){ ?>
                                                                    <?php if($listing['btn_show'] == 1) { ?>
                                                                        <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                            <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php $content_data++ ?>
                                        </div>
                                        
                                        
                                        <!-- <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/icon-arrow2-right.png"> </div>
                                        <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/icon-arrow2-left.png"></div> -->
                                    </div>
                                    <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/icon-arrow2-right.png"> </div>
                                    <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/icon-arrow2-left.png"></div>
                                </div>   
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if($section['tab_setting'] == 'all_content'){ ?>
                
                    <?php if($section['btn_more'] == 'btn_more_none'){ ?>
                            
                    <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                        <?php if($section['link_type'] == 'no'){ ?>
                        
                        <?php }else if($section['link_type'] == 'external_link'){  ?>
                            <div class="box-content text-center float-left w-100 position-relative btn-list-more"  style="z-index:10;">
                                <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                            </div>
                        <?php }else if($section['link_type'] == 'internal_link'){  ?>
                            <div class="box-content text-center float-left w-100  position-relative btn-list-more"  style="z-index:10;">
                                <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                        
                            </div>
                        <?php } ?>
                    <?php }else if($section['btn_more'] == 'btn_load_more'){  ?>  
                        
                    <?php } ?>
                
                <?php }else if($section['tab_setting'] == 'only_has'){ ?>
                    <?php if($cate_key > 0){ ?>
                        <?php if($section['btn_more'] == 'btn_more_none'){ ?>
                            
                        <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                            <?php if($section['link_type'] == 'no'){ ?>
                            
                            <?php }else if($section['link_type'] == 'external_link'){  ?>
                                <div class="box-content text-center float-left w-100 position-relative btn-list-more"  style="z-index:10;">
                                    <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                </div>
                            <?php }else if($section['link_type'] == 'internal_link'){  ?>
                                <div class="box-content text-center float-left w-100  position-relative btn-list-more"  style="z-index:10;">
                                    <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                    <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                            
                                </div>
                            <?php } ?>
                        <?php }else if($section['btn_more'] == 'btn_load_more'){  ?>  
                            
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
        </div>
    <?php } ?>
    <?php }else if($section['tab_show'] == 'merge'){  ?>
        <div class="cont-contain ml-0 mr-0 w-100">
            <?php if($section['title'] != '' || $section['sub_title'] != '' || $section['content'] != ''){ ?>
                <div class="sec-title <?php echo getSecPositionHoz( $section['sec_title_alignment'] )?>">
                    <div class="box-content">
                        <?php if($section['title'] != ''){ ?>
                            <h1 class="font-weight-bold"><?php echo  $section['title'] ?></h1>
                        <?php } ?>
                        <?php if($section['sub_title'] != ''){ ?><h4 class="font-weight-bold">
                            <?php echo $section['sub_title'] ?></h4><?php } ?>
                    </div>
                    <?php if($section['content'] != '' ){ ?>
                        <?php echo $section['content'] ?>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php   if(sizeof($section['categories']) > 1){ ?>
            <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                    
                <?php if($cate_key == 0){ ?>
                    <?php  foreach($category['listings'] as $list_key => $listing){  ?>
                        <?php if($section['hl_set'] == 'hl_set_show'){ ?>
                            <?php if($list_key == 0){?>
                                <div class="box-thumb-hl fw">
                                    <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative">
                                        <div class="thumb  list-highligh ">
                                            <div class="thumb-group">
                                            <?php if($listing['link_type'] != 'no'){?>
                                            <?php if($listing['btn_show'] == 0){?>
                                                    <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                <?php } ?>
                                            <?php } ?>
                                                <div class="thumb-img"><img
                                                        src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : '/assets/img/template/image-default.jpg'; ?>"
                                                        class="w-100 float-left"></div>
                                                <?php if($listing['link_type'] != 'no'){?>
                                                <?php if($listing['btn_show'] == 0){?>
                                                    </a>
                                                <?php } ?>
                                            <?php } ?>
                                                <div class="thumb-txt">
                                                    <?php if($listing['title'] != ''){ ?>
                                                    <div class="thumb-txt-h">
                                                        <span><?php echo $listing['title'] ?><br><?php if($listing['sub_title'] != ''){ ?> <span><?php echo $listing['sub_title'] ?></span><?php } ?></span>
                                                    </div>
                                                    <?php } ?>
                                                    <?php if($listing['price'] != ''){ ?>
                                                        <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                    <?php } ?>
                                                    <div class="thumb-tag">
                                                        <?php if( $listing['cate_title'] != ""){ ?>
                                                        <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                        <?php } ?>
                                                            <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                            <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                    <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                        <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                    </div>

                                                    <div class="position-relative fw">
                                                        <img class="ele-list-thumb-1 "
                                                            src="/assets/img/skin/line-light.png" alt="">
                                                    </div>

                                                    <?php if($listing['description'] != ''){ ?>
                                                    <div class="thumb-txt-p">
                                                        <?php echo $listing['description'] ?></div>
                                                    <?php } ?>
                                                    <?php if($listing['btn_show'] == 0 || $listing['btn_show'] == 1 ){?>
                                                        <div class="box-content box-content-button btn-hl">
                                                            <a class="btn-more"
                                                                <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?>
                                                                href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != "" ? $listing['btn_text'] : 'อ่านเพิ่มเติม' ?></a>
                                                        </div>
                                                    <?php } ?>

                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                    
                    <!-- Slides highlight -->

                    <div class="clearfix"></div>
                    <?php $y = 1 ?>

                    <div class="cont-list-swiper-custom sec-lists perview_<?php echo $section['design_template'] ?>_<?php echo $section['column_per_view']; ?> ">
                        <div class="swiper-container"
                            style=" <?php echo sizeof($section['categories']) > 1  ? 'margin-bottom:20px;' : '' ?> ">
                            <div class="swiper-wrapper">
                                <?php foreach( $category['listings'] as $list_key => $listing){ ?>
                                    <?php if($section['hl_set'] != 'hl_set_show'){ ?>
                                        <?php $y++ ?>
                                        <div class="swiper-slide <?php echo $section['design_template'] == 'table' ? 'h-100' : '' ?>  bg-fullwidth">
                                            <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                            <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                <?php if($listing['btn_show'] == 0){?>
                                                    <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                <?php } ?>
                                            <?php } ?>
                                                <?php if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){ ?>
                                                <div
                                                    class="thumb thumb-<?php echo designTemplate( $section['design_template'] );?>">
                                                    <div class="thumb-group">
                                                        <?php if($listing['image'] != ""){ ?>
                                                        <div class="thumb-img"><img
                                                                src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : ''; ?>"
                                                                class="w-100 float-left"></div>
                                                        <?php } ?>
                                                        <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                            <?php if($listing['title'] != ''){ ?>
                                                                <div class="thumb-txt-h"><?php echo $listing['title'] ?></div>
                                                            <?php } ?>
                                                            <?php if($listing['sub_title'] != ''){ ?>
                                                                <div class="thumb-txt-p"><?php echo $listing['sub_title'] ?></div>
                                                            <?php } ?>
                                                            <?php if($listing['price'] != ''){ ?>
                                                                <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                            <?php } ?>
                                                            <?php if( $section['design_template'] != "vertical"){ ?>
                                                                <?php if( $section['link_type'] != "no"){ ?>
                                                                    <?php if($listing['btn_show'] == 1) { ?>
                                                                        <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                            <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php }else if($section['design_template'] == 'table' ){ ?>
                                                    <div class="thumb">
                                                        <div class="thumb-group">
                                                            <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                <?php echo $listing['content'] ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                <?php if($listing['btn_show'] == 0){?>
                                                    </a>
                                                <?php } ?>
                                                <?php } ?>

                                                <?php if(isset($section['is_tags'])){ ?>
                                                    <?php if($section['is_tags'] == 1){ ?>
                                                        <div class="thumb-tag">
                                                            <?php if( $listing['cate_title'] != ""){ ?>
                                                                <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                            <?php } ?>
                                                            <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                            <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                    <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                        <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                                <?php if( $section['design_template'] == "vertical"){ ?>
                                                    <?php if( $section['link_type'] != "no"){ ?>
                                                        <?php if($listing['btn_show'] == 1) { ?>
                                                            <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php }else{ ?>
                                        <?php if($list_key > 0){ ?>
                                            <div class="swiper-slide <?php echo $section['design_template'] == 'table' ? 'h-100' : '' ?>  bg-fullwidth">
                                                <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                    <?php if($listing['btn_show'] == 0){?>
                                                        <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                    <?php } ?>
                                                <?php } ?>
                                                    <?php if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){ ?>
                                                    <div
                                                        class="thumb thumb-<?php echo designTemplate( $section['design_template'] );?>">
                                                        <div class="thumb-group">
                                                            <?php if($listing['image'] != ""){ ?>
                                                            <div class="thumb-img"><img
                                                                    src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : ''; ?>"
                                                                    class="w-100 float-left"></div>
                                                            <?php } ?>
                                                            <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                <?php if($listing['title'] != ''){ ?>
                                                                    <div class="thumb-txt-h"><?php echo $listing['title'] ?></div>
                                                                <?php } ?>
                                                                <?php if($listing['sub_title'] != ''){ ?>
                                                                    <div class="thumb-txt-p"><?php echo $listing['sub_title'] ?></div>
                                                                <?php } ?>
                                                                <?php if($listing['price'] != ''){ ?>
                                                                    <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                <?php } ?>
                                                                <?php if( $section['design_template'] != "vertical"){ ?>
                                                                    <?php if( $section['link_type'] != "no"){ ?>
                                                                        <?php if($listing['btn_show'] == 1) { ?>
                                                                            <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                            </div>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }else if($section['design_template'] == 'table' ){ ?>
                                                        <div class="thumb">
                                                            <div class="thumb-group">
                                                                <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                    <?php echo $listing['content'] ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                    <?php if($listing['btn_show'] == 0){?>
                                                        </a>
                                                    <?php } ?>
                                                    <?php } ?>

                                                    <?php if(isset($section['is_tags'])){ ?>
                                                        <?php if($section['is_tags'] == 1){ ?>
                                                            <div class="thumb-tag">
                                                                <?php if( $listing['cate_title'] != ""){ ?>
                                                                    <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                <?php } ?>
                                                                <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                    <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                        <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                            <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if( $section['design_template'] == "vertical"){ ?>
                                                        <?php if( $section['link_type'] != "no"){ ?>
                                                            <?php if($listing['btn_show'] == 1) { ?>
                                                                <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                    <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>

                                <?php } ?>
                            </div>
                                
                        </div>
                        <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/icon-arrow2-right.png"></div>
                        <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/icon-arrow2-left.png"></div>
                    </div><!-- cont-list-swiper-custom sec-lists perview_<?php echo $section['design_template'] ?>_<?php echo $section['column_per_view']; ?>  -->
                    
                <?php } ?>
            <?php } ?>
            <?php if($section['btn_more'] == 'btn_more_none'){ ?>
                                        
            <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                <?php if($section['link_type'] == 'no'){ ?>
                
                <?php }else if($section['link_type'] == 'external_link'){  ?>
                    <div class="box-content text-center float-left w-100 position-relative btn-list-more"  style="z-index:10;">
                        <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                    </div>
                <?php }else if($section['link_type'] == 'internal_link'){  ?>
                    <div class="box-content text-center float-left w-100  position-relative btn-list-more"  style="z-index:10;">
                        <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                        <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                    </div>
                <?php } ?>
            <?php }else if($section['btn_more'] == 'btn_load_more'){  ?>  
                
            <?php } ?>
            <?php } ?>
        </div>
    <?php } ?>  
<?php } ?>