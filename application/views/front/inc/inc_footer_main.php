<div class="sec-prefooter">
  <div class="container">
    <div class="box-prefooter t-bold">
      <div class="prefoot-contact-title">สอบถามข้อมูลเพิ่มเติมได้ที่</div>
      <div class="prefoot-contact-group">
        <div class="prefoot-contact-group-call">
          <div class="prefoot-contact-group-call-text">ศูนย์บริการ<br>ลูกค้า</div>
          <a href="tel:1239" class="prefoot-contact-group-call-num txt-gdcolor">1239</a>
        </div>
        <div class="prefoot-contact-group-or t-medium"><span>หรือ</span></div>
        <div class="prefoot-contact-group-call"><a href="javascript:void(0)" onclick="popin('popup-form',$(this))" data-formTopic="ขอรายละเอียดเพิ่มเติม" class="btn btn-secondary"><span>คลิก เพื่อขอข้อมูล</span></a></div>
      </div>
    </div> 
  </div>
</div>

<!-- footer  -->
<footer class="">
    <div class="container-xl container-md container-sm position-relative">
      <!-- btnmenu back to top foe mobile  -->
      <button type="button" class="btnmenu d-md-none btnmenu-back-to-top" onclick="backToTop()">
        <span class="icon-arrow-up align-middle lh-1 fs-4 fw-bold"></span>
      </button>
      <div class="col-12 pt-3 pt-md-5 pb-2 pb-md-4">
        <div class="row">
          <div class="col-12 col-lg-8 fs-small menu-footer d-none d-md-block">
            <div class="d-flex flex-wrap">
              <!-- True services -->
              <ul class="list-unstyled fw-light mb-0 me-5">
                <li class="fw-bold fs-5">True services</li>
                  <li>
                    <a href="">Mobile</a>
                  </li>
                  <li>
                    <a href="">Internet</a>
                  </li>
                  <li>
                    <a href="">Digital TV</a>
                  </li>
                  <li>
                    <a href="">Fixedline</a>
                  </li>
                  <li>
                    <a href="">Enterprise Solutions</a>
                  </li>
              </ul>
              <!-- Support  -->
              <ul class="list-unstyled fw-light mb-0 me-5">
                <li class="fw-bold fs-5">
                  Support
                </li>
                  <li>
                    <a href="">Email Us</a>
                  </li>
                  <li>
                    <a href="">Find a True store</a>
                  </li>
                  <li>
                    <a href="">Contact us 1239</a>
                  </li>
              </ul>
              <!-- Digital services  -->
              <ul class="list-unstyled fw-light mb-0 me-5">
                <li class="fw-bold fs-5">
                  My Account
                </li>
                  <li>
                    <a href="">Pay bills</a>
                  </li>
                  <li>
                    <a href="">Check usage</a>
                  </li>
                  <li>
                    <a href="">Manage my account</a>
                  </li>
                  <li>
                    <a href="">Apply e-Bill, e-Tax Invoice</a>
                  </li>
                  <li>
                    <a href="">Register TrueBusiness iService</a>
                  </li>
              </ul>
              <!-- My Account  -->
              <ul class="list-unstyled fw-light mb-0">
                <li class="fw-bold fs-5">
                  FAQ
                </li>
                  <li>
                    <a href="">How can True help my business?</a>
                  </li>
                  <li>
                    <a href="">What is TrueBusiness iService?</a>
                  </li>
                  <li>
                    <a href="">What is TrueSphere?</a>
                  </li>
              </ul>
            </div>

          </div>
          <div class="col-12 col-md-4 menu-footer">
            <p class="fw-bold fs-4">
              Follow us
            </p>
            <ul class="d-flex  list-unstyled fw-light mb-0">
              <li>
                <a href=""><span class="icon-facebook color-white me-3 fs-4"></span></a>
              </li>
              <li>
                <a href=""><span class="icon-instagram color-white me-3 fs-4"></span></a>
              </li>
              <li>
                <a href=""><span class="icon-youtube color-white me-3 fs-4"></span></a>
              </li>
              <li>
                <a href=""><span class="icon-line color-white fs-4"></span></a>
              </li>
            </ul>
          </div>
        </div>

        <!-- Footer for mobile  -->
        <hr class="d-md-none mb-0">
        <div class="d-md-none accordion accordion-flush accordion-footer" id="accordionFlushExample">

          <!-- True services  -->
          <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingOne">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                True business services
              </button>
            </h2>
            <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne"
              data-bs-parent="#accordionFlushExample">
              <div class="accordion-body pt-0">
                <!-- True services -->
                <ul class="list-unstyled fw-light">
                  <li>
                    <a href="">Mobile</a>
                  </li>
                  <li>
                    <a href="">Internet</a>
                  </li>
                  <li>
                    <a href="">Digital TV</a>
                  </li>
                  <li>
                    <a href="">Fixedline</a>
                  </li>
                  <li>
                    <a href="">Solutions</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <!-- Digital services  -->
          <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingTwo">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                My Account
              </button>
            </h2>
            <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo"
              data-bs-parent="#accordionFlushExample">
              <div class="accordion-body">
                <!-- Digital services  -->
                <ul class="list-unstyled fw-light mb-0 me-5">
                  <li>
                    <a href="">Pay bills</a>
                  </li>
                  <li>
                    <a href="">Check usage</a>
                  </li>
                  <li>
                    <a href="">Manage account & services</a>
                  </li>
                  <li>
                    <a href="">Register TrueBusiness iService</a>
                  </li>
                  <li>
                    <a href="">Apply WHT, e-Bills, e-Tax</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <!-- Support  -->
          <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingTwo">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                Support
              </button>
            </h2>
            <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo"
              data-bs-parent="#accordionFlushExample">
              <div class="accordion-body">
                <!-- Support  -->
                <ul class="list-unstyled fw-light mb-0 me-5">
                  <li>
                    <a href="">Email Us</a>
                  </li>
                  <li>
                    <a href="">Find a True store</a>
                  </li>
                  <li>
                    <a href="">Contact us 1239</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <!-- My Account  -->
          <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingThree">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                FAQ
              </button>
            </h2>
            <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree"
              data-bs-parent="#accordionFlushExample">
              <div class="accordion-body">
                <!-- My Account  -->
                <ul class="list-unstyled fw-light mb-0">
                  <li>
                    <a href="">How can True help my business?</a>
                  </li>
                  <li>
                    <a href="">What is TrueBusiness iService?</a>
                  </li>
                  <li>
                    <a href="">What is TrueSphere?</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr class="color-white">
      <div class="d-flex justify-content-between fw-light fs-small pb-3 flex-column flex-md-row">
        <div class="text-center text-md-start lh-1">
          ©True Corporation Public Company Limited All rights reserved
        </div>
        <div class="d-flex justify-content-center">
          <a class="link-menu" href="">Terms of Use</a>
          <span class="mx-2"> • </span>

          <a class="link-menu" href="">Privacy Policy</a>
        </div>
      </div>
    </div>
  </footer>





  <div class="content_on_popup" style="display:none">
    <div class="form-api">
        <div class="row justify-content-center">
            <div class="col col-12 col-md-9">
                <div class="form-group form-group-hoz">
                    <label for="company_name">ชื่อบริษัท *</label>
                    <input type="text" id="company_name" name="company_name" data-required data-error-message="กรุณาระบุข้อมูล">
                </div>
                <div class="form-group form-group-hoz">
                    <label for="">เลขทะเบียนพาณิชย์ *</label>
                    <input type="text" data-required data-error-message="กรุณาระบุข้อมูล">
                </div>
                <div class="form-group form-group-hoz">
                    <label for="name">ชื่อผู้ติดต่อ *</label>
                    <input type="text" id="name" name="name" data-required data-error-message="กรุณาระบุข้อมูล">
                </div>
                <div class="form-group form-group-hoz">
                    <label for="phone">หมายเลขโทรศัพท์ *</label>
                    <input type="text" id="phone" name="phone" data-required data-error-message="กรุณาระบุข้อมูล">
                </div>
                <div class="form-group form-group-hoz">
                    <label for="">ต่อ</label>
                    <input type="text" data-required data-error-message="กรุณาระบุข้อมูล">
                </div>
                <div class="form-group form-group-hoz">
                    <label for="">อีเมล *</label>
                    <input type="text" data-required data-error-message="กรุณาระบุข้อมูล">
                </div>
                <div class="form-group form-group-hoz">
                    <label for="">รายละเอียด *</label>
                    <textarea name="" id="" cols="30" rows="3"></textarea>
                </div>
                <div class="form-group form-group-hoz">
                    <div class="check-group checkinline">
                        <div class="check-title"></div>
                        <div class="check-list">
                            <input id="ca0" type="checkbox" name="prefer-check" value="phone call" class="check" >
                            <label for="ca0">ข้าพเจ้ายอมรับ <a href="http://" class="" target="_blank" rel="noopener noreferrer">นโยบายส่วนบุคคล</a> และขอรับรองว่าคำตอบและข้อความทั้งหมดที่ข้าพเจ้าให้ไว้ในใบสมัครของบริษัทกรุงศรี คอนซูมเมอร์นี้เป็นความจริงทุกประการ 
                        </label>
                        </div>
                    </div>
                </div>
                <div class="text-center fw"><button class="btn"><span>ส่งข้อมูล</span></button></div>
            </div>
        </div>
    </div>
</div>