<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Product">
<head>
<?php 
if( sizeof($page_seo) > 0 ){
    $title = (isset($page_seo['page_title']) && $page_seo['page_title'] != "") ? $page_seo['page_title'] : $global_seo['page_title'];
    $meta_title = (isset($page_seo['page_title']) && $page_seo['meta_title'] != "") ? $page_seo['meta_title'] : $global_seo['meta_title'];
    $meta_keyword = (isset($page_seo['page_title']) && $page_seo['meta_keyword'] != "") ? $page_seo['meta_keyword'] : $global_seo['meta_keyword'];
    $meta_desc = (isset($page_seo['page_title']) && $page_seo['meta_desc'] != "") ? $page_seo['meta_desc'] : $global_seo['meta_desc'];
    $share_img = (isset($page_seo['page_title']) && $page_seo['share_img'] != "") ? $page_seo['share_img'] : $global_seo['share_img'];
    $actual_link = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}else{
    $title = $global_seo['page_title'];
    $meta_title = $global_seo['meta_title'];
    $meta_keyword = $global_seo['meta_keyword'];
    $meta_desc = $global_seo['meta_desc'];
    $share_img = $global_seo['share_img'];
    $actual_link = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title><?php echo $title; ?></title>
<meta name="description" content="<?php echo $meta_desc; ?>" />
<meta name="keywords" content="<?php echo $meta_keyword; ?>" />
                        
<!-- Facebook+ -->  
<meta property="og:title" content="<?php echo $meta_title; ?>">
<meta property="og:image" content="<?php echo base_url().'uploads/'.$share_img; ?>">
<meta property="og:description" content="<?php echo $meta_desc; ?>">
<meta property="og:url" content="<?php echo $actual_link; ?>">
<meta property="og:type" content="website" /> 


 
<link rel="shortcut icon" href="/assets/img/skin/favicon.png" type="image/x-icon"/>
<link type="text/css" rel="stylesheet" href="/assets/css/reset.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap4_5.min.css"/>
<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-select.min.css"/>
<link type="text/css" rel="stylesheet" href="/assets/css/swiper.min.css" />
<link type="text/css" rel="stylesheet" href="/assets/css/mapbox-gl.css" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<link type="text/css" rel="stylesheet" href="/assets/css/stylesheet.min.css" />
<link type="text/css" rel="stylesheet" href="/assets/css/global.css" />
<link type="text/css" rel="stylesheet" href="/assets/css/sec-custom-1.css" />
<!-- <?php if(isset($css) && $css != "" ){ ?>
<link type="text/css" rel="stylesheet" href="/assets/css/<?php echo $css; ?>.css" />
<?php } ?> -->

<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-34289891-14"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-34289891-14');
</script> -->
<link rel='stylesheet' href='<?php echo base_url(); ?>/assets/css/bundle.min.php' type='text/css' media='all'>

<?php 
if( sizeof($landing_scripts) > 0 ){ 
    echo $landing_scripts['scripts_header'];
}
?>

<?php if( isset( $rows ) ){ ?>
<?php //print_r( $rows ); ?>
<style>
    <?php foreach( $rows as $row ){
        foreach( $row['columns'] as $column ){
            foreach( $column['sections'] as $section ){
                if( $section['custom_css'] != '' ){ 
                    echo trim($section['custom_css']," ");
                }
            }
        }
    } ?>
</style>
<?php } ?>
</head>

<body class="<?php echo $body_class;  ?>">
<?php 
if( sizeof($landing_scripts) > 0 ){ 
    echo $landing_scripts['scripts_body'];
}
?>
<?php /*
<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Product">
<head>
<?php 
if( sizeof($page_seo) > 0 ){
	$title = $page_seo['page_title'] != "" ? $page_seo['page_title'] : $global_seo['page_title'];
	$meta_title = $page_seo['meta_title'] != "" ? $page_seo['meta_title'] : $global_seo['meta_title'];
	$meta_keyword = $page_seo['meta_keyword'] != "" ? $page_seo['meta_keyword'] : $global_seo['meta_keyword'];
	$meta_desc = $page_seo['meta_desc'] != "" ? $page_seo['meta_desc'] : $global_seo['meta_desc'];
	$share_img = $page_seo['share_img'] != "" ? $page_seo['share_img'] : $global_seo['share_img'];
	$actual_link = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}else{
	$title = $global_seo['page_title'];
	$meta_title = $global_seo['meta_title'];
	$meta_keyword = $global_seo['meta_keyword'];
	$meta_desc = $global_seo['meta_desc'];
	$share_img = $global_seo['share_img'];
	$actual_link = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}
	

?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title><?php echo $title; ?></title>
<meta name="description" content="<?php echo $meta_desc; ?>" />
<meta name="keywords" content="<?php echo $meta_keyword; ?>" />
                        
<!-- Facebook+ -->  
<meta property="og:title" content="<?php echo $meta_title; ?>">
<meta property="og:image" content="<?php echo base_url().'uploads/'.$share_img; ?>">
<meta property="og:description" content="<?php echo $meta_desc; ?>">
<meta property="og:url" content="<?php echo $actual_link; ?>">
<meta property="og:type" content="website" /> 

 
<link rel="shortcut icon" href="/assets/img/skin/favicon.png" type="image/x-icon"/>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link type="text/css"  rel="stylesheet" href="/assets/css/bundle.min.css">
<?php if(isset($css) && $css != "" ){ ?>
<link type="text/css" rel="stylesheet" href="/assets/css/<?php echo $css; ?>.min.css" />
<?php } ?>

<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/mapbox-gl.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-34289891-14"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-34289891-14');
</script>
<?php if( isset( $rows ) ){ ?>
<?php //print_r( $rows ); ?>
<style>
	<?php foreach( $rows as $row ){
		foreach( $row['columns'] as $column ){
			foreach( $column['sections'] as $section ){
				if( $section['custom_css'] != '' ){ 
					echo trim($section['custom_css']," ");
				}
			}
		}
	} ?>
</style>
<?php } ?>
</head>

<body class="<?php echo $body_class; ?>">
*/ ?>