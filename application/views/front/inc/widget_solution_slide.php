<?php if( sizeof($solution_slide_list) > 0 ) { ?>
<div class="sec-solutionSlide">
    <img class="ele-fluid-right abs" src="/assets/img/element/ele-fluid-right.png" alt="">   
    <div class="box-solutionSlide">
        <div class="genSlide" data-perview-xl="5.5" data-perview-d="4.5" data-perview-t="3.5" data-perview-m="1.2" data-space-d="10" data-space-t="10" data-space-m="20" >
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php 
                    // $img_name = ['solution-thumb-ver-network.jpg','solution-thumb-ver-voice.jpg','solution-thumb-ver-mobility.jpg','solution-thumb-ver-iot.jpg','solution-thumb-ver-marketing.jpg','solution-thumb-ver-productivity.jpg','solution-thumb-ver-5g-infra.jpg','solution-thumb-ver-cyber-security.jpg','solution-thumb-ver-data.jpg'];
                    // $title_name = ['Network','Voice & Conferencing','Mobility','5G Infrastructure','IOT and Digital','Marketing','Productivity & Collaboration','Data Center and Cloud','Cyber Security'];
                    foreach ($solution_slide_list as $key => $list) {
                    ?>
                    <div class="swiper-slide">
                        <div class="box-thumb box-shadow fw position-relative text-left">
                            <a href="<?php echo base_url().$lang_path.'/solution/'.$list['slug']; ?>">
                            <div class="thumb thumb-incard">
                                <div class="thumb-group">
                                    <div class="thumb-img"><img src="<?php echo $list['thumb_image_verticals'] != ''  ? upload_path($list['thumb_image_verticals']) : '' ; ?>" class="w-100 float-left"></div>
                                    <div class="thumb-txt text-left">
                                        <div class="thumb-txt-h txt-upper t-black"><?php echo $list['title']; ?></div>
                                        <!-- <div class="thumb-txt-p txt-upper t-light">Solution</div> -->
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>    
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- <div class="swiper-pagination"></div> -->
            <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></div>
            <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/arrow-left-1.svg" class="svg"></div>
        </div>
    </div>
</div>
<?php } ?>