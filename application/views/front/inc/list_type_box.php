<?php if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' || $section['design_template'] == 'table' ){ ?>
    <?php // print_r($section); ?>         
    <?php if($section['tab_show'] == 'default') { ?>
            <?php
                $ischeck_highlight = 0;
                $all_cate_code = '';
                foreach( $section['categories'] as $cate_key => $category){  
                    if ( $all_cate_code == '' ) {
                        $all_cate_code .= $category['code'];
                    } else {
                        $all_cate_code .= ',' . $category['code'];
                    }
                }
            ?>
            <div class="cont-contain ml-0 mr-0 w-100">
                <?php include('inc_topic.php') ?>
                <?php //echo 'mixer';print_r($section); ?>
                <?php   if(sizeof($section['categories']) > 1){ ?>
                    <!-- ////////////////////// -->
                    <?php //print_r($section); ?>
                    <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                        <?php  // if(sizeof($section['categories']) > 1){ ?>
                            <!-- ////////////////////// -->
                            <div class="sec-lists append-content-list-<?php echo $section['code'] ?>">
                                <div class="nav-link-<?php echo $cate_key ?>">
                                <?php if(sizeof($section['categories']) > 2 && $cate_key > 0){ ?>
                                    <?php if( $category['title'] != '' || $category['sub_title'] != ''){ ?>
                                        <div class="sec-list-cate">
                                            <div class="box-content <?php echo getSecPositionHoz( $section['cate_title_alignment'] )?>">
                                                <?php if($category['title'] != ''){ ?>
                                                    <h4 class="font-weight-bold"><?php echo  $category['title'] ?></h4>
                                                <?php } ?>
                                                <?php if($category['sub_title'] != ''){ ?>
                                                    <p class=""><?php echo  $category['sub_title'] ?></p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                <?php } ?>
                                    <div class="row row_cols <?php echo $section['design_template'] == 'table' ? 'row-eq-height' : '' ?>" data-row-cols-desktop="<?php echo $section['column_per_view'] ?>" data-row-cols-check="<?php echo $section['design_template'] ?>" 
                                        data-row-cols-mobile="<?php echo $section['display_per_column'] ?>">
                                        <?php if($cate_key > 0){ ?>
                                            <?php 
                                                $check_hl_layout = $category['listings']; 
                                                foreach($category['listings'] as $list_key => $listing){  
                                            ?>
                                                <?php if($list_key == 0){?>
                                                    <?php if($section['hl_set'] == 'hl_set_show'){ ?>
                                                        <?php if($list_key == 0){?>
                                                            <div class="col-12">
                                                                <div class="box-thumb-hl">
                                                                    <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative">
                                                                        <div class="thumb list-highligh">
                                                                            <div class="thumb-group">
                                                                                <div class="thumb-img">
                                                                                    <?php if($listing['link_type'] != 'no'){?>
                                                                                        <?php if($listing['btn_show'] == 0){?>
                                                                                            <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                                    <img  src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : '/assets/img/template/image-default.jpg'; ?>" class="w-100 float-left">
                                                                                    <?php if($listing['link_type'] != 'no'){?>
                                                                                    <?php if($listing['btn_show'] == 0){?>
                                                                                            </a>                                                        
                                                                                    <?php } ?>
                                                                                    <?php } ?>
                                                                                </div>
                                                                                <div class="thumb-txt">
                                                                                    <?php if($listing['title'] != ''){ ?>
                                                                                    <div class="thumb-txt-h t-bold">
                                                                                        <span><?php echo $listing['title'] ?><br><?php if($listing['sub_title'] != ''){ ?> <span><?php echo $listing['sub_title'] ?></span><?php } ?></span>
                                                                                    </div>
                                                                                    <?php } ?>
                                                                                    <?php if($listing['price'] != ''){ ?>
                                                                                        <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                                    <?php } ?>
                                                                                    <?php if( $section['is_tags'] != 0){ ?>
                                                                                        <div class="thumb-tag">
                                                                                            <?php if( $listing['cate_title'] != ""){ ?>
                                                                                            <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                                            <?php } ?>
                                                                                                <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                                                <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                                                    <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                                                        <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                                            <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                                        <?php } ?>
                                                                                                    <?php } ?>
                                                                                                <?php } ?>
                                                                                        </div>
                                                                                    <?php } ?>


                                                                                    <?php if($listing['description'] != ''){ ?>
                                                                                    <div class="thumb-txt-p">
                                                                                        <?php echo $listing['description'] ?></div>
                                                                                    <?php } ?>
                                                                                    <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($listing['published_date'] , $listing['lang']); ?></span></div>
                                                                                    <?php if($listing['btn_show'] == 0 || $listing['btn_show'] == 1 ){?>
                                                                                        <div class="btn-hl">
                                                                                            <a class="btn btn-text"
                                                                                                <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?>
                                                                                                href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != "" ? $listing['btn_text'] : 'อ่านรายละเอียด' ?></a>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php }else if($section['hl_set'] == 'hl_set_show2'){ ?>
                                                        <?php if($list_key == 0){?>
                                                            <div class="col col-12 col-md-7">
                                                                <div class="list-highlight2-main">
                                                                    <div class="box-thumb fw position-relative">
                                                                        <div class="thumb thumb-incard thumb-hl2">
                                                                            <div class="thumb-group">
                                                                            <?php if($listing['link_type'] != 'no'){?>
                                                                                <?php if($listing['btn_show'] == 0){?>
                                                                                    <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                                <div class="thumb-img"><img  src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : '/assets/img/template/image-default.jpg'; ?>" class="w-100 float-left"></div>
                                                                                <?php if($listing['link_type'] != 'no'){?>
                                                                                <?php if($listing['btn_show'] == 0){?>
                                                                                        </a>                                                        
                                                                                <?php } ?>
                                                                                <?php } ?>
                                                                                <div class="thumb-txt">
                                                                                    <?php if($listing['title'] != ''){ ?>
                                                                                    <div class="thumb-txt-h"><span><?php echo $listing['title'] ?><br><?php if($listing['sub_title'] != ''){ ?> <span><?php echo $listing['sub_title'] ?></span><?php } ?></span></div>
                                                                                    <?php } ?>

                                                                                    <?php if($listing['price'] != ''){ ?><div class="thumb-txt-price"><?php echo $listing['price'] ?></div><?php } ?>
                                                                                    <?php if( $section['is_tags'] != 0){ ?>
                                                                                        <div class="thumb-tag">
                                                                                            <?php if( $listing['cate_title'] != ""){ ?>
                                                                                            <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                                            <?php } ?>
                                                                                            <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                                            <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                                                <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                                                    <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                                        <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                                    <?php } ?>
                                                                                                <?php } ?>
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                    <?php } ?>

                                                                                    <?php if($listing['description'] != ''){ ?>
                                                                                    <div class="thumb-txt-p">
                                                                                        <?php echo $listing['description'] ?></div>
                                                                                    <?php } ?>
                                                                                    <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($listing['published_date'] , $listing['lang']); ?></span></div>
                                                                                    <?php /* if($listing['btn_show'] == 0 || $listing['btn_show'] == 1 ){?>
                                                                                        <div class="btn-hl">
                                                                                            <a class="btn btn-text" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?>  href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != "" ? $listing['btn_text'] : 'อ่านรายละเอียด' ?></a>
                                                                                        </div>
                                                                                    <?php } */ ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php  } ?>
                                                <?php  } ?>
                                                <?php // foreach( $category['listings'] as $list_key => $listing){ ?>
                                                <?php //if($section['tab_setting'] == 'only_has'){ ?>
                                                <?php if($section['hl_set'] == 'hl_set_show'){ ?>
                                                    <?php  if($list_key != 0){ ?>
                                                        <div class="col">
                                                            <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?> fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                                    <?php if($listing['btn_show'] == 0){?>
                                                                        <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                    <?php if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){?>
                                                                        <div
                                                                            class="thumb thumb-<?php echo designTemplate( $section['design_template'] )?>">
                                                                            <div class="thumb-group">
                                                                                
                                                                                <?php if($listing['image'] != ""){ ?>
                                                                                <div class="thumb-img"><img
                                                                                        src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : ''; ?>"
                                                                                        class="w-100 float-left"></div>
                                                                                <?php } ?>
                                                                                <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                    <?php if($listing['title'] != ''){ ?>
                                                                                    <div class="thumb-txt-h"><span><?php echo $listing['title'] ?></span></div>
                                                                                    <?php } ?>
                                                                                    <?php if($listing['sub_title'] != ''){ ?>
                                                                                    <div class="thumb-txt-p"><?php echo $listing['sub_title'] ?>
                                                                                    </div>
                                                                                    <?php } ?>
                                                                                    <?php if($listing['price'] != ''){ ?>
                                                                                    <div class="thumb-txt-price"><?php echo $listing['price'] ?>
                                                                                    </div>
                                                                                    <?php } ?>
                                                                                    <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($listing['published_date'] , $listing['lang']); ?></span></div>
                                                                                    <?php if( $section['design_template'] != "vertical"){ ?>
                                                                                        <?php if( $section['link_type'] != "no"){ ?>
                                                                                            <?php if($listing['btn_show'] == 1) { ?>
                                                                                                <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                                    <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                                                </div>
                                                                                            <?php } ?>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php }else if($section['design_template'] == 'table' ){ ?>
                                                                        <div class="thumb">
                                                                            <div class="thumb-group">
                                                                                <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                    <?php echo $listing['content'] ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                                <?php if($listing['btn_show'] == 0){?>
                                                                    </a>
                                                                <?php } ?>
                                                                <?php } ?>
                                                                <?php if(isset($section['is_tags'])){ ?>
                                                                    <?php if($section['is_tags'] == 1){ ?>
                                                                        <div class="thumb-tag">
                                                                            <?php if( $listing['cate_title'] != ""){ ?>
                                                                                <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                            <?php } ?>
                                                                            <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                            <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                                <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                                    <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                        <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                <?php if( $section['design_template'] == "vertical"){ ?>
                                                                    <?php if( $section['link_type'] != "no"){ ?>
                                                                        <?php if($listing['btn_show'] == 1) { ?>
                                                                            <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                            </div>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php  } ?>
                                                <?php }else if($section['hl_set'] == 'hl_set_show2'){ ?>
                                                    <?php if($ischeck_highlight < 1) {?>
                                                    <div class="col col-12 col-md-5">
                                                    <div class="row list-highlight2-sidebar">
                                                    <?php foreach( $check_hl_layout as $key => $check_hl ){ //print_r($check_hl); ?>
                                                        <?php  if($key != 0){ ?>
                                                        <div class="col-12">
                                                            <div class="box-thumb fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                <?php if($check_hl['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                                    <?php if($check_hl['btn_show'] == 0){?>
                                                                        <a <?php echo $check_hl['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $check_hl['link_type'],$check_hl['link_out'],$check_hl['cate_slug'],$check_hl['slug'],$page_slug   )?>">
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                    
                                                                    <div class="thumb thumb-hoz">
                                                                        <div class="thumb-group">
                                                                            <?php if($check_hl['image'] != ""){ ?>
                                                                            <div class="thumb-img"><img
                                                                                    src="<?php echo  $check_hl['image']  != ''  ? upload_path($check_hl['image']) : ''; ?>"
                                                                                    class="w-100 float-left"></div>
                                                                            <?php } ?>
                                                                            <div class="thumb-txt text-left">
                                                                                <?php if($check_hl['title'] != ''){ ?>
                                                                                <div class="thumb-txt-h"><span><?php echo $check_hl['title'] ?></span></div>
                                                                                <?php } ?>
                                                                                <?php if($check_hl['sub_title'] != ''){ ?>
                                                                                <div class="thumb-txt-p"><?php echo $check_hl['sub_title'] ?>
                                                                                </div>
                                                                                <?php } ?>
                                                                                <?php if($check_hl['price'] != ''){ ?>
                                                                                <div class="thumb-txt-price"><?php echo $check_hl['price'] ?>
                                                                                </div>
                                                                                <?php } ?>
                                                                                <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($listing['published_date'] , $listing['lang']); ?></span></div>
                                                                                <?php if( $section['design_template'] != "vertical"){ ?>
                                                                                    <?php if( $section['link_type'] != "no"){ ?>
                                                                                        <?php if($check_hl['btn_show'] == 1) { ?>
                                                                                            <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                                <a class="btn btn-sm-incard" <?php echo $check_hl['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $check_hl['link_type'],$check_hl['link_out'],$check_hl['cate_slug'],$check_hl['slug'],$page_slug   )?>"><?php echo $check_hl['btn_text'] != " " ? $check_hl['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                                            </div>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                <?php if($check_hl['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                                <?php if($check_hl['btn_show'] == 0){?>
                                                                    </a>
                                                                <?php } ?>
                                                                <?php } ?>
                                                                <?php if(isset($section['is_tags'])){ ?>
                                                                    <?php if($section['is_tags'] == 1){ ?>
                                                                        <div class="thumb-tag">
                                                                            <?php if( $check_hl['cate_title'] != ""){ ?>
                                                                                <a class="tag-cate" href="javascript:void(0);"><?php echo  $check_hl['cate_title'] ?></a>
                                                                            <?php } ?>
                                                                            <?php $ar_tag = explode( ",", $check_hl['tags'] ); ?>
                                                                            <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                                <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                                    <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                        <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                <?php if( $section['design_template'] == "vertical"){ ?>
                                                                    <?php if( $section['link_type'] != "no"){ ?>
                                                                        <?php if($check_hl['btn_show'] == 1) { ?>
                                                                            <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                <a class="btn btn-sm-incard" <?php echo $check_hl['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $check_hl['link_type'],$check_hl['link_out'],$check_hl['cate_slug'],$check_hl['slug'],$page_slug   )?>"><?php echo $check_hl['btn_text'] != " " ? $check_hl['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                            </div>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    </div>
                                                    </div>
                                                    <?php } ?>
                                                    <?php $ischeck_highlight++; ?>

                                                <?php }else if($section['hl_set'] == 'hl_set_none'){ ?>
                                                    <?php if($list_key >= 0){ ?>
                                                        <div class="col">
                                                            <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?> fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                                    <?php if($listing['btn_show'] == 0){?>
                                                                        <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                    <?php if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){?>
                                                                        <div
                                                                            class="thumb thumb-<?php echo designTemplate( $section['design_template'] )?>">
                                                                            <div class="thumb-group">
                                                                                
                                                                                <?php if($listing['image'] != ""){ ?>
                                                                                <div class="thumb-img"><img
                                                                                        src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : ''; ?>"
                                                                                        class="w-100 float-left"></div>
                                                                                <?php } ?>
                                                                                <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                    <?php if($listing['title'] != ''){ ?>
                                                                                    <div class="thumb-txt-h"><span><?php echo $listing['title'] ?></span></div>
                                                                                    <?php } ?>
                                                                                    <?php if($listing['sub_title'] != ''){ ?>
                                                                                    <div class="thumb-txt-p"><?php echo $listing['sub_title'] ?>
                                                                                    </div>
                                                                                    <?php } ?>
                                                                                    <?php if($listing['price'] != ''){ ?>
                                                                                    <div class="thumb-txt-price"><?php echo $listing['price'] ?>
                                                                                    </div>
                                                                                    <?php } ?>
                                                                                    <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($listing['published_date'] , $listing['lang']); ?></span></div>
                                                                                    <?php if( $section['design_template'] != "vertical"){ ?>
                                                                                        <?php if( $section['link_type'] != "no"){ ?>
                                                                                            <?php if($listing['btn_show'] == 1) { ?>
                                                                                                <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                                    <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                                                </div>
                                                                                            <?php } ?>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php }else if($section['design_template'] == 'table' ){ ?>
                                                                        <div class="thumb">
                                                                            <div class="thumb-group">
                                                                                <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                    <?php echo $listing['content'] ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                                <?php if($listing['btn_show'] == 0){?>
                                                                    </a>
                                                                <?php } ?>
                                                                <?php } ?>
                                                                <?php if(isset($section['is_tags'])){ ?>
                                                                    <?php if($section['is_tags'] == 1){ ?>
                                                                        <div class="thumb-tag">
                                                                            <?php if( $listing['cate_title'] != ""){ ?>
                                                                                <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                            <?php } ?>
                                                                            <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                            <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                                <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                                    <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                        <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                <?php if( $section['design_template'] == "vertical"){ ?>
                                                                    <?php if( $section['link_type'] != "no"){ ?>
                                                                        <?php if($listing['btn_show'] == 1) { ?>
                                                                            <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                            </div>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!-- /////////// btn more /////////// -->
                                <?php if($section['tab_setting'] == 'all_content'){ ?>
                                    
                                        <?php if($section['btn_more'] == 'btn_more_none'){ ?>
                                                    
                                        <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                                            <?php if($section['link_type'] == 'no'){ ?>
                                                
                                            <?php }else if($section['link_type'] == 'external_link'){  ?>
                                                <div class="box-content text-center float-left w-100  position-relative"  style="z-index:10;">
                                                    <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                                </div>
                                            <?php }else if($section['link_type'] == 'internal_link'){  ?>
                                                <div class="box-content text-center float-left w-100  position-relative"  style="z-index:10;">
                                                    <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                                        <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                                </div>
                                            <?php } ?>

                                        <?php }else if( $section['btn_more'] == 'btn_load_more'){  ?> 
                                            <div class="box-content text-center float-left w-100 mt-5 position-relative box-loadmore"
                                            style="z-index:10;">
                                            <a href="javascript:void(0);" class="btn btn-skew btn-loadmore"  onclick="loadMore($(this))"
                                                data-list="content" data-p="1" data-limit-page="<?php echo $section['limit_page']; ?>"
                                                data-sec-code="<?php echo $section['code'] ?>"
                                                data-limit="<?php echo $section['list_limit'] ?>"
                                                data-cate-code='<?php echo $cate_key ==  0 ? $all_cate_code:  $category['code']  ?>'
                                                data-cate-key='<?php echo $cate_key ?>'><?php echo $section['btn_text'] == '' ? 'Load more' : $section['btn_text'] ?></span></a>
                                            </div>
                                        <?php } ?>
                                
                                <?php }else if($section['tab_setting'] == 'only_has'){ ?>
                                    <?php if($cate_key > 0){ ?>
                                        <?php if($section['btn_more'] == 'btn_more_none'){ ?>
                                                    
                                        <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                                            <?php if($section['link_type'] == 'no'){ ?>
                                                
                                            <?php }else if($section['link_type'] == 'external_link'){  ?>
                                                <div class="box-content text-center float-left w-100  position-relative  btn-list-more"  style="z-index:10;">
                                                    <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-loadmore" ><span><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                                </div>
                                            <?php }else if($section['link_type'] == 'internal_link'){  ?>
                                                <div class="box-content text-center float-left w-100  position-relative btn-list-more"  style="z-index:10;">
                                                    <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                                        <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-loadmore" ><span><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                                </div>
                                            <?php } ?>

                                        <?php }else if( $section['btn_more'] == 'btn_load_more'){  ?> 
                                            <div class="box-content text-center float-left w-100 my-5 position-relative btn-list-more box-loadmore"
                                            style="z-index:10;">
                                            <a href="javascript:void(0);" class="btn btn-loadmore"  onclick="loadMore($(this))" 
                                                data-list="content" data-p="1" data-limit-page="<?php echo $section['limit_page']; ?>"
                                                data-sec-code="<?php echo $section['code'] ?>"
                                                data-limit="<?php echo $section['list_limit'] ?>"
                                                data-cate-code='<?php echo $cate_key ==  0 ? $all_cate_code :  $category['code']   ?>'
                                                data-cate-key='<?php echo $cate_key ?>'><span><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?> </span></a>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                                <!-- ////////////////////// -->
                            </div>
                    <!-- ////////////////////// -->
                        <?php // } ?>
                    <?php } ?>
                    <!-- end cate -->
                    <?php // echo $section['content'] ?>
                <?php } ?>
            </div>
            <?php }else if($section['tab_show'] == 'tabs'){ ?>
            <?php
            $all_cate_code = '';
            foreach( $section['categories'] as $cate_key => $category){  
                if ( $all_cate_code == '' ) {
                    $all_cate_code .= $category['code'];
                } else {
                    $all_cate_code .= ',' . $category['code'];
                }
            }
            ?>
            
            <div class="cont-contain ml-0 mr-0 w-100">
                <?php include('inc_topic.php') ?>
                <?php if(sizeof($section['categories']) > 1){ ?>
                <div class="box-tab tab-style-1 fw">
                    <div class="tab-nav  <?php echo getSecPositionHoz( $section['sec_title_alignment'] )?>">
                        <ul>
                            <?php if($section['tab_setting'] == 'all_content'){  ?>

                                <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                                    <?php if($cate_key == 0){ ?>
                                    <li class="tab-nav-list"><a href="javascript:void(0)" data-tab="<?php echo $cate_key ?>"
                                            class="tab-button box-content">All</a></li>
                                    <?php }else{ ?>
                                    <li class="tab-nav-list"><a href="javascript:void(0)"
                                            data-tab="<?php echo $cate_key; ?>"
                                            class="tab-button box-content"><?php echo  $category['title'] ?></a></li>
                                    <?php } ?>
                                <?php } ?>

                            <?php }else if($section['tab_setting'] == 'only_has'){ ?>
                                <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                                    <?php if($cate_key != 0){ ?>
                                    <li class="tab-nav-list"><a href="javascript:void(0)"
                                            data-tab="<?php echo $cate_key; ?>"
                                            class="tab-button box-content"><?php echo  $category['title'] ?></a></li>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                    
                    
                    
                    <div class="sec-lists append-content-list-<?php echo $section['code'] ?>">
                        <div class="tab-content">
                            <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                            <div class="tab-content-list fw nav-link-<?php echo $cate_key ?> "
                                style="display:none;">
                                <?php if($cate_key != 0){ ?>
                                    <?php if( $category['sub_title'] != ''){ ?>
                                        <div class="sec-list-cate">
                                            <div class="box-content <?php echo getSecPositionHoz( $section['cate_title_alignment'] )?>">
                                                <?php if($category['sub_title'] != ''){ ?>
                                                    <h6 class=""><?php echo  $category['sub_title'] ?></h6>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                <?php } ?>
                                <div class="clearfix"></div>
                                <div class="row row_cols <?php echo $section['design_template'] == 'table' ? 'row-eq-height ' : '' ?>"
                                    data-row-cols-desktop="<?php echo $section['column_per_view'] ?>" data-row-cols-check="<?php echo $section['design_template'] ?>"
                                    data-row-cols-mobile="<?php echo $section['display_per_column'] ?>">
                                    <?php  foreach($category['listings'] as $list_key => $listing){  ?>
                                        <?php if($section['hl_set'] == 'hl_set_show'){ ?>
                                            <?php if($list_key == 0){?>
                                                <div class="col-12">
                                                    <div class="box-thumb-hl">
                                                        <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative ">
                                                            <div class="thumb  list-highligh ">
                                                                <div class="thumb-group">
                                                                <div class="thumb-img">
                                                                        <?php if($listing['link_type'] != 'no'){?>
                                                                            <?php if($listing['btn_show'] == 0){?>
                                                                                <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        <img  src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : '/assets/img/template/image-default.jpg'; ?>" class="w-100 float-left">
                                                                        <?php if($listing['link_type'] != 'no'){?>
                                                                        <?php if($listing['btn_show'] == 0){?>
                                                                                </a>                                                        
                                                                        <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="thumb-txt">
                                                                        <?php if($listing['title'] != ''){ ?>
                                                                        <div class="thumb-txt-h t-bold">
                                                                            <span><?php echo $listing['title'] ?><br><?php if($listing['sub_title'] != ''){ ?> <span><?php echo $listing['sub_title'] ?></span><?php } ?></span>
                                                                        </div>
                                                                        <?php } ?>
                                                                        <?php if($listing['price'] != ''){ ?>
                                                                            <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                        <?php } ?>
                                                                        <?php if( $section['is_tags'] != 0){ ?>
                                                                            <div class="thumb-tag">
                                                                                <?php if( $listing['cate_title'] != ""){ ?>
                                                                                <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                                <?php } ?>
                                                                                    <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                                    <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                                        <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                                            <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                                <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                            <?php } ?>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                            </div>
                                                                        <?php } ?>

                                                                        <div class="position-relative fw">
                                                                            <img class="ele-list-thumb-1 "
                                                                                src="/assets/img/skin/line-light.png" alt="">
                                                                        </div>

                                                                        <?php if($listing['description'] != ''){ ?>
                                                                        <div class="thumb-txt-p">
                                                                            <?php echo $listing['description'] ?></div>
                                                                        <?php } ?>
                                                                        <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($listing['published_date'] , $listing['lang']); ?></span></div>
                                                                        <?php if($listing['btn_show'] == 0 || $listing['btn_show'] == 1 ){?>
                                                                            <div class="btn-hl">
                                                                                <a class="btn btn-text"
                                                                                    <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?>
                                                                                    href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != "" ? $listing['btn_text'] : 'อ่านรายละเอียด' ?></a>
                                                                            </div>
                                                                        <?php } ?>

                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php  foreach($category['listings'] as $list_key => $listing){  ?>
                                        <?php if($section['hl_set'] == 'hl_set_show'){ ?>
                                            <?php if($list_key > 0){?>
                                                <!-- Content normal -->
                                                <div class="col">
                                                    <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                    <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                    <?php if($listing['btn_show'] == 0){?>
                                                        <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                    <?php } ?>
                                                    <?php } ?>
                                                        <?php if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){ ?>
                                                            <div
                                                                class="thumb thumb-<?php echo designTemplate( $section['design_template'] )?>">
                                                                <div class="thumb-group">
                                                                    <?php if($listing['image'] != ""){ ?>
                                                                    <div class="thumb-img">
                                                                        <img src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : ''; ?>"
                                                                            class="w-100 float-left">
                                                                    </div>
                                                                    <?php } ?>
                                                                    <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                        <?php if($listing['title'] != ''){ ?>
                                                                            <div class="thumb-txt-h"><?php echo $listing['title'] ?></div>
                                                                        <?php } ?>
                                                                        <?php if($listing['sub_title'] != ''){ ?>
                                                                            <div class="thumb-txt-p"><?php echo $listing['sub_title'] ?></div>
                                                                        <?php } ?>
                                                                        <?php if($listing['price'] != ''){ ?>
                                                                        <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                        <?php } ?>
                                                                        <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($listing['published_date'] , $listing['lang']); ?></span></div>
                                                                        <?php if( $section['design_template'] != "vertical"){ ?>
                                                                            <?php if( $section['link_type'] != "no"){ ?>
                                                                                <?php if($listing['btn_show'] == 1) { ?>
                                                                                    <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                        <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php }else if($section['design_template'] == 'table' ){ ?>
                                                            <div class="thumb">
                                                                <div class="thumb-group">
                                                                    <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                        <?php echo $listing['content'] ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                        <?php if($listing['btn_show'] == 0){?>
                                                            </a>
                                                        <?php } ?>
                                                        <?php } ?>
                                                        <?php if(isset($section['is_tags'])){ ?>
                                                            <?php if($section['is_tags'] == 1){ ?>
                                                                <div class="thumb-tag">
                                                                    <?php if( $listing['cate_title'] != ""){ ?>
                                                                        <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                    <?php } ?>
                                                                    <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                    <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                        <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                            <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        <?php if( $section['design_template'] == "vertical"){ ?>
                                                            <?php if( $section['link_type'] != "no"){ ?>
                                                                <?php if($listing['btn_show'] == 1) { ?>
                                                                    <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                        <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <!-- End Content normal -->
                                            <?php } ?>
                                        <?php }else { ?>
                                            <!-- Content normal -->
                                            <div class="col">
                                                    <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                    <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                        <?php if($listing['btn_show'] == 0){?>
                                                            <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                        <?php } ?>
                                                    <?php } ?>
                                                            <?php if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){ ?>
                                                                <div
                                                                    class="thumb thumb-<?php echo designTemplate( $section['design_template'] )?>">
                                                                    <div class="thumb-group">
                                                                        <?php if($listing['image'] != ""){ ?>
                                                                        <div class="thumb-img">
                                                                            <img src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : ''; ?>"
                                                                                class="w-100 float-left">
                                                                        </div>
                                                                        <?php } ?>
                                                                        <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                            <?php if($listing['title'] != ''){ ?>
                                                                                <div class="thumb-txt-h"><?php echo $listing['title'] ?></div>
                                                                            <?php } ?>
                                                                            <?php if($listing['sub_title'] != ''){ ?>
                                                                            <div class="thumb-txt-p">
                                                                                <?php echo $listing['sub_title'] ?></div>
                                                                            <?php } ?>
                                                                            <?php if($listing['price'] != ''){ ?>
                                                                            <div class="thumb-txt-price">
                                                                                <?php echo $listing['price'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($listing['published_date'] , $listing['lang']); ?></span></div>
                                                                            <?php if( $section['design_template'] != "vertical"){ ?>
                                                                                <?php if( $section['link_type'] != "no"){ ?>
                                                                                    <?php if($listing['btn_show'] == 1) { ?>
                                                                                        <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                            <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php }else if($section['design_template'] == 'table' ){ ?>
                                                                <div class="thumb">
                                                                    <div class="thumb-group">
                                                                        <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                            <?php echo $listing['content'] ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                            <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table'){?>
                                                            <?php if($listing['btn_show'] == 0){?>
                                                                </a>
                                                            <?php } ?>
                                                            <?php } ?>
                                                            <?php if(isset($section['is_tags'])){ ?>
                                                                <?php if($section['is_tags'] == 1){ ?>
                                                                    <div class="thumb-tag">
                                                                        <?php if( $listing['cate_title'] != ""){ ?>
                                                                            <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                        <?php } ?>
                                                                        <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                        <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                            <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                                <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                    <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <?php if( $section['design_template'] == "vertical"){ ?>
                                                                <?php if( $section['link_type'] != "no"){ ?>
                                                                    <?php if($listing['btn_show'] == 1) { ?>
                                                                        <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                            <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                    </div>
                                                </div>
                                                <!-- End Content normal -->
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php if($section['tab_setting'] == 'all_content'){ ?>
                                    
                                        <?php if($section['btn_more'] == 'btn_more_none'){ ?>
                                                    
                                        <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                                            <?php if($section['link_type'] == 'no'){ ?>
                                                
                                            <?php }else if($section['link_type'] == 'external_link'){  ?>
                                                <div class="box-content text-center float-left w-100  position-relative btn-list-more"  style="z-index:10;">
                                                    <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-loadmore" ><span><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                                </div>
                                            <?php }else if($section['link_type'] == 'internal_link'){  ?>
                                                <div class="box-content text-center float-left w-100  position-relative btn-list-more"  style="z-index:10;">
                                                    <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                                        <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-loadmore" ><span><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                                </div>
                                            <?php } ?>

                                        <?php }else if( $section['btn_more'] == 'btn_load_more'){  ?> 
                                            <div class="box-content text-center float-left w-100 my-5 position-relative btn-list-more box-loadmore"
                                            style="z-index:10;">
                                            <a href="javascript:void(0);" class="btn btn-loadmore"  onclick="loadMore($(this))"
                                                data-list="content" data-p="1" data-limit-page="<?php echo $section['limit_page']; ?>"
                                                data-sec-code="<?php echo $section['code'] ?>"
                                                data-limit="<?php echo $section['list_limit'] ?>"
                                                data-cate-code='<?php echo $cate_key ==  0 ? $all_cate_code :  $category['code']  ?>'
                                                data-cate-key='<?php echo $cate_key ?>'><span><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                            </div>
                                        <?php } ?>
                                    
                                <?php }else if($section['tab_setting'] == 'only_has'){ ?>
                                    <?php if($cate_key > 0){ ?>
                                        <?php if($section['btn_more'] == 'btn_more_none'){ ?>
                                                    
                                        <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                                            <?php if($section['link_type'] == 'no'){ ?>
                                                
                                            <?php }else if($section['link_type'] == 'external_link'){  ?>
                                                <div class="box-content text-center float-left w-100  position-relative btn-list-more"  style="z-index:10;">
                                                    <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-loadmore" ><span><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                                </div>
                                            <?php }else if($section['link_type'] == 'internal_link'){  ?>
                                                <div class="box-content text-center float-left w-100  position-relative btn-list-more"  style="z-index:10;">
                                                    <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                                        <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-loadmore" ><span><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                                </div>
                                            <?php } ?>

                                        <?php }else if( $section['btn_more'] == 'btn_load_more'){  ?> 
                                            <div class="box-content text-center float-left w-100 my-5 position-relative btn-list-more box-loadmore"
                                            style="z-index:10;">
                                            <a href="javascript:void(0);" class="btn btn-loadmore"  onclick="loadMore($(this))"
                                                data-list="content" data-p="1" data-limit-page="<?php echo $section['limit_page']; ?>"
                                                data-sec-code="<?php echo $section['code'] ?>"
                                                data-limit="<?php echo $section['list_limit'] ?>"
                                                data-cate-code='<?php echo $cate_key ==  0 ? $all_cate_code :  $category['code']  ?>'
                                                data-cate-key='<?php echo $cate_key ?>'><span><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>

                            </div>

                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div><!-- cont-contain ml-0 mr-0 w-100 tab -->
            <?php }else if($section['tab_show'] == 'merge'){ ?>
            <?php
            $all_cate_code = '';
            foreach( $section['categories'] as $cate_key => $category){  
                if ( $all_cate_code == '' ) {
                    $all_cate_code .= $category['code'];
                } else {
                    $all_cate_code .= ',' . $category['code'];
                }
            }
            ?>
            <div class="cont-contain ml-0 mr-0 w-100">
                <?php include('inc_topic.php') ?>
                <?php  if(sizeof($section['categories']) > 1){ ?>
                <!-- ////////////////////// -->
                <?php 
                foreach( $section['categories'] as $cate_key => $category){       
                ?>
                    <div class="clearfix"></div>
                    <!-- ////////////////////// -->
                    <div class="sec-lists append-content-list-<?php echo $section['code'] ?>">
                        <div class="nav-link-<?php echo $cate_key ?>">
                            <div class="row row_cols <?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?> " data-row-cols-desktop="<?php echo $section['column_per_view'] ?>" data-row-cols-check="<?php echo $section['design_template'] ?>"
                                data-row-cols-mobile="<?php echo $section['display_per_column'] ?>">
                                <?php  if($cate_key == 0){?>
                                        <?php  
                                        foreach($category['listings'] as $list_key => $listing){  
                                        ?>
                                        <?php if($section['hl_set'] == 'hl_set_show'){ ?>
                                            <?php if($list_key == 0){?>
                                                <div class="col-12">
                                                    <div class="box-thumb-hl">
                                                        <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative">
                                                            <div class="thumb list-highligh ">
                                                                <div class="thumb-group">
                                                                
                                                                    <div class="thumb-img">
                                                                        <?php if($listing['link_type'] != 'no'){?>
                                                                            <?php if($listing['btn_show'] == 0){?>
                                                                                <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        <img  src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : '/assets/img/template/image-default.jpg'; ?>" class="w-100 float-left">
                                                                        <?php if($listing['link_type'] != 'no'){?>
                                                                        <?php if($listing['btn_show'] == 0){?>
                                                                                </a>                                                        
                                                                        <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                    
                                                                    <div class="thumb-txt">
                                                                        <?php if($listing['title'] != ''){ ?>
                                                                        <div class="thumb-txt-h t-bold">
                                                                            <span><?php echo $listing['title'] ?><br><?php if($listing['sub_title'] != ''){ ?> <span><?php echo $listing['sub_title'] ?></span><?php } ?></span>
                                                                        </div>
                                                                        <?php } ?>
                                                                        <?php if($listing['price'] != ''){ ?>
                                                                            <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                        <?php } ?>
                                                                        <?php if( $section['is_tags'] != 0){ ?>
                                                                            <div class="thumb-tag">
                                                                                <?php if( $listing['cate_title'] != ""){ ?>
                                                                                <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                                <?php } ?>
                                                                                    <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                                    <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                                        <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                                            <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                                <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                                            <?php } ?>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                            </div>
                                                                        <?php } ?>

                                                                        <div class="position-relative fw">
                                                                            <img class="ele-list-thumb-1 "
                                                                                src="/assets/img/skin/line-light.png" alt="">
                                                                        </div>

                                                                        <?php if($listing['description'] != ''){ ?>
                                                                        <div class="thumb-txt-p">
                                                                            <?php echo $listing['description'] ?></div>
                                                                        <?php } ?>
                                                                        <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($listing['published_date'] , $listing['lang']); ?></span></div>
                                                                        <?php if($listing['btn_show'] == 0 || $listing['btn_show'] == 1 ){?>
                                                                            <div class="btn-hl">
                                                                                <a class="btn btn-text"
                                                                                    <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?>
                                                                                    href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != "" ? $listing['btn_text'] : 'อ่านรายละเอียด' ?></a>
                                                                            </div>
                                                                        <?php } ?>

                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php // } ?>
                                        <?php // } ?>
                                        <?php  //foreach($category['listings'] as $list_key => $listing){  ?>
                                        <?php  if($section['hl_set'] != 'hl_set_show'){ ?>
                                            <?php //if($list_key != 0){?>
                                                <div class="col">
                                                    <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                    <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                        <?php if($listing['btn_show'] == 0){?>
                                                            <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                        <?php } ?>
                                                        <?php } ?>
                                                            <?php  if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){ ?>
                                                                <div  class="thumb thumb-<?php echo designTemplate( $section['design_template'] )?>">
                                                                    <div class="thumb-group">
                                                                        <?php if($listing['image'] != ""){ ?>
                                                                        <div class="thumb-img"><img
                                                                                src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : ''; ?>"
                                                                                class="w-100 float-left"></div>
                                                                        <?php } ?>
                                                                        <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                            <?php if($listing['title'] != ''){ ?>
                                                                                <div class="thumb-txt-h"><span><?php echo $listing['title'] ?></span></div>
                                                                            <?php } ?>
                                                                            <?php if($listing['sub_title'] != ''){ ?>
                                                                                <div class="thumb-txt-p"><?php echo $listing['sub_title'] ?></div>
                                                                            <?php } ?>
                                                                            <?php if($listing['price'] != ''){ ?>
                                                                                <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($listing['published_date'] , $listing['lang']); ?></span></div>
                                                                            <?php if( $section['design_template'] != "vertical"){ ?>
                                                                                <?php if( $section['link_type'] != "no"){ ?>
                                                                                    <?php if($listing['btn_show'] == 1) { ?>
                                                                                        <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                            <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php }else if($section['design_template'] == 'table' ){ ?>
                                                                    <div class="thumb">
                                                                        <div class="thumb-group">
                                                                            <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                <?php echo $listing['content'] ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                        <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                        <?php if($listing['btn_show'] == 0){?>
                                                            </a>
                                                        <?php } ?>
                                                        <?php } ?>
                                                        <?php if(isset($section['is_tags'])){ ?>
                                                            <?php if($section['is_tags'] == 1){ ?>
                                                                <div class="thumb-tag">
                                                                    <?php if( $listing['cate_title'] != ""){ ?>
                                                                        <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                    <?php } ?>
                                                                    <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                    <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                        <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                            <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        <?php if( $section['design_template'] == "vertical"){ ?>
                                                            <?php if( $section['link_type'] != "no"){ ?>
                                                                <?php if($listing['btn_show'] == 1) { ?>
                                                                    <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                        <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <?php // } ?>
                                        <?php } ?>
                                        <?php  if($section['hl_set'] == 'hl_set_show'){ ?>
                                            <?php if($list_key != 0){?>
                                                <div class="col">
                                                    <div class="<?php echo $section['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                    <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                        <?php if($listing['btn_show'] == 0){?>
                                                            <a <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>">
                                                        <?php } ?>
                                                        <?php } ?>
                                                            <?php  if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){ ?>
                                                                <div  class="thumb thumb-<?php echo designTemplate( $section['design_template'] )?>">
                                                                    <div class="thumb-group">
                                                                        <?php if($listing['image'] != ""){ ?>
                                                                        <div class="thumb-img"><img
                                                                                src="<?php echo  $listing['image']  != ''  ? upload_path($listing['image']) : ''; ?>"
                                                                                class="w-100 float-left"></div>
                                                                        <?php } ?>
                                                                        <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                            <?php if($listing['title'] != ''){ ?>
                                                                                <div class="thumb-txt-h"><span><?php echo $listing['title'] ?></span></div>
                                                                            <?php } ?>
                                                                            <?php if($listing['sub_title'] != ''){ ?>
                                                                                <div class="thumb-txt-p"><?php echo $listing['sub_title'] ?></div>
                                                                            <?php } ?>
                                                                            <?php if($listing['price'] != ''){ ?>
                                                                                <div class="thumb-txt-price"><?php echo $listing['price'] ?></div>
                                                                            <?php } ?>
                                                                            <div class="date-post"><img src="/assets/img/skin/icon-date.svg" class="svg"><span><?php echo getCalendarDate_listing($listing['published_date'] , $listing['lang']); ?></span></div>
                                                                            <?php if( $section['design_template'] != "vertical"){ ?>
                                                                                <?php if( $section['link_type'] != "no"){ ?>
                                                                                    <?php if($listing['btn_show'] == 1) { ?>
                                                                                        <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                            <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php }else if($section['design_template'] == 'table' ){ ?>
                                                                    <div class="thumb">
                                                                        <div class="thumb-group">
                                                                            <div class="thumb-txt <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                                <?php echo $listing['content'] ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                        <?php if($listing['link_type'] != 'no' && $section['design_template'] != 'table' ){?>
                                                        <?php if($listing['btn_show'] == 0){?>
                                                            </a>
                                                        <?php } ?>
                                                        <?php } ?>
                                                        <?php if(isset($section['is_tags'])){ ?>
                                                            <?php if($section['is_tags'] == 1){ ?>
                                                                <div class="thumb-tag">
                                                                    <?php if( $listing['cate_title'] != ""){ ?>
                                                                        <a class="tag-cate" href="javascript:void(0);"><?php echo  $listing['cate_title'] ?></a>
                                                                    <?php } ?>
                                                                    <?php $ar_tag = explode( ",", $listing['tags'] ); ?>
                                                                    <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                                                        <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                                                            <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                                                                <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        <?php if( $section['design_template'] == "vertical"){ ?>
                                                            <?php if( $section['link_type'] != "no"){ ?>
                                                                <?php if($listing['btn_show'] == 1) { ?>
                                                                    <div class="box-content <?php echo getSecPositionHoz( $section['listing_alignment'] )?>">
                                                                        <a class="btn btn-sm-incard" <?php echo $listing['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $listing['link_type'],$listing['link_out'],$listing['cate_slug'],$listing['slug'],$page_slug   )?>"><?php echo $listing['btn_text'] != " " ? $listing['btn_text'] : 'ดูเพิ่มเติม' ?></a>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <?php  } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <!-- /////////// btn more /////////// -->
                
                    <?php if($cate_key == 0){ ?>
                        <?php if($section['btn_more'] == 'btn_more_none'){ ?> 
                        <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                            <?php if($section['link_type'] == 'no'){ ?>
                                
                            <?php }else if($section['link_type'] == 'external_link'){  ?>
                                <div class="box-content text-center float-left w-100  position-relative btn-list-more"  style="z-index:10;">
                                    <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                </div>
                            <?php }else if($section['link_type'] == 'internal_link'){  ?>
                                <div class="box-content text-center float-left w-100  position-relative btn-list-more"  style="z-index:10;">
                                    <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                        <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                </div>
                            <?php } ?>

                        <?php }else if( $section['btn_more'] == 'btn_load_more'){  ?> 
                            <div class="box-content text-center float-left w-100 my-5 position-relative btn-list-more box-loadmore"
                            style="z-index:10;">
                            <a href="javascript:void(0);" class="btn btn-skew btn-loadmore"  onclick="loadMore($(this))"
                                data-list="content" data-p="1" data-limit-page="<?php echo $section['limit_page']; ?>"
                                data-sec-code="<?php echo $section['code'] ?>"
                                data-limit="<?php echo $section['list_limit'] ?>"
                                data-cate-code='<?php echo $cate_key ==  0 ? $all_cate_code :  $category['code']  ?>'
                                data-cate-key='<?php echo $cate_key ?>'><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?> </span></a>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                
            <?php } ?>
                    <!-- ////////////////////// -->
                    
            </div>
        <?php } ?>
    <?php } ?>