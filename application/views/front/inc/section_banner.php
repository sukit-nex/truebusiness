<!--
    |*******************************************************************************************
    | SECTION BANNER SLIDE
    |*******************************************************************************************
    -->

<?php
    // $imagebg_d = array('immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg');
    //  $imagebg_m = array('immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg','immersive-banner-2.jpg');
    // $content_position = array('text-left bancont-top','text-left bancont-middle','text-left bancont-bottom','text-center bancont-top','text-center bancont-middle','text-center bancont-bottom','text-right bancont-top','text-right bancont-middle','text-right bancont-bottom');
?>
<div id="sec_id_<?php echo $section['code'] ?>" class="section cont mg st st-banner gen_margin <?php echo $section['custom_class'] != '' ?  customClass($section['custom_class']) : '' ?> "
    data-mg-desktop="<?php echo ar_decodeFront($section['mg_desktop'],'margin');?>"
    data-mg-mobile="<?php echo ar_decodeFront($section['mg_mobile'],'margin');?>">
    <div class=" bg-fullwidth fw  gen_bg <?php echo getBackgroundSettingClass($section['bg_position_ver'],$section['bg_position_hoz'],$section['bg_repeat'],$section['bg_fixed'], $section['bg_size']);?>
        <?php  if( $section['title'] != '' && $section['sub_title'] != '' || $section['section_layout'] != "fullwidth" ) {echo ' pd pb-0 '; } ?>   <?php echo $section['custom_class'] != ' ' ? $section['custom_class'] : ''; ?>"
        data-bg-desktop="<?php echo $section['bg1_desktop'];  ?>"
        data-bg-mobile="<?php echo $section['bg1_mobile'];  ?>" data-bg-color="<?php echo $section['bg_color']; ?>">

        <div class="container <?php echo is_fullwidth( $section['section_layout'] ); ?> ">

            <?php include('inc_graphic.php') ?>
            <?php include('inc_topic.php') ?>

            <div class="clearfix"></div>
            <div class="position-relative gen_padding "
                data-pd-desktop="<?php echo ar_decodeFront($section['pd_desktop'],'padding');?>"
                data-pd-mobile="<?php echo ar_decodeFront($section['pd_mobile'],'padding');?>">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php 
                        $is_img = 0;
                        foreach( $section['banner_listings'] as $banner_key => $banner_listing){ 
                            if($banner_listing['type'] == 'image'){ 
                                $is_img++; 
                            }
                        }
                        ?>

                        <?php $y=0;  ?>
                        <?php foreach( $section['banner_listings'] as $banner_key => $banner_listing){ ?>
                            <?php if($banner_listing['type'] == 'image'){ ?>

                                <?php if($section['gen_height'] == 0 || $section['section_layout'] == 'container'){ ?>

                                    <div class="swiper-slide bg-fullwidth ">
                                          <!-- url banner -->
                                          <?php   if($banner_listing['link_type'] == 'no'){ ?>
                                
                                            <?php }else if($banner_listing['link_type'] == 'external_link'){  ?>
                                                <a target="_blank" class="fw"  href="<?php echo $banner_listing['ex_link'] == '' ? 'javascript:void(0);' : $banner_listing['ex_link'] ?>">
                                            <?php }else if($banner_listing['link_type'] == 'internal_link'){  ?>
                                                    <?php  $uri = getSlug( $banner_listing['page_code'] , $pages );?>
                                                    <a class="fw" href="<?php echo $banner_listing['page_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>" >
                                            <?php }  ?>
                                        <div class="position-relative fw">
                                        
                                    
                                            <?php if( $banner_listing['image'] != ''){ ?>
                                                <div class="blank-png d-sm-block d-none"><img class="fw" src="<?php echo upload_path($banner_listing['image']) ?>"></div>
                                            <?php } ?>
                                            <?php if( $banner_listing['image_m'] != ''){ ?>
                                                <div class="blank-png d-sm-none"><img class="fw" src="<?php echo upload_path($banner_listing['image_m'])  ?>"></div>
                                            <?php } ?>

                                           
                                            <?php if($banner_listing['content'] != "" || $banner_listing['link_type'] == 'no'){ ?>
                                                <div class="cont-contain pd cont-abs <?php echo getContentPositionClass( $banner_listing['content_position_ver'], $banner_listing['content_position_hoz']); ?> <?php echo getContentPositionClassMobile( $banner_listing['content_position_ver_m'], $banner_listing['content_position_hoz_m']); ?>">
                                                    <div class="cont-txt"><?php echo $banner_listing['content']?></div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <?php   if($banner_listing['link_type'] == 'no'){ ?>

                                        <?php  }else if($banner_listing['link_type'] == 'external_link'){  ?>
                                            </a>
                                        <?php }else if($banner_listing['link_type'] == 'internal_link'){  ?>
                                            </a>
                                        <?php }  ?>
                                        <!-- url banner -->
                                    </div>

                                <?php }else{ ?>
                                    <div class="swiper-slide bg-fullwidth" style="background-image:url(<?php echo $section['section_layout'] == 'fullwidth' ? upload_path($banner_listing['image']) : ''; ?>);">
                                         <!-- url banner -->
                                         <?php   if($banner_listing['link_type'] == 'no'){ ?>
                                
                                        <?php }else if($banner_listing['link_type'] == 'external_link'){  ?>
                                            <a target="_blank" class="fw"  href="<?php echo $banner_listing['ex_link'] == '' ? 'javascript:void(0);' : $banner_listing['ex_link'] ?>">
                                        <?php }else if($banner_listing['link_type'] == 'internal_link'){  ?>
                                                <?php   $uri = getSlug( $banner_listing['page_code'] , $pages ); ?>
                                                <a class="fw " href="<?php echo $banner_listing['page_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>" >
                                        <?php }  ?>
                                        <div class="container container-1600 pd-m-0 p-0">
                                            <div class="position-relative fw">
                                                
                                                <div class="d-sm-block d-none <?php echo $section['gen_height'] == 0 ? '' : 'gen-height' ?>"
                                                    data-image-desktop="<?php echo $section['section_layout'] == 'fullwidth' ?  upload_path($banner_listing['image']) : '/assets/img/skin/blank.png' ?> "
                                                    data-image-mobile="<?php echo $section['section_layout'] == 'fullwidth' ?  upload_path($banner_listing['image_m']) : '' ?> "
                                                    data-column="<?php echo sizeof($row['columns'] ); ?>">
                                                </div>
                                                <?php if( $banner_listing['image_m'] != ''){ ?>
                                                    <div class="blank-png d-sm-none"><img class="fw" src="<?php echo upload_path($banner_listing['image_m'])  ?>"></div>
                                                <?php } ?>
                                                
                                                <?php if($banner_listing['content'] != "" || $banner_listing['link_type'] == 'no'){ ?>
                                                <div class="cont-contain pd cont-abs <?php echo getContentPositionClass( $banner_listing['content_position_ver'], $banner_listing['content_position_hoz']); ?>  <?php echo getContentPositionClassMobile( $banner_listing['content_position_ver_m'], $banner_listing['content_position_hoz_m']); ?>">
                                                    <div class="cont-txt"><?php echo $banner_listing['content']?></div>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php if($banner_listing['link_type'] == 'no'){ ?>

                                        <?php  }else if($banner_listing['link_type'] == 'external_link'){  ?>
                                            </a>
                                        <?php }else if($banner_listing['link_type'] == 'internal_link'){  ?>
                                            </a>
                                        <?php }  ?>
                                        <!-- url banner -->
                                    </div>
                                <?php } ?>
                                
                            <!-- <div class="swiper-slide bg-fullwidth "
                                style="background-image:url(<?php echo $section['section_layout'] == 'fullwidth' ? upload_path($banner_listing['image']) : ''; ?>);">
                                <div class="container container-1600 container-fullwidth pd-m-0 p-0  ">
                                    <div class="position-relative fw sec-ar-1-herobanner">
                                        <?php if($section['section_layout'] == 'container'){ ?>
                                        <div class="blank-png d-sm-block d-none ">
                                            <img class="fw" src="<?php echo  $section['section_layout'] == 'container'  ? upload_path($banner_listing['image']) : ''; ?>" alt="">
                                        </div>
                                        <?php }else{ ?>
                                        <div class="d-sm-block d-none <?php echo $section['gen_height'] == 0 ? '' : 'gen-height' ?>"
                                            data-image-desktop="<?php echo $section['section_layout'] == 'fullwidth' ?  upload_path($banner_listing['image']) : '/assets/img/skin/blank.png' ?> "
                                            data-image-mobile="<?php echo $section['section_layout'] == 'fullwidth' ?  upload_path($banner_listing['image_m']) : '' ?> "
                                            data-column="<?php echo sizeof($row['columns'] ); ?>">
                                                <?php if($section['gen_height'] == 0){ ?>
                                                    <img class="fw" src="<?php echo  upload_path($banner_listing['image'])  ?>">
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                        <?php if( $banner_listing['image_m'] != ''){ ?>
                                        <div class="blank-png d-sm-none"><img class="fw" src="<?php echo  upload_path($banner_listing['image_m'])  ?>"></div>
                                        <?php } ?>
                                        <div class="cont-contain pd cont-abs <?php echo getContentPositionClass( $banner_listing['content_position_ver'], $banner_listing['content_position_hoz']); ?>  <?php echo getContentPositionClassMobile( $banner_listing['content_position_ver_m'], $banner_listing['content_position_hoz_m']); ?>">
                                            <div class="cont-txt">
                                                <?php echo $banner_listing['content']?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <?php }elseif( $banner_listing['type'] == 'video'){ ?>
                                <?php if($banner_listing['video_type'] == 'youtube'){ ?>
                                    <div class="swiper-slide bg-fullwidth" style="background-image:url(<?php echo $section['section_layout'] == 'fullwidth' ? upload_path($banner_listing['image']) : ''; ?>);">
                                        <div class="container <?php echo is_fullwidth( $section['section_layout'] ); ?> pd-m-0 p-0">
                                            <div class="box-video <?php if( ( $section['gen_height'] == 1 && $section['section_layout'] != 'container' )  && $is_img > 0){ echo 'video_abs'; }?>">
                                                <div class="ganeral-video">
                                                    <div class="video-container inactive-state <?php echo $banner_listing['video'] != "" ?  'click-to-play-video':''  ?>" data-video="<?php echo $banner_listing['video'] ?>" data-code="<?php echo $banner_listing['id']?>" section_type="<?php echo $section['section_type'] ?>"   <?php echo $banner_listing['video'] != "" ?  'onclick="playVideo($(this))" ':''  ?>>
                                                        <div class="video-put">
                                                            <div id="player_ganeral_<?php echo $banner_listing['id'] ?>_<?php echo $section['section_type'] ?>" class="player_ganeral"></div>
                                                        </div>
                                                        <div class="video-space d-flex h-100">
                                                            <?php if($banner_listing['image'] != "" ){ ?>
                                                                <img class="float-left w-100 d-none d-md-block"  style="object-fit: cover;" src="<?php echo isset($banner_listing['image']) ? upload_path($banner_listing['image']) : '' ?>">
                                                            <?php } ?>
                                                           
                                                                <img class="float-left w-100 d-block d-md-none" style="object-fit: cover;"  src="<?php echo $banner_listing['image_m'] != "" ? upload_path($banner_listing['image_m']) : upload_path($banner_listing['image']) ?>">
                                                             
                                                        </div>
                                                        <?php if( $banner_listing['video'] != '' ){ ?>
                                                            <div class="videoplay-icon"><img class="" src="/assets/img/xr/button-play.png"></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                <?php }elseif($banner_listing['video_type'] == 'loop'){ ?>
                                    <div class="swiper-slide bg-fullwidth">
                                        <div class="container container-fluid pd-m-0 p-0">
                                            <div class="box-video <?php if( ( $section['gen_height'] == 1 && $section['section_layout'] != 'container' )  && $is_img > 0){ echo 'video_abs'; }?>">
                                                <video class="video-container" muted autoplay loop controlsList="nodownload" oncontextmenu="return false;">
                                                    <source src="<?php echo isset( $banner_listing['video_file'] ) ? upload_path($banner_listing['video_file']) : ''; ?>" type="video/mp4">
                                                </video>
                                                <div class="cont-contain cont-abs <?php echo getContentPositionClass( $banner_listing['content_position_ver'], $banner_listing['content_position_hoz']); ?>  <?php echo getContentPositionClassMobile( $banner_listing['content_position_ver_m'], $banner_listing['content_position_hoz_m']); ?>">
                                                    <div class="cont-txt">
                                                        <?php echo $banner_listing['content']?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php }  ?>
                        <?php $y++; ?>

                        <?php } ?>
                    </div>
                    <?php if( $y > 1 ){ ?>
                        <div class="swiper-pagination"></div>
                    <?php } ?>
                </div>
                <?php if( $y > 1 ){ ?>
                    <!-- <div class="swiper-pagination"></div> -->
                <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/icon-arrow2-right.png"></div>
                <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/icon-arrow2-left.png"></div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>