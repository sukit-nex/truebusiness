<?php if( isset($page_footer) ){ ?>
    <?php if( sizeof($page_footer) > 0 ){ ?>
        <?php if($page_footer['onoff'] == 1){ ?>
        <div class="section-prefooter bg-fullwidth" style="background-image: url(<?php echo upload_path($page_footer['image']);  ?>);">
            <div class="container container-1600">
                <div class="blank-png" style="opacity:0"><img src="/assets/img/xr/blank-prefooter.png"></div>
            </div>
        </div>
        <?php if($page_footer['contact_status'] == 1){ ?>
            <div class="section-contact">
                <div class="container container-1600 text-center  ">
                    <div class="contact-set">
                        <h3 class="t-black  text-uppercase">Contact US</h3>
                        <div class="vertical-line "></div>
                        <div class="logo-contact contact-first">
                            <a href="<?php echo $page_footer['fb_link'] ?>" target="_blank">
                                <img src="/assets/img/xr/f_logo.png" alt="">
                                <h4 class="t-black t-gray"><?php echo $page_footer['fb_title'] ?>
                                    <br><span class="t-medium"><?php echo $page_footer['fb_sub_title'] ?></span>
                                </h4>
                            </a>
                        </div>
                        <div class="vertical-line"></div>
                        <div class="logo-contact"><a href="<?php echo $page_footer['yt_link'] ?>"
                                target="_blank"><img src="/assets/img/xr/yt_logo.png" alt="">
                                <h4 class="t-black t-gray"><?php echo $page_footer['yt_title'] ?>
                                <br>
                                <?php if($page_footer['yt_sub_title'] != ''){ ?>
                                    <span class="t-medium"><?php echo $page_footer['yt_sub_title'] ?></span>
                                <?php } ?>
                                </h4>
                            </a></div>
                        <div class="vertical-line d-none d-md-block"></div>
                        <div class="logo-studio-contact d-none d-md-block"><img src="<?php echo upload_path($page_footer['logo_image']);  ?>">
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php } ?>
    <?php } ?>
<?php } ?>


<div class="section-footer">
    <div class="container container-1360 text-center footer-order-m ">
        <a id="pri-1" href="https://www3.truecorp.co.th/qos/truemoveh/QOS-Q3-tmh-2563.html?_ga=2.10577853.820278509.1607918584-334778289.1607918584" target="_blank"><?php echo $words['term_and_condition']; ?> </a>
        <a id="pri-2" href="https://www3.truecorp.co.th/new/privacy-policy?_ga=2.10577853.820278509.1607918584-334778289.1607918584"  target="_blank"><?php echo $words['privacy_policy']; ?></a>
        <p id="pri-3"><?php echo $words['copy_right']; ?> </p>
    </div>
</div>