   <?php //print_r($experience_auto_list); ?>
   <?php if( sizeof($experience_list) > 0 ) { ?>
    <div class="sec-experience-business">
        <div class="container">
            <div class="row row-cols-2 row-cols-md-3 row-cols-lg-5 justify-content-center">
            <?php 
            // $img_name = ['experince-icon-financial.png','experince-icon-retail.png','experince-icon-healthcare.png','experince-icon-industrial.png','experince-icon-property.png','experince-icon-technology.png','experince-icon-goverment.png','experince-icon-education.png','experince-icon-5gexperiences.png'];
            // $title_name = ['ธุรกิจการเงิน','ธุรกิจค้าปลีก','ธุรกิจความงามและสุขภาพ','ธุรกิจอุตสาหกรรม','ธุรกิจอสังหาริมทรัพย์','ธุรกิจเทคโนโลยี','งานราชการ','การศึกษา','ประสบการณ์ 5G'];
            // for($i=0;$i<9;$i++){ 
                foreach ($experience_list as $key => $list) {
            ?>
                <div class="col">
                    <div class="box-thumb fw position-relative text-left">
                        <a href="<?php echo base_url().$lang_path.'/experience/'.$list['slug']; ?>">
                        <div class="thumb thumb-ver">
                            <div class="thumb-group">
                                <div class="thumb-img"><img src="<?php echo $list['thumb_image'] != ''  ? upload_path($list['thumb_image']) : '' ; ?>" class="w-100 float-left"></div>
                                <div class="thumb-txt text-center">
                                    <div class="thumb-txt-h"><span><b><?php echo $list['title']; ?></b></span></div>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
    <img src="/assets/img/element/skew-right.png"  class="abs ele-exp-tr"alt="">
    <img src="/assets/img/element/skew-left.png"  class="abs ele-exp-tl"alt="">
    <?php } ?>