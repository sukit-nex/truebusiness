<?php if( sizeof($solution_auto_list) > 0 ) { ?>
    <img src="/assets/img/element/ele-buble-slt.png" alt="" class="abs ele-fluid-top">
    <img src="/assets/img/element/ele-buble-slt-lb.png" alt="" class="abs ele-fluid-lb">
<div class="sec section-news box-greenline mb-0 st">
   
    <div class="sec mb-0" style="position:relative;">
        <?php foreach ($solution_auto_list as $key => $list) { ?>
        <div class="box-thumb fw position-relative text-left">
            <div class="thumb thumb-hoz thumb-lr">
                <div class="thumb-group">
                    <div class="thumb-img <?php echo ($key%2 == 0)? 'order-first order-md-2':''; ?>"><img src="<?php echo $list['thumb_image_horizontal'] != ''  ? upload_path($list['thumb_image_horizontal']) : '/assets/img/template/image-default.jpg' ; ?>" class="w-100 float-left"></div>
                    <div class="thumb-txt text-left <?php echo ($key%2 == 0)? 'order-2 order-md-first':''; ?>">
                        <div class="thumb-txt-h txt-upper"><span class="txt-gdcolor"><b><?php echo $list['title']; ?></b></span></div>
                        <div class="thumb-txt-s"><b><?php echo $list['sub_title']; ?></b></div>
                        <div class="thumb-txt-p"><?php echo $list['description']; ?></div>
                        <div class="thumb-txt-a text-md-left text-center">
                            <a class="btn" href="<?php echo base_url().$lang_path.'/solution/'.$list['slug']; ?>"><span>รายละเอียดเพิ่มเติม</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <?php } ?>
    </div>
</div>
<?php } ?>