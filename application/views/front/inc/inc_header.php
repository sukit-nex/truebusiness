<?php /* ?>
<div class="section-header">
	<div class="box-logo">
		<a href="<?php echo base_url() ?>">
            <img src="/assets/img/skin/logo-true5g-main.png" class="d-none d-lg-block">
            <img src="/assets/img/skin/logo-true5g-main-2.png" class="d-lg-none">
        </a>
	</div>
    <?php if( $page == "search"){ ?>
        <div class="head-page  d-lg-none"><h6><?php echo $page_detail['page_name']; ?></h6></div>
    <?php }else{ ?>
        <div class="head-page  d-lg-none"><h6><?php echo isset( $content_detail ) && sizeof( $content_detail ) > 0 ? '' : $page_detail['page_name']; ?></h6></div>
    <?php } ?>
    
    <div class="btn-menu d-block d-lg-none">
        <div class="groupMenu"> <span class="pan1"></span> <span class="pan2"></span> <span class="pan3"></span> </div>
    </div>

    <div class="box-menu">
    	<div class="container container-1600">
            <div class="main-menu scroll-menu">
                <ul class="ul-menu ul-menu-product">
                    <?php foreach( $menus as $menu ){ ?>
                        
                            <?php  $uri = getSlug( $menu['page_code'] , $pages );

                                if( $menu['type'] == 'link' ){
                                    $link_click = $menu['link'];
                                    if( $menu['link'] == '/shop' ){
                                        $uri = 'shop';
                                    }else if($menu['link'] == '/'){
                                        $uri = 'home';
                                    }
                                }else if( $menu['type'] == 'slug' ){
                                    $uri = $menu['slug'];
                                    $link_click = "javascript:void(0);";
                                }else{
                                    $link_click = base_url().$lang_path.'/'.$uri;
                                }
                                if( $link_click == "" ){ $link_click = "javascript:void(0);";}
                            ?>
                        <li><a class="m-menu m-parent <?php echo $uri == $menu_check ? 'active' : ''; ?> <?php echo sizeof( $menu['sub_level1'] ) > 0 ? 'has-sub' : '';  ?>" data-menu="<?php echo $uri; ?>" href="<?php echo $link_click ?>"><span><?php echo $menu['name']; ?></span></a>
                            <?php if( sizeof( $menu['sub_level1'] ) > 0 ){ ?>
                                <ul class="sub-nav sub-menu">
                                    <div class="container container-fluid">
                                        <?php foreach( $menu['sub_level1'] as $menu1 ){ ?>
                                            <?php echo 'mixer-'.$sub_menu_check; ?>
                                            <?php
                                                if( $menu1['type'] == 'slug'){
                                                    $sub_uri = $menu1['slug'];
                                                }else{
                                                    $sub_uri = getSlug( $menu1['page_code'] , $pages );
                                                }
                                                if( $menu1['type'] == 'link' ){
                                                    $link_click = $menu1['link'];
                                                }else if( $menu1['type'] == 'page' ){
                                                    $link_click = base_url().$lang_path.'/'.$uri.'/'.$sub_uri;
                                                }else if( $menu1['type'] == 'slug' ){
                                                    $link_click = base_url().$lang_path.'/'.$menu['slug'].'/'.$sub_uri;
                                                }
                                                if( $link_click == "" ){ $link_click = "javascript:void(0);";}
                                            ?>
                                            <li><a class="m-menu <?php echo ($sub_uri != '' && $sub_uri == $sub_menu_check ) ? 'active' : ''; ?>" href="<?php echo $link_click; ?>"><?php echo $menu1['name']; ?></a></li>
                                         <?php } ?>
                                    </div>
                                </ul>
                            <?php } ?>
                        </li>
                    <?php } ?>
                    <!-- <li><a class="m-menu has-sub" data-menu="uidesign" href="javascript:void(0)"><span>UI Design</span></a>
                    	<ul class="sub-nav sub-menu">
                        	<div class="container">
                            	<li><a class="m-menu" href="content-ui">Content Design</a></li>
                                <li><a class="m-menu" href="banner-ui">Banner Design</a></li>
                                <li><a class="m-menu" href="video-ui">Video Design</a></li>
                                <li><a class="m-menu" href="list-ui">List Design</a></li>
                            </div>
                        </ul>
                    </li> -->
                </ul>
               
                <!-- <ul class="ul-menu ul-menu-product">
                    <li><a class="m-menu active has-sub" data-menu="life" href="true5g-life"><span>True5G Life</span></a>
                    	<ul class="sub-nav sub-menu">
                        	<div class="container">
                        	<li><a class="m-menu active" href="smart-living">Smart Living</a></li>
                            <li><a class="m-menu" href="smart-transportation">Smart transportation</a></li>
                            <li><a class="m-menu" href="smart-productivity">Smart productivity</a></a></li>
                            <li><a class="m-menu" href="smart-leisure">Smart leisure</a></li>
                            <li><a class="m-menu" href="smart-shopping">Smart shopping</a></li>
                            <li><a class="m-menu" href="smart-entertainment">Smart entertainment</a></li>
                            </div>
                        </ul>
                    </li>
					<li><a class="m-menu has-sub" data-menu="verticle" href="javascript:void(0)"><span>True5G Verticle</span></a>
                    	<ul class="sub-nav sub-menu">
                        	<div class="container">
                        	<li><a class="m-menu" href="">5G Intelligent</a></li>
                            <li><a class="m-menu" href="">5G Agriculture</a></li>
                            <li><a class="m-menu" href="">5G Education</a></a></li>
                            <li><a class="m-menu" href="">5G Industry</a></li>
                            <li><a class="m-menu" href="">5G Health & wellness</a></li>
                            <li><a class="m-menu" href="">5G Retail</a></li>
							<li><a class="m-menu" href="">Business solution</a></li>
                            </div>
                        </ul>
                    </li>
                    <li><a class="m-menu has-sub" data-menu="xphenomenon" href="javascript:void(0)"><span>Xphenomenon</span></a>
                    	<ul class="sub-nav sub-menu">
                        	<div class="container">
                            	<li><a class="m-menu" href="pagelist.php">5G XR Studio</a></li>
                                <li><a class="m-menu" href="pagelist.php">5G WorldtechX</a></li>
                            </div>
                        </ul>
                    </li>
                    <li><a class="m-menu has-sub" data-menu="uidesign" href="javascript:void(0)"><span>UI Design</span></a>
                    	<ul class="sub-nav sub-menu">
                        	<div class="container">
                            	<li><a class="m-menu" href="content-ui">Content Design</a></li>
                                <li><a class="m-menu" href="banner-ui">Banner Design</a></li>
                                <li><a class="m-menu" href="video-ui">Video Design</a></li>
                                <li><a class="m-menu" href="list-ui">List Design</a></li>
                            </div>
                        </ul>
                    </li>
                </ul> -->
            </div>
        </div>
        <div class="box-lang d-none">
            <ul>
                <li><a class="m-menu" href="javascript:void(0)">TH</a></li>
                <li><a class="m-menu" href="javascript:void(0)">EN</a></li>
            </ul>
        </div>
    </div>
    
</div>
<?php */ ?>

<nav class="navbar navbar-expand-lg nav-main-top fixed-top p-0 d-none d-lg-block py-1" aria-label="">
    <div class="container-xl container-md list-menu">
      <a class="navbar-brand mb-1" href="#"><img width="65" src="/assets/img/skin/LOGO-b.png" alt=""></a>

      <div class="collapse navbar-collapse color-white justify-content-end" id="">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0 d-none d-xxl-flex">
          <li class="nav-item">
            <a class="nav-link" href="#">TrueMove H</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">TrueOnline</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">TrueVision</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">TrueID</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">iService</a>
          </li>
        </ul>
        <div class="d-flex align-items-center">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 pe-2 border-end border-gray">
            <li class="nav-item">
              <a class="nav-link" href="#">Business</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">About Us</a>
            </li>
          </ul>
          <!-- Shop  -->
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 ps-1 pe-2 border-end border-gray">
            <li class="nav-item">
              <a class="nav-link" href="#"><span class="icon icon-box align-bottom me-2 fs-small"></span>Tracking
                order</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><span class="icon icon-cart align-bottom me-2 fs-small"></span>Cart</a>
            </li>
          </ul>
          <!-- Log in  -->
          <ul class="navbar-nav me-auto mb-2 ps-1 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="#"><span class="icon icon-account align-bottom me-2 fs-small"></span>Log in</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><span
                  class="icon icon-register align-bottom me-2 fs-small"></span>Register</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><span class="icon icon-search align-bottom me-2"></span></a>
            </li>

          </ul>
          <!-- Switch lang  -->
          <div class="d-flex align-items-center">
            <div class="my-2">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0 switch-lang ">
                <li class="nav-item">
                  <a class="nav-link  fs-small lh-08 py-1 px-2" href="#">ไทย</a>
                </li>
                <li class="nav-item active">
                  <a class="nav-link fs-small lh-08 py-1 px-2" href="#">EN</a>
                </li>
              </ul>
            </div>
          </div>
        </div>


      </div>
    </div>
  </nav>
  <!-- Nav Bar White -->
  <nav class="navbar navbar-expand-lg  bg-white nav-mainmenu fixed-top" aria-label="Tenth navbar example">
    <div class="container py-1 py-md-0">
      <button class="navbar-toggler navbar-toggler-button ps-0" type="button" data-bs-toggle="collapse"
        data-bs-target="#navbarsMainMenu" aria-controls="navbarsMainMenu" aria-expanded="false"
        aria-label="Toggle navigation">
        <div class="animated-hamburger"><span></span><span></span><span></span></div>
      </button>

      <a class="navbar-brand me-0 d-lg-none" href="#"><img width="65" src="assets/images/LOGO-c.png" alt=""></a>
      <div class="d-flex d-lg-none">
        <ul class="navbar-nav me-auto ps-1 mb-lg-0 d-flex flex-row fs-4">
          <li class="nav-item">
            <a class="nav-link py-1" href="#"><span class="icon icon-cart align-bottom me-3"></span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link py-1" href="#"><span class="icon icon-account align-bottom"></span></a>
          </li>

        </ul>
      </div>
      <div class="collapse navbar-collapse justify-content-md-center" id="navbarsMainMenu">
        <div class="menu w-100">
          <ul class=" navbar-nav fw-light justify-content-around">
            <li class="nav-item ">
              <a class="nav-link  container-sm" href="#">5G</a>
            </li>
            <!-- Mobile Menu  -->
            <li class="nav-item dropdown ">
              <a class="nav-link dropdown-toggle container-sm" href="#" id="DropdownMenuMobile" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Mobile
              </a>
              <div class="dropdown-menu" aria-labelledby="DropdownMenuMobile">
                <div class="container d-flex flex-row flex-wrap fs-6 pb-2">
                  <!-- dropdown -->
                  <ul class="col-6 col-md-3 navbar-nav me-auto mb-2 mb-lg-0 pe-2 flex-column">
                    <li class="nav-item fw-bold mb-2">
                      Postpaid
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Most Advanced Services</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Hora numbers</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Extra packages</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Switch to Postpaid</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Move To True</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Roaming and IDD</a>
                    </li>
                  </ul>
                  <!-- dropdown -->
                  <ul class="col-6 col-md-3 navbar-nav me-auto mb-2 mb-lg-0 pe-2 flex-column">
                    <li class="nav-item fw-bold mb-2">
                      Prepaid
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Prepaid packages</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Tourist</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Prepaid extra packages</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Switch to Postpaid</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Top-up</a>
                    </li>
                  </ul>
                  <!-- dropdown -->
                  <ul class="col-6 col-md-3 navbar-nav me-auto mb-2 mb-lg-0 pe-2 flex-column">
                    <li class="nav-item fw-bold mb-2">
                      Network
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Experience True 5G</a>
                    </li>
                  </ul>
                  <!-- dropdown -->
                  <ul class="col-6 col-md-3 navbar-nav me-auto mb-2 mb-lg-0 pe-2 flex-column">
                    <li class="nav-item fw-bold mb-2">
                      Mobile Guides & Tips
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Activating your new SIM</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Setting up your new device</a>
                    </li>
                  </ul>
                </div>

              </div>
            </li>
            <!-- Fober Menu  -->
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle container-sm" href="#" id="DropdownMenuFiber" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Internet Fiber
              </a>
              <div class="dropdown-menu menu-fiber" aria-labelledby="DropdownMenuFiber">
                <div class="container d-flex flex-row flex-wrap fs-6 pb-2">
                  <!-- dropdown -->
                  <ul class="col-6 col-md-3 navbar-nav me-auto mb-2 mb-lg-0 pe-2 flex-column flex-wrap">
                    <li class="nav-item fw-bold mb-2">
                      Packages
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Broadband packages</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Broadband + Mobile packages</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Broadband + Entertainment + Mobile packages</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Broadband extra packages</a>
                    </li>
                  </ul>
                  <!-- dropdown -->
                  <ul class="col-6 col-md-3 navbar-nav me-auto mb-2 mb-lg-0 pe-2 flex-column">
                    <li class="nav-item fw-bold mb-2">
                      Shop online
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Smart home solutions</a>
                    </li>
                  </ul>
                  <!-- dropdown -->
                  <ul class="col-6 col-md-3 navbar-nav me-auto mb-2 mb-lg-0 pe-2 flex-column">
                    <li class="nav-item fw-bold mb-2">
                      Coverage
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Check network coverage</a>
                    </li>
                  </ul>
                  <!-- dropdown -->
                  <ul class="col-6 col-md-3 navbar-nav me-auto mb-2 mb-lg-0 pe-2 flex-column">
                    <li class="nav-item fw-bold mb-2">
                      WiFi Guides & Tips
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Test your speed</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Improve WiFi connection</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Setting up WiFi router</a>
                    </li>
                  </ul>
                </div>

              </div>
            </li>
            <!-- Entertainment Menu  -->
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle container-sm" href="#" id="DropdownMenuTV" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Entertainment
              </a>
              <div class="dropdown-menu menu-tv" aria-labelledby="DropdownMenuTV">
                <div class="container d-flex flex-row flex-wrap fs-6 pb-2">
                  <!-- dropdown -->
                  <ul class="col-6 col-md-3 navbar-nav me-auto mb-2 mb-lg-0 pe-2 flex-column flex-wrap">
                    <li class="nav-item fw-bold mb-2">
                      True Vision
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">True Vision packages</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">True Vision + Broadband packages</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">True Vision + Broadband + Mobile packages</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">True Vision channels and guides</a>
                    </li>
                  </ul>
                  <!-- dropdown -->
                  <ul class="col-6 col-md-3 navbar-nav me-auto mb-2 mb-lg-0 pe-2 flex-column">
                    <li class="nav-item fw-bold mb-2">
                      True ID TV
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">True ID TV subscription</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">True ID TV box</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">True ID TV + Broadband + Mobile packages</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">True ID TV channels and guides</a>
                    </li>
                  </ul>
                  <!-- dropdown -->
                  <ul class="col-6 col-md-3 navbar-nav me-auto mb-2 mb-lg-0 pe-2 flex-column">
                    <li class="nav-item fw-bold mb-2">
                      Ways to watch
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">True ID TV online</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">True ID TV app</a>
                    </li>
                  </ul>
                  <!-- dropdown -->
                  <ul class="col-6 col-md-3 navbar-nav me-auto mb-2 mb-lg-0 pe-2 flex-column">
                    <li class="nav-item fw-bold mb-2">
                      Entertainment Guides & Tips
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Activating True Vision box</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Recording programs on True Vision</a>
                    </li>
                  </ul>
                </div>

              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link container-sm" href="#">Devices & Gadgets</a>
            </li>
            <li class="nav-item">
              <a class="nav-link container-sm" href="#">Digital Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link container-sm" href="#">Rewards & Privileges</a>
            </li>
            <li class="nav-item">
              <a class="nav-link container-sm" href="#">True Store</a>
            </li>
          </ul>
          <div class="d-flex d-lg-none align-items-center px-3 container-sm">
            <div class="my-2">
              <ul class="flex-row navbar-nav me-auto mb-2 mb-lg-0 switch-lang ">
                <li class="nav-item">
                  <a class="nav-link  fs-small lh-08 py-1 px-2" href="#">ไทย</a>
                </li>
                <li class="nav-item active">
                  <a class="nav-link fs-small lh-08 py-1 px-2" href="#">EN</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

    </div>
  </nav>