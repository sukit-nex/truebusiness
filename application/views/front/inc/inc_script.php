<script> var base_url = '<?php echo base_url(); ?>';</script>
<script type="text/javascript" src="/assets/js/bootstrap.bundle.min.js"></script> 
<script type="text/javascript" src="/assets/js/TweenMax.min.js"></script> 
<script type="text/javascript" src="/assets/js/popper.min.js"></script> 
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="/assets/js/bootstrap-select.min.js"></script> 
<script type="text/javascript" src="/assets/js/sticky-sidebar.js"></script> 
<script type="text/javascript" src="/assets/js/superscrollorama.js"></script> 
<script type="text/javascript" src="/assets/js/swiper.min.js"></script> 
<script type="text/javascript" src="/assets/js/navbar.js"></script> 
<script type="text/javascript" src="/assets/js/main.js"></script>

<?php if(isset($js) && $js != "" ){ ?>
<script type="text/javascript" src="/assets/js/<?php echo $js.'.min.js'; ?>"></script>
<?php } ?>

<?php 
if( sizeof($landing_scripts) > 0 ){ 
    echo $landing_scripts['scripts_footer'];
}
?>
</body>

</html>

<?php  /* <script> var base_url = '<?php echo base_url(); ?>';</script>

<script type="text/javascript" src="/assets/js/bundle.min.js"></script>

<?php if(isset($js) && $js != "" ){ ?>
<script type="text/javascript" src="/assets/js/<?php echo $js.'.min.js'; ?>"></script>
<?php } ?>

</body>

</html> <?php */ ?>
