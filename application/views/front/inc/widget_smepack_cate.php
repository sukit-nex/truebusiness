
<div class="fw">
 <?php 
    if (sizeof($cate_detail) > 0 ) { 
        foreach ($cate_detail as $key_cate => $row) {
          
 ?>

        <?php 
            if ( sizeof($cate_detail[$key_cate]) > 0 ) {
                foreach($cate_detail[$key_cate] as $key => $cate_content) {
        ?>
       
        <div class="sec-smepackcate genSlide smepackcate-color-<?php echo $cate_content['code']?>" data-perview-d="3" data-perview-t="1"  data-perview-m="1.2" data-space-d="30"  data-space-d="30" data-space-m="20" id="smunu-<?php echo $cate_content['code'] ?>">
            <div class="container">
                <div class="smepackcate-title">
                    <h2><b><?php echo $words['prefix_title_pack'] != "" && $lang_path == "th" ? $words['prefix_title_pack'] :'' ?> <?php echo $cate_content['title']?> <?php echo $words['prefix_title_pack'] != "" && $lang_path == "en" ? $words['prefix_title_pack'] :'' ?></b></h2>
                    <a href="<?php echo base_url().$lang_path.'/smepack/'.$cate_content['slug'];?>" class="btn btn-sm btn-icon d-none d-md-inline-block"><span><span>ดูแพ็กเกจทั้งหมด</span><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></span></a>
                </div>
                <div class="smepackcate-slide">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                        <?php 
                            if ( sizeof($cate_content['cate_list']) > 0 ) {
                                
                                foreach($cate_content['cate_list'] as $con_key => $content) {
                                    $is_mobile = 0; $is_mobile_internet = 0; $is_internet = 0;$is_true_visions = 0;$is_fixedline = 0;$is_valueset = 0;$is_mobile_bundle  = 0; $is_hl  = 0; 
                        ?>
                    
                            <div class="swiper-slide">
                                <a href="<?php echo base_url().$lang_path.'/smepack/'.$cate_content['slug'].'/'.$content['slug'];?>">
                                    <div class="box-tablecate">
                                        <div class="tablecate-list">
                                            <div class="tablecate-head">
                                                <div class="tablecate-head-group">
                                                    <?php if($content['title'] != ""){ ?>
                                                        <div class="tablecate-head-group-topic t-bold"><?php echo $content['title']; ?></div>
                                                    <?php } ?>
                                                    <?php if($cate_content['short_description'] != ""){ ?>
                                                        <div class="tablecate-head-group-detail"><?php echo $content['short_description'] ?></div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="tablecate-image">
                                                <img class="fw" src="<?php echo $content['thumb'] != ""  ? UPLOAD_PATH.$content['thumb'] :'/assets/img/package/sme-cate-image.jpg' ?>">
                                            </div>
                                            <?php
                                                if ( sizeof($content['smepack']) > 0 ) { ?>
                                                <div class="tablecate-detail">
                                                    <div class="tablecate-detail-price">
                                                        <?php if( $content['smepack']['price'] != '' ){ ?>
                                                            <b><?php echo $content['smepack']['price'] ?></b>
                                                        <?php } ?>   
                                                        <?php if($content['smepack']['price_billing_cycle'] != '' ){ 
                                                            echo $content['smepack']['price_billing_cycle']; 
                                                        } ?>  
                                                    </div>    
                                                    <div class="tablecate-detail-group">
                                                        <?php if ( $content['smepack']['package'] != '' ) { $features  = json_decode($content['smepack']['package'],true ); } ?>

                                                            <?php 
                                                        
                                                            if($key_cate == 'internet'){
                                                                if(array_search('internet', array_column($features, 'feature')) !== false &&  array_search('mobile_bundle', array_column($features, 'feature')) !== false ){
                                                                    $check_feature = 1;
                                                                }else{ $check_feature = 0;}
                                                            }elseif($key_cate == 'digitaltv'){
                                                                if(array_search('internet', array_column($features, 'feature')) !== false &&  array_search('true_visions', array_column($features, 'feature')) !== false){
                                                                    $check_feature = 1;
                                                                }else{ $check_feature = 0;}
                                                            }elseif($key_cate == 'valueset'){
                                                                if(array_search('internet', array_column($features, 'feature')) !== false &&  array_search('mobile_bundle', array_column($features, 'feature')) !== false){
                                                                    $check_feature = 1;
                                                                }else{ $check_feature = 0;}
                                                            }
                                                            foreach($features as $key => $smepack){ 
                                                                if($smepack['onoff'] == 1){
                                                            ?>
                                                                
                                                                <?php if($key_cate == 'mobile'){ ?>
                                                                    <?php if($content['smepack']['highlight_sme_mainpage'] == 0 || $content['smepack']['highlight_sme_mainpage'] == 1 && $content['smepack']['highlight_main_desciption'] == "" ){ ?>
                                                                        <?php if($smepack['feature'] == "mobile" &&  $is_mobile == 0 ){  ?> 
                                                                            <div class="tablecate-detail-group-list text-left"><div><?php echo $words['free_call'] != "" ? $words['free_call'] : "" ?> <b class="boldup"><?php echo $smepack['main_template_number_calls'] ?></b> <?php echo $words['time_minute'] != "" ? $words['time_minute'] : "" ?></div></div>  
                                                                        <?php $is_mobile++; } ?>  
                                                                        <?php if($smepack['feature'] == "mobile_internet" &&  $is_mobile_internet == 0 ){  ?> 
                                                                            <div class="tablecate-detail-group-list text-left"><div><?php echo $words['internet'] != "" ? $words['internet'] : "" ?> <b class="boldup"><?php echo $smepack['main_template_usage_amount'] ?></b>GB</div></div>  
                                                                        <?php $is_mobile_internet++; } ?>  
                                                                    <?php }elseif($content['smepack']['highlight_sme_mainpage'] == 1 &&  $is_hl == 0 && $content['smepack']['highlight_main_desciption'] != ""){
                                                                        echo $content['smepack']['highlight_main_desciption'];
                                                                $is_hl++; }  ?>
                                                                
                                                                <?php  }elseif( $key_cate == 'internet' ) { ?>
                                                                    <?php if($content['smepack']['highlight_sme_mainpage'] == 0 || $content['smepack']['highlight_sme_mainpage'] == 1 && $content['smepack']['highlight_main_desciption'] == "" ){ ?>
                                                                        <?php if($check_feature == 1){?>
                                                                            <?php if($smepack['feature'] == 'mobile_bundle' &&  $is_mobile_bundle  == 0){ ?>
                                                                                <div class="tablecate-detail-group-list text-left tdgl-small">
                                                                                    <div>
                                                                                        <?php if($smepack['main_template_time_calls'] != ""){ ?>
                                                                                            <div><?php echo $words['free_call'] != "" ? $words['free_call'] : "" ?> <b class="boldup"><?php echo $smepack['main_template_time_calls'] ?></b> <?php echo $words['time_minute'] != "" ? $words['time_minute'] : "" ?></div>
                                                                                        <?php } ?>
                                                                                        <?php if($smepack['main_template_usage_amount'] != ""){ ?>
                                                                                            <div><?php echo $words['internet'] != "" ? $words['internet'] : "" ?> <b class="boldup"><?php echo $smepack['main_template_usage_amount'] ?></b>GB</div>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                            <?php $is_mobile_bundle++;
                                                                            }elseif($smepack['feature'] == "internet" &&  $is_internet == 0 ){  ?> 
                                                                                <div class="tablecate-detail-group-list text-left tdgl-small">
                                                                                    <div>
                                                                                        <div><span class="tdgl-small-left"><?php echo $words['download_speed'] != "" ? $words['download_speed'] : "" ?></span> <span class="tdgl-small-right"><b class="boldup"><?php echo $smepack['main_template_download_speed'] ?></b>Mbps</span></div>
                                                                                        <div><span class="tdgl-small-left"><?php echo $words['upload_speed'] != "" ? $words['upload_speed'] : "" ?></span> <span class="tdgl-small-right"><b class="boldup"><?php echo $smepack['detail_template_upload_speed'] ?></b>Mbps</span></div>
                                                                                    </div>
                                                                                </div>
                                                                            <?php $is_internet++; }?>

                                                                        <?php }elseif($check_feature == 0){ ?>
                                                                            <?php if($smepack['feature'] == "internet" &&  $is_internet == 0){ ?> 
                                                                                <div class="tablecate-detail-group-list text-left">
                                                                                    <div>
                                                                                        <div><?php echo $words['max_download'] != "" ? $words['max_download'] : "" ?></div>
                                                                                        <div><b class="boldup"><?php echo $smepack['main_template_download_speed'] ?></b> Mbps</div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tablecate-detail-group-list text-left">
                                                                                    <div>
                                                                                        <div><?php echo $words['max_upload'] != "" ? $words['max_upload'] : "" ?></div>
                                                                                        <div><b class="boldup"><?php echo $smepack['detail_template_upload_speed'] ?></b> Mbps</div>
                                                                                    </div>
                                                                                </div>
                                                                            <?php $is_internet++; } ?>  
                                                                        <?php } ?>
                                                                    <?php }elseif($content['smepack']['highlight_sme_mainpage'] == 1 &&  $is_hl == 0 && $content['smepack']['highlight_main_desciption'] != ""){
                                                                        echo $content['smepack']['highlight_main_desciption'];
                                                                    $is_hl++; }  ?>

                                                                <?php  }elseif( $key_cate == 'digitaltv'){ ?>
                                                                    <?php if($content['smepack']['highlight_sme_mainpage'] == 0 || $content['smepack']['highlight_sme_mainpage'] == 1 && $content['smepack']['highlight_main_desciption'] == "" ){ ?>
                                                                        <?php if($check_feature == 1){ ?>
                                                                            <?php if($smepack['feature'] == "true_visions" &&  $is_true_visions == 0){ ?> 
                                                                                <div class="tablecate-detail-group-list text-left">
                                                                                    <div>
                                                                                        <?php if($smepack['main_template_number_boxes']  !== ""){ ?>
                                                                                            <div><b class="boldup"><?php echo $smepack['main_template_number_boxes']; ?></b> <?php echo $words['unit_box'] != "" ? $words['unit_box'] : "" ?> </div>
                                                                                        <?php } ?>

                                                                                        <?php if($smepack['main_template_channel_details']  !== ""){ ?>
                                                                                            <div><?php echo $smepack['main_template_channel_details'] ?></div>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                            <?php $is_true_visions++; } ?>  

                                                                            <?php if($smepack['feature'] == "internet" &&  $is_internet == 0){ ?>
                                                                                <div class="tablecate-detail-group-list text-left tdgl-small">
                                                                                    <div>
                                                                                    <div><span class="tdgl-small-left"><?php echo $words['download_speed'] != "" ? $words['download_speed'] : "" ?></span> <span class="tdgl-small-right"><b class="boldup"><?php echo $smepack['main_template_download_speed'] ?></b>Mbps</span></div>
                                                                                        <div><span class="tdgl-small-left"><?php echo $words['upload_speed'] != "" ? $words['upload_speed'] : "" ?></span> <span class="tdgl-small-right"><b class="boldup"><?php echo $smepack['detail_template_upload_speed'] ?></b>Mbps</span></div>
                                                                                    </div> 
                                                                                </div>
                                                                            <?php $is_internet++; } ?>
                                                                        <?php }elseif($check_feature == 0){ ?>
                                                                            <?php if($smepack['feature'] == "true_visions" &&  $is_true_visions == 0){ ?> 
                                                                                <div class="tablecate-detail-group-list ">
                                                                                    <div>
                                                                                        <?php if($smepack['main_template_number_boxes']  !== ""){ ?>
                                                                                            <div><b class="boldup"><?php echo $smepack['main_template_number_boxes']; ?></b> <?php echo $words['unit_box'] != "" ? $words['unit_box'] : "" ?> </div>
                                                                                        <?php } ?>

                                                                                        <?php if($smepack['main_template_channel_details']  !== ""){ ?>
                                                                                            <div><?php echo $smepack['main_template_channel_details'] ?></div>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                            <?php $is_true_visions++; } ?>  
                                                                        <?php } ?>
                                                                    <?php }elseif($content['smepack']['highlight_sme_mainpage'] == 1 &&  $is_hl == 0 && $content['smepack']['highlight_main_desciption'] != ""){
                                                                        echo $content['smepack']['highlight_main_desciption'];
                                                                $is_hl++; }  ?>

                                                                <?php  }elseif( $key_cate == 'fixedline'){ ?>
                                                                    <?php if($content['smepack']['highlight_sme_mainpage'] == 0 || $content['smepack']['highlight_sme_mainpage'] == 1 && $content['smepack']['highlight_main_desciption'] == "" ){ ?>
                                                                        <?php if($smepack['feature'] == "fixedline" &&  $is_fixedline == 0){ ?> 
                                                                            <?php if($smepack['main_template_call_price']  !== ""){ ?>
                                                                                <div class="tablecate-detail-group-list ">
                                                                                    <div><?php echo $words['free_call'] != "" ? $words['free_call'] : "" ?> <b class="boldup"><?php echo $smepack['main_template_call_price']; ?></b> <?php echo $words['time_minute'] != "" ? $words['time_minute'] : "" ?></div>  
                                                                                </div>
                                                                            <?php } ?>
                                                                        <?php $is_fixedline++; } ?>  
                                                                    <?php }elseif($content['smepack']['highlight_sme_mainpage'] == 1 &&  $is_hl == 0 && $content['smepack']['highlight_main_desciption'] != ""){
                                                                        echo $content['smepack']['highlight_main_desciption'];
                                                                $is_hl++; }  ?>
                                                                <?php  }elseif( $key_cate == 'valueset'){ ?>
                                                                    <?php if($content['smepack']['highlight_sme_mainpage'] == 0 || $content['smepack']['highlight_sme_mainpage'] == 1 && $content['smepack']['highlight_main_desciption'] == "" ){ ?>
                                                                        <?php if($check_feature == 1){ ?>
                                                                            <?php if($smepack['feature'] == "internet" &&  $is_internet == 0 ){  ?> 
                                                                                <div class="tablecate-detail-group-list text-left tdgl-small">
                                                                                    <div>
                                                                                        <div><span class="tdgl-small-left"><?php echo $words['download_speed'] != "" ? $words['download_speed'] : "" ?></span> <span class="tdgl-small-right"><b class="boldup"><?php echo $smepack['main_template_download_speed'] ?></b>Mbps</span></div>
                                                                                        <div><span class="tdgl-small-left"><?php echo $words['upload_speed'] != "" ? $words['upload_speed'] : "" ?></span> <span class="tdgl-small-right"><b class="boldup"><?php echo $smepack['detail_template_upload_speed'] ?></b>Mbps</span></div>
                                                                                    </div> 
                                                                                </div>
                                                                            <?php $is_internet++; }
                                                                            elseif($smepack['feature'] == 'mobile_bundle' &&  $is_mobile_bundle  == 0){ ?>
                                                                                <div class="tablecate-detail-group-list text-left tdgl-small">
                                                                                    <div>
                                                                                        <?php if($smepack['main_template_time_calls'] != ""){ ?>
                                                                                            <div><?php echo $words['free_call'] != "" ? $words['free_call'] : "" ?> <b class="boldup"><?php echo $smepack['main_template_time_calls'] ?></b> <?php echo $words['time_minute'] != "" ? $words['time_minute'] : "" ?></div>
                                                                                        <?php } ?>
                                                                                        <?php if($smepack['main_template_usage_amount'] != ""){ ?>
                                                                                            <div><?php echo $words['internet'] != "" ? $words['internet'] : "" ?> <b class="boldup"><?php echo $smepack['main_template_usage_amount'] ?></b>GB</div>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                            <?php $is_mobile_bundle++;} ?>

                                                                        <?php }elseif($check_feature == 0){ ?>
                                                                            <?php if($smepack['feature'] == "internet" &&  $is_internet == 0){ ?> 
                                                                                <div class="tablecate-detail-group-list text-left">
                                                                                    <div>
                                                                                        <div>ดาวน์โหลดสูงสุด</div>
                                                                                        <div><b><?php echo $smepack['main_template_download_speed'] ?></b></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tablecate-detail-group-list text-left">
                                                                                    <div>
                                                                                        <div>อัพโหลดสูงสุด</div>
                                                                                        <div><b><?php echo $smepack['detail_template_upload_speed'] ?></b></div>
                                                                                    </div>
                                                                                </div>
                                                                            <?php $is_internet++; } ?>  
                                                                        <?php } ?>
                                                                    <?php }elseif($content['smepack']['highlight_sme_mainpage'] == 1 &&  $is_hl == 0 && $content['smepack']['highlight_main_desciption'] != ""){
                                                                        echo $content['smepack']['highlight_main_desciption'];
                                                                $is_hl++; }  ?>
                                                                <?php  } ?>   
                                                            <?php  }
                                                            } ?>  
                                                            
                                                                
                                                    </div>  
                                                </div>
                                            <?php  } ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php 
                                }
                            } 
                        ?>
                        </div>
                        <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></div>
                        <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/arrow-left-1.svg" class="svg"></div> 
                    </div>
                </div>
                <div class="smepackcate-button text-center mb-5 d-block d-md-inline-none">
                    <a href="<?php echo base_url().$lang_path.'/smepack/'.$cate_content['slug'];?>" class="btn btn-sm btn-icon"><span><span>ดูแพ็กเกจทั้งหมด</span><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></span></a>
                </div>
            </div>
        </div>
            <?php 
                } 
            } 
            ?>
    
<?php 
        }
    } 
?>
</div>
