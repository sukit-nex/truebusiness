
	<!-- <div class="box-content sec-title">
		<?php if($section['title'] != ''){ ?><h1 class="font-weight-bold"><?php echo  $section['title'] ?></h1><?php } ?>
		<?php if($section['sub_title'] != ''){ ?><h4 class="font-weight-bold"><?php echo  $section['sub_title'] ?></h4><?php } ?>
	</div> -->

<?php if( has_topic( $section['title'] , $section['sub_title'] )){ ?>
	<div class="sec-title <?php echo getSecPositionHoz( $section['sec_title_alignment'] )?>">
		<div class="box-content">
			<?php if($section['title'] != ''){ ?>
				<h2 class="font-weight-bold"><span class="txt-gdcolor"><?php echo  $section['title'] ?></span></h2>
			<?php } ?>
			<?php if($section['sub_title'] != ''){ ?>
				<div class="title-sub"><?php echo $section['sub_title'] ?></div>
			<?php } ?>
		</div>
		<?php if($section['content'] != '' ){ ?>
			<?php echo $section['content'] ?>
		<?php } ?>
	</div>
<?php } ?>
<!-- <?php if($section['title'] != '' || $section['sub_title'] != '' || $section['content'] != ''){ ?>
	
<?php } ?> -->