<div id="sec_id_<?php echo $section['code'] ?>" class="section cont mg st st-list gen_margin  <?php echo $section['acc_type'] == 0 ? '' : 'st-list-acc-default'?>  <?php echo $section['custom_class'] != '' ?  customClass($section['custom_class']) : '' ?> "
    data-mg-desktop="<?php echo ar_decodeFront($section['mg_desktop'],'margin');?>"
    data-mg-mobile="<?php echo ar_decodeFront($section['mg_mobile'],'margin');?>"
    data-slidecol="<?php echo $section['column_per_view']; ?>"
    data-slidepercolumn="<?php echo $section['display_per_column']; ?>">
    <div class="bg-fullwidth fw gen_bg <?php echo getBackgroundSettingClass($section['bg_position_ver'],$section['bg_position_hoz'],$section['bg_repeat'],$section['bg_fixed'], $section['bg_size']);?>"
        data-bg-desktop="<?php echo $section['bg1_desktop'];  ?>"
        data-bg-mobile="<?php echo $section['bg1_mobile'];  ?>" data-bg-color="<?php echo $section['bg_color']; ?>">
        <div class="container <?php echo $section['section_layout'] == "fullwidth" ? 'container-fluid' : 'container-1600' ?> <?php echo $section['gen_height'] == 0 ? '' : 'gen-height' ?> "
            data-image-desktop="<?php echo $section['bg1_desktop'] != ''  ? upload_path($section['bg1_desktop']) : '/assets/img/skin/blank.png' ; ?>"
            data-image-mobile="<?php echo $section['bg1_mobile'] != ''  ? upload_path($section['bg1_mobile']) : '' ; ?>"
            data-column="<?php echo sizeof($row['columns'] ); ?>">
            <div class="position-relative fw gen_padding pd"
                data-pd-desktop="<?php echo ar_decodeFront($section['pd_desktop'],'padding');?>"
                data-pd-mobile="<?php echo ar_decodeFront($section['pd_mobile'],'padding');?>">
                <div class="cont-contain ml-0 mr-0 w-100">
                    <?php include('inc_topic.php') ?>

                    <?php 
                        if($section['widget'] == 'smepack_highlight'){
                            include('widget_smepack_highlight.php');
                        } else if ($section['widget'] == 'smepack_cate') {
                            include('widget_smepack_cate.php');
                        } else if ($section['widget'] == 'smepack_cate_menu') {
                            include('widget_smepack_menu.php');
                        } else if ($section['widget'] == 'solution_auto') {
                            include('widget_solution_auto.php');
                        } else if ($section['widget'] == 'solution_slide') {
                            include('widget_solution_slide.php');
                        } else if ($section['widget'] == 'experience_auto') {
                            include('widget_experience_auto.php');
                        } else if ($section['widget'] == 'experience_slide') {
                            include('widget_experience_slide.php');
                        } else if ($section['widget'] == 'experience_logo') {
                            include('widget_experience_logo.php');
                        }
                    ?>

                </div>
                
            </div>
        </div>
    </div>
</div>







    