  <?php //print_r($cate_list); ?>
  <?php //print_r($cate_detail); ?>
  <div class="sec-smehighlight">  
        <img class="abs ele-fluid-top-m" src="/assets/img/element/ele-fluid-top-m.png" alt="">  
        <div class="box-jumpto pb-5 pb-md-5" data-function="tabs">
            <div class="container container-1440">
                <div class="jumpto-title d-block d-md-none"><b>คุณกำลังมองหา<span style="display:inline-block">แพ็กเกจ ?</span></b></div>
                <div class="jumpto-group d-inline-block d-md-flex align-items-center bg-gdcolor-orange">
                    <div class="jumpto-title d-none d-lg-block"><b>คุณกำลังมองหา<span style="display:inline-block">แพ็กเกจ ?</span></b></div>
                    <div class="jumpto-list border-gradient bg-light d-none d-md-flex align-items-center justify-content-between">
                        <?php foreach($cate_list as $key => $catelist_val){ ?>
                            <div class=" jumpto-item jumpto-item-<?php echo $key; ?> solution-color-<?php echo $catelist_val['code']; ?>" ><a href="javascript:void(0)"  data-link="<?php echo $catelist_val['code'] ?>" class="btn btn-jump"><span><?php echo $catelist_val['title'] ?></span></a></div>
                        <?php } ?>
                    </div>
                    <div class="jumpto-select d-block d-md-none">
                        <div class="group-cate">
                            <div class="cate-show"><span class="cate-txt">โทรศัพท์มือถือ</span></div>
                            <ul class="cate-select">
                                <?php foreach($cate_list as $key => $catelist_val){ ?>
                                <li><a href="javascript:void(0)" class="select-cate-1"><span class="cate-txt"><?php echo $catelist_val['title'] ?></span></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="container">
            <div class="box-smehighlight" data-slide="1">
            <?php 
                if (sizeof($cate_detail) > 0 ) { 
                    foreach ($cate_detail as $key_cate => $sme_row) {
            ?>  
            <?php 
                if ( sizeof($cate_detail[$key_cate]) > 0 ) {
                    foreach($cate_detail[$key_cate] as $key => $cate_content) {
            ?>
            
                <div class="smehighlight-list smehighlight-list-<?php echo $cate_content['code']; ?>">
                    <div class="smehighlight-info">
                        <div class="smehighlight-info-group">
                            <div>
                                <div class="smehighlight-info-topic">
                                    <?php if($cate_content['prefix_title'] != ""){ ?>
                                        <div class="smehighlight-info-topic-prefix"><?php echo $cate_content['prefix_title']; ?></div>
                                    <?php } ?>
                                    <div class="smehighlight-info-topic-title"><img class="svg" src="<?php echo UPLOAD_PATH.$cate_content['icon_image']; ?>" alt="<?php echo $cate_content['title']?>"><b class="txt-gdcolor"><?php echo $cate_content['title']?></b></div>
                                </div>
                                <div class="smehighlight-info-desc d-none d-lg-block">สนับสนุนให้ทีมทำงานได้ทุกที่ <br>ด้วยเครื่องพร้อมแพ็กเกจในราคาสุดคุ้ม</div>
                                <div class="smehighlight-info-price">เริ่มต้น <b class="t-red">399</b> บาท/เดือน</div>
                            </div>
                        </div>
                        <div class="smehighlight-info-button d-none d-lg-block"><a href="<?php echo base_url().$lang_path.'/smepack/'.$cate_content['slug'];?>" class="btn btn-secondary"><span>ดูแพ็กเกจทั้งหมด</span></a></div>
                    </div>
                    <div class="smehighlight-group genSlide" data-perview-d="2" data-perview-t="2" data-perview-m="1.2"  data-space-m="20" >
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                            <?php 
                                if ( sizeof($cate_content['smepack']) > 0 ) {
                                    
                                    foreach($cate_content['smepack'] as $sme_key => $sme_content) {
                                        $is_mobile = 0; $is_mobile_internet = 0; $is_internet = 0;$is_true_visions = 0;$is_fixedline = 0;$is_valueset = 0;$is_mobile_bundle  = 0; $is_hl  = 0; 
                                        $cate_smes =  get_multiple_cate_array(array($sme_content['child_cate_code']), $cate_list_all); 
                                        
                            ?>
                            <?php if ( $sme_content['package']!= '' ) { $features  = json_decode($sme_content['package'],true ); } ?>
                                <div class="swiper-slide">
                                    <div class="tablepack-list">
                                        <div class="tablepack-info-cate">
                                        <?php foreach($cate_smes as $cate_sme ){ ?>
                                                <?php if($cate_sme['title'] != "" ){ ?>
                                                    <div class="tablepack-info-cate-topic"><b><?php echo $cate_sme['title'] ?></b></div>
                                                <?php } ?>
                                                <?php if($cate_sme['short_description'] != "" ){ ?>
                                                    <div class="tablepack-info-cate-detail"><?php echo $cate_sme['short_description']; ?></div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>

                                        <?php if($sme_content['highlight_homepage'] == 1){ ?>
                                                <div class="tablepack-highlight"><span><?php echo $words['pack_recommend'] != "" ? $words['pack_recommend'] :'' ?></span></div>
                                            <?php } ?>
                                            <div class="tablepack-head 
                                                <?php if($key_cate == 'mobile'){echo 'bg-gdcolor-orange'; 
                                                }elseif($key_cate == 'internet'){echo 'bg-gdcolor-blue'; 
                                                }elseif($key_cate == 'digitaltv'){echo 'bg-gdcolor-pink'; 
                                                }elseif($key_cate == 'fixedline'){echo 'bg-gdcolor-darkblue' ;
                                                }elseif($key_cate == 'valueset'){echo 'bg-gdcolor-red'; }
                                                ?>">
                                                <div class="pack-price-group">
                                                    <?php if($sme_content['price'] != ""){ ?>
                                                        <div class="pack-price t-black"><?php echo $sme_content['price'] ?></div>
                                                    <?php } ?>
                                                    <?php if($sme_content['price_billing_cycle'] !=  ""){ ?>
                                                    <div class="pack-circle"><?php echo $sme_content['price_billing_cycle'] ?></div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <div class="tablepack-body">
                                            <?php  foreach($features as $feature){ ?>
                                                <?php  if($feature['feature'] == "mobile" ){ ?>
                                                    <div class="feature-list">
                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                            <?php  if($feature['header_message']){ ?>
                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                            <?php } ?>
                                                            <div class="g-feature-main">
                                                                <?php if($feature['detail_type'] == "template"  ){ ?>
                                                                    <?php if($feature['detail_template_service_fee'] != "" || $feature['detail_template_freecall_conditions'] != "" || $feature['detail_template_4g_hd_voice'] != ""  ){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>
                                                                <?php if($feature['main_type'] == 'template' ){ ?>
                                                                    <?php if( $feature['main_template_type_calls'] == "อั้น" ){ ?>
                                                                        <div class="g-feature-detail"><span><b><?php echo $words['free_call'] != "" ? $words['free_call'] :'' ?> <?php echo $feature['main_template_number_calls'] ?></b></span> <?php echo $words['time_minute'] != "" ? $words['time_minute'] :'' ?><br><?php echo $feature['main_template_type_networks'] ?>
                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                            <br> <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                            <br> <a class="g-feature-detail-link" target="<?php echo $feature['btn_external_link'] == "" ?'': '_blank'?>"   href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                            <br> <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                        </div>
                                                                    <?php } elseif( $feature['main_template_type_calls'] == "ไม่อั้น" ){?>
                                                                        <div class="g-feature-detail"><b><?php echo $words['free_call'] != "" ? $words['free_call'] :'' ?> <?php echo $words['unlimited'] != "" ? $words['unlimited'] :'' ?> <?php echo $feature['main_template_type_networks'] ?></b><br><?php echo $feature['main_template_time_calls']; ?>
                                                                            <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                <br> <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                            <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                <br> <a class="g-feature-detail-link" target="<?php echo $feature['btn_external_link'] == "" ?'': '_blank'?>"   href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                            <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                <br> <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                            <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                <br> <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                            <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php }elseif($feature['main_type'] == "manual"){ ?>
                                                                    <div class="g-feature-detail">
                                                                        <?php echo $feature['main_manual_description'] ?>
                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                <a class="g-feature-detail-link" target="<?php echo $feature['btn_external_link'] == "" ?'': '_blank'?>"   href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                            <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            
                                                            <div class="g-feature-toggle">
                                                                <?php if($feature['detail_type'] == "manual" ){ ?>
                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                    <?php } ?>
                                                                <?php }elseif($feature['detail_type'] == "template" ){ ?>
                                                                    <?php if($feature['detail_template_service_fee'] != ""){ ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $words['service_charge'] != "" ? $words['service_charge'] :'' ?>  <?php echo $feature['detail_template_service_fee']; ?> <?php echo $words['baht_per_month'] != "" ?  $words['baht_per_month'] : '' ?></div>
                                                                    <?php } ?>
                                                                    <?php if($feature['detail_template_freecall_conditions'] != ""){ ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_template_freecall_conditions']; ?></div>
                                                                    <?php } ?>
                                                                    <?php if($feature['detail_template_4g_hd_voice'] != ""){ ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_template_4g_hd_voice']; ?></div>
                                                                        
                                                                    <?php } ?>
                                                                <?php }else{} ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }elseif($feature['feature'] == "mobile_internet" ){ ?>
                                                    <div class="feature-list">
                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                            <?php  if($feature['header_message']){ ?>
                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                            <?php } ?>
                                                            <div class="g-feature-main">
                                                                <?php if($feature['detail_type'] == "template"  ){ ?>
                                                                    <?php if($feature['detail_template_use_full_speed'] != ""  ){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>

                                                                <?php if($feature['main_type'] == "template"){ ?>
                                                                    <?php if($feature['main_template_type_use'] == "อั้น"){ ?>
                                                                        <div class="g-feature-detail"><b><?php echo $words['internet'] != "" ?  $words['internet'] : '' ?></b> <span><b><?php echo $feature['main_template_usage_amount']; ?></b></span>GB<br><?php echo $words['net_top_speed'] != "" ? $words['net_top_speed'] :'' ?> <?php echo $feature['main_template_maximum_speed']; ?> Mbps
                                                                        
                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                            <br> <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                            <br> <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                            <br><a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                        </div>
                                                                    <?php } if($feature['main_template_type_use'] == "ไม่อั้น"){ ?>
                                                                        <div class="g-feature-detail"><b><?php echo $words['internet'] != "" ? $words['internet'] :'' ?> <?php echo $words['unlimited'] != "" ? $words['unlimited'] :'' ?></b> <br><?php echo $words['net_top_speed'] != "" ? $words['net_top_speed'] :'' ?> <?php echo $feature['main_template_maximum_speed']; ?> Mbps 
                                                                        
                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                            <br>  <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                            <br><a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                            <br> <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php } elseif($feature['main_type'] == "manual") { ?>
                                                                        <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                        
                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                            <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                            ><a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                        </div>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="g-feature-toggle">
                                                                <?php if($feature['detail_type'] == "template"  ){ ?>
                                                                    <?php if($feature['detail_template_use_full_speed'] != ""  ){ ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_template_use_full_speed']; ?></div>
                                                                    <?php } ?>
                                                                <?php }elseif($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                    <?php  }
                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }elseif($feature['feature'] == "wifi" ){  ?>
                                                    <div class="feature-list">
                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                            <?php  if($feature['header_message']){ ?>
                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                            <?php } ?>
                                                            <div class="g-feature-main">
                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>
                                                                <?php if($feature['main_type'] == "template"){ ?>
                                                                    <?php if($feature['main_template_type_use'] == "อั้น"){ ?>
                                                                        <div class="g-feature-detail"><b>WiFi</b> <span><b><?php echo $feature['main_template_usage_amount']; ?></b></span>GB
                                                                        
                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                            <br><a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                            <br> <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                            <br> <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                        </div>
                                                                    <?php } if($feature['main_template_type_use'] == "ไม่อั้น"){ ?>
                                                                        <div class="g-feature-detail"><b>WiFi <?php echo $words['unlimited'] != "" ? $words['unlimited'] :'' ?></b>
                                                                        
                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                            <br><a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                            <br><a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                            <br><a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php } elseif($feature['main_type'] == "manual") { ?>
                                                                    <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                    
                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                        <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="g-feature-toggle">
                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                    <?php  }
                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }elseif($feature['feature'] == "internet" ){  ?>
                                                    <div class="feature-list">
                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                            <?php  if($feature['header_message']){ ?>
                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                            <?php } ?>
                                                            <div class="g-feature-main">
                                                                <?php if($feature['detail_type'] == "template"){ ?>
                                                                    <?php if($feature['detail_template_upload_speed'] != ""){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>

                                                                <?php if($feature['main_type'] == "template"){ ?>
                                                                    <div class="g-feature-detail"><?php echo $words['max_download_speed'] != "" ? $words['max_download_speed'] :'' ?><br><span><b><?php echo $feature['main_template_download_speed']; ?></b></span> Mbps
                                                                        
                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                            <br> <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                            <br><a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                            <br> <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                    </div>
                                                                <?php } elseif($feature['main_type'] == "manual") { ?>
                                                                    <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                    
                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                            <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                            <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="g-feature-toggle">
                                                                <?php if($feature['detail_type'] == "template"){ ?>
                                                                    <?php if($feature['detail_template_upload_speed'] != ""){ ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $words['max_upload_speed'] != "" ? $words['max_upload_speed'] :'' ?><?php echo $feature['detail_template_upload_speed']; ?>Mbps</div>
                                                                    <?php } ?>
                                                                <?php }elseif($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                    <?php  }
                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }elseif($feature['feature'] == "smart_wifi" ){  ?>
                                                    <div class="feature-list">
                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                            <?php  if($feature['header_message']){ ?>
                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                            <?php } ?>
                                                            <div class="g-feature-main">
                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>
                                                                    <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                        <br>
                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                        <br>
                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                        <br>
                                                                        <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                    </div>
                                                            </div>
                                                            <div class="g-feature-toggle">
                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                    <?php  }
                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }elseif($feature['feature'] == "true_visions" ){  ?>
                                                    <div class="feature-list">
                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                            <?php  if($feature['header_message']){ ?>
                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                            <?php } ?>
                                                            <div class="g-feature-main">
                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>

                                                                <?php if($feature['main_type'] == "template") { ?>
                                                                    <div class="g-feature-detail"><b><?php echo $feature['main_template_number_boxes']; ?> <?php echo $words['true_box'] != "" ? $words['true_box'] :'' ?></b><br><span><?php echo $feature['main_template_channel_details']; ?> </span>
                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                        <br>
                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                        <br>
                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                        <br>
                                                                        <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                    </div>
                                                                <?php }elseif($feature['main_type'] == "manual") { ?>
                                                                    <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                        <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="g-feature-toggle">
                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                    <?php  }
                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }elseif($feature['feature'] == "notebook" ){  ?>
                                                    <div class="feature-list">
                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                            <?php  if($feature['header_message']){ ?>
                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                            <?php } ?>
                                                            <div class="g-feature-main">
                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                

                                                                <?php if($feature['main_type'] == "template"){ ?>
                                                                    <div>
                                                                        <div class="g-feature-thumb"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>
                                                                        <div class="g-feature-detail text-center"><span><b><?php echo $feature['main_template_brand']; ?></b></span><br> <?php echo $feature['main_template_model_description']; ?>
                                                                            <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                                <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                                <br> <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                            <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                                <br><a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                            <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                                <br> <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                            <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                                <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                            <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                        </div>
                                                                    </div>
                                                                <?php } elseif($feature['main_type'] == "manual") { ?>
                                                                    <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>
                                                                    <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                    
                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                            <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                            <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="g-feature-toggle">
                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                    <?php  }
                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }elseif($feature['feature'] == "fixedline" ){  ?>
                                                    <div class="feature-list">
                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                            <?php  if($feature['header_message']){ ?>
                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                            <?php } ?>
                                                            <div class="g-feature-main">
                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>

                                                                <?php if($feature['main_type'] == "template") { ?>
                                                                    <div class="g-feature-detail"><b><?php echo $words['call_cost'] != "" ? $words['call_cost'] :'' ?> <?php echo $feature['main_template_call_price']; ?> <?php echo $words['baht_per_month'] != "" ? $words['baht_per_month'] :''?></b><br><span><?php echo $feature['main_template_freecall_conditions']; ?> </span>
                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                        <br>
                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                        <br>
                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                        <br>
                                                                        <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                    </div>
                                                                <?php }elseif($feature['main_type'] == "manual") { ?>
                                                                    <div class="g-feature-detail"> <?php echo $feature['main_manual_description']; ?>
                                                                    <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                        <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                        <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                        <a class="g-feature-detail-link" target="_blank"  href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                        <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                    <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                        <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                    <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="g-feature-toggle">
                                                                <?php if($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != "") {  ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                    <?php  }
                                                                }elseif( $feature['detail_type'] != "none"){ }?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }elseif($feature['feature'] == "mobile_bundle" ){ ?>
                                                    <div class="feature-list">
                                                        <div class="g-feature <?php echo $feature['display_toggle'] == 0 ? '' : 'active' ?>">
                                                            <?php  if($feature['header_message']){ ?>
                                                                <div class="g-feature-headline"><?php echo $feature['header_message'] ?></div>
                                                            <?php } ?>
                                                            <div class="g-feature-main">
                                                                <?php if($feature['detail_type'] == "template"  ){ ?>
                                                                    <?php if($feature['detail_template_service_fee'] != "" || $feature['detail_template_freecall_conditions'] != "" || $feature['detail_template_4g_hd_voice'] != "" || $feature['detail_template_use_full_speed'] || $feature['detail_template_usage_amount']){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif($feature['detail_type'] == "manual"){ ?>
                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                        <div class="icon-toggle"></div>
                                                                    <?php } ?>
                                                                <?php }elseif( $feature['detail_type'] != "none"){ }?>
                                                                <div class="g-feature-icon"><img class="fw" src="<?php echo  $feature['display_icon_image_txt']  != ''  ? UPLOAD_PATH.$feature['display_icon_image_txt'] : '/assets/img/package/icon-mobile.png'; ?>"></div>
                                                                <div class="g-feature-detail">
                                                                    <?php if($feature['main_type'] == 'template' ){ ?>
                                                                        <?php if( $feature['main_template_type_calls'] == "อั้น" ){ ?>
                                                                            <b>โทรฟรี</b> <span><b><?php echo $feature['main_template_number_calls'] ?></b></span> นาที <?php echo $feature['main_template_type_networks'] ?><br>
                                                                        <?php } elseif( $feature['main_template_type_calls'] == "ไม่อั้น" ){?>
                                                                            <b>โทรฟรี <?php echo $feature['main_template_type_networks'] ?></b><br><?php echo $feature['main_template_time_calls']; ?><br>
                                                                        <?php } ?>

                                                                        <?php if( $feature['main_template_internet_type_use'] == "อั้น" ){ ?>
                                                                            <b>เน็ต</b> <span>(4G/5G)<b><?php echo $feature['main_template_usage_amount'] ?>GB</b></span>
                                                                        <?php } elseif( $feature['main_template_internet_type_use'] == "ไม่อั้น" ){?>
                                                                            <b>เน็ต</b> <span>(4G/5G)<b> <?php echo $words['unlimited'] != "" ? $words['unlimited'] :''?></b></span>
                                                                        <?php } ?>
                                                                    <?php }elseif($feature['main_type'] == "manual"){ ?>
                                                                        <?php echo $feature['main_manual_description'] ?>
                                                                    <?php } ?>
                                                                        <?php if($feature['btn_type'] == 'internal_link'){  ?>
                                                                            <?php  $uri = getSlug( $feature['btn_internal_link'] , $pages ); ?>
                                                                            <br> <a class="g-feature-detail-link" href="<?php echo $feature['btn_internal_link'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'external_link'){  ?> 
                                                                            <br> <a class="g-feature-detail-link" target="<?php echo $feature['btn_external_link'] == "" ?'': '_blank'?>"   href="<?php echo $feature['btn_external_link'] == '' ? 'javascript:void(0);' : $feature['btn_external_link']; ?>"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'pdf'){  ?>
                                                                            <br> <a  class="g-feature-detail-link" href="<?php echo $feature['btn_pdf_file'] == "" ? 'javascript:void(0);' : upload_path($feature['btn_pdf_file']) ?>" target="<?php echo $feature['btn_pdf_file'] == "" ?'': '_blank'?>" rel="noopener noreferrer"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>  
                                                                        <?php }else if($feature['btn_type'] == 'content'){  ?>
                                                                            <a class="g-feature-detail-link" href="javascript:void(0)" onclick="popin('popup-content',$(this))"><?php echo $feature['btn_name'] == '' ? 'ดูรายละเอียด' : $feature['btn_name'] ?></a>
                                                                        <?php }else if($feature['btn_type'] == 'no_link'){ } ?>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="g-feature-toggle">
                                                                <?php if($feature['detail_type'] == "manual" ){ ?>
                                                                    <?php if($feature['detail_manual_description'] != ""){ ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_manual_description']; ?></div>
                                                                    <?php } ?>
                                                                <?php }elseif($feature['detail_type'] == "template" ){ ?>
                                                                    <?php if($feature['detail_template_service_fee'] != ""){ ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $words['service_charge'] != "" ? $words['service_charge'] :''?> <?php echo $feature['detail_template_service_fee']; ?> <?php echo $words['baht_per_month'] != "" ? $words['baht_per_month'] :''?> </div>
                                                                    <?php } ?>
                                                                    <?php if($feature['detail_template_freecall_conditions'] != ""){ ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_template_freecall_conditions']; ?></div>
                                                                    <?php } ?>
                                                                    <?php if($feature['detail_template_4g_hd_voice'] != ""){ ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_template_4g_hd_voice']; ?></div>
                                                                        
                                                                    <?php } ?>
                                                                    <?php if($feature['detail_template_use_full_speed'] != ""  ){ ?>
                                                                        <div class="g-feature-toggle-list"><?php echo $feature['detail_template_use_full_speed']; ?></div>
                                                                    <?php } ?>

                                                                    <?php if($feature['detail_template_wifi_type_use'] == "อั้น"  ){ ?>
                                                                        <div class="g-feature-toggle-list">WiFi <?php echo $feature['detail_template_usage_amount'] ?>GB</div>
                                                                    <?php }else{ ?>
                                                                        <div class="g-feature-toggle-list">WiFi <?php echo $words['unlimited'] != "" ? $words['unlimited'] :''?> </div>
                                                                    <?php } ?>
                                                                    
                                                                <?php }else{} ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="tablepack-foot">
                                            <hr>
                                            <a href="javascript:void(0)" class="btn w-100" onclick="popin('popup-content')"><span><?php echo $words['request_more_information'] != "" ?  $words['request_more_information'] : '' ?></span></a>
                                            <?php if( isset($cate_list['tc_pdf'] ) ){ ?>
                                                <a href="<?php echo upload_path().'/'.$cate_list['tc_pdf'] ?>" class="btn btn-text w-100"><span><?php echo $words['terms_and_conditions'] != "" ?  $words['terms_and_conditions'] : '' ?></span></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    </div>
                                <?php } 
                                } ?>
                            
                        </div>
                        <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></div>
                        <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/arrow-left-1.svg" class="svg"></div>
                    </div>
                    <div class="smehighlight-info-button text-center d-block d-lg-none"><a href="" class="btn btn-text"><span>ดูแพ็กเกจทั้งหมด</span></a></div>
                </div>
                </div>
                <?php 
                        } 
                    } 
                    ?>
            
            <?php 
                    }
                } 
            ?>
            </div>
        </div>
    </div>