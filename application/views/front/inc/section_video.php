<!--
    |*******************************************************************************************
    | SECTION VIDEO
    |*******************************************************************************************
-->
<?php // if($section['video_type'] == 'youtube' || $section['video_type'] == 'loop'){  ?>
<div id="sec_id_<?php echo $section['code'] ?>" class="section cont st st-list st-video mg  gen_margin <?php echo $section['custom_class'] != '' ?  customClass($section['custom_class']) : '' ?> "
    data-mg-desktop="<?php echo ar_decodeFront($section['mg_desktop'],'margin');?>"
    data-mg-mobile="<?php echo ar_decodeFront($section['mg_mobile'],'margin');?>">
    <div class=" bg-fullwidth fw   gen_bg   <?php echo getBackgroundSettingClass($section['bg_position_ver'],$section['bg_position_hoz'],$section['bg_repeat'],$section['bg_fixed'], $section['bg_size']);?>"
        data-bg-desktop="<?php echo $section['bg1_desktop'];  ?>"
        data-bg-mobile="<?php echo $section['bg1_mobile'];  ?>" data-bg-color="<?php echo $section['bg_color']; ?>">
        <div
            class="container <?php echo $section['section_layout'] == "fullwidth" ? 'container-fluid' : 'container-1600' ?> ">
            <div class="position-relative fw gen_padding pd"
                data-pd-desktop="<?php echo ar_decodeFront($section['pd_desktop'],'padding');?>"
                data-pd-mobile="<?php echo ar_decodeFront($section['pd_mobile'],'padding');?>">
                <div class="cont-contain ml-0 mr-0 w-100">
                    <?php include('inc_topic.php') ?>
                    <!-- /////// ELEMENT //////  -->
                    <?php include('inc_graphic_section.php') ?>
                    <?php include('inc_graphic.php') ?>
                    <!-- /////// ELEMENT //////  -->
                    <div class="box-video">
                        <?php if($section['video_type'] == 'loop'){  ?>
                        <div class="ganeral-video ">
                            <div class="container-video-loop  click-to-play-video-loop" onclick="playVideo_mp4($(this))" >
                                <video class="video-container video-loop"  controls controlsList="nodownload"
                                    oncontextmenu="return false;">
                                    <source
                                        src="<?php echo isset( $section['video_file'] ) ? upload_path($section['video_file']) : ''; ?>"
                                        type="video/mp4">
                                </video>
                                <div class="video-loop-space  ">
                                    <img class=" float-left w-100 d-md-block d-none " src="<?php  echo isset( $section['video_thumb'] ) ? upload_path($section['video_thumb']) : ''; ?>">
                                    <img class=" float-left w-100 d-md-none d-block " src="<?php  echo isset( $section['video_thumb_m'] ) ? upload_path($section['video_thumb_m']) : ''; ?>">

                                </div>
                                <div class="videoplay-icon"><img class="" src="/assets/img/xr/button-play.png"> </div>
                            </div>
                        </div>
                        <?php }elseif($section['video_type'] == 'youtube'){ ?>
                            <?php if($section['video_thumb'] != ""){ ?>
                                <div class="ganeral-video ">
                                    <div class="video-container inactive-state click-to-play-video "
                                        data-video="<?php echo $section['video_id'] ?>" data-code="<?php echo $section['id']?>"
                                        section_type="<?php echo $section['section_type'] ?>" onclick="playVideo($(this))">
                                        <div class="video-put">
                                            <div id="player_ganeral_<?php echo $section['id'] ?>_<?php echo $section['section_type'] ?>"
                                                class="player_ganeral">
                                            </div>
                                        </div>
                                        <div class="video-space">
                                            <img class=" float-left w-100 d-md-block d-none " src="<?php echo isset( $section['video_thumb'] ) ? upload_path($section['video_thumb']) : ''; ?>">
                                            <img class=" float-left w-100 d-md-none d-block " src="<?php echo isset( $section['video_thumb_m'] ) ? upload_path($section['video_thumb_m']) : ''; ?>">
                                        </div>
                                        <?php if( $section['video_id'] != '' ){ ?>
                                        <div class="videoplay-icon"><img class="" src="/assets/img/xr/button-play.png"> </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php }else{ ?>
                                <?php if($section['video_id'] != ""){ ?>
                                <div class="ganeral-video">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $section['video_id'] ?>"></iframe>
                                    </div>
                                </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>