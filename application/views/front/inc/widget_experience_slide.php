<?php if( sizeof($experience_slide_list) > 0 ) { ?>
<div class="sec-experienceSlide">
    <div class="box-experienceSlide bg-gdcolor">
        <div class="inner-experience position-relative">
            <div class="genSlide " data-perview-d="5" data-perview-t="4" data-perview-m="2" data-space-xl="60" data-space-d="40"  data-space-t="40" data-space-m="20" data-percol-d="1" data-percol-t="1" data-percol-m="2">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php 
                        // $img_name = ['experince-icon-financial.png','experince-icon-retail.png','experince-icon-healthcare.png','experince-icon-industrial.png','experince-icon-property.png','experince-icon-technology.png','experince-icon-goverment.png','experince-icon-education.png','experince-icon-5gexperiences.png'];
                        // $title_name = ['ธุรกิจการเงิน','ธุรกิจค้าปลีก','ธุรกิจความงามและสุขภาพ','ธุรกิจอุตสาหกรรม','ธุรกิจอสังหาริมทรัพย์','ธุรกิจเทคโนโลยี','งานราชการ','การศึกษา','ประสบการณ์ 5G'];
                        foreach ($experience_slide_list as $key => $list) {
                        ?>
                        <div class="swiper-slide">
                            <div class="box-thumb fw position-relative text-left">
                                <a href="<?php echo base_url().$lang_path.'/experience/'.$list['slug']; ?>">
                                <div class="thumb thumb-ver">
                                    <div class="thumb-group">
                                        <div class="thumb-img"><img src="<?php echo $list['thumb_image'] != ''  ? upload_path($list['thumb_image']) : '' ; ?>" class="w-100 float-left"></div>
                                        <div class="thumb-txt text-center">
                                            <div class="thumb-txt-h"><b><span><?php echo $list['title']; ?></span></b></div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>    
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <!-- <div class="swiper-pagination"></div> -->
                <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></div>
                <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/arrow-left-1.svg" class="svg"></div>
            </div>
        </div>
    </div>
</div>
<?php } ?>