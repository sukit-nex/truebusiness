<!--
    |*******************************************************************************************
    | SECTION CONTENT
    |*******************************************************************************************
    -->

<?php if($section['content_layout'] == 'hoz_left' || $section['content_layout'] == 'hoz_right' || $section['content_layout'] == 'vertical' || $section['content_layout'] == 'content_first' ){ ?>
<div id="sec_id_<?php echo $section['code'] ?>" class="section cont st st-content gen_margin <?php echo $section['custom_class'] != '' ?  customClass($section['custom_class']) : '' ?>   contlay-<?php echo contentLayout( $section['content_layout'] ); ?> mg "
    data-mg-desktop="<?php echo ar_decodeFront($section['mg_desktop'],'margin');?>"
    data-mg-mobile="<?php echo ar_decodeFront($section['mg_mobile'],'margin');?>">
    <div class="bg-fullwidth fw gen_bg <?php echo getBackgroundSettingClass($section['bg_position_ver'],$section['bg_position_hoz'],$section['bg_repeat'],$section['bg_fixed'], $section['bg_size']);?>"
        data-bg-desktop="<?php echo $section['bg1_desktop'];  ?>"
        data-bg-mobile="<?php echo $section['bg1_mobile'];  ?>" data-bg-color="<?php echo $section['bg_color']; ?>">

        <?php include('inc_graphic_section.php');?>
        <div class="container  <?php echo $section['section_layout'] == "fullwidth" ? 'container-fluid' : 'container-1600' ?> 
            <?php  echo $section['gen_height'] != 0 ? getSecPositionVer($section['sec_content_position_ver']) : ''; ?> 
            <?php echo $section['gen_height'] == 0 ? '' : 'gen-height' ?>"
            data-image-desktop="<?php echo $section['bg1_desktop'] != ''  ? upload_path($section['bg1_desktop']) : '/assets/img/skin/blank.png' ; ?> "
            data-image-mobile="<?php echo $section['bg1_mobile'] != ''  ? upload_path($section['bg1_mobile']) : '' ; ?> "
            data-column="<?php echo sizeof($row['columns'] ); ?>">
            <div class="sec-contain position-relative fw gen_padding pd"
                data-pd-desktop="<?php echo ar_decodeFront($section['pd_desktop'],'padding');?>"
                data-pd-mobile="<?php echo ar_decodeFront($section['pd_mobile'],'padding');?>">
                <?php if($section['title'] != '' || $section['sub_title'] != ''){ ?>
                <div class="box-content sec-title">
                    <?php if($section['title'] != ''){ ?><h1 class="font-weight-bold"><?php echo  $section['title'] ?>
                    </h1><?php } ?>
                    <?php if($section['sub_title'] != ''){ ?><h4 class="font-weight-bold">
                        <?php echo $section['sub_title'] ?></h4><?php } ?>
                </div>
                <?php } ?>
                <div
                    class="cont-contain <?php  echo getSecPositionHoz( $section['sec_content_position_hoz']); ?> <?php  echo $section['gen_height'] == 0 ?  getSecPositionVer( $section['sec_content_position_ver']) : '' ?>  ">
                    <?php if($section['content_layout'] == 'vertical'){ ?>
                    <div class="cont-img">
                        <?php if($section['img1_desktop'] != ''){ ?>
                        <img src="<?php echo $section['img1_desktop'] != '' ? upload_path($section['img1_desktop']) : ''; ?>">
                        <?php }?>
                        <?php include('inc_graphic.php');?>
                    </div>
                    <div class="cont-txt ">
                        <?php echo  $section['content'] ?>
                    </div>
                    <?php }  ?>
                    
                    <?php if($section['content_layout'] == 'content_first'){ ?>
                        <div class="cont-txt ">
                            <?php echo  $section['content'] ?>
                        </div>
                        <div class="cont-img">
                            <img 
                                src="<?php echo $section['img1_desktop'] != '' ? upload_path($section['img1_desktop']) : ''; ?>">
                            <?php include('inc_graphic.php');?>
                        </div>
                    <?php }  ?>
                    
                    <?php if( ( $section['m_content_layout'] == 'image_first') && ( $section['content_layout'] == 'hoz_left' || $section['content_layout'] == 'hoz_right' ) ){ ?>
                    <div class="cont-img <?php echo $section['content_layout'] == 'hoz_right' ? 'order-md-2' : ''; ?>">
                        <img
                            src="<?php echo $section['img1_desktop'] != '' ? upload_path($section['img1_desktop']) : ''; ?>">
                        <?php include('inc_graphic.php');?>
                    </div>
                    <div class="cont-txt <?php echo $section['content_layout'] == 'hoz_right' ? 'order-first' : ''; ?>">
                        <?php echo  $section['content'] ?>
                    </div>
                    <?php } ?>
                    <?php if( ( $section['m_content_layout'] == 'content_first' ) && ( $section['content_layout'] == 'hoz_left' || $section['content_layout'] == 'hoz_right' ) ){ ?>
                    <div class="cont-txt <?php echo $section['content_layout'] == 'hoz_left' ? 'order-md-2' : ''; ?>">
                        <?php echo  $section['content'] ?>
                    </div>
                    <div class="cont-img <?php echo $section['content_layout'] == 'hoz_left' ? 'order-first' : ''; ?>">
                        <img 
                            src="<?php echo $section['img1_desktop'] != '' ? upload_path($section['img1_desktop']) : ''; ?>">
                        <?php include('inc_graphic.php');?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>