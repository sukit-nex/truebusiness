
<?php if( sizeof($solution_business_type_list) > 0 ) { ?>
<div class="sec-experience-business">
    <div class="container">
        <div class="row row-cols-2 row-cols-md-4 row-cols-lg-6 ">
        <?php 
        // $img_name = ['experince-icon-financial.png','experince-icon-retail.png','experince-icon-healthcare.png','experince-icon-industrial.png','experince-icon-property.png','experince-icon-technology.png','experince-icon-goverment.png','experince-icon-education.png','experince-icon-5gexperiences.png'];
        // $title_name = ['ธุรกิจการเงิน','ธุรกิจค้าปลีก','ธุรกิจความงามและสุขภาพ','ธุรกิจอุตสาหกรรม','ธุรกิจอสังหาริมทรัพย์','ธุรกิจเทคโนโลยี','งานราชการ','การศึกษา','ประสบการณ์ 5G'];
        // for($i=0;$i<17;$i++){ 
            foreach ($solution_business_type_list as $key => $list) {
        ?>
            <div class="col">
                <div class="box-thumb fw position-relative text-left">
                    <div class="thumb thumb-ver">
                        <div class="thumb-group">
                            <div class="thumb-img"><img src="<?php echo $list['icon_image'] != ''  ? upload_path($list['icon_image']) : '' ; ?>" class="w-100 float-left"></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
</div>
<img src="/assets/img/element/ele-fluid-left-bottom.png"  class="abs ele-exp-bt"alt="">
<?php } ?>