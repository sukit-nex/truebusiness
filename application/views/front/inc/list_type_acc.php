<?php if($section['design_template'] == 'vertical' || $section['design_template'] == 'horizontal' || $section['design_template'] == 'text_in_card' ){ ?>
                    <?php if($section['tab_show'] == "default"){ ?>

                        <div class="cont-contain ml-0 mr-0 w-100">
                            <?php if($section['title'] != '' || $section['sub_title'] != '' || $section['content'] != ''){ ?>
                                <div class="sec-title <?php echo getSecPositionHoz( $section['sec_title_alignment'] )?>">
                                    <div class="box-content">
                                        <?php if($section['title'] != ''){ ?>
                                            <h1 class="font-weight-bold"><?php echo  $section['title'] ?></h1>
                                        <?php } ?>
                                        <?php if($section['sub_title'] != ''){ ?><h4 class="font-weight-bold">
                                            <?php echo $section['sub_title'] ?></h4><?php } ?>
                                    </div>
                                    <?php if($section['content'] != '' ){ ?>
                                        <div class="sec-list-content">
                                            <?php echo $section['content']; ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php   if(sizeof($section['categories']) > 1){ ?>
                            <div class="sec-acc-group position-relative fw">
                                <?php if($section['acc_type'] == 0){ ?>
                                <?php } ?>
                                <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                                    
                                        <?php //if($section['tab_setting'] == 'only_has'){ ?>
                                        <?php if($cate_key > 0){ ?>
                                            <?php if(sizeof($section['categories']) > 2 && $cate_key > 0){ ?>
                                                <?php if( $category['title'] != '' || $category['sub_title'] != ''){ ?>
                                                    <div class="sec-list-cate">
                                                        <div class="box-content <?php echo getSecPositionHoz( $section['cate_title_alignment'] )?>">
                                                            <?php if($category['title'] != ''){ ?>
                                                                <h4 class="font-weight-bold"><?php echo  $category['title'] ?></h4>
                                                            <?php } ?>
                                                            <?php if($category['sub_title'] != ''){ ?>
                                                                <p class=""><?php echo  $category['sub_title'] ?></p>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="clearfix"></div>
                                            <?php } ?>
                                            <div class="tab-content fw">
                                                <div class="fw">
                                                    <div class="box-accordion fw mb-4">
                                                        <?php foreach( $category['listings'] as $list_key => $listing){ ?>
                                                            <div class="accor-list <?php echo $section['acc_type'] == 0 ? '' : 'default'?> <?php echo $list_key == 0 && $section['acc_switch'] == 0 ? 'accor-list-'.$section['acc_switch'] : '' ?> ">
                                                                
                                                                <div class="accor-head">
                                                                    <?php if($section['acc_type'] == 0){ ?>
                                                                        <div class="accor-icon box-content">
                                                                            <p><strong>Q</strong></p>
                                                                        </div>
                                                                    <?php } ?>
                                                                    <div class="accor-head-topic box-content">
                                                                        <p><span><strong><?php echo $listing['title'] ?></strong></span>
                                                                        <p>
                                                                    </div>
                                                                    <div class="accor-head-symbol"></div>
                                                                </div>
                                                                
                                                                <div class="accor-body">
                                                                    <?php if($section['acc_type'] == 0){ ?>
                                                                        <div class="accor-icon box-content">
                                                                            <p><strong>A</strong></p>
                                                                        </div>
                                                                    <?php } ?>
                                                                        <div class="accor-body-content">
                                                                            <?php echo $listing['content'] ?>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php //} ?>
                                <?php } ?>
                            </div>
                            
                            <?php if($section['tab_setting'] == 'all_content'){ ?>
                                        
                                <?php if($section['btn_more'] == 'btn_more_none'){ ?>
                                        
                                <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                                    <?php if($section['link_type'] == 'no'){ ?>
                                    
                                    <?php }else if($section['link_type'] == 'external_link'){  ?>
                                        <div class="box-content text-center float-left w-100 mt-5 position-relative btn-list-more"  style="z-index:10;">
                                            <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                        </div>
                                    <?php }else if($section['link_type'] == 'internal_link'){  ?>
                                        <div class="box-content text-center float-left w-100 mt-5 position-relative btn-list-more"  style="z-index:10;">
                                            <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                            <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                    
                                        </div>
                                    <?php } ?>
                                <?php }else if($section['btn_more'] == 'btn_load_more'){  ?>  
                                    
                                <?php } ?>
                        
                            <?php }else if($section['tab_setting'] == 'only_has'){ ?>
                                <?php if($cate_key > 0){ ?>
                                    <?php if($section['btn_more'] == 'btn_more_none'){ ?>
                                        
                                    <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                                        <?php if($section['link_type'] == 'no'){ ?>
                                        
                                        <?php }else if($section['link_type'] == 'external_link'){  ?>
                                            <div class="box-content text-center float-left w-100 mt-5 position-relative btn-list-more"  style="z-index:10;">
                                                <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                            </div>
                                        <?php }else if($section['link_type'] == 'internal_link'){  ?>
                                            <div class="box-content text-center float-left w-100 mt-5 position-relative btn-list-more"  style="z-index:10;">
                                                <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                                <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                        
                                            </div>
                                        <?php } ?>
                                    <?php }else if($section['btn_more'] == 'btn_load_more'){  ?>  
                                        
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <?php } ?>
                        </div>

                    <?php }else if($section['tab_show'] == "tabs"){ ?>
                        <div class="cont-contain ml-0 mr-0 w-100">
                            <?php if($section['title'] != '' || $section['sub_title'] != '' || $section['content'] != ''){ ?>
                                <div class="sec-title <?php echo getSecPositionHoz( $section['sec_title_alignment'] )?>">
                                    <div class="box-content">
                                        <?php if($section['title'] != ''){ ?>
                                            <h1 class="font-weight-bold"><?php echo  $section['title'] ?></h1>
                                        <?php } ?>
                                        <?php if($section['sub_title'] != ''){ ?><h4 class="font-weight-bold">
                                            <?php echo $section['sub_title'] ?></h4><?php } ?>
                                    </div>
                                    <?php if($section['content'] != '' ){ ?>
                                        <?php echo $section['content'] ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php   if(sizeof($section['categories']) > 1){ ?>
                            <div class="box-tab tab-style-1 fw">
                                <div class="tab-nav  <?php echo getSecPositionHoz( $section['sec_title_alignment'] )?>">
                                    <ul>
                                        <?php if($section['tab_setting'] == 'all_content'){  ?>

                                        <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                                        <?php if($cate_key == 0){ ?>
                                        <li class="tab-nav-list"><a href="javascript:void(0)" data-tab="<?php echo $cate_key ?>"
                                                class="tab-button box-content">All</a></li>
                                        <?php }else{ ?>
                                        <li class="tab-nav-list"><a href="javascript:void(0)"
                                                data-tab="<?php echo $cate_key; ?>"
                                                class="tab-button box-content"><?php echo  $category['title'] ?></a></li>
                                        <?php } ?>
                                        <?php } ?>

                                        <?php }else if($section['tab_setting'] == 'only_has'){ ?>
                                        <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                                        <?php if($cate_key != 0){ ?>
                                        <li class="tab-nav-list"><a href="javascript:void(0)"
                                                data-tab="<?php echo $cate_key; ?>"
                                                class="tab-button box-content"><?php echo  $category['title'] ?></a></li>
                                        <?php } ?>

                                        <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </div>
                                
                                <div class="sec-acc-group position-relative fw">
                                    <?php if($section['acc_type'] == 0){ ?>
                                    <?php } ?>
                                    <div class="tab-content fw">
                                        <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                                            <div class="tab-content-list fw nav-link-<?php echo $cate_key ?>" style="display:none;">
                                              
                                                <div class="box-accordion fw">
                                                    <?php foreach( $category['listings'] as $list_key => $listing){ ?>
                                                        <div class="accor-list <?php echo $section['acc_type'] == 0 ? '' : 'default'?> <?php echo $list_key == 0 && $section['acc_switch'] == 0 ? 'accor-list-'.$section['acc_switch'] : '' ?>">
                                                        <div class="accor-head">
                                                                    <?php if($section['acc_type'] == 0){ ?>
                                                                        <div class="accor-icon box-content">
                                                                            <p><strong>Q</strong></p>
                                                                        </div>
                                                                    <?php } ?>
                                                                    <div class="accor-head-topic box-content">
                                                                        <p><span><strong><?php echo $listing['title'] ?></strong></span>
                                                                        <p>
                                                                    </div>
                                                                    <div class="accor-head-symbol"></div>
                                                                </div>
                                                                
                                                                <div class="accor-body">
                                                                    <?php if($section['acc_type'] == 0){ ?>
                                                                        <div class="accor-icon box-content">
                                                                            <p><strong>A</strong></p>
                                                                        </div>
                                                                    <?php } ?>
                                                                        <div class="accor-body-content">
                                                                            <?php echo $listing['content'] ?>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php if($section['tab_setting'] == 'all_content'){ ?>  
                                <?php if($section['btn_more'] == 'btn_more_none'){ ?>
                                        
                                <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                                    <?php if($section['link_type'] == 'no'){ ?>
                                    
                                    <?php }else if($section['link_type'] == 'external_link'){  ?>
                                        <div class="box-content text-center float-left w-100 mt-5 position-relative btn-list-more"  style="z-index:10;">
                                            <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                        </div>
                                    <?php }else if($section['link_type'] == 'internal_link'){  ?>
                                        <div class="box-content text-center float-left w-100 mt-5 position-relative btn-list-more"  style="z-index:10;">
                                            <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                            <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                    
                                        </div>
                                    <?php } ?>
                                <?php }else if($section['btn_more'] == 'btn_load_more'){  ?>  
                                    
                                <?php } ?>
                        
                            <?php }else if($section['tab_setting'] == 'only_has'){ ?>
                                <?php if($cate_key > 0){ ?>
                                    <?php if($section['btn_more'] == 'btn_more_none'){ ?>
                                        
                                    <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                                        <?php if($section['link_type'] == 'no'){ ?>
                                        
                                        <?php }else if($section['link_type'] == 'external_link'){  ?>
                                            <div class="box-content text-center float-left w-100 mt-5 position-relative btn-list-more"  style="z-index:10;">
                                                <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                            </div>
                                        <?php }else if($section['link_type'] == 'internal_link'){  ?>
                                            <div class="box-content text-center float-left w-100 mt-5 position-relative btn-list-more"  style="z-index:10;">
                                                <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                                <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                        
                                            </div>
                                        <?php } ?>
                                    <?php }else if($section['btn_more'] == 'btn_load_more'){  ?>  
                                        
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    <?php }else if($section['tab_show'] == "merge"){ ?>

                        <div class="cont-contain ml-0 mr-0 w-100">
                        <?php if($section['title'] != '' || $section['sub_title'] != '' || $section['content'] != ''){ ?>
                                <div class="sec-title <?php echo getSecPositionHoz( $section['sec_title_alignment'] )?>">
                                    <div class="box-content">
                                        <?php if($section['title'] != ''){ ?>
                                            <h1 class="font-weight-bold"><?php echo  $section['title'] ?></h1>
                                        <?php } ?>
                                        <?php if($section['sub_title'] != ''){ ?><h4 class="font-weight-bold">
                                            <?php echo $section['sub_title'] ?></h4><?php } ?>
                                    </div>
                                    <?php if($section['content'] != '' ){ ?>
                                        <?php echo $section['content'] ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php   if(sizeof($section['categories']) > 1){ ?>
                            <div class="sec-acc-group position-relative fw">
                                <?php if($section['acc_type'] == 0){ ?>
                                <?php } ?>
                                <?php foreach( $section['categories'] as $cate_key => $category){ ?>
                                    <?php if($cate_key == 0){ ?>
                                        <div class="tab-content fw">
                                            <div class="fw">
                                                <div class="box-accordion fw ">
                                                    <?php foreach( $category['listings'] as $list_key => $listing){ ?>
                                                        <div class="accor-list <?php echo $section['acc_type'] == 0 ? '' : 'default'?>  <?php echo $list_key == 0 && $section['acc_switch'] == 0 ? 'accor-list-'.$section['acc_switch'] : '' ?>">
                                                        <div class="accor-head">
                                                                    <?php if($section['acc_type'] == 0){ ?>
                                                                        <div class="accor-icon box-content">
                                                                            <p><strong>Q</strong></p>
                                                                        </div>
                                                                    <?php } ?>
                                                                    <div class="accor-head-topic box-content">
                                                                        <p><span><strong><?php echo $listing['title'] ?></strong></span>
                                                                        <p>
                                                                    </div>
                                                                    <div class="accor-head-symbol"></div>
                                                                </div>
                                                                
                                                                <div class="accor-body">
                                                                    <?php if($section['acc_type'] == 0){ ?>
                                                                        <div class="accor-icon box-content">
                                                                            <p><strong>A</strong></p>
                                                                        </div>
                                                                    <?php } ?>
                                                                        <div class="accor-body-content">
                                                                            <?php echo $listing['content'] ?>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <?php if($section['btn_more'] == 'btn_more_none'){ ?>
                                                        
                            <?php }else if($section['btn_more'] == 'btn_more_show'){  ?>
                                <?php if($section['link_type'] == 'no'){ ?>
                                
                                <?php }else if($section['link_type'] == 'external_link'){  ?>
                                    <div class="box-content text-center float-left w-100 mt-5 position-relative btn-list-more"  style="z-index:10;">
                                        <a target="_blank"  href="<?php echo $section['link_out'] == '' ? 'javascript:void(0);' : $section['link_out'] ?>" class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                    </div>
                                <?php }else if($section['link_type'] == 'internal_link'){  ?>
                                    <div class="box-content text-center float-left w-100 mt-5 position-relative btn-list-more"  style="z-index:10;">
                                        <?php  $uri = getSlug( $section['page_internal_code'] , $pages ); ?>
                                        <a href="<?php echo $section['page_internal_code'] == '' ? 'javascript:void(0);' : base_url().$lang_path.'/'.$uri ?>"  class="btn btn-skew btn-loadmore" ><?php echo $section['btn_text'] == '' ? 'ดูเพิ่มเติม' : $section['btn_text'] ?></span></a>
                                    </div>
                                <?php } ?>
                            <?php }else if($section['btn_more'] == 'btn_load_more'){  ?>  
                                
                            <?php } ?>
                            <?php } ?>
                        </div>

                    <?php } ?>
                <?php } ?>