
    <?php foreach($lists as $list_key => $list){  ?>
        <!-- Content normal -->
        
        <!-- End Content normal -->

        <div class="col">
            <div class="<?php echo $list['design_template'] == 'table' ? 'box-table' : 'box-thumb' ?>  fw position-relative <?php echo getSecPositionHoz( $list['listing_alignment'] )?> slide-in-bottom ">
            <?php if($list['link_type'] != 'no' && $list['design_template'] != 'table' ){?>
            <?php if($list['btn_show'] == 0){?>
                <a <?php echo $list['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $list['link_type'],$list['link_out'],$list['cate_slug'],$list['slug'],$list['page_slug']   )?>">
            <?php } ?>
            <?php } ?>
                <?php if($list['design_template'] == 'vertical' || $list['design_template'] == 'horizontal' || $list['design_template'] == 'text_in_card' ){ ?>
                    <div
                        class="thumb thumb-<?php echo designTemplate( $list['design_template'] )?>">
                        <div class="thumb-group">
                            <?php if($list['image'] != ""){ ?>
                            <div class="thumb-img">
                                <img src="<?php echo  $list['image']  != ''  ? upload_path($list['image']) : ''; ?>"
                                    class="w-100 float-left">
                            </div>
                            <?php } ?>
                            <div class="thumb-txt <?php echo getSecPositionHoz( $list['listing_alignment'] )?>">
                                <?php if($list['title'] != ''){ ?>
                                    <div class="thumb-txt-h"><?php echo $list['title'] ?></div>
                                <?php } ?>
                                <?php if($list['sub_title'] != ''){ ?>
                                    <div class="thumb-txt-p"><?php echo $list['sub_title'] ?></div>
                                <?php } ?>
                                <?php if($list['price'] != ''){ ?>
                                <div class="thumb-txt-price"><?php echo $list['price'] ?></div>
                                <?php } ?>
                                <?php if( $list['design_template'] != "vertical"){ ?>
                                    <?php if( $list['link_type'] != "no"){ ?>
                                        <?php if($list['btn_show'] == 1) { ?>
                                            <div class="box-content <?php echo getSecPositionHoz( $list['listing_alignment'] )?>">
                                                <a class="btn btn-sm-incard" <?php echo $list['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $list['link_type'],$list['link_out'],$list['cate_slug'],$list['slug'],$list['page_slug']   )?>"><?php echo $list['btn_text'] != " " ? $list['btn_text'] : 'ดูเพิ่มเติมw' ?></a>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php }else if($list['design_template'] == 'table' ){ ?>
                    <div class="thumb">
                        <div class="thumb-group">
                            <div class="thumb-txt <?php echo getSecPositionHoz( $list['listing_alignment'] )?>">
                                <?php echo $list['content'] ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if($list['link_type'] != 'no' && $list['design_template'] != 'table' ){?>
                <?php if($list['btn_show'] == 0){?>
                    </a>
                <?php } ?>
                <?php } ?>
                <?php if(isset($list['is_tags'])){ ?>
                    <?php if($list['is_tags'] == 1){ ?>
                        <div class="thumb-tag">
                            <?php if( $list['cate_title'] != ""){ ?>
                                <a class="tag-cate" href="javascript:void(0);"><?php echo  $list['cate_title'] ?></a>
                            <?php } ?>
                            <?php $ar_tag = explode( ",", $list['tags'] ); ?>
                            <?php if( sizeof( $ar_tag ) > 0 ){ ?>
                                <?php foreach( $ar_tag as $tag_key => $tag ){ ?>
                                    <?php if( $tag != "" & $tag_key < 2 ){ ?>
                                        <a href="<?php echo base_url().$lang_path.'/'.'search?tag='.$tag; ?>"><?php echo $tag; ?></a>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php } ?>
                <?php if( $list['design_template'] == "vertical"){ ?>
                    <?php if( $list['link_type'] != "no"){ ?>
                        <?php if($list['btn_show'] == 1) { ?>
                            <div class="box-content <?php echo getSecPositionHoz( $list['listing_alignment'] )?>">
                                <a class="btn btn-sm-incard" <?php echo $list['link_type'] == 'blank' ? "target='_blank'" : '' ?> href="<?php  echo linkType( $list['link_type'],$list['link_out'],$list['cate_slug'],$list['slug'],$list['page_slug']   )?>"><?php echo $list['btn_text'] != " " ? $list['btn_text'] : 'ดูเพิ่มเติม1' ?></a>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
