<div class="loadpage"></div>
<?php // print_r($cate_detail); ?>

<?php // print_r($solution_content_detail); ?>

<?php //print_r($experience_content_detail); ?>

<?php //print_r($solution_cate_more); ?>

<div class="wrapper">
    <?php if ( sizeof($cate_detail) > 0 ) { ?>
    <div class="section cont bg-fullwidth st st-content contlay-hoz-left banner-skew-custom ">
        <div class="bg-fullwidth fw  gen_bg" data-bg-desktop="<?php echo $cate_detail['banner_desktop_image'];  ?>" data-bg-mobile="<?php echo $cate_detail['banner_mobile_image'];  ?>" >
            <div class="container container-1600 gen-height text-md-middle text-bottom"  data-image-desktop="<?php echo $cate_detail['banner_desktop_image'] != ''  ? upload_path($cate_detail['banner_desktop_image']) : '/assets/img/skin/blank.png' ; ?> "
            data-image-mobile="<?php echo $cate_detail['banner_mobile_image'] != ''  ? upload_path($cate_detail['banner_mobile_image']) : '' ; ?>" data-column="1">
                <div class="position-relative fw">
                    <div class="cont-contain pd ">
                        <!-- <div class="cont-img">
                            <img src="/assets/img/ar/sec-2-img.png">
                        </div> -->
                        
                        <div class="cont-txt text-left">
                            <div class="row">
                                <div class="col col-12">
                                    <div class="box-content"> 
                                        <h2 class="txt-gdcolor"><b><u><?php echo $cate_detail['title']; ?></u></b></h2>
                                        <h5><b><?php echo $cate_detail['sub_title']; ?></b></h5>
                                        <p><?php echo $cate_detail['description']; ?></p>
                                        <?php if( isset($cate_detail['brochure_pdf'] ) ){ ?>
                                        <a href="<?php echo upload_path().'/'.$cate_detail['brochure_pdf']  ?>"target="_blank"  class="btn btn-text btn-icon mt-3 btn-download"><span><img src="/assets/img/skin/icon-download.svg" class="svg"><span><b>ดาวน์โหลดโบรชัวร์</b></span></span></a>

                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

    <?php if(sizeof($solution_content_detail) > 0){ ?>
    <div id="sec_id_3" class="section cont mg st st-list gen_margin  solution-thumb-ver  " data-mg-desktop="" data-mg-mobile="" data-slidecol="1" data-slidepercolumn="1" style="">
        <div class="bg-fullwidth fw gen_bg bg-py-t bg-px-c bg-rp-no bg-fx-st bg-sz-cv" data-bg-desktop="" data-bg-mobile="" data-bg-color="" style="">
          
            <div class="container container-1600  " data-image-desktop="/assets/img/skin/blank.png" data-image-mobile="" data-column="1">
                <div class="position-relative fw gen_padding pd" data-pd-desktop="" data-pd-mobile="" style="">
                    <div class="cont-contain ml-0 mr-0 w-100">
                        <div class="sec-title text-center" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                            <div class="box-content">
                                <h2 class="font-weight-bold"><span class="txt-gdcolor">ทำความรู้จัก<br class="d-md-none d-block">โซลูชันในกลุ่มนี้</span></h2>
                                <div class="title-sub">ผลักดันองค์กรของคุณสู่ความสำเร็จ</div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <?php foreach($solution_content_detail as $solution_detail){  ?>
                                <div class="col col-12 col-md-6 col-lg-4">
                                    <div class="box-thumb fw position-relative text-left">
                                        <a class="position-relative zindex-1" href="<?php echo base_url().$lang_path.'/solution/'.$cate_detail['slug'].'/'.$solution_detail['slug'];?>">
                                        <div class="thumb thumb-ver">
                                            <div class="thumb-group">
                                                <div class="thumb-img"><img src="/assets/img/template/image-default.jpg" class="fw"></div>
                                                <div class="thumb-txt text-left">
                                                    <div class="thumb-txt-h txt-gdcolor"><b><?php echo $solution_detail['title']; ?></b></div>
                                                    <div class="thumb-txt-p"><?php echo $solution_detail['description']; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        </a>
                                        <div class="thumb-ele-buble"><img src="/assets/img/skin/ele-buble-thumb.png" alt=""></div>
                                    </div>    
                                </div>
                            <?php } ?>
                        </div>
                        
                    </div>
                </div>
            </div>
            <img class="ele_img-skew abs d-md-block d-none" src="/assets/img/skin/ele-skew-img-d.png" alt="">
        </div>
    </div>
    <?php } ?>

    <?php if(sizeof($experience_content_detail) > 0){ ?>
    <div id="sec_id_3" class="section cont mg st st-video gen_margin  solution-thumb-ver  " data-mg-desktop="" data-mg-mobile="" data-slidecol="1" data-slidepercolumn="1" style="">
        <div class="bg-fullwidth fw gen_bg bg-py-t bg-px-c bg-rp-no bg-fx-st bg-sz-cv" data-bg-desktop="" data-bg-mobile="" data-bg-color="" style="">
        <img src="/assets/img/element/ele-buble-slt-ttc.png" alt="" class="ele-buble-slt-ttc abs d-md-none d-block">
            <div class="container   " data-image-desktop="/assets/img/skin/blank.png" data-image-mobile="" data-column="1">
                <div class="position-relative fw gen_padding pd" data-pd-desktop="" data-pd-mobile="" style="">
                    
                    <div class="cont-contain ml-0 mr-0 w-100">
                        <div class="sec-title text-center" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                            <div class="box-content">
                                <h2 class="font-weight-bold"><span class="txt-gdcolor">เสียงสะท้อน<br class="d-md-none d-block">ความประทับใจ</span></h2>
                            </div>
                        </div>

                        <div class="sec-solutionVideoSlide">
                            <div class="box-solutionSlide">
                                <div class="genSlide" data-perview-d="1" data-perview-t="1" data-perview-m="1.1" data-space="10" data-space-m="20">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">
                                            <?php foreach($experience_content_detail as $ex_key => $experience_detail){ ?>
                                                <div class="swiper-slide">
                                                    <div class="ganeral-video ">
                                                        <div class="video-container inactive-state click-to-play-video "
                                                            data-video="<?php echo $experience_detail['vdo_youtube_id'] ?>" data-code="<?php echo $ex_key ?>"
                                                            section_type="banner" onclick="playVideo($(this))">
                                                            <div class="video-put">
                                                                <div id="player_ganeral_<?php echo $ex_key ?>_banner"
                                                                    class="player_ganeral">
                                                                </div>
                                                            </div>
                                                            <div class="video-space">
                                                                <img class=" float-left w-100 d-md-block d-none " src="/assets/img/demo/solution-vdo-thumb-mock.jpg">
                                                                <img class=" float-left w-100 d-md-none d-block " src="/assets/img/demo/solution-vdo-thumb-mock.jpg">
                                                            </div>
                                                            <?php if($experience_detail['vdo_youtube_id'] != "") { ?>
                                                                <div class="videoplay-icon"><img class="" src="/assets/img/skin/button-play.png"> </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <!-- <div class="swiper-pagination"></div> -->
                                    <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></div>
                                    <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/arrow-left-1.svg" class="svg"></div>
                                </div>
                            </div>
                        </div>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if(sizeof($solution_cate_more) > 0){ ?>
    <div  class="section cont mg st st-list" >
        <div class="bg-fullwidth fw gen_bg bg-py-t bg-px-c bg-rp-no bg-fx-st bg-sz-cv" >
            
            <div class="container container-fluid ">
                <div class="position-relative fw  pd" >
                   
                    <div class="cont-contain ml-0 mr-0 w-100">
                        <div class="sec-title text-center" >
                            <div class="box-content">
                                <h2 class="font-weight-bold"><span class="txt-gdcolor">โซลูชันอื่นๆ</span></h2>
                                <div class="title-sub">ผลักดันองค์กรของคุณสู่ความสำเร็จ</div>
                            </div>
                        </div>

                        <div class="sec-solutionSlide">
                            <div class="box-solutionSlide">
                                <div class="genSlide" data-perview-xl="5.5" data-perview-d="4.5" data-perview-t="3.5" data-perview-m="1.2" data-space-d="10" data-space-t="10" data-space-m="20" >
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">
                                            <?php 
                                            // $img_name = ['solution-thumb-ver-network.jpg','solution-thumb-ver-voice.jpg','solution-thumb-ver-mobility.jpg','solution-thumb-ver-iot.jpg','solution-thumb-ver-marketing.jpg','solution-thumb-ver-productivity.jpg','solution-thumb-ver-5g-infra.jpg','solution-thumb-ver-cyber-security.jpg','solution-thumb-ver-data.jpg'];
                                            // $title_name = ['Network','Voice & Conferencing','Mobility','5G Infrastructure','IOT and Digital','Marketing','Productivity & Collaboration','Data Center and Cloud','Cyber Security'];
                                            foreach ($solution_cate_more as $key => $list) { ?>
                                            <div class="swiper-slide">
                                                <div class="box-thumb box-shadow fw position-relative text-left">
                                                    <a href="https://true5gdev.digitalnex.com/th/content/1622624239/1622612664">
                                                    <div class="thumb thumb-incard">
                                                        <div class="thumb-group">
                                                            <div class="thumb-img"><img src="/assets/img/demo/<?php echo $list['thumb_image_verticals'] != "" ? $list['thumb_image_verticals'] : '' ?>" class="w-100 float-left"></div>
                                                            <div class="thumb-txt text-left">
                                                                <div class="thumb-txt-h txt-upper"><b><?php echo $list['title']; ?></b></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </a>
                                                </div>    
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <!-- <div class="swiper-pagination"></div> -->
                                    <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></div>
                                    <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/arrow-left-1.svg" class="svg"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img src="/assets/img/element/ele-fluid-bottom-m.png" alt="" class="ele-slt-bt  d-md-none d-block">
        </div>
    </div>
    <?php } ?>
   
</div>