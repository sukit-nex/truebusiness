<div class="loadpage"></div>
<?php //include('inc/inc_header.php');?>
<div class="wrapper">

    <?php foreach( $rows as $row){ ?>
       <div class="row-wrap bg-fullwidth fw gen_bg  <?php echo getBackgroundSettingClass($row['bg_position_ver'],$row['bg_position_hoz'],$row['bg_repeat'],$row['bg_fixed'], $row['bg_size']);?>"
            data-bg-desktop="<?php echo $row['row_bg'];  ?>"
            data-bg-mobile="<?php echo $row['row_bg_m'];  ?>" data-bg-color="<?php echo $row['bg_color']; ?>" style="">
           <div class="container <?php echo $row['is_container'] == 1 ? 'container-1600' : 'container-fluid' ;?>" >
                <div class="row">
                    <?php foreach( $row['columns'] as $column ){ ?>
                        <div class="col col-12 <?php echo sizeof($row['columns'] ) > 1 ? 'col-md-6' : 'col-md-12'; ?> ">
                            <?php $sections = $column['sections']; ?>
                            <?php if(sizeof($sections) > 0){  ?>
                                <?php foreach($sections as $key => $section){ ?>
                                    <?php if($section['section_type'] == 'content'){ ?>
                                        <?php include('inc/section_content.php');?>
                                    <?php }elseif($section['section_type'] == 'listing'){ ?> 
                                        <?php include('inc/section_listing.php');?>
                                    <?php }elseif($section['section_type'] == 'video'){ ?>
                                        <?php include('inc/section_video.php');?> 
                                    <?php }elseif($section['section_type'] == 'banner'){ ?> 
                                        <?php include('inc/section_banner.php');?>
                                    <?php }elseif($section['section_type'] == 'widget'){ ?>
                                        <?php include('inc/section_widget.php');?>
                                    <?php } ?>
                                <?php } ?> 
                            <?php } ?>
                        </div>
                    <?php } ?> 
                </div>
           </div>
       </div>

      

    <?php } ?> 
    <?php if($page == "smepack"){ ?>
        <div class="box-download">
            <a class="btn-downloadmain" href="<?php echo upload_path().'/'.$smepack_brochure_pdf['brochure_pdf'];?>" target="_blank" rel="noopener noreferrer">
                <img src="/assets/img/skin/icon-download.svg" class="svg" alt="">
                <span class="download-txt">
                    <span class="download-txt-sm">ดาวน์โหลดโบชัวร์</span>
                    <span class="download-txt-xl txt-upper t-black">SME<br>Package</span>
                </span>
            </a>  
        </div>
    <?php } ?>

    <?php if($page == "solution"){ ?>
        <div class="box-download">
            <a class="btn-downloadmain" href="<?php echo upload_path().'/'.$solution_brochure['brochure_pdf'];?>" target="_blank" rel="noopener noreferrer">
                <img src="/assets/img/skin/icon-download.svg" class="svg" alt="">
                <span class="download-txt">
                    <span class="download-txt-sm">ดาวน์โหลดโบรชัวร์</span>
                    <span class="download-txt-xl txt-upper t-black">SOLUTIONS</span>
                </span>
            </a>  
        </div>
    <?php } ?>                
    <?php // include('inc/inc_footer_main.php');?>
     
</div>

        