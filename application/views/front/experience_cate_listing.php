<?php 
// print_r($cate_detail);
// print_r($experience_content_detail);
// print_r($experience_slide_list); 
?>
<div class="loadpage"></div>

    
    
<div class="wrapper">
    <div class="box-smedetail-banner section cont bg-fullwidth st st-content contlay-hoz-right">
        <div class="bg-fullwidth fw gen_bg" data-bg-desktop="<?php echo $cate_detail['banner_desktop_image'] != '' ? $cate_detail['banner_desktop_image'] : 'blank-sme-banner-d.jpg' ;  ?>" data-bg-mobile="<?php echo $cate_detail['banner_mobile_image'];  ?>" >
            <div class="container container-1600 gen-height text-md-bottom text-bottom"  
                data-image-desktop="<?php echo $cate_detail['banner_desktop_image'] != ''  ? upload_path($cate_detail['banner_desktop_image']) : '/assets/img/blank/blank-sme-banner-d.jpg' ; ?> "
                data-image-mobile="<?php echo $cate_detail['banner_mobile_image'] != ''  ? upload_path($cate_detail['banner_mobile_image']) : '' ; ?>" data-column="1">
                <div class="position-relative fw">
                    <div class="cont-contain pd">
                        <div class="cont-txt text-center">
                            <div class="smedetail-banner-detail">
                                <div class="box-content light-content ">
                                    <div class="smedetail-banner-topic">
                                        <h1><?php echo $cate_detail['prefix_title'] ?><b><?php echo $cate_detail['title'] ?></b></h1>
                                    </div>
                                    <p><?php echo $cate_detail['sub_title'] ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>

    <div class="sec-cate-info">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col col-12 col-md-12 col-lg-10">
                    <div class="box-content text-center">
                        <h2 class="txt-gdcolor"><b><?php echo $cate_detail['topic_content'] ?></b></h2>
                        <p><?php echo $cate_detail['description'] ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
            
            
        
    <?php if( sizeof($experience_content_detail) > 0 ) { ?>
    <div class="sec-experience-list">
        <div class="container">
            <?php foreach ($experience_content_detail as $key => $list) { ?>
                <div class="experience-list">
                    <div class="row">
                        <div class="col col-12 col-md-6">
                            <div class="experience-list-txt">
                                <div class="experience-list-txt-logo"><img src="<?php echo upload_path($list['logo_image']); ?>" class="w-100 float-left"></div>
                                <div class="experience-list-txt-title"><?php echo $cate_detail['title'] ?></div>
                                <div class="experience-list-txt-detail"><?php echo $cate_detail['description'] ?></div>
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 ">
                            <div class="experience-list-img"><img src="<?php echo upload_path($list['thumb_image']); ?>" class="w-100 float-left"></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>

    <?php if( sizeof($experience_slide_list) > 0 ) { ?>
        <div  class="section cont mg st st-list" >
            <div class="bg-fullwidth fw gen_bg bg-py-t bg-px-c bg-rp-no bg-fx-st bg-sz-cv" >
                <div class="container container-1600 ">
                    <div class="position-relative fw  pd" >
                        <div class="cont-contain ml-0 mr-0 w-100">
                            <div class="sec-title text-center" >
                                <div class="box-content">
                                    <h2 class="font-weight-bold"><span class="txt-gdcolor">ผลงานธุรกิจด้านอื่นๆ</span></h2>
                                </div>
                            </div>
                            <div class="sec-experienceSlide">
                                <div class="box-experienceSlide bg-gdcolor">
                                    <div class="inner-experience position-relative">
                                        <div class="genSlide " data-perview-d="5" data-perview-t="4" data-perview-m="2" data-space-xl="60" data-space-d="40"  data-space-t="40" data-space-m="20" data-percol-d="1" data-percol-t="1" data-percol-m="2">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <?php foreach ($experience_slide_list as $key => $list) { ?>
                                                    <div class="swiper-slide">
                                                        <div class="box-thumb fw position-relative text-left">
                                                            <a href="<?php echo base_url().$lang_path.'/experience/'.$list['slug']; ?>">
                                                            <div class="thumb thumb-ver">
                                                                <div class="thumb-group">
                                                                    <div class="thumb-img"><img src="<?php echo $list['thumb_image'] != ''  ? upload_path($list['thumb_image']) : '' ; ?>" class="w-100 float-left"></div>
                                                                    <div class="thumb-txt text-center">
                                                                        <div class="thumb-txt-h"><b><span><?php echo $list['title']; ?></span></b></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </a>
                                                        </div>    
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <!-- <div class="swiper-pagination"></div> -->
                                            <div class="swiper-button swiper-button-next"><img src="/assets/img/skin/arrow-right-1.svg" class="svg"></div>
                                            <div class="swiper-button swiper-button-prev"><img src="/assets/img/skin/arrow-left-1.svg" class="svg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>