<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preview extends MY_Front_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
		
		/*$user_type = array('SA', 'A', 'W');
		if ( !in_array($this->session->userdata('user_type'), $user_type, true)) {
			redirect('/');
		}*/
		if( !$this->session->userdata('is_admin_login') ){
			redirect('/');
		}
   	}
	public function index()
	{	
		redirect('/');
	}
	public function template( $page_slug, $page_sub_slug = ""){
		$this->data['css'] = "";
		$this->data['js'] = "";
		$this->data['tag'] = $page_slug;
		$this->data['body_class'] = "";
		$this->data['menu_check'] = $page_slug;
        $this->data['sub_menu_check'] = $page_sub_slug;
		$this->data['page'] = $page_slug;
		$this->data['page_active'] = $page_slug;
		$this->data['page_slug'] = $page_slug;
		//$this->data['banners'] = array(); //$this->common->loadBanner();
		$this->data['page_detail'] = $this->common->page_detail($page_slug);
		$this->data['page_seo'] = $this->common->loadSeo($page_slug);
		if($page_sub_slug != ""){
			$this->data['page_detail'] = $this->common->page_detail($page_sub_slug);
			$this->data['page_seo'] = $this->common->loadSeo($page_sub_slug);
		}

		//echo '<pre>';print_r($this->data['page_detail']);echo '</pre>';
		$this->data['is_padding'] = "";
		if( sizeof( $this->data['page_detail'] ) > 0 ){
			$preview_id = isset( $_GET['preview_id'] ) ? $this->input->get('preview_id') : '';
			if ( $preview_id != '' ) {
				$this->data['rows'] = $this->common->loadSections_preview( $this->data['page_detail']['code'], $preview_id );
				if(sizeof($this->data['rows']) > 0){
					foreach( $this->data['rows'] as $row ){
						foreach( $row['columns'] as $column ){
							//echo '<pre>';print_r($column['sections']);echo '</pre>';
							
							foreach( $column['sections'] as $section ){
								if( $section['section_type'] == "widget" && $section['widget'] == "coverage"){
									$this->data['provinces'] = $this->common->loadProvinceList();
									$this->data['coverage'] = $this->common->loadCoverage();
									$this->data['coverage_groups'] = $this->common->loadCoverageGroup();
									$this->data['coverage_search'] = $this->common->loadProvinceDistrict();
								}
							}
						}
					}
				}
				$this->page_construct( 'page_template' , $this->data );
			} else {
				$this->page_construct_nohead( 'errors/error_404' );
			}
		}else{
			$this->page_construct_nohead( 'errors/error_404' );
		}
		
	}

	public function listview( $page_slug = "", $cate_slug = "" , $content_slug = "" ){
		$this->data['css'] = "";
		$this->data['js'] = "";
		$this->data['tag'] = 'content_detail';
		$this->data['body_class'] = "";
		$this->data['menu_check'] = 'content_detail';
        $this->data['sub_menu_check'] = $content_slug;
		$this->data['page'] = 'content_detail';
		$this->data['page_active'] = 'content_detail';
		$this->data['page_slug'] = $page_slug;
		$this->data['page_seo'] = $this->common->loadSeo($content_slug);
		
		$this->data['cate_detail'] = $this->common->cate_detail($cate_slug);
		$this->data['content_detail'] = $this->common->content_detail($content_slug);
		
		$preview_id = isset( $_GET['preview_id'] ) ? $this->input->get('preview_id') : '';
		if ( $preview_id == '' ) {
			redirect('errors/404_override');
		} else {
			$this->data['content_detail'] = $this->common->content_detail_preview($content_slug, $preview_id);
		}

		$this->data['bread_type'] = '';
		if( sizeof( $this->data['cate_detail'] ) == 0 || sizeof( $this->data['content_detail'] ) == 0 ){
			redirect('errors/404_override');
		}else{
			$this->data['page_detail'] = $this->common->page_detail($page_slug);
			if( sizeof( $this->data['page_detail'] ) == 0 ){
				$this->db->where('slug', $page_slug);
				$this->db->where('lang', DEFAULT_LANG );
				$q = $this->db->get('menu');
				$result_menu = $q->result_array();
				if( sizeof( $result_menu ) > 0 ){
					$this->data['bread_type'] = 'slug';
					$this->data['page_detail']['page_name'] = $result_menu[0]['name'];
				}
			}

			//$this->data['page_detail']['page_name'] = $this->data['content_detail']['title'];
			//$this->data['relate_lists'] = $this->common->loadRelateList( $this->data['cate_detail']['code'],$this->data['content_detail']['code'] );
			$this->page_construct( 'listing_detail' , $this->data );
		}
	 }

	 public function search(){
		$this->data['css'] = "";
		$this->data['js'] = "";
		$this->data['tag'] = '';
		$this->data['body_class'] = "";
		$this->data['menu_check'] = 'search';
        $this->data['sub_menu_check'] = 'search';
		$this->data['page'] = 'search';
		$this->data['page_active'] = 'search';

		$this->data['page_detail']['page_name'] = 'search';

		$tag = isset( $_GET['tag'] ) ? $this->input->get('tag') : '';

		if( $tag != '' ){
			$this->data['content_detail'] = $this->common->search_tag( $tag );
			//echo '<pre>';print_r( $this->data['content_detail'] );echo '</pre>';
			$this->page_construct( 'tag_listing' , $this->data );
		}else{
			redirect('errors/404_override');
		}
	 }

}
