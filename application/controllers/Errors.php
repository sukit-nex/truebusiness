<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$this->load->view('front/welcome_message', $this->data);
		//$this->load->view('welcome_message');
	}
  public function nopage()
	{

		$this->load->view('errors/error_404');
		//$this->load->view('welcome_message');
	}
	public function serverError()
	{

		$this->load->view('errors/error_500');
		//$this->load->view('welcome_message');
	}

}
