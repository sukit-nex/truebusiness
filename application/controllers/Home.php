<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Front_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
   	}
	public function index()
	{	
		$this->cache->clean();
		$this->data['page'] = 'index';
		$this->data['page_active'] = 'index';
		$this->data['css'] = 'style-index';
		$this->data['js'] = 'page_index';
		$this->data['tag'] = 'home';
		$body_class = 'index';
		$this->data['body_class'] = $body_class."_page";
		$this->data['page_seo'] = $this->common->loadSeo('index');
		//redirect( '/'.$this->data['lang_path'].'/immersive-experience');
		$this->page_construct( 'index' , $this->data );
	}
	public function pages( $view_name, $js = "", $tab = "" ){
		$view_lower = strtolower( $view_name );
		$this->data['css'] = 'style-'.str_replace("_cms","",$view_lower);
		$this->data['js'] = $js;
		$this->data['tag'] = 'home';
		$body_class = str_replace("_cms","",$view_lower);
		$this->data['body_class'] = $body_class."_page";
		$this->data['page_seo'] = $this->common->loadSeo($body_class);
		$this->data['sections'] = $this->common->loadSection( $body_class,'');
		$this->data['page_footer'] = $this->common->loadFooter( $body_class );
		$this->data['banners'] = $this->common->loadBanner( $body_class );

		$this->data['menu_check'] = $view_lower;
		$this->data['sub_menu_check'] = $view_lower;
		if( $view_lower == "immersive"){
			$this->data['menu_check'] = 'true5g-consumer';
			$this->data['sub_menu_check'] = 'immersive-experience';
		}
        
		$this->data['page'] = $view_lower;
		$this->data['page_active'] = $view_lower;

		$this->data['is_padding'] = "ph1";
		$this->data['page'] = $view_lower;
		$this->data['page_active'] = $view_lower;

		$this->data['page_detail'] = array();
		if( $view_lower == 'immersive'){
			$this->data['page_detail']['page_name'] = 'immersive experience';
		}else{
			$this->data['page_detail']['page_name'] = $view_lower;
		}
		//$this->data['banners'] = array(); //$this->common->loadBanner();
		$this->page_construct( $view_lower , $this->data );
	}
	public function coverage()
	{
		
		$this->page_construct_nohead('front/coverage', $this->data);
	}
	public function coverage_choice()
	{
		$this->page_construct_nohead('front/coverage_choice', $this->data);
	}
	public function coverage_v3()
	{
		$this->page_construct_nohead('front/coverage_v3', $this->data);
	}
	public function solution_detail()
	{
		$this->page_construct('solution_detail', $this->data );
	}
	public function listing_detail()
	{
		$this->page_construct_nohead('front/listing_detail', $this->data);
	}

	// public function solution_detail()
	// {
	// 	$this->page_construct_nohead('front/solution_detail', $this->data);
	// }

	public function tag_listing()
	{
		$this->page_construct_nohead('front/tag_listing', $this->data);
	}
	public function nopage()
	{
		$this->page_construct_nohead('error_404', $this->data);
	}
	
	function language($lang) {
		$this->session->set_userdata('lang', $lang);
		$refer =  $_SERVER["HTTP_REFERER"];
		if( $lang == "thailand"){
			redirect( str_replace("/en/","/th/",$refer) );
		}else{
			redirect( str_replace("/th/","/en/",$refer) );
		}
 	}
  	public function delete_redis(){
	  $this->cache->clean();
  	}
	
}
