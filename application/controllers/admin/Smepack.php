<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Smepack extends MY_Admin_Controller {

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see https://codeigniter.com/user_guide/general/urls.html
	*/
	public function __construct() {
		parent::__construct();
		if( !( $this->session->userdata('user_type') == "SA" || $this->session->userdata('user_type') == "A" ) ){
			redirect('admin/dashboard');
		}

		//$this->load->model($this->modelFolder.'Banner_model','banner',TRUE);
		$this->load->model($this->modelFolder.'Smepack_category_model','category',TRUE);
		$this->load->model($this->modelFolder.'Smepack_model','package',TRUE);
		//$this->load->model($this->modelFolder.'Brand_model','brand',TRUE);

		$default_field = array(
			'btn_type'	=>	'no_link',
			'btn_no_link'	=>	'',
			'btn_internal_link'	=>	'',
			'btn_external_link'	=>	'',
			'display_toggle'	=>	0,
			'onoff'	=> 1
		);
		$default_field_multilang = array(
			'header_message'	=>	'',
			'btn_name'	=>	'',
			'btn_content_popup'	=>	'',
		);
		$default_field_file = array(
			'btn_pdf_file'	=>	'',
			'display_icon_image'	=>	'',
		);

		/* ----------------- End Defalut field ---------------------  */

		$data_field = array(
				'main_type'	=>	'template',
				'main_template_type_calls'	=>	'อั้น',
				'main_template_number_calls'	=>	'',
				'main_template_type_networks'	=>	'ทุกเครือข่าย',
				'detail_type'	=>	'template',
				'detail_template_service_fee'	=>	'',
			);

		$data_multilang = array(
				'main_template_time_calls'	=>	'',
				'main_manual_description'	=>	'',
				'detail_template_freecall_conditions'	=>	'',
				'detail_template_4g_hd_voice'	=>	'',
				'detail_manual_description'	=>	'',
			);

		$this->data['fields']['mobile']['field'] = array_merge($default_field, $data_field);
		$this->data['fields']['mobile']['multilang'] = array_merge($default_field_multilang, $data_multilang);
		$this->data['fields']['mobile']['file'] = $default_field_file;

		/* ----------------- End mobile ---------------------  */

		$data_field = array(
			'main_type'	=>	'template',
			'main_template_type_use'	=>	'อั้น',
			'main_template_usage_amount'	=>	'',
			'main_template_maximum_speed'	=>	'',
			'detail_type'	=>	'template',
			'detail_template_use_full_speed'	=>	'',
		);

		$data_multilang = array(
				'main_manual_description'	=>	'',
				'detail_manual_description'	=>	'',
			);

		$this->data['fields']['mobile_internet']['field'] = array_merge($default_field, $data_field);
		$this->data['fields']['mobile_internet']['multilang'] = array_merge($default_field_multilang, $data_multilang);
		$this->data['fields']['mobile_internet']['file'] = $default_field_file;

		/* ----------------- End mobile_internet ---------------------  */

		$data_field = array(
			'main_type'	=>	'template',
			'main_template_type_use'	=>	'อั้น',
			'main_template_usage_amount'	=>	'',
			'detail_type'	=>	'manual',
		);

		$data_multilang = array(
				'main_manual_description'	=>	'',
				'detail_manual_description'	=>	'',
			);

		$this->data['fields']['wifi']['field'] = array_merge($default_field, $data_field);
		$this->data['fields']['wifi']['multilang'] = array_merge($default_field_multilang, $data_multilang);
		$this->data['fields']['wifi']['file'] = $default_field_file;

		/* ----------------- End wifi ---------------------  */

		$data_field = array(
			'main_type'	=>	'template',
			'main_template_brand'	=>	'',
			'detail_type'	=>	'manual',
		);

		$data_multilang = array(
				'main_template_model_description'	=> '',
				'main_manual_description'	=>	'',
				'detail_manual_description'	=>	'',
			);

		$this->data['fields']['notebook']['field'] = array_merge($default_field, $data_field);
		$this->data['fields']['notebook']['multilang'] = array_merge($default_field_multilang, $data_multilang);
		$this->data['fields']['notebook']['file'] = $default_field_file;

		/* ----------------- End notebook ---------------------  */

		$data_field = array(
			'main_type'	=>	'template',
			'main_template_type_calls'	=>	'อั้น',
			'main_template_number_calls'	=>	'',
			'main_template_type_networks'	=>	'ทุกเครือข่าย',
			'main_template_internet_type_use'	=>	'อั้น',
			'main_template_usage_amount'	=>	'',
			'main_template_maximum_speed'	=>	'',
			'detail_type'	=>	'template',
			'detail_template_service_fee'	=>	'',
			'detail_template_use_full_speed'	=>	'',
			'detail_template_wifi_type_use'	=>	'อั้น',
			'detail_template_usage_amount'	=>	'',
		);

		$data_multilang = array(
				'main_template_time_calls'	=>	'',
				'main_manual_description'	=>	'',
				'detail_template_freecall_conditions'	=>	'',
				'detail_template_4g_hd_voice'	=>	'',
				'detail_manual_description'	=>	'',
			);

		$this->data['fields']['mobile_bundle']['field'] = array_merge($default_field, $data_field);
		$this->data['fields']['mobile_bundle']['multilang'] = array_merge($default_field_multilang, $data_multilang);
		$this->data['fields']['mobile_bundle']['file'] = $default_field_file;

		/* ----------------- End mobile_bundle ---------------------  */

		$data_field = array(
			'main_type'	=>	'template',
			'main_template_download_speed'	=>	'',
			'detail_type'	=>	'template',
			'detail_template_upload_speed'	=>	'',
		);

		$data_multilang = array(
				'main_manual_description'	=>	'',
				'detail_manual_description'	=>	'',
			);

		$this->data['fields']['internet']['field'] = array_merge($default_field, $data_field);
		$this->data['fields']['internet']['multilang'] = array_merge($default_field_multilang, $data_multilang);
		$this->data['fields']['internet']['file'] = $default_field_file;

		/* ----------------- End internet ---------------------  */

		$data_field = array(
			'detail_type'	=>	'manual',
		);

		$data_multilang = array(
				'main_manual_description'	=>	'',
				'detail_manual_description'	=>	'',
			);

		$this->data['fields']['smart_wifi']['field'] = array_merge($default_field, $data_field);
		$this->data['fields']['smart_wifi']['multilang'] = array_merge($default_field_multilang, $data_multilang);
		$this->data['fields']['smart_wifi']['file'] = $default_field_file;

		/* ----------------- End smart_wifi ---------------------  */
		
		$data_field = array(
			'main_type'	=>	'template',
			'main_template_number_boxes'	=>	'',
			'detail_type'	=>	'manual',
		);

		$data_multilang = array(
				'main_template_channel_details'	=>	'',
				'main_manual_description'	=>	'',
				'detail_manual_description'	=>	'',
			);

		$this->data['fields']['true_visions']['field'] = array_merge($default_field, $data_field);
		$this->data['fields']['true_visions']['multilang'] = array_merge($default_field_multilang, $data_multilang);
		$this->data['fields']['true_visions']['file'] = $default_field_file;

		/* ----------------- End true_visions ---------------------  */

		$data_field = array(
			'main_type'	=>	'template',
			'main_template_call_price'	=>	'',
			'detail_type'	=>	'manual',
		);

		$data_multilang = array(
				'main_template_freecall_conditions'	=>	'',
				'main_manual_description'	=>	'',
				'detail_manual_description'	=>	'',
			);

		$this->data['fields']['fixedline']['field'] = array_merge($default_field, $data_field);
		$this->data['fields']['fixedline']['multilang'] = array_merge($default_field_multilang, $data_multilang);
		$this->data['fields']['fixedline']['file'] = $default_field_file;

		/* ----------------- End fixedline ---------------------  */

		$data_field = array(
			'detail_type'	=>	'manual',
		);

		$data_multilang = array(
				'main_manual_description'	=>	'',
				'detail_manual_description'	=>	'',
			);

		$this->data['fields']['general']['field'] = array_merge($default_field, $data_field);
		$this->data['fields']['general']['multilang'] = array_merge($default_field_multilang, $data_multilang);
		$this->data['fields']['general']['file'] = $default_field_file;

		/* ----------------- End general ---------------------  */

		$this->data['fields']['top_up']['field'] = array(
													'display_toggle'	=>	0,
													'onoff'	=> 1,
													//'special_offer'	=> '',
													'special_offer_select'	=> '',
													);
		$this->data['fields']['top_up']['multilang'] = array(
														'header_message'	=>	'',
													);
		$this->data['fields']['top_up']['file'] = array();



		$this->data['fields']['get_free']['field'] = $this->data['fields']['top_up']['field'];
		$this->data['fields']['get_free']['multilang'] = $this->data['fields']['top_up']['multilang'];
		$this->data['fields']['get_free']['file'] = $this->data['fields']['top_up']['file'];
		
		$this->data['fields']['discounts_privileges']['field'] = $this->data['fields']['top_up']['field'];
		$this->data['fields']['discounts_privileges']['multilang'] = $this->data['fields']['top_up']['multilang'];
		$this->data['fields']['discounts_privileges']['file'] = $this->data['fields']['top_up']['file'];

		/* ----------------- End top_up ---------------------  */	
	}


	

	public function index(){

	}
	
	public function lists( $cate_code = '' ){
		if( $cate_code == "" || $cate_code == 0 || $cate_code > 5 ){
			redirect('admin/dashboard');
		}
		$this->data['menu_active'] = 'smepack/lists/'.$cate_code;
		$this->data['data_code'] = '';//$this->common->get_code( 'package' );
		$this->data['detail'] = array();
		$this->data['cate_detail'] = $this->category->loadDetail( $cate_code );
		$this->data['cate_lists'] = $this->category->loadListsChild( $cate_code );

		$this->data['lists'] = $this->package->loadLists( $cate_code );
		
		$this->page_construct('smepack_list', $this->data);
	}

	function original_feature( $feature_name = '',$method = '', $pkg_number = 'pkg_number', $data_feature = array() ){
		$data = array();
		$data['langs'] = $this->data['langs'];
		$data['method'] = $method;
		$data['pkg_number'] = $pkg_number;
		$data['detail'] = $data_feature;
		$data['pagesList'] = $this->data['pagesList'];
		$data['fields'] = $this->data['fields'];
		$data['sp_get_free'] = $this->package->loadSpecialOfferLists( 'get_free' );
		$data['sp_top_up'] = $this->package->loadSpecialOfferLists( 'top_up' );
		$data['sp_discounts_privileges'] = $this->package->loadSpecialOfferLists( 'discounts_privileges' );


		$return_data = array();
		$return_data['mobile'] = $this->load->view('admin/smepack-feature/mobile',$data, true);		
		$return_data['mobile_internet'] = $this->load->view('admin/smepack-feature/mobile_internet',$data, true);
		$return_data['wifi'] = $this->load->view('admin/smepack-feature/wifi',$data, true);	
		$return_data['notebook'] = $this->load->view('admin/smepack-feature/notebook',$data, true);	
		$return_data['mobile_bundle'] = $this->load->view('admin/smepack-feature/mobile_bundle',$data, true);
		$return_data['internet'] = $this->load->view('admin/smepack-feature/internet',$data, true);
		$return_data['smart_wifi'] = $this->load->view('admin/smepack-feature/smart_wifi',$data, true);
		$return_data['true_visions'] = $this->load->view('admin/smepack-feature/true_visions',$data, true);
		$return_data['fixedline'] = $this->load->view('admin/smepack-feature/fixedline',$data, true);
		$return_data['general'] = $this->load->view('admin/smepack-feature/general',$data, true);
		$return_data['get_free'] = $this->load->view('admin/smepack-feature/get_free',$data, true);
		$return_data['top_up'] = $this->load->view('admin/smepack-feature/top_up',$data, true);
		$return_data['discounts_privileges'] = $this->load->view('admin/smepack-feature/discounts_privileges',$data, true);

		
		
		if ( $feature_name == '' ) {
			return $return_data; // original_feature
		} else {
			return $return_data[$feature_name];
		}
		//echo '<pre>'; print_r( $ddd ); echo '</pre>'; exit;
	}

	public function add( $cate_code = '' ){
		$this->data['menu_active'] = "smepack/add/".$cate_code;
		$this->data['data_code'] = '';//$this->common->get_code( 'package' );
		$this->data['detail'] = array();
		$this->data['seo'] = array();
		$this->data['cate_code'] = $cate_code;
		$this->data['cate_detail'] = $this->category->loadDetail( $cate_code );
		$this->data['cate_lists'] = $this->category->loadListsChild( $cate_code );

		//echo '<pre>'; print_r( $this->data['sp_top_up'] ); echo '</pre>'; exit;

		$package_detail_lang = array();
		$method = 'add';

		$default_feature = array();
		if ( $cate_code == 1 ) {
			// Default mobile, mobile_internet, wifi
			// original_feature(feature_name, method, pkg_number, data_feature)
			$default_feature[] = $this->original_feature('mobile', $method, 0, $package_detail_lang);
			$default_feature[] = $this->original_feature('mobile_internet', $method, 1, $package_detail_lang);
			$default_feature[] = $this->original_feature('wifi', $method, 2, $package_detail_lang);
	
		} else if ( $cate_code == 2 ) {
			$default_feature[] = $this->original_feature('internet', $method, 0, $package_detail_lang);
		} else if ( $cate_code == 3 ) {
			$default_feature[] = $this->original_feature('true_visions', $method, 0, $package_detail_lang);
		} else if ( $cate_code == 4 ) {
			$default_feature[] = $this->original_feature('fixedline', $method, 0, $package_detail_lang);
		} else if ( $cate_code == 5 ) {
			// Bundle not default
		}

		// $default_feature[] = $this->original_feature('notebook', $method, 3, $package_detail_lang);
		// $default_feature[] = $this->original_feature('mobile_bundle', $method, 4, $package_detail_lang);
		// $default_feature[] = $this->original_feature('internet', $method, 5, $package_detail_lang);
		// $default_feature[] = $this->original_feature('smart_wifi', $method, 6, $package_detail_lang);
		// $default_feature[] = $this->original_feature('true_visions', $method, 7, $package_detail_lang);
		// $default_feature[] = $this->original_feature('fixedline', $method, 8, $package_detail_lang);
		// $default_feature[] = $this->original_feature('general', $method, 9, $package_detail_lang);

		// $default_feature[] = $this->original_feature('get_free', $method, 10, $package_detail_lang);
		// $default_feature[] = $this->original_feature('top_up', $method, 11, $package_detail_lang);
		// $default_feature[] = $this->original_feature('discounts_privileges', $method, 12, $package_detail_lang);

		$this->data['default_feature'] = $default_feature;
		$this->data['original_feature'] = $this->original_feature();


		$this->page_construct('smepack_form', $this->data);
	}

	public function edit( $cate_code = '', $code = "" ){
		$this->data['menu_active'] = 'smepack/add/'.$cate_code;
		if( $code == "" ){
			redirect('admin/dashboard');
		}
		$this->data['cate_code'] = $cate_code;
		$this->data['cate_detail'] = $this->category->loadDetail( $cate_code );
		$this->data['cate_lists'] = $this->category->loadListsChild( $cate_code );
		$this->data['detail'] = $this->package->loadDetail( $code );

		if ( sizeof($this->data['detail']) > 0 ) {
			
			$default_feature = array();
			foreach( $this->data['langs'] as $key => $lang ){
				if ( $this->data['detail'][$lang['text']]['package'] != '' ) {
					$package_detail[$lang['text']] = json_decode($this->data['detail'][$lang['text']]['package'],true);
				} else {
					$package_detail[$lang['text']] = array();
				}
			}

			$i = 0;

			if ( sizeof( $package_detail[DEFAULT_LANG] ) > 0 ) {
				foreach ($package_detail[DEFAULT_LANG] as $key => $row) {
					$package_detail_lang = array();
					$package_detail_lang['pagesList'] = $this->common->pagesList();
					foreach( $this->data['langs'] as $key_lang => $lang ){
						$package_detail_lang[$lang['text']] = $package_detail[$lang['text']][$key];
					}

					$default_feature[] = $this->original_feature($row['feature'], 'edit', $i, $package_detail_lang);
					
					$i++;
				}
			}

			$this->data['default_feature'] = $default_feature;
			$this->data['original_feature'] = $this->original_feature();
			
			// echo '<pre>'; print_r( $default_feature ); echo '</pre>'; 
			// exit;
		}

		

		$this->page_construct('smepack_form', $this->data);
	}

	public function saveForm()
	{	
		$data_lang = array();
		$fields = $this->data['fields'];

		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';

		$data['name'] = isset( $_POST['name'] ) ? $this->input->post('name') : '';
		$data['price'] = isset( $_POST['code'] ) ? $this->input->post('price') : '';
		
		$data['cate_code'] = isset( $_POST['cate_code'] ) ? $this->input->post('cate_code') : '';
		$data['child_cate_code'] = isset( $_POST['child_cate_code'] ) ? $this->input->post('child_cate_code') : '';
		$data['highlight_homepage'] = isset( $_POST['highlight_homepage'] ) ? $this->input->post('highlight_homepage') : '';
		$data['highlight_sme_mainpage'] = isset( $_POST['highlight_sme_mainpage'] ) ? $this->input->post('highlight_sme_mainpage') : '';

		


		$data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';
		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
		
		// multilang
		$price_billing_cycle = isset( $_POST['price_billing_cycle'] ) ? $this->input->post('price_billing_cycle') : '';

		$highlight_main_desciption = isset( $_POST['highlight_main_desciption'] ) ? $this->input->post('highlight_main_desciption') : '';

        $files = $_FILES['tc_pdf']; // files
		$tc_pdf_txt = isset( $_POST['brochure_pdf_txt'] ) ? $this->input->post('tc_pdf_txt') : '';

		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'smepack' );
			$this->common->addLogs( "smepack", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "smepack", $data['code'], "update", "" );
		}


		// Upload liberies config
		$config = array();
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg|pdf|svg';
		$this->load->library('upload', $config);

		// if( $this->upload->do_upload('tc_pdf') ){
		// 	$upload = array('upload_data' => $this->upload->data());
		// 	$data['tc_pdf'] = $upload['upload_data']['file_name'];
		// }else{
		// 	if( isset($_POST['tc_pdf_txt']) && $_POST['tc_pdf_txt'] == ""){
		// 		$data['tc_pdf'] = "";
		// 	}
		// }

		$package = array();
		
		if ( sizeof($_POST['package']) > 0 ) {
			foreach ($_POST['package'] as $key => $row) {

				

				if ( isset( $row['onoff'] ) && $row['onoff'] != '' ) {
					
				} else {
					$row['onoff'] = 0;
				}

				if ( $row['feature'] == 'get_free' || $row['feature'] == 'top_up' || $row['feature'] == 'discounts_privileges' ) {
					
					if ( isset( $row['special_offer_select'] ) && sizeof($row['special_offer_select']) > 0 ) {
						$special_offer_select = '';
						foreach ($row['special_offer_select'] as $key_sos => $sos_row) {
							$special_offer_select .= $sos_row.',';
						}

						$special_offer_select = substr($special_offer_select, 0, -1); 
						$row['special_offer_select'] = $special_offer_select;
					} else {
						$row['special_offer_select'] = '';
					}
				}

				$package[] = $row;
			}

			//echo '<pre>'; print_r( $_FILES['package'] ); echo '</pre>';  exit;

			// Start Section Sort & upload file

			foreach ($_POST['package'] as $key => $row) {
				if ( sizeof( $_FILES['package']['name'] ) > 0 ) {
					$i = 0;
					foreach ($_FILES['package']['name'] as $key => $row) {
						$files['package']['name'][$i]= $row;
						$i++;
					}
					$i = 0;
					foreach ($_FILES['package']['type'] as $key => $row) {
						$files['package']['type'][$i]= $row;
						$i++;
					}
					$i = 0;
					foreach ($_FILES['package']['tmp_name'] as $key => $row) {
						$files['package']['tmp_name'][$i]= $row;
						$i++;
					}
					$i = 0;
					foreach ($_FILES['package']['error'] as $key => $row) {
						$files['package']['error'][$i]= $row;
						$i++;
					}
					$i = 0;
					foreach ($_FILES['package']['size'] as $key => $row) {
						$files['package']['size'][$i]= $row;
						$i++;
					}
				}
			}

			// End Section Sort & upload file

		}

		//echo '<pre>'; print_r( $package ); echo '</pre>'; exit;

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('smepack');
			$result = $q->result_array();
			
			$data['price_billing_cycle'] = isset( $price_billing_cycle[$key] ) ? $price_billing_cycle[$key] : '' ;
			$data['highlight_main_desciption'] = isset( $highlight_main_desciption[$key] ) ? $highlight_main_desciption[$key] : '' ;


            if ( sizeof( $files['package']['name'] ) > 0 ) {
				//echo '<pre>'; print_r( $files ); echo '</pre>'; exit;

				for ($i=0; $i < sizeof($files['package']['name']); $i++) { 

					foreach ($fields[$package[$i]['feature']]['file'] as $key_f => $name) {
						
                        if ( $key_f == 'btn_pdf_file' ) {
                            // multilang
                            $_FILES['package']['name'] = $files['package']['name'][$i][$key_f][$key];
                            $_FILES['package']['type'] = $files['package']['type'][$i][$key_f][$key];
                            $_FILES['package']['tmp_name'] = $files['package']['tmp_name'][$i][$key_f][$key];
                            $_FILES['package']['error'] = $files['package']['error'][$i][$key_f][$key];
                            $_FILES['package']['size'] = $files['package']['size'][$i][$key_f][$key];
                        
                            if( $this->upload->do_upload('package') ) {
                                $upload = array('upload_data' => $this->upload->data());
                                $package[$i]['btn_pdf_file'] = $upload['upload_data']['file_name'];
                            } else {
                                if( isset($package[$i][$key_f.'_txt'][$key]) && $package[$i][$key_f.'_txt'][$key] != "") {
                                    $package[$i]['btn_pdf_file'] = $package[$i][$key_f.'_txt'][$key];
                                }
                            }

                        } else {
                            if ( $key == 0 ) {
                                $_FILES['package']['name'] = $files['package']['name'][$i][$key_f];
                                $_FILES['package']['type'] = $files['package']['type'][$i][$key_f];
                                $_FILES['package']['tmp_name'] = $files['package']['tmp_name'][$i][$key_f];
                                $_FILES['package']['error'] = $files['package']['error'][$i][$key_f];
                                $_FILES['package']['size'] = $files['package']['size'][$i][$key_f];
                            
                                if( $this->upload->do_upload('package') ) {
                                    $upload = array('upload_data' => $this->upload->data());
                                    $package[$i][$key_f] = $upload['upload_data']['file_name'];
                                } else {
                                    if( isset($package[$i][$key_f.'_txt']) && $package[$i][$key_f.'_txt'] != "") {
                                        $package[$i][$key_f] = $package[$i][$key_f.'_txt'];
                                    }
                                }
                            } else {
                                $package[$i][$key_f] = $package[$i][$key_f];
                            }
                            
                        }
						
						
					}

				}

                //echo '<pre>'; print_r( $package ); echo '</pre>'; exit;
			}

			if ( sizeof($package) ) {
				$package_data = $package;
				foreach ($package as $key_p => $row) {

					foreach ($fields[$row['feature']]['multilang'] as $key_f => $field_name) {
						$package_data[$key_p][$key_f] = $row[$key_f][$key];
					}
					
				}

				$data['package'] = json_encode( $package_data );

			} else {
				$data['package'] = '';
			}

            $_FILES['tc_pdf']['name'] = $files['name'][$key];
            $_FILES['tc_pdf']['type'] = $files['type'][$key];
            $_FILES['tc_pdf']['tmp_name'] = $files['tmp_name'][$key];
            $_FILES['tc_pdf']['error'] = $files['error'][$key];
            $_FILES['tc_pdf']['size'] = $files['size'][$key];
        
            if( $this->upload->do_upload('tc_pdf') ) {
                $upload = array('upload_data' => $this->upload->data());
                $data['tc_pdf'] = $upload['upload_data']['file_name'];
            } else {
                
                if( isset($tc_pdf_txt[$key]) && $tc_pdf_txt[$key] == "") {
                    $data['tc_pdf'] = '';
                }
            }

			if( sizeof($result) > 0 ){
				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('smepack', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('smepack', $data );
			}
		}

		redirect('admin/smepack/edit/'.$data['cate_code'].'/'.$data['code']);

		//echo '<pre>'; print_r( $_FILES ); echo '</pre>'; exit;
		//echo '<pre>'; print_r( $data ); echo '</pre>'; exit;	
	}
	

	public function special_offer( $cmd = '', $code = "" ){
		if ( $cmd == 'lists' ) {
			$this->data['menu_active'] = "smepack/special_offer/lists";
			$this->data['lists'] = $this->package->loadSpecialOfferLists();
			//echo '<pre>'; print_r( $this->data['lists'] ); echo '</pre>';
			$this->page_construct('smepack_special_offer_list', $this->data);
		} else if ( $cmd == 'add' ) {
			$this->data['menu_active'] = "smepack/special_offer/add";
			$this->data['data_code'] = '';//$this->common->get_code( 'product' );
			$this->data['detail'] = array();
			$this->page_construct('smepack_special_offer_form', $this->data);
		} else if ( $cmd == 'edit' ) {
			$this->data['menu_active'] = "smepack/special_offer/add";
			if( $code == "" ){
				redirect('admin/dashboard');
			}
			$this->data['detail'] = $this->package->loadSpecialOfferDetail( $code );

			$this->page_construct('smepack_special_offer_form', $this->data);
		} else if ( $cmd == 'saveForm' ) {
			// echo '<pre>'; print_r($_POST); echo '</pre>';
			// exit;
			$data = array();
			$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
			if( $data['code'] == '' ){
				$data['code'] = $this->common->get_code( 'smepack_special_offer' );
				$this->common->addLogs( "smepack_special_offer", $data['code'], "create", "" );
			}else{
				$this->common->addLogs( "smepack_special_offer", $data['code'], "update", "" );
			}

			$description = isset( $_POST['description'] ) ? $this->input->post('description') : '';
			$btn_text = isset( $_POST['btn_text'] ) ? $_POST['btn_text'] : '';
			$btn_content_popup = isset( $_POST['btn_content_popup'] ) ? $_POST['btn_content_popup'] : '';
            $files = $_FILES['btn_pdf_file']; // files
		    $btn_pdf_file_txt = isset( $_POST['btn_pdf_file_txt'] ) ? $this->input->post('btn_pdf_file_txt') : '';
			
			$data['name'] = isset( $_POST['name'] ) ? ( $this->input->post('name') ): '';
			$data['category'] = isset( $_POST['category'] ) ? ( $this->input->post('category') ): '';
			$data['btn_link_type'] = isset( $_POST['btn_link_type'] ) ? ( $this->input->post('btn_link_type') ): '';
			$data['btn_internal_page_code'] = isset( $_POST['btn_internal_page_code'] ) ? ( $this->input->post('btn_internal_page_code') ): '';
			$data['btn_external_link'] = isset( $_POST['btn_external_link'] ) ? ( $this->input->post('btn_external_link') ): '';

			$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';


			// Upload liberies config
			$config = array();
			$config['upload_path'] = editor_upload_path();
			$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg|pdf|svg';
			$this->load->library('upload', $config);

			if( isset( $_POST['icon_image_txt'] ) ){
				if( $this->upload->do_upload('icon_image') ){
					$upload = array('upload_data' => $this->upload->data());
					$data['icon_image'] = $upload['upload_data']['file_name'];
				}else{
					if( isset($_POST['icon_image_txt']) and $_POST['icon_image_txt'] == ""){
						$data['icon_image'] = "";
					}
				}
			}

			// if( isset( $_POST['btn_pdf_file_txt'] ) ){
			// 	if( $this->upload->do_upload('btn_pdf_file') ){
			// 		$upload = array('upload_data' => $this->upload->data());
			// 		$data['btn_pdf_file'] = $upload['upload_data']['file_name'];
			// 	}else{
			// 		if( isset($_POST['btn_pdf_file_txt']) and $_POST['btn_pdf_file_txt'] == ""){
			// 			$data['btn_pdf_file'] = "";
			// 		}
			// 	}
			// }

			foreach( $this->data['langs'] as $key => $lang ){
				$data['lang'] = $lang['text'];
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$q = $this->db->get('smepack_special_offer');
				$result = $q->result_array();
				
				$data['description'] = isset( $description[$key] ) ? $description[$key] : '' ;
				$data['btn_text'] = isset( $btn_text[$key] ) ? $btn_text[$key] : '' ;
				$data['btn_content_popup'] = isset( $btn_content_popup[$key] ) ? $btn_content_popup[$key] : '' ;
                
                //upload file multilang
                $_FILES['btn_pdf_file']['name'] = $files['name'][$key];
                $_FILES['btn_pdf_file']['type'] = $files['type'][$key];
                $_FILES['btn_pdf_file']['tmp_name'] = $files['tmp_name'][$key];
                $_FILES['btn_pdf_file']['error'] = $files['error'][$key];
                $_FILES['btn_pdf_file']['size'] = $files['size'][$key];
            
                if( $this->upload->do_upload('btn_pdf_file') ) {
                    $upload = array('upload_data' => $this->upload->data());
                    $data['btn_pdf_file'] = $upload['upload_data']['file_name'];
                } else {
                    
                    if( isset($btn_pdf_file_txt[$key]) && $btn_pdf_file_txt[$key] == "") {
                        $data['btn_pdf_file'] = '';
                    }
                }

				if( sizeof($result) > 0 ){
					$data['u_date'] = date('Y-m-d H:i:s') ;
					$data['u_by'] = $this->session->userdata('id');
					$this->db->where('code', $data['code'] );
					$this->db->where('lang', $data['lang'] );
					$this->db->update('smepack_special_offer', $data );
	
				}else{
					$data['c_date'] = date('Y-m-d H:i:s') ;
					$data['c_by'] = $this->session->userdata('id');
					$this->db->insert('smepack_special_offer', $data );
				}
			}

			redirect('admin/smepack/special_offer/edit/'.$data['code']);
			// echo '<pre>'; print_r($_POST); echo '</pre>';
			// exit;


		}
	}
}
