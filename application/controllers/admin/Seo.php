<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seo extends MY_Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 public function __construct() {
		 parent::__construct();
     /*if( !( $this->session->userdata('user_type') == "SA" || $this->session->userdata('user_type') == "A" ) ){
       redirect('admin/dashboard');
     }*/

     $this->load->model($this->modelFolder.'Seo_model','seo',TRUE);
 }

	public function index(){
		redirect('admin/section/detail/xr/phenomenon');
	}

  	public function detail( $page, $sec_key = "" ){
		if( !check_permission( $this->session->userdata('user_type'), 'global_seo') ){
			$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
			redirect('admin/dashboard');
		  }
		if( $page == "" || $sec_key == "" ){
			//redirect('admin/section/detail/xr/phenomenon');
		}
		$this->data['param_page'] = $page;
		$this->data['param_sec_key'] = $sec_key;
		$this->data['menu_active'] = 'seo/detail/'.$page;
		$this->data['detail'] = $this->seo->loadDetail( $page );
		$this->page_construct( 'seo', $this->data );
	}
	public function saveDetailForm(){
		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';

		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'seo' );
			$this->common->addLogs( "seo", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "seo", $data['code'], "update", "" );
		}
		$data['page'] = isset( $_POST['page'] ) ? $this->input->post('page') : '';
		$data['sec_key'] = isset( $_POST['sec_key'] ) ? $this->input->post('sec_key') : '';
		$page_title = isset( $_POST['page_title'] ) ? $this->input->post('page_title') : '';
		$meta_title = isset( $_POST['meta_title'] ) ? $this->input->post('meta_title') : '';
		$meta_keyword = isset( $_POST['meta_keyword'] ) ? $this->input->post('meta_keyword') : '';
		$meta_desc = isset( $_POST['meta_desc'] ) ? $this->input->post('meta_desc') : '';

		// Upload liberies config
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx';

		$this->load->library('upload', $config);
		if( $this->upload->do_upload('share_img') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['share_img'] = $upload['upload_data']['file_name'];
		}else{
			if( $_POST['share_img_txt'] == ""){
				$data['share_img'] = "";
			}
		}

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('seo');
			$result = $q->result_array();
			$data['page_title'] = isset( $page_title[$key] ) ? $page_title[$key] : '' ;
			$data['meta_title'] = isset( $meta_title[$key] ) ? $meta_title[$key] : '' ;
			$data['meta_keyword'] = isset( $meta_keyword[$key] ) ? $meta_keyword[$key] : '' ;
			$data['meta_desc'] = isset( $meta_desc[$key] ) ? $meta_desc[$key] : '' ;

			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('seo', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('seo', $data );
			}
		}
		redirect('admin/seo/detail/'.$data['page'].'/'.$data['sec_key']);
	}
	
}
