<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission extends MY_Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 public function __construct() {
		 parent::__construct();
     $this->load->model($this->modelFolder.'Permission_model','permission',TRUE);
     
    if( !check_permission( $this->session->userdata('user_type'), 'permission') ){
        $this->common->setAlert('warning','You do not have permission for that page.','Warning !');
        redirect('admin/dashboard');
	}
 }
	public function index()
	{
		redirect('admin/permission/editor');
	}
  public function editor()
	{
    /*if( !( $this->session->userdata('user_type') == "SA") ){
       redirect('admin/dashboard');
     }*/
    	//$this->data['lists'] = $this->permission->loadCateLists();
		$this->data['menu_active'] = "permission/editor";
		$this->page_construct('cate_editor', $this->data );
	}
  
  public function saveEditorCate()
	{
		$data = array();
		$cate_code = isset( $_POST['cate_code'] ) ? $this->input->post('cate_code') : '';
		$role_code = isset( $_POST['role_code'] ) ? $this->input->post('role_code') : '';
		if( $cate_code != "" && $role_code != ""){
			$this->db->where('code', $cate_code );
			$this->db->where('lang', DEFAULT_LANG );
			$this->db->where('onoff', 1 );
			$this->db->where('status', 1 );
			$q = $this->db->get('section_cate');
			$result = $q->result_array();
			if( sizeof( $result ) > 0 ){
				$cate_permission = $result[0]['cate_permission'] == '' ? array() : explode(",",$result[0]['cate_permission']);
				if( !in_array( $role_code, $cate_permission ) ){
					array_push( $cate_permission , $role_code );
				}
				$data['cate_permission'] = implode($cate_permission,",");
				$this->db->where('code', $cate_code );
				$this->db->update('section_cate', $data );
				$this->common->setAlert('success','Permission has been saved.','Sucess !');
			}else{
				$this->common->setAlert('error','Category is not valid.','Error !');
			}

			//$detail[DEFAULT_LANG]['cate_permission'] == '' ? array() : explode(",",$detail[DEFAULT_LANG]['cate_permission'])
			
		}else{
			$this->common->setAlert('error','Permission has not been saved.','Error !');
		}
		redirect('admin/permission/editor');
	}

	public function removeRoleCate(){
		$cate_code = isset( $_POST['cate_code'] ) ? $this->input->post('cate_code') : '';
		$role_code = isset( $_POST['role_code'] ) ? $this->input->post('role_code') : '';
		if( $cate_code != "" && $role_code != ""){
			$this->db->where('code', $cate_code );
			$this->db->where('lang', DEFAULT_LANG );
			$this->db->where('onoff', 1 );
			$this->db->where('status', 1 );
			$q = $this->db->get('section_cate');
			$result = $q->result_array();
			if( sizeof( $result ) > 0 ){
				$cate_permission = $result[0]['cate_permission'] == '' ? array() : explode(",",$result[0]['cate_permission']);
				foreach( $cate_permission as $key => $permission ){
					if( $permission == $role_code ){
						unset( $cate_permission[$key] );
					}
				}
				$data['cate_permission'] = implode($cate_permission,",");
				$this->db->where('code', $cate_code );
				$this->db->update('section_cate', $data );
			}
		}
		$ar_result = array();
		$ar_result['rs'] = true;
		echo json_encode( $ar_result );
	}
}
