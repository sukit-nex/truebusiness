<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 public function __construct() {
		 parent::__construct();
     $this->load->model($this->modelFolder.'user_model','user',TRUE);
     $this->data['menu_active'] = "user";
    if( !check_permission( $this->session->userdata('user_type'), 'user') ){
        $this->common->setAlert('warning','You do not have permission for that page.','Warning !');
        redirect('admin/dashboard');
		}
 }
	public function index()
	{
		redirect('admin/user/lists');
	}
  public function lists()
	{
    /*if( !( $this->session->userdata('user_type') == "SA") ){
       redirect('admin/dashboard');
     }*/
    $this->data['lists'] = $this->user->loadLists();
		$this->page_construct('userList', $this->data, "user");
	}
  public function edit( $id = "" )
	{
    if( $id == "" ){
      redirect('admin/user/lists');
    }else{
      if( $this->session->userdata('user_type') != "SA" && $this->session->userdata('user_type') != "A" && $this->session->userdata('id') != $id){
        redirect('admin/dashboard');
      }
    }
    $this->data['detail'] = $this->user->loadDetail( $id );
    if( $this->session->userdata('user_type') != "SA" ){
      if( $this->data['detail'][0]['user_type'] == "SA" ){
        redirect('admin/user/lists');
      }
    }
		$this->page_construct('userForm', $this->data, "user");
	}
  public function add()
	{
    /*if( !( $this->session->userdata('user_type') == "SA") ){
       redirect('admin/dashboard');
     }*/
    $this->data['detail'] = array();
		$this->page_construct('userForm', $this->data, "user");
  }
  public function changePass(){
    $id = isset( $_POST['id'] ) ? $this->input->post('id') : '';
    $password = isset( $_POST['password'] ) ? $this->input->post('password') : '';
    $pass_enc = hash('sha256', $password );
    $data['password'] = $pass_enc;
    if( $id != "" ){
      $data['u_date'] = date('Y-m-d H:i:s') ;
      $data['u_by'] = $this->session->userdata('id');
      $this->db->where('id', $id );
      $this->db->update('user', $data );
      $this->common->addLogs( "user", $id, "change password", "" );
    }
    redirect('admin/user/edit/'.$id);
  }
  public function saveForm()
	{
    $data = array();
    $id = isset( $_POST['id'] ) ? $this->input->post('id') : '';
    $data['name'] = isset( $_POST['firstname'] ) ? $this->input->post('firstname') : '';
    $data['lastname'] = isset( $_POST['lastname'] ) ? $this->input->post('lastname') : '';
    $data['position'] = isset( $_POST['position'] ) ? $this->input->post('position') : '';
    $data['username'] = isset( $_POST['username'] ) ? $this->input->post('username') : '';
    $data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
    if( $id == "" ){
      $password = isset( $_POST['password'] ) ? $this->input->post('password') : '';
      if( $id != "" ){
        $data['password'] = $password;
      }else{
        $pass_enc = hash('sha256', $password );
        $data['password'] = $pass_enc;
      }
    }
    if( isset( $_POST['user_type'] ) ){
      $data['user_type'] = $this->input->post('user_type');
    }
    //$data['user_type'] = isset( $_POST['user_type'] ) ? $this->input->post('user_type') : '';

    // Upload liberies config
    $config['upload_path'] = editor_upload_path();
    $config['allowed_types'] = 'gif|jpg|png';
    $this->load->library('upload', $config);
    if( $this->upload->do_upload('thumb') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['thumb'] = $upload['upload_data']['file_name'];
    }

    if( $id != "" ){
      $data['u_date'] = date('Y-m-d H:i:s') ;
      $data['u_by'] = $this->session->userdata('id');
      $this->db->where('id', $id );
      $this->db->update('user', $data );
      $this->common->addLogs( "user", $id, "create", "" );
    }else{
      $data['c_date'] = date('Y-m-d H:i:s') ;
      $data['c_by'] = $this->session->userdata('id');
      $this->db->insert('user', $data );
      $id = $this->db->insert_id();
      $this->common->addLogs( "user", $id, "update", "" );
    }
    if( $id == $this->session->userdata('id') ){
      if( $data['thumb'] != ""){
        $this->session->set_userdata('thumb', $data['thumb']) ;
      }
    }
    $this->common->setAlert('success','User has been saved.','Sucess !');
    redirect('admin/user/edit/'.$id);
	}
}
