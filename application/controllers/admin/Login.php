<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 public function __construct() {
		 parent::__construct();
     $this->load->library('form_validation');
     $this->admin_login = $this->session->userdata('is_admin_login');
 }
	public function index()
	{
    //phpinfo();
    $this->admin_login = $this->session->userdata('is_admin_login');
    $data = array();
    $data['error'] = $this->session->userdata('error');
    //print_r( $this->session );
    if( $this->admin_login ){
      redirect('admin/dashboard/');
    }else{
      $this->load->view('admin/' . 'inc/meta');
      $this->load->view('admin/login',$data);
      $this->load->view('admin/' . 'inc/script');
    }

	}
	public function do_login()
	{

     //$apc_key = "{$_SERVER['SERVER_NAME']}~login:{$_SERVER['REMOTE_ADDR']}";
  //  $tries = (int)apcu_fetch($apc_key);
    /*if ($tries >= 3) {
      header("HTTP/1.1 429 Too Many Requests");
      echo "You've exceeded the number of login attempts. We've blocked IP address {$_SERVER['REMOTE_ADDR']} for a few minutes.";
      exit();
    }*/
    $this->load->model('admin/Login_model','login',TRUE);
    $username = isset( $_POST['username'] ) ? $this->security->xss_clean( $this->input->post('username') ) : '';
    $password = isset( $_POST['password'] ) ? $this->security->xss_clean( $this->input->post('password') ) : '';

    $this->form_validation->set_rules('username', 'Username', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');

    if ($this->form_validation->run() == FALSE) {
        $this->session->set_userdata('error', '<strong>Access Denied</strong> Invalid Username/Password');
        redirect('admin/login');
    } else {
      $set_time_expire = 60; // 1 minutes
      $failed_logins = $this->session->userdata('failed_logins');
      $failed_logins_time = $this->session->userdata('failed_logins_time');
      //reset failed_logins / failed_logins_time
      if ( $failed_logins > 3 and ($failed_logins_time+$set_time_expire) < time()) {
        $failed_logins = $this->session->set_userdata('failed_logins', 0);
        $failed_logins_time = $this->session->set_userdata('failed_logins_time', time());
      }

      if ( $failed_logins >= 3 ) {
        $failed_logins++;
        $this->session->set_userdata('failed_logins', $failed_logins);
        $this->session->set_userdata('error', '<strong>Your account is currently locked.</strong> You must wait 1 minutes before you can log in again!.');
        $this->session->set_userdata('failed_logins_time', time());
        redirect('admin/login');
      } else {
        $success = $this->login->checkLogin($username, $password);
        if (!$success) {
          if ($failed_logins < 4) {
            $failed_logins = ( $failed_logins == '' )? 0 : $failed_logins;

            $failed_logins++;
            $this->session->set_userdata('failed_logins', $failed_logins);
            $this->session->set_userdata('failed_logins_time', time());
            $this->session->set_userdata('error', '<strong>Access Denied</strong> Invalid Username/Password');
          }
          //apcu_store( $apc_key, $tries+1, 60 ); // # store tries for 1 minutes
          //echo 'dologin';
          redirect('admin/login');

        } else {
          //echo 'dologin else';
          //$this->session->set_userdata('error', '');
          redirect('admin/dashboard');
        }
      }
      
    }

  /*  $apc_key = $_SERVER['REMOTE_ADDR'];
    $count = $this->session->userdata('login_count');
    if( $count == "" ){ $count = 1 ;}
    if( $count >= 3 ){
      $this->session->set_userdata('error', "We've blocked IP address {$_SERVER['REMOTE_ADDR']} for a few minutes.");
      header("HTTP/1.1 429 Too Many Requests");
      echo "You've exceeded the number of login attempts. We've blocked IP address {$_SERVER['REMOTE_ADDR']} for a few minutes.";
      exit();
    }else{
      $this->load->model('admin/Login_model','login',TRUE);
      $username = isset( $_POST['username'] ) ? $this->security->xss_clean( $this->input->post('username') ) : '';
      $password = isset( $_POST['password'] ) ? $this->security->xss_clean( $this->input->post('password') ) : '';

      $this->form_validation->set_rules('username', 'Username', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required');
      if ($this->form_validation->run() == FALSE) {
          $this->session->set_userdata('error', '<strong>Access Denied</strong> Invalid Username/Password 1');
          redirect('admin/login');
      } else {
        $success = $this->login->checkLogin($username, $password);
        if (!$success) {
          if( $apc_key == $this->session->userdata('temp_key') ){
            $count++;
            $this->session->set_userdata('temp_key', $apc_key );
            $this->session->set_userdata('login_count', $count );
          }else{
            $this->session->set_userdata('temp_key', $apc_key );
            $this->session->set_userdata('login_count', $count );
          }
          redirect('admin/login');

        } else {
          $this->session->set_userdata('temp_key', "" );
          $this->session->set_userdata('login_count', "" );
          $this->session->set_userdata('error', '');
          $this->session->sess_expiration = '7200';
          redirect('admin/dashboard');
        }
      }
    }*/

/*
    $tries = (int)apcu_fetch($apc_key);
    if ($tries >= 3) {
      header("HTTP/1.1 429 Too Many Requests");
      echo "You've exceeded the number of login attempts. We've blocked IP address {$_SERVER['REMOTE_ADDR']} for a few minutes.";
      exit();
    }
    $this->load->model('admin/Login_model','login',TRUE);
    $username = isset( $_POST['username'] ) ? $this->security->xss_clean( $this->input->post('username') ) : '';
    $password = isset( $_POST['password'] ) ? $this->security->xss_clean( $this->input->post('password') ) : '';

    $this->form_validation->set_rules('username', 'Username', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');

    if ($this->form_validation->run() == FALSE) {
        $this->session->set_userdata('error', '<strong>Access Denied</strong> Invalid Username/Password 1');
        redirect('admin/login');
    } else {
      $success = $this->login->checkLogin($username, $password);
      if (!$success) {

        apcu_store( $apc_key, $tries+1, 60 ); // # store tries for 1 minutes

        redirect('admin/login');

      } else {
        $this->session->set_userdata('error', '');
        redirect('admin/dashboard');
      }
    } */

	}
}
