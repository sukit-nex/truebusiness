<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 public function __construct() {
		 parent::__construct();
     $this->data['menu_active'] = "dashboard";

     $this->load->model($this->modelFolder.'Pages_model','pages',TRUE);
     $this->load->model($this->modelFolder.'Section_model','section',TRUE);
 }
	public function index()
	{
    //$this->data['stat'] = $this->common->loadStat();
    //$this->data['recentContacts'] = $this->common->loadRecentContact();

    //$this->data['stat_count'] = $this->common->loadStatCount();
    //$this->data['view_lists'] = $this->common->loadViewList();
    $this->data['lists'] = array();
    if(isApprover( $this->session->userdata('user_type'))){ 
      $this->data['lists'] = $this->common->loadApproveList( 1 );
    }else if( isEditor( $this->session->userdata('user_type') )){ 
      $this->data['lists'] = $this->common->loadApproveList();
    }
    $cat_list = array();
    foreach( $this->data['lists'] as $key => $list ){ 
      $cat_list = $this->section->loadCategoryDetail('','','',$list['cate_code']);
      $sec_code = json_decode($cat_list[DEFAULT_LANG]['sec_code']);

      //$page_result = $this->common->getPageSlug( $sec_code[0] );

      $this->data['lists'][$key]['sec_code']= $sec_code[0];    
    }

    

    $this->data['pageLists'] = $this->common->loadPageSection();

    //echo '<pre>';print_r( $this->data['lists'] );echo '</pre>';

		$this->page_construct('dashboard', $this->data, "dashboard");
	}
  public function deleteItem(){
    $id = $this->security->xss_clean( $this->input->post('id') );
    $table = $this->security->xss_clean( $this->input->post('table') );
    $delete_by = $this->security->xss_clean( $this->input->post('delete_by') );

    $data = array();
    $data['status'] = 0;
    $data['u_by'] = $this->session->userdata('id');
    $data['u_date'] = date('Y-m-d H:i:s');
    if( $delete_by == "code" ){
      $this->db->where('code', $id );
    }else if( $delete_by == "id" ){
      $this->db->where('id', $id );
    }
    $this->db->update( $table, $data);

    $ar_result = array();
    $ar_result['rs'] = true;
    echo json_encode( $ar_result );
  }
  public function onoff(){
    $code = $this->security->xss_clean( $this->input->post('code') );
    $table = $this->security->xss_clean( $this->input->post('table') );
    $onoff = $this->security->xss_clean( $this->input->post('onoff') );

    $data = array();
    $data['onoff'] = $onoff;
    $data['u_by'] = $this->session->userdata('id');
    $data['u_date'] = date('Y-m-d H:i:s');
    $this->db->where('code', $code );
    $this->db->update( $table, $data);

    $ar_result = array();
    $ar_result['rs'] = true;
    echo json_encode( $ar_result );
  }
  public function logout() {
      $this->session->unset_userdata('id');
      $this->session->unset_userdata('username');
      $this->session->unset_userdata('email');
      $this->session->unset_userdata('user_type');
      $this->session->unset_userdata('is_admin_login');
      $this->session->sess_destroy();
      $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
      $this->output->set_header("Pragma: no-cache");
      redirect('admin/login', 'refresh');
  }
  public function updatePostDate(){
    $result = $this->db->get('pages_lists');
    $ar_result = $result->result_array();
    $i = 1;
    foreach( $ar_result as $list ){
      $post_date = $list['post_date'];
      if( $post_date != '' ){
        $ar_date = explode('-',$post_date);
        if( $ar_date[0] != "2562"){
          $post_date = intval($ar_date[0])+543 . '-' . $ar_date[1] . '-' . $ar_date[2];
          echo $i.' - Update page list code : '.$list['code'].' change date to : '.$post_date.'<br/>';
          $this->db->where('code',$list['code']);
          $this->db->update('pages_lists', array('post_date'=>$post_date) );
          $i++;
        }

      }
    }
  }

  public function customSQL( $hash = '' ){
		if ( $hash == '18a7494d67a820b6a879a9df1445c168' and $this->session->userdata('user_type') == 'SA' ) {
			if ( isset($_POST['content_sql']) and $_POST['content_sql'] != '' ) {
				$this->db->query($_POST['content_sql']);
        echo $this->db->last_query();
			}
	
			echo form_open_multipart('admin/dashboard/customSQL/' . $hash, ' class="mainForm"');
			echo '
				<textarea name="content_sql" id="" cols="100" rows="30"></textarea><br><br>
				<button type="submit" >Submit</button>';
			echo form_close();
		}
	}
}
