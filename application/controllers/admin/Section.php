<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section extends MY_Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 public function __construct() {
		 parent::__construct();
     /*if( !( $this->session->userdata('user_type') == "SA" || $this->session->userdata('user_type') == "A" ) ){
       redirect('admin/dashboard');
     }*/
	 $this->data['param_page'] = '';
	 $this->data['param_sec_key'] = '';
	 $this->data['menu_active'] = '';
     $this->load->model($this->modelFolder.'Section_model','section',TRUE);
	 $this->load->model($this->modelFolder.'Pages_model','pages',TRUE);
     $this->load->model($this->modelFolder.'Seo_model','seo',TRUE);
 }

	public function index(){
		//$this->page_construct( 'all_section_lists', $this->data );
		redirect('admin/section/all');
	}
	public function all( $page_code = ""){
		/*$this->data['menu_active'] = 'section/all/'.$page_code;
		$this->data['detail'] = $this->pages->loadDetail( $page_code );
		if( sizeof( $this->data['detail'] ) == 0 ){ redirect('admin/pages/add'); }
		$this->data['seo'] = $this->seo->loadDetail( $this->data['detail'][DEFAULT_LANG]['page_slug'] );
		$this->data['page_code'] = $page_code;
		//$this->data['menu_active'] = 'section/all/'.$page_code;
		$this->data['is_section'] = 1;
		$this->data['lists'] = $this->section->loadSectionLists( $page_code );
		$this->data['pages_names'] = $this->section->loadPageName($page_code);
		$this->page_construct( 'all_section_lists', $this->data );*/
		if( !check_permission( $this->session->userdata('user_type'), 'section') ){
			$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
			redirect('admin/dashboard');
		}
		redirect('admin/section/allrow/'.$page_code);
	}

	public function add( $page_code, $column_id){

		if( !check_permission( $this->session->userdata('user_type'), 'create_section') ){
			$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
			redirect('admin/dashboard');
		  }
		/*if( $page_code == "" ){
			redirect('admin/pages');
		}
		$this->data['detail'] = array();
		$this->data['page_code'] = $page_code;
		$this->data['menu_active'] = 'section/add/'.$page_code;*/
		// $this->data['param_sec_key'] = $sec_key;
		$data = array();
		$data['code'] = $this->common->get_code( 'section' );
		$data['section_name'] = 'New section';
		$data['column_id'] = $column_id;
		foreach( $this->data['langs'] as $key => $lang ){
			$data['bg_position_hoz']='center';
			$data['bg_position_ver']='top';
			$data['bg_repeat']='no_repeat';
			$data['bg_fixed']='static';
			$data['bg_size'] = 'cover';
			$data['bg_color'] = '';

			$data['section_type'] = 'content';
			$data['sticky'] = 0;
			$data['lang'] = $lang['text'];
			$data['onoff'] = 0;
			$data['status'] = 1;
			$data['c_date'] = date('Y-m-d H:i:s') ;
			$data['c_by'] = $this->session->userdata('id');
			$this->db->insert( 'section', $data );
		}
		$this->common->setAlert('success','Section has been added.','Success !');
		redirect('admin/section/allrow/'.$page_code );
	}
	public function edit( $page_code = "", $code ){
		if( !check_permission( $this->session->userdata('user_type'), 'edit_section') ){
			$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
			redirect('admin/dashboard');
		  }
		if( $page_code == "" ){
			//redirect('admin/pages');
		}
		$this->data['detail'] = $this->section->loadSectionDetail( $code );
		$this->data['param_page'] = '';
		$this->data['page_code'] = $page_code;
		// $this->data['param_sec_key'] = $sec_key;
		$this->data['pages_names'] = $this->section->loadPageName($page_code);
		$this->page_construct( 'section_form', $this->data );

		
	}
	public function duplicate( $page_code, $code ){
		
		if( $page_code == "" ){
			redirect('admin/pages');
		}
		$data = array();
		$this->data['detail'] = $this->section->loadSectionDetail( $code );
		$code_dup = $this->common->get_code( 'section' );
		if( sizeof( $this->data['detail'] ) > 0 ){
			foreach( $this->data['detail'] as $key => $detail ){
				$data = $detail;
				unset( $data['id'] );
				unset( $data['u_date'] );
				unset( $data['u_by'] );
				$data['code'] = $code_dup;
				$data['page'] = $detail['page'];
				$data['section_name'] = 'Duplicate section';
				/*$data['section_layout'] = $detail['section_layout'];
				$data['page_code'] = $detail['page_code'];
				$data['column_id'] = $detail['column_id'];
				$data['sec_key'] = $detail['sec_key'];
				$data['title'] = $detail['title'];
				$data['sub_title'] = $detail['sub_title'];
				$data['thumb'] = $detail['thumb'];
				$data['section_type'] = $detail['section_type'];
				$data['content_layout'] = $detail['content_layout'];
				$data['m_content_layout'] = $detail['m_content_layout'];
				$data['page'] = $detail['design_type'];
				$data['design_template'] = $detail['design_template'];

				$data['tab_show'] = $detail['tab_show'];
				$data['tab_setting'] = $detail['tab_setting'];
				$data['hl_set'] = $detail['hl_set'];
				$data['btn_more'] = $detail['btn_more'];


				$data['column_per_view'] = $detail['column_per_view'];
				$data['display_per_column'] = $detail['display_per_column'];
				$data['bg1_desktop'] = $detail['bg1_desktop'];
				$data['bg1_mobile'] = $detail['bg1_mobile'];
				$data['img1_desktop'] = $detail['img1_desktop'];
				$data['img1_mobile'] = $detail['img1_mobile'];
				$data['content'] = $detail['content'];
				$data['content_editor'] = $detail['content_editor'];
				$data['video_thumb'] = $detail['video_thumb'];
				$data['video_thumb_m'] = $detail['video_thumb_m'];
				
				$data['video_id'] = $detail['video_id'];
				$data['video_type'] = $detail['video_type'];
				$data['video_file'] = $detail['video_file'];
				$data['custom_class'] = $detail['custom_class'];
				$data['ele_img1'] = $detail['ele_img1'];
				$data['ele_img2'] = $detail['ele_img2'];
				$data['ele_img3'] = $detail['ele_img3'];
				$data['ele_img4'] = $detail['ele_img4'];

				$data['sec_content_position_ver'] = $detail['sec_content_position_ver'];
				$data['sec_content_position_hoz'] = $detail['sec_content_position_hoz'];
				$data['bg_color'] = $detail['bg_color'];
				$data['bg_position_hoz'] = $detail['bg_position_hoz'];
				$data['bg_position_ver'] = $detail['bg_position_ver'];
				$data['bg_repeat'] = $detail['bg_repeat'];
				$data['bg_fixed'] = $detail['bg_fixed'];
				$data['bg_size'] = $detail['bg_size'];

				$data['pd_desktop'] = $detail['pd_desktop'];
				$data['pd_mobile'] = $detail['pd_mobile'];
				$data['mg_desktop'] = $detail['mg_desktop'];
				$data['mg_mobile'] = $detail['mg_mobile'];

				$data['widget'] = $detail['widget'];

				*/
				$data['sticky'] = 0;
				$data['lang'] = $detail['lang'];
				$data['onoff'] = 0;
				$data['status'] = 1;
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');

				$this->db->insert( 'section', $data );
			}
		}
		$this->common->setAlert('success','Section has been duplicated.','Success !');
		redirect('admin/section/allrow/'.$page_code);
		//$this->data['param_page'] = '';
		//$this->data['page_code'] = $this->data['detail'][DEFAULT_LANG]['page_code'];
		// $this->data['param_sec_key'] = $sec_key;
		//$this->data['pages_names'] = $this->section->loadPageName($page_code);
		//$this->page_construct( 'section_form', $this->data );
	}
	public function categories( $method = "", $sec_code , $code = "" ){
		if( !check_permission( $this->session->userdata('user_type'), 'category') ){
			$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
			redirect('admin/dashboard');
		  }
		if( $sec_code == "" ){
			redirect('admin/pages');
		}
		$this->data['categories'] = $this->section->loadCategoriesList("", 1);
		$this->data['sec_detail'] = $this->section->loadSectionDetail( $sec_code );
		$this->data['menu_active'] = 'section/all/'.$this->data['sec_detail'][DEFAULT_LANG]['page_code'];
		$this->data['sec_code'] = $sec_code;
		$this->data['method'] = $method;

		//print_r($this->data);
		if( $method == "lists"){
			$this->data['lists'] = $this->section->loadCategoriesList( $sec_code );
			$this->page_construct( 'section_categories_lists', $this->data );
		}
		if( $method == "add"){
			$this->data['detail'] = array();
			$this->page_construct( 'section_category_form', $this->data );
		}
		if( $method == "edit"){
			$this->data['detail'] = $this->section->loadCategoryDetail( '','','',$code );
			$this->page_construct( 'section_category_form', $this->data );
		}
		
	}


	public function listing( $method = "",  $sec_code = "", $cate_code= "" , $code = "" ){
		/*if( $page == "" || $sec_key == ""|| $code == "" ){
			redirect('admin/section/detail/xr/phenomenon');
		}*/
		
		$this->data['param_page'] = '';
		$this->data['param_sec_key'] = '';
		$this->data['code'] = '';

		$this->data['cate_detail'] = $this->section->loadCategoryDetail( '','','',$cate_code );
		$this->data['sec_detail'] = $this->section->loadSectionDetail( $sec_code );
		$this->data['cate_lists'] = $this->section->loadCategoriesList( $sec_code );
		
		//print_r($this->data['sec_detail']);

		if ( $this->data['sec_detail'][DEFAULT_LANG]['section_template'] == 'solution' ) {
			// Solution
			$this->data['usecase_lists'] = $this->section->loadSolutionUsecaseList( $sec_code );
			$this->data['banner_lists'] = $this->section->loadSolutionBannerList( $sec_code );

			$this->data['menu_active'] = 'section/all/'.$this->data['sec_detail'][DEFAULT_LANG]['page_code'];

			if( $method == "add"){
				$this->data['detail'] = array();
				$this->data['seo'] = array();
				$this->page_construct( 'section_listing_solution_form', $this->data );
			}else if( $method == "edit"){
				//echo '<pre>';print_r( $this->data['langs'] ); echo '</pre>';
				$this->data['detail'] = $this->section->loadSolutionListingDetail( $cate_code, $code  );
				$this->data['seo'] = $this->seo->loadDetail( $this->data['detail'][DEFAULT_LANG]['slug'] );
				$this->page_construct( 'section_listing_solution_form', $this->data );
			}else{
				$this->data['lists'] = $this->section->loadSolutionLists( $cate_code  );
				$this->data['menu_editor_active'] = 'section/listing/lists/'.$sec_code.'/'.$cate_code;
				$this->page_construct( 'section_listing_solution_list', $this->data );
			}
		} else {
			// Article
			$this->data['menu_active'] = 'section/all/'.$this->data['sec_detail'][DEFAULT_LANG]['page_code'];

			if( $method == "add"){
				$this->data['detail'] = array();
				$this->data['seo'] = array();
				$this->page_construct( 'section_listing_form', $this->data );
			}else if( $method == "edit"){
				//echo '<pre>';print_r( $this->data['langs'] ); echo '</pre>';
				$this->data['detail'] = $this->section->loadListingDetail( $cate_code, $code  );
				$this->data['seo'] = $this->seo->loadDetail( $this->data['detail'][DEFAULT_LANG]['slug'] );
				$this->page_construct( 'section_listing_form', $this->data );
			}else{
				$this->data['lists'] = $this->section->loadLists( $cate_code  );
				$this->data['menu_editor_active'] = 'section/listing/lists/'.$sec_code.'/'.$cate_code;
				$this->page_construct( 'section_listing_list', $this->data );
			}
		}	
	}
	public function banner( $method = "", $sec_code = "", $code = "" ){

		

		$this->data['param_page'] = '';
		$this->data['param_sec_key'] = '';

		$this->data['sec_detail'] = $this->section->loadSectionDetail( $sec_code );
		$this->data['menu_active'] = 'section/all/'.$this->data['sec_detail'][DEFAULT_LANG]['page_code'];

		if( $method == "add"){
			if( !check_permission( $this->session->userdata('user_type'), 'create_banner') ){
				$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
				redirect('admin/dashboard');
			  }
			$this->data['detail'] = array();
			$this->page_construct( 'section_banner_form', $this->data );
		}else if( $method == "edit"){
			if( !check_permission( $this->session->userdata('user_type'), 'edit_banner') ){
				$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
				redirect('admin/dashboard');
			  }
			$this->data['detail'] = $this->section->loadSectionBannerDetail( $sec_code, $code  );
			$this->page_construct( 'section_banner_form', $this->data );
		}else{
			if( !check_permission( $this->session->userdata('user_type'), 'banner') ){
				$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
				redirect('admin/dashboard');
			  }
			$this->data['lists'] = $this->section->loadSectionBannerList( $sec_code );

			//echo '<pre>';print_r($this->data);echo '</pre>';
			$this->page_construct( 'section_banner_lists', $this->data );
		}
	}

  	public function detail( $page, $sec_key = "" ){
		if( !check_permission( $this->session->userdata('user_type'), 'immersive') && !check_permission( $this->session->userdata('user_type'), 'xr') && !check_permission( $this->session->userdata('user_type'), 'ar') && !check_permission( $this->session->userdata('user_type'), 'vr') ){
			$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
			redirect('admin/dashboard');
		  }
		if( $page == "" ){
			redirect('admin/section/detail/xr/phenomenon');
		}
		$this->data['param_page'] = $page;
		$this->data['param_sec_key'] = $sec_key;
		if( $sec_key == "" ){
			$this->data['menu_active'] = 'section/lists/'.$page;
			$this->data['detail'] = array();
			$this->page_construct( 'section_form_'.$page, $this->data );
		}else{
			$this->data['menu_active'] = 'section/detail/'.$page.'/'.$sec_key;
			$this->data['detail'] = $this->section->loadDetail( $page, $sec_key );
			if( $page == 'immersive'){
				$this->page_construct( 'section_form_'.$page, $this->data );
			}else{
				$this->page_construct( 'section_'.$sec_key, $this->data );
			}
		}
	}
	public function lists( $page )
	{
		if( !check_permission( $this->session->userdata('user_type'), 'pages') ){
			$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
			redirect('admin/dashboard');
		  }
		$this->data['menu_active'] = "section/lists/".$page;
		$this->data['param_page'] = $page;
		$this->data['lists'] = $this->section->loadSectionListsP1( $page );
		$this->page_construct('section_lists', $this->data);
	}
	public function category( $method = "", $page, $sec_key,  $code = "" ){
		
		if( $page == "" || $sec_key == "" ){
			redirect('admin/section/detail/xr/phenomenon');
		}
		$this->data['menu_active'] = 'section/category/'.$page.'/'.$sec_key;
		$this->data['param_page'] = $page;
		$this->data['param_sec_key'] = $sec_key;
		if( $method == "lists"){
			if( !check_permission( $this->session->userdata('user_type'), 'category') ){
				$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
				redirect('admin/dashboard');
			  }
			$this->data['lists'] = $this->section->loadCategoryList( $page, $sec_key );
			$this->page_construct( 'section_category', $this->data );
		}
		if( $method == "add"){
			if( !check_permission( $this->session->userdata('user_type'), 'create_category') ){
				$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
				redirect('admin/dashboard');
			  }
			$this->data['detail'] = array();
			$this->page_construct( 'category_form', $this->data );
		}
		if( $method == "edit"){
			if( !check_permission( $this->session->userdata('user_type'), 'edit_category') ){
				$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
				redirect('admin/dashboard');
			  }
			$this->data['detail'] = $this->section->loadCategoryDetail( $page, $sec_key, $code );
			$this->page_construct( 'category_form', $this->data );
		}
		
	}
	public function content( $method = "", $page, $sec_key, $code = "" ){
		if( $page == "" || $sec_key == "" ){
			redirect('admin/section/detail/xr/phenomenon');
		}
		$this->data['menu_active'] = 'section/content/'.$page.'/'.$sec_key;
		$this->data['param_page'] = $page;
		$this->data['param_sec_key'] = $sec_key;

		$this->data['categories'] = $this->section->loadCategoryList( $page, $sec_key );

		if( $method == "add"){
			$this->data['detail'] = array();
			$this->page_construct( 'section_content_form', $this->data );
		}else if( $method == "edit"){
			$this->data['detail'] = $this->section->loadListsDetail( $page, $sec_key, $code );
			$this->page_construct( 'section_content_form', $this->data );
		}else{
			$this->data['lists'] = $this->section->loadListing( $page, $sec_key, $code  );
			$this->page_construct( 'section_content', $this->data );
		}
		
	}
	public function footer( $page ){
		if( !check_permission( $this->session->userdata('user_type'), 'footer') ){
			$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
			redirect('admin/dashboard');
		  }
		$this->data['menu_active'] = 'section/footer/'.$page;
		$this->data['param_page'] = $page;
		$this->data['param_sec_key'] = "";

		$this->data['detail'] = $this->section->loadFooterDetail( $page );
		$this->page_construct( 'section_footer', $this->data );
	}

	public function saveDetailForm(){
		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';

		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'section' );
			$this->common->addLogs( "section", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "section", $data['code'], "update", "" );
		}
		$data['page'] = isset( $_POST['page'] ) ? $this->input->post('page') : '';
		$data['page_code'] = isset( $_POST['page_code'] ) ? $this->input->post('page_code') : '';
		$data['sec_key'] = isset( $_POST['sec_key'] ) ? $this->input->post('sec_key') : '';
		$data['section_name'] = isset( $_POST['section_name'] ) ? $this->input->post('section_name') : '';
		$data['section_type'] = isset( $_POST['section_type'] ) ? $this->input->post('section_type') : '';
		$data['content_layout'] = isset( $_POST['content_layout'] ) ? $this->input->post('content_layout') : '';
		$data['m_content_layout'] = isset( $_POST['m_content_layout'] ) ? $this->input->post('m_content_layout') : '';
		$data['design_type'] = isset( $_POST['design_type'] ) ? $this->input->post('design_type') : '';
		$data['design_template'] = isset( $_POST['design_template'] ) ? $this->input->post('design_template') : '';

		$data['tab_show'] = isset( $_POST['tab_show'] ) ? $this->input->post('tab_show') : '';
		$data['tab_setting'] = isset( $_POST['tab_setting'] ) ? $this->input->post('tab_setting') : '';
		$data['hl_set'] = isset( $_POST['hl_set'] ) ? $this->input->post('hl_set') : '';
		$data['btn_more'] = isset( $_POST['btn_more'] ) ? $this->input->post('btn_more') : '';
		
		
		$data['column_per_view'] = isset( $_POST['column_per_view'] ) ? $this->input->post('column_per_view') : '';
		$data['display_per_column'] = isset( $_POST['display_per_column'] ) ? $this->input->post('display_per_column') : 1;
		$data['video_id'] = isset( $_POST['video_id'] ) ? $this->input->post('video_id') : '';
		$data['column_id'] = isset( $_POST['column_id'] ) ? $this->input->post('column_id') : 0;
		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
		$data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';

		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
		$sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';

		$content = isset( $_POST['content'] ) ? $this->input->post('content') : '';
		$content_editor = isset( $_POST['content_editor'] ) ? $_POST['content_editor'] : '';
		$image_editor = isset( $_FILES['image_editor'] ) ? $_FILES['image_editor'] : array();

		$time = isset( $_POST['time'] ) ? $this->input->post('time') : '';
		$this->common->uploadEditorImage( $image_editor, $time );

		// Upload liberies config
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';

		$this->load->library('upload', $config);
		if( isset( $_POST['bg1_desktop_txt'] ) ){
			if( $this->upload->do_upload('bg1_desktop') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['bg1_desktop'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['bg1_desktop_txt']) and $_POST['bg1_desktop_txt'] == ""){
					$data['bg1_desktop'] = "";
				}
			}
		}
		if( isset( $_POST['bg1_mobile_txt'] ) ){
			if( $this->upload->do_upload('bg1_mobile') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['bg1_mobile'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['bg1_mobile_txt']) and $_POST['bg1_mobile_txt'] == ""){
					$data['bg1_mobile'] = "";
				}
			}
		}
		if( isset( $_POST['img1_desktop_txt'] ) ){
			if( $this->upload->do_upload('img1_desktop') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['img1_desktop'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['img1_desktop_txt']) and $_POST['img1_desktop_txt'] == ""){
					$data['img1_desktop'] = "";
				}
			}
		}
		if( isset( $_POST['img1_mobile_txt'] ) ){
			if( $this->upload->do_upload('img1_mobile') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['img1_mobile'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['img1_mobile_txt']) and $_POST['img1_mobile_txt'] == ""){
					$data['img1_mobile'] = "";
				}
			}
		}

		if( isset( $_POST['vido_thumb'] ) ){
			if( $this->upload->do_upload('vido_thumb') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['vido_thumb'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['vido_thumb_txt']) and $_POST['vido_thumb_txt'] == ""){
					$data['vido_thumb'] = "";
				}
			}
		}

		if( isset( $_POST['thumb'] ) ){
			if( $this->upload->do_upload('thumb') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['thumb'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['thumb_txt']) and $_POST['thumb_txt'] == ""){
					$data['thumb'] = "";
				}
			}
		}
		
		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('section');
			$result = $q->result_array();
			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
			$data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;
			$data['content'] = isset( $content[$key] ) ? $content[$key] : '' ;
			$data['content_editor'] = isset( $content_editor[$key] ) ? $content_editor[$key] : '' ;

			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('section', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('section', $data );
			}
		}
		$this->common->setAlert('success','Section has been saved.','Success !');
		redirect('admin/section/detail/'.$data['page'].'/'.$data['sec_key']);
	}
	public function saveSectionDetailForm(){
		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';

		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'section' );
			$this->common->addLogs( "section", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "section", $data['code'], "update", "" );
		}
		$data['page'] = isset( $_POST['page'] ) ? $this->input->post('page') : '';
		$data['page_code'] = isset( $_POST['page_code'] ) ? $this->input->post('page_code') : '';
		$data['sec_key'] = isset( $_POST['sec_key'] ) ? $this->input->post('sec_key') : '';
		$data['section_name'] = isset( $_POST['section_name'] ) ? $this->input->post('section_name') : '';
		$data['section_layout'] = isset( $_POST['section_layout'] ) ? $this->input->post('section_layout') : '';
		$data['section_type'] = isset( $_POST['section_type'] ) ? $this->input->post('section_type') : '';
		$data['content_layout'] = isset( $_POST['content_layout'] ) ? $this->input->post('content_layout') : '';
		$data['m_content_layout'] = isset( $_POST['m_content_layout'] ) ? $this->input->post('m_content_layout') : '';
		$data['design_type'] = isset( $_POST['design_type'] ) ? $this->input->post('design_type') : '';
		$data['design_template'] = isset( $_POST['design_template'] ) ? $this->input->post('design_template') : '';

		$data['tab_show'] = isset( $_POST['tab_show'] ) ? $this->input->post('tab_show') : '';
		$data['tab_setting'] = isset( $_POST['tab_setting'] ) ? $this->input->post('tab_setting') : '';
		$data['hl_set'] = isset( $_POST['hl_set'] ) ? $this->input->post('hl_set') : '';
		$data['is_tags'] = isset( $_POST['is_tags'] ) ? $this->input->post('is_tags') : '';
		$data['btn_more'] = isset( $_POST['btn_more'] ) ? $this->input->post('btn_more') : '';

		$data['acc_type'] = isset( $_POST['acc_type'] ) ? $this->input->post('acc_type') : 0;
		$data['acc_switch'] = isset( $_POST['acc_switch'] ) ? $this->input->post('acc_switch') : 0;

		$data['link_type'] = isset( $_POST['link_type'] ) ? $this->input->post('link_type') : '';
		$data['page_internal_code'] = isset( $_POST['page_internal_code'] ) ? $this->input->post('page_internal_code') : '';
		$data['link_out'] = isset( $_POST['link_out'] ) ? $this->input->post('link_out') : '';
		
		

		$data['column_per_view'] = isset( $_POST['column_per_view'] ) ? $this->input->post('column_per_view') : '';
		$data['display_per_column'] = isset( $_POST['display_per_column'] ) ? $this->input->post('display_per_column') : 1;
		$data['video_id'] = isset( $_POST['video_id'] ) ? $this->input->post('video_id') : '';
		$data['video_type'] = isset( $_POST['video_type'] ) ? $this->input->post('video_type') : '';

		$data['column_id'] = isset( $_POST['column_id'] ) ? $this->input->post('column_id') : 0;
		$data['widget'] = isset( $_POST['widget'] ) ? $this->input->post('widget') : '';

		$data['sec_content_position_ver'] = isset( $_POST['sec_content_position_ver'] ) ? $this->input->post('sec_content_position_ver') : '';
		$data['sec_content_position_hoz'] = isset( $_POST['sec_content_position_hoz'] ) ? $this->input->post('sec_content_position_hoz') : '';
		$data['bg_color'] = isset( $_POST['bg_color'] ) ? $this->input->post('bg_color') : '';
		$data['bg_position_hoz'] = isset( $_POST['bg_position_hoz'] ) ? $this->input->post('bg_position_hoz') : '';
		$data['bg_position_ver'] = isset( $_POST['bg_position_ver'] ) ? $this->input->post('bg_position_ver') : '';
		$data['bg_repeat'] = isset( $_POST['bg_repeat'] ) ? $this->input->post('bg_repeat') : '';
		$data['bg_fixed'] = isset( $_POST['bg_fixed'] ) ? $this->input->post('bg_fixed') : '';
		$data['bg_size'] = isset( $_POST['bg_size'] ) ? $this->input->post('bg_size') : '';

		$data['gen_height'] = isset( $_POST['gen_height'] ) ? $this->input->post('gen_height') : 0;
		$data['list_limit'] = isset( $_POST['list_limit'] ) ? $this->input->post('list_limit') : 0;
		
		$data['pd_desktop'] = isset( $_POST['pd_desktop'] ) ? json_encode($this->input->post('pd_desktop')) : '';
		$data['pd_mobile'] = isset( $_POST['pd_mobile'] ) ? json_encode($this->input->post('pd_mobile')) : '';
		$data['mg_desktop'] = isset( $_POST['mg_desktop'] ) ? json_encode($this->input->post('mg_desktop')) : '';
		$data['mg_mobile'] = isset( $_POST['mg_mobile'] ) ? json_encode($this->input->post('mg_mobile')) : '';

		$data['listing_alignment'] = isset( $_POST['listing_alignment'] ) ? $this->input->post('listing_alignment') : '';

		$data['custom_class'] = isset( $_POST['custom_class'] ) ? $this->input->post('custom_class') : '';

		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : ''; 

		$data['sec_title_alignment'] = isset( $_POST['sec_title_alignment'] ) ? $this->input->post('sec_title_alignment') : '';
		$data['cate_title_alignment'] = isset( $_POST['cate_title_alignment'] ) ? $this->input->post('cate_title_alignment') : '';

		$data['custom_css'] = isset( $_POST['custom_css'] ) ? $this->input->post('custom_css') : '';
		$data['sort_option'] = isset( $_POST['sort_option'] ) ? $this->input->post('sort_option') : 'asc';

		$data['section_template'] = isset( $_POST['section_template'] ) ? $this->input->post('section_template') : '';
		$data['section_template'] = ( $data['section_type'] == 'listing' && $data['section_template'] == '' ) ? 'article' : $data['section_template'];
 
		
		//print_r( $data );
		//exit;
		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
		$sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';
		$btn_text = isset( $_POST['btn_text'] ) ? $this->input->post('btn_text') : '';
		$content = isset( $_POST['content'] ) ? $this->input->post('content') : '';
		$content_editor = isset( $_POST['content_editor'] ) ? $_POST['content_editor'] : '';
		$image_editor = isset( $_FILES['image_editor'] ) ? $_FILES['image_editor'] : array();

		$time = isset( $_POST['time'] ) ? $this->input->post('time') : '';
		$this->common->uploadEditorImage( $image_editor, $time );

		// Upload liberies config
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';

		$this->load->library('upload', $config);

		if( isset( $_POST['video_file_txt'] ) ){
			if( $this->upload->do_upload('video_file') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['video_file'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['video_file_txt']) and $_POST['video_file_txt'] == ""){
					$data['video_file'] = "";
				}
			}
		}

		if( isset( $_POST['bg1_desktop_txt'] ) ){
			if( $this->upload->do_upload('bg1_desktop') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['bg1_desktop'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['bg1_desktop_txt']) and $_POST['bg1_desktop_txt'] == ""){
					$data['bg1_desktop'] = "";
				}
			}
		}
		if( isset( $_POST['bg1_mobile_txt'] ) ){
			if( $this->upload->do_upload('bg1_mobile') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['bg1_mobile'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['bg1_mobile_txt']) and $_POST['bg1_mobile_txt'] == ""){
					$data['bg1_mobile'] = "";
				}
			}
		}
		if( isset( $_POST['img1_desktop_txt'] ) ){
			if( $this->upload->do_upload('img1_desktop') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['img1_desktop'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['img1_desktop_txt']) and $_POST['img1_desktop_txt'] == ""){
					$data['img1_desktop'] = "";
				}
			}
		}
		if( isset( $_POST['img1_mobile_txt'] ) ){
			if( $this->upload->do_upload('img1_mobile') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['img1_mobile'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['img1_mobile_txt']) and $_POST['img1_mobile_txt'] == ""){
					$data['img1_mobile'] = "";
				}
			}
		}

		if( isset( $_POST['video_thumb_txt'] ) ){
			if( $this->upload->do_upload('video_thumb') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['video_thumb'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['video_thumb_txt']) and $_POST['video_thumb_txt'] == ""){
					$data['video_thumb'] = "";
				}
			}

			if( $this->upload->do_upload('video_thumb_m') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['video_thumb_m'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['video_thumb_txt']) and $_POST['video_thumb_txt'] == ""){
					$data['video_thumb_m'] = "";
				}
			}
		}


		if( isset( $_POST['thumb_txt'] ) ){
			if( $this->upload->do_upload('thumb') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['thumb'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['thumb_txt']) and $_POST['thumb_txt'] == ""){
					$data['thumb'] = "";
				}
			}
		}

		if( isset( $_POST['ele_img1_txt'] ) ){
			if( $this->upload->do_upload('ele_img1') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['ele_img1'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['ele_img1_txt']) and $_POST['ele_img1_txt'] == ""){
					$data['ele_img1'] = "";
				}
			}
		}
		if( isset( $_POST['ele_img2_txt'] ) ){
			if( $this->upload->do_upload('ele_img2') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['ele_img2'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['ele_img2_txt']) and $_POST['ele_img2_txt'] == ""){
					$data['ele_img2'] = "";
				}
			}
		}
		if( isset( $_POST['ele_img3_txt'] ) ){
			if( $this->upload->do_upload('ele_img3') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['ele_img3'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['ele_img3_txt']) and $_POST['ele_img3_txt'] == ""){
					$data['ele_img3'] = "";
				}
			}
		}
		if( isset( $_POST['ele_img4_txt'] ) ){
			if( $this->upload->do_upload('ele_img4') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['ele_img4'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['ele_img4_txt']) and $_POST['ele_img4_txt'] == ""){
					$data['ele_img4'] = "";
				}
			}
		}
		
		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('section');
			$result = $q->result_array();
			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
			$data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;
			$data['content'] = isset( $content[$key] ) ? $content[$key] : '' ;
			$data['content_editor'] = isset( $content_editor[$key] ) ? $content_editor[$key] : '' ;
			$data['btn_text'] = isset( $btn_text[$key] ) ? $btn_text[$key] : '' ;

			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('section', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('section', $data );
			}
		}
		$this->common->setAlert('success','Section has been saved.','Success !');
		redirect('admin/section/edit/'.$data['page_code'].'/'.$data['code']);
	}

	public function previewSectionDetailForm(){
		$data = array();
		$data['preview'] = isset( $_POST['preview'] ) ? $this->input->post('preview') : '';
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';

		//addLogs
		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'section_preview' );
		}
		$this->common->addLogs( "section_preview", $data['code'], "preview", "" );

		$data['page'] = isset( $_POST['page'] ) ? $this->input->post('page') : '';
		$data['page_code'] = isset( $_POST['page_code'] ) ? $this->input->post('page_code') : '';
		$data['sec_key'] = isset( $_POST['sec_key'] ) ? $this->input->post('sec_key') : '';
		$data['section_name'] = isset( $_POST['section_name'] ) ? $this->input->post('section_name') : '';
		$data['section_layout'] = isset( $_POST['section_layout'] ) ? $this->input->post('section_layout') : '';
		$data['section_type'] = isset( $_POST['section_type'] ) ? $this->input->post('section_type') : '';
		$data['content_layout'] = isset( $_POST['content_layout'] ) ? $this->input->post('content_layout') : '';
		$data['m_content_layout'] = isset( $_POST['m_content_layout'] ) ? $this->input->post('m_content_layout') : '';
		$data['design_type'] = isset( $_POST['design_type'] ) ? $this->input->post('design_type') : '';
		$data['design_template'] = isset( $_POST['design_template'] ) ? $this->input->post('design_template') : '';

		$data['tab_show'] = isset( $_POST['tab_show'] ) ? $this->input->post('tab_show') : '';
		$data['tab_setting'] = isset( $_POST['tab_setting'] ) ? $this->input->post('tab_setting') : '';
		$data['hl_set'] = isset( $_POST['hl_set'] ) ? $this->input->post('hl_set') : '';
		$data['is_tags'] = isset( $_POST['is_tags'] ) ? $this->input->post('is_tags') : '';
		$data['btn_more'] = isset( $_POST['btn_more'] ) ? $this->input->post('btn_more') : '';
		
		$data['acc_type'] = isset( $_POST['acc_type'] ) ? $this->input->post('acc_type') : 0;
		$data['acc_switch'] = isset( $_POST['acc_switch'] ) ? $this->input->post('acc_switch') : 0;

		$data['link_type'] = isset( $_POST['link_type'] ) ? $this->input->post('link_type') : '';
		$data['page_internal_code'] = isset( $_POST['page_internal_code'] ) ? $this->input->post('page_internal_code') : '';
		$data['link_out'] = isset( $_POST['link_out'] ) ? $this->input->post('link_out') : '';
		
		

		$data['column_per_view'] = isset( $_POST['column_per_view'] ) ? $this->input->post('column_per_view') : '';
		$data['display_per_column'] = isset( $_POST['display_per_column'] ) ? $this->input->post('display_per_column') : 1;
		$data['video_id'] = isset( $_POST['video_id'] ) ? $this->input->post('video_id') : '';
		$data['video_type'] = isset( $_POST['video_type'] ) ? $this->input->post('video_type') : '';

		$data['column_id'] = isset( $_POST['column_id'] ) ? $this->input->post('column_id') : 0;
		$data['widget'] = isset( $_POST['widget'] ) ? $this->input->post('widget') : '';

		$data['sec_content_position_ver'] = isset( $_POST['sec_content_position_ver'] ) ? $this->input->post('sec_content_position_ver') : '';
		$data['sec_content_position_hoz'] = isset( $_POST['sec_content_position_hoz'] ) ? $this->input->post('sec_content_position_hoz') : '';
		$data['bg_color'] = isset( $_POST['bg_color'] ) ? $this->input->post('bg_color') : '';
		$data['bg_position_hoz'] = isset( $_POST['bg_position_hoz'] ) ? $this->input->post('bg_position_hoz') : '';
		$data['bg_position_ver'] = isset( $_POST['bg_position_ver'] ) ? $this->input->post('bg_position_ver') : '';
		$data['bg_repeat'] = isset( $_POST['bg_repeat'] ) ? $this->input->post('bg_repeat') : '';
		$data['bg_fixed'] = isset( $_POST['bg_fixed'] ) ? $this->input->post('bg_fixed') : '';
		$data['bg_size'] = isset( $_POST['bg_size'] ) ? $this->input->post('bg_size') : '';

		$data['gen_height'] = isset( $_POST['gen_height'] ) ? $this->input->post('gen_height') : 0;
		$data['list_limit'] = isset( $_POST['list_limit'] ) ? $this->input->post('list_limit') : 0;
		
		$data['pd_desktop'] = isset( $_POST['pd_desktop'] ) ? json_encode($this->input->post('pd_desktop')) : '';
		$data['pd_mobile'] = isset( $_POST['pd_mobile'] ) ? json_encode($this->input->post('pd_mobile')) : '';
		$data['mg_desktop'] = isset( $_POST['mg_desktop'] ) ? json_encode($this->input->post('mg_desktop')) : '';
		$data['mg_mobile'] = isset( $_POST['mg_mobile'] ) ? json_encode($this->input->post('mg_mobile')) : '';

		$data['listing_alignment'] = isset( $_POST['listing_alignment'] ) ? $this->input->post('listing_alignment') : '';

		$data['custom_class'] = isset( $_POST['custom_class'] ) ? $this->input->post('custom_class') : '';

		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';

		$data['sec_title_alignment'] = isset( $_POST['sec_title_alignment'] ) ? $this->input->post('sec_title_alignment') : '';
		$data['cate_title_alignment'] = isset( $_POST['cate_title_alignment'] ) ? $this->input->post('cate_title_alignment') : '';

		$data['custom_css'] = isset( $_POST['custom_css'] ) ? $this->input->post('custom_css') : '';
		$data['sort_option'] = isset( $_POST['sort_option'] ) ? $this->input->post('sort_option') : 'asc';
		
		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
		$sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';
		$btn_text = isset( $_POST['btn_text'] ) ? $this->input->post('btn_text') : '';
		$content = isset( $_POST['content'] ) ? $this->input->post('content') : '';
		$content_editor = isset( $_POST['content_editor'] ) ? $_POST['content_editor'] : '';
		$image_editor = isset( $_FILES['image_editor'] ) ? $_FILES['image_editor'] : array();

		$time = isset( $_POST['time'] ) ? $this->input->post('time') : '';
		$this->common->uploadEditorImage( $image_editor, $time );

		// Upload liberies config
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';

		$this->load->library('upload', $config);

		if( isset( $_POST['video_file_txt'] ) ){
			if( $this->upload->do_upload('video_file') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['video_file'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['video_file_txt']) and $_POST['video_file_txt'] == ""){
					$data['video_file'] = "";
				}
			}
		}

		if( isset( $_POST['bg1_desktop_txt'] ) ){
			if( $this->upload->do_upload('bg1_desktop') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['bg1_desktop'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['bg1_desktop_txt']) and $_POST['bg1_desktop_txt'] == ""){
					$data['bg1_desktop'] = "";
				}
			}
		}
		if( isset( $_POST['bg1_mobile_txt'] ) ){
			if( $this->upload->do_upload('bg1_mobile') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['bg1_mobile'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['bg1_mobile_txt']) and $_POST['bg1_mobile_txt'] == ""){
					$data['bg1_mobile'] = "";
				}
			}
		}
		if( isset( $_POST['img1_desktop_txt'] ) ){
			if( $this->upload->do_upload('img1_desktop') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['img1_desktop'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['img1_desktop_txt']) and $_POST['img1_desktop_txt'] == ""){
					$data['img1_desktop'] = "";
				}
			}
		}
		if( isset( $_POST['img1_mobile_txt'] ) ){
			if( $this->upload->do_upload('img1_mobile') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['img1_mobile'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['img1_mobile_txt']) and $_POST['img1_mobile_txt'] == ""){
					$data['img1_mobile'] = "";
				}
			}
		}

		if( isset( $_POST['video_thumb_txt'] ) ){
			if( $this->upload->do_upload('video_thumb') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['video_thumb'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['video_thumb_txt']) and $_POST['video_thumb_txt'] == ""){
					$data['video_thumb'] = "";
				}
			}

			if( $this->upload->do_upload('video_thumb_m') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['video_thumb_m'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['video_thumb_txt']) and $_POST['video_thumb_txt'] == ""){
					$data['video_thumb_m'] = "";
				}
			}
		}


		if( isset( $_POST['thumb_txt'] ) ){
			if( $this->upload->do_upload('thumb') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['thumb'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['thumb_txt']) and $_POST['thumb_txt'] == ""){
					$data['thumb'] = "";
				}
			}
		}

		if( isset( $_POST['ele_img1_txt'] ) ){
			if( $this->upload->do_upload('ele_img1') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['ele_img1'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['ele_img1_txt']) and $_POST['ele_img1_txt'] == ""){
					$data['ele_img1'] = "";
				}
			}
		}
		if( isset( $_POST['ele_img2_txt'] ) ){
			if( $this->upload->do_upload('ele_img2') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['ele_img2'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['ele_img2_txt']) and $_POST['ele_img2_txt'] == ""){
					$data['ele_img2'] = "";
				}
			}
		}
		if( isset( $_POST['ele_img3_txt'] ) ){
			if( $this->upload->do_upload('ele_img3') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['ele_img3'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['ele_img3_txt']) and $_POST['ele_img3_txt'] == ""){
					$data['ele_img3'] = "";
				}
			}
		}
		if( isset( $_POST['ele_img4_txt'] ) ){
			if( $this->upload->do_upload('ele_img4') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['ele_img4'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['ele_img4_txt']) and $_POST['ele_img4_txt'] == ""){
					$data['ele_img4'] = "";
				}
			}
		}
		
		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('preview', $data['preview'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('section_preview');
			$result = $q->result_array();
			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
			$data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;
			$data['content'] = isset( $content[$key] ) ? $content[$key] : '' ;
			$data['content_editor'] = isset( $content_editor[$key] ) ? $content_editor[$key] : '' ;
			$data['btn_text'] = isset( $btn_text[$key] ) ? $btn_text[$key] : '' ;

			if( sizeof($result) > 0 ){

				// Delete file old
				$row = $result[0];
				$img_arr = array('video_file', 'bg1_desktop', 'bg1_mobile', 'img1_desktop', 'img1_mobile', 'video_thumb', 'video_thumb_m', 'thumb', 'ele_img1', 'ele_img2', 'ele_img3', 'ele_img3', 'ele_img4');
				foreach ($img_arr as $key => $val) {
					if ( isset($data[$val]) and $data[$val] != '' and $row[$val] != $data[$val] ) {
						if (file_exists(editor_upload_path().$row[$val])) {
							unlink(editor_upload_path().$row[$val]);
						}
					}
				}
				// End delete file old

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('preview', $data['preview'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('section_preview', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('section_preview', $data );
			}
		}

		$ar_result = array();
		$ar_result['rs'] = true;
		$page_list =  $this->common->loadPageDetail($data['page_code']);
		$page_slug = $page_list[DEFAULT_LANG]['page_slug'];
		$ar_result['url'] = base_url().'th/preview/'.$page_slug.'?preview_id='.$data['preview'];
		echo json_encode( $ar_result );
	}

	public function saveCategoryForm(){

		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		//$data['cate_permission'] = isset( $_POST['cate_permission'] ) ? implode($this->input->post('cate_permission'),",") : '';
		$data['must_be_approve'] = isset( $_POST['must_be_approve'] ) ? $this->input->post('must_be_approve') : 0;
		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'section_cate' );
			$data['sec_code'] = isset( $_POST['sec_code'] ) ? json_encode( array( $this->input->post('sec_code') ) ) : '';
			$this->common->addLogs( "section_cate", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "section_cate", $data['code'], "update", "" );
			$sec_code = isset( $_POST['sec_code'] ) ? $this->input->post('sec_code')  : '';
			$this->db->where('lang', DEFAULT_LANG );
			$this->db->where('code', $data['code'] );
			$q = $this->db->get('section_cate');
			$result = $q->result_array();
			if( sizeof( $result ) > 0 ){
				$ar_sec_code = json_decode( $result[0]['sec_code'], true );
				if ( !in_array($sec_code, $ar_sec_code)){
					array_push( $ar_sec_code , $sec_code);
					$data['sec_code'] = json_encode( $ar_sec_code );
				}
			}else{
				$data['sec_code'] = isset( $_POST['sec_code'] ) ? json_encode( array( $this->input->post('sec_code') ) ) : '';
			}
		}
		
		//$data['sec_code'] = isset( $_POST['sec_code'] ) ? json_encode( array( $this->input->post('sec_code') ) ) : '';
		$data['page'] = isset( $_POST['page'] ) ? $this->input->post('page') : '';
		$data['sec_key'] = isset( $_POST['sec_key'] ) ? $this->input->post('sec_key') : '';

		$data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';

		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';

		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
		$sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';

		// Upload liberies config
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';

		$this->load->library('upload', $config);

		if( isset( $_POST['image_txt'] ) ){
			if( $this->upload->do_upload('image') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['image'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['image_txt']) and $_POST['image_txt'] == ""){
					$data['image'] = "";
				}
			}
		}

		$section_template = isset( $_POST['section_template'] ) ? $this->input->post('section_template') : '';
		if ( $section_template == 'solution' ) {
			$prefix_title = isset( $_POST['prefix_title'] ) ? $this->input->post('prefix_title') : '';
			
			if( isset( $_POST['thumb_image_txt'] ) ){
				if( $this->upload->do_upload('thumb_image') ){
					$upload = array('upload_data' => $this->upload->data());
					$data['thumb_image'] = $upload['upload_data']['file_name'];
				}else{
					if( isset($_POST['thumb_image_txt']) and $_POST['thumb_image_txt'] == ""){
						$data['thumb_image'] = "";
					}
				}
			}

			if( isset( $_POST['bg_d_txt'] ) ){
				if( $this->upload->do_upload('bg_d') ){
					$upload = array('upload_data' => $this->upload->data());
					$data['bg_d'] = $upload['upload_data']['file_name'];
				}else{
					if( isset($_POST['bg_d_txt']) and $_POST['bg_d_txt'] == ""){
						$data['bg_d'] = "";
					}
				}
			}

			if( isset( $_POST['bg_m_txt'] ) ){
				if( $this->upload->do_upload('bg_m') ){
					$upload = array('upload_data' => $this->upload->data());
					$data['bg_m'] = $upload['upload_data']['file_name'];
				}else{
					if( isset($_POST['bg_m_txt']) and $_POST['bg_m_txt'] == ""){
						$data['bg_m'] = "";
					}
				}
			}

			if( isset( $_POST['banner_top_d_txt'] ) ){
				if( $this->upload->do_upload('banner_top_d') ){
					$upload = array('upload_data' => $this->upload->data());
					$data['banner_top_d'] = $upload['upload_data']['file_name'];
				}else{
					if( isset($_POST['banner_top_d_txt']) and $_POST['banner_top_d_txt'] == ""){
						$data['banner_top_d'] = "";
					}
				}
			}

			if( isset( $_POST['banner_top_m_txt'] ) ){
				if( $this->upload->do_upload('banner_top_m') ){
					$upload = array('upload_data' => $this->upload->data());
					$data['banner_top_m'] = $upload['upload_data']['file_name'];
				}else{
					if( isset($_POST['banner_top_m_txt']) and $_POST['banner_top_m_txt'] == ""){
						$data['banner_top_m'] = "";
					}
				}
			}
		}
		

		/*
		/* Add category slug
		*/
		$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
		if ( $slug == '' ) {
			$slug = time();
		} else {
			$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
		}

		if( $data['code'] == '' ){
			$this->db->where('slug', $slug );
		} else {
			$this->db->where('code !=', $data['code'] );
			$this->db->where('slug', $slug );
		}
		$q = $this->db->get('section_cate');
		$resultx = $q->result_array();
		if( sizeof( $resultx ) > 0 ){
			$data['slug'] = $slug.'-'.time();
		} else {
			$data['slug'] = $slug;
		}

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('section_cate');
			$result = $q->result_array();
			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
			$data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;

			if ( $section_template == 'solution' ) {
				$data['prefix_title'] = isset( $prefix_title[$key] ) ? $prefix_title[$key] : '' ;
			}

			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('section_cate', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('section_cate', $data );
			}
		}
		$this->common->setAlert('success','Category has been saved.','Success !');
		redirect('admin/section/categories/edit/'.$_POST['sec_code'].'/'.$data['code']);
	}
	public function useExitingCate(){
		
		$data = array();
		$sec_code = isset( $_POST['sec_code'] ) ? $this->input->post('sec_code') : '';
		$cate_code = isset( $_POST['cate_code'] ) ? $this->input->post('cate_code') : '';

		$this->db->where('code' , $cate_code );
		$q = $this->db->get('section_cate');
		$result = $q->result_array();
		$ar_sec_code = array();
		if( $result[0]['sec_code'] != "" ){
			$ar_sec_code = json_decode( $result[0]['sec_code'], TRUE );
		}
		
		if( !in_array( $sec_code , $ar_sec_code ) ){
			array_push( $ar_sec_code , $sec_code);
		}

		$data['sec_code'] = json_encode($ar_sec_code);

		/*
		/* Add category slug
		*/
		/*$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
		if ( $slug == '' ) {
			$slug = time();
		} else {
			$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
		}

		if( $cate_code == '' ){
			$this->db->where('slug', $slug );
		} else {
			$this->db->where('code !=', $cate_code );
			$this->db->where('slug', $slug );
		}
		$q = $this->db->get('section_cate');
		$resultx = $q->result_array();
		if( sizeof( $resultx ) > 0 ){
			$data['slug'] = $slug.'-'.time();
		} else {
			$data['slug'] = $slug;
		}*/
		
		$this->db->where('code' , $cate_code );
		$this->db->update('section_cate', $data);
		$this->common->setAlert('success','Category has been saved.','Success !');
		redirect('admin/section/categories/lists/'.$sec_code);
	}
	public function saveContentForm(){
		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';

		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'section_lists' );
			$this->common->addLogs( "section_lists", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "section_lists", $data['code'], "update", "" );
		}
		$data['cate_code'] = isset( $_POST['cate_code'] ) ? $this->input->post('cate_code') : '';
		$data['page'] = isset( $_POST['page'] ) ? $this->input->post('page') : '';
		$data['sec_key'] = isset( $_POST['sec_key'] ) ? $this->input->post('sec_key') : '';

		$data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';

		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
		$data['link_out'] = isset( $_POST['link_out'] ) ? $this->input->post('link_out') : '';

		if( isset( $_POST['post_date'] ) ){
			if( $_POST['post_date'] != "" ){
				$data['post_date'] = $this->input->post('post_date');
			}else{
				$data['post_date'] = NULL;
			}
		}
		
		$data['post_time'] = isset( $_POST['post_time'] ) ? $this->input->post('post_time') : NULL;

		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
		$sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';

		// Upload liberies config
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';

		$this->load->library('upload', $config);
		if( $this->upload->do_upload('image') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['image'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['image_txt']) and $_POST['image_txt'] == ""){
				$data['image'] = "";
			}
		}
		if( $this->upload->do_upload('image2') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['image2'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['image_txt']) and $_POST['image_txt'] == ""){
				$data['image2'] = "";
			}
		}

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('section_lists');
			$result = $q->result_array();
			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
			$data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;

			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('section_lists', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('section_lists', $data );
			}
		}
		$this->common->setAlert('success','Content has been saved.','Success !');
		redirect('admin/section/content/edit/'.$data['page'].'/'.$data['sec_key'].'/'.$data['code']);
	}
	public function saveListingForm(){
		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		$data['approve_status'] = isset( $_POST['approve_status'] ) ? $this->input->post('approve_status') : '';
		// if( (isApprover($this->session->userdata('user_type')) or $cate_detail[DEFAULT_LANG]['must_be_approve'] == '0') and $data['code'] == '' ){
		// 	$data['approve_status'] = 2;
		// }
		$sec_code = isset( $_POST['sec_code'] ) ? $this->input->post('sec_code') : '';
		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'section_lists' );
			$this->common->addLogs( "section_lists", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "section_lists", $data['code'], "update", "" );
		}
		$data['cate_code'] = isset( $_POST['cate_code'] ) ? $this->input->post('cate_code') : '';
		$data['page'] = isset( $_POST['page'] ) ? $this->input->post('page') : '';
		$data['sec_key'] = isset( $_POST['sec_key'] ) ? $this->input->post('sec_key') : '';

		$data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';

		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
		$data['link_out'] = isset( $_POST['link_out'] ) ? $this->input->post('link_out') : '';
		$data['link_type'] = isset( $_POST['link_type'] ) ? $this->input->post('link_type') : '';
		$data['btn_show'] = isset( $_POST['link_out'] ) ? $this->input->post('btn_show') : '';

		$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
		if ( $slug == '' ) {
			$slug = time();
		} else {
			$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
		}

		if( $data['code'] == '' ){
			$this->db->where('slug', $slug );
		} else {
			$this->db->where('code !=', $data['code'] );
			$this->db->where('slug', $slug );
		}
		$q = $this->db->get('section_lists');
		$resultx = $q->result_array();
		if( sizeof( $resultx ) > 0 ){
			$data['slug'] = $slug.'-'.time();
		} else {
			$data['slug'] = $slug;
		}

		$data['tags'] = isset( $_POST['tags'] ) ? $this->input->post('tags') : '';
		$data['is_highlight'] = isset( $_POST['is_highlight'] ) ? $this->input->post('is_highlight') : '';

		if( isset( $_POST['post_date'] ) ){
			if( $_POST['post_date'] != "" ){ 
				$data['post_date'] = $this->input->post('post_date');
			}else{
				$data['post_date'] = NULL;
			}
		}
		
		//$data['post_date'] = isset( $_POST['post_date'] ) ? $this->input->post('post_date') : NULL;
		$data['post_time'] = isset( $_POST['post_time'] ) ? $this->input->post('post_time') : NULL;

		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
		$sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';
		$price = isset( $_POST['price'] ) ? $this->input->post('price') : '';
		$description = isset( $_POST['description'] ) ? $this->input->post('description') : '';
		$btn_text = isset( $_POST['btn_text'] ) ? $this->input->post('btn_text') : '';
		$content = isset( $_POST['content'] ) ? $this->input->post('content') : '';
		$content_editor = isset( $_POST['content_editor'] ) ? $_POST['content_editor'] : '';

		$image_editor = isset( $_FILES['image_editor'] ) ? $_FILES['image_editor'] : array();
		$time = isset( $_POST['time'] ) ? $this->input->post('time') : '';
		$this->common->uploadEditorImage( $image_editor, $time );

		$config = array();
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';

		$this->load->library('upload', $config);

		if( $this->upload->do_upload('image') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['image'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['image_txt']) and $_POST['image_txt'] == ""){
				$data['image'] = "";
			}
		}

		//Note: This code not use. 
		if( $this->upload->do_upload('image2') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['image2'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['image_txt']) and $_POST['image_txt'] == ""){
				$data['image2'] = "";
			}
		}

		if( $this->upload->do_upload('hero_banner') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['hero_banner'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['hero_banner_txt']) and $_POST['hero_banner_txt'] == ""){
				$data['hero_banner'] = "";
			}
		}
		if( $this->upload->do_upload('hero_banner_m') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['hero_banner_m'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['hero_banner_m_txt']) and $_POST['hero_banner_m_txt'] == ""){
				$data['hero_banner_m'] = "";
			}
		}

		// Editor approve
		if ( ($_POST['is_approve'] == 1) and $this->input->post('approve_status') != 2  ) {
			$data['approve_status'] = 1;
		}

		$data['submit_date'] = date('Y-m-d H:i:s') ;

		$post_date = (isset( $_POST['published_date'] ) and $_POST['published_date'] != '') ? $this->input->post('published_date') : '';
		$post_time = (isset( $_POST['published_time'] ) and $_POST['published_time'] != '' ) ? $this->input->post('published_time') : date('H:i:s');
		if ( $post_date == '' ) {
			$post_date = date('Y-m-d');
		} else {
			$post_date = explode('-', $post_date);
			$post_date = ($post_date[0]-543).'-'.$post_date[1].'-'.$post_date[2];
		}

		$data['published_date'] = $post_date.' '.$post_time;

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('section_lists');
			$result = $q->result_array();
			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
			$data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;
			$data['price'] = isset( $price[$key] ) ? $price[$key] : '' ;
			$data['description'] = isset( $description[$key] ) ? $description[$key] : '' ;
			$data['btn_text'] = isset( $btn_text[$key] ) ? $btn_text[$key] : '' ;
			$data['content'] = isset( $content[$key] ) ? $content[$key] : '' ;
			$data['content_editor'] = isset( $content_editor[$key] ) ? $content_editor[$key] : '' ;
			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('section_lists', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('section_lists', $data );
			}
		}

		// Seo data
		$data_seo = array();
		
		$data_seo['code'] = isset( $_POST['seo_code'] ) ? $this->input->post('seo_code') : '';
		if( $data_seo['code'] == '' ){
			$data_seo['code'] = $this->common->get_code( 'seo' );
		}
		$data_seo['seo_type'] = isset( $_POST['seo_type'] ) ? $this->input->post('seo_type') : '';
		$data_seo['page'] = $data['slug'];
		$data_seo['page_title'] = isset( $_POST['page_title'] ) ? $this->input->post('page_title') : '';
		$data_seo['meta_title'] = isset( $_POST['meta_title'] ) ? $this->input->post('meta_title') : '';
		$data_seo['meta_desc'] = isset( $_POST['meta_desc'] ) ? $this->input->post('meta_desc') : '';
		$data_seo['meta_keyword'] = isset( $_POST['meta_keyword'] ) ? $this->input->post('meta_keyword') : '';
		
		//Upload liberies config
		if( $this->upload->do_upload('share_img') ){
		  $upload = array('upload_data' => $this->upload->data());
		  $data_seo['share_img'] = $upload['upload_data']['file_name'];
		} else {
			if( $_POST['share_img_txt'] == ""){
				$data_seo['share_img'] = "";
			}
		}
		$this->common->updateSEO( $data_seo );
		$this->common->setAlert('success','Listing has been saved.','Success !');
		//echo 'admin/section/listing/edit/'.$sec_code.'/'.$data['cate_code'].'/'.$data['code'];
		redirect('admin/section/listing/edit/'.$sec_code.'/'.$data['cate_code'].'/'.$data['code']);
	}
	public function previewListingForm(){
		$data = array();
		$data['preview'] = isset( $_POST['preview'] ) ? $this->input->post('preview') : '';
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		$sec_code = isset( $_POST['sec_code'] ) ? $this->input->post('sec_code') : '';
		//addLogs
		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'section_lists_preview' );
		}
		$this->common->addLogs( "section_lists_preview", $data['code'], "preview", "" );

		$data['cate_code'] = isset( $_POST['cate_code'] ) ? $this->input->post('cate_code') : '';
		$data['page'] = isset( $_POST['page'] ) ? $this->input->post('page') : '';
		$data['sec_key'] = isset( $_POST['sec_key'] ) ? $this->input->post('sec_key') : '';

		$data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';

		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
		$data['link_out'] = isset( $_POST['link_out'] ) ? $this->input->post('link_out') : '';
		$data['link_type'] = isset( $_POST['link_type'] ) ? $this->input->post('link_type') : '';
		$data['btn_show'] = isset( $_POST['link_out'] ) ? $this->input->post('btn_show') : '';

		$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
		if ( $slug == '' ) {
			$slug = time();
		} else {
			$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
		}

		if( $data['code'] == '' ){
			$this->db->where('slug', $slug );
		} else {
			$this->db->where('code !=', $data['code'] );
			$this->db->where('slug', $slug );
		}
		$q = $this->db->get('section_lists_preview');
		$resultx = $q->result_array();
		if( sizeof( $resultx ) > 0 ){
			$data['slug'] = $slug.'-'.time();
		} else {
			$data['slug'] = $slug;
		}

		$data['tags'] = isset( $_POST['tags'] ) ? $this->input->post('tags') : '';
		$data['is_highlight'] = isset( $_POST['is_highlight'] ) ? $this->input->post('is_highlight') : '';

		if( isset( $_POST['post_date'] ) ){
			if( $_POST['post_date'] != "" ){ 
				$data['post_date'] = $this->input->post('post_date');
			}else{
				$data['post_date'] = NULL;
			}
		}

		$data['post_time'] = isset( $_POST['post_time'] ) ? $this->input->post('post_time') : NULL;

		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
		$sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';
		$price = isset( $_POST['price'] ) ? $this->input->post('price') : '';
		$description = isset( $_POST['description'] ) ? $this->input->post('description') : '';
		$btn_text = isset( $_POST['btn_text'] ) ? $this->input->post('btn_text') : '';
		$content = isset( $_POST['content'] ) ? $this->input->post('content') : '';
		$content_editor = isset( $_POST['content_editor'] ) ? $_POST['content_editor'] : '';

		$image_editor = isset( $_FILES['image_editor'] ) ? $_FILES['image_editor'] : array();
		$time = isset( $_POST['time'] ) ? $this->input->post('time') : '';
		$this->common->uploadEditorImage( $image_editor, $time );

		$config = array();
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';

		$this->load->library('upload', $config);

		if( $this->upload->do_upload('image') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['image'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['image_txt']) and $_POST['image_txt'] == ""){
				$data['image'] = "";
			}
		}

		//Note: This code not use. 
		if( $this->upload->do_upload('image2') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['image2'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['image_txt']) and $_POST['image_txt'] == ""){
				$data['image2'] = "";
			}
		}

		if( $this->upload->do_upload('hero_banner') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['hero_banner'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['hero_banner_txt']) and $_POST['hero_banner_txt'] == ""){
				$data['hero_banner'] = "";
			}
		}
		if( $this->upload->do_upload('hero_banner_m') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['hero_banner_m'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['hero_banner_m_txt']) and $_POST['hero_banner_m_txt'] == ""){
				$data['hero_banner_m'] = "";
			}
		}

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('preview', $data['preview'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('section_lists_preview');
			$result = $q->result_array();

			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
			$data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;
			$data['price'] = isset( $price[$key] ) ? $price[$key] : '' ;
			$data['description'] = isset( $description[$key] ) ? $description[$key] : '' ;
			$data['btn_text'] = isset( $btn_text[$key] ) ? $btn_text[$key] : '' ;
			$data['content'] = isset( $content[$key] ) ? $content[$key] : '' ;
			$data['content_editor'] = isset( $content_editor[$key] ) ? $content_editor[$key] : '' ;
			if( sizeof($result) > 0 ){
				
				// Delete images old
				$row = $result[0];
				$img_arr = array('image', 'image2', 'hero_banner', 'hero_banner_m');
				foreach ($img_arr as $key => $val) {
					if ( isset($data[$val]) and $data[$val] != '' and $row[$val] != $data[$val] ) {
						if (file_exists(editor_upload_path().$row[$val])) {
							unlink(editor_upload_path().$row[$val]);
						}
					}
				}
				// End delete images old

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('preview', $data['preview'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('section_lists_preview', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('section_lists_preview', $data );
			}
		}

		$ar_result = array();
		$ar_result['rs'] = true;
		$cat_list = $this->section->loadCategoryDetail('','','',$data['cate_code']);
		$cat_slug = $cat_list[DEFAULT_LANG]['slug'];

		$page_result = $this->common->getPageSlug( $sec_code );

		$ar_result['url'] = base_url().'th/preview/'.$page_result['page_slug'].'/'.$cat_slug.'/'.$data['slug'].'?preview_id='.$data['preview'];
		echo json_encode( $ar_result );
	}
	public function saveListingSolutionForm(){
		//echo '<pre>';print_r($_POST);echo '</pre>';
		//echo '<pre>';print_r( $_FILES );echo '</pre>';

		//echo '<pre>';print_r( $features );echo '</pre>';
		//exit;
		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		$data['approve_status'] = isset( $_POST['approve_status'] ) ? $this->input->post('approve_status') : '';

		$sec_code = isset( $_POST['sec_code'] ) ? $this->input->post('sec_code') : '';
		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'section_solution_lists' );
			$this->common->addLogs( "section_lists_solution", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "section_lists_solution", $data['code'], "update", "" );
		}

		
		$data['page'] = isset( $_POST['page'] ) ? $this->input->post('page') : '';
		$data['sec_key'] = isset( $_POST['sec_key'] ) ? $this->input->post('sec_key') : '';
		$data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';
		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';

		$cate_code_one = isset( $_POST['cate_code_one'] ) ? $this->input->post('cate_code_one') : '';
		$data['cate_code'] = isset( $_POST['cate_code'] ) ? json_encode($this->input->post('cate_code')) : '';

		$data['usecase_code'] = isset( $_POST['usecase_code'] ) ? json_encode($this->input->post('usecase_code')) : '';
		$data['banner_code'] = isset( $_POST['banner_code'] ) ? json_encode($this->input->post('banner_code')) : '';

		$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
		if ( $slug == '' ) {
			$slug = time();
		} else {
			$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
		}

		if( $data['code'] == '' ){
			$this->db->where('slug', $slug );
		} else {
			$this->db->where('code !=', $data['code'] );
			$this->db->where('slug', $slug );
		}

		$q = $this->db->get('section_solution_lists');
		$resultx = $q->result_array();
		if( sizeof( $resultx ) > 0 ){
			$data['slug'] = $slug.'-'.time();
		} else {
			$data['slug'] = $slug;
		}

		if( isset( $_POST['post_date'] ) ){
			if( $_POST['post_date'] != "" ){ 
				$data['post_date'] = $this->input->post('post_date');
			}else{
				$data['post_date'] = NULL;
			}
		}
		
		$prefix_title = isset( $_POST['prefix_title'] ) ? $this->input->post('prefix_title') : '';
		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
		$sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';
		$description = isset( $_POST['description'] ) ? $this->input->post('description') : '';
		// $btn_text = isset( $_POST['btn_text'] ) ? $this->input->post('btn_text') : '';
		$content = isset( $_POST['content'] ) ? $this->input->post('content') : '';
		$content_editor = isset( $_POST['content_editor'] ) ? $_POST['content_editor'] : '';

		

		$btn1_text = isset( $_POST['btn1_text'] ) ? $this->input->post('btn1_text') : '';
		$btn1_link = isset( $_POST['btn1_link'] ) ? $this->input->post('btn1_link') : '';
		$btn2_text = isset( $_POST['btn2_text'] ) ? $this->input->post('btn2_text') : '';
		$btn2_link = isset( $_POST['btn2_link'] ) ? $this->input->post('btn2_link') : '';


		$config = array();
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';

		$this->load->library('upload', $config);


		$config = array();
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';

		$this->load->library('upload', $config);
		$features = array();
		if ( $_POST['features'] ) {
			foreach( $this->data['langs'] as $key => $lang ){
				foreach ($_POST['features'] as $key_f => $row) {
					$features[$key][$key_f]['title'] = $row['title_'.$lang['text']];
					$features[$key][$key_f]['description'] = $row['description_'.$lang['text']];
					$features[$key][$key_f]['image'] = $row['image_txt'];
				}
			}

			foreach ($_POST['features'] as $key => $row) {
				if ( sizeof( $_FILES['features']['name'] ) > 0 ) {
					$i = 0;
					foreach ($_FILES['features']['name'] as $key => $row) {
						$files['features']['name'][$i]= $row;
						$i++;
					}
					$i = 0;
					foreach ($_FILES['features']['type'] as $key => $row) {
						$files['features']['type'][$i]= $row;
						$i++;
					}
					$i = 0;
					foreach ($_FILES['features']['tmp_name'] as $key => $row) {
						$files['features']['tmp_name'][$i]= $row;
						$i++;
					}
					$i = 0;
					foreach ($_FILES['features']['error'] as $key => $row) {
						$files['features']['error'][$i]= $row;
						$i++;
					}
					$i = 0;
					foreach ($_FILES['features']['size'] as $key => $row) {
						$files['features']['size'][$i]= $row;
						$i++;
					}
				}
			}
			// echo '<pre>';print_r( $features ); echo '</pre>';

			// echo '<pre>';print_r( $files ); echo '</pre>';

			// exit;

			if ( sizeof( $files['features']['name'] ) > 0 ) {
				for ($i=0; $i < sizeof($files['features']['name']); $i++) { 
					$_FILES['features']['name'] = $files['features']['name'][$i]['image'];
					$_FILES['features']['type'] = $files['features']['type'][$i]['image'];
					$_FILES['features']['tmp_name'] = $files['features']['tmp_name'][$i]['image'];
					$_FILES['features']['error'] = $files['features']['error'][$i]['image'];
					$_FILES['features']['size'] = $files['features']['size'][$i]['image'];
					
					if ( $_FILES['features']['name'] != '') {
						if( $this->upload->do_upload('features') ) {
							$upload = array('upload_data' => $this->upload->data());
							foreach( $this->data['langs'] as $key => $lang ){
								$features[$key][$i]['image'] = $upload['upload_data']['file_name'];
							}
						} else {
							if( $features[$key][$i]['image'] == "") {
								$features[$key][$i]['image'] = $features[$key][$i]['image'];
							}
						}
					}
				}
			}

			$features_data = array();
			foreach( $features as $key => $row ){
				$features_data[] =  json_encode( $row );
			}
		}

		//Note: This code not use. 
		if( $this->upload->do_upload('image_product') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['image_product'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['image_product_txt']) and $_POST['image_product_txt'] == ""){
				$data['image_product'] = "";
			}
		}

		if( $this->upload->do_upload('image_detail') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['image_detail'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['image_detail_txt']) and $_POST['image_detail_txt'] == ""){
				$data['image_detail'] = "";
			}
		}

		if( $this->upload->do_upload('hero_banner') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['hero_banner'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['hero_banner_txt']) and $_POST['hero_banner_txt'] == ""){
				$data['hero_banner'] = "";
			}
		}
		if( $this->upload->do_upload('hero_banner_m') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['hero_banner_m'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['hero_banner_m_txt']) and $_POST['hero_banner_m_txt'] == ""){
				$data['hero_banner_m'] = "";
			}
		}

		// Editor approve
		if ( ($_POST['is_approve'] == 1) and $this->input->post('approve_status') != 2  ) {
			$data['approve_status'] = 1;
		}

		$data['submit_date'] = date('Y-m-d H:i:s') ;

		$post_date = (isset( $_POST['published_date'] ) and $_POST['published_date'] != '') ? $this->input->post('published_date') : '';
		$post_time = (isset( $_POST['published_time'] ) and $_POST['published_time'] != '' ) ? $this->input->post('published_time') : date('H:i:s');
		if ( $post_date == '' ) {
			$post_date = date('Y-m-d');
		} else {
			$post_date = explode('-', $post_date);
			$post_date = ($post_date[0]-543).'-'.$post_date[1].'-'.$post_date[2];
		}

		$data['published_date'] = $post_date.' '.$post_time;

		
		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('section_solution_lists');
			$result = $q->result_array();
			$data['prefix_title'] = isset( $prefix_title[$key] ) ? $prefix_title[$key] : '' ;
			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
			$data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;
			$data['description'] = isset( $description[$key] ) ? $description[$key] : '' ;
			$data['btn1_text'] = isset( $btn1_text[$key] ) ? $btn1_text[$key] : '' ;
			$data['btn1_link'] = isset( $btn1_link[$key] ) ? $btn1_link[$key] : '' ;
			$data['btn2_text'] = isset( $btn2_text[$key] ) ? $btn2_text[$key] : '' ;
			$data['btn2_link'] = isset( $btn2_link[$key] ) ? $btn2_link[$key] : '' ;
			$data['content'] = isset( $content[$key] ) ? $content[$key] : '' ;
			$data['content_editor'] = isset( $content_editor[$key] ) ? $content_editor[$key] : '' ;

			$data['features'] = isset( $features_data[$key] ) ? $features_data[$key] : '' ;

			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('section_solution_lists', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('section_solution_lists', $data );
			}
		}

		// Seo data
		$data_seo = array();
		
		$data_seo['code'] = isset( $_POST['seo_code'] ) ? $this->input->post('seo_code') : '';
		if( $data_seo['code'] == '' ){
			$data_seo['code'] = $this->common->get_code( 'seo' );
		}
		$data_seo['seo_type'] = isset( $_POST['seo_type'] ) ? $this->input->post('seo_type') : '';
		$data_seo['page'] = $data['slug'];
		$data_seo['page_title'] = isset( $_POST['page_title'] ) ? $this->input->post('page_title') : '';
		$data_seo['meta_title'] = isset( $_POST['meta_title'] ) ? $this->input->post('meta_title') : '';
		$data_seo['meta_desc'] = isset( $_POST['meta_desc'] ) ? $this->input->post('meta_desc') : '';
		$data_seo['meta_keyword'] = isset( $_POST['meta_keyword'] ) ? $this->input->post('meta_keyword') : '';
		
		//Upload liberies config
		if( $this->upload->do_upload('share_img') ){
		  $upload = array('upload_data' => $this->upload->data());
		  $data_seo['share_img'] = $upload['upload_data']['file_name'];
		} else {
			if( $_POST['share_img_txt'] == ""){
				$data_seo['share_img'] = "";
			}
		}

		// echo '<pre>';print_r($data);echo '</pre>';
		// exit;
		$this->common->updateSEO( $data_seo );
		$this->common->setAlert('success','Listing has been saved.','Success !');

		
		redirect('admin/section/listing/edit/'.$sec_code.'/'.$cate_code_one.'/'.$data['code']);
	}
	public function saveFooterForm(){
		//print_r( $_POST );
		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';

		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'section_footer' );
			$this->common->addLogs( "section_footer", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "section_footer", $data['code'], "update", "" );
		}
		$data['page'] = isset( $_POST['page'] ) ? $this->input->post('page') : '';

		$data['fb_title'] = isset( $_POST['fb_title'] ) ? $this->input->post('fb_title') : '';
		$data['fb_sub_title'] = isset( $_POST['fb_sub_title'] ) ? $this->input->post('fb_sub_title') : '';
		$data['fb_link'] = isset( $_POST['fb_link'] ) ? $this->input->post('fb_link') : '';

		$data['yt_title'] = isset( $_POST['yt_title'] ) ? $this->input->post('yt_title') : '';
		$data['yt_sub_title'] = isset( $_POST['yt_sub_title'] ) ? $this->input->post('yt_sub_title') : '';
		$data['yt_link'] = isset( $_POST['yt_link'] ) ? $this->input->post('yt_link') : '';

		$data['logo_link'] = isset( $_POST['logo_link'] ) ? $this->input->post('logo_link') : '';
		$data['contact_status'] = isset( $_POST['contact_status'] ) ? $this->input->post('contact_status') : '';
		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';

		// Upload liberies config
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';

		$this->load->library('upload', $config);

		if( $this->upload->do_upload('image') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['image'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['image_txt']) and $_POST['image_txt'] == ""){
				$data['image'] = "";
			}
		}
		if( $this->upload->do_upload('logo_image') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['logo_image'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['logo_image_txt']) and $_POST['logo_image_txt'] == ""){
				$data['logo_image'] = "";
			}
		}

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('section_footer');
			$result = $q->result_array();

			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('section_footer', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('section_footer', $data );
			}
		}
		$this->common->setAlert('success','Footer has been saved.','Success !');
		redirect('admin/section/footer/'.$data['page']);
	}

		public function saveRowSetting(){
			
			$data = array();
			$id = isset( $_POST['id'] ) ? $this->input->post('id') : '';
			$page_code = isset( $_POST['page_code'] ) ? $this->input->post('page_code') : '';
			$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
			$data['is_container'] = isset( $_POST['is_container'] ) ? $this->input->post('is_container') : 0;

			$data['bg_color'] = isset( $_POST['bg_color'] ) ? $this->input->post('bg_color') : '';
			$data['bg_position_hoz'] = isset( $_POST['bg_position_hoz'] ) ? $this->input->post('bg_position_hoz') : '';
			$data['bg_position_ver'] = isset( $_POST['bg_position_ver'] ) ? $this->input->post('bg_position_ver') : '';
			$data['bg_repeat'] = isset( $_POST['bg_repeat'] ) ? $this->input->post('bg_repeat') : '';
			$data['bg_fixed'] = isset( $_POST['bg_fixed'] ) ? $this->input->post('bg_fixed') : '';
			$data['bg_size'] = isset( $_POST['bg_size'] ) ? $this->input->post('bg_size') : '';

			// Upload liberies config
			$config['upload_path'] = editor_upload_path();
			$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';

			$this->load->library('upload', $config);
			if( $this->upload->do_upload('row_bg') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['row_bg'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['row_bg_txt']) and $_POST['row_bg_txt'] == ""){
					$data['row_bg'] = "";
				}
			}
			if( $this->upload->do_upload('row_bg_m') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['row_bg_m'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['row_bg_m_txt']) and $_POST['row_bg_m_txt'] == ""){
					$data['row_bg_m'] = "";
				}
			}
			$this->db->where('id', $id);
			$this->db->update('row', $data);
			$this->common->setAlert('success','Row setting has been saved.','Success !');

			redirect('admin/section/allrow/'.$page_code);
		}

	public function updateDragable(){
		$ar_result = array();
		$ar_index = isset( $_POST['ar_index'] ) ? explode(",",$_POST['ar_index']) : array();
		$column_id = isset( $_POST['column_id'] ) ? $this->input->post('column_id') : '';
		//print_r( $ar_index );
		if( sizeof( $ar_index ) > 0 ){
			foreach( $ar_index as $key => $code ){
				$this->db->where('code', $code);
				$this->db->update('section', array('sticky' => sizeof( $ar_index )-1-intval($key) , 'column_id' => $column_id )  );
			}
		}
		$ar_result['rs'] = true;
		echo json_encode( $ar_result );
	}
	public function updateRowDragable(){
		$ar_result = array();
		$ar_index = isset( $_POST['ar_index'] ) ? explode(",",$_POST['ar_index']) : array();
		$page_code = isset( $_POST['page_code'] ) ? $this->input->post('page_code') : '';
		//print_r( $ar_index );
		if( sizeof( $ar_index ) > 0 ){
			foreach( $ar_index as $key => $code ){
				$this->db->where('id', $code);
				$this->db->update('row', array('sticky' => sizeof( $ar_index )-1-intval($key)  )  );
			}
		}
		$ar_result['rs'] = true;
		echo json_encode( $ar_result );
	}

	public function allrow( $page_code = ""){
		if( !check_permission( $this->session->userdata('user_type'), 'pages') ){
			$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
			redirect('admin/dashboard');
		  }
		$this->data['menu_active'] = 'section/allrow/'.$page_code;
		/*if( $page_code == "" ){
			redirect('admin/pages');
		}*/
		$this->data['pages_names'] = $this->section->loadPageName($page_code);
		$this->data['detail'] = $this->pages->loadDetail( $page_code );
		if( sizeof( $this->data['detail'] ) == 0 ){ redirect('admin/pages/add'); }
		$this->data['seo'] = $this->seo->loadDetail( $this->data['detail'][DEFAULT_LANG]['page_slug'] );
		$this->data['page_code'] = $page_code;
		//$this->data['menu_active'] = 'section/all/'.$page_code;
		$this->data['is_section'] = 1;
		$this->data['lists'] = $this->section->loadSectionLists( $page_code );
		$this->page_construct( 'all_row_section_lists', $this->data );

		//redirect('admin/section/detail/xr/phenomenon');
	}

	public function cancelApprove(){
		$code = $this->security->xss_clean( $this->input->post('code') );

		$data = array();
		$data['approve_status'] = '0';
		$data['u_by'] = $this->session->userdata('id');
		$data['u_date'] = date('Y-m-d H:i:s');
		$this->db->where('code', $code );
		$this->db->update( 'section_lists', $data);

		$ar_result = array();
		$ar_result['rs'] = true;

		$this->common->setAlert('success','Content for approve has been canceled.','Success !');
		echo json_encode( $ar_result );


	}
	public function rejectApprove(){
		$code = $this->security->xss_clean( $this->input->post('code') );
		

		$data = array();
		$data['approve_note'] = $this->security->xss_clean( $this->input->post('approve_note') );
		$data['approve_status'] = 3;
		$data['onoff'] = 0;
		$data['approve_by'] = $this->session->userdata('id');
		$data['approve_date'] = date('Y-m-d H:i:s');
		$this->db->where('code', $code );
		$this->db->update( 'section_lists', $data);

		$ar_result = array();
		$ar_result['rs'] = true;

		$this->common->setAlert('success','Content has been rejected.','Success !');
		echo json_encode( $ar_result );


	}
	public function confirmApprove(){
		$code = $this->security->xss_clean( $this->input->post('code') );
		

		$data = array();
		$data['approve_status'] = 2;
		$data['onoff'] = $this->security->xss_clean( $this->input->post('onoff') );;
		$data['approve_by'] = $this->session->userdata('id');
		$data['approve_date'] = date('Y-m-d H:i:s');
		$this->db->where('code', $code );
		$this->db->update( 'section_lists', $data);

		$ar_result = array();
		$ar_result['rs'] = true;

		$this->common->setAlert('success','Content has been approved.','Success !');
		echo json_encode( $ar_result );


	}
	public function removeCateFromSection(){
		$cate_code = isset( $_POST['cate_code'] ) ? $this->input->post('cate_code') : '';
		$sec_code = isset( $_POST['sec_code'] ) ? $this->input->post('sec_code') : '';
		if( $cate_code != '' && $sec_code != '' ){
			$this->db->where('code', $cate_code);
			$this->db->where('lang', DEFAULT_LANG );
			$this->db->limit(1);
			$q = $this->db->get('section_cate');
			$result = $q->result_array();
			if( sizeof($result) > 0 ){
				$ar_sec_code = array();
				$ar_data_sec = array();
				if( $result[0]['sec_code'] != ""){
					$ar_sec_code = json_decode( $result[0]['sec_code'], true );
				}
				foreach( $ar_sec_code as $key => $a_sec_code){
					if( $a_sec_code == $sec_code ){
						unset(  $ar_sec_code[ $key ] );
					}else{
						array_push( $ar_data_sec , $a_sec_code);
					}
				}
				$data = array();
				$data['sec_code'] = json_encode( $ar_data_sec );
				$this->db->where('code', $cate_code);
				$this->db->update('section_cate', $data);
			}
		}
		$ar_result = array();
		$ar_result['rs'] = true;
		$this->common->setAlert('success','Category has been removed.','Success !');
		echo json_encode( $ar_result );
	}
	// public function approve_Preview(){
	// 	$ar_result = array();

	// 	$code = $this->security->xss_clean( $this->input->post('code') );

	// 	$this->db->where('code', $data['code'] );
	// 	$q = $this->db->get('section_lists_preview');
	// 	$resultx = $q->result_array();
	// 	if( sizeof( $resultx ) > 0 ){
	// 		$data['slug'] = $slug.'-'.time();

	// 		$ar_result['rs'] = true;
	// 		$ar_result['preview_id'] = $resultx['preview'];
	// 	} else {
	// 		$ar_result['rs'] = false;

	// 	}

	// 	echo json_encode( $ar_result );
	// }

}
