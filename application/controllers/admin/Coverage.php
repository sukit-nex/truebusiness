<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coverage extends MY_Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 public function __construct() {
		 parent::__construct();
     $this->load->model($this->modelFolder.'coverage_model','coverage',TRUE);
     $this->data['menu_active'] = "coverage";
     
 }
	public function index()
	{
		if( !check_permission( $this->session->userdata('user_type'), 'coverage') ){
			$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
			redirect('admin/dashboard');
		}
		$this->data['lists'] = $this->coverage->loadHistory();
		$this->page_construct('coverageList', $this->data );
	}
  public function geojson( $his_id = "" ){
    if( $his_id != "" ){
      $this->db->where( 'id', $his_id );
      $q = $this->db->get('coverage_history');
      $result = $q->result_array();
      if( sizeof( $result ) > 0 ){
        $q = $this->db->get('coverage_list');
        $lists = $q->result_array();
        if( sizeof( $lists ) > 0 ){
          $data = array();
          $data['features'] = array();
          $data['type'] = "FeatureCollection";
          foreach( $lists as $list ){
            $data_inside = array();
            $data_inside['type'] = "Feature";
            $data_inside['properties'] = array();
            $data_inside['geometry'] = array();
            $data_properties = array();
            $data_properties['ID'] = $list['id'];
            $data_properties['zone'] = $list['zone'];
            $data_properties['marker_type'] = $list['marker_type'];
            $data_properties['marker_name_th'] = $list['marker_name_th'];
            $data_properties['marker_name_en'] = $list['marker_name_en'];
            $data_properties['Sub-District'] = $list['sub_district'];
            $data_properties['District'] = $list['district'];
            $data_properties['Province'] = $list['province'];
            $data_inside['properties'] = $data_properties ;
            
            $data_geometry = array();
            $data_geometry['coordinates'] = array( floatVal($list['lng']) , floatVal($list['lat']));
            $data_geometry['type'] = 'Point';

            $data_inside['geometry'] = $data_geometry ;

            array_push( $data['features'] , $data_inside );
          }

          
          $geojson = json_encode( $data, JSON_UNESCAPED_UNICODE );
          $file_name = "geojson_5g_".date('Ymd').'_'.date('His').'.geojson';
          if( file_put_contents(editor_upload_path()."geojson/".$file_name, $geojson) ){
			$this->db->update('coverage_history',array('geojson_status' => 0 ));

            $this->db->where( 'id', $his_id );
            $this->db->update('coverage_history', array('geojson_file' => $file_name, 'geojson_status' => 1) );
            redirect('admin/coverage');
          }else{
            echo "Oops! Error creating geojson file...";
          }

        }
        //redirect('admin/coverage');
      }else{
        redirect('admin/coverage');
      }
    }else{
      redirect('admin/coverage');
    }
  }
  public function upload_coverage(){
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'csv';
		$this->load->library('upload', $config);
		$this->upload->set_upload_path( file_upload_path() );
		$this->upload->set_max_filesize('100000000');
		if( $this->upload->do_upload('file_upload') ){
			$this->db->truncate('coverage_list');
			$upload = array('upload_data' => $this->upload->data());
			$this->db->insert('coverage_history', array('source_file' => $upload['upload_data']['file_name'], 'c_date' => date('Y-m-d H:i:s') , 'c_by' => $this->session->userdata('id') ));
			$his_id = $this->db->insert_id();
      
			include 'Classes/PHPExcel/IOFactory.php';
			$inputFileName = 'uploads/files/'.$upload['upload_data']['file_name'];
			date_default_timezone_set("Asia/Bangkok");
			try {
				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
			} catch(Exception $e) {
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}
			$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
			//return $allDataInSheet;
			$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
			$insert_count = 0;
			$update_count = 0;
			$error_count = 0;
			for($i=2;$i<=$arrayCount;$i++){
				sleep(0.1);
				$storeData = array(
					'zone'=>$allDataInSheet[$i]['B'],
					'marker_type'=>$allDataInSheet[$i]['C'],
					'marker_name_th'=>$allDataInSheet[$i]['D'],
					'marker_name_en'=>$allDataInSheet[$i]['E'],
					'sub_district'=>$allDataInSheet[$i]['F'],
					'district'=>$allDataInSheet[$i]['G'],
					'province'=>$allDataInSheet[$i]['H'],
					'lat'=>$allDataInSheet[$i]['I'],
					'lng'=>$allDataInSheet[$i]['J'],
					'his_id'=> $his_id,
					'c_date'=> date('Y-m-d H:i:s'),
				);
				//echo $storeData['code'];
				$this->db->insert('coverage_list', $storeData);
				$insert_count++;
			}
			$this->common->setAlert('success','Your CSV has been uploaded.','Sucess !');
			redirect('admin/coverage');
		}else{
			$message = strip_tags($this->upload->display_errors());
			$this->common->setAlert('error',$message,'Error !');
			redirect('admin/coverage');
			//$error = array('error' => $this->upload->display_errors());
			//print_r( $error );
		}
	}
 
  
}
