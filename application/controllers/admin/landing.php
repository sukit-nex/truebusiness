<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends MY_Admin_Controller {

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see https://codeigniter.com/user_guide/general/urls.html
	*/
	public function __construct() {
		parent::__construct();
		if( !( $this->session->userdata('user_type') == "SA" || $this->session->userdata('user_type') == "A" ) ){
			redirect('admin/dashboard');
		}

		$this->load->model($this->modelFolder.'Landing_model','landing',TRUE);
		$this->load->model($this->modelFolder.'Seo_model','seo',TRUE);
	}

	public function index(){
		$this->data['menu_active'] = "landing/lists";

		$this->data['lists'] = $this->landing->load_landing_lists();
		$this->page_construct('landing_list', $this->data);
	}

	public function lists(){
		$this->data['menu_active'] = "landing/lists";

		$this->data['lists'] = $this->landing->load_landing_lists();
		$this->page_construct('landing_list', $this->data);
	}

	public function add(){
		$this->data['menu_active'] = "landing/add";
		$this->data['data_code'] = '';
		$this->data['detail'] = array();

		$this->page_construct('landing_form', $this->data);
	}

	public function edit( $code = '' ){
		$this->data['menu_active'] = "landing/add";
		if( $code == "" ){
			redirect('admin/dashboard');
		}

		$this->data['detail'] = $this->landing->load_landing_detail( $code );
		$this->data['seo'] = $this->seo->loadDetail( $this->data['detail'][DEFAULT_LANG]['slug'] );

		$this->page_construct('landing_form', $this->data);
	}

	public function saveForm()
	{
		// echo '<pre>'; print_r( $_FILES ); echo '</pre>';
		// exit;
		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'landing' );
			$this->common->addLogs( "landing", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "landing", $data['code'], "update", "" );
		}

		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
        $data['scripts_header'] = isset( $_POST['scripts_header'] ) ? $this->input->post('scripts_header') : '';
        $data['scripts_body'] = isset( $_POST['scripts_body'] ) ? $this->input->post('scripts_body') : '';
        $data['scripts_footer'] = isset( $_POST['scripts_footer'] ) ? $this->input->post('scripts_footer') : '';
		
		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';

		$content = isset( $_POST['content'] ) ? $this->input->post('content') : '';
		$content_editor = isset( $_POST['content_editor'] ) ? $_POST['content_editor'] : '';
		$image_editor = isset( $_FILES['image_editor'] ) ? $_FILES['image_editor'] : array();

		$time = isset( $_POST['time'] ) ? $this->input->post('time') : '';
		$this->common->uploadEditorImage( $image_editor, $time );

		// Upload liberies config
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';
		$this->load->library('upload', $config);

		if( $this->upload->do_upload('banner_image_d') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['banner_image_d'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['banner_image_d_txt']) and $_POST['banner_image_d_txt'] == ""){
				$data['banner_image_d'] = "";
			}
		}

		if( $this->upload->do_upload('banner_image_m') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['banner_image_m'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['banner_image_m_txt']) and $_POST['banner_image_m_txt'] == ""){
				$data['banner_image_m'] = "";
			}
		}

		$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
		if ( $slug == '' ) {
			$slug = time();
		} else {
			$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
		}

		if( $data['code'] == '' ){
			$this->db->where('slug', $slug );
		} else {
			$this->db->where('code !=', $data['code'] );
			$this->db->where('slug', $slug );
		}

		$q = $this->db->get('landing');
		$resultx = $q->result_array();
		if( sizeof( $resultx ) > 0 ){
			$data['slug'] = $slug.'-'.time();
		} else {
			$data['slug'] = $slug;
		}

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('landing');
			$result = $q->result_array();

			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
			$data['content'] = isset( $content[$key] ) ? $content[$key] : '' ;
			$data['content_editor'] = isset( $content_editor[$key] ) ? $content_editor[$key] : '' ;

			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('landing', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('landing', $data );
			}
		}

		
		// Seo data
		$data_seo = array();
		$data_seo['code'] = isset( $_POST['seo_code'] ) ? $this->input->post('seo_code') : '';
		if( $data_seo['code'] == '' ){
			$data_seo['code'] = $this->common->get_code( 'seo' );
		}
		$data_seo['seo_type'] = isset( $_POST['seo_type'] ) ? $this->input->post('seo_type') : '';
		$data_seo['page'] = $data['slug'];
		$data_seo['page_title'] = isset( $_POST['page_title'] ) ? $this->input->post('page_title') : '';
		$data_seo['meta_title'] = isset( $_POST['meta_title'] ) ? $this->input->post('meta_title') : '';
		$data_seo['meta_desc'] = isset( $_POST['meta_desc'] ) ? $this->input->post('meta_desc') : '';
		$data_seo['meta_keyword'] = isset( $_POST['meta_keyword'] ) ? $this->input->post('meta_keyword') : '';
		
		//Upload liberies config
		if( $this->upload->do_upload('share_img') ){
			$upload = array('upload_data' => $this->upload->data());
			$data_seo['share_img'] = $upload['upload_data']['file_name'];
		} else {
			if( $_POST['share_img_txt'] == ""){
				$data_seo['share_img'] = "";
			}
		}
		$this->common->updateSEO( $data_seo );
		$this->common->setAlert('success','Landing has been saved.','Success !');

		redirect('admin/landing/edit/'.$data['code']);
	}
}
