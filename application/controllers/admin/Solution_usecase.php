<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solution_usecase extends MY_Admin_Controller {

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see https://codeigniter.com/user_guide/general/urls.html
	*/
	public function __construct() {
		parent::__construct();
		if( !( $this->session->userdata('user_type') == "SA" || $this->session->userdata('user_type') == "A" ) ){
			redirect('admin/dashboard');
		}

		$this->load->model($this->modelFolder.'Solution_usecase_model','usecase',TRUE);
	}

	public function index(){
		//redirect('admin/dashboard');
		$this->data['menu_active'] = 'solution_usecase';
		$this->data['lists'] = $this->usecase->loadLists();
		$this->page_construct('solution_usecase_list', $this->data);
	}

	public function add(){

		//$this->data['menu_active'] = "solution_usecase/add";
		$this->data['menu_active'] = "solution_usecase";
		$this->data['data_code'] = '';//$this->common->get_code( 'product' );
		$this->data['detail'] = array();
		$this->page_construct('solution_usecase_form', $this->data);
	}

	public function edit( $code = "" ){
		//$this->data['menu_active'] = 'solution_usecase/edit/'.$code;
		$this->data['menu_active'] = "solution_usecase";
		if( $code == "" ){
			redirect('admin/dashboard');
		}
		$this->data['detail'] = $this->usecase->loadDetail( $code );

		$this->page_construct('solution_usecase_form', $this->data);
	}

	public function saveForm()
	{
		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'solution_usecase' );
			$this->common->addLogs( "solution_usecase", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "solution_usecase", $data['code'], "update", "" );
		}
		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';

		$data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';
		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';

		// Upload liberies config
		$config['upload_path'] = editor_upload_path().$this->data['upload_prefix'];
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx';
		$this->load->library('upload', $config);

		if( $this->upload->do_upload('image') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['image'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['image_txt']) and $_POST['image_txt'] == ""){
				$data['image'] = "";
			}
		}

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('solution_usecase');
			$result = $q->result_array();

			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;

			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('solution_usecase', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('solution_usecase', $data );
			}
		}

		redirect('admin/solution_usecase/edit/'.$data['code']);
	}
}
