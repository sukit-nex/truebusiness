<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends MY_Admin_Controller { 

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
public function __construct() {
	parent::__construct();
     /*if( !( $this->session->userdata('user_type') == "SA" || $this->session->userdata('user_type') == "A" ) ){
       redirect('admin/dashboard');
     }*/
     //$this->load->model($this->modelFolder.'Import_model','member',TRUE);
 }

	public function index(){
		redirect('admin/import/code_x2');
	}
	public function code_x2(){
		$this->data['menu_active'] = 'import/code_x2';
		$this->page_construct('code_x2', $this->data);
	}
	

}
