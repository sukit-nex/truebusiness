<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 public function __construct() {
		 parent::__construct();
     /*if( !( $this->session->userdata('user_type') == "SA" || $this->session->userdata('user_type') == "A" ) ){
       redirect('admin/dashboard');
     }*/

    //  $this->load->model($this->modelFolder.'Banner_model','banner',TRUE);
     $this->load->model($this->modelFolder.'Pages_model','pages',TRUE);
     $this->load->model($this->modelFolder.'Seo_model','seo',TRUE);
     if( !check_permission( $this->session->userdata('user_type'), 'pages') ){
      $this->common->setAlert('warning','You do not have permission for that page.','Warning !');
      redirect('admin/dashboard');
    }
    //  $this->load->model($this->modelFolder.'Category_model','category',TRUE);
    //  $this->data['banner_cates'] = $this->category->loadLists('banner');
 }

	public function index(){
		//redirect('admin/dashboard');
    $this->data['menu_active'] = 'admin/pages';
    $this->data['lists'] = $this->pages->loadLists();
    $this->page_construct('page_lists', $this->data);
	}

  public function add(){

    $this->data['menu_active'] = "admin/pages/add";
    $this->data['data_code'] = '';//$this->common->get_code( 'pages' );
    $this->data['detail'] = array();
    $this->data['seo'] = array();
		$this->page_construct('pagesForm', $this->data);
	}

  public function edit( $code = "" ){
    $this->data['menu_active'] = 'pages/edit/'.$code;
    if( $code == "" ){
      redirect('admin/pages');
    }
    $this->data['detail'] = $this->pages->loadDetail( $code );
    if( sizeof( $this->data['detail'] ) == 0 ){ redirect('admin/pages/add'); }
    $this->data['seo'] = $this->seo->loadDetail( $this->data['detail'][DEFAULT_LANG]['page_slug'] );
		$this->page_construct('pagesForm', $this->data);
	}

  public function rows( $method = "add", $page_code , $column = 1 ){
    if( $method == "add"){
        $row_data = array();
        $row_data['page_code'] = $page_code;

        $row_data['bg_position_hoz']='center';
        $row_data['bg_position_ver']='top';
        $row_data['bg_repeat']='no_repeat';
        $row_data['bg_fixed']='static';
        $row_data['bg_size'] = 'cover';
        $row_data['bg_color'] = 'transparent';

        $row_data['onoff'] = 1 ;
        $row_data['c_date'] = date('Y-m-d H:i:s') ;
        $row_data['c_by'] = $this->session->userdata('id');
        $this->db->insert('row', $row_data );

        $row_id = $this->db->insert_id();
        for( $i = 1 ; $i <= $column ; $i++ ){
            $col_data = array();
            $col_data['row_id'] = $row_id;
            $col_data['c_date'] = date('Y-m-d H:i:s') ;
            $col_data['c_by'] = $this->session->userdata('id');
            $this->db->insert('column', $col_data );
        }
        redirect('admin/section/allrow/'.$page_code);
    }
  }

  public function saveForm(){
    $data = array();
    $data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
    $data['page_permission'] = isset( $_POST['page_permission'] ) ? implode(",", $this->input->post('page_permission')) : '';
    if( $data['code'] == '' ){
      $data['code'] = $this->common->get_code( 'pages' );
      $this->common->addLogs( "pages", $data['code'], "create", "" );
    }else{
      $this->common->addLogs( "pages", $data['code'], "update", "" );
    }
    $data['parent_code'] = isset( $_POST['parent_code'] ) ? $this->input->post('parent_code') : 0;
    $data['page_name'] = isset( $_POST['page_name'] ) ? $this->input->post('page_name') : '';
    $data['page_slug'] = isset( $_POST['page_slug'] ) ? $this->input->post('page_slug') : '';
    $data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';

    $data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';

    $title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
    $sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';

    // Upload liberies config
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx';

		$this->load->library('upload', $config);

    // Seo data
    $data_seo = array();
    $data_seo['code'] = isset( $_POST['seo_code'] ) ? $this->input->post('seo_code') : '';
		if( $data_seo['code'] == '' ){
			$data_seo['code'] = $this->common->get_code( 'seo' );
		}
		$data_seo['seo_type'] = isset( $_POST['seo_type'] ) ? $this->input->post('seo_type') : '';
    $data_seo['page'] = isset( $_POST['page_slug'] ) ? $this->input->post('page_slug') : '';
    $data_seo['page_title'] = isset( $_POST['page_title'] ) ? $this->input->post('page_title') : '';
    $data_seo['meta_title'] = isset( $_POST['meta_title'] ) ? $this->input->post('meta_title') : '';
    $data_seo['meta_desc'] = isset( $_POST['meta_desc'] ) ? $this->input->post('meta_desc') : '';
    $data_seo['meta_keyword'] = isset( $_POST['meta_keyword'] ) ? $this->input->post('meta_keyword') : '';

    if( $this->upload->do_upload('share_img') ){
      $upload = array('upload_data' => $this->upload->data());
      $data_seo['share_img'] = $upload['upload_data']['file_name'];
    } else {
      if( $_POST['share_img_txt'] == ""){
				$data_seo['share_img'] = "";
			}
    }

    foreach( $this->data['langs'] as $key => $lang ){
      $data['lang'] = $lang['text'];
      $this->db->where('code', $data['code'] );
      $this->db->where('lang', $data['lang'] );
      $q = $this->db->get('pages');
      $result = $q->result_array();
      $data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
      $data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;

      if( sizeof($result) > 0 ){

        $data['u_date'] = date('Y-m-d H:i:s') ;
        $data['u_by'] = $this->session->userdata('id');
        $this->db->where('code', $data['code'] );
        $this->db->where('lang', $data['lang'] );
        $this->db->update('pages', $data );

      }else{
        $data['c_date'] = date('Y-m-d H:i:s') ;
        $data['c_by'] = $this->session->userdata('id');
        $this->db->insert('pages', $data );
      }
    }
    $this->common->updateSEO( $data_seo);
    redirect('admin/section/all/'.$data['code']);
  }
  
  public function json(){
    $data = array();
    $data['data'] = array(
      "row" => array(
        "type" => "211",
        "setting" => "default"
      )
    );
    echo json_encode( $data );
  }

}
