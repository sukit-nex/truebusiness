<?php
/**
 * Admin Panel for Codeigniter
 * Author: Wutikorn J.
 *
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index() {

    }

     public function do_upload() {
       if( empty( $_FILES['file'] ) ){
         exit();
       }
       $errorImgFile = "./img/img_upload_error.jpg";
       $destinationFilePath = editor_upload_path().$_FILES["file"]["name"];
       if( !move_uploaded_file( $_FILES['file']['tmp_name'], $destinationFilePath )){
         echo $errorImgFile;
       }else{
         echo REAL_PATH.$destinationFilePath;
       }
    }
    public function do_upload_tiny() {
       if( empty( $_FILES['file'] ) ){
         exit();
       }
       $errorImgFile = "./img/img_upload_error.jpg";
       $destinationFilePath = editor_upload_path().$_FILES["file"]["name"];
       if( !move_uploaded_file( $_FILES['file']['tmp_name'], $destinationFilePath )){
         echo $errorImgFile;
       }else{
         echo json_encode( array('location' =>  REAL_PATH.$destinationFilePath ) );
       }
    }
    public function do_upload_dropzone_gall() {
      if( empty( $_FILES['file'] ) ){
        exit();
      }
      $data_insert = array();
      $data_insert['gall_code'] = isset( $_POST['gall_code'] ) ? $this->input->post('gall_code') : '';
      $data_insert['token'] = $this->security->get_csrf_hash();
      $errorImgFile = "./img/img_upload_error.jpg";
      $destinationFilePath = editor_upload_path().$_FILES["file"]["name"];
      if( !move_uploaded_file( $_FILES['file']['tmp_name'], $destinationFilePath )){
        echo $errorImgFile;
      }else{
        $data_insert['image'] = $_FILES["file"]["name"] ;
        $data_insert['c_date'] = date('Y-m-d H:i:s') ;
        $data_insert['c_by'] = $this->session->userdata('id');
        $this->db->insert('gallery_image', $data_insert );

        $data = array();
        $data['id'] = $this->db->insert_id();
        $data['filename'] = REAL_PATH.$destinationFilePath;
        $data['responseContext'] = array();
        echo json_encode( $data );
      }
   }

}
