<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Admin_Controller {

	public function __construct() {
		parent::__construct();
		$this->data['menu_active'] = "menu";
		$this->load->model($this->modelFolder.'Menu_model','menu',TRUE);
	}
	public function index(){
		if( !check_permission( $this->session->userdata('user_type'), 'menu') ){
			$this->common->setAlert('warning','You do not have permission for that page.','Warning !');
			redirect('admin/dashboard');
		  }
		$this->data['top_lists'] = $this->menu->loadLists( 'top' );
		$this->data['foot_lists'] = $this->menu->loadLists( 'footer' );
		$this->page_construct('menu-manage', $this->data);
	}
	public function updateSort(){
		$data = array();
		$ar_result = array();
		$code = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		$this->common->addLogs( "update", $code, "update", "" );
		$data['parent_code'] = isset( $_POST['parent_code'] ) ? $this->input->post('parent_code') : 0;

		$data['u_date'] = date('Y-m-d H:i:s') ;
		$data['u_by'] = $this->session->userdata('id');
		$this->db->where('code', $code );
		$this->db->update('menu', $data );
		$ar_result['rs'] = true;
		echo json_encode( $ar_result );
	}
	public function updateSortList(){
		$data = array();
		$ar_result = array();
		$catelists = isset( $_POST['menulists'] ) ? $this->input->post('menulists') : '';

		foreach( $catelists as $list ){
			$data = array();
			$data['sticky'] = $list['sticky'] ;
			$data['u_date'] = date('Y-m-d H:i:s') ;
			$data['u_by'] = $this->session->userdata('id');
			$this->db->where('code', $list['code'] );
			$this->db->update('menu', $data );
		}

		$ar_result['rs'] = true;

		// Clear Cache (redis)
		if($this->cache->redis->is_supported()) {
			$this->cache->clean();
		}

		echo json_encode( $ar_result );
	}
	public function updateMenu(){
		// print_r($_POST);
		// exit;
		$data = array();
		$ar_result = array();
		$code = isset( $_POST['menu_code'] ) ? $this->input->post('menu_code') : '';
		if( $code == ""){
			$data['code'] = $this->common->get_code( 'menu' );
		}else{
			$data['code'] = $code;
		}
		$name = isset( $_POST['name'] ) ? $this->input->post('name') : '';
		$data['parent_code'] = isset( $_POST['parent_code'] ) ? $this->input->post('parent_code') : '';
		$data['type'] = $this->input->post('type') ;
		$data['page_code'] = $this->input->post('page_code') ;
		$data['link'] = isset( $_POST['link'] ) ? $this->input->post('link') : '';
		$data['position'] = $this->input->post('position') ;
		$data['is_newtab'] = $this->input->post('is_newtab') ;
		$data['onoff'] = $this->input->post('onoff') ;
		//$data['slug'] = $this->input->post('slug') ;

		if ( $data['type'] == 'slug' ) {
			$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
			if ( $slug == '' ) {
				$slug = time();
			} else {
				$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
			}
			
			if( $data['code'] == '' ){
				$this->db->where('slug', $slug );
			} else {
				$this->db->where('code !=', $data['code'] );
				$this->db->where('slug', $slug );
			}
			$q = $this->db->get('menu');
			$resultx = $q->result_array();

			if( sizeof( $resultx ) > 0 ){
				$data['slug'] = $slug.'-'.time();
			} else {
				$data['slug'] = $slug;
			}
		}

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('menu');
			$result = $q->result_array();

			$data['name'] = isset( $name[$key] ) ? $name[$key] : '' ;

			if( sizeof($result) > 0 ){

			$data['u_date'] = date('Y-m-d H:i:s') ;
			$data['u_by'] = $this->session->userdata('id');
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$this->db->update('menu', $data );

			}else{
			$data['c_date'] = date('Y-m-d H:i:s') ;
			$data['c_by'] = $this->session->userdata('id');
			$this->db->insert('menu', $data );
			}
		}

        $this->common->setAlert('success','Menu has been saved.','Success !');
        
		$ar_result['rs'] = true;
		echo json_encode( $ar_result );
	}
	public function edit(){
		$ar_result = array();
		$code = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		$this->data['menu_active'] = 'menu/edit/'.$code;
		if( $code == "" ){
			redirect('admin/dashboard');
		}
		$this->data['detail'] = $this->menu->loadDetail( $code );
		$ar_result['details'] = $this->data['detail'];
		$ar_result['rs'] = true;
		echo json_encode( $ar_result );
	}
	public function activeMenu(){
		$data = array();
		$ar_result = array();
		$code = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : 0;
		$data['u_date'] = date('Y-m-d H:i:s') ;
		$data['u_by'] = $this->session->userdata('id');
		$this->db->where('code', $code );
		$this->db->update('menu', $data );
		$ar_result['rs'] = true;
		echo json_encode( $ar_result );
	}
}
