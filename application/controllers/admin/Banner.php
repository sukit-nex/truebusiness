<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends MY_Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 public function __construct() {
		 parent::__construct();
     $this->load->model($this->modelFolder.'Banner_model','banner',TRUE);
 }
	public function index()
	{
		redirect('admin/banner/lists/immersive');
	}
  public function lists( $page )
	{
    if( !check_permission( $this->session->userdata('user_type'), 'banner') ){
      $this->common->setAlert('warning','You do not have permission for that page.','Warning !');
      redirect('admin/dashboard');
    }
    $this->data['menu_active'] = "banner/lists/".$page;
    $this->data['param_page'] = $page;
    $this->data['lists'] = $this->banner->loadLists( $page );
		$this->page_construct('bannerList', $this->data);

	}
  public function edit( $page = "",$code = "" )
	{
    if( !check_permission( $this->session->userdata('user_type'), 'edit_banner') ){
      $this->common->setAlert('warning','You do not have permission for that page.','Warning !');
      redirect('admin/dashboard');
    }
    if( $code == "" ){
      redirect('admin/banner/lists/'.$page);
    }
    $this->data['menu_active'] = "banner/lists/".$page;
    $this->data['param_page'] = $page;
    $this->data['detail'] = $this->banner->loadDetail( $code );
		$this->page_construct('bannerForm', $this->data);
	}
  public function add( $page )
	{
    if( !check_permission( $this->session->userdata('user_type'), 'create_banner') ){
      $this->common->setAlert('warning','You do not have permission for that page.','Warning !');
      redirect('admin/dashboard');
    }
    //$this->data['data_code'] = $this->common->get_code( 'banner' );
    $this->data['menu_active'] = "banner/lists/".$page;
    $this->data['param_page'] = $page;
    $this->data['detail'] = array();
		$this->page_construct('bannerForm', $this->data);
	}
  public function saveForm()
	{
    //print_r( $_POST );
    $data = array();
    $data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
    if( $data['code'] == '' ){
      $data['code'] = $this->common->get_code( 'banner' );
      $this->common->addLogs( "banner", $data['code'], "create", "" );
    }else{
      $this->common->addLogs( "banner", $data['code'], "update", "" );
    }
    $data['page'] = isset( $_POST['page'] ) ? $this->input->post('page') : '';
    $title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
    $sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';
    $text_on_banner_1 = isset( $_POST['text_on_banner_1'] ) ? $this->input->post('text_on_banner_1') : '';
    $text_on_banner_2 = isset( $_POST['text_on_banner_2'] ) ? $this->input->post('text_on_banner_2') : '';

    $data['type'] = isset( $_POST['type'] ) ? $this->input->post('type') : '';
    $data['video_type'] = isset( $_POST['video_type'] ) ? $this->input->post('video_type') : '';
    $data['video'] = isset( $_POST['video'] ) ? $this->input->post('video') : '';

    $data['content_position_ver'] = isset( $_POST['content_position_ver'] ) ? $this->input->post('content_position_ver') : '';
    $data['content_position_hoz'] = isset( $_POST['content_position_hoz'] ) ? $this->input->post('content_position_hoz') : '';

    $data['content_position_ver_m'] = isset( $_POST['content_position_ver_m'] ) ? $this->input->post('content_position_ver_m') : '';
    $data['content_position_hoz_m'] = isset( $_POST['content_position_hoz_m'] ) ? $this->input->post('content_position_hoz_m') : '';

    $data['link_type'] = isset( $_POST['link_type'] ) ? $this->input->post('link_type') : '';
    $data['page_code'] = isset( $_POST['page_code'] ) ? $this->input->post('page_code') : '';
    $data['link'] = isset( $_POST['link'] ) ? $this->input->post('link') : '';
    $data['ex_link'] = isset( $_POST['ex_link'] ) ? $this->input->post('ex_link') : '';

    $data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';
    $data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
    //$data['cate_code'] = isset( $_POST['cate_code'] ) ? json_encode( $this->input->post('cate_code') ): '';

    $content = isset( $_POST['content'] ) ? $this->input->post('content') : '';
		$content_editor = isset( $_POST['content_editor'] ) ? $_POST['content_editor'] : '';
		$image_editor = isset( $_FILES['image_editor'] ) ? $_FILES['image_editor'] : array();

		$time = isset( $_POST['time'] ) ? $this->input->post('time') : '';
		$this->common->uploadEditorImage( $image_editor, $time );

    // Upload liberies config
    $config['upload_path'] = editor_upload_path();
    $config['allowed_types'] = 'gif|jpg|png|mp4';
    $this->load->library('upload', $config);

    /*if( $this->upload->do_upload('logo_banner') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['logo_banner'] = $upload['upload_data']['file_name'];
    }else{
			if( $_POST['logo_banner_txt'] == ""){
				$data['logo_banner'] = "";
			}
		}*/
    
    if( $this->upload->do_upload('video_file') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['video_file'] = $upload['upload_data']['file_name'];
    }else{
			if( $_POST['video_file_txt'] == ""){
				$data['video_file'] = "";
			}
		}
    if( $this->upload->do_upload('image') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['image'] = $upload['upload_data']['file_name'];
    }else{
			if( $_POST['image_txt'] == ""){
				$data['image'] = "";
			}
		}
    if( $this->upload->do_upload('image_m') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['image_m'] = $upload['upload_data']['file_name'];
    }else{
			if( $_POST['image_m_txt'] == ""){
				$data['image_m'] = "";
			}
		}

    foreach( $this->data['langs'] as $key => $lang ){
      $data['lang'] = $lang['text'];
      $this->db->where('code', $data['code'] );
      $this->db->where('lang', $data['lang'] );
      $q = $this->db->get('banner');
      $result = $q->result_array();

      $data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
      $data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;
      $data['text_on_banner_1'] = isset( $text_on_banner_1[$key] ) ? $text_on_banner_1[$key] : '' ;
      $data['text_on_banner_2'] = isset( $text_on_banner_2[$key] ) ? $text_on_banner_2[$key] : '' ;
      $data['content'] = isset( $content[$key] ) ? $content[$key] : '' ;
      $data['content_editor'] = isset( $content_editor[$key] ) ? $content_editor[$key] : '' ;
      
      if( sizeof($result) > 0 ){
        $data['u_date'] = date('Y-m-d H:i:s') ;
        $data['u_by'] = $this->session->userdata('id');
        $this->db->where('code', $data['code'] );
        $this->db->where('lang', $data['lang'] );
        $this->db->update('banner', $data );
      }else{
        $data['c_date'] = date('Y-m-d H:i:s') ;
        $data['c_by'] = $this->session->userdata('id');
        $this->db->insert('banner', $data );
      }
    }
    $this->common->setAlert('success','Your banner has been saved.','Sucess !');
    redirect('admin/banner/edit/'.$data['page'].'/'.$data['code']);
	}
  public function saveSectionBannerForm()
	{
    //print_r( $_POST );
    $data = array();
    $data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
    if( $data['code'] == '' ){
      $data['code'] = $this->common->get_code( 'banner' );
      $this->common->addLogs( "banner", $data['code'], "create", "" );
    }else{
      $this->common->addLogs( "banner", $data['code'], "update", "" );
    }
    $data['page'] = isset( $_POST['page'] ) ? $this->input->post('page') : '';
    $data['sec_code'] = isset( $_POST['sec_code'] ) ? $this->input->post('sec_code') : '';
    //$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
    $sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';

    $data['title'] = isset( $_POST['title'] ) ? $this->input->post('title') : '';
    $data['type'] = isset( $_POST['type'] ) ? $this->input->post('type') : '';
    $data['video_type'] = isset( $_POST['video_type'] ) ? $this->input->post('video_type') : '';
    $data['video'] = isset( $_POST['video'] ) ? $this->input->post('video') : '';

    $data['content_position_ver'] = isset( $_POST['content_position_ver'] ) ? $this->input->post('content_position_ver') : '';
    $data['content_position_hoz'] = isset( $_POST['content_position_hoz'] ) ? $this->input->post('content_position_hoz') : '';

    $data['content_position_ver_m'] = isset( $_POST['content_position_ver_m'] ) ? $this->input->post('content_position_ver_m') : '';
    $data['content_position_hoz_m'] = isset( $_POST['content_position_hoz_m'] ) ? $this->input->post('content_position_hoz_m') : '';

    $data['link_type'] = isset( $_POST['link_type'] ) ? $this->input->post('link_type') : '';
    $data['page_code'] = isset( $_POST['page_code'] ) ? $this->input->post('page_code') : '';
    $data['link'] = isset( $_POST['link'] ) ? $this->input->post('link') : '';
    $data['ex_link'] = isset( $_POST['ex_link'] ) ? $this->input->post('ex_link') : '';

    $data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';
    $data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
    //$data['cate_code'] = isset( $_POST['cate_code'] ) ? json_encode( $this->input->post('cate_code') ): '';

    $content = isset( $_POST['content'] ) ? $this->input->post('content') : '';
		$content_editor = isset( $_POST['content_editor'] ) ? $_POST['content_editor'] : '';
		$image_editor = isset( $_FILES['image_editor'] ) ? $_FILES['image_editor'] : array();

		$time = isset( $_POST['time'] ) ? $this->input->post('time') : '';
		$this->common->uploadEditorImage( $image_editor, $time );

    // Upload liberies config
    $config['upload_path'] = editor_upload_path();
    $config['allowed_types'] = 'gif|jpg|png|mp4';
    $config['max_size'] = 3000000;
    $this->load->library('upload', $config);

    /*if( $this->upload->do_upload('logo_banner') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['logo_banner'] = $upload['upload_data']['file_name'];
    }else{
			if( $_POST['logo_banner_txt'] == ""){
				$data['logo_banner'] = "";
			}
		}*/

    if( $this->upload->do_upload('video_file') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['video_file'] = $upload['upload_data']['file_name'];
    }else{
			if( $_POST['video_file_txt'] == ""){
				$data['video_file'] = "";
			}
		}
    if( $this->upload->do_upload('video_thumb') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['video_thumb'] = $upload['upload_data']['file_name'];
    }else{
			if( $_POST['video_thumb_txt'] == ""){
				$data['video_thumb'] = "";
			}
		}
    if( $this->upload->do_upload('image') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['image'] = $upload['upload_data']['file_name'];
    }else{
      $error = array('error' => $this->upload->display_errors());
      print_r( $error );
			if( $_POST['image_txt'] == ""){
				$data['image'] = "";
			}
		}

    if( $this->upload->do_upload('image_m') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['image_m'] = $upload['upload_data']['file_name'];
    }else{
			if( $_POST['image_m_txt'] == ""){
				$data['image_m'] = "";
			}
		}

    foreach( $this->data['langs'] as $key => $lang ){
      $data['lang'] = $lang['text'];
      $this->db->where('code', $data['code'] );
      $this->db->where('lang', $data['lang'] );
      $q = $this->db->get('banner');
      $result = $q->result_array();

      //$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
      $data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;
      $data['content'] = isset( $content[$key] ) ? $content[$key] : '' ;
      $data['content_editor'] = isset( $content_editor[$key] ) ? $content_editor[$key] : '' ;
      
      if( sizeof($result) > 0 ){
        $data['u_date'] = date('Y-m-d H:i:s') ;
        $data['u_by'] = $this->session->userdata('id');
        $this->db->where('code', $data['code'] );
        $this->db->where('lang', $data['lang'] );
        $this->db->update('banner', $data );
      }else{
        $data['c_date'] = date('Y-m-d H:i:s') ;
        $data['c_by'] = $this->session->userdata('id');
        $this->db->insert('banner', $data );
      }
    }
    $this->common->setAlert('success','Your banner has been saved.','Sucess !');
    redirect('admin/section/banner/edit/'.$data['sec_code'].'/'.$data['code']);
	}
}
