<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solution extends MY_Admin_Controller {

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see https://codeigniter.com/user_guide/general/urls.html
	*/
	public function __construct() {
		parent::__construct();
		if( !( $this->session->userdata('user_type') == "SA" || $this->session->userdata('user_type') == "A" ) ){
			redirect('admin/dashboard');
		}

		$this->load->model($this->modelFolder.'Solution_model','solution',TRUE);
		$this->load->model($this->modelFolder.'Seo_model','seo',TRUE);
	}

	public function index(){
		$this->data['menu_active'] = "solution/lists";

		$this->data['cate_lists'] = $this->solution->load_category_lists();	
		$this->data['lists'] = $this->solution->load_solution_lists();
		//echo '<pre>'; print_r( $this->data['lists'] ); echo '</pre>';
		$this->page_construct('solution_list', $this->data);
	}

	public function lists(){
		$this->data['menu_active'] = "solution/lists";

		$this->data['cate_lists'] = $this->solution->load_category_lists();	
		$this->data['lists'] = $this->solution->load_solution_lists();
		//echo '<pre>'; print_r( $this->data['lists'] ); echo '</pre>';
		$this->page_construct('solution_list', $this->data);
	}

	public function add(){
		$this->data['menu_active'] = "solution/add";
		$this->data['data_code'] = '';//$this->common->get_code( 'product' );
		$this->data['detail'] = array();

		$this->data['business_type_lists'] = $this->solution->load_business_type_lists();
		$this->data['category_lists'] = $this->solution->load_category_lists();

		//echo '<pre>'; print_r( $this->data['category_lists'] ); echo '</pre>';
		$this->page_construct('solution_form', $this->data);
	}

	public function edit( $code = '' ){
		$this->data['menu_active'] = "solution/add";
		if( $code == "" ){
			redirect('admin/dashboard');
		}
		
		$this->data['business_type_lists'] = $this->solution->load_business_type_lists();
		$this->data['category_lists'] = $this->solution->load_category_lists();
		$this->data['detail'] = $this->solution->load_solution_detail( $code );
		$this->data['seo'] = $this->seo->loadDetail( $this->data['detail'][DEFAULT_LANG]['slug'] );
		//echo '<pre>'; print_r( $this->data['seo'] ); echo '</pre>';
		$this->page_construct('solution_form', $this->data);
	}

	public function saveForm()
	{

		
		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'solution' );
			$this->common->addLogs( "solution", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "solution", $data['code'], "update", "" );
		}

		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
		$description = isset( $_POST['description'] ) ? $this->input->post('description') : '';
		$benefit = isset( $_POST['benefit'] ) ? $this->input->post('benefit') : '';
		//sort re-order
        $benefit_reorder = array();
		if ( sizeof($benefit) > 0 ) {
			foreach ($benefit as $key => $row) {
				$benefit_reorder[] = $row;
			}
		}

        // echo '<pre>'; print_r( $benefit ); echo '</pre>';
        
        //echo '<pre>'; print_r( $benefit_reorder ); echo '</pre>';
        // exit;
		
		$data['cate_code'] = isset( $_POST['cate_code'] ) ? json_encode($this->input->post('cate_code')) : '';
		$business_type_code = isset( $_POST['business_type_code'] ) ? $this->input->post('business_type_code') : '';
		//sort re-order
        $business_type_reorder = array();
		if ( isset($business_type_code) && $business_type_code != '' ) {
			foreach ($business_type_code as $key => $row) {
				$business_type_reorder[] = $row;
			}
			$business_type_reorder = json_encode($business_type_reorder);
		} else {
			$business_type_reorder = '';
		}
		$data['business_type_code'] = $business_type_reorder;
		$data['btn_link_type'] = isset( $_POST['btn_link_type'] ) ? $this->input->post('btn_link_type') : '';
		$data['btn_display'] = isset( $_POST['btn_display'] ) ? $this->input->post('btn_display') : '';
		$data['btn_link'] = isset( $_POST['btn_link'] ) ? $this->input->post('btn_link') : '';
		
		$data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';
		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';

		$content = isset( $_POST['content'] ) ? $this->input->post('content') : '';
		$content_editor = isset( $_POST['content_editor'] ) ? $_POST['content_editor'] : '';
		$image_editor = isset( $_FILES['image_editor'] ) ? $_FILES['image_editor'] : array();
		$btn_text = isset( $_POST['btn_text'] ) ? $this->input->post('btn_text') : '';
		$files = $_FILES['brochure_pdf']; // files
		$brochure_pdf_txt = isset( $_POST['brochure_pdf_txt'] ) ? $this->input->post('brochure_pdf_txt') : '';

		$time = isset( $_POST['time'] ) ? $this->input->post('time') : '';
		$this->common->uploadEditorImage( $image_editor, $time );

		// Upload liberies config
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';
		$this->load->library('upload', $config);

		if( $this->upload->do_upload('thumb_image') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['thumb_image'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['thumb_image_txt']) and $_POST['thumb_image_txt'] == ""){
				$data['thumb_image'] = "";
			}
		}
		if( $this->upload->do_upload('banner_desktop_image') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['banner_desktop_image'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['banner_desktop_image_txt']) and $_POST['banner_desktop_image_txt'] == ""){
				$data['banner_desktop_image'] = "";
			}
		}

		if( $this->upload->do_upload('banner_mobile_image') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['banner_mobile_image'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['banner_mobile_image_txt']) and $_POST['banner_mobile_image_txt'] == ""){
				$data['banner_mobile_image'] = "";
			}
		}

		$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
		if ( $slug == '' ) {
			$slug = time();
		} else {
			$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
		}

		if( $data['code'] == '' ){
			$this->db->where('slug', $slug );
		} else {
			$this->db->where('code !=', $data['code'] );
			$this->db->where('slug', $slug );
		}

		$q = $this->db->get('solution');
		$resultx = $q->result_array();
		if( sizeof( $resultx ) > 0 ){
			$data['slug'] = $slug.'-'.time();
		} else {
			$data['slug'] = $slug;
		}

        // echo '<pre>'; print_r( $benefit_reorder ); echo '</pre>'; exit;
        // exit;
		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('solution');
			$result = $q->result_array();

			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
			$data['description'] = isset( $description[$key] ) ? $description[$key] : '' ;
			$data['content'] = isset( $content[$key] ) ? $content[$key] : '' ;
			$data['content_editor'] = isset( $content_editor[$key] ) ? $content_editor[$key] : '' ;
			$data['btn_text'] = isset( $btn_text[$key] ) ? $btn_text[$key] : '' ;

			$data_benefit = array();
			if ( sizeof($benefit_reorder) > 0 ) {
				foreach ($benefit_reorder as $key_benefit => $row) {
					$data_benefit[$key_benefit]['title'] = $row['title'][$key];
					$data_benefit[$key_benefit]['description'] = $row['description'][$key];
				}
				$data['benefit'] = json_encode($data_benefit);
			}

            //echo '<pre>'; print_r( $data_benefit ); echo '</pre>'; exit;
			//upload file multilang
			$_FILES['brochure_pdf']['name'] = $files['name'][$key];
			$_FILES['brochure_pdf']['type'] = $files['type'][$key];
			$_FILES['brochure_pdf']['tmp_name'] = $files['tmp_name'][$key];
			$_FILES['brochure_pdf']['error'] = $files['error'][$key];
			$_FILES['brochure_pdf']['size'] = $files['size'][$key];
		
			if( $this->upload->do_upload('brochure_pdf') ) {
				$upload = array('upload_data' => $this->upload->data());
				$data['brochure_pdf'] = $upload['upload_data']['file_name'];
			} else {
				
				if( isset($brochure_pdf_txt[$key]) && $brochure_pdf_txt[$key] == "") {
					$data['brochure_pdf'] = '';
				}
			}

			//echo '<pre>'; print_r( $data ); echo '</pre>'; exit;

			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('solution', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('solution', $data );
			}
		}

		// Seo data
		$data_seo = array();
			
		$data_seo['code'] = isset( $_POST['seo_code'] ) ? $this->input->post('seo_code') : '';
		if( $data_seo['code'] == '' ){
			$data_seo['code'] = $this->common->get_code( 'seo' );
		}
		$data_seo['seo_type'] = isset( $_POST['seo_type'] ) ? $this->input->post('seo_type') : '';
		$data_seo['page'] = $data['slug'];
		$data_seo['page_title'] = isset( $_POST['page_title'] ) ? $this->input->post('page_title') : '';
		$data_seo['meta_title'] = isset( $_POST['meta_title'] ) ? $this->input->post('meta_title') : '';
		$data_seo['meta_desc'] = isset( $_POST['meta_desc'] ) ? $this->input->post('meta_desc') : '';
		$data_seo['meta_keyword'] = isset( $_POST['meta_keyword'] ) ? $this->input->post('meta_keyword') : '';
		
		//Upload liberies config
		if( $this->upload->do_upload('share_img') ){
			$upload = array('upload_data' => $this->upload->data());
			$data_seo['share_img'] = $upload['upload_data']['file_name'];
		} else {
			if( $_POST['share_img_txt'] == ""){
				$data_seo['share_img'] = "";
			}
		}
		$this->common->updateSEO( $data_seo );
		$this->common->setAlert('success','Solution has been saved.','Success !');


		//echo '<pre>'; print_r( $_POST ); echo '</pre>'; exit;

		redirect('admin/solution/edit/'.$data['code']);
	}

	public function category( $method = "", $code = "" ){
		if ( $method == 'lists' ) {
			$this->data['menu_active'] = "solution/category/lists";
			
			$this->data['lists'] = $this->solution->load_category_lists();
			//echo '<pre>'; print_r( $this->data['lists'] ); echo '</pre>';
			$this->page_construct('solution_category_list', $this->data);
		} else if ( $method == 'add' ) {
			$this->data['menu_active'] = "solution/category/lists";
			$this->data['data_code'] = '';//$this->common->get_code( 'product' );
			$this->data['detail'] = array();
			$this->page_construct('solution_category_form', $this->data);
		} else if ( $method == 'edit' ) {
			$this->data['menu_active'] = "solution/category/lists";
			if( $code == "" ){
				redirect('admin/dashboard');
			}
			
			$this->data['detail'] = $this->solution->load_category_detail( $code );
			$this->data['seo'] = $this->seo->loadDetail( $this->data['detail'][DEFAULT_LANG]['slug'] );

			$this->page_construct('solution_category_form', $this->data);
		} else if ( $method == 'saveForm' ) {
			// echo '<pre>'; print_r($_POST); echo '</pre>';
			// exit;
			$data = array();
			$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
			if( $data['code'] == '' ){
				$data['code'] = $this->common->get_code( 'solution_category' );
				$this->common->addLogs( "solution_category", $data['code'], "create", "" );
			}else{
				$this->common->addLogs( "solution_category", $data['code'], "update", "" );
			}

			$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
			$sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';
			$description = isset( $_POST['description'] ) ? $this->input->post('description') : '';
			$files = $_FILES['brochure_pdf']; // files
			$brochure_pdf_txt = isset( $_POST['brochure_pdf_txt'] ) ? $this->input->post('brochure_pdf_txt') : '';

			$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';


			// Upload liberies config
			$config = array();
			$config['upload_path'] = editor_upload_path();
			$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg|pdf';
			$this->load->library('upload', $config);

			if( $this->upload->do_upload('thumb_image_horizontal') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['thumb_image_horizontal'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['thumb_image_horizontal_txt']) and $_POST['thumb_image_horizontal_txt'] == ""){
					$data['thumb_image_horizontal'] = "";
				}
			}

			if( $this->upload->do_upload('thumb_image_verticals') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['thumb_image_verticals'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['thumb_image_verticals_txt']) and $_POST['thumb_image_verticals_txt'] == ""){
					$data['thumb_image_verticals'] = "";
				}
			}

			if( $this->upload->do_upload('banner_desktop_image') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['banner_desktop_image'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['banner_desktop_image_txt']) and $_POST['banner_desktop_image_txt'] == ""){
					$data['banner_desktop_image'] = "";
				}
			}

			if( $this->upload->do_upload('banner_mobile_image') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['banner_mobile_image'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['banner_mobile_image_txt']) and $_POST['banner_mobile_image_txt'] == ""){
					$data['banner_mobile_image'] = "";
				}
			}

			$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
			if ( $slug == '' ) {
				$slug = time();
			} else {
				$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
			}

			if( $data['code'] == '' ){
				$this->db->where('slug', $slug );
			} else {
				$this->db->where('code !=', $data['code'] );
				$this->db->where('slug', $slug );
			}

			$q = $this->db->get('solution_category');
			$resultx = $q->result_array();
			if( sizeof( $resultx ) > 0 ){
				$data['slug'] = $slug.'-'.time();
			} else {
				$data['slug'] = $slug;
			}

			foreach( $this->data['langs'] as $key => $lang ){
				$data['lang'] = $lang['text'];
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$q = $this->db->get('solution_category');
				$result = $q->result_array();
				
				$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
				$data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;
				$data['description'] = isset( $description[$key] ) ? $description[$key] : '' ;

				$_FILES['brochure_pdf']['name'] = $files['name'][$key];
				$_FILES['brochure_pdf']['type'] = $files['type'][$key];
				$_FILES['brochure_pdf']['tmp_name'] = $files['tmp_name'][$key];
				$_FILES['brochure_pdf']['error'] = $files['error'][$key];
				$_FILES['brochure_pdf']['size'] = $files['size'][$key];
			
				if( $this->upload->do_upload('brochure_pdf') ) {
					$upload = array('upload_data' => $this->upload->data());
					$data['brochure_pdf'] = $upload['upload_data']['file_name'];
				} else {
					
					if( isset($brochure_pdf_txt[$key]) && $brochure_pdf_txt[$key] == "") {
						$data['brochure_pdf'] = '';
					}
				}

				if( sizeof($result) > 0 ){
					$data['u_date'] = date('Y-m-d H:i:s') ;
					$data['u_by'] = $this->session->userdata('id');
					$this->db->where('code', $data['code'] );
					$this->db->where('lang', $data['lang'] );
					$this->db->update('solution_category', $data );
	
				}else{
					$data['c_date'] = date('Y-m-d H:i:s') ;
					$data['c_by'] = $this->session->userdata('id');
					$this->db->insert('solution_category', $data );
				}
			}

			// Seo data
			$data_seo = array();
			
			$data_seo['code'] = isset( $_POST['seo_code'] ) ? $this->input->post('seo_code') : '';
			if( $data_seo['code'] == '' ){
				$data_seo['code'] = $this->common->get_code( 'seo' );
			}
			$data_seo['seo_type'] = isset( $_POST['seo_type'] ) ? $this->input->post('seo_type') : '';
			$data_seo['page'] = $data['slug'];
			$data_seo['page_title'] = isset( $_POST['page_title'] ) ? $this->input->post('page_title') : '';
			$data_seo['meta_title'] = isset( $_POST['meta_title'] ) ? $this->input->post('meta_title') : '';
			$data_seo['meta_desc'] = isset( $_POST['meta_desc'] ) ? $this->input->post('meta_desc') : '';
			$data_seo['meta_keyword'] = isset( $_POST['meta_keyword'] ) ? $this->input->post('meta_keyword') : '';
			
			//Upload liberies config
			if( $this->upload->do_upload('share_img') ){
				$upload = array('upload_data' => $this->upload->data());
				$data_seo['share_img'] = $upload['upload_data']['file_name'];
			} else {
				if( $_POST['share_img_txt'] == ""){
					$data_seo['share_img'] = "";
				}
			}
			$this->common->updateSEO( $data_seo );
			$this->common->setAlert('success','Category has been saved.','Success !');

			redirect('admin/solution/category/edit/'.$data['code']);
		}
	}


	public function business_type( $method = "", $code = "" ){
		if ( $method == 'lists' ) {
			$this->data['menu_active'] = "solution/business_type/lists";
			
			$this->data['lists'] = $this->solution->load_business_type_lists();
			//echo '<pre>'; print_r( $this->data['lists'] ); echo '</pre>';
			$this->page_construct('solution_business_type_list', $this->data);
		} else if ( $method == 'add' ) {
			$this->data['menu_active'] = "solution/business_type/add";
			$this->data['data_code'] = '';//$this->common->get_code( 'product' );
			$this->data['detail'] = array();
			$this->page_construct('solution_business_type_form', $this->data);
		} else if ( $method == 'edit' ) {
			$this->data['menu_active'] = "solution/business_type/add";
			if( $code == "" ){
				redirect('admin/dashboard');
			}
			
			$this->data['detail'] = $this->solution->load_business_type_detail( $code );

			$this->page_construct('solution_business_type_form', $this->data);
		} else if ( $method == 'saveForm' ) {
			// echo '<pre>'; print_r($_POST); echo '</pre>';
			// exit;
			$data = array();
			$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
			if( $data['code'] == '' ){
				$data['code'] = $this->common->get_code( 'solution_business_type' );
				$this->common->addLogs( "solution_business_type", $data['code'], "create", "" );
			}else{
				$this->common->addLogs( "solution_business_type", $data['code'], "update", "" );
			}

			$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';

			$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';


			// Upload liberies config
			$config = array();
			$config['upload_path'] = editor_upload_path();
			$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg|pdf';
			$this->load->library('upload', $config);

			if( isset( $_POST['icon_image_txt'] ) ){
				if( $this->upload->do_upload('icon_image') ){
					$upload = array('upload_data' => $this->upload->data());
					$data['icon_image'] = $upload['upload_data']['file_name'];
				}else{
					if( isset($_POST['icon_image_txt']) and $_POST['icon_image_txt'] == ""){
						$data['icon_image'] = "";
					}
				}
			}
		

			foreach( $this->data['langs'] as $key => $lang ){
				$data['lang'] = $lang['text'];
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$q = $this->db->get('solution_business_type');
				$result = $q->result_array();
				
				$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
				
	
				if( sizeof($result) > 0 ){
					$data['u_date'] = date('Y-m-d H:i:s') ;
					$data['u_by'] = $this->session->userdata('id');
					$this->db->where('code', $data['code'] );
					$this->db->where('lang', $data['lang'] );
					$this->db->update('solution_business_type', $data );
	
				}else{
					$data['c_date'] = date('Y-m-d H:i:s') ;
					$data['c_by'] = $this->session->userdata('id');
					$this->db->insert('solution_business_type', $data );
				}
			}

			redirect('admin/solution/business_type/edit/'.$data['code']);
			// echo '<pre>'; print_r($_POST); echo '</pre>';
			// exit;
		}
	}

	public function brochure( $method = "", $code = "" ){
		 if ( $method == 'edit' ) {
			$this->data['menu_active'] = "solution/brochure/edit";
			if( $code == "" ){
				redirect('admin/dashboard');
			}
			
			$this->data['detail'] = $this->solution->load_brochure_detail( $code );

			$this->page_construct('solution_brochure_form', $this->data);
		} else if ( $method == 'saveForm' ) {
			// echo '<pre>'; print_r($_POST); echo '</pre>';
			// exit;
			$data = array();
			$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
			if( $data['code'] == '' ){
				$data['code'] = $this->common->get_code( 'solution_brochure' );
				$this->common->addLogs( "solution_brochure", $data['code'], "create", "" );
			}else{
				$this->common->addLogs( "solution_brochure", $data['code'], "update", "" );
			}

			$files = $_FILES['brochure_pdf']; // files
			$brochure_pdf_txt = isset( $_POST['brochure_pdf_txt'] ) ? $this->input->post('brochure_pdf_txt') : '';

			$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';

			// Upload liberies config
			$config = array();
			$config['upload_path'] = editor_upload_path();
			$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg|pdf';
			$this->load->library('upload', $config);

			foreach( $this->data['langs'] as $key => $lang ){
				$data['lang'] = $lang['text'];
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$q = $this->db->get('solution_brochure');
				$result = $q->result_array();

				$_FILES['brochure_pdf']['name'] = $files['name'][$key];
				$_FILES['brochure_pdf']['type'] = $files['type'][$key];
				$_FILES['brochure_pdf']['tmp_name'] = $files['tmp_name'][$key];
				$_FILES['brochure_pdf']['error'] = $files['error'][$key];
				$_FILES['brochure_pdf']['size'] = $files['size'][$key];
			
				if( $this->upload->do_upload('brochure_pdf') ) {
					$upload = array('upload_data' => $this->upload->data());
					$data['brochure_pdf'] = $upload['upload_data']['file_name'];
				} else {
					
					if( isset($brochure_pdf_txt[$key]) && $brochure_pdf_txt[$key] == "") {
						$data['brochure_pdf'] = '';
					}
				}

				if( sizeof($result) > 0 ){
					$data['u_date'] = date('Y-m-d H:i:s') ;
					$data['u_by'] = $this->session->userdata('id');
					$this->db->where('code', $data['code'] );
					$this->db->where('lang', $data['lang'] );
					$this->db->update('solution_brochure', $data );
	
				}else{
					$data['c_date'] = date('Y-m-d H:i:s') ;
					$data['c_by'] = $this->session->userdata('id');
					$this->db->insert('solution_brochure', $data );
				}
			}

			redirect('admin/solution/brochure/edit/'.$data['code']);
		}
	}

	
}
