<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends MY_Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 public function __construct() {
		 parent::__construct();
     if( !( $this->session->userdata('user_type') == "SA" || $this->session->userdata('user_type') == "A" ) ){
       redirect('admin/dashboard');
     }
     $this->data['menu_active'] = "setting";
     $this->load->model($this->modelFolder.'Setting_model','setting',TRUE);
 }
	public function index(){
    $this->data['detail'] = $this->setting->loadDetail( 1 );
    $this->data['seo'] = $this->common->loadSEO( $this->data['detail'][DEFAULT_LANG]['seo_id'] );
		$this->page_construct('setting', $this->data);
	}
  public function wording( $action = "", $id = "" ){
    $this->data['menu_active'] = "wording";
    if( $action == "add" ){
      $this->data['detail'] = array();
      $this->page_construct('wordingForm', $this->data);
    }else if( $action == "edit" ){
      if( $id != "" ){
        // Load detail for edit
        $this->data['detail'] = $this->setting->loadWordDetail( $id );
        $this->page_construct('wordingForm', $this->data);
      }else{
        redirect('admin/setting/wording');
      }
    }else{
      $this->data['lists'] = $this->setting->loadWords();
  		$this->page_construct('wording', $this->data);
    }
	}
  public function saveForm()
	{
    $data = array();
    $data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
    $this->common->addLogs( "setting", $data['code'], "update", "" );
    $data['seo_status'] = isset( $_POST['seo_status'] ) ? $this->input->post('seo_status') : '';

    $com_name = isset( $_POST['com_name'] ) ? $this->input->post('com_name') : '';
    $com_address = isset( $_POST['com_address'] ) ? $this->input->post('com_address') : '';
    $copy_right = isset( $_POST['copy_right'] ) ? $this->input->post('copy_right') : '';
    $data['link_to_map'] = isset( $_POST['link_to_map'] ) ? $this->input->post('link_to_map') : '';
    $data['com_phone'] = isset( $_POST['com_phone'] ) ? $this->input->post('com_phone') : '';
    $data['phone_call'] = isset( $_POST['phone_call'] ) ? $this->input->post('phone_call') : '';
    $data['com_email'] = isset( $_POST['com_email'] ) ? $this->input->post('com_email') : '';
    $data['facebook'] = isset( $_POST['facebook'] ) ? $this->input->post('facebook') : '';
    $data['facebook2'] = isset( $_POST['facebook2'] ) ? $this->input->post('facebook2') : '';
    $data['twitter'] = isset( $_POST['twitter'] ) ? $this->input->post('twitter') : '';
    $data['line'] = isset( $_POST['line'] ) ? $this->input->post('line') : '';
    $data['youtube'] = isset( $_POST['youtube'] ) ? $this->input->post('youtube') : '';
    $data['instagram'] = isset( $_POST['instagram'] ) ? $this->input->post('instagram') : '';
    $data['corp_map'] = isset( $_POST['corp_map'] ) ? $this->input->post('corp_map') : '';

    // Upload liberies config
    $config['upload_path'] = editor_upload_path();
    $config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|svg';
    $this->load->library('upload', $config);

    if( $this->upload->do_upload('logo_top') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['logo_top'] = $upload['upload_data']['file_name'];
    }
    if( $this->upload->do_upload('logo_foot') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['logo_foot'] = $upload['upload_data']['file_name'];
    }
    if( $this->upload->do_upload('contact_banner') ){
      $upload = array('upload_data' => $this->upload->data());
      $data['contact_banner'] = $upload['upload_data']['file_name'];
    }

    $data_seo = array();
    $seo_id = isset( $_POST['seo_id'] ) ? $this->input->post('seo_id') : '';
    $data_seo['slug'] = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
    $data_seo['meta_title'] = isset( $_POST['meta_title'] ) ? $this->input->post('meta_title') : '';
    $data_seo['meta_desc'] = isset( $_POST['meta_desc'] ) ? $this->input->post('meta_desc') : '';
    $data_seo['meta_keyword'] = isset( $_POST['meta_keyword'] ) ? $this->input->post('meta_keyword') : '';
    if( $this->upload->do_upload('share_img') ){
      $upload = array('upload_data' => $this->upload->data());
      $data_seo['share_img'] = $upload['upload_data']['file_name'];
    }
    $data['seo_id'] = $this->common->updateSEO( $seo_id , $data_seo);

    foreach( $this->data['langs'] as $key => $lang ){
      $data['lang'] = $lang['text'];
      $this->db->where('code', $data['code'] );
      $this->db->where('lang', $data['lang'] );
      $q = $this->db->get('setting');
      $result = $q->result_array();

      $data['com_name'] = isset( $com_name[$key] ) ? $com_name[$key] : '' ;
      $data['com_address'] = isset( $com_address[$key] ) ? $com_address[$key] : '' ;
      $data['copy_right'] = isset( $copy_right[$key] ) ? $copy_right[$key] : '' ;

      if( sizeof($result) > 0 ){

        $data['u_date'] = date('Y-m-d H:i:s') ;
        $data['u_by'] = $this->session->userdata('id');
        $this->db->where('code', $data['code'] );
        $this->db->where('lang', $data['lang'] );
        $this->db->update('setting', $data );

      }else{
        $data['c_date'] = date('Y-m-d H:i:s') ;
        $data['c_by'] = $this->session->userdata('id');
        $this->db->insert('setting', $data );
      }
    }
    $this->common->setAlert('success','Setting has been saved.','Sucess !');
    redirect('admin/setting');
	}
  public function saveWord()
	{
    $data = array();
    $id = isset( $_POST['id'] ) ? $this->input->post('id') : '';
    if( $id == "" ){
      $data['label'] = isset( $_POST['label'] ) ? $this->input->post('label') : '';
      $this->common->addLogs( "wording", $id, "create", "" );
    }else{
      $this->common->addLogs( "wording", $id, "update", "" );
    }
    $data['thailand'] = isset( $_POST['thailand'] ) ? $this->input->post('thailand') : '';
    $data['english'] = isset( $_POST['english'] ) ? $this->input->post('english') : '';

    if( $id != "" ){
      $data['u_date'] = date('Y-m-d H:i:s') ;
      $data['u_by'] = $this->session->userdata('id');
      $this->db->where('id', $id );
      $this->db->update('wording', $data );
    }else{
      $data['c_date'] = date('Y-m-d H:i:s') ;
      $data['c_by'] = $this->session->userdata('id');
      $this->db->insert('wording', $data );
    }
    $this->common->setAlert('success','Wording has been saved.','Sucess !');
    redirect('admin/setting/wording');
	}
}
