<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Experience extends MY_Admin_Controller {

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see https://codeigniter.com/user_guide/general/urls.html
	*/
	public function __construct() {
		parent::__construct();
		if( !( $this->session->userdata('user_type') == "SA" || $this->session->userdata('user_type') == "A" ) ){
			redirect('admin/dashboard');
		}

		$this->load->model($this->modelFolder.'Experience_model','experience',TRUE);
		$this->load->model($this->modelFolder.'Seo_model','seo',TRUE);
	}

	public function index(){
		$this->data['menu_active'] = "experience/lists";
			
		$this->data['lists'] = $this->experience->load_experience_lists();
		//echo '<pre>'; print_r( $this->data['lists'] ); echo '</pre>';
		$this->page_construct('experience_list', $this->data);
	}

	public function lists(){
		$this->data['menu_active'] = "experience/lists";

		$this->data['cate_lists'] = $this->experience->load_category_lists();	
		$this->data['lists'] = $this->experience->load_experience_lists();
		//echo '<pre>'; print_r( $this->data['lists'] ); echo '</pre>';
		$this->page_construct('experience_list', $this->data);
	}

	public function add(){
		$this->data['menu_active'] = "experience/add";
		$this->data['data_code'] = '';//$this->common->get_code( 'product' );
		$this->data['detail'] = array();

		$this->data['solution_used_lists'] = $this->experience->load_solution_used_lists();
		$this->data['solution_category_lists'] = $this->experience->load_solution_category_lists();
		$this->data['category_lists'] = $this->experience->load_category_lists();

		//echo '<pre>'; print_r( $this->data['category_lists'] ); echo '</pre>';
		$this->page_construct('experience_form', $this->data);
	}

	public function edit( $code = '' ){
		$this->data['menu_active'] = "experience/add";
		if( $code == "" ){
			redirect('admin/dashboard');
		}
		
		$this->data['solution_used_lists'] = $this->experience->load_solution_used_lists();
		$this->data['solution_category_lists'] = $this->experience->load_solution_category_lists();
		$this->data['category_lists'] = $this->experience->load_category_lists();
		$this->data['detail'] = $this->experience->load_experience_detail( $code );
		$this->data['seo'] = $this->seo->loadDetail( $this->data['detail'][DEFAULT_LANG]['slug'] );
		//echo '<pre>'; print_r( $this->data['seo'] ); echo '</pre>';
		$this->page_construct('experience_form', $this->data);
	}

	public function saveForm()
	{
		// echo '<pre>'; print_r( $_FILES ); echo '</pre>';
		// exit;
		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'experience' );
			$this->common->addLogs( "experience", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "experience", $data['code'], "update", "" );
		}

		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
        $description = isset( $_POST['description'] ) ? $this->input->post('description') : '';
        
		
		$data['cate_code'] = isset( $_POST['cate_code'] ) ? json_encode($this->input->post('cate_code')) : '';
		$data['solution_cate_code'] = isset( $_POST['solution_cate_code'] ) ? json_encode($this->input->post('solution_cate_code')) : '';
		
		$solution_used_code = isset( $_POST['solution_used_code'] ) ? $this->input->post('solution_used_code') : '';
		//sort re-order
		if ( isset($solution_used_code) && $solution_used_code != '' ) {
			foreach ($solution_used_code as $key => $row) {
				$solution_used_code[$key] = $row;
			}
			$solution_used_code = json_encode($solution_used_code);
		} else {
			$solution_used_code = '';
		}
		$data['solution_used_code'] = $solution_used_code;

		
		$data['vdo_youtube_id'] = isset( $_POST['vdo_youtube_id'] ) ? $this->input->post('vdo_youtube_id') : '';
		$data['sticky'] = isset( $_POST['sticky'] ) ? $this->input->post('sticky') : '';
		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
		$data['btn_link_type'] = isset( $_POST['btn_link_type'] ) ? $this->input->post('btn_link_type') : '';
		$data['btn_display'] = isset( $_POST['btn_display'] ) ? $this->input->post('btn_display') : '';
		$data['btn_link'] = isset( $_POST['btn_link'] ) ? $this->input->post('btn_link') : '';

		$content = isset( $_POST['content'] ) ? $this->input->post('content') : '';
		$content_editor = isset( $_POST['content_editor'] ) ? $_POST['content_editor'] : '';
		$image_editor = isset( $_FILES['image_editor'] ) ? $_FILES['image_editor'] : array();
		$btn_text = isset( $_POST['btn_text'] ) ? $this->input->post('btn_text') : '';
		

		$time = isset( $_POST['time'] ) ? $this->input->post('time') : '';
		$this->common->uploadEditorImage( $image_editor, $time );

		// Upload liberies config
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg';
		$this->load->library('upload', $config);

		if( $this->upload->do_upload('thumb_image') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['thumb_image'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['thumb_image_txt']) and $_POST['thumb_image_txt'] == ""){
				$data['thumb_image'] = "";
			}
		}

		if( $this->upload->do_upload('logo_image') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['logo_image'] = $upload['upload_data']['file_name'];
		}else{
			if( isset($_POST['logo_image_txt']) and $_POST['logo_image_txt'] == ""){
				$data['logo_image'] = "";
			}
		}


		$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
		if ( $slug == '' ) {
			$slug = time();
		} else {
			$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
		}

		if( $data['code'] == '' ){
			$this->db->where('slug', $slug );
		} else {
			$this->db->where('code !=', $data['code'] );
			$this->db->where('slug', $slug );
		}

		$q = $this->db->get('experience');
		$resultx = $q->result_array();
		if( sizeof( $resultx ) > 0 ){
			$data['slug'] = $slug.'-'.time();
		} else {
			$data['slug'] = $slug;
		}

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('experience');
			$result = $q->result_array();

			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
            $data['description'] = isset( $description[$key] ) ? $description[$key] : '' ;
			$data['content'] = isset( $content[$key] ) ? $content[$key] : '' ;
			$data['content_editor'] = isset( $content_editor[$key] ) ? $content_editor[$key] : '' ;
			$data['btn_text'] = isset( $btn_text[$key] ) ? $btn_text[$key] : '' ;

			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('experience', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('experience', $data );
			}
		}

		// Seo data
		$data_seo = array();
			
		$data_seo['code'] = isset( $_POST['seo_code'] ) ? $this->input->post('seo_code') : '';
		if( $data_seo['code'] == '' ){
			$data_seo['code'] = $this->common->get_code( 'seo' );
		}
		$data_seo['seo_type'] = isset( $_POST['seo_type'] ) ? $this->input->post('seo_type') : '';
		$data_seo['page'] = $data['slug'];
		$data_seo['page_title'] = isset( $_POST['page_title'] ) ? $this->input->post('page_title') : '';
		$data_seo['meta_title'] = isset( $_POST['meta_title'] ) ? $this->input->post('meta_title') : '';
		$data_seo['meta_desc'] = isset( $_POST['meta_desc'] ) ? $this->input->post('meta_desc') : '';
		$data_seo['meta_keyword'] = isset( $_POST['meta_keyword'] ) ? $this->input->post('meta_keyword') : '';
		
		//Upload liberies config
		if( $this->upload->do_upload('share_img') ){
			$upload = array('upload_data' => $this->upload->data());
			$data_seo['share_img'] = $upload['upload_data']['file_name'];
		} else {
			if( $_POST['share_img_txt'] == ""){
				$data_seo['share_img'] = "";
			}
		}
		$this->common->updateSEO( $data_seo );
		$this->common->setAlert('success','Experience has been saved.','Success !');


		//echo '<pre>'; print_r( $_POST ); echo '</pre>'; exit;

		redirect('admin/experience/edit/'.$data['code']);
	}

	public function category( $method = "", $code = "" ){
		if ( $method == 'lists' ) {
			$this->data['menu_active'] = "experience/category/lists";
			
			$this->data['lists'] = $this->experience->load_category_lists();
			//echo '<pre>'; print_r( $this->data['lists'] ); echo '</pre>';
			$this->page_construct('experience_category_list', $this->data);
		} else if ( $method == 'add' ) {
			$this->data['menu_active'] = "experience/category/lists";
			$this->data['data_code'] = '';//$this->common->get_code( 'product' );
			$this->data['detail'] = array();
			$this->page_construct('experience_category_form', $this->data);
		} else if ( $method == 'edit' ) {
			$this->data['menu_active'] = "experience/category/lists";
			if( $code == "" ){
				redirect('admin/dashboard');
			}
			
			$this->data['detail'] = $this->experience->load_category_detail( $code );
			$this->data['seo'] = $this->seo->loadDetail( $this->data['detail'][DEFAULT_LANG]['slug'] );

			$this->page_construct('experience_category_form', $this->data);
		} else if ( $method == 'saveForm' ) {
			// echo '<pre>'; print_r($_POST); echo '</pre>';
			// exit;
			$data = array();
			$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
			if( $data['code'] == '' ){
				$data['code'] = $this->common->get_code( 'experience_category' );
				$this->common->addLogs( "experience_category", $data['code'], "create", "" );
			}else{
				$this->common->addLogs( "experience_category", $data['code'], "update", "" );
			}

			$prefix_title = isset( $_POST['prefix_title'] ) ? $this->input->post('prefix_title') : '';
			$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
			$topic_content = isset( $_POST['topic_content'] ) ? $this->input->post('topic_content') : '';
			$description = isset( $_POST['description'] ) ? $this->input->post('description') : '';

			$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';


			// Upload liberies config
			$config = array();
			$config['upload_path'] = editor_upload_path();
			$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg|pdf';
			$this->load->library('upload', $config);

			if( $this->upload->do_upload('thumb_image') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['thumb_image'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['thumb_image_txt']) and $_POST['thumb_image_txt'] == ""){
					$data['thumb_image'] = "";
				}
			}

			if( $this->upload->do_upload('banner_desktop_image') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['banner_desktop_image'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['banner_desktop_image_txt']) and $_POST['banner_desktop_image_txt'] == ""){
					$data['banner_desktop_image'] = "";
				}
			}

			if( $this->upload->do_upload('banner_mobile_image') ){
				$upload = array('upload_data' => $this->upload->data());
				$data['banner_mobile_image'] = $upload['upload_data']['file_name'];
			}else{
				if( isset($_POST['banner_mobile_image_txt']) and $_POST['banner_mobile_image_txt'] == ""){
					$data['banner_mobile_image'] = "";
				}
			}

			$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
			if ( $slug == '' ) {
				$slug = time();
			} else {
				$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
			}

			if( $data['code'] == '' ){
				$this->db->where('slug', $slug );
			} else {
				$this->db->where('code !=', $data['code'] );
				$this->db->where('slug', $slug );
			}

			$q = $this->db->get('experience_category');
			$resultx = $q->result_array();
			if( sizeof( $resultx ) > 0 ){
				$data['slug'] = $slug.'-'.time();
			} else {
				$data['slug'] = $slug;
			}

			foreach( $this->data['langs'] as $key => $lang ){
				$data['lang'] = $lang['text'];
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$q = $this->db->get('experience_category');
				$result = $q->result_array();
				
				$data['prefix_title'] = isset( $prefix_title[$key] ) ? $prefix_title[$key] : '' ;
				$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
				$data['topic_content'] = isset( $topic_content[$key] ) ? $topic_content[$key] : '' ;
				$data['description'] = isset( $description[$key] ) ? $description[$key] : '' ;
	
				if( sizeof($result) > 0 ){
					$data['u_date'] = date('Y-m-d H:i:s') ;
					$data['u_by'] = $this->session->userdata('id');
					$this->db->where('code', $data['code'] );
					$this->db->where('lang', $data['lang'] );
					$this->db->update('experience_category', $data );
	
				}else{
					$data['c_date'] = date('Y-m-d H:i:s') ;
					$data['c_by'] = $this->session->userdata('id');
					$this->db->insert('experience_category', $data );
				}
			}

			// Seo data
			$data_seo = array();
			
			$data_seo['code'] = isset( $_POST['seo_code'] ) ? $this->input->post('seo_code') : '';
			if( $data_seo['code'] == '' ){
				$data_seo['code'] = $this->common->get_code( 'seo' );
			}
			$data_seo['seo_type'] = isset( $_POST['seo_type'] ) ? $this->input->post('seo_type') : '';
			$data_seo['page'] = $data['slug'];
			$data_seo['page_title'] = isset( $_POST['page_title'] ) ? $this->input->post('page_title') : '';
			$data_seo['meta_title'] = isset( $_POST['meta_title'] ) ? $this->input->post('meta_title') : '';
			$data_seo['meta_desc'] = isset( $_POST['meta_desc'] ) ? $this->input->post('meta_desc') : '';
			$data_seo['meta_keyword'] = isset( $_POST['meta_keyword'] ) ? $this->input->post('meta_keyword') : '';
			
			//Upload liberies config
			if( $this->upload->do_upload('share_img') ){
				$upload = array('upload_data' => $this->upload->data());
				$data_seo['share_img'] = $upload['upload_data']['file_name'];
			} else {
				if( $_POST['share_img_txt'] == ""){
					$data_seo['share_img'] = "";
				}
			}
			$this->common->updateSEO( $data_seo );
			$this->common->setAlert('success','Category has been saved.','Success !');

			redirect('admin/experience/category/edit/'.$data['code']);
		}
	}

}
