<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Smepack_category extends MY_Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
		* 		http://example.com/index.php/welcome/index
		*	- or -
		* Since this controller is set as the default controller in
		* config/routes.php, it's displayed at http://example.com/
		*
		* So any other public methods not prefixed with an underscore will
		* map to /index.php/welcome/<method_name>
		* @see https://codeigniter.com/user_guide/general/urls.html
		*/
	public function __construct() {
		parent::__construct();
		$this->load->model($this->modelFolder.'Smepack_category_model','category',TRUE);
		$this->load->model($this->modelFolder.'Seo_model','seo',TRUE);
	}
	public function index(){}
	public function lists( $code = "",$edit_code = "" ){
		if( $code == "" || $code == 0 || $code > 5 ){
			redirect('admin/dashboard');
		}

		$this->data['menu_active'] = 'smepack_category/lists/'.$code;
		$this->data['cate_detail'] = $this->category->loadDetail( $code );
		$this->data['lists'] = $this->category->loadListsChild( $code );

		$this->data['edit_cate_detail'] = array();
		if ( $edit_code != '' ) {
			$this->data['edit_cate_detail'] = $this->category->loadDetail( $edit_code );
		}

		$this->page_construct('smepack_category_list', $this->data);
	}

	public function content( $code = "" ){
		if( $code == "" || $code == 0 || $code > 5 ){
			redirect('admin/dashboard');
		}

		$this->data['menu_active'] = 'smepack_category/content/'.$code;		
		$this->data['detail'] = $this->category->loadDetail( $code );
		$this->data['seo'] = $this->seo->loadDetail( $this->data['detail'][DEFAULT_LANG]['slug'] );
		$this->page_construct('smepack_category_content', $this->data);
	}

	public function main_content(){
		$code = 0;

		$this->data['menu_active'] = 'smepack_category/main_content';
		$this->data['detail'] = $this->category->loadDetail( $code );
		$this->data['seo'] = $this->seo->loadDetail( $this->data['detail'][DEFAULT_LANG]['slug'] );
		$this->page_construct('smepack_category_main_content', $this->data);
	}

	

	public function lists_ajax() {
		$ar_result = array();
		$cate_for = isset( $_POST['cate_for'] ) ? $this->input->post('cate_for') : '';
		$parent_code = isset( $_POST['parent_code'] ) ? $this->input->post('parent_code') : '';
		$ar_result['rs'] = true;
		$ar_result['lists'] = $this->category->loadLists( $cate_for, $parent_code );
		echo json_encode( $ar_result );
	}

	public function edit(){
		$ar_result = array();
		$code = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		$this->data['menu_active'] = 'category/edit/'.$code;
		if( $code == "" ){
			redirect('admin/dashboard');
		}
		$this->data['detail'] = $this->category->loadDetail( $code );
		$ar_result['details'] = $this->data['detail'];
		$ar_result['rs'] = true;
		echo json_encode( $ar_result );
	}
	public function add( $cate_group = "content" ){
		$this->data['menu_active'] = 'category/add/';
		$this->data['detail'] = array();
		$this->page_construct('categoryForm', $this->data);
	}
	
	public function saveCategoryForm(){
		//print_r($_POST);
		//exit();
		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		$data['parent_code'] = isset( $_POST['parent_code'] ) ? $this->input->post('parent_code') : '';
		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'smepack_category' );
			$this->common->addLogs( "smepack_category", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "smepack_category", $data['code'], "update", "" );
		}

		$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
		if ( $slug == '' ) {
			$slug = time();
		} else {
			$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
		}
		
		if( $data['code'] == '' ){
			$this->db->where('slug', $slug );
		} else {
			$this->db->where('code !=', $data['code'] );
			$this->db->where('slug', $slug );
		}

		$q = $this->db->get('smepack_category');
		$resultx = $q->result_array();
		if( sizeof( $resultx ) > 0 ){
			$data['slug'] = $slug.'-'.time();
		} else {
			$data['slug'] = $slug;
		}

		$package_name = isset( $_POST['package_name'] ) ? $this->input->post('package_name') : '';
		$prefix_title = isset( $_POST['prefix_title'] ) ? $this->input->post('prefix_title') : '';
		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
		$short_description = isset( $_POST['short_description'] ) ? $this->input->post('short_description') : '';
		$description = isset( $_POST['description'] ) ? $this->input->post('description') : '';

		$config = array();
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|jpeg|svg';

		$this->load->library('upload', $config);

		if( $this->upload->do_upload('thumb') ) {
			$upload = array('upload_data' => $this->upload->data());
			$data['thumb'] = $upload['upload_data']['file_name'];
		} else {
			if( isset($_POST['thumb_txt']) && $_POST['thumb_txt'] == "") {
				$data['thumb'] = "";
			}
		}

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('smepack_category');
			$result = $q->result_array();

			$data['package_name'] = isset( $package_name[$key] ) ? $package_name[$key] : '' ;
			$data['prefix_title'] = isset( $prefix_title[$key] ) ? $prefix_title[$key] : '' ;
			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
			$data['short_description'] = isset( $short_description[$key] ) ? $short_description[$key] : '' ;
			$data['description'] = isset( $description[$key] ) ? $description[$key] : '' ;
			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('smepack_category', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('smepack_category', $data );
			}
		}
        $this->common->setAlert('success','Category has been saved.','Success !');

		redirect('admin/smepack_category/lists/'.$data['parent_code'].'/'.$data['code']);
	}

	public function saveCategoryContentForm(){

		$data = array();
		$data['code'] = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : '';
		if( $data['code'] == '' ){
			$data['code'] = $this->common->get_code( 'smepack_category' );
			$this->common->addLogs( "smepack_category", $data['code'], "create", "" );
		}else{
			$this->common->addLogs( "smepack_category", $data['code'], "update", "" );
		}

		$slug = isset( $_POST['slug'] ) ? $this->input->post('slug') : '';
		if ( $slug == '' ) {
			$slug = time();
		} else {
			$slug = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $slug) ); // Removes special chars.
		}
		
		if( $data['code'] == '' ){
			$this->db->where('slug', $slug );
		} else {
			$this->db->where('code !=', $data['code'] );
			$this->db->where('slug', $slug );
		}

		$q = $this->db->get('smepack_category');
		$resultx = $q->result_array();
		if( sizeof( $resultx ) > 0 ){
			$data['slug'] = $slug.'-'.time();
		} else {
			$data['slug'] = $slug;
		}

		$prefix_title = isset( $_POST['prefix_title'] ) ? $this->input->post('prefix_title') : '';
		$title = isset( $_POST['title'] ) ? $this->input->post('title') : '';
		$sub_title = isset( $_POST['sub_title'] ) ? $this->input->post('sub_title') : '';
		$description = isset( $_POST['description'] ) ? $this->input->post('description') : '';
        $files = $_FILES['brochure_pdf']; // files
		$brochure_pdf_txt = isset( $_POST['brochure_pdf_txt'] ) ? $this->input->post('brochure_pdf_txt') : '';

		// Upload liberies config
		$config = array();
		$config['upload_path'] = editor_upload_path();
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp4|zip|rar|7zip|doc|docx|xls|xlsx|ppt|pptx|pdf|svg';
		$this->load->library('upload', $config);

		if( $this->upload->do_upload('icon_image') ) {
			$upload = array('upload_data' => $this->upload->data());
			$data['icon_image'] = $upload['upload_data']['file_name'];
		} else {
			if( isset($_POST['icon_image_txt']) && $_POST['icon_image_txt'] == "") {
				$data['icon_image'] = "";
			}
		}
        

		// if( $this->upload->do_upload('brochure_pdf') ) {
		// 	$upload = array('upload_data' => $this->upload->data());
		// 	$data['brochure_pdf'] = $upload['upload_data']['file_name'];
		// } else {
		// 	if( isset($_POST['brochure_pdf_txt']) && $_POST['brochure_pdf_txt'] == "") {
		// 		$data['brochure_pdf'] = "";
		// 	}
		// }

		if( $this->upload->do_upload('bg_section') ) {
			$upload = array('upload_data' => $this->upload->data());
			$data['bg_section'] = $upload['upload_data']['file_name'];
		} else {
			if( isset($_POST['bg_section_txt']) && $_POST['bg_section_txt'] == "") {
				$data['bg_section'] = "";
			}
		}

		if( $this->upload->do_upload('banner_d') ) {
			$upload = array('upload_data' => $this->upload->data());
			$data['banner_d'] = $upload['upload_data']['file_name'];
		} else {
			if( isset($_POST['banner_d_txt']) && $_POST['banner_d_txt'] == "") {
				$data['banner_d'] = "";
			}
		}

		if( $this->upload->do_upload('banner_m') ) {
			$upload = array('upload_data' => $this->upload->data());
			$data['banner_m'] = $upload['upload_data']['file_name'];
		} else {
			if( isset($_POST['banner_m_txt']) && $_POST['banner_m_txt'] == "") {
				$data['banner_m'] = "";
			}
		}

		foreach( $this->data['langs'] as $key => $lang ){
			$data['lang'] = $lang['text'];
			$this->db->where('code', $data['code'] );
			$this->db->where('lang', $data['lang'] );
			$q = $this->db->get('smepack_category');
			$result = $q->result_array();
			
			$data['prefix_title'] = isset( $prefix_title[$key] ) ? $prefix_title[$key] : '' ;
			$data['title'] = isset( $title[$key] ) ? $title[$key] : '' ;
			$data['sub_title'] = isset( $sub_title[$key] ) ? $sub_title[$key] : '' ;
			$data['description'] = isset( $description[$key] ) ? $description[$key] : '' ;

            $_FILES['brochure_pdf']['name'] = $files['name'][$key];
            $_FILES['brochure_pdf']['type'] = $files['type'][$key];
            $_FILES['brochure_pdf']['tmp_name'] = $files['tmp_name'][$key];
            $_FILES['brochure_pdf']['error'] = $files['error'][$key];
            $_FILES['brochure_pdf']['size'] = $files['size'][$key];
        
            if( $this->upload->do_upload('brochure_pdf') ) {
                $upload = array('upload_data' => $this->upload->data());
                $data['brochure_pdf'] = $upload['upload_data']['file_name'];
            } else {
                
                if( isset($brochure_pdf_txt[$key]) && $brochure_pdf_txt[$key] == "") {
                    $data['brochure_pdf'] = '';
                }
            }

			if( sizeof($result) > 0 ){

				$data['u_date'] = date('Y-m-d H:i:s') ;
				$data['u_by'] = $this->session->userdata('id');
				$this->db->where('code', $data['code'] );
				$this->db->where('lang', $data['lang'] );
				$this->db->update('smepack_category', $data );

			}else{
				$data['c_date'] = date('Y-m-d H:i:s') ;
				$data['c_by'] = $this->session->userdata('id');
				$this->db->insert('smepack_category', $data );
			}
		}

		// Seo data
		$data_seo = array();
		
		$data_seo['code'] = isset( $_POST['seo_code'] ) ? $this->input->post('seo_code') : '';
		if( $data_seo['code'] == '' ){
			$data_seo['code'] = $this->common->get_code( 'seo' );
		}
		$data_seo['seo_type'] = isset( $_POST['seo_type'] ) ? $this->input->post('seo_type') : '';
		$data_seo['page'] = $data['slug'];
		$data_seo['page_title'] = isset( $_POST['page_title'] ) ? $this->input->post('page_title') : '';
		$data_seo['meta_title'] = isset( $_POST['meta_title'] ) ? $this->input->post('meta_title') : '';
		$data_seo['meta_desc'] = isset( $_POST['meta_desc'] ) ? $this->input->post('meta_desc') : '';
		$data_seo['meta_keyword'] = isset( $_POST['meta_keyword'] ) ? $this->input->post('meta_keyword') : '';
		
		//Upload liberies config
		if( $this->upload->do_upload('share_img') ){
		  $upload = array('upload_data' => $this->upload->data());
		  $data_seo['share_img'] = $upload['upload_data']['file_name'];
		} else {
			if( $_POST['share_img_txt'] == ""){
				$data_seo['share_img'] = "";
			}
		}
		$this->common->updateSEO( $data_seo );
        $this->common->setAlert('success','Category has been saved.','Success !');

		if ( $data['code'] == 0 ) {
			redirect('admin/smepack_category/main_content');
		} else {
			redirect('admin/smepack_category/content/'.$data['code']);
		}
	}

	public function updateSort(){
		$data = array();
		$ar_result = array();
		$code = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		//$data['parent_code'] = isset( $_POST['parent_code'] ) ? $this->input->post('parent_code') : 0;

		$data['u_date'] = date('Y-m-d H:i:s') ;
		$data['u_by'] = $this->session->userdata('id');
		$this->db->where('code', $code );
		$this->db->update('smepack_category', $data );

		$this->common->addLogs( "smepack_category", $code, "update", "" );
		$ar_result['rs'] = true;
		echo json_encode( $ar_result );
	}
	public function updateSortList(){
		$data = array();
		$ar_result = array();
		$catelists = isset( $_POST['catelists'] ) ? $this->input->post('catelists') : '';

		foreach( $catelists as $list ){
			$data = array();
			$data['sticky'] = $list['sticky'] ;
			$data['u_date'] = date('Y-m-d H:i:s') ;
			$data['u_by'] = $this->session->userdata('id');
			$this->db->where('code', $list['code'] );
			$this->db->update('smepack_category', $data );
		}
		$this->common->addLogs( "smepack_category", "", "update sorting", "" );
		$ar_result['rs'] = true;
		echo json_encode( $ar_result );
	}

	public function activeCate(){
		$data = array();
		$ar_result = array();
		$code = isset( $_POST['code'] ) ? $this->input->post('code') : '';
		$data['onoff'] = isset( $_POST['onoff'] ) ? $this->input->post('onoff') : 0;
		$data['u_date'] = date('Y-m-d H:i:s') ;
		$data['u_by'] = $this->session->userdata('id');
		$this->db->where('code', $code );
		$this->db->update('smepack_category', $data );
		$ar_result['rs'] = true;
		$this->common->addLogs( "smepack_category", $code, "update set active", "" );
		echo json_encode( $ar_result );
	}
}
