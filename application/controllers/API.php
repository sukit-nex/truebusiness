<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API extends MY_Front_Controller {
				  
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see https://codeigniter.com/user_guide/general/urls.html
	*/
	public function __construct() {
		parent::__construct();
	}
	public function loadMapPoint(){
		$zone = isset( $_GET['zone'] ) ? $this->input->get('zone') : '';

		$this->db->select('province');
		$this->db->from('coverage_list');
		$this->db->where('zone', $zone );
		$this->db->group_by('province');
		$q = $this->db->get();
		$provinces = $q->result_array();

		$this->db->select('*');
		$this->db->from('coverage_list');
		$this->db->where('zone', $zone );
		$q = $this->db->get();
		$lists = $q->result_array();

		foreach( $provinces as $key => $province){
			$provinces[ $key ]['lists'] = array();
			foreach( $lists as $list ){
				if( $province['province'] == $list['province']){
					array_push( $provinces[ $key ]['lists'] , $list ) ;
				}
			}
		}
		//print_r( $provinces );
		$this->data['coverage_lists'] = $provinces;
		$this->page_html('inc/coverage_lists', $this->data );
	}
	public function searchMapPoint(){
		$search_area = isset( $_GET['search_area'] ) ? $this->input->get('search_area') : '';
		$ar_search = explode(",",$search_area);

		$this->db->select('*');
		$this->db->from('coverage_list');
		if( $ar_search[0] != ""){
			$this->db->where('province', $ar_search[0] );
		}
		if( $ar_search[1] != ""){
			$this->db->where('district', $ar_search[1] );
		}
		$q = $this->db->get();
		$lists = $q->result_array();

		$this->data['coverage_lists'] = $lists;
		$this->data['search_area'] = $ar_search[0].' , '.$ar_search[1];
		$this->page_html('inc/coverage_search', $this->data );
	}
	public function loadmore(){
		$ar_result = array();
		$sec_code = isset( $_GET['sec_code'] ) ? $this->input->get('sec_code') : '' ;
		$cate_code = isset( $_GET['cate_code'] ) ? $this->input->get('cate_code') : '' ;
		$limit = isset( $_GET['data_limit'] ) ? $this->input->get('data_limit') : '' ;
		$p = isset( $_GET['page'] ) ? $this->input->get('page') : '' ;
		$cate_key = isset( $_GET['cate_key'] ) ? $this->input->get('cate_key') : '' ;
		$data_list = isset( $_GET['data_list'] ) ? $this->input->get('data_list') : '' ;
	
		if( $data_list == "content" ){
			$this->data['cate_key'] = $cate_key;
			$this->data['lists'] = $this->common->loadContentList( $sec_code , $cate_code , $limit, intVal( $p )+1  );

			$this->page_html('inc/inc_load_content', $this->data );
		}
	}
	public function send_contact(){
		date_default_timezone_set("Asia/Bangkok");
		$data = array();
		$data['request'] = $this->security->xss_clean( $this->input->post('subject') );
		$data['title'] = $this->security->xss_clean( $this->input->post('title') );
		$data['fullname'] = $this->security->xss_clean( $this->input->post('fullname') );
		$data['phone'] = $this->security->xss_clean( $this->input->post('phone') );
		$data['email'] = $this->security->xss_clean( $this->input->post('email') );
		$data['message'] = $this->security->xss_clean( $this->input->post('message') );
		$data['c_date'] = @date('Y-m-d H:i:s') ;

		$this->db->insert('contact_us',$data);
		$id = $this->db->insert_id();

		//$this->send_email( $data, CONTACT_EMAIL, $id );

		$ar_result = array();
		$ar_result['rs'] = true;
		echo json_encode( $ar_result );
	}
	public function apply_job(){
		$data = array();
		$data['posi_code'] = isset( $_POST['posi_code'] ) ? $this->input->post('posi_code') : '';
		if( $data['posi_code'] == "" ){
			redirect('home');
		}
		$data['fullname'] = isset( $_POST['fullname'] ) ? $this->security->xss_clean( $this->input->post('fullname') ) : '';
		$data['level'] = isset( $_POST['level'] ) ? $this->security->xss_clean( $this->input->post('level') ) : '';
		$data['email'] = isset( $_POST['email'] ) ? $this->security->xss_clean( $this->input->post('email') ): '';
		$data['phone'] = isset( $_POST['phone'] ) ? $this->security->xss_clean( $this->input->post('phone') ): '';

		// Upload liberies config
		$config['upload_path'] = file_upload_path();
		$config['allowed_types'] = 'jpg|png|pdf|doc|docx';
		$this->load->library('upload', $config);

		if( $this->upload->do_upload('resume') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['resume'] = $upload['upload_data']['file_name'];
		}
		if( $this->upload->do_upload('transcript') ){
			$upload = array('upload_data' => $this->upload->data());
			$data['transcript'] = $upload['upload_data']['file_name'];
		}
		$data['c_date'] = date('Y-m-d H:i:s') ;
		$this->db->insert('career_submit', $data );
		$ar_result = array();
		$ar_result['rs'] = true;
		echo json_encode( $ar_result );
	}

    public function request_token(){
        $ar_result = array();
        $ar_result['rs'] = true;
        //Token expired 1 hour
        $this->db->where('c_date >=', 'DATE_SUB(NOW(), INTERVAL 50 MINUTE' );
        $this->db->order_by('id', 'desc' );
        $this->db->limit(1);
		$q = $this->db->get('api_request_token');
		$result = $q->result_array();
        //print_r($this->db->last_query());

        //echo '<pre>';print_r( $result );echo '</pre>';
		if( sizeof( $result ) > 0 ){
            $row = $result[0];
            $ar_result['key_name'] = $row['key_name'];
		} else {
			if ( 0 ) {
                $url = TRUE_AUTH_URL;
    
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    
                $headers = array(
                    "secret_key: ".TRUE_SECRET_KEY,
                    "Content-Length: 0",
                );
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                //for debug only!
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    
                $resp = curl_exec($curl);
                curl_close($curl);

                if ( isset($resp) && $resp['code'] == 200 && $resp['key'] != '' ) {
                    $data = array();
                    $data['s_token'] = $this->session->userdata('s_token');
                    $data['ip_address'] = $this->session->userdata('client_ip');
                    $data['c_date'] = date('Y-m-d H:i:s') ;
                    $this->db->insert('api_request_token', $data );

                    $ar_result['key_name'] = $resp['key'];
                } else {
                    $ar_result['rs'] = false;
                }
                
            }
		}
        
        

		echo json_encode( $ar_result );
    }
}
