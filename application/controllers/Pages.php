<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Front_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
   	}
	public function index()
	{	
		redirect('/');
	}
	public function template( $page_slug, $page_sub_slug = ""){
		$this->data['css'] = "";
		$this->data['js'] = "";
		$this->data['tag'] = $page_slug;
		$this->data['body_class'] = "";
		$this->data['menu_check'] = $page_slug;
        $this->data['sub_menu_check'] = $page_sub_slug;
		$this->data['page'] = $page_slug;
		$this->data['page_active'] = $page_slug;
		$this->data['page_slug'] = $page_slug;
		//$this->data['banners'] = array(); //$this->common->loadBanner();
		$this->data['page_detail'] = $this->common->page_detail($page_slug);
		
		//echo 'mixer-'.$this->data['sub_menu_check'];
		$this->data['page_seo'] = $this->common->loadSeo($page_slug);

		if($page_sub_slug != ""){
			$this->data['page_detail'] = $this->common->page_detail($page_sub_slug);
			$this->data['page_seo'] = $this->common->loadSeo($page_sub_slug);
		}
		
		$this->data['is_padding'] = "";
		if( sizeof( $this->data['page_detail'] ) > 0 ){
			$this->data['body_class'] = 'page_code_'.$this->data['page_detail']['code'];
			$this->data['rows'] = $this->common->loadSections( $this->data['page_detail']['code'] );
			//print_r( $this->data['rows'] );
			if(sizeof($this->data['rows']) > 0){
				foreach( $this->data['rows'] as $row ){
					foreach( $row['columns'] as $column ){
						foreach( $column['sections'] as $section ){
							if( $section['section_type'] == "widget"){

								if ( $section['widget'] == "coverage" ) {

									$this->data['provinces'] = $this->common->loadProvinceList();
									$this->data['coverage'] = $this->common->loadCoverage();
									$this->data['coverage_groups'] = $this->common->loadCoverageGroup();
									$this->data['coverage_search'] = $this->common->loadProvinceDistrict();

								} else if( $section['widget'] == "solution_home" ){

									$this->data['cate_list'] = $this->common->widget_solution_home_cate_detail();

								} else if( $section['widget'] == "smepack_cate_menu" || $section['widget'] == 'smepack_highlight' ){

									$this->data['cate_list'] = $this->common->widget_smepack_cate_menu();

                                    if ( $section['widget'] == "smepack_highlight" ) {
                                        $this->data['cate_list_all'] = $this->common->widget_smepack_cate_menu( 'all' );
                                        //echo '<pre>';print_r( $this->data['cate_list_all'] );echo '</pre>'; exit;
                                        //get_multiple_cate_array($child_code, $cate_list_all);
                                        $this->data['cate_detail']['mobile'] = $this->common->widget_smepack_highlight( 1 ); // cate_code, '', limit
                                        $this->data['cate_detail']['internet'] = $this->common->widget_smepack_highlight( 2 );
                                        $this->data['cate_detail']['digitaltv'] = $this->common->widget_smepack_highlight( 3 );
                                        $this->data['cate_detail']['fixedline'] = $this->common->widget_smepack_highlight( 4 );
                                        $this->data['cate_detail']['valueset'] = $this->common->widget_smepack_highlight( 5 );
    
                                        //echo '<pre>';print_r( $this->data['cate_detail'] );echo '</pre>'; exit;
                                    }

								} else if ( $section['widget'] == "smepack_cate" ) {
									$this->data['cate_detail']['mobile'] = $this->common->widget_smepack_cate_detail( 1 ); // cate_code, '', limit
                                    $this->data['cate_detail']['internet'] = $this->common->widget_smepack_cate_detail( 2 );
                                    $this->data['cate_detail']['digitaltv'] = $this->common->widget_smepack_cate_detail( 3 );
                                    $this->data['cate_detail']['fixedline'] = $this->common->widget_smepack_cate_detail( 4 );
                                    $this->data['cate_detail']['valueset'] = $this->common->widget_smepack_cate_detail( 5 );
								} else if ( $section['widget'] == "smepack_cate_mobile" ) {
									$code = 1;
									$this->data['cate_detail']['mobile'] = $this->common->widget_smepack_cate_detail( $code );
								} else if ( $section['widget'] == "smepack_cate_internet" ) {
									$code = 2;
									$this->data['cate_detail']['internet'] = $this->common->widget_smepack_cate_detail( $code );
								} else if ( $section['widget'] == "smepack_cate_digitaltv" ) {
									$code = 3;
									$this->data['cate_detail']['digitaltv'] = $this->common->widget_smepack_cate_detail( $code );
								} else if ( $section['widget'] == "smepack_cate_fixedline" ) {
									$code = 4;
									$this->data['cate_detail']['fixedline'] = $this->common->widget_smepack_cate_detail( $code );
								} else if ( $section['widget'] == "smepack_cate_valueset" ) {
									$code = 5;
									$this->data['cate_detail']['valueset'] = $this->common->widget_smepack_cate_detail( $code );
								} else if( $section['widget'] == "solution_slide" ){

                                    $this->data['solution_slide_list'] = $this->common->general_list( 'solution_category' );

								} else if( $section['widget'] == "solution_auto" ){

                                    $this->data['solution_auto_list'] = $this->common->general_list( 'solution_category' );

								} else if( $section['widget'] == "experience_slide" ){

                                    $this->data['experience_slide_list'] = $this->common->general_list( 'experience_category' );
	
								} else if( $section['widget'] == "experience_logo" ){

                                    $this->data['solution_business_type_list'] = $this->common->general_list( 'solution_business_type' );
	
								} else if( $section['widget'] == "experience_auto" ){

                                    $this->data['experience_list'] = $this->common->general_list( 'experience_category' );
	
								}
								
							} 
						}
					}
				}
			}
			$this->page_construct( 'page_template' , $this->data );
		}else{
			$this->page_construct_nohead( 'errors/error_404' );
		}
		
	}

	public function listview( $page_slug = "", $cate_slug = "" , $content_slug = "" ){
		
		$this->data['css'] = "";
		$this->data['js'] = "";
		$this->data['tag'] = 'content_detail';
		$this->data['body_class'] = "";
		$this->data['menu_check'] = 'content_detail';
        $this->data['sub_menu_check'] = $content_slug;
		$this->data['page'] = 'content_detail';
		$this->data['page_active'] = 'content_detail';
		$this->data['page_slug'] = $page_slug;
		$this->data['page_seo'] = $this->common->loadSeo($content_slug);
		
		//$this->data['banners'] = array(); //$this->common->loadBanner();

		//echo 'cate : '.$cate_slug;
		//echo 'contetnt : '.$content_slug;
		
        $this->data['cate_detail'] = $this->common->general_list( 'section_cate', 'slug', $cate_slug, true );

		$section_detail_ar = json_decode( $this->data['cate_detail']['sec_code'] );

		$section_template = '';
		if ( sizeof($section_detail_ar) > 0 ) {
			$section_detail = $this->common->loadSectionsDetail( $section_detail_ar[0] );
			if ( sizeof($section_detail) > 0 ) {
				$section_template = $section_detail['section_template'];
			} else {
				redirect('errors/404_override');
			}
		} else {
			redirect('errors/404_override');
		}	

		if ( $section_template == 'solution' ) {
			$this->data['content_detail'] = $this->common->content_solution_detail($content_slug);
		} else {
			$this->data['content_detail'] = $this->common->content_detail($content_slug);
		}
		
		$this->data['bread_type'] = '';
		

		if( sizeof( $this->data['cate_detail'] ) == 0 || sizeof( $this->data['content_detail'] ) == 0 ){
			redirect('errors/404_override');
		} else {
			$this->data['page_detail'] = array();
			/*
			$this->data['page_detail'] = $this->common->page_detail($page_slug);
			if( sizeof( $this->data['page_detail'] ) == 0 ){
				$this->db->where('slug', $page_slug);
				$this->db->where('lang', DEFAULT_LANG );
				$q = $this->db->get('menu');
				$result_menu = $q->result_array();
				if( sizeof( $result_menu ) > 0 ){
					$this->data['bread_type'] = 'slug';
					$this->data['page_detail']['page_name'] = $result_menu[0]['name'];
				}
			} */
			//$this->data['page_detail']['page_name'] = $this->data['content_detail']['title'];
			if ( $section_template == 'solution' ) {
				if ( isset($this->data['content_detail']['cate_code']) && $this->data['content_detail']['cate_code'] != '' ) {
					$cate_detail_code = json_decode($this->data['content_detail']['cate_code']);
				} else {
					$cate_detail_code = array();
				}

				$this->data['relate_lists'] = $this->common->loadRelateListSolution( $cate_detail_code,$this->data['content_detail']['code'] );
				$this->page_construct( 'solution_listing_detail' , $this->data );
			} else {
				$this->data['relate_lists'] = $this->common->loadRelateList( $this->data['cate_detail']['code'],$this->data['content_detail']['code'] );
				$this->page_construct( 'listing_detail' , $this->data );
			}
			
		}
	 }

	 public function search(){
		$this->data['css'] = "";
		$this->data['js'] = "";
		$this->data['tag'] = '';
		$this->data['body_class'] = "";
		$this->data['menu_check'] = 'search';
        $this->data['sub_menu_check'] = 'search';
		$this->data['page'] = 'search';
		$this->data['page_active'] = 'search';
		$this->data['page_slug'] = 'news-and-event';
		$this->data['page_detail']['page_name'] = 'search';
		
		$tag = isset( $_GET['tag'] ) ? $this->input->get('tag') : '';

		if( $tag != '' ){
			$this->data['content_detail'] = $this->common->search_tag( $tag );
			
			//echo '<pre>';print_r( $this->data['content_detail'] );echo '</pre>';
			$this->page_construct( 'tag_listing' , $this->data );
		}else{
			redirect('errors/404_override');
		}
	 }

	public function solution_cate( $cate_slug = "" ) {
		$this->data['css'] = "";
		$this->data['js'] = "";
		$this->data['tag'] = '';
		$this->data['body_class'] = "";
		$this->data['menu_check'] = '';
        $this->data['sub_menu_check'] = '';
		$this->data['page'] = '';
		$this->data['page_active'] = '';
		$this->data['page_slug'] = '';
		$this->data['page_seo'] = array();
		$this->data['bread_type'] = '';
		$this->data['page_detail'] = array();
		$this->data['page_detail']['page_name'] = 'Category';
		$this->data['content_detail'] = array();

        $this->data['cate_detail'] = $this->common->general_list( 'solution_category', 'slug', $cate_slug, true );
		if ( sizeof($this->data['cate_detail']) > 0 ) {
            $this->data['solution_content_detail'] = $this->common->general_list( 'solution', 'cate_code', $this->data['cate_detail']['code'], '', 'like' ); // table, field, value, is_single, type

            $this->data['experience_content_detail'] = $this->common->experience_list_solution( $this->data['cate_detail']['code'] );

            $this->data['solution_cate_more'] = $this->common->solution_cate_list( $this->data['cate_detail']['code'] );
           // echo 'mixer';
			//print_r( $this->data['experience_content_detail'] ); exit;

			$this->page_construct( 'solution_cate_listing' , $this->data );
		} else {
			redirect('errors/404_override');
		}	
	}

	public function solution_detail( $cate_slug = "", $slug = "" ) {
		$this->data['css'] = "";
		$this->data['js'] = "";
		$this->data['tag'] = '';
		$this->data['body_class'] = "";
		$this->data['menu_check'] = '';
        $this->data['sub_menu_check'] = '';
		$this->data['page'] = '';
		$this->data['page_active'] = '';
		$this->data['page_slug'] = '';
		$this->data['page_seo'] = array();
		$this->data['bread_type'] = '';
		$this->data['page_detail'] = array();
		$this->data['page_detail']['page_name'] = 'Category';
		$this->data['content_detail'] = array();
       
        $this->data['cate_detail'] = $this->common->general_list( 'solution_category', 'slug', $cate_slug, true );
		if ( sizeof($this->data['cate_detail']) > 0 ) {

            //$this->data['content_detail'] = $this->common->solution_list( $slug, 'slug' );
            $this->data['content_detail'] = $this->common->general_list( 'solution', 'slug', $slug, true ); // table, field, value, is_single, type


            if ( $this->data['content_detail']['business_type_code'] != '' ) {

                $this->data['solution_business_type'] = get_multiple_cate_array( json_decode($this->data['content_detail']['business_type_code'],true),$this->common->general_list( 'solution_business_type' ) );

            } else {
                $this->data['solution_business_type'] = array();
            }

            if ( $this->data['content_detail']['benefit'] != '' ) {
                $this->data['solution_benefit'] = json_decode( $this->data['content_detail']['benefit'], true );
             }else {
                $this->data['solution_benefit'] = array();
            }

			//print_r( $this->data['solution_business_type'] );
			$this->page_construct( 'solution_listing_detail' , $this->data );
		} else {
			redirect('errors/404_override');
		}	
	}

    public function experience_cate( $cate_slug = "" ) {
		$this->data['css'] = "";
		$this->data['js'] = "";
		$this->data['tag'] = '';
		$this->data['body_class'] = "";
		$this->data['menu_check'] = '';
        $this->data['sub_menu_check'] = '';
		$this->data['page'] = '';
		$this->data['page_active'] = '';
		$this->data['page_slug'] = '';
		$this->data['page_seo'] = array();
		$this->data['bread_type'] = '';
		$this->data['page_detail'] = array();
		$this->data['page_detail']['page_name'] = 'Category';
		$this->data['content_detail'] = array();

		$this->data['cate_detail'] = $this->common->experience_cate_detail($cate_slug);
		if ( sizeof($this->data['cate_detail']) > 0 ) {
			$this->data['experience_content_detail'] = $this->common->experience_list_detail( $this->data['cate_detail']['code'] );

            $this->data['experience_slide_list'] = $this->common->general_list( 'experience_category' );

			$this->page_construct( 'experience_cate_listing' , $this->data );
		} else {
			redirect('errors/404_override');
		}	
	}

	public function experience_detail( $cate_slug = "", $slug = "" ) {
		$this->data['css'] = "";
		$this->data['js'] = "";
		$this->data['tag'] = '';
		$this->data['body_class'] = "";
		$this->data['menu_check'] = '';
        $this->data['sub_menu_check'] = '';
		$this->data['page'] = '';
		$this->data['page_active'] = '';
		$this->data['page_slug'] = '';
		$this->data['page_seo'] = array();
		$this->data['bread_type'] = '';
		$this->data['page_detail'] = array();
		$this->data['page_detail']['page_name'] = 'Category';
		$this->data['content_detail'] = array();
       
		$this->data['cate_detail'] = $this->common->experience_cate_detail($cate_slug);
		if ( sizeof($this->data['cate_detail']) > 0 ) {

			$this->data['content_detail'] = $this->common->experience_detail( $slug );

            if ( $this->data['content_detail']['solution_cate_code'] != '' ) {
                $this->data['solution_cate_list'] = get_multiple_cate_array( json_decode($this->data['content_detail']['solution_cate_code'],true),$this->common->solution_cate_list() );
            } else {
                $this->data['solution_cate_list'] = array();
            }

            if ( $this->data['content_detail']['solution_used_code'] != '' ) {
                $this->data['solution_use_list'] = get_multiple_cate_array( json_decode($this->data['content_detail']['solution_used_code'],true),$this->common->solution_cate_list() );
            } else {
                $this->data['solution_use_list'] = array();
            }

            // if ( $this->data['content_detail']['benefit'] != '' ) {
            //     $this->data['solution_benefit'] = json_decode( $this->data['content_detail']['benefit'], true );
            // }
            //echo '<pre>';print_r( $this->data['solution_cate_list'] );echo '</pre>';  exit;
			$this->page_construct( 'solution_cate_list' , $this->data );
		} else {
			redirect('errors/404_override');
		}	
	}

	public function smepack( $parent_slug = "", $child_slug = "") {

		$this->data['css'] = "";
		$this->data['js'] = "";
		$this->data['tag'] = '';
		$this->data['body_class'] = "";
		$this->data['menu_check'] = '';
        $this->data['sub_menu_check'] = '';
		$this->data['page'] = '';
		$this->data['page_active'] = '';
		$this->data['page_slug'] = '';
		$this->data['page_seo'] = $this->common->loadSeo($parent_slug);
		$this->data['bread_type'] = '';
		$this->data['page_detail'] = array();
		$this->data['page_detail']['page_name'] = 'Category';
		$this->data['content_detail'] = array();

		$this->data['active_cate_parent'] = $parent_slug;
		$this->data['active_cate_child'] = $child_slug;

		//$this->data['cate_detail'] = $this->common->cate_detail($cate_slug);
		
		if ( $parent_slug != '' ) {

            $this->data['cate_list'] = $this->common->widget_smepack_cate_menu();
            
			$this->data['special_offer_list'] = $this->common->get_special_offer( $parent_slug );

			$this->data['cate_detail'] = $this->common->smepack_cate_detail( $parent_slug );

			//echo '<pre>';print_r( $this->data['cate_detail'] );echo '</pre>';

			$this->page_construct( 'smepack_detail' , $this->data );
		} else {
			redirect('errors/404_override');
		}	
	}

	public function landing_detail( $slug = "" ) {

		$this->data['css'] = "";
		$this->data['js'] = "";
		$this->data['tag'] = '';
		$this->data['body_class'] = "";
		$this->data['menu_check'] = '';
        $this->data['sub_menu_check'] = '';
		$this->data['page'] = '';
		$this->data['page_active'] = '';
		$this->data['page_slug'] = '';
		$this->data['page_seo'] = array();
		$this->data['bread_type'] = '';
		$this->data['page_detail'] = array();
		$this->data['page_detail']['page_name'] = 'Landing';
		$this->data['content_detail'] = array();

		$this->data['content_detail'] = $this->common->get_landing_detail( $slug );

		if ( sizeof($this->data['content_detail']) > 0 ) {
			$this->page_construct( 'landing_detail' , $this->data );
		} else {
			redirect('errors/404_override');
		}	
	}
	
}