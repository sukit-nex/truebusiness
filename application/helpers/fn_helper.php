
<?php
//======= FRONTEND ======

function ar_decode( $json ){ 
  $ar_set = $json == "" ? array('','','','') : json_decode( $json, true );
  return $ar_set;
}
function  ar_decodeFront($json, $type){
  $style = "";
  $ar_set = $json == "" ? array('','','','') : json_decode( $json, true );
  $ar_position = array('top', 'right' , 'bottom' , 'left');
  foreach ($ar_set as $key => $set) {
    if( $set != "" ){
      if (is_numeric($set)) {
        $style .= $type."-".$ar_position[$key].": ".$set."px;";
      } else {
        $style .= $type."-".$ar_position[$key].": ".$set.";";
      }
    }
  }
    return $style;
}
function is_fullwidth( $value ){
  return $value == "fullwidth" ? ' container-fluid' : ' container-1600';
}
function has_topic( $title , $sub_title ){
  if( $title != "" || $sub_title != "" ) {
    return true;
  }else{
    return false;
  }
}
function getBackgroundSettingClass($ver , $hoz, $bg_p, $bg_fix , $bg_size ){
  $ar_hoz = array('' => '','left' => 'bg-px-l','center' => 'bg-px-c','right' => 'bg-px-r');
  $ar_ver = array('' => '','top' => 'bg-py-t','middle' => 'bg-py-m','bottom' => 'bg-py-b');
  $ar_bg_p = array('' => '','no_repeat' => 'bg-rp-no','repeat_all' => 'bg-rp-all','repeat_x' => 'bg-rp-x','repeat_y' => 'bg-rp-y');
  $ar_bg_fix = array('' => '','static' => 'bg-fx-st','fixed' => 'bg-fx-fx');
  $ar_bg_size = array('' => '','cover' => 'bg-sz-cv','contain' => 'bg-sz-ct','auto' => 'bg-sz-at');
  return $ar_ver[ $ver ].' '.$ar_hoz[ $hoz ].' '.$ar_bg_p[ $bg_p ].' '.$ar_bg_fix[ $bg_fix ].' '.$ar_bg_size[ $bg_size ];
}
function getContentPositionClass( $ver , $hoz ){
  $ar_hoz = array('left' => 'text-md-left','center' => 'text-md-center','right' => 'text-md-right');
  $ar_ver = array('top' => 'align-items-md-flex-start','middle' => 'align-items-md-center','bottom' => 'align-items-md-end');
  return $ar_ver[ $ver ].' '.$ar_hoz[ $hoz ];
}

function getContentPositionClassMobile( $ver , $hoz ){
  $ar_hoz = array('left' => 'text-left','center' => 'text-center','right' => 'text-right');
  $ar_ver = array('top' => 'align-items-flex-start','middle' => 'align-items-center','bottom' => 'align-items-end');
  return $ar_ver[ $ver ].' '.$ar_hoz[ $hoz ];
}

function linkType( $link_type, $link_out, $cate_slug,$list_slug, $page_slug = ""){
  $ar_link_type = array('no' => 'javascript:void(0)','blank' => $link_out ,'auto' =>  base_url().'th/content/'.$cate_slug .'/'.$list_slug);
  return $ar_link_type[ $link_type ];
}

function getSecPositionHoz($hoz ){
  $ar_hoz = array('' => '', 'left' => 'text-left','center' => 'text-center','right' => 'text-right');
  return $ar_hoz[ $hoz ];
}
function getSecPositionVer($ver){
  $ar_ver = array('' => '','top' => 'text-top','middle' => 'text-middle','bottom' => 'text-bottom');
  return $ar_ver[ $ver ];
}
function designTemplate( $d_template ){
  $ar_d_template = array( 'vertical' => 'ver','horizontal' => 'hoz', 'text_in_card' => 'incard' );
  return $ar_d_template[$d_template];
}

function contentLayout( $con_lay ){
  $ar_con_lay = array('vertical' => 'ver','hoz_left' => 'hoz-left','hoz_right' => 'hoz-right','content_first' => 'ver');
  return $ar_con_lay[$con_lay];
}

function getSlug( $code = 0, $datas = array(), $seos = array() ){
  $seo_id = 0;
  $slug = "";
  foreach( $datas as $data ){
    if( $data['code'] == $code ){
      // $seo_id = $data['seo_id'];
      $slug = $data['page_slug'];
    }
  }
  // foreach( $seos as $seo ){
  //   if( $seo['id'] == $seo_id ){
  //     $slug = $seo['slug'];
  //   }
  // }
  return $slug;
}

function customClass($class){
  if( $class != ""){
    $ar_tag = explode( ",", $class );
    $ar_tag = implode( " ", $ar_tag );
  }
  return $ar_tag;
}


//======== CMS Function =====================
function removeHTML( $str = "" ){
  return strip_tags($str);
}
function changeDateStyle( $data = "" ){
  if( $data != ""){
    $ar = explode( "-" , $data);
    if( sizeof( $ar ) == 3 ){
      $ar[0] = intval( $ar[0] );
      $data = $ar[2].'.'.$ar[1].'.'.substr($ar[0],2,3);
    }else{
      $data = "วันที่ไม่ตรงรูปแบบ";
    }
  }
  return $data;
}
function loadCateName( $cates , $cate_code = "" ){
  $str_cate = "";
  foreach( $cates as $cate ){
    if( $cate_code == $cate['code']){
      if( $str_cate == "" ){
        $str_cate = $cate['title'];
      }
    }
  }
  return $str_cate;
}
function getEventDate( $date, $lang ){
  $str_date = "";
  if( $date != ""){
    $ar_month_en = array("","January","Febuary","March","April","May","June","July","August","September","October","November","December");
    $ar_month_th = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤษจิกายน","ธันวาคม");
    $ardate = explode("-", $date);
    //echo $ardate[0].' '.$ar_month_th[intval($ardate[1])].' '.$ardate[2];
    if( $lang == "thailand"){
      $str_date = intval( $ardate[2] ).' '.$ar_month_th[intval($ardate[1])].' '.( intval( $ardate[0] ) );
    }else if( $lang == "english"){
      $str_date = intval( $ardate[2] ).' '.$ar_month_en[intval($ardate[1])].' '.( intval( $ardate[0] )-543 );
    }
  }

  return $str_date;
}
function getCalendarDate( $date ){
  $str_date = "";
  if( $date != ""){
    $ar_month_en = array("","Jan","Feb","Marh","April","May","June","July","Aug","Sepr","Oct","Nov","Dec");
    $ardate = explode("-", $date);
    $str_date = $ar_month_en[intval($ardate[1])].' '.$ardate[2];
  }

  return $str_date;
}


function getCalendarDate_listing( $date , $lang  ){
  $str_date = "";
  if( $date != ""){
    $ar_month_en = array("","Jan","Feb","Marh","April","May","June","July","Aug","Sepr","Oct","Nov","Dec");
    $ar_month_th = array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $ardate = explode("-", $date);
    $str_date = $ar_month_en[intval($ardate[1])].' '.$ardate[2];
    if( $lang == "thailand"){
      $str_date = intval( $ardate[2] ).' '.$ar_month_th[intval($ardate[1])].' '.( intval( $ardate[0] ) );
    }else if( $lang == "english"){
      $str_date = intval( $ardate[2] ).' '.$ar_month_en[intval($ardate[1])].' '.( intval( $ardate[0] )-543 );
    }
  }

  return $str_date;
}


function getRealIpAddr(){
 if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
  // Check IP from internet.
  $ip = $_SERVER['HTTP_CLIENT_IP'];
 } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
  // Check IP is passed from proxy.
  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
 } else {
  // Get IP address from remote address.
  $ip = $_SERVER['REMOTE_ADDR'];
 }
 return $ip;
}

function check_permission( $role , $location ){
  $permission = array();
  $permission['SA'] = array('dashboard','approve_content','create_content','edit_content','delete_content','global_seo','footer','permission',
                            'menu','user','coverage','immersive','xr','vr','ar','pages','edit_pages','delete_page','page_seo','create_row','edit_row',
                            'delete_row','row_setting','create_column','edit_column','delete_column','section','duplicate_section','create_section',
                            'edit_section','delete_section','section_gen_height','section_type','section_display','section_advance',
                            'create_category','edit_category','delete_category','category','create_banner','edit_banner','delete_banner','banner',
                            'create_video','edit_video','delete_video','total_page');
                            
  $permission['A'] = array( 'dashboard','approve_content','create_content','edit_content','delete_content','global_seo','page_seo','permission',
                            'user','coverage','immersive','xr','vr','ar','edit_row','row_setting','edit_column','section','pages',
                            'edit_section','create_category','edit_category','delete_category','category','create_banner','banner',
                            'edit_banner','delete_banner','create_video','edit_video','delete_video','total_page');
  
  $permission['AL'] = array( 'dashboard','edit_row','row_setting','edit_column','section','pages',
                            'edit_section','create_category','edit_category','delete_category','category','create_banner','banner',
                            'edit_banner','delete_banner','create_video','edit_video','delete_video','create_content','edit_content','delete_content','global_seo');

  $permission['AC'] = array( 'dashboard','edit_row','row_setting','edit_column','section','immersive','pages','xr','vr','ar',
                            'edit_section','create_category','edit_category','delete_category','category','create_banner','banner',
                            'edit_banner','delete_banner','create_video','edit_video','delete_video','create_content','edit_content','delete_content','global_seo');

  $permission['AV'] = array( 'dashboard','edit_row','row_setting','edit_column','section','pages',
                            'edit_section','create_category','edit_category','delete_category','category','create_banner','banner',
                            'edit_banner','delete_banner','create_video','edit_video','delete_video','create_content','edit_content','delete_content','global_seo');
                    
  $permission['AX'] = array( 'dashboard','edit_row','row_setting','edit_column','section','pages',
                            'edit_section','create_category','edit_category','delete_category','category','create_banner','banner',
                            'edit_banner','delete_banner','create_video','edit_video','delete_video','create_content','edit_content','delete_content','global_seo');

  $permission['AR'] = array( 'dashboard','edit_row','row_setting','edit_column','section','pages','coverage',
                            'edit_section','create_category','edit_category','delete_category','category','create_banner','banner',
                            'edit_banner','delete_banner','create_video','edit_video','delete_video','create_content','edit_content','delete_content','global_seo');
                
  $permission['AK'] = array( 'dashboard','edit_row','row_setting','edit_column','section','pages',
                            'edit_section','create_category','edit_category','delete_category','category','create_banner','banner',
                            'edit_banner','delete_banner','create_video','edit_video','delete_video','create_content','edit_content','delete_content','global_seo');

  $permission['WM'] = array( 'dashboard','create_content','edit_content','category','delete_content');

  //$permission['W'] = array( 'dashboard','create_content','edit_content','category');
  $permission['WC'] = array( 'dashboard','create_content','edit_content','category');
  //$permission['WB'] = array( 'dashboard','create_content','edit_content','category');

  if( in_array( $location , $permission[ $role ] ) ){
    return true;
  }else{
    return false;
  }
}

function hasPermission( $role_code , $cate_data ){ // for check category permission
    $cate_permission = $cate_data['cate_permission'] == '' ? array() : explode(",",$cate_data['cate_permission']);
    if( in_array( $role_code, $cate_permission ) ){
      return true;
    }else{
      return false;
    }
}
function hasPagePermission( $role_code , $page_data ){ // for check page permission
    $page_permission = $page_data['page_permission'] == '' ? array() : explode(",",$page_data['page_permission']);
    if( in_array( $role_code, $page_permission ) ){
      return true;
    }else{
      return false;
    }
}
function getEditorRole(){
  return array(
          array('role_text' => 'Editor', 'role_code' => 'WC')
        );
}
function getApproveRole(){
  return array(
          array('role_text' => 'Editor', 'role_code' => 'SA'),
          array('role_text' => 'Editor', 'role_code' => 'A'),
          array('role_text' => 'Editor', 'role_code' => 'WM')
        );
}
function getRole( $role = "" ){
  $all_role = array(
          array('role_text' => 'Super Admin', 'role_code' => 'SA'),
          array('role_text' => 'Admin Manager', 'role_code' => 'A'),
          array('role_text' => 'Admin 5G Life', 'role_code' => 'AL'),
          array('role_text' => 'Admin 5G Consumer', 'role_code' => 'AC'),
          array('role_text' => 'Admin 5G Vertical', 'role_code' => 'AV'),
          array('role_text' => 'Admin XPhenomenon', 'role_code' => 'AX'),
          array('role_text' => 'Admin Coverage', 'role_code' => 'AR'),
          array('role_text' => 'Admin Knowledge', 'role_code' => 'AK'),
          array('role_text' => 'Editor Manager', 'role_code' => 'WM'),
          array('role_text' => 'Editor', 'role_code' => 'WC')
        );
  if( $role == ""){
    return $all_role;
  }else{
    $role_text = "";
    foreach( $all_role as $is_role ){
      if( $is_role['role_code'] == $role ){
        $role_text = $is_role['role_text'];
      }
    }
    return $role_text;
  }
}
function isEditor( $role_code = '' ){
  $check = false;
  if( $role_code != '' ){
    $ar_role = getEditorRole();
    foreach( $ar_role as $role ){
      if( $role_code == $role['role_code']){
        $check = true;
      }
    }
  }

  return $check;
}
function isApprover( $role_code = '' ){
  $check = false;
  if( $role_code != '' ){
    $ar_role = getApproveRole();
    foreach( $ar_role as $role ){
      if( $role_code == $role['role_code']){
        $check = true;
      }
    }
  }

  return $check;
}
function getApproveStatusTxt( $status = '' ){
  $ar_status = array('รอส่งเพื่อขออนุมัติ','รอพิจารณา','ได้รับการอนุมัติ','ไม่ได้รับการอนุมัติ');
  if( $status != '' ){
    if( isset( $ar_status[$status]) ){
      return $ar_status[ $status ];
    }else{
      return 'ไม่พบค่าสถานะที่ระบุ';
    }
  }else{
    return 'ไม่ได้รับค่าสถานะ';
  }
}

function get_cat_sp_txt( $cat = '' ){
  $ar_status = array(
                    'get_free'=>'รับฟรี',
                    'top_up'=>'ซื้อเพิ่ม',
                    'discounts_privileges'=>'ส่วนลด สิทธิพิเศษ'
                  );
  if( $cat != '' ){
    if( isset( $ar_status[$cat]) ){
      return $ar_status[ $cat ];
    }else{
      return 'ไม่พบค่าสถานะที่ระบุ';
    }
  }else{
    return 'ไม่ได้รับค่าสถานะ';
  }
}

function sme_feature_list ( $feature = '' ){
  $ar_status = array(
                    'general'=>'General',
                    'mobile_bundle'=>'รวมโทรศัพท์มือถือ + เน็ต',
                    'mobile'=>'โทร',
                    'mobile_internet'=>'อินเทอร์เน็ตมือถือ',
                    'wifi'=>'WiFi',
                    'notebook'=>'Notebook',
                    'internet'=>'อินเทอร์เน็ต',
                    'smart_wifi'=>'Smart WiFi',
                    'true_visions'=>'ทรูวิชั่นส์',
                    'fixedline'=>'โทรศัพท์สำนักงาน',
                    'top_up'=>'TOP UP',
                    'get_free'=>'GET FREE',
                    'discounts_privileges'=>'Discounts, privileges'
                  );
    if ( $feature == '' ) {
        return $ar_status;
    } else {
        if( isset( $ar_status[$feature]) ){
            return $ar_status[ $feature ];
        } else {
            return 'ไม่พบค่าสถานะที่ระบุ';
        }
    }
    
}

function sme_accordion_header ( $inc_data = array(), $row = array()) {
    $feature_name = $inc_data['feature_name'];
    $pkg_number = $inc_data['pkg_number'];
    $langs = $inc_data['langs'];
    $detail = $inc_data['detail'];
	$collapse_show = $inc_data['collapse_show'];

	if ( $collapse_show == 'show' ) {
		// original
		$data_toggle = '';
	} else {
		$data_toggle = 'data-toggle="toggle"';
	}
?>
<div class="card list-group-item feature-<?php echo $feature_name; ?> card-item-<?php echo $pkg_number; ?>" data-pkg="<?php echo $pkg_number; ?>">
    <input type="hidden" name="package[<?php echo $pkg_number; ?>][feature]" value="<?php echo $feature_name; ?>">
    <div class="card-header package-head nexu-tools-list drag-cusor ui-sortable-handle d-flex align-items-center" role="tab" id="heading-<?php echo $pkg_number; ?>">
        <div class="accordion-title">
            <h2><?php echo sme_feature_list( $feature_name ); ?></h2>
        </div>

        <div class="acc-header-btn">
            <small class="mb-0 mr-2 d-flex align-items-center" style="font-size:12px;">Status</small>
            <div class="button-switch">
                <input type="checkbox" name="package[<?php echo $pkg_number; ?>][onoff]" <?php echo $data_toggle; ?>  data-onstyle="info" data-on="Publish" data-off="Unpublish"
                    data-offstyle="outline-secondary" data-size="xs" <?php echo ($row['onoff'] == '1')?'checked':''; ?> value="1">
            </div>
            <div class="lineW"></div>
            <a class="btn-delete" href="javascript:void(0)" onclick="smepack_deleteItem( $(this) );" data-toggle="modal">Delete</a> 
            <a data-toggle="collapse" href="#collapse-<?php echo $pkg_number; ?>" aria-expanded="true" class="acc-toggle" aria-controls="collapse-<?php echo $pkg_number; ?>">Show detail<img src="/assets/admin/images/element/section/nx-close.png" alt=""></a>
        </div>
    </div>

    <div id="collapse-<?php echo $pkg_number; ?>" class="collapse <?php echo $collapse_show; ?> acc-body" role="tabpanel" aria-labelledby="heading-<?php echo $pkg_number; ?>">

<?php
}

function sme_header_box ( $inc_data = array(), $row = array()) {
    $feature_name = $inc_data['feature_name'];
    $pkg_number = $inc_data['pkg_number'];
    $langs = $inc_data['langs'];
    $detail = $inc_data['detail'];
?>

<div class="card-title">Header box</div>
<div class="box-form-inner">
    <div class="form-group ">
        <div class="row d-block d-md-flex align-items-center">
            <div class="col col-12 col-md-3"><label>Message</label></div>
            <div class="col col-12 col-md-9">

                <div class="tab-content tab-content-lang">
                    <?php foreach( $langs as $lang ){ ?>
                        <?php
                            
                            if( isset( $detail[ $lang['text'] ] ) ) {
                                $header_message = (isset($detail[ $lang['text'] ]['header_message']) )? $detail[ $lang['text'] ]['header_message']:'';
                            } else {
                                $header_message = '';
                            }

                            //print_r($detail);
                        ?>
                        <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
                            <div class="form-group mb-3">
                                <input type="text" name="package[<?php echo $pkg_number; ?>][header_message][]" class="form-control" value="<?php echo $header_message; ?>">
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>
</div>

<?php
}

function sme_link_button ( $inc_data = array(), $row = array()) {
    $feature_name = $inc_data['feature_name'];
    $tinymce = $inc_data['tinymce'];
    $dropify = $inc_data['dropify'];
    $fields = $inc_data['fields'];
    $pkg_number = $inc_data['pkg_number'];
    $langs = $inc_data['langs'];
    $pagesList = $inc_data['pagesList'];
    $detail = $inc_data['detail'];

    //print_r( $detail );
?>

<div class="card-title">Link button</div>
<div class="box-form-inner">
    
    <div class="form-group">
        <div class="row d-block d-md-flex align-items-center ">
            <div class="col col-12 col-md-3"><label>Link Type</label></div>
            <div class="col col-12 col-md-9">
                <div class="d-md-flex align-items-center nav smepack-linktype" role="tablist">
                    <div class="form-check form-check-dark mr-3 mb-0">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input btn_tab" name="package[<?php echo $pkg_number; ?>][btn_type]" value="no_link" data-target="#no_link_content_<?php echo $pkg_number; ?>" <?php echo ($row['btn_type'] == 'no_link')?'checked':''; ?>> No Link </label>
                    </div>        
                    <div class="form-check form-check-dark mr-3 mb-0">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input btn_tab" name="package[<?php echo $pkg_number; ?>][btn_type]" value="internal_link"  data-target="#internal_link_content_<?php echo $pkg_number; ?>" <?php echo ($row['btn_type'] == 'internal_link')?'checked':''; ?> > Internal Link </label>
                    </div>
                    <div class="form-check form-check-dark mr-3 mb-0">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input btn_tab" name="package[<?php echo $pkg_number; ?>][btn_type]" value="external_link" data-target="#external_link_content_<?php echo $pkg_number; ?>" <?php echo ($row['btn_type'] == 'external_link')?'checked':''; ?>> External Link </label>
                    </div>
                    <div class="form-check form-check-dark mr-3 mb-0">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input btn_tab" name="package[<?php echo $pkg_number; ?>][btn_type]" value="pdf" data-target="#pdf_content_<?php echo $pkg_number; ?>" <?php echo ($row['btn_type'] == 'pdf')?'checked':''; ?>> PDF </label>
                    </div>
                    <div class="form-check form-check-dark mb-0">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input btn_tab" name="package[<?php echo $pkg_number; ?>][btn_type]" value="content" data-target="#content_content_<?php echo $pkg_number; ?>" <?php echo ($row['btn_type'] == 'content')?'checked':''; ?>> Content </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="form-group">

        <div class="showall-not-nolink" style="<?php echo ($row['btn_type'] == 'no_link')?'display:"none"':'display:"block"'; ?>">
            <div class="row d-block d-md-flex align-items-center mb-3">
                <div class="col col-12 col-md-3"><label>Text button</label></div>
                <div class="col col-12 col-md-9">
                    <div class="tab-content tab-content-lang">
                        <?php foreach( $langs as $lang ){ ?>
                            <?php
                                if( isset( $detail[ $lang['text'] ] ) ) {
                                    $btn_name = $detail[ $lang['text'] ]['btn_name'];
                                } else {
                                    $btn_name = '';
                                }
                            ?>
                            <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
                                <input type="text" name="package[<?php echo $pkg_number; ?>][btn_name][]" placeholder="ดูรายละเอียดเพิ่มเติม" class="form-control" value="<?php echo $btn_name; ?>">
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content border-0 p-0">
            <div class="tab-pane <?php echo ($row['btn_type'] == 'no_link')?'active':''; ?>" id="no_link_content_<?php echo $pkg_number; ?>" >

            </div>

            <div class="tab-pane <?php echo ($row['btn_type'] == 'internal_link')?'active':''; ?>" id="internal_link_content_<?php echo $pkg_number; ?>" >
                <div class="row d-block d-md-flex align-items-center my-3">
                    <div class="col col-12 col-md-3"><label>Internal Link</label></div>
                    <div class="col col-12 col-md-9">
                        <select class="form-control" name="package[<?php echo $pkg_number; ?>][btn_internal_link]">
                            <option value="">Select the page</option>
                            <?php foreach( $pagesList as $page_menu ){ ?>
                                <option value="<?php echo $page_menu['code']; ?>" <?php echo ( $page_menu['code'] == $row['btn_internal_link'] )?'selected':''; ?>><?php  echo $page_menu['page_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="tab-pane <?php echo ($row['btn_type'] == 'external_link')?'active':''; ?>" id="external_link_content_<?php echo $pkg_number; ?>" >
                <div class="row d-block d-md-flex align-items-center my-3">
                    <div class="col col-12 col-md-3"><label>External Link</label></div>
                    <div class="col col-12 col-md-9">
                        <input type="text" name="package[<?php echo $pkg_number; ?>][btn_external_link]" class="form-control"  placeholder="" value="<?php echo $row['btn_external_link']; ?>">
                    </div>
                </div>
            </div>
            <div class="tab-pane <?php echo ($row['btn_type'] == 'pdf')?'active':''; ?>" id="pdf_content_<?php echo $pkg_number; ?>" >
                <div class="row d-block d-md-flex align-items-center my-3">
                    <div class="col col-12 col-md-3"><label>PDF</label></div>
                    <div class="col col-12 col-md-9">
                        <div class="tab-content tab-content-lang">
                            <?php foreach( $langs as $lang ){ ?>
                                <?php
                                    if( isset( $detail[ $lang['text'] ] ) ) {
                                        $btn_pdf_file = $detail[ $lang['text'] ]['btn_pdf_file'];
                                    } else {
                                        $btn_pdf_file = '';
                                    }
                                ?>
                                <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
                                    <input type="file" name="package[<?php echo $pkg_number; ?>][btn_pdf_file][]" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload PDF" value="<?php echo $btn_pdf_file; ?>">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn  btn-gradient-dark " type="button">Upload</button>
                                        </span>
                                        <input type="hidden" name="package[<?php echo $pkg_number; ?>][btn_pdf_file_txt][]" value="<?php echo $btn_pdf_file; ?>" >
                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                        
                    </div>
                </div>
            </div>
            <div class="tab-pane <?php echo ($row['btn_type'] == 'content')?'active':''; ?>" id="content_content_<?php echo $pkg_number; ?>" >
                <div class="row d-block d-md-flex align-items-center">
                    <div class="col col-12 col-md-3"><label>Content</label></div>
                    <div class="col col-12 col-md-9">
                        <div class="tab-content tab-content-lang">
                            <?php foreach( $langs as $lang ){ ?>
                                <?php
                                    if( isset( $detail[ $lang['text'] ] ) ) {
                                        $btn_content_popup = $detail[ $lang['text'] ]['btn_content_popup'];
                                    } else {
                                        $btn_content_popup = '';
                                    }
                                ?>
                                <div class="tab-pane fade show <?php echo $lang['text'] == DEFAULT_LANG ? 'active' : '';  ?> <?php echo 'tab-'.$lang['text']; ?>" role="tabpanel">
                                    <div class="form-group mb-3">
                                        <textarea name="package[<?php echo $pkg_number; ?>][btn_content_popup][]" class="form-control <?php echo $tinymce; ?> " rows="7" placeholder="" ><?php echo $btn_content_popup; ?></textarea>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php
}

function sme_display_content ( $inc_data = array(), $row = array()) {
    $feature_name = $inc_data['feature_name'];
    $dropify = $inc_data['dropify'];
    $fields = $inc_data['fields'];
    $pkg_number = $inc_data['pkg_number'];


    $notshow_image = array('notebook', 'top_up', 'get_free', 'discounts_privileges');
?>

<div class="card-title">Display Content</div>
<div class="box-form-inner">
	<div class="form-group">
		<div class="row d-block d-md-flex align-items-center">
			<div class="col col-12 col-md-3"><label>Toggle content</label></div>
			<div class="col-md-9 d-md-flex align-items-center">
				<div class="form-check form-check-dark mr-3">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][display_toggle]" value="0" <?php echo ($row['display_toggle'] == '0')?'checked':''; ?>>
						ปิด ข้อมูลย่อย
					</label>
				</div>
				<div class="form-check form-check-dark">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="package[<?php echo $pkg_number; ?>][display_toggle]" value="1" <?php echo ($row['display_toggle'] == '1')?'checked':''; ?> >
						เปิด ข้อมูลย่อย
					</label>
				</div>
			</div>
		</div>
	</div>
    <?php if ( !array_search($feature_name, $notshow_image) ) { ?>
	<div class="form-group">
		<div class="row d-block d-md-flex align-items-center">
			<div class="col col-12 col-md-3"><label>Icon / Image (50x50) </label></div>
			<div class="col col-12 col-md-9">	
				<input type="file" data-height="190" name="package[<?php echo $pkg_number; ?>][display_icon_image]"
				class="<?php echo $dropify; ?>" data-max-file-size="3000kb" data-content="Icon / Image (50px x 50px) " data-max-width="50" data-max-height="50"
				data-default-file="<?php echo $row['display_icon_image'] != "" ? UPLOAD_PATH.$row['display_icon_image'] : ''; ?>" />
				<input type="hidden" class="image_as_text" name="package[<?php echo $pkg_number; ?>][display_icon_image_txt]"
				value="<?php echo $row['display_icon_image']; ?>" />
			</div>
		</div>
	</div>
    <?php } ?>
</div>

<?php
}

function upload_path( $name = '' ) {
    $CI = & get_instance();  //get instance, access the CI superobject
    $session_usr_id = $CI->session->userdata('id');

    if ( $session_usr_id == '38' ) {
        $path = REAL_PATH.'uploads-test/';
        //echo 'mixer->'.$path.$name;
        if (!file_exists('uploads-test/'.$name)) {
          $path = UPLOAD_PATH;
        }
    } else {
        $path = UPLOAD_PATH;

        if (!file_exists('uploads/'.$name)) {
            $path = REAL_PATH.'uploads-test/';

            if (!file_exists('uploads-test/'.$name)) {
              $path = UPLOAD_PATH;
            }
        }
    }
    return $path.$name;
}

function editor_upload_path( $name = '' ) {
    $CI = & get_instance();  //get instance, access the CI superobject
    $session_usr_id = $CI->session->userdata('id');

    if ( $session_usr_id == '38' ) {
        $path = ROOT_DIR.'uploads-test/'.$name;
    } else {
        $path = EDITOR_UPLOAD_PATH.'/'.$name;
    }
    
    return $path;
}

function file_upload_path() {
    $CI = & get_instance();  //get instance, access the CI superobject
    $session_usr_id = $CI->session->userdata('id');

    if ( $session_usr_id == '38' ) {
        $path = ROOT_DIR.'uploads-test/files/';
    } else {
        $path = FILE_UPLOAD_PATH;
    }
    return $path;
}


function get_special_offer( $code = '', $datas = array() ){
    $seo_id = 0;
    $slug = "";
    $return_data = '';
    if ( $code != ''  ) {
        foreach( $datas as $data ){
            if( $data['code'] == $code ){
                $return_data = $data;
            }
        }
    }

  return $return_data;
}

function get_multiple_cate( $cate_lists = '', $datas = array() ){
  if ( $cate_lists != '' ) {
    $return_data = '';
    $cate_lists = json_decode($cate_lists, true);
    foreach ($cate_lists as $key => $code) {
        foreach( $datas as $data ){
            if( $data['code'] == $code ){
                if ( $return_data != '' ) {
                    $return_data .= ', ';
                }
                $return_data .= $data['title'];
            }
        }
    }

  } else {
    $return_data = '';
  }
//   $seo_id = 0;
//   $slug = "";
//   $return_data = '';
//   if ( $code != ''  ) {
//       foreach( $datas as $data ){
//           if( $data['code'] == $code ){
//               $return_data = $data;
//           }
//       }
//   }

return $return_data;
}

function get_multiple_cate_array( $cate_lists = array(), $datas = array() ){
   // echo 'mixer';
    //print_r($datas);
    if ( sizeof($cate_lists) > 0 ) {
        $return_data = array();
        //$cate_lists = json_decode($cate_lists, true);
        foreach ( $cate_lists as $key => $code) {
            foreach( $datas as $data ){
                if( $data['code'] == $code ){
                    $return_data[] = $data;
                }
            }
        }

    } else {
        $return_data = array();
    }

    return $return_data;
}
