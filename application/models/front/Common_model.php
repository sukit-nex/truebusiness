<?php
class Common_model extends CI_Model {
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }
    public function loadCoverage(){
        $return = array();
        $this->db->where('geojson_file !=', '');
        $this->db->where('geojson_status', 1);
        $this->db->order_by('id', 'desc');
        $result = $this->db->get('coverage_history');
        $return = $result->result_array();
        if( sizeof( $return ) > 0 ){
            return $return[0];
        }else{
            return $return;
        }

    }
    public function loadProvinceList(){
        $return = array();
        $this->db->select('DISTINCT( province ) ');
        $this->db->from('coverage_list');
        $this->db->order_by('province','asc');
        $result = $this->db->get();
        $return = $result->result_array();
        return $return;
    }
    public function loadProvinceDistrict(){
        $return = array();
        $this->db->select('DISTINCT( district ), province');
        $this->db->from('coverage_list');
        //$this->db->group_by('district');
        $this->db->order_by('province','asc');
        $q = $this->db->get();
        $return = $q->result_array();
        return $return;
    }
    public function loadCoverageGroup(){
        $return = array();
        $this->db->select('DISTINCT(zone)');
        $this->db->from('coverage_list');
            //$this->db->group_by('zone');
        $result = $this->db->get();
        $return = $result->result_array();
        /*foreach( $return as $key => $group ){
            $return[ $key ]['provinces'] = array();
            $this->db->select('province');
            $this->db->from('coverage_list');
            $this->db->where('zone', $group['zone'] );
            $this->db->group_by('province');
            $result = $this->db->get();
            $return[ $key ]['provinces'] = $result->result_array();
        }*/
        return $return;
    }
    public function setPageView( $table , $code ){
        if( !isset( $_SESSION['content_'.$code] )){
            $this->session->set_userdata('content_'.$code, "1" );
            $this->db->where( 'code', $code );
            $this->db->set('page_view', '`page_view`+1', FALSE);
            $this->db->update( 'content' );
        }
    }
    public function loadSeo( $page ){
        $return = array();
        if( $page == ""){
            return $return;
        }else{
            $this->db->where( 'page', $page );
            $this->db->where( 'lang', $this->session->userdata('lang') );
            $q = $this->db->get('seo');
            $result = $q->result_array();
            if( sizeof( $result ) > 0 ){
            $return = $result[0];
            }
            return $return;
        }
    }
  public function setSiteStat( $url ){
    $data = array();
    $data['s_token'] = $this->session->userdata('s_token');
    $data['ip_address'] = $this->session->userdata('client_ip');
    $data['url'] = $url;
    $data['c_date'] = date('Y-m-d H:i:s') ;

    $this->db->where('s_token', $data['s_token']);
    $this->db->where('ip_address', $data['ip_address']);
    $this->db->where('url', $data['url']);
    $q = $this->db->get('site_stat');
    $result = $q->result_array();
    if( sizeof($result) == 0 ){
      $this->db->insert('site_stat', $data );
    }
  }
  public function loadBanner( $page = "" ){
    $result = array();
    $this->db->where( 'page', $page );
    $this->db->where( 'onoff', 1 );
    $this->db->where( 'status', 1 );
    $this->db->where( 'lang', $this->session->userdata('lang') );
    $this->db->order_by( 'sticky', 'desc' );
    $q = $this->db->get('banner');
    $result = $q->result_array();
    return $result;
  }
  public function loadSection( $page = "" , $sec_key = "" ){
    $return = array();
    if( $page == ""){
      return $return;
    }else{
      $this->db->where( 'page', $page );
      $this->db->where( 'onoff', 1 );
      $this->db->where( 'status', 1 );
      $this->db->where( 'lang', $this->session->userdata('lang') );
      if( $sec_key != ""){
        $this->db->where( 'sec_key', $sec_key );
      }
      $this->db->order_by( 'sticky', 'desc' );
      $q = $this->db->get('section');
      $result = $q->result_array();
      foreach( $result as $res ){
        $return[ $res['sec_key'] ] = $res;

        $this->db->where( 'page', $res['page'] );
        $this->db->where( 'sec_key', $res['sec_key'] );
        $this->db->where( 'lang', $this->session->userdata('lang') );
        $this->db->where( 'onoff', 1 );
        $this->db->where( 'status', 1 );
        $this->db->order_by( 'sticky', 'desc' );
        $this->db->order_by( 'id', 'asc' );
        $q = $this->db->get('section_cate');
        $cates =$q->result_array();
        $return[ $res['sec_key'] ]['categories'] = $cates;

        $this->db->where( 'page', $res['page'] );
        $this->db->where( 'sec_key', $res['sec_key'] );
        $this->db->where( 'lang', $this->session->userdata('lang') );
        $this->db->where( 'onoff', 1 );
        $this->db->where( 'status', 1 );
        if( $res['sec_key'] == "calendar" ){
          $curent_date = (intval( date("Y") ) + 543 ).'-'.date("m").'-'.date("d");
          $this->db->where('post_date >=', $curent_date );
        }
        $this->db->order_by( 'sticky', 'desc' );
        $this->db->order_by( 'ISNULL(post_date)', 'asc' );
        $this->db->order_by( 'post_date', 'asc' );
        if( $res['sec_key'] == "vrproduct" || $res['sec_key'] == "touchthebeyond" || $res['sec_key'] == "vrinfo"){ 
          $this->db->order_by( 'id', 'asc' );
        }else{
          $this->db->order_by( 'id', 'desc' );
        }
        $this->db->where('published_date <=', date('Y-m-d H:i:s') );
        $q = $this->db->get('section_lists');
        $contents =$q->result_array();
        $return[ $res['sec_key'] ]['contents'] = $contents;
        
      }
      return $return;
    }
  }


  public function loadFooter( $page = "" ){
    $return = array();
    if( $page == ""){
      return $return;
    }else{
      $this->db->where( 'page', $page );
      $this->db->where( 'onoff', 1 );
      $this->db->where( 'lang', $this->session->userdata('lang') );
      $q = $this->db->get('section_footer');
      $result = $q->result_array();
      if( sizeof( $result ) > 0 ){
        $return = $result[0];
      }
      return $return;
    }
  }
  public function loadWord(){
      $ar_return = array();
      $return = array();
      if($this->cache->redis->is_supported()) {
      $cached = $this->cache->get(REDIS_PREFIX.'loadWord');
        //echo $cached;
        if($cached != null){
            $ar_return = json_decode( $cached , true );
        }
        else{
            // something
            $result = $this->db->get('wording');
            $lists = $result->result_array();
            foreach( $lists as $list ){
              $return[ $list['label'] ] = $list[ $this->session->userdata('lang') ];
            }
            $ar_return = $return;
            $this->cache->save(REDIS_PREFIX.'loadWord', json_encode($ar_return));
        }
      }else{
          // something
          $result = $this->db->get('wording');
          $lists = $result->result_array();
          foreach( $lists as $list ){
            $return[ $list['label'] ] = $list[ $this->session->userdata('lang') ];
          }
          $ar_return = $return;
      }
      
      if( sizeof( $ar_return ) <= 0 ){
        $result = $this->db->get(REDIS_PREFIX.'wording');
          $lists = $result->result_array();
          foreach( $lists as $list ){
            $return[ $list['label'] ] = $list[ $this->session->userdata('lang') ];
          }
          $ar_return = $return;
      }
      return $ar_return;
  }
  public function page_detail($page_slut){

    $return = array();
    $this->db->where('page_slug',$page_slut);
    $this->db->where('lang', $this->session->userdata('lang') );
    if(!$this->session->userdata('is_admin_login')){
      $this->db->where('onoff', 1 );
    }
    $this->db->where('status', 1 );
    $result = $this->db->get('pages');
    $pages = $result->result_array();
    if( sizeof( $pages ) > 0 ){
      return $pages[0];
    }else{
      return array();
    }
    
  }

  public function loadContentList( $sec_code , $cate_code = '' , $limit = 6 , $p = 0 ){

    
    $this->db->where('code', $sec_code);
    $this->db->where('lang', DEFAULT_LANG );
    $q = $this->db->get('section');
    $section = $q->result_array();

    $this->db->like('sec_code','"'.$sec_code.'"', 'both');
    $this->db->where('lang', $this->session->userdata('lang') );
    $this->db->where('onoff', 1 );
    $this->db->where('status', 1 );
    $this->db->order_by('sticky' , 'desc' );
    $this->db->order_by('c_date' , 'asc' );
    $result = $this->db->get('section_cate');
    $categories = $result->result_array();

    $this->db->select('section_lists.*, section_cate.slug as cate_slug, section_cate.title as cate_title');
    $this->db->from('section_lists');
    $this->db->join('section_cate','section_cate.code = section_lists.cate_code');
    if( $cate_code != '' ){
      $this->db->where('section_lists.cate_code', $cate_code );
    } else {
      $this->db->where_in('section_lists.cate_code', $cate_code );
    }
    $this->db->like('section_cate.sec_code','"'.$sec_code.'"', 'both');
    $this->db->where('section_cate.lang', $this->session->userdata('lang') );
    $this->db->where('section_lists.lang', $this->session->userdata('lang') );
    $this->db->where('section_lists.onoff', 1 );
    $this->db->where('section_lists.status', 1 );
    $this->db->where('section_lists.published_date <=', date('Y-m-d H:i:s') );

    if( sizeof( $categories ) > 0 ){
        foreach( $categories as $cate_key => $category ){
          if( $category['must_be_approve'] == 1){
            $this->db->where('section_lists.approve_status', 2 );
          }
        }
    }

    $this->db->order_by('section_lists.sticky', 'desc' );
    $this->db->order_by('section_lists.is_highlight', 'desc' );
    $this->db->order_by('section_lists.published_date' , $section[0]['sort_option'] );
    //$this->db->order_by('section_lists.c_date', 'desc' );
    $offset = 0;
    if( $limit == 0){ $limit = 6; }
    if( $p != 0){
      if( $p == 1 ){
        $offset = 0;
      }else{
        $offset = ( intVal( $limit )* ( intVal( $p ) - 1 ) );
      }
      $this->db->limit( $limit, $offset );
    }else{
      $this->db->limit( $limit );
    }
    
    $q = $this->db->get();
    $results = $q->result_array();
    //echo $this->db->last_query();
    $this->db->select('section.*, pages.page_slug');
    $this->db->from('section');
    $this->db->join('pages','section.page_code = pages.code');
    $this->db->where('section.code', $sec_code);
    $q = $this->db->get();
    $sections = $q->result_array();
    foreach( $results as $key => $result){
      $results[$key]['design_template'] = $sections[0]['design_template'];
      $results[$key]['is_tags'] = $sections[0]['is_tags'];
      $results[$key]['page_slug'] = $sections[0]['page_slug'];
      $results[$key]['listing_alignment'] = $sections[0]['listing_alignment'];
    }
    
    return $results;
    
  }

  public function loadSections($code){

      $return = array();
      $this->db->where('page_code', $code );
      $this->db->where('status', 1 );
      $this->db->where('onoff', 1 );
      $this->db->order_by('sticky', 'desc' );
      $q = $this->db->get('row');
      $rows = $q->result_array();

      $this->db->where('status', 1 );
      $q = $this->db->get('column');
      $columns = $q->result_array();

      $this->db->where('lang', $this->session->userdata('lang') );
      $this->db->where('status', 1 );
      $this->db->where('onoff', 1 );
      $this->db->order_by('sticky', 'desc' );
      $this->db->order_by('c_date', 'asc' );
      $q = $this->db->get('section');
      $sections = $q->result_array();

      foreach( $rows as $key => $row ){
        $rows[ $key ]['columns'] = array();
        foreach( $columns as $col_key => $column ){
          
          if( $column['row_id'] == $row['id']){ 
            $rows[ $key ]['columns'][ $col_key ] = $column;
            $rows[ $key ]['columns'][ $col_key ]['sections'] = array();
            // foreach( $sections as $sec_key => $sec ){
            //   if( $column['id'] == $sec['column_id']){ 
            //     $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key] = $sec; 
            //   }
            // }
            foreach( $sections as $sec_key => $section ){
              if( $column['id'] == $section['column_id']){ 
                $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key] = $section; 
                $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'] = array();
                $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['banner_listings'] = array();
                if( $section['section_type'] == "listing"){
                  // Start load category in section
                  $this->db->like('sec_code','"'.$section['code'].'"', 'both');
                  $this->db->where('lang', $this->session->userdata('lang') );
                  $this->db->where('onoff', 1 );
                  $this->db->where('status', 1 );
                  $this->db->order_by('sticky' , 'desc' );
                  $this->db->order_by('c_date' , 'asc' );
                  $result = $this->db->get('section_cate');
                  $categories = $result->result_array();
                  //array_push( $sections[$key]['categories'] , $categories);
                  $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'] = array();
                  $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0] = array();
                  $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0]['code'] = '';
                  foreach( $categories as $cate_key => $category ){
                    $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][$cate_key+1] = $category;
                  }
                  // Start load listing in category
                  //if( sizeof( $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'] ) > 0 && $section['tab_setting'] == 'all_content' ){
                    $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0]['listings'] = array();
                    
                    $this->db->select('section_lists.*, section_cate.slug as cate_slug, section_cate.title as cate_title');
                    $this->db->from('section_lists');
                    $this->db->join('section_cate','section_lists.cate_code = section_cate.code');
                    /*if( $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0]['cate_permission'] != ""){
                      $this->db->where('section_lists.approve_status', 2 );
                    }*/
                    //$this->db->where('IF( dn_section_cate.cate_permission != "", dn_section_lists.approve_status = 2 , "") ');
                    if( sizeof( $categories ) > 0 ){
                      $this->db->group_start();
                        foreach( $categories as $cate_key => $category ){
                          if( $cate_key == 0 ){
                            $this->db->where('section_lists.cate_code',$category['code']);
                          }else{
                            $this->db->or_where('section_lists.cate_code',$category['code']);
                          }
                          //if( $category['cate_permission'] != ""){
                          if( $category['must_be_approve'] == 1){
                            $this->db->where('dn_section_lists.approve_status', 2 );
                          }
                        }
                      $this->db->group_end();
                    }

                    $this->db->where('section_lists.lang', $this->session->userdata('lang') );
                    $this->db->where('section_cate.lang', $this->session->userdata('lang') );
                    $this->db->where('section_lists.onoff', 1 );
                    $this->db->where('section_lists.status', 1 );
                    $this->db->where('section_lists.published_date <=', date('Y-m-d H:i:s') );
                    $this->db->order_by('section_lists.is_highlight' , 'desc' );
                    $this->db->order_by('section_lists.sticky' , 'desc' );

                    $this->db->order_by('section_lists.published_date' , $section['sort_option'] );

                    if( $section['list_limit'] != 0 ){
                      $this->db->limit( $section['list_limit'] );
                    }
                    
                    $result = $this->db->get();
                    $listings = $result->result_array();
                    //echo $this->db->last_query();
                    //echo '<br>';
                    //array_push( $sections[$key]['categories'][$cate_key]['listings'] , $listings );
                    $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0]['listings'] = $listings;
                    //}else{
                    $listings_all_sum = 0;
                    foreach( $categories as $cate_key => $category ){
                      
                      $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][$cate_key+1]['listings'] = array();
                      $this->db->where('cate_code',$category['code']);
                      $this->db->where('lang', $this->session->userdata('lang') );
                      $this->db->where('onoff', 1 );
                      $this->db->where('status', 1 );
                      //if( $category['cate_permission'] != ""){
                      if( $category['must_be_approve'] == 1){
                        $this->db->where('approve_status', 2 );
                      }
                      $this->db->where('published_date <=', date('Y-m-d H:i:s') );
                      $this->db->order_by('is_highlight' , 'desc' );
                      $this->db->order_by('sticky' , 'desc' );
                      
                      $this->db->order_by('published_date' , $section['sort_option'] );

                      $result_all = $this->db->get('section_lists');
                      $listings_all = $result_all->result_array();
                      $listings_all_sum = $listings_all_sum + sizeof($listings_all);

                      //echo $this->db->last_query();
                      //echo '<br>';
                      //echo '<br>';
                      //print_r($category);
                      $this->db->where('cate_code',$category['code']);
                      $this->db->where('lang', $this->session->userdata('lang') );
                      $this->db->where('onoff', 1 );
                      $this->db->where('status', 1 );
                      if( $category['cate_permission'] != ""){
                        $this->db->where('approve_status', 2 );
                      }
                      $this->db->where('published_date <=', date('Y-m-d H:i:s') );
                      $this->db->order_by('is_highlight' , 'desc' );
                      $this->db->order_by('sticky' , 'desc' );
                      
                      $this->db->order_by('published_date' , $section['sort_option'] );

                      if( $section['list_limit'] != 0 ){
                        $this->db->limit( $section['list_limit'] );
                      }

                      $result = $this->db->get('section_lists');
                      $listings = $result->result_array();
                      //print_r($category);
                      foreach( $listings as $list_key => $lists ){
                        $listings[ $list_key ]['cate_slug'] = $category['slug'];
                        $listings[ $list_key ]['cate_title'] = $category['title'];
                        
                        // $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['limit_page'] = '';
                        // if( $section['list_limit'] != 0 ){
                        //   if ( sizeof( $listings_all )) {
                        //     $listings_all_count = count($listings_all);
                        //     $limit_page = ceil($listings_all_count/$section['list_limit']);
                        //     $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['limit_page'] = $limit_page;
                        //   }
                        // }
                        
                      }
                      //array_push( $sections[$key]['categories'][$cate_key]['listings'] , $listings );
                      $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][$cate_key+1]['listings'] = $listings;
                    //}
                    }
                    $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['limit_page'] = '';
                    if( $section['list_limit'] != 0 ){
                        if ( $listings_all_sum > 0 ) {
                        $listings_all_count = $listings_all_sum;
                        $limit_page = ceil($listings_all_count/$section['list_limit']);

                        $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['limit_page'] = $limit_page;
                        $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['listings_all_count'] = $listings_all_count;

                        }
                    }
                  

                }elseif($section['section_type'] == "banner"){
                  $this->db->where('sec_code',$section['code']);
                  $this->db->where('lang', $this->session->userdata('lang') );
                  $this->db->where('onoff', 1 );
                  $this->db->where('status', 1 );
                  $this->db->order_by('sticky' , 'desc' );
                  //$this->db->order_by('is_highlight' , 'desc' );
                  $result = $this->db->get('banner');
                  $banner_listings = $result->result_array();
                  $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['banner_listings'] = $banner_listings;
                }
              }
            }
            

          }
        }
      }
      $return = $rows;

      /*$this->db->where('page_code', $page_code );
      $this->db->where('lang', 'thailand' );
      $this->db->where('status', 1 );
      $this->db->order_by('sticky', 'desc' );
      $this->db->order_by('c_date', 'asc' );
      $result = $this->db->get('section');
      $return = $result->result_array();*/
      return $return;


    // $return = array();
    // $this->db->where('page_code',$code);
    // $this->db->where('lang', $this->session->userdata('lang') );
    // $this->db->where('onoff', 1 );
    // $this->db->where('status', 1 );
    // $this->db->order_by('sticky' , 'desc' );
    // $result = $this->db->get('section');
    // $sections = $result->result_array();
    // foreach( $sections as $key => $section ){
    //   $sections[$key]['categories'] = array();
    //   $sections[$key]['banner_listings'] = array();
    //   if( $section['section_type'] == "listing"){
    //     // Start load category in section
    //     $this->db->where('sec_code',$section['code']);
    //     $this->db->where('lang', $this->session->userdata('lang') );
    //     $this->db->where('onoff', 1 );
    //     $this->db->where('status', 1 );
    //     $result = $this->db->get('section_cate');
    //     $categories = $result->result_array();
    //     //array_push( $sections[$key]['categories'] , $categories);
    //     $sections[$key]['categories'] = $categories;
    //     // Start load listing in category
    //     foreach( $categories as $cate_key => $category ){
    //       $sections[$key]['categories'][$cate_key]['listings'] = array();
    //       $this->db->where('cate_code',$category['code']);
    //       $this->db->where('lang', $this->session->userdata('lang') );
    //       $this->db->where('onoff', 1 );
    //       $this->db->where('status', 1 );
    //       $result = $this->db->get('section_lists');
    //       $listings = $result->result_array();
    //       //array_push( $sections[$key]['categories'][$cate_key]['listings'] , $listings );
    //       $sections[$key]['categories'][$cate_key]['listings'] = $listings;
    //     }
    //   }elseif($section['section_type'] == "banner"){
    //     $this->db->where('sec_code',$section['code']);
    //     $this->db->where('lang', $this->session->userdata('lang') );
    //     $this->db->where('onoff', 1 );
    //     $this->db->where('status', 1 );
    //     $this->db->order_by('sticky' , 'desc' );
    //     $result = $this->db->get('banner');
    //     $banner_listings = $result->result_array();
    //     $sections[$key]['banner_listings'] = $banner_listings;
    //   }
    // }
    // return $sections;
  }

  // public function loadCategories($code){

  //   $return = array();
  //   $this->db->where('sec_code',$code);
  //   $this->db->where('lang', $this->session->userdata('lang') );
  //   $this->db->where('onoff', 1 );
  //   $this->db->where('status', 1 );
  //   $result = $this->db->get('section_cate');
  //   return $result->result_array();
    
  // }
  // public function loadListings($code){

  //   $return = array();
  //   $this->db->where('cate_code',$code);
  //   $this->db->where('lang', $this->session->userdata('lang') );
  //   $this->db->where('onoff', 1 );
  //   $this->db->where('status', 1 );
  //   $result = $this->db->get('section_lists');
  //   return $result->result_array();
    
  // }

  public function loadMenu( $position = "top" ){
    if( $position == "top"){
        $ar_return = array();
        if($this->cache->redis->is_supported()) {
        $cached = $this->cache->get(REDIS_PREFIX.'loadMenu');
        //echo $cached;
        if($cached != null && $cached != ""){
            $ar_return = json_decode( $cached , true );
        }
        else{
            $this->db->where('onoff', 1 );
            $this->db->where('status', 1 );
            $this->db->where('position', $position );
            $this->db->where('lang', $this->session->userdata('lang') );
            $this->db->order_by('sticky', 'desc' );
            $result = $this->db->get('menu');
            $ar_menus = $result->result_array();
            $ar_level0 = array();
            // Set level 0
            foreach( $ar_menus as $menu ){
              if( $menu['parent_code'] == 0 ){
                $menu['sub_level1'] = array();
                array_push($ar_level0, $menu);
              }
            }
            foreach( $ar_level0 as $key => $level0 ){
              foreach( $ar_menus as $menu ){
                if( $menu['parent_code'] == $level0['code'] ){
                  $menu['sub_level2'] = array();
                  array_push( $ar_level0[$key]['sub_level1'], $menu);
                }
              }
            }
            foreach( $ar_level0 as $key0 => $level0 ){
              foreach( $level0['sub_level1'] as $key1 => $level1 ){
                foreach( $ar_menus as $menu ){
                  if( $menu['parent_code'] == $level1['code'] ){
                    array_push( $ar_level0[$key0]['sub_level1'][$key1]['sub_level2'], $menu);
                  }
                }
              }
            }
            $ar_return = $ar_level0;
            $this->cache->save(REDIS_PREFIX.'loadMenu', json_encode($ar_return));
        }
      }else{
          $this->db->where('onoff', 1 );
          $this->db->where('status', 1 );
          $this->db->where('position', $position );
          $this->db->where('lang', $this->session->userdata('lang') );
          $this->db->order_by('sticky', 'desc' );
          $result = $this->db->get('menu');
          $ar_menus = $result->result_array();
          $ar_level0 = array();
          // Set level 0
          foreach( $ar_menus as $menu ){
            if( $menu['parent_code'] == 0 ){
              $menu['sub_level1'] = array();
              array_push($ar_level0, $menu);
            }
          }
          foreach( $ar_level0 as $key => $level0 ){
            foreach( $ar_menus as $menu ){
              if( $menu['parent_code'] == $level0['code'] ){
                $menu['sub_level2'] = array();
                array_push( $ar_level0[$key]['sub_level1'], $menu);
              }
            }
          }
          foreach( $ar_level0 as $key0 => $level0 ){
            foreach( $level0['sub_level1'] as $key1 => $level1 ){
              foreach( $ar_menus as $menu ){
                if( $menu['parent_code'] == $level1['code'] ){
                  array_push( $ar_level0[$key0]['sub_level1'][$key1]['sub_level2'], $menu);
                }
              }
            }
          }
          $ar_return = $ar_level0;
      }
      return $ar_return;
      
    }else{
      $return = array();
      $this->db->where('status', 1 );
      $this->db->where('position', "top" );
      $this->db->where('lang', $this->session->userdata('lang') );
      $this->db->order_by('sticky', 'desc' );
      $result = $this->db->get('menu');
      $ar_menus = $result->result_array();
      return $ar_menus;
    }
  }

    public function loadPages(){
        $ar_return = array();
        if($this->cache->redis->is_supported()) {
            $cached = $this->cache->get(REDIS_PREFIX.'loadPages');
            //echo $cached;
            if($cached != null && $cached != ""){
                $ar_return = json_decode( $cached , true );
            }
            else{
                // something
                $this->db->select('*');
                $this->db->from( 'pages' );
                $this->db->where('pages.onoff', 1 );
                $this->db->where('pages.status', 1 );
                $this->db->where('pages.lang', $this->session->userdata('lang') );
                $result = $this->db->get();
                $ar_return = $result->result_array();
                $this->cache->save(REDIS_PREFIX.'loadPages', json_encode($ar_return));
            }
        }else{
            // something
            $this->db->select('*');
            $this->db->from( 'pages' );
            $this->db->where('pages.onoff', 1 );
            $this->db->where('pages.status', 1 );
            $this->db->where('pages.lang', $this->session->userdata('lang') );
            $result = $this->db->get();
            $ar_return = $result->result_array();
        }

        return $ar_return;
    }

    

    public function general_detail(){

    }

    public function solution_list( $data = '', $type = 'list', $limit = '' ){
        $return_single = true;

        if ( $type == 'list' ) {
            $return_single = false;
        } else if ( $type == 'more' ) {
            $this->db->where('code !=', $data );
            $return_single = false;
        } else if ( $type == 'slug' ) {
            $this->db->where('slug', $data );
        } else if ( $type == 'code' ) {
            $this->db->where('code', $data );
        } else if ( $type == 'like_cate' ) {
            $this->db->like('cate_code','"'.$data.'"', 'both');
            $return_single = false;
        }

        $this->db->where('onoff', 1 );
        $this->db->where('status', 1 );
        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->order_by('sticky', 'desc' );
        $this->db->order_by('c_date' , 'asc' );

        if( $limit != '' ){
            $this->db->limit( $limit );
        }

        $q = $this->db->get('solution');
        $result = $q->result_array();
        if( sizeof( $result ) > 0 ){
            return $return_single ? $result[0] : $result;
        }else{
            return array();
        }
    }

    public function experience_list_solution( $cate_code ){

        $this->db->like('solution_cate_code','"'.$cate_code.'"', 'both');
        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->where('status', 1 );
        $this->db->where('onoff', 1 );
        $this->db->order_by('sticky', 'desc' );
        $this->db->order_by('c_date', 'asc' );

        $q = $this->db->get('experience');
        $result = $q->result_array();
        if( sizeof( $result ) > 0 ){
            return $result;
        }else{
            return array();
        }
    }

    public function solution_cate_list( $code = '' ){
        if ( $code != '' ) {
            $this->db->where('code !=', $code );
        }
        
        $this->db->where('onoff', 1 );
        $this->db->where('status', 1 );
        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->order_by('sticky', 'desc' );
        $this->db->order_by('c_date' , 'asc' );
        $q = $this->db->get('solution_category');
        $result = $q->result_array();
        if( sizeof( $result ) > 0 ){
            return $result;
        }else{
            return array();
        }
    }

    

    public function experience_cate_detail( $cate_slug = '' ){
        if ( $cate_slug != '' ) {
            $this->db->where('slug', $cate_slug );
        }

        $this->db->where('lang', $this->session->userdata('lang') );
        $q = $this->db->get('experience_category');
        $result = $q->result_array();
        if( sizeof( $result ) > 0 ){
            return $result[0];
        }else{
            return array();
        }
    }

    public function experience_list_detail( $cate_code = '' ){
        $this->db->like('cate_code','"'.$cate_code.'"', 'both');
        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->where('status', 1 );
        $this->db->where('onoff', 1 );
        $this->db->order_by('sticky', 'desc' );
        $this->db->order_by('c_date', 'asc' );

        $q = $this->db->get('experience');
        $result = $q->result_array();
        if( sizeof( $result ) > 0 ){
            return $result;
        }else{
            return array();
        }
    }

    public function experience_detail( $slug = '' ){
        //$this->db->like('cate_code','"'.$cate_code.'"', 'both');
        $this->db->where('slug', $slug );
        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->where('status', 1 );
        $this->db->where('onoff', 1 );
        $this->db->order_by('sticky', 'desc' );
        $this->db->order_by('c_date', 'asc' );

        $q = $this->db->get('experience');
        $result = $q->result_array();
        if( sizeof( $result ) > 0 ){
            return $result[0];
        }else{
            return array();
        }
    }

    public function content_detail( $content_slug ){
        $this->db->where('slug', $content_slug );
        $this->db->where('lang', $this->session->userdata('lang') );
        if( !$this->session->userdata('is_admin_login') ){
            $this->db->where('published_date <=', date('Y-m-d H:i:s') );
        }
        $q = $this->db->get('section_lists');
        $result = $q->result_array();
        if( sizeof( $result ) > 0 ){
            return $result[0];
        }else{
            return array();
        }
    }

    public function content_detail_preview( $content_slug, $preview_id ){
        $this->db->where('slug', $content_slug );
        $this->db->where('preview', $preview_id );
        $this->db->where('lang', $this->session->userdata('lang') );
        $q = $this->db->get('section_lists_preview');
        $result = $q->result_array();

        //print_r($result);
        //print_r($this->db->last_query());
        if( sizeof( $result ) > 0 ){
            return $result[0];
        }else{
            return array();
        }
    }

    public function loadRelateList(  $cate_code = '' , $content_code = '' ){
    if( $cate_code == '' || $content_code == '' ){
        return array();
    }else{

        $this->db->select('section_lists.*, section_cate.slug as cate_slug, section_cate.title as cate_title');
        $this->db->from('section_lists');
        $this->db->join('section_cate','section_cate.code = section_lists.cate_code');
        $this->db->where('section_lists.code !=', $content_code );
        $this->db->where('section_lists.cate_code', $cate_code );
        $this->db->where('section_cate.lang', $this->session->userdata('lang') );
        $this->db->where('section_lists.lang', $this->session->userdata('lang') );
        $this->db->where('section_lists.onoff', 1 );
        $this->db->where('section_lists.status', 1 );
        $this->db->where('section_lists.published_date <=', date('Y-m-d H:i:s') );

        $this->db->order_by('section_lists.sticky', 'desc' );
        $this->db->order_by('section_lists.is_highlight', 'desc' );
        $this->db->order_by('section_lists.c_date', 'desc' );
        $q = $this->db->get();
        $results = $q->result_array();
        return $results;
    }

    }

    public function search_tag( $tag ){

        $this->db->select('section_lists.*, section_cate.slug as cate_slug, section_cate.title as cate_title');
        $this->db->from('section_lists');
        $this->db->join('section_cate','section_cate.code = section_lists.cate_code');
        $this->db->like('section_lists.tags',$tag, 'both');
        $this->db->where('section_cate.lang', $this->session->userdata('lang') );
        $this->db->where('section_lists.lang', $this->session->userdata('lang') );
        $this->db->where('section_lists.onoff', 1 );
        $this->db->where('section_lists.status', 1 );
        $this->db->where('section_lists.published_date <=', date('Y-m-d H:i:s') );
        
        $q = $this->db->get();
        $result = $q->result_array();
        //print_r($this->db->last_query());
        if( sizeof( $result ) > 0 ){
            return $result;
        }else{
            return array();
        }
    }

    public function loadSections_preview($code, $preview_id){

      $return = array();
      $this->db->where('page_code', $code );
      $this->db->where('status', 1 );
      $this->db->where('onoff', 1 );
      $this->db->order_by('sticky', 'desc' );
      $q = $this->db->get('row');
      $rows = $q->result_array();

      $this->db->where('status', 1 );
      $q = $this->db->get('column');
      $columns = $q->result_array();

      $this->db->where('lang', $this->session->userdata('lang') );
      $this->db->where('status', 1 );
      $this->db->where('onoff', 1 );
      $this->db->order_by('sticky', 'desc' );
      $this->db->order_by('c_date', 'asc' );
      $q = $this->db->get('section');
      $sections = $q->result_array();

      foreach( $rows as $key => $row ){
        $rows[ $key ]['columns'] = array();
        foreach( $columns as $col_key => $column ){
          
          if( $column['row_id'] == $row['id']){ 
            $rows[ $key ]['columns'][ $col_key ] = $column;
            $rows[ $key ]['columns'][ $col_key ]['sections'] = array();
            // foreach( $sections as $sec_key => $sec ){
            //   if( $column['id'] == $sec['column_id']){ 
            //     $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key] = $sec; 
            //   }
            // }
            foreach( $sections as $sec_key => $section ){

              $this->db->where('lang', $this->session->userdata('lang') );
              $this->db->where('status', 1 );
              $this->db->where('onoff', 1 );
              $this->db->where('code', $section['code'] );
              $this->db->where('preview', $preview_id );
              $this->db->order_by('sticky', 'desc' );
              $this->db->order_by('c_date', 'asc' );
              $q = $this->db->get('section_preview');
              $sections_preview = $q->result_array();
              if ( sizeof($sections_preview) ) {
                $section = $sections_preview[0];
              }

              if( $column['id'] == $section['column_id']){ 
                $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key] = $section; 
                $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'] = array();
                $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['banner_listings'] = array();
                if( $section['section_type'] == "listing"){
                  // Start load category in section
                  $this->db->like('sec_code','"'.$section['code'].'"', 'both');
                  $this->db->where('lang', $this->session->userdata('lang') );
                  $this->db->where('onoff', 1 );
                  $this->db->where('status', 1 );
                  $result = $this->db->get('section_cate');
                  $categories = $result->result_array();
                  //array_push( $sections[$key]['categories'] , $categories);
                  $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'] = array();
                  $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0] = array();
                  $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0]['code'] = '';
                  foreach( $categories as $cate_key => $category ){
                    $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][$cate_key+1] = $category;
                  }
                  // Start load listing in category
                  //if( sizeof( $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'] ) > 0 && $section['tab_setting'] == 'all_content' ){
                    $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0]['listings'] = array();
                    
                    $this->db->select('section_lists.*, section_cate.slug as cate_slug, section_cate.title as cate_title');
                    $this->db->from('section_lists');
                    $this->db->join('section_cate','section_lists.cate_code = section_cate.code');
                    if( sizeof( $categories ) > 0 ){
                      $this->db->group_start();
                        foreach( $categories as $cate_key => $category ){
                          if( $cate_key == 0 ){
                            $this->db->where('section_lists.cate_code',$category['code']);
                          }else{
                            $this->db->or_where('section_lists.cate_code',$category['code']);
                          }
                        }
                      $this->db->group_end();
                    }

                    $this->db->where('section_lists.lang', $this->session->userdata('lang') );
                    $this->db->where('section_cate.lang', $this->session->userdata('lang') );
                    $this->db->where('section_lists.onoff', 1 );
                    $this->db->where('section_lists.status', 1 );
                    $this->db->where('section_lists.published_date <=', date('Y-m-d H:i:s') );
                    $this->db->order_by('section_lists.sticky' , 'desc' );
                    $this->db->order_by('section_lists.is_highlight' , 'desc' );
                    $this->db->order_by('section_lists.c_date' , 'desc' );
                    if( $section['list_limit'] != 0 ){
                      $this->db->limit( $section['list_limit'] );
                    }
                    
                    $result = $this->db->get();
                    $listings = $result->result_array();
                    
                    //array_push( $sections[$key]['categories'][$cate_key]['listings'] , $listings );
                    $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0]['listings'] = $listings;
                  //}else{
                    foreach( $categories as $cate_key => $category ){
                      $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][$cate_key+1]['listings'] = array();
                          $this->db->where('cate_code',$category['code']);
                          $this->db->where('lang', $this->session->userdata('lang') );
                          $this->db->where('onoff', 1 );
                          $this->db->where('status', 1 );
                          //if( $category['cate_permission'] != ""){
                          if( $category['must_be_approve'] == 1){
                            $this->db->where('approve_status', 2 );
                          }
                          $this->db->where('published_date <=', date('Y-m-d H:i:s') );
                          $this->db->order_by('is_highlight' , 'desc' );
                          $this->db->order_by('sticky' , 'desc' );
                          
                          $this->db->order_by('published_date' , $section['sort_option'] );

                          $result_all = $this->db->get('section_lists');
                          $listings_all = $result_all->result_array();

                          $this->db->where('cate_code',$category['code']);
                          $this->db->where('lang', $this->session->userdata('lang') );
                          $this->db->where('onoff', 1 );
                          $this->db->where('status', 1 );
                          if( $category['cate_permission'] != ""){
                            $this->db->where('approve_status', 2 );
                          }
                          $this->db->where('published_date <=', date('Y-m-d H:i:s') );
                          $this->db->order_by('is_highlight' , 'desc' );
                          $this->db->order_by('sticky' , 'desc' );
                          
                          $this->db->order_by('published_date' , $section['sort_option'] );

                          if( $section['list_limit'] != 0 ){
                            $this->db->limit( $section['list_limit'] );
                          }
                          
                      $result = $this->db->get('section_lists');
                      $listings = $result->result_array();

                      foreach( $listings as $list_key => $lists ){
                        $listings[ $list_key ]['cate_slug'] = $category['slug'];
                        $listings[ $list_key ]['cate_title'] = $category['title'];

                        $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['limit_page'] = '';
                        if( $section['list_limit'] != 0 ){
                          if ( sizeof( $listings_all )) {
                            $listings_all_count = count($listings_all);
                            $limit_page = ceil($listings_all_count/$section['list_limit']);
                            //$listings[ $list_key ]['limit_page'] = $limit_page;
                            $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['limit_page'] = $limit_page;
                          }
                        }
                      }
                      //array_push( $sections[$key]['categories'][$cate_key]['listings'] , $listings );
                      $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][$cate_key+1]['listings'] = $listings;
                    //}
                  }
                  

                }elseif($section['section_type'] == "banner"){
                  $this->db->where('sec_code',$section['code']);
                  $this->db->where('lang', $this->session->userdata('lang') );
                  $this->db->where('onoff', 1 );
                  $this->db->where('status', 1 );
                  $this->db->order_by('sticky' , 'desc' );
                  //$this->db->order_by('is_highlight' , 'desc' );
                  $result = $this->db->get('banner');
                  $banner_listings = $result->result_array();
                  $rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key]['banner_listings'] = $banner_listings;
                }
              }
            }
  
          }
        }
      }
      $return = $rows;

      return $return;
  }

    public function loadSectionsDetail( $section_id = '' ) {
        if ( $section_id != '' ) {
            $this->db->where( 'code', $section_id );
            // $this->db->where( 'onoff', 1 );
            // $this->db->where( 'status', 1 );
            $this->db->where( 'lang', $this->session->userdata('lang') );
            $this->db->order_by( 'sticky', 'desc' );
            $q = $this->db->get('section');
            $result = $q->row_array();
        } else {
            $result = array();
        }

        return $result;
    }

    public function content_solution_detail( $content_slug ){
        $this->db->where('slug', $content_slug );
        $this->db->where('lang', $this->session->userdata('lang') );
        if( !$this->session->userdata('is_admin_login') ){
            $this->db->where('published_date <=', date('Y-m-d H:i:s') );
        }
        $q = $this->db->get('section_solution_lists');
        $result = $q->row_array();

        if( sizeof( $result ) > 0 ){
            // get section_cate
            // $result['cate_list'] = '';
            // if ( $result['cate_code'] != '' ) {
            //     $cate_code = json_decode( $result['cate_code'] );

            //     $this->db->where_in('code', $cate_code );
            //     $this->db->where('lang', $this->session->userdata('lang') );
  
            //     $q = $this->db->get('section_cate');
            //     $result['cate_list'] = $q->result_array();
            // }

            // get usecase
            $result['usecase_list'] = '';
            if ( $result['usecase_code'] != '' ) {
                $usecase_code = json_decode( $result['usecase_code'] );

                $this->db->where_in('code', $usecase_code );
                $this->db->where('lang', $this->session->userdata('lang') );
  
                $q = $this->db->get('solution_usecase');
                $result['usecase_list'] = $q->result_array();
            }

            // get banner_code
            $result['banner_list'] = '';
            if ( $result['banner_code'] != '' ) {
                $banner_code = json_decode( $result['banner_code'] );

                $this->db->where_in('code', $banner_code );
                $this->db->where('lang', $this->session->userdata('lang') );
  
                $q = $this->db->get('solution_banner');
                $result['banner_list'] = $q->row_array();
            }

            //print_r( $result );
            // exit;
            return $result;
        }else{
            return array();
        }
    }

    public function loadRelateListSolution(  $cate_code = array() , $content_code = '' ){
        if( sizeof($cate_code) == 0 || $content_code == '' ){
            return array();
        }else{
            $this->db->from('section_solution_lists');
            //$this->db->join('section_cate','section_cate.code = section_lists.cate_code');
            $this->db->where('code !=', $content_code );
            if ( $cate_code ) {
                foreach ($cate_code as $key => $val) {
                    $this->db->like('cate_code','"'.$val.'"', 'both');
                }
            }
            
            //$this->db->where('section_cate.lang', $this->session->userdata('lang') );
            $this->db->where('lang', $this->session->userdata('lang') );
            $this->db->where('onoff', 1 );
            $this->db->where('status', 1 );
            $this->db->where('published_date <=', date('Y-m-d H:i:s') );
        
            $this->db->order_by('sticky', 'desc' );
            $this->db->order_by('is_highlight', 'desc' );
            $this->db->order_by('c_date', 'desc' );
            $q = $this->db->get();
            $results = $q->result_array();

            //print_r($this->db->last_query());
            return $results;
        }
    }

    public function loadListSolution(  $cate_code = ''  ){
        if( ($cate_code) == 0 ){
            return array();
        }else{
            $this->db->from('section_solution_lists');
            //$this->db->join('section_cate','section_cate.code = section_lists.cate_code');
            $this->db->like('cate_code','"'.$cate_code.'"', 'both');
            
            //$this->db->where('section_cate.lang', $this->session->userdata('lang') );
            $this->db->where('lang', $this->session->userdata('lang') );
            $this->db->where('onoff', 1 );
            $this->db->where('status', 1 );
            $this->db->where('published_date <=', date('Y-m-d H:i:s') );
        
            $this->db->order_by('sticky', 'desc' );
            $this->db->order_by('is_highlight', 'desc' );
            $this->db->order_by('c_date', 'desc' );
            $q = $this->db->get();
            $results = $q->result_array();

            //print_r($this->db->last_query());
            return $results;
        }
    }

    public function solution_brochure(){

        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->where('code', 1 );
        $this->db->where('onoff', 1 );
        $this->db->where('status', 1 );
        //$this->db->order_by('sticky', 'desc' );
        
        $q = $this->db->get('solution_brochure');
        $result = $q->row_array();
        return $result;
    } 

    public function smepack_brochure_pdf(){

        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->where('code', 0 );
        $this->db->where('onoff', 1 );
        $this->db->where('status', 1 );
        $this->db->where('parent_code', 0 );
        $this->db->order_by('sticky', 'desc' );
        
        $q = $this->db->get('smepack_category');
        $result = $q->row_array();
        return $result;
    } 
    

    public function smepack_cate_detail(  $slug = ''  ){

        $data = array();
        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->where('onoff', 1 );
        $this->db->where('status', 1 );
        $this->db->where('slug', $slug );
        $this->db->order_by('sticky', 'desc' );
        
        $q = $this->db->get('smepack_category');
        $row = $q->row_array();
        if( $row != '' ) {
            $data['detail'] = $row;

            $this->db->where('lang', $this->session->userdata('lang') );
            $this->db->where('onoff', 1 );
            $this->db->where('status', 1 );
            $this->db->where('parent_code', $data['detail']['code'] );
            $this->db->order_by('sticky', 'desc' );
            
            $q = $this->db->get('smepack_category');
            $result = $q->result_array();
            if( sizeof( $result ) > 0 ) {
                foreach ($result as $key => $row) {
                    $data['detail']['cate_list'][$key] = $row;

                    $this->db->where('lang', $this->session->userdata('lang') );
                    $this->db->where('onoff', 1 );
                    $this->db->where('status', 1 );
                    $this->db->where('cate_code', $data['detail']['code'] );
                    $this->db->where('child_cate_code', $row['code'] );
                    // $this->db->order_by('price', 'asc' );
                    $this->db->order_by('sticky', 'desc' );

                    $q = $this->db->get('smepack');
                    $result_smepack = $q->result_array();
                   
                    if( sizeof($result_smepack) > 0 ) {

                        $data['detail']['cate_list'][$key]['smepack'] = $result_smepack;
                    } else {
                        $data['detail']['cate_list'][$key]['smepack'] = array();
                    }

                }
            } else {
                $data['detail']['cate_list'][] = array();
            }
        }

        return $data;
    } 

    public function get_special_offer(){

        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->where('onoff', 1 );
        $this->db->where('status', 1 );

        $q = $this->db->get('smepack_special_offer');
        $result = $q->result_array();
        if( sizeof( $result ) > 0 ) {
            return $result;
        } else{
            return array();
        }
    } 

    public function get_landing_detail( $slug = '' ) {
        if ( $slug != '' ) {
            $this->db->where( 'slug', $slug );
            $this->db->where( 'onoff', 1 );
            $this->db->where( 'status', 1 );
            $this->db->where( 'lang', $this->session->userdata('lang') );
            $q = $this->db->get('landing');
            $result = $q->row_array();
        } else {
            $result = array();
        }

        return $result;
    }

    /*
    * Start widget
    */

    public function widget_smepack_cate_menu( $parent_code = '' ){

        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->where('code !=', 0 );
        $this->db->where('onoff', 1 );
        $this->db->where('status', 1 );

        if ( $parent_code == '' ) {
            $this->db->where('parent_code', 0 );
        }
        
        $this->db->order_by('sticky', 'desc' );
        
        $q = $this->db->get('smepack_category');
        $result = $q->result_array();
        //print_r( $this->db->last_query() );    
        if( sizeof( $result ) > 0 ) {
            return $result;
        } else{
            return array();
        }
        
    } 

    public function widget_smepack_highlight(  $code = '', $acb = '', $limit = '2'  ){

        $data = array();

        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->where('onoff', 1 );
        $this->db->where('status', 1 );
        $this->db->where('code', $code );
        $this->db->order_by('sticky', 'desc' );
        
        $q = $this->db->get('smepack_category');
        $row = $q->row_array();
        if( $row != '' ) {
            $data['detail'] = $row;

            $this->db->where('lang', $this->session->userdata('lang') );
            $this->db->where('onoff', 1 );
            $this->db->where('status', 1 );
            $this->db->where('cate_code', $code );
            //$this->db->where('child_cate_code', $row['code'] );
            //$this->db->where('highlight_sme_mainpage', 1 ); // set highlight_sme_mainpage
            $this->db->order_by('highlight_homepage', 'desc' );
            $this->db->order_by('price', 'asc' );
            $this->db->order_by('sticky', 'desc' );
            $this->db->order_by('c_date', 'asc' );
            $this->db->limit($limit);
            
            $q = $this->db->get('smepack');
            $row_smepack = $q->result_array();
            // echo gettype( $row_smepack );
            // echo '<pre>';print_r( $row_smepack );echo '</pre>'; exit;
            if( sizeof( $row_smepack ) > 0 ) {
                $data['detail']['smepack'] = $row_smepack;
            } else {
                $data['detail']['smepack'] = array();
            }
        }

        return $data;
    } 

    public function widget_smepack_cate_detail(  $code = ''  ){

        $data = array();

        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->where('onoff', 1 );
        $this->db->where('status', 1 );
        $this->db->where('code', $code );
        $this->db->order_by('sticky', 'desc' );
        
        $q = $this->db->get('smepack_category');
        $row = $q->row_array();
        if( sizeof( $row ) > 0 ) {
            $data['detail'] = $row;

            $this->db->where('lang', $this->session->userdata('lang') );
            $this->db->where('onoff', 1 );
            $this->db->where('status', 1 );
            $this->db->where('parent_code', $code );
            $this->db->order_by('sticky', 'desc' );
            
            $q = $this->db->get('smepack_category');
            $result = $q->result_array();
            if( sizeof( $result ) > 0 ) {
                foreach ($result as $key => $row) {
                    $data['detail']['cate_list'][$key] = $row;

                    $this->db->where('lang', $this->session->userdata('lang') );
                    $this->db->where('onoff', 1 );
                    $this->db->where('status', 1 );
                    $this->db->where('cate_code', $code );
                    $this->db->where('child_cate_code', $row['code'] );
                    //$this->db->where('highlight_sme_mainpage', 1 ); // set highlight_sme_mainpage
                    $this->db->order_by('highlight_sme_mainpage', 'desc' );
                    $this->db->order_by('price', 'asc' );
                    $this->db->order_by('sticky', 'desc' );
                    $this->db->limit(1);
                    
                    $q = $this->db->get('smepack');
                    $row_smepack = $q->row_array();
                    // echo gettype( $row_smepack );
                    // echo '<pre>';print_r( $row_smepack );echo '</pre>'; exit;
                    if( $row_smepack != '' ) {
                        $data['detail']['cate_list'][$key]['smepack'] = $row_smepack;
                    } else {
                        $this->db->where('lang', $this->session->userdata('lang') );
                        $this->db->where('onoff', 1 );
                        $this->db->where('status', 1 );
                        $this->db->where('cate_code', $code );
                        $this->db->where('child_cate_code', $row['code'] );
                        //$this->db->where('highlight_sme_mainpage', 1 ); // set highlight_sme_mainpage
                        $this->db->order_by('price', 'asc' );
                        $this->db->order_by('sticky', 'desc' );
                        $this->db->limit(1);
                        
                        $q = $this->db->get('smepack');
                        $row_smepack = $q->row_array();
                        if( $row_smepack != '' ) {
                            $data['detail']['cate_list'][$key]['smepack'] = $row_smepack;   
                        } else {
                            $data['detail']['cate_list'][$key]['smepack'] = array();
                        }

                    }

                }
            } else {
                $data['detail']['cate_list'][] = array();
            }
        }

        return $data;
    } 

    public function widget_solution_home_cate_detail(){
        $this->db->like('sec_code','"166"', 'both');
        $this->db->where('lang', $this->session->userdata('lang') );
        $q = $this->db->get('section_cate');
        $result = $q->result_array();
        if( sizeof( $result ) > 0 ){
            return $result;
        }else{
            return array();
        }
    }

    /*
    * End widget
    */


    public function get_landing_scripts( $slug = '' ){
        $this->db->where('slug', $slug );
        $this->db->where('onoff', 1 );
        $this->db->where('status', 1 );
        $this->db->where('lang', $this->session->userdata('lang') );
        //$this->db->order_by('sticky', 'desc' );
        //$this->db->order_by('c_date' , 'asc' );
        $q = $this->db->get('landing');
        $result = $q->result_array();
        if( sizeof( $result ) > 0 ){
            return $result[0];
        }else{
            return array();
        }
    }

    public function general_list( $table = '', $field = '', $data = '', $is_single = false , $type = ''){
        // table, field, value, is_single, type
        if ( $type == '' ) {
            if ( $field != '' && $data != '' ) {
                $this->db->where( $field, $data );
            }
        } else if ( $type == 'like' ) {
            if ( $field != '' && $data != '' ) {
                $this->db->like( $field,'"'.$data.'"', 'both') ;
            }
        }

        $this->db->where('lang', $this->session->userdata('lang') );
        $this->db->where('status', 1 );
        $this->db->where('onoff', 1 );
        $this->db->order_by('sticky', 'desc' );
        $this->db->order_by('c_date', 'asc' );

        $q = $this->db->get( $table );
        $result = $q->result_array();

        //print_r($this->db->last_query());

        if( sizeof( $result ) > 0 ){
            return $is_single ? $result[0] : $result;
        }else{
            return array();
        }
    }
    
    
}
