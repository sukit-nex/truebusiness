<?php
class Smepack_model extends CI_Model {

	public function __construct(){
		// Call the CI_Model constructor
		parent::__construct();
	}
	public function loadLists( $cate_code = "" ){
		$this->db->from( 'smepack' );
		$this->db->where('status', 1 );
		$this->db->where('lang', DEFAULT_LANG );
		if( $cate_code != ""){
			$this->db->where('cate_code', $cate_code );
		}
		$this->db->order_by('sticky', 'desc' );
		$this->db->order_by('u_date', 'desc' );
		$result = $this->db->get();
		return $result->result_array();
	}

	public function loadDetail( $code ){
		$return = array();
		$this->db->where('code', $code );
		$result = $this->db->get('smepack');
		$detailList = $result->result_array();
		foreach( $detailList as $data ){
			$return[$data['lang']] = $data ;
		}
		return $return;
	}

	public function loadSpecialOfferLists( $category = "" ){
		$this->db->from( 'smepack_special_offer' );
		$this->db->where('status', 1 );
		$this->db->where('lang', DEFAULT_LANG );
		if( $category != ""){
			$this->db->where('category', $category );
		}
		//$this->db->order_by('sticky', 'desc' );
		$this->db->order_by('c_date', 'desc' );
		$result = $this->db->get();
		return $result->result_array();
	}

	public function loadSpecialOfferDetail( $code ){
		$return = array();
		$this->db->where('code', $code );
		$result = $this->db->get('smepack_special_offer');
		$detailList = $result->result_array();
		foreach( $detailList as $data ){
			$return[$data['lang']] = $data ;
		}
		return $return;
	}



}
?>
