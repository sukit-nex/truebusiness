<?php
class Smepack_category_model extends CI_Model {

	public function __construct(){
		// Call the CI_Model constructor
		parent::__construct();
	}
	public function loadLists( $cate_group = "", $parent_code = "" ){
		$this->db->where('status', 1 );
		$this->db->where('lang', DEFAULT_LANG );
		if( $cate_group != ""){
			$this->db->where('cate_for', $cate_group );
		}
		if( $parent_code != ""){
			$this->db->where('parent_code', $parent_code );
		}
		$this->db->where('onoff', 1 );
		
		$this->db->order_by('sticky', 'desc' );
		$result = $this->db->get('smepack_category');
		return $result->result_array();
	}

	public function loadListsLevel( $cate_group = "" ){
		$this->db->where('status', 1 );
		$this->db->where('lang', DEFAULT_LANG );
		if( $cate_group != ""){
			$this->db->where('cate_for', $cate_group );
		}
		$this->db->order_by('sticky', 'desc' );
		$result = $this->db->get('smepack_category');
		$ar_cates = $result->result_array();
		$ar_level0 = array();
		// Set level 0
		foreach( $ar_cates as $cate ){
			if( $cate['parent_code'] == 0 ){
				$cate['sub_level1'] = array();
				array_push($ar_level0, $cate);
			}
		}
		foreach( $ar_level0 as $key => $level0 ){
			foreach( $ar_cates as $cate ){
				if( $cate['parent_code'] == $level0['code'] ){
					$cate['sub_level2'] = array();
					array_push( $ar_level0[$key]['sub_level1'], $cate);
				}
			}
		}
		foreach( $ar_level0 as $key0 => $level0 ){
			foreach( $level0['sub_level1'] as $key1 => $level1 ){
				foreach( $ar_cates as $cate ){
					if( $cate['parent_code'] == $level1['code'] ){
						array_push( $ar_level0[$key0]['sub_level1'][$key1]['sub_level2'], $cate);
					}
				}
			}
		}
		return $ar_level0;
	}

	public function loadListsChild( $parent_code = "" ){
		$this->db->where('status', 1 );
		$this->db->where('lang', DEFAULT_LANG );
		if( $parent_code != ""){
			$this->db->where('parent_code', $parent_code );
		}
		$this->db->order_by('sticky', 'desc' );
		$result = $this->db->get('smepack_category');
		$ar_cates = $result->result_array();
		
		return $ar_cates;
	}

	public function loadDetail( $code ){
		$return = array();
		$this->db->where('code', $code );
		$result = $this->db->get('smepack_category');
		$detailList = $result->result_array();
		foreach( $detailList as $data ){
			$return[$data['lang']] = $data ;
		}
		
		return $return;
	}
}
?>
