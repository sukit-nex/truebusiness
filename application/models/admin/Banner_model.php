<?php
class Banner_model extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
        public function loadLists( $page ){
          $this->db->where('status', 1 );
          $this->db->where('page', $page );
          $this->db->where('lang', DEFAULT_LANG );
          $this->db->order_by('sticky', 'desc' );
          $this->db->order_by('c_date', 'desc' );
          $result = $this->db->get('banner');
          return $result->result_array();
        }
        public function loadDetail( $code ){
          $return = array();
          $this->db->where('code', $code );
          $result = $this->db->get('banner');
          $detailList = $result->result_array();
          foreach( $detailList as $data ){
            $return[$data['lang']] = $data ;
          }
          return $return;
        }
}
?>
