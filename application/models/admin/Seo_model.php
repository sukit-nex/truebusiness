<?php
class Seo_model extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
        public function loadDetail( $page ){
          $return = array();
          $this->db->where('page', $page );
          //$this->db->where('sec_key', $sec_key );
          $result = $this->db->get('seo');
          $detailList = $result->result_array();
          foreach( $detailList as $data ){
            $return[$data['lang']] = $data ;
          }
          return $return;
		}
}
?>
