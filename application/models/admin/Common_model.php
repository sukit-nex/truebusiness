<?php
class Common_model extends CI_Model {
      // Model for common function
      //  public $title;
      //  public $content;
      //  public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
        /*=========  Function ========*/
        public function loadSectionCategory( $cate_for = "" ){
          $this->db->where('onoff', 1 );
          $this->db->where('status', 1 );
          if( $cate_for != "" ){
            $this->db->where('cate_for !=', $cate_for );
          }
          $this->db->where('lang', DEFAULT_LANG );
          $result = $this->db->get('section_cate');
          return $result->result_array();
        }
        public function loadStatCount(){
          $data = array();
          $this->db->select('id');
          $this->db->from('site_stat');
          $this->db->group_by('s_token');
          $q = $this->db->get();
          $result = $q->result_array();
          $data['visitor'] = sizeof( $result );

          $this->db->select('id');
          $this->db->from('site_stat');
          $this->db->group_by('ip_address');
          $q = $this->db->get();
          $result = $q->result_array();
          $data['uniqe'] = sizeof( $result );

          $this->db->select('id');
          $this->db->from('site_stat');
          $q = $this->db->get();
          $result = $q->result_array();
          $data['total_pageview'] = sizeof( $result );

          return $data;
        }
        public function loadViewList(){
          $this->db->select('count(*) as cnt , url');
          $this->db->from('site_stat');
          $this->db->group_by('url');
          $this->db->limit( 10 );
          $q = $this->db->get();
          $result = $q->result_array();
          return $result;
        }
        /*public function uploadEditorImage( $image_editor, $time ){
          if( sizeof( $image_editor ) > 0 ){
            for( $i = 0 ; $i < sizeof( $image_editor['name'] ) ; $i++ ){

              $filename = $image_editor['name'][$i];
              $file_basename = substr($filename, 0, strripos($filename, '.')); // get file name
      	      $file_ext = substr($filename, strripos($filename, '.')); // get file extention
              $imageTargetPath = EDITOR_UPLOAD_PATH.$file_basename.'_'.$time.$file_ext;
              $imageSourcePath = $image_editor['tmp_name'][$i];

              if( move_uploaded_file( $imageSourcePath,$imageTargetPath ) ){
                //echo 'complete '.$i.' ';
              }
            }
          }
        }*/
        public function uploadEditorImage( $image_editor, $time ){
          if( sizeof( $image_editor ) > 0 ){
            for( $i = 0 ; $i < sizeof( $image_editor['name'] ) ; $i++ ){
              $filename = $image_editor['name'][$i];
              $file_basename = substr($filename, 0, strripos($filename, '.')); // get file name
              $file_ext = substr($filename, strripos($filename, '.')); // get file extention
              //$imageTargetPath = EDITOR_UPLOAD_PATH.$file_basename.'_'.$time.$file_ext;
              $imageTargetPath = editor_upload_path($file_basename.'_'.$time.$file_ext);
              $imageSourcePath = $image_editor['tmp_name'][$i];
              if( move_uploaded_file( $imageSourcePath,$imageTargetPath ) ){
                //echo 'complete '.$i.' ';
              }
            }
          }
        }
        public function loadLang(){ 
          $this->db->where('status', 1 );
          $result = $this->db->get('lang');
          return $result->result_array();
        }
        public function loadStat(){
          $this->db->where('id', 1 );
          $result = $this->db->get('stat');
          return $result->result_array();
        }
        public function get_code( $table ){
          $return = array();
          $this->db->select_max('code');
          $query = $this->db->get( $table );

          $ar_data =  $query->result_array();
          $code = intval($ar_data[0]['code']) + 1;
          return $code;
        }
        public function loadRecentContact(){
          $this->db->order_by('c_date', 'desc');
          $this->db->limit('5');
          $result = $this->db->get('contact_us');
          return $result->result_array();
        }
        public function wording(){
          $wording = array();
          $q = $this->db->get('wording');
          $result = $q->result_array();
          foreach( $result as $rs ){
            $wording[$rs['label']] = array(
              'thailand' => $rs['thailand'],
              'english' => $rs['english']
            );
          }
          return $wording;
        }
        public function addLogs( $type = "", $type_code = 0, $action = "", $lang = "" ){
          $data = array();
          $data['user_id'] = $this->session->userdata('id');
          $data['type'] = $type;
          $data['type_code'] = $type_code;
          $data['action'] = $action;
          $data['lang'] = $lang;
          $data['c_date'] = date('Y-m-d H:i:s');
          $this->db->insert('admin_logs' , $data );
        }

        public function updateSEO( $data = array() ){
          if( sizeof( $data ) > 0 ){
            $time = time();
            $string = str_replace(' ', '-', $data['page'] ); // Replaces all spaces with hyphens.
            $data['page'] = strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', $string) ); // Removes special chars.

            $page_title = $data['page_title'];
            $meta_title = $data['meta_title'];
            $meta_keyword = $data['meta_keyword'];
            $meta_desc = $data['meta_desc'];
            $share_img = $data['share_img'];
            //print_r( $data );
            //$max_code = $this->common->get_code( 'seo' );
            foreach( $this->data['langs'] as $key => $lang ){
              $data_insert = array();
              $data_insert['code'] = $data['code'];
              $data_insert['page'] = $data['page'];
              $data_insert['lang'] = $lang['text'];
              $data_insert['share_img'] = $data['share_img'];
              if( $data['seo_type'] == "listing"){
                $this->db->where('code', $data['code'] );
              }else{
                $this->db->where('page', $data['page'] );
              }
              $this->db->where('lang', $lang['text'] );
              $q = $this->db->get('seo');
              $result = $q->result_array();
              
              $data_insert['page_title'] = isset( $page_title[$key] ) ? $page_title[$key] : '' ;
              $data_insert['meta_title'] = isset( $meta_title[$key] ) ? $meta_title[$key] : '' ;
              $data_insert['meta_keyword'] = isset( $meta_keyword[$key] ) ? $meta_keyword[$key] : '' ;
              $data_insert['meta_desc'] = isset( $meta_desc[$key] ) ? $meta_desc[$key] : '' ;
              /*if( sizeof($result) > 0 ){

              if ( $data['page'] == $result[0]['page'] ) {
                $data['page'] = $data['page'].'-'.time();
              }
            }*/
            
              if( sizeof($result) > 0 ){

                if( $data['seo_type'] == "listing"){
                  $this->db->where('page', $data['page'] );
                  $this->db->where('lang', $lang['text'] );
                  $this->db->where('code !=', $data['code'] );
                  $q = $this->db->get('seo');
                  $resultx = $q->result_array();
                  if( sizeof( $resultx ) > 0 ){
                    $data_insert['page'] = $data['page'].'-'.$time;
                  }
                }

                $data_insert['u_date'] = date('Y-m-d H:i:s') ;
                $data_insert['u_by'] = $this->session->userdata('id');
                if( $data['seo_type'] == "listing"){
                  $this->db->where('code', $data['code'] );
                }else{
                  $this->db->where('page', $data['page'] );
                }
                
                $this->db->where('lang', $lang['text'] );
                $this->db->update('seo', $data_insert );

              }else{
                
                if( $data['seo_type'] == "listing"){
                  $this->db->where('page', $data['page'] );
                  $this->db->where('lang', $lang['text'] );
                  $q = $this->db->get('seo');
                  $resultx = $q->result_array();
                  if( sizeof( $resultx ) > 0 ){
                    $data_insert['page'] = $data['page'].'-'.$time;
                  }
                }

                $data_insert['code'] = $data['code'];
                $data_insert['c_date'] = date('Y-m-d H:i:s') ;
                $data_insert['c_by'] = $this->session->userdata('id');
                $this->db->insert('seo', $data_insert );
              }
            }
          }

        }
        public function loadSEO( $seo_id ){
          $this->db->where('id', $seo_id );
          $q = $this->db->get('seo');
          $ar_seo = $q->result_array();
          return $ar_seo;
        }
        public function loadGroup(){
          $return = array();
          $this->db->where('status', 1 );
          $this->db->where('lang', 'thailand' );
          $result = $this->db->get('group');
          $group = $result->result_array();
          return $group;
        }
        /*========= Category Function ========*/
        public function loadCategory( $cate_for = "" ){
          $this->db->where('status', 1 );
          if( $cate_for != "" ){
            $this->db->where('cate_for !=', $cate_for );
          }
          $this->db->where('lang', DEFAULT_LANG );
          $result = $this->db->get('category');
          return $result->result_array();
        }

        /*========= Menu Function ========*/
        public function loadMenu( $parent_code = 0 , $is_avai ){
          $this->db->where('status', 1 );
          if( $parent_code == 0 ){
            $this->db->where('parent_code', $parent_code );
          }
          $this->db->where('is_avai', $is_avai );
          $this->db->where('lang', DEFAULT_LANG );
          $this->db->order_by('sticky', 'asc' );
          $result = $this->db->get('menu');
          return $result->result_array();
        }
        /*public function loadHoverMenu(){
          $this->db->select('id,code,cate_code,is_parent, page_type, title');
          $this->db->where('status', 1 );
          $this->db->where('parent_code', 0 );
          $this->db->where('lang', DEFAULT_LANG );
          $this->db->order_by('sticky', 'asc' );
          //$this->db->order_by('id', 'asc' );
          $result = $this->db->get('pages');
          return $result->result_array();
        }*/
        public function loadSubMenu(){
          $this->db->select('id,code,cate_code,parent_code,title, page_type');
          $this->db->where('status', 1 );
          $this->db->where('parent_code !=', 0 );
          $this->db->where('lang', DEFAULT_LANG );
          $this->db->order_by('sticky', 'asc' );
          //$this->db->order_by('id', 'asc' );
          $result = $this->db->get('pages');
          return $result->result_array();
        }

        /*========= Pages Function ========*/
        public function loadAllPage(){
          $this->db->where('status', 1 );
        //   if( $cate_for != "" ){
        //     $this->db->where('cate_for !=', $cate_for );
        //   }
          $this->db->where('lang', DEFAULT_LANG );
          $result = $this->db->get('category');
          return $result->result_array();
        }
        public function loadPageDetail( $code ){
          $return = array();
          $this->db->where('code', $code );
          $result = $this->db->get('pages');
          $detailList = $result->result_array();
          foreach( $detailList as $data ){
            $return[$data['lang']] = $data ;
          }
          return $return;
        }
        public function loadPageDetailWithLang( $code ){
          $return = array();
          $this->db->select('pages.code,pages.title,pages.cate_code,category.name,pages.filter_type,pages.template');
          $this->db->from('pages');
          $this->db->join('category', 'category.code = pages.cate_code');
          $this->db->where('pages.code', $code );
          $this->db->where('pages.lang', DEFAULT_LANG );
          $result = $this->db->get();
          $detailList = $result->result_array();
          if( sizeof( $detailList ) > 0  ){
            return $detailList[0];
          }else{
            return array();
          }
        }
        public function loadPagesLists(){
          $return = array();
          $this->db->where('status', 1 );
          $this->db->where('lang', DEFAULT_LANG );
          $result = $this->db->get('pages');
          $detailList = $result->result_array();
          return $detailList;
        }
        public function loadLists( $code ){
          $return = array();
          $this->db->where('page_code', $code );
          $this->db->where('status', 1 );
          $this->db->where('lang', DEFAULT_LANG );
          $result = $this->db->get('pages_lists');
          $detailList = $result->result_array();
          return $detailList;
        }
        public function loadPageListDetail( $code ){
          $return = array();
          $this->db->where('code', $code );
          $result = $this->db->get('pages_lists');
          $detailList = $result->result_array();
          foreach( $detailList as $data ){
            $return[$data['lang']] = $data ;
          }
          return $return;
        }

        /*=========== Popup ===============*/
        public function loadPopupLists(){
          $return = array();
          $this->db->where('status', 1 );
          $result = $this->db->get('popup');
          $popups = $result->result_array();
          return $popups;
        }
        public function loadPopupDetail( $id ){
          $this->db->where('id', $id );
          $result = $this->db->get('popup');
          $detailList = $result->result_array();
          if( sizeof( $detailList ) > 0  ){
            return $detailList[0];
          }else{
            return array();
          }
        }
        /*======== Page list ===========*/
        public function pagesList(){
          $return = array();
          $this->db->where('status', 1 );
          $this->db->where('lang', 'thailand' );
          $this->db->order_by('sticky', 'desc' );
          $this->db->order_by('c_date', 'asc' );
          $result = $this->db->get('pages');
          $pages = $result->result_array();
          return $pages;
        }
        public function parentPagesList(){
          $return = array();
          $this->db->where('status', 1 );
          $this->db->where('onoff', 1 );
          $this->db->where('parent_code =', 0 );
          $this->db->where('lang', 'thailand' );
          $this->db->order_by('sticky', 'desc' );
          $this->db->order_by('c_date', 'asc' );
          $result = $this->db->get('pages');
          $pages = $result->result_array();
          return $pages;
        }
        public function subPagesList(){
          $return = array();
          $this->db->where('status', 1 );
          $this->db->where('onoff', 1 );
          $this->db->where('parent_code !=', 0 );
          $this->db->where('lang', 'thailand' );
          $this->db->order_by('sticky', 'desc' );
          $this->db->order_by('c_date', 'asc' );
          $result = $this->db->get('pages');
          $pages = $result->result_array();
          return $pages;
        }

        public function sectionList(){
          $return = array();
          $this->db->where('lang', 'thailand' );
          $this->db->where('status', 1 );
          $result = $this->db->get('section');
          $pages = $result->result_array();
          return $pages;
        }
        public function setAlert( $type , $message, $title = ""){
          $this->session->set_userdata('alert' , 1);
          $this->session->set_userdata('alert_type' ,  $type);
          $this->session->set_userdata('alert_msg' , $message);
          $this->session->set_userdata('alert_title' , $title);
        }
        public function loadApproveList( $approve_status = ''){
          $return = array();
          $this->db->select('section_lists.*, section_cate.title as cate_name, section_cate.slug as cate_slug, user.name as create_name, user.lastname as create_lastname');
          $this->db->from('section_lists');
          $this->db->join('section_cate','section_lists.cate_code = section_cate.code');
          $this->db->join('user','section_lists.c_by = user.id');
          $this->db->where('section_lists.status', 1 );
          if( $approve_status != ''){
            $this->db->where('section_lists.approve_status', $approve_status );
          }
          $this->db->where('section_lists.approve_status !=', 2 );
          $this->db->where('section_lists.approve_status !=', 0 );
          $this->db->where('section_lists.lang', 'thailand' );
          $this->db->where('section_cate.lang', 'thailand' );
          $this->db->order_by('section_lists.submit_date', 'desc' );
          $result = $this->db->get();
          $return = $result->result_array();
          return $return;
        }

        public function loadPageSection_backup( ){
          $return = array();
          
          $this->db->from( 'pages' );
          $this->db->where('status', 1 );
          $this->db->where( 'onoff', 1 );
          $this->db->where('lang', DEFAULT_LANG );
          $this->db->order_by( 'sticky', 'desc' );
          $this->db->order_by( 'id', 'asc' );
          $result = $this->db->get();
          $pages = $result->result_array();
          if( sizeof( $pages ) > 0 ){
            $return = $pages;

            if( sizeof( $return ) > 0 ){
              foreach ($return as $key => $row) {
                $sectionList = array();
                $this->db->where('lang', DEFAULT_LANG );
                $this->db->where('status', 1 );
                $this->db->where( 'onoff', 1 );
                $this->db->where('page_code', $row['code'] );
                $this->db->order_by( 'sticky', 'desc' );
                $this->db->order_by( 'id', 'asc' );
                $result = $this->db->get('section');
                $sections = $result->result_array();
                if ( sizeof( $sections ) ) {
                  $return[$key]['sections'] = $sections;
                } else {
                  $return[$key]['sections'] = array();
                }
              }
            }

          } else {
            $return = array();
          }

          return $return;
        }

        public function loadPageSection( $code = '' ){

          $return = array();

          $this->db->from( 'pages' );
          $this->db->where('status', 1 );
          $this->db->where( 'onoff', 1 );
          if( $this->session->userdata('user_type') == 'A'){
            $this->db->like( 'page_permission', ','.$this->session->userdata('user_type') );
          }
          $this->db->where('lang', DEFAULT_LANG );
          $this->db->order_by( 'sticky', 'desc' );
          $this->db->order_by( 'id', 'asc' );
          $result = $this->db->get();
          $pages = $result->result_array();
          if( sizeof( $pages ) > 0 ){
            foreach ($pages as $page_key => $page) {
              $this->db->where('page_code', $page['code'] );
              $this->db->where('status', 1 );
              $this->db->where('onoff', 1 );
              $this->db->order_by('sticky', 'desc' );
              $q = $this->db->get('row');
              $rows = $q->result_array();

              $this->db->where('status', 1 );
              $q = $this->db->get('column');
              $columns = $q->result_array();
        
              $this->db->where('lang', DEFAULT_LANG );
              $this->db->where('status', 1 );
              $this->db->where('onoff', 1 );
              $this->db->order_by('sticky', 'desc' );
              $this->db->order_by('c_date', 'asc' );
              $q = $this->db->get('section');
              $sections = $q->result_array();
              $pages[ $page_key ]['rows'] = array();
              foreach( $rows as $row_key => $row ){
                $pages[ $page_key ]['rows'][ $row_key ] = $row;
                $pages[ $page_key ]['rows'][ $row_key ]['columns'] = array();
                foreach( $columns as $col_key => $column ){
                  
                  if( $column['row_id'] == $row['id']){ 
                    $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ] = $column;
                    $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'] = array();

                    foreach( $sections as $sec_key => $section ){
                      if( $column['id'] == $section['column_id']){ 
                        $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key] = $section; 
                        $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'] = array();
                        $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key]['banner_listings'] = array();
                        if( $section['section_type'] == "listing"){
                          // Start load category in section
                          $this->db->like('sec_code','"'.$section['code'].'"', 'both');
                          $this->db->where('lang', DEFAULT_LANG );
                          $this->db->where('onoff', 1 );
                          $this->db->where('status', 1 );
                          $this->db->where('status', 1 );
                          $this->db->order_by('sticky' , 'asc' );
                          $result = $this->db->get('section_cate');
                          $categories = $result->result_array();
                          //array_push( $sections[$key]['categories'] , $categories);
                          $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'] = array();
                          $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0] = array();
                          $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0]['code'] = '';
                          foreach( $categories as $cate_key => $category ){
                            $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][$cate_key+1] = $category;
                          }
                          // Start load listing in category
                          //if( sizeof( $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'] ) > 0 && $section['tab_setting'] == 'all_content' ){
                            $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0]['listings'] = array();
                            
                            $this->db->select('section_lists.*, section_cate.slug as cate_slug, section_cate.title as cate_title');
                            $this->db->from('section_lists');
                            $this->db->join('section_cate','section_lists.cate_code = section_cate.code');
                            if( sizeof( $categories ) > 0 ){
                              $this->db->group_start();
                                foreach( $categories as $cate_key => $category ){
                                  if( $cate_key == 0 ){
                                    $this->db->where('section_lists.cate_code',$category['code']);
                                  }else{
                                    $this->db->or_where('section_lists.cate_code',$category['code']);
                                  }
                                }
                              $this->db->group_end();
                            }

                            $this->db->where('section_lists.lang', DEFAULT_LANG );
                            $this->db->where('section_cate.lang', DEFAULT_LANG );
                            $this->db->where('section_lists.onoff', 1 );
                            $this->db->where('section_lists.status', 1 );
                            $this->db->where('section_lists.published_date <=', date('Y-m-d H:i:s') );
                            $this->db->order_by('section_lists.sticky' , 'desc' );
                            $this->db->order_by('section_lists.is_highlight' , 'desc' );
                            $this->db->order_by('section_lists.c_date' , 'desc' );
                            if( $section['list_limit'] != 0 ){
                              $this->db->limit( $section['list_limit'] );
                            }
                            
                            $result = $this->db->get();
                            $listings = $result->result_array();
                            //array_push( $sections[$key]['categories'][$cate_key]['listings'] , $listings );
                            $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][0]['listings'] = $listings;
                          //}else{
                            foreach( $categories as $cate_key => $category ){
                              $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][$cate_key+1]['listings'] = array();
                              $this->db->where('cate_code',$category['code']);
                              $this->db->where('lang', DEFAULT_LANG );
                              $this->db->where('onoff', 1 );
                              $this->db->where('status', 1 );
                              $this->db->where('published_date <=', date('Y-m-d H:i:s') );
                              $this->db->order_by('sticky' , 'desc' );
                              $this->db->order_by('is_highlight' , 'desc' );
                              $this->db->order_by('c_date' , 'desc' );
                              if( $section['list_limit'] != 0 ){
                                $this->db->limit( $section['list_limit'] );
                              }
                              $result = $this->db->get('section_lists');
                              $listings = $result->result_array();
                              foreach( $listings as $list_key => $lists ){
                                $listings[ $list_key ]['cate_slug'] = $category['slug'];
                                $listings[ $list_key ]['cate_title'] = $category['title'];
                              }
                              //array_push( $sections[$key]['categories'][$cate_key]['listings'] , $listings );
                              $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key]['categories'][$cate_key+1]['listings'] = $listings;
                            //}
                          }
                          

                        }elseif($section['section_type'] == "banner"){
                          $this->db->where('sec_code',$section['code']);
                          $this->db->where('lang', DEFAULT_LANG );
                          $this->db->where('onoff', 1 );
                          $this->db->where('status', 1 );
                          $this->db->order_by('sticky' , 'desc' );
                          //$this->db->order_by('is_highlight' , 'desc' );
                          $result = $this->db->get('banner');
                          $banner_listings = $result->result_array();
                          $pages[ $page_key ]['rows'][ $row_key ]['columns'][ $col_key ]['sections'][$sec_key]['banner_listings'] = $banner_listings;
                        }
                      }
                    }
                    

                  }
                }
              }
              
            }
          }
          
          $return = $pages;

          return $return;
      }


        public function getPageSlug( $sec_code = '' ){
          // by section code
          $return = array();
          $this->db->select( 'pages.page_slug' );
          $this->db->from( 'section' );
          $this->db->join('pages', 'pages.code = section.page_code');
          $this->db->where('section.code', $sec_code );
          $this->db->where('section.lang', DEFAULT_LANG );

          $result = $this->db->get();
          $return = $result->row_array();

          return $return;
        }


}
?>
