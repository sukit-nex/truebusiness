<?php
class Login_model extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
        public function checkLogin( $username, $password ){
          $pass_enc = hash('sha256', $password );

          $this->db->where('username', $username );
          $this->db->where('password', $pass_enc );
          $this->db->where('onoff', 1 );
          $this->db->where('status', 1 );
          $val = $this->db->get('user');
          //$val = $result->result_array();

          if ($val->num_rows()) {

            //$apc_key = "{$_SERVER['SERVER_NAME']}~login:{$_SERVER['REMOTE_ADDR']}";
            //apcu_delete($apc_key);

              foreach ($val->result_array() as $recs => $res) {
                  $this->session->set_userdata(array(
                      'id' => $res['id'],
                      'name' => $res['name'],
                      'lastname' => $res['lastname'],
                      'username' => $res['username'],
                      'thumb' => $res['thumb'],
                      'email' => $res['email'],
                      'position' => $res['position'],
                      'is_admin_login' => true,
                      'user_type' => $res['user_type'],
                      'location_code' => $res['location_code']
                    )
                  );
              }
              return true;
          } else {
              $this->session->set_userdata('error', '<strong>Access Denied</strong> Invalid Username/Password');
              return false;
          }
        }
}
?>
