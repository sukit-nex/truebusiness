<?php
class Pages_model extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
        public function loadLists(){
          $this->db->select('*');
          $this->db->from( 'pages' );
          $this->db->where('pages.status', 1 );
          $this->db->where('pages.lang', DEFAULT_LANG );
          $this->db->order_by('pages.sticky', 'desc' );
          $this->db->order_by('pages.c_date', 'desc' );
          $result = $this->db->get();
          return $result->result_array();
        }
        public function loadDetail( $code ){
          $return = array();
          $this->db->select('*');
          $this->db->from( 'pages' );
          $this->db->where('pages.code', $code );
          $result = $this->db->get();
          $detailList = $result->result_array();
          foreach( $detailList as $data ){
            $return[$data['lang']] = $data ;
          }
          return $return;
        }
}
?>
