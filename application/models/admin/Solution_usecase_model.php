<?php
class Solution_usecase_model extends CI_Model {

	public function __construct(){
		// Call the CI_Model constructor
		parent::__construct();
	}
	public function loadLists( $cate_group = "" ){
		$this->db->select('*');
		$this->db->from( 'solution_usecase' );
		$this->db->where('status', 1 );
		$this->db->where('lang', DEFAULT_LANG );

		if( $cate_group != ""){
			$this->db->where('cate_for', $cate_group );
		}
		$this->db->order_by('sticky', 'desc' );
		$this->db->order_by('c_date', 'desc' );
		$result = $this->db->get();
		return $result->result_array();
	}
	public function loadDetail( $code ){
		$return = array();
		$this->db->where('code', $code );
		$result = $this->db->get('solution_usecase');
		$detailList = $result->result_array();
		foreach( $detailList as $data ){
			$return[$data['lang']] = $data ;
		}
		return $return;
	}
}
?>
