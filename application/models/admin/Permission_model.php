<?php
class Permission_model extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
        public function loadDetail( $code ){
          $return = array();
          $this->db->where('code', $code );
          $result = $this->db->get('setting');
          $detailList = $result->result_array();
          foreach( $detailList as $data ){
            $return[$data['lang']] = $data ;
          }
          return $return;
        }
        public function loadWords(){
          $this->db->order_by('id', 'asc' );
          $result = $this->db->get('wording');
          return $result->result_array();
        }
        public function loadWordDetail( $id ){
          $this->db->where('id', $id );
          $result = $this->db->get('wording');
          $return = $result->result_array();
          return $return[0];
        }
}
?>
