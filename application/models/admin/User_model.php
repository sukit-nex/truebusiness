<?php
class User_model extends CI_Model {
      // Model for common function
      //  public $title;
      //  public $content;
      //  public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
        public function loadLists(){
          $this->db->where('status', 1 );
          if( $this->session->userdata('user_type') != "SA" ){
            $this->db->where('user_type !=', 'SA' );
          }
          $this->db->order_by('c_date', 'desc' );
          $result = $this->db->get('user');
          return $result->result_array();
        }
        public function loadDetail( $id ){
          $return = array();
          $this->db->where('id', $id );
          $result = $this->db->get('user');
          $detailList = $result->result_array();
          return $detailList;
        }
}
?>
