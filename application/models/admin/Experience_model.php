<?php
class Experience_model extends CI_Model {

	public function __construct(){
		// Call the CI_Model constructor
		parent::__construct();
	}

	public function load_solution_used_lists( $category = "" ){
		$this->db->from( 'solution' );
		$this->db->where('status', 1 );
		$this->db->where('lang', DEFAULT_LANG );
		if( $category != ""){
			$this->db->where('category', $category );
		}
		//$this->db->order_by('sticky', 'desc' );
		$this->db->order_by('c_date', 'desc' );
		$result = $this->db->get();
		return $result->result_array();
	}

	public function load_business_type_detail( $code ){
		$return = array();
		$this->db->where('code', $code );
		$result = $this->db->get('solution_business_type');
		$detailList = $result->result_array();
		foreach( $detailList as $data ){
			$return[$data['lang']] = $data ;
		}
		return $return;
	}
	
	public function load_experience_lists( $category = "" ){
		$this->db->from( 'experience' );
		$this->db->where('status', 1 );
		$this->db->where('lang', DEFAULT_LANG );
		// if( $category != ""){
		// 	$this->db->where('category', $category );
		// }
		$this->db->order_by('sticky', 'desc' );
		$this->db->order_by('c_date', 'desc' );
		$result = $this->db->get();
		return $result->result_array();
	}

	public function load_category_lists( $category = "" ){
		$this->db->from( 'experience_category' );
		$this->db->where('status', 1 );
		$this->db->where('lang', DEFAULT_LANG );
		// if( $category != ""){
		// 	$this->db->where('category', $category );
		// }
		$this->db->order_by('sticky', 'desc' );
		$this->db->order_by('c_date', 'desc' );
		$result = $this->db->get();
		return $result->result_array();
	}

	public function load_solution_category_lists( $category = "" ){
		$this->db->from( 'solution_category' );
		$this->db->where('status', 1 );
		$this->db->where('lang', DEFAULT_LANG );
		// if( $category != ""){
		// 	$this->db->where('category', $category );
		// }
		$this->db->order_by('sticky', 'desc' );
		$this->db->order_by('c_date', 'desc' );
		$result = $this->db->get();
		return $result->result_array();
	}

	public function load_category_detail( $code ){
		$return = array();
		$this->db->where('code', $code );
		$result = $this->db->get('experience_category');
		$detailList = $result->result_array();
		foreach( $detailList as $data ){
			$return[$data['lang']] = $data ;
		}
		return $return;
	}
	
	public function load_experience_detail( $code ){
		$return = array();
		$this->db->where('code', $code );
		$result = $this->db->get('experience');
		$detailList = $result->result_array();
		foreach( $detailList as $data ){
			$return[$data['lang']] = $data ;
		}
		return $return;
	}
}
?>
