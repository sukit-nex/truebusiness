<?php
class Landing_model extends CI_Model {

	public function __construct(){
		// Call the CI_Model constructor
		parent::__construct();
	}

	public function load_landing_lists( $category = "" ){
		$this->db->from( 'landing' );
		$this->db->where('status', 1 );
		$this->db->where('lang', DEFAULT_LANG );
		$this->db->order_by('c_date', 'desc' );
		$result = $this->db->get();
		return $result->result_array();
	}

	public function load_landing_detail( $code ){
		$return = array();
		$this->db->where('code', $code );
		$result = $this->db->get('landing');
		$detailList = $result->result_array();
		foreach( $detailList as $data ){
			$return[$data['lang']] = $data ;
		}
		return $return;
	}
}
?>
