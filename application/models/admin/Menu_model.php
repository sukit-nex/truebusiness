<?php
class Menu_model extends CI_Model {

	public function __construct(){
		// Call the CI_Model constructor
		parent::__construct();
	}
	public function loadLists( $position = "top" ){
		$return = array();
		$this->db->where('status', 1 );
		$this->db->where('position', $position );
		$this->db->where('lang', DEFAULT_LANG );
		$this->db->order_by('sticky', 'desc' );
		$result = $this->db->get('menu');
		$ar_menus = $result->result_array();

		$ar_level0 = array();
		// Set level 0
		foreach( $ar_menus as $menu ){
			if( $menu['parent_code'] == 0 ){
				$menu['sub_level1'] = array();
				array_push($ar_level0, $menu);
			}
		}
		foreach( $ar_level0 as $key => $level0 ){
			foreach( $ar_menus as $menu ){
				if( $menu['parent_code'] == $level0['code'] ){
					$menu['sub_level2'] = array();
					array_push( $ar_level0[$key]['sub_level1'], $menu);
				}
			}
		}
		foreach( $ar_level0 as $key0 => $level0 ){
			foreach( $level0['sub_level1'] as $key1 => $level1 ){
				foreach( $ar_menus as $menu ){
					if( $menu['parent_code'] == $level1['code'] ){
						array_push( $ar_level0[$key0]['sub_level1'][$key1]['sub_level2'], $menu);
					}
				}
			}
		}
		return $ar_level0;
	}
	public function loadDetail( $code ){
		$return = array();
		$this->db->where('code', $code );
		$result = $this->db->get('menu');
		$detailList = $result->result_array();
		foreach( $detailList as $data ){
			$return[$data['lang']] = $data ;
		}
		return $return;
	}
}
?>
