<?php
class Section_model extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
        public function loadDetail( $page , $sec_key ){
          $return = array();
		  $this->db->where('page', $page );
		  $this->db->where('sec_key', $sec_key );
          $result = $this->db->get('section');
          $detailList = $result->result_array();
          foreach( $detailList as $data ){
            $return[$data['lang']] = $data ;
          }
          return $return;
		}
		public function loadSectionDetail( $code ){
          $return = array();
		  $this->db->where('code', $code );
          $result = $this->db->get('section');
          $detailList = $result->result_array();
          foreach( $detailList as $data ){
            $return[$data['lang']] = $data ;
          }
          return $return;
		}
		public function loadFooterDetail( $page ){
          $return = array();
		  $this->db->where('page', $page );
          $result = $this->db->get('section_footer');
          $detailList = $result->result_array();
          foreach( $detailList as $data ){
            $return[$data['lang']] = $data ;
          }
          return $return;
		}
		public function loadSectionListsP1( $page ){
			$return = array();
			$this->db->where('page', $page );
			$this->db->where('lang', 'thailand' );
			$this->db->where('status', 1 );
			$this->db->order_by('sticky', 'desc' );
			$result = $this->db->get('section');
			$return = $result->result_array();
			return $return;
		}
		public function loadSectionLists( $page_code  ){
			$return = array();
			$this->db->where('page_code', $page_code );
			$this->db->where('status', 1 );
			$this->db->order_by('sticky', 'desc' );
			$q = $this->db->get('row');
			$rows = $q->result_array();

			$this->db->where('status', 1 );
			$q = $this->db->get('column');
			$columns = $q->result_array();

			$this->db->where('lang', 'thailand' );
			$this->db->where('status', 1 );
			$this->db->order_by('sticky', 'desc' );
			$this->db->order_by('c_date', 'asc' );
			$q = $this->db->get('section');
			$sections = $q->result_array();

			foreach( $rows as $key => $row ){
				$rows[ $key ]['columns'] = array();
				foreach( $columns as $col_key => $column ){
					if( $column['row_id'] == $row['id']){ 
						$rows[ $key ]['columns'][ $col_key ] = $column;
						$rows[ $key ]['columns'][ $col_key ]['sections'] = array();
						foreach( $sections as $sec_key => $sec ){
							if( $column['id'] == $sec['column_id']){ 
								$rows[ $key ]['columns'][ $col_key ]['sections'][$sec_key] = $sec; 
							}
						}
					}
				}
			}
			$return = $rows;

			/*$this->db->where('page_code', $page_code );
			$this->db->where('lang', 'thailand' );
			$this->db->where('status', 1 );
			$this->db->order_by('sticky', 'desc' );
			$this->db->order_by('c_date', 'asc' );
			$result = $this->db->get('section');
			$return = $result->result_array();*/
			return $return;
		}
		public function loadPageName( $page_code  ){
			$return = array();
			$this->db->where('code', $page_code );
			$this->db->where('lang', 'thailand' );
			$this->db->where('status', 1 );
			$this->db->order_by('sticky', 'desc' );
			$this->db->order_by('c_date', 'asc' );
			$result = $this->db->get('pages');
			$return = $result->result_array();
			return $return;
		}


		
		

		public function loadCategoryList( $page , $sec_key ){
			$return = array();
			$this->db->where('page', $page );
			$this->db->where('sec_key', $sec_key );
			$this->db->where('lang', 'thailand' );
			$this->db->where('status', 1 );
			$result = $this->db->get('section_cate');
			$return = $result->result_array();
			return $return;
		}


		public function loadCategoriesList( $sec_code = "", $onoff = "" ){
			$return = array();
			if( $sec_code != ""){
				$this->db->like('sec_code', '"'.$sec_code.'"', 'both' );
			}
			$this->db->where('lang', 'thailand' );
			if( $onoff != "" ){
				$this->db->where('onoff', $onoff );
			}
			$this->db->where('status', 1 );
			$result = $this->db->get('section_cate');
			$return = $result->result_array();
			//echo  $this->db->last_query();
			return $return;
		}

		public function loadCategoryDetail( $page = "", $sec_key = "", $sec_code = "", $code ="" ){
			$return = array();
			$this->db->where('code', $code );
			if( $page != ""){
				$this->db->where('page', $page );
			}
			if( $sec_key != ""){
				$this->db->where('sec_key', $sec_key );
			}
			if( $sec_code != ""){
				$this->db->where('sec_code', $sec_code );
			}
			$result = $this->db->get('section_cate');
			$detailList = $result->result_array();
			foreach( $detailList as $data ){
				$return[$data['lang']] = $data ;
			}
			return $return;
		}
		public function loadLists( $cate_code ){ // For phase 2
			$return = array();
			$this->db->where('cate_code', $cate_code );

			$this->db->where('lang', DEFAULT_LANG );
			$this->db->where('status', 1 );
			$this->db->order_by('is_highlight', 'desc');
			$this->db->order_by('sticky', 'desc');
			$this->db->order_by('published_date', 'asc');
			$result = $this->db->get('section_lists');
			$return = $result->result_array();
			return $return;
		}
		 public function loadListing( $page , $sec_key, $code  ){ // For phase 1
		 	$return = array();
		 	$this->db->where('page', $page );
		 	$this->db->where('sec_key', $sec_key );
			if( $code != '' ){
				$this->db->where('cate_code',$code);
			}
		 	$this->db->where('lang', 'thailand' );
		 	$this->db->where('status', 1 );
		 	$this->db->order_by('sticky', 'desc');
		 	$this->db->order_by('post_date', 'asc');
		 	$result = $this->db->get('section_lists');
		 	$return = $result->result_array();
			 //echo $this->db->last_query();
		 	return $return;
		 }

		public function loadListsDetail( $page, $sec_key, $code ){
			$return = array();
			$this->db->where('code', $code );
			$result = $this->db->get('section_lists');
			$detailList = $result->result_array();
			foreach( $detailList as $data ){
				$return[$data['lang']] = $data ;
			}
			return $return;
		}		
		public function loadListingDetail( $cate_code, $code ){
			$return = array();
			$this->db->where('code', $code );
			$result = $this->db->get('section_lists');
			$detailList = $result->result_array();
			foreach( $detailList as $data ){
				$return[$data['lang']] = $data ;
			}
			return $return;
		}
		public function loadSectionBannerList( $sec_code ){
			$return = array();
			$this->db->where('sec_code', $sec_code );
			$this->db->where('lang', DEFAULT_LANG );
			$this->db->where('status', 1 );
			$this->db->order_by('sticky', 'desc');
			$this->db->order_by('c_date', 'desc');
			$result = $this->db->get('banner');
			$return = $result->result_array();
			return $return;
		}
		public function loadSectionBannerDetail( $sec_code, $code ){
			$return = array();
			$this->db->where('code', $code );
			$result = $this->db->get('banner');
			$detailList = $result->result_array();
			foreach( $detailList as $data ){
				$return[$data['lang']] = $data ;
			}
			return $return;
		}
		public function loadSolutionListingDetail( $cate_code, $code ){
			$return = array();
			$this->db->where('code', $code );
			$result = $this->db->get('section_solution_lists');
			$detailList = $result->result_array();
			foreach( $detailList as $data ){
				$return[$data['lang']] = $data ;
			}
			return $return;
		}
		public function loadSolutionLists( $cate_code ){ // For phase 2
			$return = array();

			if( $cate_code != ""){
				$this->db->like('cate_code', '"'.$cate_code.'"', 'both' );
			}

			$this->db->where('lang', DEFAULT_LANG );
			$this->db->where('status', 1 );
			//$this->db->order_by('is_highlight', 'desc');
			$this->db->order_by('sticky', 'desc');
			$this->db->order_by('published_date', 'asc');
			$result = $this->db->get('section_solution_lists');
			$return = $result->result_array();
			return $return;
		}
		public function loadSolutionUsecaseList( $sec_code = "", $onoff = "" ){
			$return = array();
			$this->db->where('lang', 'thailand' );
			if( $onoff != "" ){
				$this->db->where('onoff', $onoff );
			}
			$this->db->where('status', 1 );
			$result = $this->db->get('solution_usecase');
			$return = $result->result_array();
			//echo  $this->db->last_query();
			return $return;
		}
		public function loadSolutionBannerList( $sec_code = "", $onoff = "" ){
			$return = array();
			$this->db->where('lang', 'thailand' );
			if( $onoff != "" ){
				$this->db->where('onoff', $onoff );
			}
			$this->db->where('status', 1 );
			$result = $this->db->get('solution_banner');
			$return = $result->result_array();
			//echo  $this->db->last_query();
			return $return;
		}
}
?>
