<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Front_Controller extends CI_Controller {
  function __construct()
    {
        // Load common data , model below
        parent::__construct();

        // Enter password before
        // if( !$this->session->userdata('authen-login') ){
        //   if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])){
        //     $Username = $_SERVER['PHP_AUTH_USER'];
        //     $Password = $_SERVER['PHP_AUTH_PW'];

        //     if ($Username == 'true5guat' && $Password == 'ftHnFTu8e0S1MS3wmU7u') {
        //       $this->session->set_userdata('authen-login', true);
        //     }else{
        //       $this->session->set_userdata('authen-login', false);
        //       header('WWW-Authenticate: Basic realm="Secret page"');
        //       header('HTTP/1.0 401 Unauthorized');
        //       exit();
        //     }
        //   }else{
        //     header('WWW-Authenticate: Basic realm="Secret page"');
        //     header('HTTP/1.0 401 Unauthorized');
        //     exit();
        //   }
        // }

        // if( !$this->session->userdata('authen-login') ){
        //   header('WWW-Authenticate: Basic realm="Secret page"');
        //   header('HTTP/1.0 401 Unauthorized');
        // }
        // End Enter password before

        $this->load->library('form_validation');
		    $this->load->driver('cache', array('adapter' => 'redis','backup' => 'file'));
        $this->viewFolder = "front/";
        $this->modelFolder = "front/";
        $this->data = array();

        if( $this->session->userdata('lang') == "" ){
          $this->session->set_userdata('lang', DEFAULT_LANG );
        }
        $this->data['page_seo'] = array();
        $ar_url = parse_url( $_SERVER['REQUEST_URI'] );

       
        //echo $ar_url['path'];
        if( $ar_url['path'] == 'th' || $ar_url['path'] == '/th' || $ar_url['path'] == '/th/' ){
          redirect('th/home');
        }else if( $ar_url['path'] == 'en' || $ar_url['path'] == '/en' || $ar_url['path'] == '/en/' ){
          redirect('en/home');
        }
        
        $sub_path = substr($ar_url['path'],0,4);
        $api_path = substr($ar_url['path'],0,5);
        if( $api_path !== "/API/" ){
          if( $sub_path == "/th/" ){
            $this->session->set_userdata('lang', 'thailand' );
          }else if( $sub_path == "/en/" ){
            $this->session->set_userdata('lang', 'english' );
          }else{
            if( $ar_url['path'] != '/' ){
              if( $this->session->userdata('lang') == "thailand"){
                redirect('th'.$ar_url['path']);
              }else{
                redirect('en'.$ar_url['path']);
              }
            }

          }

          if( $ar_url['path'] == '/' ){
            if( $this->session->userdata('lang') == "thailand"){
              redirect('th/home');
            }else{
              redirect('en/home');
            }
          }
          
        }
        if( $this->session->userdata('lang') == "thailand"){
          $this->data['lang_path'] = "th";
        }else{
          $this->data['lang_path'] = "en";
        }

        


        $this->data['lang'] = $this->session->userdata('lang');
        $this->load->model($this->modelFolder.'Common_model','common',TRUE);

        $landing_path = substr($ar_url['path'],4,7);
        $landing_arr = explode('/', $ar_url['path']);
        
        $this->data['landing_scripts'] = array();

        if ( sizeof($landing_arr) == 4 && $landing_arr[2] == 'landing' ) {
            if ( $landing_arr[3] != '' ) {
                $this->data['landing_scripts'] = $this->common->get_landing_scripts( $landing_arr[3] );
            }
        }

        $this->data['page_check'] = "";
        $this->data['menu_check'] = "";
        $this->data['sub_menu_check'] = "";
        $this->data['banners'] = array();
        $this->data['words'] = $this->common->loadWord();
        $this->data['global_seo'] = $this->common->loadSeo('global');

        $this->data['smepack_brochure_pdf'] = $this->common->smepack_brochure_pdf();
        $this->data['solution_brochure'] = $this->common->solution_brochure();

        
        $this->data['menus'] = $this->common->loadMenu('top');
        $this->data['pages'] = $this->common->loadPages();
        // Check site stat
        if( !isset( $_SESSION['s_token'] )){
          $time_mod = time();
          $this->session->set_userdata('s_token', md5( $time_mod ) );
        }
        if( !isset( $_SESSION['client_ip'] )){
          $ip_address = getRealIpAddr();
          $this->session->set_userdata('client_ip', $ip_address );
        }




    }
    function page_construct($page, $data = array()) {
        /*if( isset( $data['preview'] ) ){
          define('UPLOAD_PATH', REAL_PATH.'uploads/temp/');
        }*/
        $this->load->view($this->viewFolder . 'inc/inc_meta', $data);
        //$this->load->view($this->viewFolder . 'inc/inc_header_cg', $data);
        $this->load->view($this->viewFolder . 'inc/inc_header', $data);
        $this->load->view($this->viewFolder.$page, $data);
        /*if( $page == "page_template" || $page == "listing_detail" || $page == "tag_listing"){
          $this->load->view($this->viewFolder . 'inc/inc_footer_main', $data);
        }else{
          $this->load->view($this->viewFolder . 'inc/inc_footer', $data);
        }*/
        $this->load->view($this->viewFolder . 'inc/inc_footer_main', $data);
        $this->load->view($this->viewFolder . 'inc/inc_script');
    }
    function page_html( $page, $data = array() ){
      $this->load->view($this->viewFolder.$page, $data);
    }
    function page_construct_nohead($page, $data = array()) {
        //$this->load->view($this->viewFolder . 'inc/meta');
        $this->load->view($page, $data);
        //$this->load->view($this->viewFolder . 'inc/script');
    }
    function page_construct_nopage($page, $data = array()) {
        $this->load->view('errors/'.$page, $data);
    }
}
