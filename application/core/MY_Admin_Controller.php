<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Admin_Controller extends CI_Controller {
  function __construct()
    {
        // Load common data , model below
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->driver('cache', array('adapter' => 'redis','backup' => 'file'));
        $this->viewFolder = "admin/";
        $this->modelFolder = "admin/";
        $this->data = array();
        $this->data['page'] = '';

        $this->admin_login = $this->session->userdata('is_admin_login');
        if( !$this->admin_login ){
          redirect('admin/login');
        }
        date_default_timezone_set("Asia/Bangkok");
        $this->load->model($this->modelFolder.'Common_model','common',TRUE);
        $this->data['permission_role'] = getEditorRole();
        $this->data['langs'] = $this->common->loadLang();
        $this->data['cates'] = $this->common->loadSectionCategory();
        $this->data['menu_editor_active'] = '';
        //$this->data['menus'] = $this->common->loadMenu( 0 , 0 );
        //$this->data['avai_menus'] = $this->common->loadMenu(  0 , 1 );
        //$this->data['sub_menus'] = $this->common->loadMenu( 'sub' , 0 );
        $this->data['pagesList'] = $this->common->pagesList();
        $this->data['parent_pages'] = $this->common->parentPagesList();
        $this->data['sub_pages'] = $this->common->subPagesList();
        
        foreach( $this->data['parent_pages'] as $key => $parent ){
          $this->data['parent_pages'][$key]['sub_pages'] = array();
          foreach( $this->data['sub_pages'] as $sub ){
            if( $sub['parent_code'] == $parent['code'] ){
              array_push( $this->data['parent_pages'][$key]['sub_pages'] , $sub);
            }
          }
        }
        $this->data['section_list'] = $this->common->sectionList();
        $this->data['wording'] = $this->common->wording();
        $this->data['seo'] = array();
        $this->data['time'] = time();
    }
   
    function page_construct($page, $data = array() , $menu_active = "") {
        $this->load->view($this->viewFolder . 'inc/meta', $data);
        $this->load->view($this->viewFolder . 'inc/header');
        $this->load->view($this->viewFolder . 'inc/sidemenu', $data);
        $this->load->view($this->viewFolder.$page, $data);
        $this->load->view($this->viewFolder . 'inc/footer');
        $this->load->view($this->viewFolder . 'inc/script', $data);
        if( $this->data['menu_active'] == "dashboard" ){
          $this->load->view($this->viewFolder . 'inc/dashboard_js', $data);
        }
    }
    function page_construct_nohead($page, $data = array()) {
        $this->load->view($this->viewFolder . 'inc/meta');
        $this->load->view($this->viewFolder.$page, $data);
        $this->load->view($this->viewFolder . 'inc/script');
    }
    function page_export($page, $data = array()) {
        $this->load->view($page, $data);
    }
}
