<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
//$route['404_override'] = '';
//$route['translate_uri_dashes'] = FALSE;

$route['default_controller'] = 'home';
$route['translate_uri_dashes'] = FALSE;
$route['admin'] = 'admin/login';
$route['404_override'] = 'errors/nopage';
$route['500_override'] = 'errors/serverError';

// New route for seo best practice
$route['API/(:any)'] = 'API/$1';

$route['th'] = 'pages/template/home';
$route['en'] = 'pages/template/home';
// $route['home'] = 'home';
//$route['th/home'] = 'home';
//$route['en/home'] = 'home';

$route['th/immersive-experience'] = 'home/pages/immersive/page_immersive';
$route['en/immersive-experience'] = 'home/pages/immersive/page_immersive';

$route['th/true5g-consumer/immersive-experience'] = 'home/pages/immersive/page_immersive';
$route['en/true5g-consumer/immersive-experience'] = 'home/pages/immersive/page_immersive';

$route['th/ui'] = 'home/pages/ui';
$route['en/ui'] = 'home/pages/ui';

$route['th/xr'] = 'home/pages/xr/page_xr';
$route['en/xr'] = 'home/pages/xr/page_xr';

$route['th/xr_cms'] = 'home/pages/xr_cms/page_xr';
$route['en/xr_cms'] = 'home/pages/xr_cms/page_xr';

$route['th/ar_cms'] = 'home/pages/ar_cms/page_ar';
$route['en/ar_cms'] = 'home/pages/ar_cms/page_ar';

$route['th/vr_cms'] = 'home/pages/vr_cms/page_vr';
$route['en/vr_cms'] = 'home/pages/vr_cms/page_vr';

$route['th/ar'] = 'home/pages/ar/page_ar';
$route['en/ar'] = 'home/pages/ar/page_ar';

$route['th/vr'] = 'home/pages/vr/page_vr';
$route['en/vr'] = 'home/pages/vr/page_vr';

$route['th/page'] = 'home/pages/page/page_page';
$route['en/page'] = 'home/pages/page/page_page';

$route['th/listing-detail'] = 'home/listing_detail';
$route['en/listing-detail'] = 'home/listing_detail';

$route['th/tag_listing'] = 'home/tag_listing';
$route['en/tag_listing'] = 'home/tag_listing';

// $route['th/coverage_choice'] = 'home/coverage_choice';
// $route['en/coverage_choice'] = 'home/coverage_choice';

$route['th/solution/(:any)'] = 'pages/solution_cate/$1';
$route['en/solution/(:any)'] = 'pages/solution_cate/$1';

$route['th/solution/(:any)/(:any)'] = 'pages/solution_detail/$1/$2'; // solution/cate_slug/detail_slug
$route['en/solution/(:any)/(:any)'] = 'pages/solution_detail/$1/$2';

$route['th/landing/(:any)'] = 'pages/landing_detail/$1';
$route['en/landing/(:any)'] = 'pages/landing_detail/$1';


$route['th/experience/(:any)'] = 'pages/experience_cate/$1';
$route['en/experience/(:any)'] = 'pages/experience_cate/$1';

$route['th/experience/(:any)/(:any)'] = 'pages/experience_detail/$1/$2'; // solution/cate_slug/detail_slug
$route['en/experience/(:any)/(:any)'] = 'pages/experience_detail/$1/$2';


// $route['th/solution-detail'] = 'home/solution_detail';
// $route['en/solution-detail'] = 'home/solution_detail';

// $route['th/smepack'] = 'pages/smepack';
// $route['en/smepack'] = 'pages/smepack';
$route['th/smepack/(:any)'] = 'pages/smepack/$1';
$route['en/smepack/(:any)'] = 'pages/smepack/$1';
//$route['en/smepack'] = 'pages/smepack';
$route['th/smepack/(:any)/(:any)'] = 'pages/smepack/$1/$2';
$route['en/smepack/(:any)/(:any)'] = 'pages/smepack/$1/$2';


$route['th/coverage_v4'] = 'home/coverage_v4';
$route['en/coverage_v4'] = 'home/coverage_v4';

$route['th/search'] = 'pages/search';
$route['en/search'] = 'pages/search';

$route['th/content/(:any)/(:any)'] = 'pages/listview/$1/$2';
$route['en/content/(:any)/(:any)'] = 'pages/listview/$1/$2';

// Preview route
$route['th/preview/(:any)'] = 'preview/template/$1';
$route['en/preview/(:any)'] = 'preview/template/$1';

$route['th/preview/(:any)/(:any)'] = 'preview/template/$1/$2';
$route['en/preview/(:any)/(:any)'] = 'preview/template/$1/$2';

$route['th/preview/(:any)/(:any)/(:any)'] = 'preview/listview/$1/$2/$3';
$route['en/preview/(:any)/(:any)/(:any)'] = 'preview/listview/$1/$2/$3';
// End preview

$route['th/(:any)'] = 'pages/template/$1';
$route['en/(:any)'] = 'pages/template/$1';

$route['th/(:any)/(:any)'] = 'pages/template/$1/$2';
$route['en/(:any)/(:any)'] = 'pages/template/$1/$2';

$route['th/content/(:any)/(:any)'] = 'pages/listview/content/$1/$2';
$route['en/content/(:any)/(:any)'] = 'pages/listview/content/$1/$2';

// $route['th/(:any)/(:any)/(:any)'] = 'pages/listview/$1/$2/$3';
// $route['en/(:any)/(:any)/(:any)'] = 'pages/listview/$1/$2/$3';






//$route['search'] = 'search';
//$route['(:any)'] = 'pages/view/$1';

//$route['product/(:any)'] = 'pages/view/$1';
//$route['content/(:any)'] = 'pages/view/$1';

//$route['content/(:any)/(:any)'] = 'pages/listview/content/$1/$2';
//$route['product/(:any)/(:any)'] = 'pages/listview/product/$1/$2';
